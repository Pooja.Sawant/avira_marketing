﻿using Avira.MarketingResearch.Common.IService.ImportExport;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.ImportExport
{
    public class CompanyBulkImportExportService : ICompanyBulkImportExportService
    {
        private readonly ICompanyBulkImportExportRepository _companyBulkImportExportRepository;

        public CompanyBulkImportExportService(ICompanyBulkImportExportRepository companyBulkImportExportRepository)
        {
            _companyBulkImportExportRepository = companyBulkImportExportRepository;
        }
        public List<ImportCommonResponseModel> Create(CompanyBulkImportViewModel viewModel, AviraUser currentUser)
        {
            viewModel.JsonCompanyBasic = JsonConvert.SerializeObject(viewModel.CompanyBasicInfoList);
            viewModel.JsonCompanyFinancial = JsonConvert.SerializeObject(viewModel.CompanyFinancialList);
            var cpFundamentalsImportResponse = _companyBulkImportExportRepository.Create(viewModel);
            return cpFundamentalsImportResponse.ToList();
        }

    }
}
