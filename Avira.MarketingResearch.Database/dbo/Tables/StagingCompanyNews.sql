﻿CREATE TABLE [dbo].[StagingCompanyNews] (
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [ImportId]          UNIQUEIDENTIFIER NULL,
    [NewsDate]          DATETIME         NULL,
    [News]              NVARCHAR (1000)  NULL,
    [NewsCategory]      NVARCHAR (255)   NULL,
    [Importance]        NVARCHAR (255)   NULL,
    [Impact]            NVARCHAR (255)   NULL,
    [OtherParticipants] NVARCHAR (255)   NULL,
    [Remarks]           NVARCHAR (512)   NULL,
    [CategoryId]        UNIQUEIDENTIFIER NULL,
    [ImportanceId]      UNIQUEIDENTIFIER NULL,
    [ImpactId]          UNIQUEIDENTIFIER NULL,
    [CompanyId]         UNIQUEIDENTIFIER NULL,
    [ErrorNotes]        NVARCHAR (1000)  NULL,
    [RowNo]             INT              NULL,
    CONSTRAINT [PK_StagingCompanyNews] PRIMARY KEY CLUSTERED ([Id] ASC)
);



