﻿CREATE PROCEDURE [dbo].[SFabChart_GetAllEcoSystemsByProjectId]
(
	--@ProjectIdList [dbo].[udtUniqueIdentifier] READONLY
	  @ProjectId UniqueIdentifier = null,
	  @AvirauserId UniqueIdentifier = null
)
as
/*================================================================================
Procedure Name: [SFabChart_GetAllEcoSystemsByProjectId]
Author: Sai Krishna
Create date: 15/05/2019
Description: Get list from EcoSystem by market Id
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
15/05/2019	Sai Krishna		Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
Declare @IndustryIdList [dbo].[udtUniqueIdentifier],
		@MarketIdList [dbo].[udtUniqueIdentifier],
		@UserRoleName nvarchar(50) = null;
declare @ProjectApprovedStatusId uniqueidentifier
select @ProjectApprovedStatusId = ID from ProjectStatus
where ProjectStatusName = 'Completed'
declare @TrendApprovedStatusId uniqueidentifier
select @TrendApprovedStatusId = Id
	from TrendStatus
	where TrendStatusName = 'Approved';

Set @UserRoleName = (Select UserRoleName from UserRole 
where Id = @AvirauserId)

Insert Into @IndustryIdList
Select SegmentId
From UserSegmentMap
where 
--CustomerUserId = @AvirauserId
--AND 
(SegmentType = 'BoardIndustries' 
     OR SegmentType = 'Industries')

Insert Into @MarketIdList
Select SegmentId
From UserSegmentMap
where SegmentType = 'Market'
--AND CustomerUserId = @AvirauserId 

IF(@ProjectId = null)
Begin
SET @ProjectId = 
(Select Top(1) SegmentId
From UserSegmentMap
where SegmentType = 'Project'
and exists (select 1 from Project where Project.ProjectStatusID = @ProjectApprovedStatusId
and project.ID = UserSegmentMap.SegmentId)
--AND CustomerUserId = @AvirauserId
)
End

--IF(@UserRoleName = 'Admin')
--Begin
--;WITH cte AS (
--Select tim.TrendId,tim.IndustryId
--from TrendIndustryMap tim
--Where tim.IsDeleted = 0
--)
SELECT Distinct TOP 10 tes.EcoSystemId,
	es.SegmentName as EcoSystemName 
FROM TrendEcoSystemMap tes
--INNER JOIN cte
--ON tes.TrendId = cte.TrendId
Inner Join Segment es
On es.Id = tes.EcoSystemId
WHERE tes.IsDeleted = 0
and exists (select 1 from Trend
			inner join Project
			on Trend.ProjectID = Project.Id
			where Project.ID = @ProjectId
			and project.ProjectStatusID = @ProjectApprovedStatusId
			and trend.TrendStatusID = @TrendApprovedStatusId
			)
--;WITH cte AS (
--Select tmm.TrendId,tmm.MarketId
--from TrendMarketMap tmm
--Where tmm.IsDeleted = 0
--)
--SELECT Distinct tes.EcoSystemId,
--	es.EcoSystemName 
--FROM TrendEcoSystemMap tes
--INNER JOIN cte
--ON tes.TrendId = cte.TrendId
--Inner Join EcoSystem es
--On es.Id = tes.EcoSystemId
--WHERE tes.IsDeleted = 0

--;WITH cte AS (
--SELECT tmm.TrendId
--FROM TrendProjectMap tmm
--WHERE tmm.IsDeleted = 0 
----and tmm.ProjectId = @ProjectId
--)
--SELECT Distinct tes.EcoSystemId,
--	es.EcoSystemName 
--FROM TrendEcoSystemMap tes
--INNER JOIN cte
--ON tes.TrendId = cte.TrendId
--Inner Join EcoSystem es
--On es.Id = tes.EcoSystemId
--WHERE tes.IsDeleted = 0
--End

--Else
--Begin
--;WITH cte AS (
--Select tim.TrendId,tim.IndustryId
--from TrendIndustryMap tim
--Inner Join @IndustryIdList indlist
--On tim.IndustryId = indlist.ID
--Where tim.IsDeleted = 0
--)
--SELECT Distinct top 10  tes.EcoSystemId,
--	es.EcoSystemName 
--FROM TrendEcoSystemMap tes
--INNER JOIN cte
--ON tes.TrendId = cte.TrendId
--Inner Join EcoSystem es
--On es.Id = tes.EcoSystemId
--WHERE tes.IsDeleted = 0

--;WITH cte AS (
--Select tmm.TrendId,tmm.MarketId
--from TrendMarketMap tmm
--Inner Join @MarketIdList marlist
--On tmm.MarketId = marlist.ID
--Where tmm.IsDeleted = 0
--)
--SELECT Distinct top 10 tes.EcoSystemId,
--	es.EcoSystemName 
--FROM TrendEcoSystemMap tes
--INNER JOIN cte
--ON tes.TrendId = cte.TrendId
--Inner Join EcoSystem es
--On es.Id = tes.EcoSystemId
--WHERE tes.IsDeleted = 0

--;WITH cte AS (
--SELECT tmm.TrendId
--FROM TrendProjectMap tmm
--WHERE tmm.IsDeleted = 0 
----and tmm.ProjectId = @ProjectId
--)
--SELECT Distinct top 10 tes.EcoSystemId,
--	es.EcoSystemName 
--FROM TrendEcoSystemMap tes
--INNER JOIN cte
--ON tes.TrendId = cte.TrendId
--Inner Join EcoSystem es
--On es.Id = tes.EcoSystemId
--WHERE tes.IsDeleted = 0
--End
END