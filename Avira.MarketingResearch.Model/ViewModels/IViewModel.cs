﻿using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels
{
    public interface IViewModel
    {
        bool IsSuccess { get; set; }
        string Message { get; set; }
        Guid Id { get; set; }
        List<MarketSizingImportViewModel> Data { get; set; }
        List<TrendImportViewModel> TrendData { get; set; }
        List<ImportCommonResponseModel> cpData { get; set; }        
        List<ImportCommonResponseModel> CBIData { get; set; }
    }

    public interface IMultiViewData
    {
        string Label { get; set; }
        Guid? Value { get; set; }
    }
}
