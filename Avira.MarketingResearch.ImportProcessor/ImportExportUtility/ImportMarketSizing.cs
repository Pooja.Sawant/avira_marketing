﻿using Avira.MarketingResearch.Common.IService.ImportExport;
using Avira.MarketingResearch.Common.Service_Implementation.ImportExport;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Repository.Implementations.ImportExport;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;
using Avira.MarketingResearch.Utility;
using Newtonsoft.Json;

namespace Avira.MarketingResearch.ImportProcessor.ImportExportUtility
{
    public class ImportMarketSizing
    {
        private string _importFilePath = string.Empty;
        private string _importErrorFilePath = string.Empty;
        string _mSTemplateName = string.Empty;
        string _importStatusPending = string.Empty;
        string _importStatusSuccess = string.Empty;
        string _importStatusInprogress = string.Empty;
        string _importStatusFailed = string.Empty;
        string _adminUserId = string.Empty;
        string _loggerFilePath = string.Empty;
        string _ImportProcessedFilePath = string.Empty;
        int _BatchSize = 0;
        IConfiguration configuration = null;

        public ImportMarketSizing(IConfiguration iConfig)
        {
            configuration = iConfig;
            _importFilePath = configuration.GetSection("ImportFilePath").Value;
            _importErrorFilePath = configuration.GetSection("ImportErrorFilePath").Value;

            _mSTemplateName = configuration.GetSection("ImportName").GetSection("MSImportName").Value;
            _importStatusPending = configuration.GetSection("ImportStatus").GetSection("ImportStatusPending").Value;
            _importStatusSuccess = configuration.GetSection("ImportStatus").GetSection("ImportStatusSuccess").Value;
            _importStatusInprogress = configuration.GetSection("ImportStatus").GetSection("ImportStatusInProgress").Value;
            _importStatusFailed = configuration.GetSection("ImportStatus").GetSection("ImportStatusFailed").Value;
            _adminUserId = configuration.GetSection("AdminUserID").Value;
            _loggerFilePath = configuration.GetSection("LoggerFilePath").Value;
            _ImportProcessedFilePath = configuration.GetSection("ImportProcessedFilePath").Value;
            _BatchSize = Convert.ToInt32(configuration.GetSection("BatchSize").Value);
        }

        public void ProcessMSImport()
        {

            FileLogger.WriteToFile(_loggerFilePath, "Start ProcessMSImport At: " + DateTime.Now);
            IDbHelper _IDbHelper = new DbHelper(configuration);
            IImportRequestRepository iImportRequestRepository = new ImportRequestRepository(_IDbHelper);
            IImportRequestService iImportRequestService = new ImportRequestService(iImportRequestRepository);
            var pendingRequest = iImportRequestService.GetImportRequest(_mSTemplateName, _importStatusPending);

            if (pendingRequest != null && pendingRequest.Count() > 0)
            {
                foreach (var item in pendingRequest)
                {
                    var res = Import(item);
                    res.ImportFor = _mSTemplateName;

                    FileOperations.Move(item.ImportFileName, item.ImportFilePath, _ImportProcessedFilePath);

                    StringBuilder sbMessage = new StringBuilder();
                    sbMessage.Append("Excuted On");
                    sbMessage.Append(DateTime.Now.ToString());
                    sbMessage.Append("_ProcessMSImport()_");
                    sbMessage.Append(res.ImportFor);
                    sbMessage.Append("_TotalRecords:");
                    sbMessage.Append(res.TotalRecords);
                    sbMessage.Append("_");
                    sbMessage.Append(res.ResponseStatus);
                    sbMessage.Append("_");
                    sbMessage.Append(res.ResponseMessage);
                    sbMessage.Append(Environment.NewLine);

                    FileLogger.WriteToFile(_loggerFilePath, sbMessage.ToString());
                }               
            }
            else
            {
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.Append("Excuted On");
                sbMessage.Append(DateTime.Now.ToString());
                sbMessage.Append("_ProcessMSImport()_");
                sbMessage.Append(" No record(s) found to process");
                sbMessage.Append(Environment.NewLine);

                FileLogger.WriteToFile(_loggerFilePath, sbMessage.ToString());
            }

            FileLogger.WriteToFile(_loggerFilePath, "End ProcessMSImport At: " + DateTime.Now);
        }

        public ImportResponse Import(ImportRequestViewModel requestModel)
        {
            //var filePath = _importFilePath + "Market Size Import.xlsx"; 
            var res = new ImportResponse();
           
            var inputModel = new MarketSizingImportInsertViewModel();
            IDbHelper _IDbHelper = new DbHelper(configuration);
            IImportRequestRepository iImportRequestRepository = new ImportRequestRepository(_IDbHelper);
            IImportRequestService iImportRequestService = new ImportRequestService(iImportRequestRepository);
            try
            {

                inputModel.TemplateName = _mSTemplateName;

                //------------Call business method

                IMarketSizingImportExportRepository imarketSizingImportExportRepository = new MarketSizingImportExportRepository(_IDbHelper);
                IMarketSizingImportExportService imarketSizingImportExportService = new MarketSizingImportExportService(imarketSizingImportExportRepository);
                inputModel.UserCreatedById = Guid.Parse(_adminUserId);
                var resultStag = false;
                var rowCount = 0;

                //------------Change Status to InProgress--------------------------

                ImportRequestViewModel objImportRequestViewModel = new ImportRequestViewModel();
                objImportRequestViewModel.Id = requestModel.Id;
                objImportRequestViewModel.ImportException = "Market sizing importe is processing.";
                objImportRequestViewModel.StatusName = _importStatusInprogress;
                iImportRequestService.Update(objImportRequestViewModel);
                //--------------------------------------



                using (var stream = File.OpenRead(requestModel.ImportFilePath + "\\" + requestModel.ImportFileName))
                {
                    //formFile.CopyTo(stream);

                    using (var package = new ExcelPackage(stream))
                    {
                        if (package.Workbook.Worksheets["MS Import"] == null)
                        {
                            throw new Exception("MS Import worksheet is missing");
                        }
                        ExcelWorksheet worksheet = package.Workbook.Worksheets["MS Import"];
                        if (worksheet.Dimension == null)
                        {
                            res.ResponseMessage = "Please upload proper excel file";
                            res.TotalSuccessRecords = 0;
                            res.TotalFailedRecords = 0;
                            res.ResponseStatus = "Failed";
                            return res;
                        }
                        //ExcelToModel(list, worksheet);


                        rowCount = worksheet.Dimension.Rows;


                        var objSegmentMappingList = new List<MarketSizingImportSegmentModel>();
                        var objMarketSizingImportList = new List<MarketSizingImportViewModel>();
                        var splitSheetRows = DistributeInteger(rowCount, _BatchSize);
                        var rowFrom = 7;
                        var rowTo = 0;
                        foreach (var item in splitSheetRows.Where(x => x > 0))
                        {
                            rowTo = item;
                            if (rowFrom != 7)
                            {
                                rowTo = rowFrom + item;
                                rowFrom = rowFrom + 1;
                            }

                            for (int row = rowFrom; row <= rowTo; row++)
                            {
                                var objMarketSizingImport = ExcelCellToModel(worksheet, row);
                                objMarketSizingImportList.Add(objMarketSizingImport);
                            }
                            rowFrom = rowTo;

                            inputModel.TrendJson = JsonConvert.SerializeObject(objMarketSizingImportList);
                            inputModel.ImportId = requestModel.Id;
                            resultStag = imarketSizingImportExportService.CreateStagging(inputModel);
                            objMarketSizingImportList = new List<MarketSizingImportViewModel>();
                            if (!resultStag)
                            {
                                //--------Rollback of stagging table
                                imarketSizingImportExportService.DeleteMSStagging(inputModel);
                                break;
                            }
                        }
                    }
                }


                //}

                var result = new List<MarketSizingImportViewModel>();
                if (resultStag)
                {
                    result = imarketSizingImportExportService.Create(inputModel, new AviraUser());
                }

                res.TotalRecords = rowCount;

                var resError = result != null ? result.Where(s => s.ErrorNotes != null).ToList() : null;
                if (resError != null && resError.Count <= 0 || resError.First().ErrorNotes == null)
                {
                    res.ResponseMessage = "MarketSizing Details successfully Imported";
                    res.ResponseStatus = _importStatusSuccess;

                    //IImportRequestRepository iImportRequestRepository = new ImportRequestRepository(_IDbHelper);
                    //IImportRequestService iImportRequestService = new ImportRequestService(iImportRequestRepository);
                    objImportRequestViewModel = new ImportRequestViewModel();
                    objImportRequestViewModel.Id = requestModel.Id;
                    objImportRequestViewModel.ImportException = "Market sizing successfully Imported";
                    objImportRequestViewModel.StatusName = _importStatusSuccess;
                    iImportRequestService.Update(objImportRequestViewModel);
                }
                else
                {
                    var errorFilePath = string.Empty;
                    if (result != null && result.Count > 0)
                    {
                        errorFilePath = CreateMSExcel(result);
                    }

                    res.ResponseMessage = "Import marketSizing have some errors";
                    res.ResponseStatus = _importStatusFailed;

                    //IImportRequestRepository iImportRequestRepository = new ImportRequestRepository(_IDbHelper);
                    //IImportRequestService iImportRequestService = new ImportRequestService(iImportRequestRepository);
                    objImportRequestViewModel = new ImportRequestViewModel();
                    objImportRequestViewModel.Id = requestModel.Id;
                    objImportRequestViewModel.StatusName = _importStatusFailed;
                    objImportRequestViewModel.ImportException = "Import marketSizing have some errors";
                    objImportRequestViewModel.ImportExceptionFilePath = errorFilePath;
                    iImportRequestService.Update(objImportRequestViewModel);

                }



            }
            catch (Exception ex)
            {
                //_logger.LogError(ex.Message);
                res.ResponseMessage = "Import() " + ex.Message;
                res.ResponseStatus = _importStatusFailed;

                var objImportRequestViewModel = new ImportRequestViewModel();
                objImportRequestViewModel.Id = requestModel.Id;
                objImportRequestViewModel.StatusName = _importStatusFailed;
                if (ex.Message == "MS Import worksheet is missing")
                {
                    objImportRequestViewModel.ImportException = "Please select correct template for the file you want to import/Error while reading template. 'MS Import' worksheet is missing.";
                }
                else
                {
                    objImportRequestViewModel.ImportException = "Import marketSizing have some errors";
                }
                objImportRequestViewModel.ImportExceptionFilePath = string.Empty;
                iImportRequestService.Update(objImportRequestViewModel);

            }
                return res;
        }

        private MarketSizingImportViewModel ExcelCellToModel( ExcelWorksheet worksheet, int row)
        {
            var msdata = new MarketSizingImportViewModel();
            MarketSizingImportSegmentModel objSegmentMapping = null;
            List<MarketSizingImportSegmentModel> objSegmentMappingList = new List<MarketSizingImportSegmentModel>();
            for (var col = 9; col <= 34; col++)
            {               
                objSegmentMapping = new MarketSizingImportSegmentModel
                {
                    SegmentName = worksheet.Cells[6, col].Text?.ToString().Trim(),//---Index 6 for header row
                    SubSegment = worksheet.Cells[row, col].Value?.ToString().Trim()
                };
                objSegmentMappingList.Add(objSegmentMapping);
            }


            msdata = new MarketSizingImportViewModel
            {
                RowNo = row,
                SegmentMapJson = JsonConvert.SerializeObject(objSegmentMappingList),
                Project = worksheet.Cells[row, 1].Value?.ToString().Trim(),
                Market = worksheet.Cells[row, 2].Value?.ToString().Trim(),
                Region = worksheet.Cells[row, 3].Value?.ToString().Trim(),
                Country = worksheet.Cells[row, 4].Value?.ToString().Trim(),
                Currency = worksheet.Cells[row, 5].Value?.ToString().Trim(),
                Value = worksheet.Cells[row, 6].Value?.ToString().Trim(),
                ASPUnit = worksheet.Cells[row, 7].Value?.ToString().Trim(),
                VolumeUnit = worksheet.Cells[row, 8].Value?.ToString().Trim(),

                ASP_2018 = worksheet.Cells[row, 35].Value?.ToString().Trim(),
                Value_2018 = worksheet.Cells[row, 36].Value?.ToString().Trim(),
                Volume_2018 = worksheet.Cells[row, 37].Value?.ToString().Trim(),
                OranicGrowth_2018 = worksheet.Cells[row, 38].Value?.ToString().Trim(),
                TrendsGrowth_2018 = worksheet.Cells[row, 39].Value?.ToString().Trim(),
                Source1_2018 = worksheet.Cells[row, 40].Value?.ToString().Trim(),
                Source2_2018 = worksheet.Cells[row, 41].Value?.ToString().Trim(),
                Source3_2018 = worksheet.Cells[row, 42].Value?.ToString().Trim(),
                Source4_2018 = worksheet.Cells[row, 43].Value?.ToString().Trim(),
                Source5_2018 = worksheet.Cells[row, 44].Value?.ToString().Trim(),
                Rationale_2018 = worksheet.Cells[row, 45].Value?.ToString().Trim(),

                ASP_2019 = worksheet.Cells[row, 46].Value?.ToString().Trim(),
                Value_2019 = worksheet.Cells[row, 47].Value?.ToString().Trim(),
                Volume_2019 = worksheet.Cells[row, 48].Value?.ToString().Trim(),
                OranicGrowth_2019 = worksheet.Cells[row, 49].Value?.ToString().Trim(),
                TrendsGrowth_2019 = worksheet.Cells[row, 50].Value?.ToString().Trim(),
                Source1_2019 = worksheet.Cells[row, 51].Value?.ToString().Trim(),
                Source2_2019 = worksheet.Cells[row, 52].Value?.ToString().Trim(),
                Source3_2019 = worksheet.Cells[row, 53].Value?.ToString().Trim(),
                Source4_2019 = worksheet.Cells[row, 54].Value?.ToString().Trim(),
                Source5_2019 = worksheet.Cells[row, 55].Value?.ToString().Trim(),
                Rationale_2019 = worksheet.Cells[row, 56].Value?.ToString().Trim(),

                ASP_2020 = worksheet.Cells[row, 57].Value?.ToString().Trim(),
                Value_2020 = worksheet.Cells[row, 58].Value?.ToString().Trim(),
                Volume_2020 = worksheet.Cells[row, 59].Value?.ToString().Trim(),
                OranicGrowth_2020 = worksheet.Cells[row, 60].Value?.ToString().Trim(),
                TrendsGrowth_2020 = worksheet.Cells[row, 61].Value?.ToString().Trim(),
                Source1_2020 = worksheet.Cells[row, 62].Value?.ToString().Trim(),
                Source2_2020 = worksheet.Cells[row, 63].Value?.ToString().Trim(),
                Source3_2020 = worksheet.Cells[row, 64].Value?.ToString().Trim(),
                Source4_2020 = worksheet.Cells[row, 65].Value?.ToString().Trim(),
                Source5_2020 = worksheet.Cells[row, 66].Value?.ToString().Trim(),
                Rationale_2020 = worksheet.Cells[row, 67].Value?.ToString().Trim(),

                ASP_2021 = worksheet.Cells[row, 68].Value?.ToString().Trim(),
                Value_2021 = worksheet.Cells[row, 69].Value?.ToString().Trim(),
                Volume_2021 = worksheet.Cells[row, 70].Value?.ToString().Trim(),
                OranicGrowth_2021 = worksheet.Cells[row, 71].Value?.ToString().Trim(),
                TrendsGrowth_2021 = worksheet.Cells[row, 72].Value?.ToString().Trim(),
                Source1_2021 = worksheet.Cells[row, 73].Value?.ToString().Trim(),
                Source2_2021 = worksheet.Cells[row, 74].Value?.ToString().Trim(),
                Source3_2021 = worksheet.Cells[row, 75].Value?.ToString().Trim(),
                Source4_2021 = worksheet.Cells[row, 76].Value?.ToString().Trim(),
                Source5_2021 = worksheet.Cells[row, 77].Value?.ToString().Trim(),
                Rationale_2021 = worksheet.Cells[row, 78].Value?.ToString().Trim(),

                ASP_2022 = worksheet.Cells[row, 79].Value?.ToString().Trim(),
                Value_2022 = worksheet.Cells[row, 80].Value?.ToString().Trim(),
                Volume_2022 = worksheet.Cells[row, 81].Value?.ToString().Trim(),
                OranicGrowth_2022 = worksheet.Cells[row, 82].Value?.ToString().Trim(),
                TrendsGrowth_2022 = worksheet.Cells[row, 83].Value?.ToString().Trim(),
                Source1_2022 = worksheet.Cells[row, 84].Value?.ToString().Trim(),
                Source2_2022 = worksheet.Cells[row, 85].Value?.ToString().Trim(),
                Source3_2022 = worksheet.Cells[row, 86].Value?.ToString().Trim(),
                Source4_2022 = worksheet.Cells[row, 87].Value?.ToString().Trim(),
                Source5_2022 = worksheet.Cells[row, 88].Value?.ToString().Trim(),
                Rationale_2022 = worksheet.Cells[row, 89].Value?.ToString().Trim(),

                ASP_2023 = worksheet.Cells[row, 90].Value?.ToString().Trim(),
                Value_2023 = worksheet.Cells[row, 91].Value?.ToString().Trim(),
                Volume_2023 = worksheet.Cells[row, 92].Value?.ToString().Trim(),
                OranicGrowth_2023 = worksheet.Cells[row, 93].Value?.ToString().Trim(),
                TrendsGrowth_2023 = worksheet.Cells[row, 94].Value?.ToString().Trim(),
                Source1_2023 = worksheet.Cells[row, 95].Value?.ToString().Trim(),
                Source2_2023 = worksheet.Cells[row, 96].Value?.ToString().Trim(),
                Source3_2023 = worksheet.Cells[row, 97].Value?.ToString().Trim(),
                Source4_2023 = worksheet.Cells[row, 98].Value?.ToString().Trim(),
                Source5_2023 = worksheet.Cells[row, 99].Value?.ToString().Trim(),
                Rationale_2023 = worksheet.Cells[row, 100].Value?.ToString().Trim(),

                ASP_2024 = worksheet.Cells[row, 101].Value?.ToString().Trim(),
                Value_2024 = worksheet.Cells[row, 102].Value?.ToString().Trim(),
                Volume_2024 = worksheet.Cells[row, 103].Value?.ToString().Trim(),
                OranicGrowth_2024 = worksheet.Cells[row, 104].Value?.ToString().Trim(),
                TrendsGrowth_2024 = worksheet.Cells[row, 105].Value?.ToString().Trim(),
                Source1_2024 = worksheet.Cells[row, 106].Value?.ToString().Trim(),
                Source2_2024 = worksheet.Cells[row, 107].Value?.ToString().Trim(),
                Source3_2024 = worksheet.Cells[row, 108].Value?.ToString().Trim(),
                Source4_2024 = worksheet.Cells[row, 109].Value?.ToString().Trim(),
                Source5_2024 = worksheet.Cells[row, 110].Value?.ToString().Trim(),
                Rationale_2024 = worksheet.Cells[row, 111].Value?.ToString().Trim(),

                ASP_2025 = worksheet.Cells[row, 112].Value?.ToString().Trim(),
                Value_2025 = worksheet.Cells[row, 113].Value?.ToString().Trim(),
                Volume_2025 = worksheet.Cells[row, 114].Value?.ToString().Trim(),
                OranicGrowth_2025 = worksheet.Cells[row, 115].Value?.ToString().Trim(),
                TrendsGrowth_2025 = worksheet.Cells[row, 116].Value?.ToString().Trim(),
                Source1_2025 = worksheet.Cells[row, 117].Value?.ToString().Trim(),
                Source2_2025 = worksheet.Cells[row, 118].Value?.ToString().Trim(),
                Source3_2025 = worksheet.Cells[row, 119].Value?.ToString().Trim(),
                Source4_2025 = worksheet.Cells[row, 120].Value?.ToString().Trim(),
                Source5_2025 = worksheet.Cells[row, 121].Value?.ToString().Trim(),
                Rationale_2025 = worksheet.Cells[row, 122].Value?.ToString().Trim(),

                ASP_2026 = worksheet.Cells[row, 123].Value?.ToString().Trim(),
                Value_2026 = worksheet.Cells[row, 124].Value?.ToString().Trim(),
                Volume_2026 = worksheet.Cells[row, 125].Value?.ToString().Trim(),
                OranicGrowth_2026 = worksheet.Cells[row, 126].Value?.ToString().Trim(),
                TrendsGrowth_2026 = worksheet.Cells[row, 127].Value?.ToString().Trim(),
                Source1_2026 = worksheet.Cells[row, 128].Value?.ToString().Trim(),
                Source2_2026 = worksheet.Cells[row, 129].Value?.ToString().Trim(),
                Source3_2026 = worksheet.Cells[row, 130].Value?.ToString().Trim(),
                Source4_2026 = worksheet.Cells[row, 131].Value?.ToString().Trim(),
                Source5_2026 = worksheet.Cells[row, 132].Value?.ToString().Trim(),
                Rationale_2026 = worksheet.Cells[row, 133].Value?.ToString().Trim(),

                ASP_2027 = worksheet.Cells[row, 134].Value?.ToString().Trim(),
                Value_2027 = worksheet.Cells[row, 135].Value?.ToString().Trim(),
                Volume_2027 = worksheet.Cells[row, 136].Value?.ToString().Trim(),
                OranicGrowth_2027 = worksheet.Cells[row, 137].Value?.ToString().Trim(),
                TrendsGrowth_2027 = worksheet.Cells[row, 138].Value?.ToString().Trim(),
                Source1_2027 = worksheet.Cells[row, 139].Value?.ToString().Trim(),
                Source2_2027 = worksheet.Cells[row, 140].Value?.ToString().Trim(),
                Source3_2027 = worksheet.Cells[row, 141].Value?.ToString().Trim(),
                Source4_2027 = worksheet.Cells[row, 142].Value?.ToString().Trim(),
                Source5_2027 = worksheet.Cells[row, 143].Value?.ToString().Trim(),
                Rationale_2027 = worksheet.Cells[row, 144].Value?.ToString().Trim(),

                ASP_2028 = worksheet.Cells[row, 145].Value?.ToString().Trim(),
                Value_2028 = worksheet.Cells[row, 146].Value?.ToString().Trim(),
                Volume_2028 = worksheet.Cells[row, 147].Value?.ToString().Trim(),
                OranicGrowth_2028 = worksheet.Cells[row, 148].Value?.ToString().Trim(),
                TrendsGrowth_2028 = worksheet.Cells[row, 149].Value?.ToString().Trim(),
                Source1_2028 = worksheet.Cells[row, 150].Value?.ToString().Trim(),
                Source2_2028 = worksheet.Cells[row, 151].Value?.ToString().Trim(),
                Source3_2028 = worksheet.Cells[row, 152].Value?.ToString().Trim(),
                Source4_2028 = worksheet.Cells[row, 153].Value?.ToString().Trim(),
                Source5_2028 = worksheet.Cells[row, 154].Value?.ToString().Trim(),
                Rationale_2028 = worksheet.Cells[row, 155].Value?.ToString().Trim(),

                CAGR_3Years = worksheet.Cells[row, 156].Value?.ToString().Trim(),
                CAGR_5Years = worksheet.Cells[row, 157].Value?.ToString().Trim(),
                Total_CAGR = worksheet.Cells[row, 158].Value?.ToString().Trim()
            };           
            return msdata;
        }

        public string CreateMSExcel(List<MarketSizingImportViewModel> model)
        {

            string sFileName = @"MarketsizingImportResponse" + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss") + ".xlsx";
            //string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sFileName);
            var fullPath = Path.Combine(_importErrorFilePath, sFileName);
            FileInfo file = new FileInfo(fullPath);
            if (file.Exists)
            {
                file.Delete();
                file = new FileInfo(Path.Combine(_importErrorFilePath, sFileName));
            }
            using (ExcelPackage package = new ExcelPackage(file))
            {
                // add a new worksheet to the empty workbook
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("MarketSizeErrorSheet");
                //First add the headers
                worksheet.Cells["A1"].Value = "Market";
                worksheet.Cells["B1"].Value = "RowNo";
                worksheet.Cells["C1"].Value = "ErrorNotes";

                //Add values
                int row = 2;
                for (int i = 0; i < model.Count; i++)
                {
                    //worksheet.Range["A" + row].Text = model[i].Project;
                    worksheet.Cells["A" + row].Value = model[i].Market;
                    worksheet.Cells["B" + row].Value = model[i].RowNo.ToString();
                    worksheet.Cells["C" + row].Value = model[i].ErrorNotes;
                    row++;
                }

                //worksheet.Cells["A2"].Value = 1000;
                //worksheet.Cells["B2"].Value = "Jon";
                //worksheet.Cells["C2"].Value = "M";
                //worksheet.Cells["D2"].Value = 5000;

                //worksheet.Cells["A3"].Value = 1001;
                //worksheet.Cells["B3"].Value = "Graham";
                //worksheet.Cells["C3"].Value = "M";
                //worksheet.Cells["D3"].Value = 10000;

                //worksheet.Cells["A4"].Value = 1002;
                //worksheet.Cells["B4"].Value = "Jenny";
                //worksheet.Cells["C4"].Value = "F";
                //worksheet.Cells["D4"].Value = 5000;

                package.Save(); //Save the workbook.
            }
            return fullPath;
        }

        private static void ExcelToModel(List<MarketSizingImportViewModel> list, ExcelWorksheet worksheet)
        {
           
            //for (int row = rowFrom; row <= rowCount; row++)
            //{
                
            //    if (row >= 25000)
            //    {

            //    }


            //    //----Segment start from 9 to 34 columns-------
                
            //}
        }

        public static IEnumerable<int> DistributeNthInteger(int total, int divider)
        {
            if (divider == 0)
            {
                yield return 0;
            }
            else
            {
                int rest = total % divider;
                double result = total / (double)divider;

                for (int i = 0; i < divider; i++)
                {
                    if (rest-- > 0)
                        yield return (int)Math.Ceiling(result);
                    else
                        yield return (int)Math.Floor(result);
                }
            }
        }

        public static IEnumerable<int> DistributeInteger(int total, int divider)
        {
            var lst = new List<int>();
            if (divider == 0 || total == 0)
            {
                lst.Add(0);
            }
            else
            {

                if (total <= divider)
                {
                    lst.Add(total);

                }
                else
                {
                    int rest = total % divider;
                    int result = (int)(total / divider);

                    for (int i = 0; i < result; i++)
                    {
                        //if (rest-- > 0)
                        lst.Add((int)(divider));
                        // yield return (int)(result * divider);
                        //else
                        //yield return (int)Math.Floor(result);
                    }

                    if (rest > 0)
                        lst.Add(rest);
                }




            }
            return lst;
        }

        private static int GetColIndex(ExcelWorksheet worksheet, string colName)
        {
            int colIndex = worksheet.Cells["6:6"].First(c => c.Value.ToString() == colName).Start.Column;

            return colIndex;
        }
        private void UpdateImportStatus()
        {
            IDbHelper _IDbHelper = new DbHelper(configuration);
            IMarketSizingImportExportRepository imarketSizingImportExportRepository = new MarketSizingImportExportRepository(_IDbHelper);
            IMarketSizingImportExportService imarketSizingImportExportService = new MarketSizingImportExportService(imarketSizingImportExportRepository);

        }

        //private string CreateMSExcel(List<MarketSizingImportViewModel> model)
        //{
        //    string handle = Guid.NewGuid().ToString();
        //    var inputAsString = string.Empty;
        //    using (ExcelEngine excelEngine = new ExcelEngine())
        //    {
        //        IApplication application = excelEngine.Excel;

        //        application.DefaultVersion = ExcelVersion.Excel2016;

        //        //Create a workbook
        //        IWorkbook workbook = application.Workbooks.Create(1);
        //        IWorksheet worksheet = workbook.Worksheets[0];

        //        worksheet.Range["B1"].Text = "Market";
        //        worksheet.Range["C1"].Text = "ErrorNotes";


        //        int row = 2;
        //        for (int i = 0; i < model.Count; i++)
        //        {
        //            //worksheet.Range["A" + row].Text = model[i].Project;
        //            worksheet.Range["B" + row].Text = model[i].Market;
        //            worksheet.Range["C" + row].Text = model[i].ErrorNotes;
        //            row++;
        //        }
        //        //------

        //        //Saving the Excel to the MemoryStream 
        //        //MemoryStream stream = new MemoryStream();
        //        //workbook.SaveAs(stream);

        //        //Set the position as '0'.
        //        //stream.Position = 0;

        //        //Download the Excel file in the browser
        //        //FileStreamResult fileStreamResult = new FileStreamResult(stream, "application/excel");
        //        //fileStreamResult.FileDownloadName = "MarketSizeErrorList.xlsx";
        //        //return fileStreamResult;


        //        using (MemoryStream memoryStream = new MemoryStream())
        //        {
        //            workbook.SaveAs(memoryStream);
        //            memoryStream.Position = 0;
        //            inputAsString = Convert.ToBase64String(memoryStream.ToArray());
        //        }


        //    }

        //    return inputAsString;
        //}
    }
}
