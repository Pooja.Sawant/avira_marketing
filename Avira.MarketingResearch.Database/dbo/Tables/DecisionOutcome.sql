﻿CREATE TABLE [dbo].[DecisionOutcome] (
    [Id]               UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [DecisionId]       UNIQUEIDENTIFIER NOT NULL,
    [TargetedOutcome]  NVARCHAR (1024)  NOT NULL,
    [CreatedOn]        DATETIME2 (7)    DEFAULT (getdate()) NOT NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [SeqNumber]        SMALLINT         NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK__DecisionO__Decis__796A7EE6] FOREIGN KEY ([DecisionId]) REFERENCES [dbo].[Decision] ([Id])
);

