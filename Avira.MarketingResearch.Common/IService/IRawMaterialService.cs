﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;

namespace Avira.MarketingResearch.Common.IService
{
    public interface IRawMaterialService
    {
        IEnumerable<RawMaterialViewModel> GetAll(Guid Id);
        IEnumerable<RawMaterialViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false);
        RawMaterialViewModel GetById(Guid guid);


        Result<SpTransactionMessage> Create(RawMaterialInsertDbViewModel tfc, AviraUser currentUser);
        Result<SpTransactionMessage> Update(RawMaterialUpdateDbViewModel tfc, AviraUser currenUser);

    }
}
