﻿using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.Models;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using static Avira.MarketingResearch.Model.ViewModels.ComponentViewModel;

namespace Avira.MarketingResearch.Web.Controllers
{
    [NoCache]
    [Authorization]
    public class ComponentController : BaseController
    {
        public readonly IServiceRepository _serviceRepository;
        private ILoggerManager _logger;
        public ComponentController(IServiceRepository serviceRepository, ILoggerManager logger)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
        }
        public IActionResult Index()
        {
            return View();
        }

        public ActionResult LoadData(ComponentViewModel component)
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            int totalRecords = 0;
            List<ComponentViewModel> data = null;
            try
            {
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                string search = Request.Form["search[value]"];

                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDir = Request.Form["order[0][dir]"].FirstOrDefault();
                var SortDirection = -1;
                if (sortColumnDir == "asc")
                {
                    SortDirection = 1;
                }
                else
                {
                    SortDirection = -1;
                }

                int skip = start != null ? Convert.ToInt32(start) : 0;
                int pageSize = length.ToString() != null ? Convert.ToInt32(length) : 0;
                int PageStart = start.ToString() != null ? Convert.ToInt32(start) : 0;
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();

                HttpResponseMessage response = _serviceRepository.GetResponse(contollerName, PageStart, pageSize, SortDirection, sortColumn, search);
                response.EnsureSuccessStatusCode();
                List<ComponentViewModel> components = response.Content.ReadAsAsync<List<ComponentViewModel>>().Result;
                ViewBag.Title = "All Components";
                data = components;
               
                foreach (var item in components)
                {
                    totalRecords = item.TotalItems;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data });

        }

        [HttpGet]
        public IActionResult Edit(string id)
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.GetByIdResponse(contollerName, id);
                response.EnsureSuccessStatusCode();
                ComponentUpdateDbViewModel component = response.Content.ReadAsAsync<ComponentUpdateDbViewModel>().Result;
                return View(component);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                ErrorViewModel err = new ErrorViewModel { RequestId = "1234" };
                //Log the exception 
                return View("Error", err);
            }
        }

        [HttpGet]
        public IActionResult Delete(string id)
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.GetByIdResponse(contollerName, id);
                response.EnsureSuccessStatusCode();
                ComponentUpdateDbViewModel component = response.Content.ReadAsAsync<ComponentUpdateDbViewModel>().Result;
                return View(component);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                ErrorViewModel err = new ErrorViewModel { RequestId = "1234" };
                //Log the exception 
                return View("Error", err);
            }
        }

        [HttpPost]
        public IActionResult Edit(ComponentUpdateDbViewModel component)
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.PutResponse(contollerName, component);
                response.EnsureSuccessStatusCode();
                ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                if (viewModel != null)
                {
                    ViewBag.Message = viewModel.Message;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(component);
        }

        public IActionResult Create()
        {

            return View();
        }

        [HttpPost]
        public IActionResult Create(ComponentInsertDbViewModel component)
        {

            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, component);
                response.EnsureSuccessStatusCode();
                ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                if (viewModel != null)
                {
                    ViewBag.Message = viewModel.Message;
                }
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(component);
        }

        [HttpPost]
        public IActionResult Delete(ComponentUpdateDbViewModel component)
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.DeleteResponse(contollerName, component);
                response.EnsureSuccessStatusCode();
                ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                if (viewModel != null)
                {
                    ViewBag.Message = viewModel.Message;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return RedirectToAction("Index");
        }
    }
}