﻿CREATE TABLE [dbo].[BaseYearMaster] (
    [Id]       INT IDENTITY (1, 1) NOT NULL,
    [BaseYear] INT NULL,
    CONSTRAINT [PK_BaseYearMaster] PRIMARY KEY CLUSTERED ([Id] ASC)
);

