﻿using Avira.MarketingResearch.Model;
using Avira.MarketingResearch.Repository.Helpers;
using System.Collections.Generic;
using System;
using System.Data;
using Avira.MarketingResearch.Model.ViewModels;
using static Avira.MarketingResearch.Model.ViewModels.ApplicationViewModel;

namespace Avira.MarketingResearch.Repository.CoreInterfaces
{
    public interface IApplicationRepository
    {
        ApplicationViewModel GetById(Guid guid);
        IEnumerable<ApplicationViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted);
        Result<SpTransactionMessage> Create(ApplicationInsertDbViewModel viewModel);
        Result<SpTransactionMessage> Update(ApplicationUpdateDbViewModel viewModel);
    }
}
