﻿using Avira.MarketingResearch.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService
{
    public interface IImpactTypeService
    {
        IEnumerable<ImpactTypeViewModel> GetAll();
    }
}
