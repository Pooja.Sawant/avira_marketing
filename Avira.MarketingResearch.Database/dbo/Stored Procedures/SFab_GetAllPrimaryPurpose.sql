﻿  
create PROCEDURE [dbo].[SFab_GetAllPrimaryPurpose]
(@AviraUserID uniqueidentifier = null)    
 AS  
/*================================================================================  
Procedure Name: [SFab_GetAllPrimaryPurpose]  
Author: Pooja Sawant  
Create date: 09/10/2019  
Description: Get list of PrimaryPurposeName from table  
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
09/10/2019  Pooja   Initial Version  
================================================================================*/  
  
BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
	
	select * from PrimaryPurpose WHERE IsDeleted = 0 
	order by PrimaryPurposeName asc

END