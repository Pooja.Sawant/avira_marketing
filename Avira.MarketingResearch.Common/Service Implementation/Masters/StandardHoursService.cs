﻿using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Common.IService.Masters;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.CoreInterfaces.Masters;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Repository.Implementations.Masters;
using System;
using System.Collections.Generic;
namespace Avira.MarketingResearch.Common.Service_Implementation.Masters
{
    public class StandardHoursService:IStandardHoursService
    {
        private readonly IStandardHoursRepository _standardHoursRepository;

        public StandardHoursService(IStandardHoursRepository statndardHoursRepository)
        {
            _standardHoursRepository = statndardHoursRepository;
        }

        public IEnumerable<StandardHoursViewModel> GetAll(Guid Id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<StandardHoursViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false)
        {
            return _standardHoursRepository.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude);
        }

        public StandardHoursViewModel GetById(Guid guid)
        {
            return _standardHoursRepository.GetById(guid);
        }


        public Result<SpTransactionMessage> Update(StandardHoursUpdateDbViewModel tfc, AviraUser currenUser)
        {
            return _standardHoursRepository.Update(tfc);
        }
    }
}
