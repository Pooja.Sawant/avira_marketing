﻿using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport
{
    public interface IQualitativeAnalysisImportExportRepository
    {
        IEnumerable<ImportCommonResponseModel> Create(QualitativeAnalysisImportModel viewModel);
    }
}
