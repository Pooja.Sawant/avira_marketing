﻿/*================================================================================  
Procedure Name: [fun_CurrencyConversion]  
Author: Swami Aluri  
Create date: 06/15/2019  
Description: Function for currency conversion
Preference by Conversion type 1. Date Range , 2. Yearly Average 3. Quarterly Average
Change History  
Date        Developer     Reason  
__________  ____________  ______________________________________________________  
06/04/2019   Swami Aluri   Initial Version  
================================================================================*/  

CREATE FUNCTION [dbo].[fun_CurrencyConversion]  
(  
 -- Add the parameters for the function here  
 @Value DECIMAL(18,4),
 @InCurrencyId UNIQUEIDENTIFIER, 
 @OutCurrencyId UNIQUEIDENTIFIER, 
 @TransactionDate Datetime = NULL
)  
RETURNS DECIMAL(18,4)  
AS  
BEGIN  
DECLARE @USDValue DECIMAL(18,4)
DECLARE @ReturnValue DECIMAL(18,4)  
DECLARE @USDCurrencyId UNIQUEIDENTIFIER;
declare @ConversionRate DECIMAL(18,4)  
declare @Year int;
declare @Quarter varchar(2);
If @TransactionDate is null set @TransactionDate = getdate();
set @Year = datepart(YYYY, @TransactionDate);
set @Quarter = datepart(QQ, @TransactionDate);

IF @InCurrencyId = @OutCurrencyId
begin
	SET @ReturnValue = @Value;
end
ELSE
begin
	select @USDCurrencyId = ID 
	from Currency
	where CurrencyName = 'United States dollar';
--Step1 : Convert input value to USD
	If @InCurrencyId = @USDCurrencyId
	begin
		set @USDValue = @Value;
	end
	else
	begin
		select top 1 @ConversionRate = ConversionRate
		from [CurrencyConversion]
		where CurrencyId = @InCurrencyId 
				 AND ((ConversionRangeType = 1 AND @TransactionDate  between StartDate and EndDate ) --Time Range
					or (ConversionRangeType = 2 AND [Year] = @Year) --Yearly
					or (ConversionRangeType = 3 AND [Year] = @Year and [Quarter] = @Quarter)) --Quarterly
		order by ConversionRangeType;
		if isnull(@ConversionRate, 0) = 0
		set @ConversionRate = 1; 
		set @USDValue = @Value/@ConversionRate;
	end
	--Step2: Convert USd value to output value
	If @OutCurrencyId = @USDCurrencyId
	begin
		set @ReturnValue = @USDValue;
	end
	else
	begin
		select top 1 @ConversionRate = ConversionRate
		from [CurrencyConversion]
		where CurrencyId = @OutCurrencyId 
				 AND ((ConversionRangeType = 1 AND @TransactionDate  between StartDate and EndDate ) --Time Range
					or (ConversionRangeType = 2 AND [Year] = @Year) --Yearly
					or (ConversionRangeType = 3 AND [Year] = @Year and [Quarter] = @Quarter)) --Quarterly
		order by ConversionRangeType;
		if isnull(@ConversionRate, 0) = 0
		set @ConversionRate = 1; 
		set @ReturnValue = @USDValue*@ConversionRate;
	end
 end
 -- Return the result of the function  
 RETURN @ReturnValue  
  
END