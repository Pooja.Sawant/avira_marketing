﻿

CREATE procedure [dbo].[SFab_InsertUpdateMakeMyMarketImpact]      
(
	@CustomerId uniqueidentifier,
	@MakeMyMarketId uniqueidentifier,
	@ProjectId uniqueidentifier,
	@MarketImpactId uniqueidentifier,
	@YearValuesList dbo.udtIntDecimal readonly,
	@CAGR decimal(18,4),
	@MarketMoverType nvarchar(256) = null,
    @EcoSystemType nvarchar(256) = null,
    @MetricType nvarchar(256) = null
)      
AS      
/*================================================================================      
Procedure Name: [SFab_InsertUpdateMakeMyMarketImpact]      
Author: Swami      
Create date: 27/07/2019      
Description: Insert/Update a record into MakeMyMarketImpact table      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
27/07/2019 Swami   Initial Version    
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)  
BEGIN TRY         
IF EXISTS(SELECT 1 FROM MakeMyMarketImpact WHERE CustomerId = @CustomerId
		  AND ProjectId = @ProjectId AND MakeMyMarketId = @MakeMyMarketId 
		  --AND Id = @MarketImpactId 
		  --AND (@MarketImpactId != '00000000-0000-0000-0000-000000000000')
		  )
BEGIN
UPDATE MakeMyMarketImpact
SET 
	[Value]				=   list.DecimalValue,
	[CAGR]				=	@CAGR,
	[MarketMoverType]	=	@MarketMoverType,
    [EcoSystemType]		=	@EcoSystemType,
    [MetricType]		=	@MetricType,
	ModifiedOn			=   GETDATE(),
	ModifiedBy			=   @CustomerId

FROM MakeMyMarketImpact mv
inner join @YearValuesList list
on mv.Year = list.IntValue
WHERE mv.CustomerId = @CustomerId
AND mv.ProjectId = @ProjectId
AND mv.MakeMyMarketId = @MakeMyMarketId
--AND mv.Id = @MarketImpactId


SELECT StoredProcedureName ='SFab_InsertUpdateMakeMyMarketImpact',Message =@ErrorMsg,Success = 1;  
END
ELSE
BEGIN
INSERT INTO MakeMyMarketImpact
(
	[Id]
   ,[MakeMyMarketId]
   ,[ProjectId]
   ,[CustomerId]
   ,[MarketMoverType]
   ,[EcoSystemType]
   ,[MetricType]
   ,[Year]
   ,[Value]
   ,[CAGR]
   ,[CreatedOn]
   ,[CreatedBy]
   ,[IsDeleted]
) 
SELECT NEWID(),
	   @MakeMyMarketId,
	   @ProjectId,	
	   @CustomerId,
	   @MarketMoverType,
	   @EcoSystemType,
	   @MetricType,
	   list.IntValue,
	   list.DecimalValue,
	   @CAGR,
	   GETDATE(),
	   @CustomerId,
	   0
FROM @YearValuesList list;
	   	  
SELECT StoredProcedureName ='SFab_InsertUpdateMakeMyMarketImpact',Message =@ErrorMsg,Success = 1;       
END    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);      
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());      
    
 set @ErrorMsg = 'Error while insert a new TrendProjectMap, please contact Admin for more details.';    
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
	       
END CATCH            
END 

--_VariableCustomizationParital