﻿using Avira.MarketingResearch.Model.ViewModels.Trend;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.Trend
{
     public interface ITrendPreviewService
    {
        TrendPreviewViewModel GetById(Guid guid);
    }
}
