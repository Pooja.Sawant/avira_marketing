﻿
--execute [SFab_GetCIPriceInformationChartData]  @CompanyID = '9bc77007-8949-41df-8214-fe3e8efb572f', @FilterType='3M'

Create   Procedure [dbo].[SFab_GetCIPriceInformationChartData] 
(
	@CompanyID uniqueidentifier,
	@FilterType nvarchar(20) = null
)
As
/*================================================================================  
Procedure Name: [SFab_GetCIPriceInformationChartData]  
Author: Nitin 
Create date: 14-Nov-2019  
Description: Get CI Price Information Tab Data
Change History  
Date         Developer     Reason  
____________ __________  ______________________________________________________  
12-Nov-2019  Nitin       Initial Version  
================================================================================*/  
BEGIN
	Declare @LastTradedDate DateTime

	--Declare @CompanyID uniqueidentifier = '7cfa9e8e-d78d-41d9-be62-4e49fdc46fa6', @FilterType nvarchar(20) ='1M'

	If (@FilterType Is Null Or @FilterType = '')
	Begin
		
		Select CONVERT(char(10), [Date], 126) As [TIMESTAMP], [dbo].[fun_UnitDeConversion](CurrencyUnitId, IsNull([Price], 0)) As [CLOSE], 
			Cast([Volume] As bigint) As TURNOVER
		From CompanyPriceVolume
		Where CompanyId = @CompanyID
		Order By [Date] 
		Return;
	End

	Select @LastTradedDate = Max(CPV.[Date])
	From CompanyPriceVolume CPV
	Where CompanyId = @CompanyID
	
	If (@FilterType = '1M')
	Begin
		
		Select CONVERT(char(10), [Date], 126) As [TIMESTAMP], [dbo].[fun_UnitDeConversion](CurrencyUnitId, IsNull([Price], 0)) As [CLOSE], 
			Cast([Volume] As bigint) As TURNOVER
		From CompanyPriceVolume
		Where CompanyId = @CompanyID
			And [Date] Between DateAdd(DAY, -29, @LastTradedDate) And @LastTradedDate
		Order By [Date] 
		Return;
	End

	If (@FilterType = '3M')
	Begin
		
		Select CONVERT(char(10), [Date], 126) As [TIMESTAMP], [dbo].[fun_UnitDeConversion](CurrencyUnitId, IsNull([Price], 0)) As [CLOSE], 
			Cast([Volume] As bigint) As TURNOVER
		From CompanyPriceVolume
		Where CompanyId = @CompanyID
			And [Date] Between DateAdd(DAY, -89, @LastTradedDate) And @LastTradedDate
		Order By [Date]
		Return;
	End

	If (@FilterType = '6M')
	Begin
		
		Select CONVERT(char(10), [Date], 126) As [TIMESTAMP], [dbo].[fun_UnitDeConversion](CurrencyUnitId, IsNull([Price], 0)) As [CLOSE], 
			Cast([Volume] As bigint) As TURNOVER
		From CompanyPriceVolume
		Where CompanyId = @CompanyID
			And [Date] Between DateAdd(DAY, -179, @LastTradedDate) And @LastTradedDate
		Order By [Date] 
		Return;
	End
	
	If (@FilterType = '1Y')
	Begin
		
		Select CONVERT(char(10), [Date], 126) As [TIMESTAMP], [dbo].[fun_UnitDeConversion](CurrencyUnitId, IsNull([Price], 0)) As [CLOSE], 
			Cast([Volume] As bigint) As TURNOVER
		From CompanyPriceVolume
		Where CompanyId = @CompanyID
			And [Date] Between DateAdd(DAY, -1, DATEADD(YEAR, -1, @LastTradedDate)) And @LastTradedDate
		Order By [Date] 
		Return;
	End

	If (@FilterType = '2Y')
	Begin
		
		Select CONVERT(char(10), [Date], 126) As [TIMESTAMP], [dbo].[fun_UnitDeConversion](CurrencyUnitId, IsNull([Price], 0)) As [CLOSE], 
			Cast([Volume] As bigint) As TURNOVER
		From CompanyPriceVolume
		Where CompanyId = @CompanyID
			And [Date] Between DateAdd(DAY, -1, DATEADD(YEAR, -2, @LastTradedDate)) And @LastTradedDate
		Order By [Date] 
		Return;
	End

	If (@FilterType = '5Y')
	Begin
		
		Select CONVERT(char(10), [Date], 126) As [TIMESTAMP], IsNull([Price], 0) As [CLOSE], 
			Cast([Volume] As bigint) As TURNOVER
		From CompanyPriceVolume
		Where CompanyId = @CompanyID
			And [Date] Between DateAdd(DAY, -1, DATEADD(YEAR, -4, @LastTradedDate)) And @LastTradedDate
		Order By [Date] 
		Return;
	End
End