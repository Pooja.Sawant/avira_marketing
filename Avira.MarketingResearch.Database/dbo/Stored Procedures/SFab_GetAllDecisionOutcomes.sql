﻿CREATE PROCEDURE [dbo].[SFab_GetAllDecisionOutcomes]      
(    
 @DecisionId UNIQUEIDENTIFIER,
 @AviraUserId UNIQUEIDENTIFIER  
)  
As      
  
/*================================================================================      
Procedure Name: SFab_GetAllDecisionOutcomes     
Author: Pooja Sawant      
Create date: 24-Jan-2020  
Description: To return all Decision outcome by decisionId of the user  
Change History      
Date  Developer  Reason    
__________ ____________ ______________________________________________________      
24-Jan-2020  Pooja S   Initial Version      
================================================================================*/      
BEGIN      
 
 declare @DecisionStatusId uniqueidentifier
select @DecisionStatusId = ID from LookupStatus
where StatusName = 'Decision made' and StatusType='DMDecisionStatus';



SELECT        DecisionOutcome.Id, DecisionOutcome.TargetedOutcome
FROM            DecisionOutcome inner JOIN
                         Decision ON DecisionOutcome.DecisionId = Decision.Id

where Decision.Id=@DecisionId and Decision.UserId=@AviraUserId	   
       order by DecisionOutcome.TargetedOutcome
    
END