﻿


CREATE procedure [dbo].[SFab_GetQualitativeDescriptionDatabyRegionID]
(@ProjectID uniqueidentifier=null,
@RegionID uniqueidentifier=null,
@includeDeleted bit = 0,
@AviraUserID uniqueidentifier = null)
as
/*================================================================================
Procedure Name: SFab_GetQualitativeDescriptionDatabyRegionID
Author: Harshal 
Create date: 09/04/2019
Description: Get list from QualitativeDescription table by Filters
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
09/04/2019	Harshal			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if @includeDeleted is null set @includeDeleted = 0

SELECT [QualitativeDescription].[Id]
      ,[QualitativeDescription].[ProjectId]
	  ,[QualitativeDescription].[MarketId]
      ,[QualitativeAnalysisTypeId]
      ,[QualitativeAnalysisSubTypeId]
      ,[DescriptionText]
		
FROM [dbo].[QualitativeDescription]
INNER JOIN QualitativeRegionMapping ON QualitativeRegionMapping.ProjectId=QualitativeDescription.ProjectId
INNER JOIN QualitativeAnalysisType ON QualitativeAnalysisType.Id = QualitativeDescription.QualitativeAnalysisTypeId
where (QualitativeRegionMapping.ProjectId = @ProjectID)
AND (QualitativeRegionMapping.RegionId = @RegionID)
and (@includeDeleted = 1 or QualitativeRegionMapping.IsDeleted = 0)
AND QualitativeAnalysisTypeName =  'Geography'
end