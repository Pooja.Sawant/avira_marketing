﻿CREATE procedure [dbo].[SApp_insertUpdateCompanyAnalystView]
(@ID uniqueidentifier=null,
@CompanyId uniqueidentifier,
@ProjectId uniqueidentifier,
@CompanyAnalystViewName nvarchar(MAX),
@IsDeleted bit,
@AviraUserId uniqueidentifier)
as
/*================================================================================
Procedure Name: SApp_insertUpdateCompanyAnalystView
Author: Harshal
Create date: 05/23/2019
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
05/23/2019	Harshal			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ErrorMsg NVARCHAR(2048);

BEGIN TRY


IF (@ID is null OR @ID= '00000000-0000-0000-0000-000000000000')
begin
--Insert new record
Insert into [dbo].[CompanyAnalystView]([Id],
	                            [CompanyId],
								[ProjectId],
								[AnalystView],
								[IsDeleted],
								[UserCreatedById],
								[CreatedOn])
	Values(NEWID(), 
	        @CompanyId,
			@ProjectId,
			@CompanyAnalystViewName, 
			@IsDeleted,
			@AviraUserId, 
			getdate());
end
else
begin
--update record
	update [dbo].[CompanyAnalystView]
	set [AnalystView] = @CompanyAnalystViewName,
	    [CompanyId] =@CompanyId,
		[ProjectId] =@ProjectId,
		[ModifiedOn] = getdate(),
		DeletedOn = case when IsDeleted = 0 and @IsDeleted = 1 then getdate() else DeletedOn end,
		UserDeletedById = case when IsDeleted = 0 and @IsDeleted = 1 then @AviraUserId else UserDeletedById end,
		IsDeleted = isnull(@IsDeleted, IsDeleted),
		[UserModifiedById] = @AviraUserId
     where CompanyAnalystView.ID = @Id
end
SELECT StoredProcedureName ='SApp_insertUpdateCompanyAnalystView',Message =@ErrorMsg,Success = 1;  
END TRY
BEGIN CATCH
    -- Execute the error retrieval routine.
	DECLARE @ErrorNumber	int;
	DECLARE @ErrorSeverity	int;
	DECLARE @ErrorProcedure	varchar(100);
	DECLARE @ErrorLine	int;
	DECLARE @ErrorMessage	varchar(500);

	 SELECT @ErrorNumber = ERROR_NUMBER(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorProcedure = ERROR_PROCEDURE(),
        @ErrorLine = ERROR_LINE(),
        @ErrorMessage = ERROR_MESSAGE()

	insert into dbo.Errorlog(ErrorNumber,
							ErrorSeverity,
							ErrorProcedure,
							ErrorLine,
							ErrorMessage,
							ErrorDate)
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GetDate());

	set @ErrorMsg = 'Error, please contact Admin for more details.';

SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;

END CATCH


end