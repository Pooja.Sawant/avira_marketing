﻿CREATE TABLE [dbo].[StagingCompanyTrends] (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [ImportId]     UNIQUEIDENTIFIER NULL,
    [Trends]       NVARCHAR (1000)  NULL,
    [Department]   NVARCHAR (255)   NULL,
    [Category]     NVARCHAR (255)   NULL,
    [Ecosystem]    NVARCHAR (255)   NULL,
    [Importance]   NVARCHAR (255)   NULL,
    [Impact]       NVARCHAR (255)   NULL,
    [Market]       NVARCHAR (255)   NULL,
    [ImportanceId] UNIQUEIDENTIFIER NULL,
    [ImpactId]     UNIQUEIDENTIFIER NULL,
    [ProjectId]    UNIQUEIDENTIFIER NULL,
    [CompanyId]    UNIQUEIDENTIFIER NULL,
    [ErrorNotes]   NVARCHAR (1000)  NULL,
    [RowNo]        INT              NULL,
    CONSTRAINT [PK_StagingCompanyTrends] PRIMARY KEY CLUSTERED ([Id] ASC)
);





