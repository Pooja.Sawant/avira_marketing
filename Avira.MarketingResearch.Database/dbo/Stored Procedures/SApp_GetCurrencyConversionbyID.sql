﻿

CREATE PROCEDURE [dbo].[SApp_GetCurrencyConversionbyID]
(
	@Id uniqueIdentifier
)
	AS
/*================================================================================
Procedure Name: [SApp_GetCurrencyConversionbyID]
Author: Adarsh
Create date: 04/25/2019
Description: Get list from Currency table by Name or ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
04/25/2019	Sai Krishna			Initial Version
__________	____________	______________________________________________________

================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT [Currency].[Id] as CurrencyId
      ,[Currency].[CurrencyCode]
      ,[Currency].[CurrencyName]
      ,[Currency].[CurrencySymbol]
      ,[Currency].[Fractionalunit]
      ,[Currency].[IsDeleted]
	  ,[CurrencyConversion].[ConversionRangeType]
	  ,[CurrencyConversion].[StartDate]
	  ,[CurrencyConversion].[EndDate]
	  ,[CurrencyConversion].[Year]
	  ,[CurrencyConversion].[Quarter]
	  ,[CurrencyConversion].[Id] as CurrencyConversionId
	  ,[CurrencyConversion].[ConversionRate]
   FROM [dbo].[Currency] left outer JOIN CurrencyConversion on Currency.Id = CurrencyConversion.CurrencyId
  where Currency.Id = @Id

END