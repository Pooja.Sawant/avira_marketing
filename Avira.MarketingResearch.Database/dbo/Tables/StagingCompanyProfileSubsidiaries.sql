﻿CREATE TABLE [dbo].[StagingCompanyProfileSubsidiaries] (
    [Id]                       UNIQUEIDENTIFIER NOT NULL,
    [ImportId]                 UNIQUEIDENTIFIER NULL,
    [CompanySubsdiaryName]     NVARCHAR (255)   NULL,
    [CompanySubsdiaryLocation] NVARCHAR (255)   NULL,
    [CompanyId]                UNIQUEIDENTIFIER NULL,
    [ErrorNotes]               NVARCHAR (1000)  NULL,
    [RowNo]                    INT              NULL,
    CONSTRAINT [PK_StagingCompanyProfileSubsidiaries] PRIMARY KEY CLUSTERED ([Id] ASC)
);



