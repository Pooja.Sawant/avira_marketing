﻿CREATE TABLE [dbo].[CompanyFinancialRelativeValuationInput] (
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [CompanyId]         UNIQUEIDENTIFIER NULL,
    [ProjectId]         UNIQUEIDENTIFIER NULL,
    [BusinessObjective] NVARCHAR (512)   NULL,
    [CreatedOn]         DATETIME2 (7)    NULL,
    [UserCreatedById]   UNIQUEIDENTIFIER NULL,
    [ModifiedOn]        DATETIME2 (7)    NULL,
    [UserModifiedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CompanyFinancialRelativeValuationInput] PRIMARY KEY CLUSTERED ([Id] ASC)
);

