﻿using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport
{
    public interface ISqlTableSchemaRepository
    {
        DataTable GetTableSchema(SchemaModel model);
    }
}
