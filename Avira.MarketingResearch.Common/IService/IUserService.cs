﻿using System;
using System.Collections.Generic;
using System.Text;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.Helpers;

namespace Avira.MarketingResearch.Common.IService
{
    public interface IUserService
    {
        Result<SpTransactionMessage> Create(AviraUserInsertDbViewModel aviraUserInsertDbViewModel, AviraUser currentUser);
        AviraUser GetByEmail(string email);
        AviraUser GetByUserId(string userId);
        AviraUser GetUserLogin(string userName);
        IEnumerable<RoleViewModel> GetAllRoles();

        AviraUser UpdateUserLogin(string userName, bool IsSuccess);
        IEnumerable<LocationViewModel> GetAllLocation();

        IEnumerable<RoleViewModel> GetRolesByAviraUserId(Guid aviraUserId);
        IEnumerable<AviraUser> GetAllUsersByRole(string roleName);

        IEnumerable<GetAllAviraUsers> GetAll(Guid? Id, int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false);

        PermissionsByUserRoleIdViewModel GetPermissionsByUserRoleId(Guid id);
    }
}
