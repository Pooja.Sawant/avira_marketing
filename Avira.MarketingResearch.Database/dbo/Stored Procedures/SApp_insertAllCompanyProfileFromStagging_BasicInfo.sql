﻿

CREATE procedure [dbo].[SApp_insertAllCompanyProfileFromStagging_BasicInfo]
@AviraUserId uniqueidentifier =null
as    
/*================================================================================    
Procedure Name: [SApp_insertAllCompanyProfileFromStagging_BasicInfo]    
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
19/06/2019 Praveen   Initial Version    
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    
declare @ErrorMsg NVARCHAR(2048)
declare @CompanyId	UNIQUEIDENTIFIER;

BEGIN TRY       
BEGIN TRANSACTION
select @CompanyId = CompanyId	
from [StagingCompanyProfileFundamentals];

--Fundamentals
Insert Into Company(
			Id,
			CompanyName,			
			NatureId,		
			CompanyLocation,
			IsHeadQuarters,
			CreatedOn,
			UserCreatedById,
			IsDeleted
)
Select CPFStaging.CompanyId,
	   CPFStaging.CompanyName,	  
	   CPFStaging.NatureId,	
		CPFStaging.Headquarter,
		1 as IsHeadQuarters,
		 GETDATE(),
	   @AviraUserId,
	   0
From [StagingCompanyProfileFundamentals] CPFStaging;


;with CTE_SCP as (select SCPF.CompanyId, 
						 GEO.Value as RegionName
		from [StagingCompanyProfileFundamentals] SCPF
		cross apply dbo.FUN_STRING_TOKENIZER(SCPF.GeographicPresence, ',') GEO ),
CTE_CRMap as (select  CTE_SCP.CompanyId, 
					Region.Id as RegionID
			from CTE_SCP
			inner join Region
			on CTE_SCP.RegionName = Region.RegionName
			)
insert into [dbo].[CompanyRegionMap](
		[Id],      
        [RegionId],
		[CompanyId],
        [IsDeleted],      
        [UserCreatedById],      
        [CreatedOn])      
select NEWID(),
CTE_CRMap.RegionID,
CTE_CRMap.CompanyId,
0 as isDeleted,
@AviraUserId,
getdate()
from CTE_CRMap;


Declare @Today datetime = getdate()
Declare @USDCurrencyId uniqueidentifier	
Set @USDCurrencyId = dbo.[fun_GetCurrencyIDForUSD]()

UPDATE StagingCompanyBasicRevenue SET FinanicialYear = (SELECT TOP 1 value FROM STRING_SPLIT (FinanicialYear, '-'))
DELETE CPLS
	FROM CompanyPLStatement CPLS
	INNER JOIN StagingCompanyBasicRevenue StagCPBR
	ON --CPLS.Year = CAST(StagCPBR.FinanicialYear AS int)  AND 
	CPLS.CompanyId = StagCPBR.CompanyId

	;with cte_CompanyPLStatement as(              
Select  --CAST(Revenues_2018 AS decimal(19,4)) as Revenues, 
 CompanyId,  
 CurrencyId,  
 ValueConversionId,              
 CompanyName,               
 Currency,              
 Units,
CAST(FinanicialYear AS INT) FinanicialYear,
CAST(CASE WHEN ISNUMERIC(Revenue) = '0' THEN '0' ELSE Revenue END AS decimal(19,4)) AS Revenue
from StagingCompanyBasicRevenue              
)        
--SELECT * FROM cte_CompanyPLStatement

insert into CompanyPLStatement              
(              
 Id,
CompanyId,
CurrencyId,
ValueConversionId,
CompanyName,
Currency,
Units,
Year,
Revenue,
CreatedOn,              
IsDeleted              
 )
 Select NEWID(),           
	CompanyId,           
	CurrencyId,              
	ValueConversionId,            
	CompanyName,             
	Currency,              
	Units,              
	FinanicialYear,
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, Revenue), CurrencyId, @USDCurrencyId, @Today),
	GETDATE(),              
     0               
 from cte_CompanyPLStatement;

SELECT StoredProcedureName ='[SApp_insertAllCompanyProfileFromStagging_BasicInfo]',Message =@ErrorMsg,Success = 1;     
COMMIT TRANSACTION
END TRY    
BEGIN CATCH    
 if @@TRANCOUNT > 0 
 ROLLBACK TRANSACTION;

 ---Update Statics--
 UPDATE STATISTICS [Company] WITH FULLSCAN;
 UPDATE STATISTICS [CompanyRegionMap] WITH FULLSCAN;
 ---End Statics----
 
    -- Execute the error retrieval routine.    
 DECLARE @ErrorNumber int;    
 DECLARE @ErrorSeverity int;    
 DECLARE @ErrorProcedure varchar(100);    
 DECLARE @ErrorLine int;    
 DECLARE @ErrorMessage varchar(500);    
    
  SELECT @ErrorNumber = ERROR_NUMBER(),    
        @ErrorSeverity = ERROR_SEVERITY(),    
        @ErrorProcedure = ERROR_PROCEDURE(),    
        @ErrorLine = ERROR_LINE(),    
        @ErrorMessage = ERROR_MESSAGE()    
    
 insert into dbo.Errorlog(ErrorNumber,    
       ErrorSeverity,    
       ErrorProcedure,    
       ErrorLine,    
       ErrorMessage,    
       ErrorDate)    
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());    
  
 set @ErrorMsg = 'Error while creating a new Company Profile, please contact Admin for more details.';    
--THROW 50000,@ErrorMsg,1;      
--return(1)  
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;     
     
END CATCH    
    
    
end