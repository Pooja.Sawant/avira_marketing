﻿using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Utility;
using Avira.MarketingResearch.WebApi;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.Repository.CoreInterfaces.Masters;
using Avira.MarketingResearch.Common.IService.Masters;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using System;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyFundamentalController : AviraBaseController
    {
        private ILoggerManager _logger;
        public readonly ICompanyFundamentalService _companyFundamentalService;
        public readonly ICompanyFundamentalRepository _companyFundamentalRepository;

        //public CompanyStructureController(
        //    ICompanyStructureService companyStructureService,
        //    ICompanyStructureRepository CompanyStructureRepository,
        //    ILoggerManager logger)
        //{
        //    _companyStructureRepository = CompanyStructureRepository;
        //    _companyStructureService = companyStructureService;
        //    _logger = logger;
        //}
        public CompanyFundamentalController(
            ICompanyFundamentalService companyFundamentalService,
            ICompanyFundamentalRepository companyFundamentalRepository,
            ILoggerManager logger)
        {
            _companyFundamentalRepository = companyFundamentalRepository;
            _companyFundamentalService = companyFundamentalService;
            _logger = logger;
        }

        [HttpGet]
        [Route("CompanyFundamental")]
        public ActionResult<IEnumerable<CompanyFundamentalViewModel>> Get()
        {
            List<CompanyFundamentalViewModel> res = null;
            try
            {
                res = _companyFundamentalService.GetAll().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet]
        [Route("CompanyRegion")]
        public ActionResult<IEnumerable<CompanyRegionViewModel>> CompanyRegion()
        {
            List<CompanyRegionViewModel> res = null;
            try
            {
                res = _companyFundamentalService.GetAllRegion().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet]
        [Route("CompanyEntity")]
        public ActionResult<IEnumerable<CompanyEntityViewModel>> CompanyEntity()
        {
            List<CompanyEntityViewModel> res = null;
            try
            {
                res = _companyFundamentalService.GetAllEntities().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet]
        [Route("ManagersList")]
        public ActionResult<IEnumerable<CompanyManagerViewModel>> ManagersList(string companyId)
        {
            List<CompanyManagerViewModel> res = null;
            try
            {
                res = _companyFundamentalService.ManagersList(companyId).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }


        [HttpGet]
        [Route("NaturesList")]
        public ActionResult<IEnumerable<CompanyNatureViewModel>> GetAllNatures()
        {
            List<CompanyNatureViewModel> res = null;
            try
            {
                res = _companyFundamentalService.NaturesList().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet]
        [Route("CompanySubsidary")]
        public ActionResult<IEnumerable<CompanySubsidaryViewModel>> CompanySubsidary(string Id)
        {
            List<CompanySubsidaryViewModel> res = null;
            try
            {
                Guid id = new Guid(Id);
                res = _companyFundamentalService.GetCompanySubsidaries(id).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }


        //[HttpGet]
        //[Route("CompanyDetails")]
        //public ActionResult<IEnumerable<CompanyDetailsViewModel>> CompanyDetails(string CompanyId, string ProjectId)
        //{
        //    Guid cid = new Guid(CompanyId);
        //    Guid pid = new Guid(ProjectId);
        //    return _companyFundamentalService.GetCompanyDetails(cid, pid).ToList();
        //}

        [HttpGet]
        [Route("CompanyDetails")]
        public async Task<CompanyDetailsInsertUpdateViewModel> CompanyDetails(string CompanyId, string ProjectId)
        {
            Guid cid = new Guid(CompanyId);
            Guid pid = new Guid(ProjectId);
            return await _companyFundamentalService.GetCompanyDetails(cid, pid);
        }

        [HttpGet]
        [Route("AllCompaniesList")]
        public ActionResult<IEnumerable<AllCompaniesListViewModel>> AllCompaniesList()
        {
            List<AllCompaniesListViewModel> res = null;
            try
            {
                res = _companyFundamentalService.GetAllCompaniesList().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet]
        [Route("AllDesignations")]
        public ActionResult<IEnumerable<DesignationViewModel>> AllDesignations()
        {
            List<DesignationViewModel> res = null;
            try
            {
                res = _companyFundamentalService.AllDesignations().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet]
        [Route("AllCompanyStageList")]
        public ActionResult<IEnumerable<AllCompanyStageListViewModel>> AllCompanyStageList()
        {
            List<AllCompanyStageListViewModel> res = null;
            try
            {
                res = _companyFundamentalService.GetAllCompanyStageList().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpPost]
        public ActionResult Post([FromBody] CompanyDetailsInsertUpdateViewModel viewModel)
        {
            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new Company Fundamentals, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                var result = _companyFundamentalService.InsertUpdate(viewModel, CurrentUser);
                if (result != null && result.IsSuccess)
                {
                    viewData.Id = (result != null && !string.IsNullOrEmpty(result.Value.Id) ? new Guid(result.Value.Id) : Guid.Empty);
                    viewData.IsSuccess = true;
                    viewData.Message = "Company Fundamentals is updated successfully.";
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }
    }
}