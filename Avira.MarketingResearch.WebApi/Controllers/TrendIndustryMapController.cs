﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrendIndustryMapController : AviraBaseController
    {
        private readonly ITrendIndustryMapService _trendIndustryMapService;
        private readonly ITrendIndustryMapRepository _trendIndustryMapRepository;
        private ILoggerManager _logger;

        public TrendIndustryMapController(
            ITrendIndustryMapService trendIndustryMapService,
            ITrendIndustryMapRepository trendIndustryMapRepository,
             ILoggerManager logger)
        {
            _trendIndustryMapRepository = trendIndustryMapRepository;
            _trendIndustryMapService = trendIndustryMapService;
            _logger = logger;

        }

        [HttpGet]
        public ActionResult<IEnumerable<TrendIndustryMapViewModel>> Get(int PageStart = 0, int pageSize = 0, int SortDirection = 1, string OrdbyByColumnName = "CreatedDate", string Search = "", bool isDeletedInclude = false, string id = "")
        {
            List<TrendIndustryMapViewModel> res = null;
            try
            {
                res = _trendIndustryMapService.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude, id).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpPost]
        public ActionResult Post([FromBody] TrendIndustryMapInsertDbViewModel viewModel)
        {
            viewModel.CreatedOn = System.DateTime.UtcNow;
            viewModel.AviraUserId = CurrentUser.Id;

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while mapping Industry to trend, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                var result = _trendIndustryMapService.Create(viewModel, CurrentUser);
                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "TrendIndustryMap name " + viewModel.TrendId + " is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        } 
    }

}