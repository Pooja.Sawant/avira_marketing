﻿CREATE PROCEDURE [dbo].[SFab_InsertDecisionAlternative]    
(  
	@DecisionId UNIQUEIDENTIFIER,
	@Description NVARCHAR(1024),
	@ResourceAssigned NVARCHAR(899),
	@StatusId UNIQUEIDENTIFIER,
	@PriorityId UNIQUEIDENTIFIER,
	@AviraUserId UNIQUEIDENTIFIER,
	@AlternativeId uniqueidentifier out

)
As    

/*================================================================================    
Procedure Name: SFab_InsertDecisionAlternative   
Author: Praveen /Pooja  
Create date: 07-Jan-2020
Description: To add new Decision Alternative by user
Change History    
Date  Developer  Reason  
__________ ____________ ______________________________________________________    
07-Jan-2020  Praveen   Initial Version   
08-Jan-2020  Pooja S   Implementation 
================================================================================*/    
BEGIN    
------------------
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)  
	--Required Tables: DecisionAlternative
	--Notes: Add new alternative of a perticular decision.
	Declare @DecisionStatusDecisionMadeId uniqueidentifier
	Declare @DecisionStatusDecisionId uniqueidentifier
	Select @DecisionStatusDecisionMadeId = ID From LookupStatus
	Where StatusName = 'Decision made' And StatusType = 'DMDecisionStatus'

	Select @DecisionStatusDecisionId = ID From LookupStatus
	Where StatusName = 'Ongoing' And StatusType = 'DMDecisionStatus'

BEGIN TRY 
	If Exists (Select ID from Decision Where Id = @DecisionId and UserId= @AviraUserID and StatusId=@DecisionStatusDecisionMadeId)
        Begin
        	set @ErrorMsg = 'Can not add any new details since this decision has been made.';   
        		SELECT StoredProcedureName ='SFab_InsertDecisionAlternative','Message' =@ErrorMsg,'Success' = 0;  
        end
	else 
	If Exists (Select ID from Decision Where Id = @DecisionId and UserId= @AviraUserID and (StatusId != @DecisionStatusDecisionMadeId and StatusId != @DecisionStatusDecisionId))
	    Begin
	    	set @ErrorMsg = 'Please change decision status to Ongoing in order to add new details.';   
	    		SELECT StoredProcedureName ='SFab_InsertDecisionAlternative','Message' =@ErrorMsg,'Success' = 0;    
	    end
	else
	    Begin
		Declare @newAlternativeId uniqueidentifier
		set  @newAlternativeId = newid()
	        INSERT INTO [dbo].[DecisionAlternative]
           ([Id]
           ,[DecisionId]
           ,[Description]
           ,[ResourceAssigned]
           ,[StatusId]
           ,[PriorityId]
           ,[CreatedOn]
           ,[UserCreatedById]
           )
     VALUES
           (@newAlternativeId
           ,@DecisionId
           ,@Description
           ,trim (@ResourceAssigned)
           ,@StatusId
           ,@PriorityId
           ,getdate()
           ,@AviraUserId
           )
		   SET @AlternativeId = @newAlternativeId
	       	    --set @ErrorMsg = 'Your alternatives has been added to the list successfully.';    
	       		 SELECT StoredProcedureName ='SFab_InsertDecisionAlternative',Message =@ErrorMsg,Success = 1;   
	    end    
   
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 exec SApp_LogErrorInfo
 set @ErrorMsg = 'Error while insert a new DecisionAlternative, please contact Admin for more details.';    
SELECT StoredProcedureName ='SFab_InsertDecisionAlternative',Message =@ErrorMsg,Success = 0;
END CATCH
		
END