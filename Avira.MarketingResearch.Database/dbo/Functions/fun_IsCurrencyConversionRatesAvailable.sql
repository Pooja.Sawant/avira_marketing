CREATE FUNCTION fun_IsCurrencyConversionRatesAvailable
(
	@InCurrencyId UNIQUEIDENTIFIER=NULL, 
	@OutCurrencyId UNIQUEIDENTIFIER, 
	@TransactionDate Datetime = NULL	
)
/*================================================================================  
Function Name: [fun_IsCurrencyConversionRatesAvailable]  
Author: Nitin  
Create date: 30-Aug-2019  
Description: Function to check if currency conversion rates are avialable
Change History  
Date        Developer     Reason  
__________  ____________  ______________________________________________________  
30-Aug-2019 Nitin Tarkar   Initial Version  
================================================================================*/ 

RETURNS bit 
AS
BEGIN

	--Declare @InCurrencyId UNIQUEIDENTIFIER = 'E30BBB66-B534-469A-8BDD-A968487EB586', --USD
	--	    @OutCurrencyId UNIQUEIDENTIFIER = '60C75772-A723-4386-8C48-05CA640D5681', --AFG
	--		@TransactionDate Datetime = NULL	

	-- Declare the return variable here
	If (@InCurrencyId Is NULL)
	Begin

		Select @InCurrencyId = Id from Currency
		Where CurrencyName = 'United States dollar';
	End
	
	Declare @Year int
	Declare @Quarter int
	Declare @CurrencyConversionRatesAvailable Bit = 0

	If (@TransactionDate Is Null) 
	Begin
		Set @TransactionDate = GETDATE();
	End
	Set @Year = DATEPART(YYYY, @TransactionDate);
	Set @Quarter = DATEPART(QQ, @TransactionDate);

	If Exists(Select top 1 ConversionRate
			  From [CurrencyConversion]
			  Where CurrencyId = @InCurrencyId AND 
					((ConversionRangeType = 1 AND @TransactionDate  between StartDate and EndDate ) --Time Range
					OR (ConversionRangeType = 2 AND [Year] = @Year) --Yearly
					OR (ConversionRangeType = 3 AND [Year] = @Year and [Quarter] = @Quarter)) --Quarterly
			  Order By ConversionRangeType)
	Begin

		If Exists(Select top 1 ConversionRate
			  From [CurrencyConversion]
			  Where CurrencyId = @OutCurrencyId AND 
					((ConversionRangeType = 1 AND @TransactionDate  between StartDate and EndDate ) --Time Range
					OR (ConversionRangeType = 2 AND [Year] = @Year) --Yearly
					OR (ConversionRangeType = 3 AND [Year] = @Year and [Quarter] = @Quarter)) --Quarterly
			  Order By ConversionRangeType)
		Begin
			
			Set @CurrencyConversionRatesAvailable = 1
		End
	End
	Return @CurrencyConversionRatesAvailable
END
GO

