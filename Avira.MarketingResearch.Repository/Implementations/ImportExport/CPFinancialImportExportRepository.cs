﻿using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.ImportExport
{
    public class CPFinancialImportExportRepository : ICPFinancialImportExportRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "CPFinancialImportExport";

        public CPFinancialImportExportRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }
        public List<ImportCommonResponseModel> Create(CPFinancialImportInsertModel viewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(ImportExportQueries.StoreProcInsertCPFinancial, new
            {
                @TemplateName = viewModel.TemplateName,
                @ImportFileName = viewModel.ImportFileName,
                @jsonTrend = viewModel.TrendJson,
                @JsonSegmentInfo= viewModel.SegmentJson,
                @UserCreatedById = viewModel.UserCreatedById
            }, CommandType.StoredProcedure);

            var spTransactionMessage1 = new List<ImportCommonResponseModel>();
            if (spTransactionMessage.IsSuccess == true)
            {
                var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
                spTransactionMessage1 = dapper.Query<ImportCommonResponseModel>(ImportExportQueries.StoreProcUpdateCPFinancial
                            , null
                            , null, true, null, CommandType.StoredProcedure) as List<ImportCommonResponseModel>;
            }
            return spTransactionMessage1;
        }
    }
}
