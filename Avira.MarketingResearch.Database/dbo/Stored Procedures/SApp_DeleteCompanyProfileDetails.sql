﻿
--Execute SApp_DeleteCompanyProfileDetails '724468E1-AD13-4E54-8CE8-772818B96A4C'

CREATE   procedure [dbo].[SApp_DeleteCompanyProfileDetails]
 @ClientId uniqueidentifier 
As   
/*================================================================================    
Procedure Name: [SApp_DeleteCompanyProfileDetails]    
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
19-Sep-2019 Nitin   Initial Version    
================================================================================*/    
Begin

	If Exists(Select Id From Company Where Id = @ClientId)
	Begin

		--Delete FROM CompanyProduct;
		--[CompanyProductCategoryMap]
		--[CompanyProductSegmentMap]
		--[CompanyProductCountryMap]
		--[CompanyProductValue]		

		Delete FROM ShareHoldingPatternMap Where CompanyId = @ClientId
		Delete FROM Company Where ParentId = @ClientId 
		Delete FROM CompanyKeyEmployeesMap Where CompanyId = @ClientId

		Delete FROM [News] Where CompanyId = @ClientId
		Delete FROM [CompanyEcosystemPresenceESMMap] Where CompanyId = @ClientId
		Delete FROM [CompanyEcosystemPresenceSectorMap] Where CompanyId = @ClientId
		Delete FROM [CompanyEcosystemPresenceKeyCompetitersMap] Where CompanyId = @ClientId
		Delete FROM [CompanySWOTMap] Where CompanyId = @ClientId
		Delete FROM [CompanyTrendsMap] Where CompanyId = @ClientId
		Delete FROM CompanyAnalystView Where CompanyId = @ClientId
		Delete FROM [CompanyProjectMap] Where CompanyId = @ClientId
		Delete FROM [CompanyNote] Where CompanyId = @ClientId
		Delete FROM [CompanyClientsMapping] where CompanyId = @ClientId
		Delete FROM [CompanyStrategyMapping] where CompanyId = @ClientId
		Delete FROM [CompanyPriceVolume] where CompanyId = @ClientId
		Delete From CompanyEcosystemSegmentMap where CompanyId = @ClientId

	End
End