﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces
{
    public interface ICompanyRepository
    {
        CompanyViewModel GetById(Guid guid);
        IEnumerable<CompanyViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted);
        Result<SpTransactionMessage> Create(CompanyInsertDbViewModel viewModel);
        Result<SpTransactionMessage> Update(CompanyUpdateDbViewModel viewModel);
    }
}
