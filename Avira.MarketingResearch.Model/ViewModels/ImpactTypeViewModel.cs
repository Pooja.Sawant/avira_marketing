﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels
{
    public class ImpactTypeViewModel
    {
        public Guid Id { get; set; }
        public string ImpactTypeName { get; set; }
    }
}
