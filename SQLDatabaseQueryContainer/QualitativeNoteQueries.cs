﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public class QualitativeNoteQueries
    {
        public static readonly string StoreProcInsert = "SApp_InsertQualitativeNotes";

        public static string StoreProcSelectAll = "SApp_GetQualitativeNoteByID";
    }
}
