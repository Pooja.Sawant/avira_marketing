﻿CREATE PROCEDURE [dbo].[SFab_InfographicsByProjectId]
	(
	@ProjectId uniqueidentifier = null,
	@UserId uniqueidentifier = null)
	AS
/*================================================================================
Procedure Name:[SFab_InfographicsByProjectId]
Author: Praveen
Create date: 10/28/2019
Description: Get list from Infographics table by project ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
10/28/2019	Praveen			Initial Version
================================================================================*/

BEGIN
SET NOCOUNT ON;  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT 
InfoGr.Id				,
InfoGr.ProjectId			,
InfoGr.LookupCategoryId	,
InfoGr.InfogrTitle				,
InfoGr.InfogrDescription			,
InfoGr.SequenceNo			,
InfoGr.ImageActualName		,
InfoGr.ImageName,
InfoGr.ImagePath,
InfoGr.CreatedOn			,
InfoGr.UserCreatedById		,
InfoGr.ModifiedOn			,
InfoGr.UserModifiedById,
Project.ProjectName,
LookupCategory.CategoryName as Category

FROM Infographics InfoGr
JOIN Project ON Project.ID = InfoGr.ProjectId
JOIN LookupCategory ON LookupCategory.ID = InfoGr.LookupCategoryId AND LookupCategory.CategoryType = 'Infographics'
where InfoGr.ProjectId = @ProjectId ORDER BY SequenceNo


	END