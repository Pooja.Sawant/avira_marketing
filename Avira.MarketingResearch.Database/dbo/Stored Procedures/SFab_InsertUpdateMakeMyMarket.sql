﻿
CREATE procedure [dbo].[SFab_InsertUpdateMakeMyMarket]      
(
	@Id uniqueidentifier = null,
	@CustomerId uniqueidentifier = null,
	@TrendId uniqueidentifier = null,
	@ProjectId uniqueidentifier = null,
	@CustomMarketRank int = null,
	@CustomMarketName nvarchar(250) = null,
	@SegmentType nvarchar(150) = null,
	@SegmentId uniqueidentifier = null,
	@CustomizationVariableType nvarchar(150) = null,
	@CustomizationLevelType nvarchar(150) = null, 
	@BroadLevelSegmentId uniqueidentifier = null,
	@SegmentLevelSegmentId uniqueidentifier = null,
	@ConstantCustomizationLevel nvarchar(150) = null,
	@ChoiceOfChangeOverallMarket nvarchar(150) = null,
	@ChoiceOfChangeBoardSegment nvarchar(150) = null,
	@ConstantOtherBoardLevel nvarchar(150) = null,
	@ChoiceOfChangeBoardLevel nvarchar(150) = null,
	@ConstantCustomizationSegment nvarchar(150) = null,
	@ConstantOtherSegmentLevel nvarchar(150) = null,
	@SegmentLevelSegmentName nvarchar(150) = null,
	@SegmentLevelSubSegmentName nvarchar(150) = null,
	@BroadLevelSegmentName nvarchar(150) = null,
	@IsMarketSelected bit = 0,
	@UserId uniqueidentifier = null
)      
AS      
/*================================================================================      
Procedure Name: [SFab_InsertUpdateMakeMyMarket]      
Author: Sai Krishna      
Create date: 18/07/2019      
Description: Insert a record into MakeMyMarket table      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
18/07/2019 Sai Krishna   Initial Version    
================================================================================*/      
BEGIN      
	SET NOCOUNT ON;      
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
	Declare @ErrorMsg NVARCHAR(2048)  
	
	If (@CustomerId Is Null)
	Begin
		
		Set @CustomerId = dbo.fun_GetCustomerIdForLoggedInUser(@UserId)
		print 'get customer id'
	End

	BEGIN TRY         
		IF Exists(SELECT 1 FROM MakeMyMarket WHERE Id = @Id)
		BEGIN
			
			print 1
			UPDATE MakeMyMarket
			SET CustomMarketRank				=	@CustomMarketRank,
				CustomMarketName                =   @CustomMarketName,
				SegmentType						=	@SegmentType,
				SegmentId						=	@SegmentId,
				CustomizationVariableType		=	@CustomizationVariableType,
				CustomizationLevelType			=	@CustomizationLevelType,
				BroadLevelSegmentId				=	@BroadLevelSegmentId,
				SegmentLevelSegmentId			=	@SegmentLevelSegmentId,
				ConstantCustomizationLevel		=	@ConstantCustomizationLevel,
				ChoiceOfChangeOverallMarket		=	@ChoiceOfChangeOverallMarket,
				ChoiceOfChangeBoardSegment		=	@ChoiceOfChangeBoardSegment,
				ConstantOtherBoardLevel			=	@ConstantOtherBoardLevel,
				ChoiceOfChangeBoardLevel		=	@ChoiceOfChangeBoardLevel,
				ConstantCustomizationSegment    =	@ConstantCustomizationSegment,
				ConstantOtherSegmentLevel		=	@ConstantOtherSegmentLevel,
				SegmentLevelSegmentName			=	@SegmentLevelSegmentName,
				SegmentLevelSubSegmentName		=	@SegmentLevelSubSegmentName,
				BroadLevelSegmentName			=	@BroadLevelSegmentName,
				IsMarketSelected				=   @IsMarketSelected,
				ModifiedOn						=   GETDATE(),
				UserModifiedById                =   @CustomerId

			WHERE CustomerId = @CustomerId
			AND ProjectId = @ProjectId
			AND ((@TrendId Is Null And TrendId is Null) Or TrendId = @TrendId)
			AND Id = @Id
			
			SELECT StoredProcedureName ='SFab_InsertUpdateMakeMyMarket',Message =@ErrorMsg,Success = 1;  
		END
		ELSE
		BEGIN
			print 2
			INSERT INTO MakeMyMarket
			(
				Id,
				CustomerId,
				TrendId,
				ProjectId,
				CustomMarketRank,
				CustomMarketName,
				SegmentType,
				SegmentId,
				CustomizationVariableType,
				CustomizationLevelType,
				BroadLevelSegmentId,
				SegmentLevelSegmentId,
				ConstantCustomizationLevel,
				ChoiceOfChangeOverallMarket,
				ChoiceOfChangeBoardSegment,
				ConstantOtherBoardLevel,
				ChoiceOfChangeBoardLevel,
				ConstantCustomizationSegment,
				ConstantOtherSegmentLevel,
				SegmentLevelSegmentName,
				SegmentLevelSubSegmentName,
				BroadLevelSegmentName,
				IsMarketSelected,
				CreatedOn,
				UserCreatedById,
				IsDeleted
			) 
			Values
			(
				@Id,
				@CustomerId,
				@TrendId,
				@ProjectId,
				@CustomMarketRank,
				@CustomMarketName,
				@SegmentType,
				@SegmentId,
				@CustomizationVariableType,
				@CustomizationLevelType,
				@BroadLevelSegmentId,
				@SegmentLevelSegmentId,
				@ConstantCustomizationLevel,
				@ChoiceOfChangeOverallMarket,
				@ChoiceOfChangeBoardSegment,
				@ConstantOtherBoardLevel,
				@ChoiceOfChangeBoardLevel,
				@ConstantCustomizationSegment,
				@ConstantOtherSegmentLevel,
				@SegmentLevelSegmentName,
				@SegmentLevelSubSegmentName,
				@BroadLevelSegmentName,
				@IsMarketSelected,
				GETDATE(),
				@CustomerId,
				0
			)
  
			SELECT StoredProcedureName ='SFab_InsertUpdateMakeMyMarket',Message =@ErrorMsg,Success = 1;       
		END    
	END TRY      
	BEGIN CATCH      
    -- Execute the error retrieval routine.      
		DECLARE @ErrorNumber int;      
		DECLARE @ErrorSeverity int;      
		DECLARE @ErrorProcedure varchar(100);      
		DECLARE @ErrorLine int;      
		DECLARE @ErrorMessage varchar(500);      
      
		SELECT @ErrorNumber = ERROR_NUMBER(),      
			@ErrorSeverity = ERROR_SEVERITY(),      
			@ErrorProcedure = ERROR_PROCEDURE(),      
			@ErrorLine = ERROR_LINE(),      
			@ErrorMessage = ERROR_MESSAGE()      
      
		insert into dbo.Errorlog(ErrorNumber,      
		   ErrorSeverity,      
		   ErrorProcedure,      
		   ErrorLine,      
		   ErrorMessage,      
		   ErrorDate)      
			values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());      
    
		Set @ErrorMsg = 'Error while insert a new TrendProjectMap, please contact Admin for more details.';    
		SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
			,Message =@ErrorMsg,Success = 0;	       
	END CATCH
END