﻿using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.ImportExport
{
    public class ReportGenerationImportExportRepository : IReportGenerationImportExportRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "ReportGenerationImportExport";

        public ReportGenerationImportExportRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        List<ImportCommonResponseModel> IReportGenerationImportExportRepository.Create(ReportGenerationImportModel viewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(ImportExportQueries.StoreProcInsertReportGeneration, new
            {
                @TemplateName = viewModel.TemplateName,
                @ImportFileName = viewModel.ImportFileName,
                @JsonReportGeneration= viewModel.JsonReportGeration,
                @UserCreatedById = viewModel.UserCreatedById
            }, CommandType.StoredProcedure);

            var spTransactionMessage1 = new List<ImportCommonResponseModel>();
            if (spTransactionMessage.IsSuccess == true)
            {
                //var spTransactionMessage1 = _dbHelper.ExecuteQuery(ImportExportQueries.StoreProcUpdateMarketSizing1, null, CommandType.StoredProcedure);
                var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
                var spTransactionMain = dapper.Query<ImportCommonResponseModel>(ImportExportQueries.StoreProcUpdateReportGeneration
                            , null
                            , null, true, null, CommandType.StoredProcedure);

                if (spTransactionMessage1 != null)
                {
                    spTransactionMessage1 = spTransactionMain.ToList();
                }
            }
            else
            {
                spTransactionMessage1.Add(new ImportCommonResponseModel { ErrorNotes = "Error while inserting data to staging tables.", Module = "Report Generation Import." });
            }
            
            return spTransactionMessage1;
        }
    }
}
