﻿using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile
{
    public interface ICompanyTrackerRepository
    {
        IEnumerable<CompanyTrackerViewModel> GetAll(Guid ProjectId, int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted);
        ProjectInformationViewModel GetProjectInfo(Guid ProjectId);
        CompanyTrackerViewModel GetById(Guid MapId);
    }
}
