﻿
CREATE procedure [dbo].[SApp_GetQualitativeNoteByID]    
(@Id uniqueidentifier,    
@ProjectId uniqueidentifier)    
as    
/*================================================================================    
Procedure Name: SApp_GetQualitativeNoteByID   
Author: Harshal    
Create date: 07/05/2019    
Description: Get Qualitative note for perticular id and ProjectId   
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
07/05/2019  Harshal   Initial Version    
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    
    
    
SELECT TOP 1 [QualitativeNote].Id,    
QualitativeId,    
ProjectId,    
AuthorRemark,    
ApproverRemark,    
QualitativeStatusID,
[QualitativeStatus].QualitativeStatusName As QualitativeStatusName    
FROM [dbo].[QualitativeNote]    
INNER JOIN [dbo].QualitativeStatus ON [QualitativeNote].QualitativeStatusID= QualitativeNote.Id 
WHERE QualitativeId = @Id AND ProjectId = @ProjectId  
ORDER BY CASE WHEN [QualitativeNote].ModifiedOn IS NULL THEN [QualitativeNote].CreatedOn ELSE [QualitativeNote].ModifiedOn END  
END