﻿      
      
      
CREATE procedure [dbo].[SApp_GetTrendKeywordbyID]      
(@TrendId uniqueidentifier,      
@IsDeleted bit=0)      
as      
/*================================================================================      
Procedure Name: SApp_GetTrendKeywordbyID      
Author: Gopi      
Create date: 04/24/2019      
Description: Get Trend keywords TrendID      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
04/24/2019 Gopi   Initial Version      
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
    
SELECT TKEY.ID,TKEY.TRENDKEYWORD,TKEY.TRENDID,T.TRENDNAME
FROM TRENDKEYWORDS TKEY
INNER JOIN TREND T
ON TKEY.TRENDID=T.ID
WHERE (TKEY.TRENDID=@TRENDID)
AND (TKEY.ISDELETED = 0)  
ORDER BY TKEY.TrendKeyWord   
        
END