﻿

CREATE PROCEDURE [dbo].[SApp_GetCompanyBalanceSheetDataByCompanyID]             
 @CompanyID uniqueidentifier = null,         
 @includeDeleted bit = 0             
AS
/*================================================================================  
Procedure Name: SApp_GetCompanyBalanceSheetDataByCompanyID  
Author: Harshal 
Create date: 08/09/2019  
Description:Getting Data From Table WRT CompanyId 
Change History  
Date        Developer     Reason  
__________  ____________  ______________________________________________________  
08/09/2019  Harshal   Initial Version  
================================================================================*/          
BEGIN         
 SET NOCOUNT ON;          
 SELECT  Id,
CompanyId,
CurrencyId,
ValueConversionId,
CompanyName,
Currency,
Units,
Year,
Assets_,
CashAndCashEquivalents,
ShortTermInvestments,
AccountsReceivables,
Inventories,
CurrentTaxAssets,
OtherCurrentAssets,
AssetsHeldForSale,
TotalCurrentAssets,
LongTermInvestments,
PropertyPlantAndEquipment,
IntangibleAssets,
Goodwill,
DeferredTaxAssets,
OtherNoncurrentAssets,
TotalAssets,
LiabilitiesAndEquity_,
ShortTermDebt,
TradeAccountsPayable,
DividendsPayable,
IncomeTaxesPayable,
AccruedExpenses,
DeferredRevenuesShortTerm,
OtherCurrentLiabilities,
LiabilitiesHeldForSale,
TotalCurrentLiabilities,
LongTermDebt,
PensionBenefitObligations,
PostretirementBenefitObligations,
DeferredTaxLiabilities,
OtherTaxesPayable,
DeferredRevenuesLongTerm,
OtherNoncurrentLliabilities,
TotalLiabilities,
CommitmentsAndContingencies_,
PreferredStock,
CommonStock,
Debentures,
AdditionalPaidInCapital,
TresuryStock,
RetainedEarnings,
AccumulatedOtherComprehensiveLoss,
TotalShareholdersEquity,
MinorityInterest,
TotalEquity,
TotalLiabilitiesAndEquity

FROM [dbo].[CompanyBalanceSheet]        
  
  WHERE ([CompanyBalanceSheet].CompanyId = @CompanyID)              
 AND (@includeDeleted = 0 or [CompanyBalanceSheet].IsDeleted = 0)          
END