﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public class CompanyProductQueries
    {
        public static string StoreProcSelectAll = "SApp_GetCompanyProductByCompanyID";
        public static string StoreProcInsert = "SApp_InsertCompanyProduct";
        public static string StoreProcUpdate = "SApp_UpdateCompanyProduct";
    }
}
