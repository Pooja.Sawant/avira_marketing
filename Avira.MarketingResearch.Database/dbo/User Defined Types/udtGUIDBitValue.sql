﻿CREATE TYPE [dbo].[udtGUIDBitValue] AS TABLE (
    [ID]       UNIQUEIDENTIFIER NOT NULL,
    [bitValue] BIT              NOT NULL);

