﻿  
  
CREATE PROCEDURE [dbo].[SApp_GetBoardIndustries]  
AS  
/*================================================================================  
Procedure Name: [SApp_GetBoardIndustries]  
Author: Sai Krishna  
Create date: 10/05/2019  
Description: Get list of Parent Industries from table   
Change History  
Date        Developer     Reason  
__________  ____________  ______________________________________________________  
10/05/2019  Sai krishna   Initial Version  
================================================================================*/  
  
BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
declare @SegmentId uniqueidentifier;
select @SegmentId = Id from Segment
where SegmentName = 'End User';


  SELECT ind.Id as BoardIndustryId, 
	ind.SubSegmentName as BoardIndustryName
  FROM [SubSegment] as ind
where (ind.ParentId is NULL OR ind.ParentId in (Select Id FROM SubSegment))
	AND ind.SegmentId = @SegmentId
	AND (ind.IsDeleted = 0)
  order by ind.SubSegmentName asc

 END