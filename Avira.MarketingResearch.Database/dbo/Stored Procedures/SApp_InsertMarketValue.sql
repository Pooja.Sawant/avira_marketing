﻿              
CREATE PROCEDURE [dbo].[SApp_InsertMarketValue]            
AS            
/*================================================================================                
Procedure Name: SApp_InsertMarketValue               
Change History                
Date  Developer  Reason                
__________ ____________ ______________________________________________________                
06/04/2019 Gopi   Initial Version                
================================================================================*/             
SET NOCOUNT ON;              
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    
Declare @ProjectId uniqueidentifier        
        
--set @MarketId='B336F772-747D-4CD2-B264-7F0FE1302263'        
        
declare @ErrorMsg NVARCHAR(2048),@CreatedOn Datetime, @UserCreatedById uniqueidentifier;

BEGIN               
BEGIN TRY
----------Deleting Existing Records-----------
SELECT TOP 1 @ProjectId = ProjectId,
@UserCreatedById = UserCreatedById
FROM StagingMarketSizing
DELETE FROM MarketValueSegmentMapping WHERE ProjectId = @ProjectId;
DELETE FROM MarketValue WHERE ProjectId = @ProjectId;
-----------------------------------
CREATE table #MV_SegMap(StagingId uniqueidentifier, 
						MarketValueID uniqueidentifier, 
						[Year] int);

;with cte_2018 as(
 Select *,  TRY_PARSE(CASE WHEN ISNUMERIC(Value_2018) = 1 THEN Value_2018 ELSE '0' END AS decimal(18,4)) as ASP,          
  TRY_PARSE(CASE WHEN ISNUMERIC(Value_2018) = 1 THEN Value_2018 ELSE '0' END AS decimal(18,4)) as Value,
  TRY_PARSE(CASE WHEN ISNUMERIC(Volume_2018) = 1 THEN Volume_2018 ELSE '0' END AS decimal(18,4))  as Volume,
TRY_PARSE(CASE WHEN ISNUMERIC(OranicGrowth_2018) = 1 THEN OranicGrowth_2018 ELSE '0' END AS decimal(18,4))  as OranicGrowth, 
  TRY_PARSE(CASE WHEN ISNUMERIC(TrendsGrowth_2018) = 1 THEN TrendsGrowth_2018 ELSE '0' END AS decimal(18,4)) as TrendsGrowth,            
  CAST(ValueConversionId AS uniqueidentifier) as ValConversionId,          
     2018 AS Year,
	  Source1_2018 as Source1, 
	  Source2_2018 as Source2, 
	  Source3_2018 as Source3, 
	  Source4_2018 as Source4, 
	  Source5_2018 as Source5,   
	  Rationale_2018 as Rationale                
 from StagingMarketSizing          
)          
insert into MarketValue          
(          
 [Id],      
 [ProjectId],        
 [MarketId],          
 [RegionId],      
 [CountryId],           
 [ValueConversionId],          
 [CurrencyId], 
 [VolumeUnitId],
 [ASPUnitId],                    
 [Value],          
 [Volume],          
 [AvgSalePrice],          
 [GrowthRatePercentage],          
 [TrendGrowthRatePercentage],  
 [Source1],
 [Source2],
 [Source3],
 [Source4],
 [Source5],
 [Rationale],          
 [Amount],          
 [Year], 
 [CAGR_3Years],
 [CAGR_5Years], 
 [TotalCAGR],          
 [GraphOptions],
 [StagingId],
 [CreatedOn],          
 [UserCreatedById],          
 [IsDeleted]          
 )          
 output inserted.[StagingId], inserted.ID, inserted.[Year]
 into #MV_SegMap  
 select NEWID(),       
 ProjectId,       
 MarketId,          
 RegionId,        
 CountryId,         
 ValConversionId,          
 CurrencyId,   
 VolumeUnitId,
 ASPUnitId,               
 dbo.fun_UnitConversion(ValConversionId, Value) as Value,          
 dbo.fun_UnitConversion(VolumeUnitId, Volume) as Volume,          
 dbo.fun_UnitConversion(ASPUnitId, ASP) as ASP,          
 OranicGrowth,          
 TrendsGrowth,  
 Source1,
 Source2,
 Source3,
 Source4,
 Source5,  
 Rationale,         
 100,          
 Year,
 TRY_PARSE(CASE WHEN ISNUMERIC(CAGR_3Years) = 1 THEN CAGR_3Years ELSE '0' END AS decimal(18,4)),
 TRY_PARSE(CASE WHEN ISNUMERIC(CAGR_5Years) = 1 THEN CAGR_5Years ELSE '0' END AS decimal(18,4)),
 TRY_PARSE(CASE WHEN ISNUMERIC(TotalCAGR) = 1 THEN TotalCAGR ELSE '0' END AS decimal(18,4)),           
 'chart', 
 cte_2018.Id,
 GETDATE(),          
 UserCreatedById,  
 0          
 from cte_2018;
        
;with cte_2019 as(          
 Select *, TRY_PARSE(CASE WHEN ISNUMERIC(ASP_2019) = 1 THEN ASP_2019 ELSE '0' END AS decimal(18,4)) as ASP,          
  TRY_PARSE(CASE WHEN ISNUMERIC(Value_2019) = 1 THEN Value_2019 ELSE '0' END AS decimal(18,4)) as Value,          
  TRY_PARSE(CASE WHEN ISNUMERIC(Volume_2019) = 1 THEN Volume_2019 ELSE '0' END AS decimal(18,4)) as Volume,          
  TRY_PARSE(CASE WHEN ISNUMERIC(OranicGrowth_2019) = 1 THEN OranicGrowth_2019 ELSE '0' END AS decimal(19,4)) as OranicGrowth,          
  TRY_PARSE(CASE WHEN ISNUMERIC(TrendsGrowth_2019) = 1 THEN TrendsGrowth_2019 ELSE '0' END AS decimal(19,4)) as TrendsGrowth,          
  CAST(ValueConversionId AS uniqueidentifier) as ValConversionId,          
     2019 AS Year,
	  Source1_2019 as Source1, 
	  Source2_2019 as Source2, 
	  Source3_2019 as Source3, 
	  Source4_2019 as Source4, 
	  Source5_2019 as Source5,   
	  Rationale_2019 as Rationale                
 from StagingMarketSizing          
)          
insert into MarketValue          
(          
 [Id],      
 [ProjectId],        
 [MarketId],          
 [RegionId],      
 [CountryId],           
 [ValueConversionId],          
 [CurrencyId], 
 [VolumeUnitId],
 [ASPUnitId],                    
 [Value],          
 [Volume],          
 [AvgSalePrice],          
 [GrowthRatePercentage],          
 [TrendGrowthRatePercentage],  
 [Source1],
 [Source2],
 [Source3],
 [Source4],
 [Source5],
 [Rationale],          
 [Amount],          
 [Year], 
 [CAGR_3Years],
 [CAGR_5Years], 
 [TotalCAGR],          
 [GraphOptions],
 [StagingId],
 [CreatedOn],          
 [UserCreatedById],          
 [IsDeleted]          
 )          
 output inserted.[StagingId], inserted.ID, inserted.[Year]
 into #MV_SegMap  
 select NEWID(),       
 ProjectId,       
 MarketId,          
 RegionId,        
 CountryId,         
 ValConversionId,          
 CurrencyId,   
 VolumeUnitId,
 ASPUnitId,               
 dbo.fun_UnitConversion(ValConversionId, Value) as Value,          
 dbo.fun_UnitConversion(VolumeUnitId, Volume) as Volume,          
 dbo.fun_UnitConversion(ASPUnitId, ASP) as ASP,          
 OranicGrowth,          
 TrendsGrowth,  
 Source1,
 Source2,
 Source3,
 Source4,
 Source5,  
 Rationale,         
 100,          
 Year,
 TRY_PARSE(CASE WHEN ISNUMERIC(CAGR_3Years) = 1 THEN CAGR_3Years ELSE '0' END AS decimal(18,4)),
 TRY_PARSE(CASE WHEN ISNUMERIC(CAGR_5Years) = 1 THEN CAGR_5Years ELSE '0' END AS decimal(18,4)),
 TRY_PARSE(CASE WHEN ISNUMERIC(TotalCAGR) = 1 THEN TotalCAGR ELSE '0' END AS decimal(18,4)),           
 'chart', 
 cte_2019.Id,
 GETDATE(),          
 UserCreatedById,  
 0          
 from cte_2019;
        
;with cte_2020 as(          
 Select *, TRY_PARSE(CASE WHEN ISNUMERIC(ASP_2020) = 1 THEN ASP_2020 ELSE '0' END AS decimal(18,4)) as ASP,          
  TRY_PARSE(CASE WHEN ISNUMERIC(Value_2020) = 1 THEN Value_2020 ELSE '0' END AS decimal(18,4)) as Value,          
  TRY_PARSE(CASE WHEN ISNUMERIC(Volume_2020) = 1 THEN Volume_2020 ELSE '0' END AS decimal(18,4)) as Volume,          
  TRY_PARSE(CASE WHEN ISNUMERIC(OranicGrowth_2020) = 1 THEN OranicGrowth_2020 ELSE '0' END AS decimal(19,4)) as OranicGrowth,          
  TRY_PARSE(CASE WHEN ISNUMERIC(TrendsGrowth_2020) = 1 THEN TrendsGrowth_2020 ELSE '0' END AS decimal(19,4)) as TrendsGrowth,          
  CAST(ValueConversionId AS uniqueidentifier) as ValConversionId,          
  2020 AS Year,
	  Source1_2020 as Source1, 
	  Source2_2020 as Source2, 
	  Source3_2020 as Source3, 
	  Source4_2020 as Source4, 
	  Source5_2020 as Source5,   
	  Rationale_2020 as Rationale                
 from StagingMarketSizing          
)          
insert into MarketValue          
(          
 [Id],      
 [ProjectId],        
 [MarketId],          
 [RegionId],      
 [CountryId],           
 [ValueConversionId],          
 [CurrencyId], 
 [VolumeUnitId],
 [ASPUnitId],                    
 [Value],          
 [Volume],          
 [AvgSalePrice],          
 [GrowthRatePercentage],          
 [TrendGrowthRatePercentage],  
 [Source1],
 [Source2],
 [Source3],
 [Source4],
 [Source5],
 [Rationale],          
 [Amount],          
 [Year], 
 [CAGR_3Years],
 [CAGR_5Years], 
 [TotalCAGR],          
 [GraphOptions],
 [StagingId],
 [CreatedOn],          
 [UserCreatedById],          
 [IsDeleted]          
 )          
 output inserted.[StagingId], inserted.ID, inserted.[Year]
 into #MV_SegMap  
 select NEWID(),       
 ProjectId,       
 MarketId,          
 RegionId,        
 CountryId,         
 ValConversionId,          
 CurrencyId,   
 VolumeUnitId,
 ASPUnitId,               
 dbo.fun_UnitConversion(ValConversionId, Value) as Value,          
 dbo.fun_UnitConversion(VolumeUnitId, Volume) as Volume,          
 dbo.fun_UnitConversion(ASPUnitId, ASP) as ASP,          
 OranicGrowth,          
 TrendsGrowth,  
 Source1,
 Source2,
 Source3,
 Source4,
 Source5,  
 Rationale,         
 100,          
 Year,
 TRY_PARSE(CASE WHEN ISNUMERIC(CAGR_3Years) = 1 THEN CAGR_3Years ELSE '0' END AS decimal(18,4)),
 TRY_PARSE(CASE WHEN ISNUMERIC(CAGR_5Years) = 1 THEN CAGR_5Years ELSE '0' END AS decimal(18,4)),
 TRY_PARSE(CASE WHEN ISNUMERIC(TotalCAGR) = 1 THEN TotalCAGR ELSE '0' END AS decimal(18,4)),           
 'chart', 
 cte_2020.Id,
 GETDATE(),          
 UserCreatedById,  
 0          
 from cte_2020;
        
;with cte_2021 as(          
 Select *, 
 TRY_PARSE(CASE WHEN ISNUMERIC(ASP_2021) = 1 THEN ASP_2021 ELSE '0' END AS decimal(18,4)) as ASP,          
  TRY_PARSE(CASE WHEN ISNUMERIC(Value_2021) = 1 THEN Value_2021 ELSE '0' END AS decimal(18,4)) as Value,          
  TRY_PARSE(CASE WHEN ISNUMERIC(Volume_2021) = 1 THEN Volume_2021 ELSE '0' END AS decimal(18,4)) as Volume,          
  TRY_PARSE(CASE WHEN ISNUMERIC(OranicGrowth_2021) = 1 THEN OranicGrowth_2021 ELSE '0' END AS decimal(19,4)) as OranicGrowth,          
  TRY_PARSE(CASE WHEN ISNUMERIC(TrendsGrowth_2021) = 1 THEN TrendsGrowth_2021 ELSE '0' END AS decimal(19,4)) as TrendsGrowth,          
  CAST(ValueConversionId AS uniqueidentifier) as ValConversionId,          
     2021 AS Year,
	  Source1_2021 as Source1, 
	  Source2_2021 as Source2, 
	  Source3_2021 as Source3, 
	  Source4_2021 as Source4, 
	  Source5_2021 as Source5,   
	  Rationale_2021 as Rationale                
 from StagingMarketSizing          
)          
insert into MarketValue          
(          
 [Id],      
 [ProjectId],        
 [MarketId],          
 [RegionId],      
 [CountryId],           
 [ValueConversionId],          
 [CurrencyId], 
 [VolumeUnitId],
 [ASPUnitId],                    
 [Value],          
 [Volume],          
 [AvgSalePrice],          
 [GrowthRatePercentage],          
 [TrendGrowthRatePercentage],  
 [Source1],
 [Source2],
 [Source3],
 [Source4],
 [Source5],
 [Rationale],          
 [Amount],          
 [Year], 
 [CAGR_3Years],
 [CAGR_5Years], 
 [TotalCAGR],          
 [GraphOptions],
 [StagingId],
 [CreatedOn],          
 [UserCreatedById],          
 [IsDeleted]          
 )          
 output inserted.[StagingId], inserted.ID, inserted.[Year]
 into #MV_SegMap  
 select NEWID(),       
 ProjectId,       
 MarketId,          
 RegionId,        
 CountryId,         
 ValConversionId,          
 CurrencyId,   
 VolumeUnitId,
 ASPUnitId,               
 dbo.fun_UnitConversion(ValConversionId, Value) as Value,          
 dbo.fun_UnitConversion(VolumeUnitId, Volume) as Volume,          
 dbo.fun_UnitConversion(ASPUnitId, ASP) as ASP,          
 OranicGrowth,          
 TrendsGrowth,  
 Source1,
 Source2,
 Source3,
 Source4,
 Source5,  
 Rationale,         
 100,          
 Year,
 TRY_PARSE(CASE WHEN ISNUMERIC(CAGR_3Years) = 1 THEN CAGR_3Years ELSE '0' END AS decimal(18,4)),
 TRY_PARSE(CASE WHEN ISNUMERIC(CAGR_5Years) = 1 THEN CAGR_5Years ELSE '0' END AS decimal(18,4)),
 TRY_PARSE(CASE WHEN ISNUMERIC(TotalCAGR) = 1 THEN TotalCAGR ELSE '0' END AS decimal(18,4)),           
 'chart', 
 cte_2021.Id,
 GETDATE(),          
 UserCreatedById,  
 0          
 from cte_2021;
       
;with cte_2022 as(          
 Select *,
 TRY_PARSE(CASE WHEN ISNUMERIC(ASP_2022) = 1 THEN ASP_2022 ELSE '0' END AS decimal(18,4)) as ASP,          
  TRY_PARSE(CASE WHEN ISNUMERIC(Value_2022) = 1 THEN Value_2022 ELSE '0' END AS decimal(18,4)) as Value,          
  TRY_PARSE(CASE WHEN ISNUMERIC(Volume_2022) = 1 THEN Volume_2022 ELSE '0' END AS decimal(18,4)) as Volume,          
  TRY_PARSE(CASE WHEN ISNUMERIC(OranicGrowth_2022) = 1 THEN OranicGrowth_2022 ELSE '0' END AS decimal(19,4)) as OranicGrowth,          
  TRY_PARSE(CASE WHEN ISNUMERIC(TrendsGrowth_2022) = 1 THEN TrendsGrowth_2022 ELSE '0' END AS decimal(19,4)) as TrendsGrowth,          
  CAST(ValueConversionId AS uniqueidentifier) as ValConversionId,          
  2022 AS Year,
	  Source1_2022 as Source1, 
	  Source2_2022 as Source2, 
	  Source3_2022 as Source3, 
	  Source4_2022 as Source4, 
	  Source5_2022 as Source5,   
	  Rationale_2022 as Rationale                
 from StagingMarketSizing          
)          
insert into MarketValue          
(          
 [Id],      
 [ProjectId],        
 [MarketId],          
 [RegionId],      
 [CountryId],           
 [ValueConversionId],          
 [CurrencyId], 
 [VolumeUnitId],
 [ASPUnitId],                    
 [Value],          
 [Volume],          
 [AvgSalePrice],          
 [GrowthRatePercentage],          
 [TrendGrowthRatePercentage],  
 [Source1],
 [Source2],
 [Source3],
 [Source4],
 [Source5],
 [Rationale],          
 [Amount],          
 [Year], 
 [CAGR_3Years],
 [CAGR_5Years], 
 [TotalCAGR],          
 [GraphOptions],
 [StagingId],
 [CreatedOn],          
 [UserCreatedById],          
 [IsDeleted]          
 )          
 output inserted.[StagingId], inserted.ID, inserted.[Year]
 into #MV_SegMap  
 select NEWID(),       
 ProjectId,       
 MarketId,          
 RegionId,        
 CountryId,         
 ValConversionId,          
 CurrencyId,   
 VolumeUnitId,
 ASPUnitId,               
 dbo.fun_UnitConversion(ValConversionId, Value) as Value,          
 dbo.fun_UnitConversion(VolumeUnitId, Volume) as Volume,          
 dbo.fun_UnitConversion(ASPUnitId, ASP) as ASP,          
 OranicGrowth,          
 TrendsGrowth,  
 Source1,
 Source2,
 Source3,
 Source4,
 Source5,  
 Rationale,         
 100,          
 Year,
 TRY_PARSE(CASE WHEN ISNUMERIC(CAGR_3Years) = 1 THEN CAGR_3Years ELSE '0' END AS decimal(18,4)),
 TRY_PARSE(CASE WHEN ISNUMERIC(CAGR_5Years) = 1 THEN CAGR_5Years ELSE '0' END AS decimal(18,4)),
 TRY_PARSE(CASE WHEN ISNUMERIC(TotalCAGR) = 1 THEN TotalCAGR ELSE '0' END AS decimal(18,4)),           
 'chart', 
 cte_2022.Id,
 GETDATE(),          
 UserCreatedById,  
 0          
 from cte_2022;
 
 ;with cte_2023 as(          
 Select *, 
  TRY_PARSE(CASE WHEN ISNUMERIC(ASP_2023) = 1 THEN ASP_2023 ELSE '0' END AS decimal(18,4)) as ASP,          
  TRY_PARSE(CASE WHEN ISNUMERIC(Value_2023) = 1 THEN Value_2023 ELSE '0' END AS decimal(18,4)) as Value,          
  TRY_PARSE(CASE WHEN ISNUMERIC(Volume_2023) = 1 THEN Volume_2023 ELSE '0' END AS decimal(18,4)) as Volume,          
  TRY_PARSE(CASE WHEN ISNUMERIC(OranicGrowth_2023) = 1 THEN OranicGrowth_2023 ELSE '0' END AS decimal(19,4)) as OranicGrowth,          
  TRY_PARSE(CASE WHEN ISNUMERIC(TrendsGrowth_2023) = 1 THEN TrendsGrowth_2023 ELSE '0' END AS decimal(19,4)) as TrendsGrowth,            
  CAST(ValueConversionId AS uniqueidentifier) as ValConversionId,          
 2023 AS Year,
	  Source1_2023 as Source1, 
	  Source2_2023 as Source2, 
	  Source3_2023 as Source3, 
	  Source4_2023 as Source4, 
	  Source5_2023 as Source5,   
	  Rationale_2023 as Rationale                
 from StagingMarketSizing          
)          
insert into MarketValue          
(          
 [Id],      
 [ProjectId],        
 [MarketId],          
 [RegionId],      
 [CountryId],           
 [ValueConversionId],          
 [CurrencyId], 
 [VolumeUnitId],
 [ASPUnitId],                    
 [Value],          
 [Volume],          
 [AvgSalePrice],          
 [GrowthRatePercentage],          
 [TrendGrowthRatePercentage],  
 [Source1],
 [Source2],
 [Source3],
 [Source4],
 [Source5],
 [Rationale],          
 [Amount],          
 [Year], 
 [CAGR_3Years],
 [CAGR_5Years], 
 [TotalCAGR],          
 [GraphOptions],
 [StagingId],
 [CreatedOn],          
 [UserCreatedById],          
 [IsDeleted]          
 )          
 output inserted.[StagingId], inserted.ID, inserted.[Year]
 into #MV_SegMap  
 select NEWID(),       
 ProjectId,       
 MarketId,          
 RegionId,        
 CountryId,         
 ValConversionId,          
 CurrencyId,   
 VolumeUnitId,
 ASPUnitId,               
 dbo.fun_UnitConversion(ValConversionId, Value) as Value,          
 dbo.fun_UnitConversion(VolumeUnitId, Volume) as Volume,          
 dbo.fun_UnitConversion(ASPUnitId, ASP) as ASP,          
 OranicGrowth,          
 TrendsGrowth,  
 Source1,
 Source2,
 Source3,
 Source4,
 Source5,  
 Rationale,         
 100,          
 Year,
 TRY_PARSE(CASE WHEN ISNUMERIC(CAGR_3Years) = 1 THEN CAGR_3Years ELSE '0' END AS decimal(18,4)),
 TRY_PARSE(CASE WHEN ISNUMERIC(CAGR_5Years) = 1 THEN CAGR_5Years ELSE '0' END AS decimal(18,4)),
 TRY_PARSE(CASE WHEN ISNUMERIC(TotalCAGR) = 1 THEN TotalCAGR ELSE '0' END AS decimal(18,4)),           
 'chart', 
 cte_2023.Id,
 GETDATE(),          
 UserCreatedById,  
 0          
 from cte_2023;
 
 ;with cte_2024 as(          
 Select *, 
 TRY_PARSE(CASE WHEN ISNUMERIC(ASP_2024) = 1 THEN ASP_2024 ELSE '0' END AS decimal(18,4)) as ASP,          
  TRY_PARSE(CASE WHEN ISNUMERIC(Value_2024) = 1 THEN Value_2024 ELSE '0' END AS decimal(18,4)) as Value,          
  TRY_PARSE(CASE WHEN ISNUMERIC(Volume_2024) = 1 THEN Volume_2024 ELSE '0' END AS decimal(18,4)) as Volume,          
  TRY_PARSE(CASE WHEN ISNUMERIC(OranicGrowth_2024) = 1 THEN OranicGrowth_2024 ELSE '0' END AS decimal(19,4)) as OranicGrowth,          
  TRY_PARSE(CASE WHEN ISNUMERIC(TrendsGrowth_2024) = 1 THEN TrendsGrowth_2024 ELSE '0' END AS decimal(19,4)) as TrendsGrowth,         
  CAST(ValueConversionId AS uniqueidentifier) as ValConversionId,          
  2024 AS Year,
	  Source1_2024 as Source1, 
	  Source2_2024 as Source2, 
	  Source3_2024 as Source3, 
	  Source4_2024 as Source4, 
	  Source5_2024 as Source5,   
	  Rationale_2024 as Rationale                
 from StagingMarketSizing          
)          
insert into MarketValue          
(          
 [Id],      
 [ProjectId],        
 [MarketId],          
 [RegionId],      
 [CountryId],           
 [ValueConversionId],          
 [CurrencyId], 
 [VolumeUnitId],
 [ASPUnitId],                    
 [Value],          
 [Volume],          
 [AvgSalePrice],          
 [GrowthRatePercentage],          
 [TrendGrowthRatePercentage],  
 [Source1],
 [Source2],
 [Source3],
 [Source4],
 [Source5],
 [Rationale],          
 [Amount],          
 [Year], 
 [CAGR_3Years],
 [CAGR_5Years], 
 [TotalCAGR],          
 [GraphOptions],
 [StagingId],
 [CreatedOn],          
 [UserCreatedById],          
 [IsDeleted]          
 )          
 output inserted.[StagingId], inserted.ID, inserted.[Year]
 into #MV_SegMap  
 select NEWID(),       
 ProjectId,       
 MarketId,          
 RegionId,        
 CountryId,         
 ValConversionId,          
 CurrencyId,   
 VolumeUnitId,
 ASPUnitId,               
 dbo.fun_UnitConversion(ValConversionId, Value) as Value,          
 dbo.fun_UnitConversion(VolumeUnitId, Volume) as Volume,          
 dbo.fun_UnitConversion(ASPUnitId, ASP) as ASP,          
 OranicGrowth,          
 TrendsGrowth,  
 Source1,
 Source2,
 Source3,
 Source4,
 Source5,  
 Rationale,         
 100,          
 Year,
 TRY_PARSE(CASE WHEN ISNUMERIC(CAGR_3Years) = 1 THEN CAGR_3Years ELSE '0' END AS decimal(18,4)),
 TRY_PARSE(CASE WHEN ISNUMERIC(CAGR_5Years) = 1 THEN CAGR_5Years ELSE '0' END AS decimal(18,4)),
 TRY_PARSE(CASE WHEN ISNUMERIC(TotalCAGR) = 1 THEN TotalCAGR ELSE '0' END AS decimal(18,4)),           
 'chart', 
 cte_2024.Id,
 GETDATE(),          
 UserCreatedById,  
 0          
 from cte_2024;
 
 ;with cte_2025 as(          
 Select *, 
 TRY_PARSE(CASE WHEN ISNUMERIC(ASP_2025) = 1 THEN ASP_2025 ELSE '0' END AS decimal(18,4)) as ASP,          
  TRY_PARSE(CASE WHEN ISNUMERIC(Value_2025) = 1 THEN Value_2025 ELSE '0' END AS decimal(18,4)) as Value,          
  TRY_PARSE(CASE WHEN ISNUMERIC(Volume_2025) = 1 THEN Volume_2025 ELSE '0' END AS decimal(18,4)) as Volume,          
  TRY_PARSE(CASE WHEN ISNUMERIC(OranicGrowth_2025) = 1 THEN OranicGrowth_2025 ELSE '0' END AS decimal(19,4)) as OranicGrowth,          
  TRY_PARSE(CASE WHEN ISNUMERIC(TrendsGrowth_2025) = 1 THEN TrendsGrowth_2025 ELSE '0' END AS decimal(19,4)) as TrendsGrowth,            
  CAST(ValueConversionId AS uniqueidentifier) as ValConversionId,          
  2025 AS Year,
	  Source1_2025 as Source1, 
	  Source2_2025 as Source2, 
	  Source3_2025 as Source3, 
	  Source4_2025 as Source4, 
	  Source5_2025 as Source5,   
	  Rationale_2025 as Rationale                
 from StagingMarketSizing          
)          
insert into MarketValue          
(          
 [Id],      
 [ProjectId],        
 [MarketId],          
 [RegionId],      
 [CountryId],           
 [ValueConversionId],          
 [CurrencyId], 
 [VolumeUnitId],
 [ASPUnitId],                    
 [Value],          
 [Volume],          
 [AvgSalePrice],          
 [GrowthRatePercentage],          
 [TrendGrowthRatePercentage],  
 [Source1],
 [Source2],
 [Source3],
 [Source4],
 [Source5],
 [Rationale],          
 [Amount],          
 [Year], 
 [CAGR_3Years],
 [CAGR_5Years], 
 [TotalCAGR],          
 [GraphOptions],
 [StagingId],
 [CreatedOn],          
 [UserCreatedById],          
 [IsDeleted]          
 )          
 output inserted.[StagingId], inserted.ID, inserted.[Year]
 into #MV_SegMap  
 select NEWID(),       
 ProjectId,       
 MarketId,          
 RegionId,        
 CountryId,         
 ValConversionId,          
 CurrencyId,   
 VolumeUnitId,
 ASPUnitId,               
 dbo.fun_UnitConversion(ValConversionId, Value) as Value,          
 dbo.fun_UnitConversion(VolumeUnitId, Volume) as Volume,          
 dbo.fun_UnitConversion(ASPUnitId, ASP) as ASP,          
 OranicGrowth,          
 TrendsGrowth,  
 Source1,
 Source2,
 Source3,
 Source4,
 Source5,  
 Rationale,         
 100,          
 Year,
 TRY_PARSE(CASE WHEN ISNUMERIC(CAGR_3Years) = 1 THEN CAGR_3Years ELSE '0' END AS decimal(18,4)),
 TRY_PARSE(CASE WHEN ISNUMERIC(CAGR_5Years) = 1 THEN CAGR_5Years ELSE '0' END AS decimal(18,4)),
 TRY_PARSE(CASE WHEN ISNUMERIC(TotalCAGR) = 1 THEN TotalCAGR ELSE '0' END AS decimal(18,4)),           
 'chart', 
 cte_2025.Id,
 GETDATE(),          
 UserCreatedById,  
 0          
 from cte_2025;
 
 ;with cte_2026 as(          
 Select *, 
 TRY_PARSE(CASE WHEN ISNUMERIC(ASP_2026) = 1 THEN ASP_2026 ELSE '0' END AS decimal(18,4)) as ASP,          
  TRY_PARSE(CASE WHEN ISNUMERIC(Value_2026) = 1 THEN Value_2026 ELSE '0' END AS decimal(18,4)) as Value,          
  TRY_PARSE(CASE WHEN ISNUMERIC(Volume_2026) = 1 THEN Volume_2026 ELSE '0' END AS decimal(18,4)) as Volume,          
  TRY_PARSE(CASE WHEN ISNUMERIC(OranicGrowth_2026) = 1 THEN OranicGrowth_2026 ELSE '0' END AS decimal(19,4)) as OranicGrowth,          
  TRY_PARSE(CASE WHEN ISNUMERIC(TrendsGrowth_2026) = 1 THEN TrendsGrowth_2026 ELSE '0' END AS decimal(19,4)) as TrendsGrowth,             
  CAST(ValueConversionId AS uniqueidentifier) as ValConversionId,          
  2026 AS Year,
	  Source1_2026 as Source1, 
	  Source2_2026 as Source2, 
	  Source3_2026 as Source3, 
	  Source4_2026 as Source4, 
	  Source5_2026 as Source5,   
	  Rationale_2026 as Rationale                
 from StagingMarketSizing          
)          
insert into MarketValue          
(          
 [Id],      
 [ProjectId],        
 [MarketId],          
 [RegionId],      
 [CountryId],           
 [ValueConversionId],          
 [CurrencyId], 
 [VolumeUnitId],
 [ASPUnitId],                    
 [Value],          
 [Volume],          
 [AvgSalePrice],          
 [GrowthRatePercentage],          
 [TrendGrowthRatePercentage],  
 [Source1],
 [Source2],
 [Source3],
 [Source4],
 [Source5],
 [Rationale],          
 [Amount],          
 [Year], 
 [CAGR_3Years],
 [CAGR_5Years], 
 [TotalCAGR],          
 [GraphOptions],
 [StagingId],
 [CreatedOn],          
 [UserCreatedById],          
 [IsDeleted]          
 )          
 output inserted.[StagingId], inserted.ID, inserted.[Year]
 into #MV_SegMap  
 select NEWID(),       
 ProjectId,       
 MarketId,          
 RegionId,        
 CountryId,         
 ValConversionId,          
 CurrencyId,   
 VolumeUnitId,
 ASPUnitId,               
 dbo.fun_UnitConversion(ValConversionId, Value) as Value,          
 dbo.fun_UnitConversion(VolumeUnitId, Volume) as Volume,          
 dbo.fun_UnitConversion(ASPUnitId, ASP) as ASP,          
 OranicGrowth,          
 TrendsGrowth,  
 Source1,
 Source2,
 Source3,
 Source4,
 Source5,  
 Rationale,         
 100,          
 Year,
 TRY_PARSE(CASE WHEN ISNUMERIC(CAGR_3Years) = 1 THEN CAGR_3Years ELSE '0' END AS decimal(18,4)),
 TRY_PARSE(CASE WHEN ISNUMERIC(CAGR_5Years) = 1 THEN CAGR_5Years ELSE '0' END AS decimal(18,4)),
 TRY_PARSE(CASE WHEN ISNUMERIC(TotalCAGR) = 1 THEN TotalCAGR ELSE '0' END AS decimal(18,4)),           
 'chart', 
 cte_2026.Id,
 GETDATE(),          
 UserCreatedById,  
 0          
 from cte_2026;
 
 ;with cte_2027 as(          
 Select *, 
 TRY_PARSE(CASE WHEN ISNUMERIC(ASP_2027) = 1 THEN ASP_2027 ELSE '0' END AS decimal(18,4)) as ASP,          
  TRY_PARSE(CASE WHEN ISNUMERIC(Value_2027) = 1 THEN Value_2027 ELSE '0' END AS decimal(18,4)) as Value,          
  TRY_PARSE(CASE WHEN ISNUMERIC(Volume_2027) = 1 THEN Volume_2027 ELSE '0' END AS decimal(18,4)) as Volume,          
  TRY_PARSE(CASE WHEN ISNUMERIC(OranicGrowth_2027) = 1 THEN OranicGrowth_2027 ELSE '0' END AS decimal(19,4)) as OranicGrowth,          
  TRY_PARSE(CASE WHEN ISNUMERIC(TrendsGrowth_2027) = 1 THEN TrendsGrowth_2027 ELSE '0' END AS decimal(19,4)) as TrendsGrowth,            
  CAST(ValueConversionId AS uniqueidentifier) as ValConversionId,          
  2027 AS Year,
	  Source1_2027 as Source1, 
	  Source2_2027 as Source2, 
	  Source3_2027 as Source3, 
	  Source4_2027 as Source4, 
	  Source5_2027 as Source5,   
	  Rationale_2027 as Rationale                
 from StagingMarketSizing          
)          
insert into MarketValue          
(          
 [Id],      
 [ProjectId],        
 [MarketId],          
 [RegionId],      
 [CountryId],           
 [ValueConversionId],          
 [CurrencyId], 
 [VolumeUnitId],
 [ASPUnitId],                    
 [Value],          
 [Volume],          
 [AvgSalePrice],          
 [GrowthRatePercentage],          
 [TrendGrowthRatePercentage],  
 [Source1],
 [Source2],
 [Source3],
 [Source4],
 [Source5],
 [Rationale],          
 [Amount],          
 [Year], 
 [CAGR_3Years],
 [CAGR_5Years], 
 [TotalCAGR],          
 [GraphOptions],
 [StagingId],
 [CreatedOn],          
 [UserCreatedById],          
 [IsDeleted]          
 )          
 output inserted.[StagingId], inserted.ID, inserted.[Year]
 into #MV_SegMap  
 select NEWID(),       
 ProjectId,       
 MarketId,          
 RegionId,        
 CountryId,         
 ValConversionId,          
 CurrencyId,   
 VolumeUnitId,
 ASPUnitId,               
 dbo.fun_UnitConversion(ValConversionId, Value) as Value,          
 dbo.fun_UnitConversion(VolumeUnitId, Volume) as Volume,          
 dbo.fun_UnitConversion(ASPUnitId, ASP) as ASP,          
 OranicGrowth,          
 TrendsGrowth,  
 Source1,
 Source2,
 Source3,
 Source4,
 Source5,  
 Rationale,         
 100,          
 Year,
 TRY_PARSE(CASE WHEN ISNUMERIC(CAGR_3Years) = 1 THEN CAGR_3Years ELSE '0' END AS decimal(18,4)),
 TRY_PARSE(CASE WHEN ISNUMERIC(CAGR_5Years) = 1 THEN CAGR_5Years ELSE '0' END AS decimal(18,4)),
 TRY_PARSE(CASE WHEN ISNUMERIC(TotalCAGR) = 1 THEN TotalCAGR ELSE '0' END AS decimal(18,4)),           
 'chart', 
 cte_2027.Id,
 GETDATE(),          
 UserCreatedById,  
 0          
 from cte_2027;
 
 ;with cte_2028 as(          
 Select *, 
 TRY_PARSE(CASE WHEN ISNUMERIC(ASP_2028) = 1 THEN ASP_2028 ELSE '0' END AS decimal(18,4)) as ASP,          
  TRY_PARSE(CASE WHEN ISNUMERIC(Value_2028) = 1 THEN Value_2028 ELSE '0' END AS decimal(18,4)) as Value,          
  TRY_PARSE(CASE WHEN ISNUMERIC(Volume_2028) = 1 THEN Volume_2028 ELSE '0' END AS decimal(18,4)) as Volume,          
  TRY_PARSE(CASE WHEN ISNUMERIC(OranicGrowth_2028) = 1 THEN OranicGrowth_2028 ELSE '0' END AS decimal(19,4)) as OranicGrowth,          
  TRY_PARSE(CASE WHEN ISNUMERIC(TrendsGrowth_2028) = 1 THEN TrendsGrowth_2028 ELSE '0' END AS decimal(19,4)) as TrendsGrowth,            
  CAST(ValueConversionId AS uniqueidentifier) as ValConversionId,          
  2028 AS Year,
	  Source1_2028 as Source1, 
	  Source2_2028 as Source2, 
	  Source3_2028 as Source3, 
	  Source4_2028 as Source4, 
	  Source5_2028 as Source5,   
	  Rationale_2028 as Rationale                
 from StagingMarketSizing          
)          
insert into MarketValue          
(          
 [Id],      
 [ProjectId],        
 [MarketId],          
 [RegionId],      
 [CountryId],           
 [ValueConversionId],          
 [CurrencyId], 
 [VolumeUnitId],
 [ASPUnitId],                    
 [Value],          
 [Volume],          
 [AvgSalePrice],          
 [GrowthRatePercentage],          
 [TrendGrowthRatePercentage],  
 [Source1],
 [Source2],
 [Source3],
 [Source4],
 [Source5],
 [Rationale],          
 [Amount],          
 [Year], 
 [CAGR_3Years],
 [CAGR_5Years], 
 [TotalCAGR],          
 [GraphOptions],
 [StagingId],
 [CreatedOn],          
 [UserCreatedById],          
 [IsDeleted]          
 )          
 output inserted.[StagingId], inserted.ID, inserted.[Year]
 into #MV_SegMap  
 select NEWID(),       
 ProjectId,       
 MarketId,          
 RegionId,        
 CountryId,         
 ValConversionId,          
 CurrencyId,   
 VolumeUnitId,
 ASPUnitId,               
 dbo.fun_UnitConversion(ValConversionId, Value) as Value,          
 dbo.fun_UnitConversion(VolumeUnitId, Volume) as Volume,          
 dbo.fun_UnitConversion(ASPUnitId, ASP) as ASP,          
 OranicGrowth,          
 TrendsGrowth,  
 Source1,
 Source2,
 Source3,
 Source4,
 Source5,  
 Rationale,         
 100,          
 Year,
 TRY_PARSE(CASE WHEN ISNUMERIC(CAGR_3Years) = 1 THEN CAGR_3Years ELSE '0' END AS decimal(18,4)),
 TRY_PARSE(CASE WHEN ISNUMERIC(CAGR_5Years) = 1 THEN CAGR_5Years ELSE '0' END AS decimal(18,4)),
 TRY_PARSE(CASE WHEN ISNUMERIC(TotalCAGR) = 1 THEN TotalCAGR ELSE '0' END AS decimal(18,4)),           
 'chart', 
 cte_2028.Id,
 GETDATE(),          
 UserCreatedById,  
 0          
 from cte_2028;
 
;with cte_segmap as (select distinct j.[SegmentName] 
from [dbo].[StagingMarketSizing] staging
cross apply OPENJSON(staging.SegmentMapJson)            
    WITH ([SegmentName] [nvarchar](255) ,            
   [SubSegment] [nvarchar](255)) j

where j.[SegmentName] != ''
and not exists(select 1 from Segment where  Segment.SegmentName = j.[SegmentName])
)
insert into Segment(Id,
SegmentName,
Description,
CreatedOn,
UserCreatedById,
IsDeleted)
select newid() as ID,
cte_segmap.[SegmentName] as SegmentName,
cte_segmap.[SegmentName] as Description,
getdate() as CreatedOn,
@UserCreatedById as UserCreatedById,
0 as IsDeleted
from cte_segmap;

;with cte_segmap as (select distinct Segment.Id as SegmentId,
j.[SubSegment]
from [dbo].[StagingMarketSizing] staging
cross apply OPENJSON(staging.SegmentMapJson)            
    WITH (            
  [SegmentName] [nvarchar](255) ,            
   [SubSegment] [nvarchar](255)) j
inner join Segment
on j.[SegmentName] = Segment.SegmentName
where j.[SubSegment] != ''
and not exists(select 1 from SubSegment where  SubSegment.SubSegmentName = j.[SubSegment])
)
insert into SubSegment(Id,
SegmentId,
SubSegmentName,
Description,
CreatedOn,
UserCreatedById,
IsDeleted)
select NEWID(),
cte_segmap.SegmentId,
cte_segmap.[SubSegment],
cte_segmap.[SubSegment],
getdate() as CreatedOn,
@UserCreatedById as UserCreatedById,
0 as IsDeleted
from cte_segmap;

insert into dbo.[MarketValueSegmentMapping](Id,
ProjectId,
MarketValueId,
SegmentId,
SubSegmentId,
CreatedOn,
UserCreatedById,
IsDeleted)
select NEWID() as id,
staging.ProjectId,
map.MarketValueID,
Segment.Id as SegmentId,
SubSegment.Id as SubSegmentId,
getdate() as CreatedOn,
@UserCreatedById as UserCreatedById,
0 as IsDeleted
from [dbo].[StagingMarketSizing] staging
cross apply OPENJSON(staging.SegmentMapJson)            
    WITH (            
  [SegmentName] [nvarchar](255) ,            
   [SubSegment] [nvarchar](255)) j
inner join Segment 
on j.[SegmentName] = Segment.segmentname 
inner join SubSegment
on j.[SubSegment] = SubSegment.SubSegmentName and Segment.Id = [SubSegment].SegmentId
inner join #MV_SegMap map
on staging.Id = map.StagingId



exec [dbo].[SApp_GenerateMarketGraphData]  
@ProjectID = @ProjectID

 ---Update Statics--
 UPDATE STATISTICS [MarketValue] WITH FULLSCAN;
 UPDATE STATISTICS [SubSegment] WITH FULLSCAN;
  UPDATE STATISTICS [Segment] WITH FULLSCAN;
 ---End Statics----

SELECT StoredProcedureName ='SApp_InsertMarketValue',Message =@ErrorMsg,Success = 1;               
END TRY                
BEGIN CATCH                
    -- Execute the error retrieval routine.                
 DECLARE @ErrorNumber int;                
 DECLARE @ErrorSeverity int;                
 DECLARE @ErrorProcedure varchar(100);                
 DECLARE @ErrorLine int;                
 DECLARE @ErrorMessage varchar(500);                
                
  SELECT @ErrorNumber = ERROR_NUMBER(),                
        @ErrorSeverity = ERROR_SEVERITY(),                
        @ErrorProcedure = ERROR_PROCEDURE(),                
        @ErrorLine = ERROR_LINE(),                
        @ErrorMessage = ERROR_MESSAGE()  ,
		@CreatedOn = GETDATE()              
                
 insert into dbo.Errorlog(ErrorNumber,                
       ErrorSeverity,                
       ErrorProcedure,                
       ErrorLine,                
       ErrorMessage,                
       ErrorDate)                
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,@CreatedOn);                
              
 set @ErrorMsg = 'Error while inserting marketvalue, please contact Admin for more details.';                
--THROW 50000,@ErrorMsg,1;                  
--return(1)              
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine              
    ,Message =@ErrorMsg,Success = 0;                 
                 
END CATCH                 
            
            
END