﻿
CREATE procedure [dbo].[SFab_GetAllSupportCategory]

as
/*================================================================================
Procedure Name: [SFab_GetAllSupportCategory]
Author: Harshal
Create date: 08/21/2019
Description: Get list from Support Category
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
08/21/2019	Harshal 	Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT [Id], 
		[CategoryName]
	FROM [dbo].[SupportCategory]
where ([SupportCategory].[IsDeleted] = 0)  
Order by (Case CategoryName When 'Other Support' then 'ZOther Support' Else CategoryName End) ASC 

end