﻿CREATE procedure [dbo].[SApp_GetRawMaterialbyID]
(@RawMaterialID uniqueidentifier,
@includeDeleted bit = 0)
as
/*================================================================================
Procedure Name: SApp_GetRawMaterialbyID
Author: Sharath
Create date: 02/19/2019
Description: Get list from RawMaterial table by Name or ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
02/19/2019	Sharath			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if @includeDeleted is null set @includeDeleted = 0
declare @SegmentId uniqueidentifier;
select @SegmentId = Id from Segment
where SegmentName = 'Raw materials';

SELECT [Id],
		SubSegmentName as [RawMaterialName],
		[Description]
FROM [dbo].[SubSegment]
where (@RawMaterialID is null or ID = @RawMaterialID)
and SegmentId = @SegmentId
and (@includeDeleted = 1 or IsDeleted = 0)



end