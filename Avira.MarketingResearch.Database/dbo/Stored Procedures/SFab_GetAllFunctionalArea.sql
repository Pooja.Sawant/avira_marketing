﻿  
create PROCEDURE [dbo].[SFab_GetAllFunctionalArea]
(@AviraUserID uniqueidentifier = null)    
 AS  
/*================================================================================  
Procedure Name: [SFab_GetAllFunctionalArea]  
Author: Pooja Sawant  
Create date: 09/10/2019  
Description: Get list of FunctionalAreaName from table  
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
09/10/2019  Pooja   Initial Version  
================================================================================*/  
  
BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
	
	select * from FunctionalArea WHERE IsDeleted = 0 
	order by FunctionalAreaName asc

END