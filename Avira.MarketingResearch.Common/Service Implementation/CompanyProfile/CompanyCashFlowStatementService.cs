﻿using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.CompanyProfile
{
    public class CompanyCashFlowStatementService : ICompanyCashFlowStatementService
    {
        private readonly ICompanyCashFlowStatementRepository _companyCashFlowStatementRepository;
        private readonly ICompanyNoteRepository _iCompanyNoteRepository;

        public CompanyCashFlowStatementService(ICompanyCashFlowStatementRepository companyCashFlowStatementRepository, ICompanyNoteRepository CompanyNoteRepository)
        {
            _companyCashFlowStatementRepository = companyCashFlowStatementRepository;
            _iCompanyNoteRepository = CompanyNoteRepository;
        }

        public IEnumerable<CompanyCashFlowStatementViewModel> GetCompanyCashFlowStatementData(Guid CompanyId, AviraUser user)
        {
            var res = _companyCashFlowStatementRepository.GetCompanyCashFlowStatementData(CompanyId, user);
            return res;
        }
    }
}
