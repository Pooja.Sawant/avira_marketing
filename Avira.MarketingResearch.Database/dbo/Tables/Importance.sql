﻿CREATE TABLE [dbo].[Importance] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [ImportanceName]   NVARCHAR (256)   CONSTRAINT [DF_Importance_ImportanceName] DEFAULT ('') NOT NULL,
    [GroupName]        NVARCHAR (256)   CONSTRAINT [DF_Importance_ImportanceName1] DEFAULT ('') NULL,
    [Description]      NVARCHAR (256)   NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_Importance_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    [Sequence]         INT              NULL,
    CONSTRAINT [PK_Importance] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Importance_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_Importance_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id])
);




GO

GO

GO

GO

GO

GO

GO