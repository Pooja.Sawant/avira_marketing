﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrendMarketMapController : AviraBaseController
    {        
        private readonly ITrendMarketMapService _trendMarketMapService;
        private readonly ITrendMarketMapRepository _trendMarketMapRepository;
        private ILoggerManager _logger;
        public TrendMarketMapController(
            ITrendMarketMapService trendMarketMapService,
            ITrendMarketMapRepository trendMarketMapRepository,
             ILoggerManager logger)
        {
            _trendMarketMapRepository = trendMarketMapRepository;
            _trendMarketMapService = trendMarketMapService;
            _logger = logger;

        }

        [HttpGet]
        public ActionResult<IEnumerable<TrendMarketMapViewModel>> Get(int PageStart = 0, int pageSize = 0, int SortDirection = 1, string OrdbyByColumnName = "CreatedDate", string Search = "", bool isDeletedInclude = false, string id="")
        {
            List<TrendMarketMapViewModel> res = null;
            try
            {
                res = _trendMarketMapService.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude, id).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }
        
        [HttpPost]
        public ActionResult Post([FromBody] MainTrendMarketMapInsertUpdateDbViewModel viewModel)
        {
            viewModel.CreatedOn = System.DateTime.UtcNow;
            viewModel.AviraUserId = CurrentUser.Id;
            
            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new TrendMarketMapt, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                var result = _trendMarketMapService.InsertUpdate(viewModel, CurrentUser);
                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "TrendMarketMapt is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }

    }
}