﻿CREATE PROCEDURE [dbo].[SMsg_GetMessageQueue](@Type NVARCHAR(1))     
 AS    
/*================================================================================    
Procedure Name: [SMsg_GetMessageQueue]    
Author: Gopi    
Create date: 05/13/2019    
Description: Get the active message list to send the notification  
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
05/13/2019 Praveen   Initial Version    
================================================================================*/    
    
BEGIN    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED    
    
  
 SELECT TOP 50  
  [Message].Id  
 ,[Message].MessageTemplateId 
 ,[Message].[SenderFrom] 
 ,[Message].ReceiverTos  
 ,[Message].ReceiverCcs  
 ,[Message].ReceiverBccs  
 ,[Message].Subject  
 ,[Message].Message  
 ,[Message].StatusId  
 ,[Message].IsDeleted  
 ,[Message].CreatedOn  
 ,[Message].UserCreatedById  
 ,[Message].ModifiedOn  
 ,[Message].UserModifiedById   
 FROM [Message]  
 INNER JOIN [MessageTemplate] ON [Message].MessageTemplateId = [MessageTemplate].Id  
 INNER JOIN MessageStatus ON [Message].StatusId = MessageStatus.Id
 WHERE [Message].IsDeleted = 0 AND [MessageTemplate].[Type] = @Type AND MessageStatus.StatusName = 'Pending' AND [Message].CreatedOn >= DATEADD(HOUR, -12, GETDATE())
  
 END