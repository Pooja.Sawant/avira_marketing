﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.ImportExport
{
    public class CPFinancialImportModel
    {
        public int RowNo { get; set; }
        public string CompanyName { get; set; }
        public string Currency { get; set; }
        public string Units { get; set; }
        public string FinanicialYear { get; set; }
        public string Revenue { get; set; }
        public string CostsAndExpenses { get; set; }
        public string CostOfProduction { get; set; }
        public string ResourceCost { get; set; }
        public string SalaryCost { get; set; }
        public string CostOfSales { get; set; }
        public string SellingExpenses { get; set; }
        public string MarketingExpenses { get; set; }
        public string AdministrativeExpenses { get; set; }
        public string GeneralExpenses { get; set; }
        public string SellingGeneralAndAdminExpenses { get; set; }
        public string ResearchAndDevelopmentExpenses { get; set; }
        public string Depreciation { get; set; }
        public string Amortization { get; set; }
        public string Restructuring { get; set; }
        public string OtherIncomeOrDeductions { get; set; }
        public string EBIT { get; set; }
        public string InterestIncome { get; set; }
        public string InterestExpense { get; set; }
        public string EBT { get; set; }
        public string Tax { get; set; }
        public string NetIncomeFromContinuingOperations { get; set; }
        public string DiscontinuedOperations { get; set; }
        public string IncomeLossFromDiscontinuedOperations { get; set; }
        public string GainOnDisposalOfDiscontinuedOperations { get; set; }
        public string DiscontinuedOperationsNetOfTax { get; set; }
        public string NetIncomeBeforeAllocationToNoncontrollingInterests { get; set; }
        public string NonControllingInterests { get; set; }
        public string NetIncome { get; set; }
        public string BasicEPS_ { get; set; }
        public string BasicEPSFromContinuingOperations { get; set; }
        public string BasicEPSFromDiscontinuedOperations { get; set; }
        public string BasicEPS { get; set; }
        public string DilutedEPS_ { get; set; }
        public string DilutedEPSFromContinuingOperations { get; set; }
        public string DilutedEPSFromDiscontinuedOperations { get; set; }
        public string DilutedEPS { get; set; }
        public string WeightedAverageSharesOutstandingBasic { get; set; }
        public string WeightedAverageSharesOutstandingDiluted { get; set; }
        public string Assets_ { get; set; }
        public string CashAndCashEquivalents { get; set; }
        public string ShortTermInvestments { get; set; }
        public string AccountsReceivables { get; set; }
        public string Inventories { get; set; }
        public string CurrentTaxAssets { get; set; }
        public string OtherCurrentAssets { get; set; }
        public string AssetsHeldForSale { get; set; }
        public string TotalCurrentAssets { get; set; }
        public string LongTermInvestments { get; set; }
        public string PropertyPlantAndEquipment { get; set; }
        public string IntangibleAssets { get; set; }
        public string Goodwill { get; set; }
        public string DeferredTaxAssets { get; set; }
        public string OtherNoncurrentAssets { get; set; }
        public string TotalAssets { get; set; }
        public string LiabilitiesAndEquity_ { get; set; }
        public string ShortTermDebt { get; set; }
        public string TradeAccountsPayable { get; set; }
        public string DividendsPayable { get; set; }
        public string IncomeTaxesPayable { get; set; }
        public string AccruedExpenses { get; set; }
        public string OtherCurrentLiabilities { get; set; }
        public string LiabilitiesHeldForSale { get; set; }
        public string TotalCurrentLiabilities { get; set; }
        public string LongTermDebt { get; set; }
        public string PensionBenefitObligations { get; set; }
        public string PostretirementBenefitObligations { get; set; }
        public string DeferredTaxLiabilities { get; set; }
        public string OtherTaxesPayable { get; set; }
        public string OtherNoncurrentLliabilities { get; set; }
        public string TotalLiabilities { get; set; }
        public string CommitmentsAndContingencies_ { get; set; }
        public string PreferredStock { get; set; }
        public string CommonStock { get; set; }
        public string Debentures { get; set; }
        public string AdditionalPaidInCapital { get; set; }
        public string TresuryStock { get; set; }
        public string RetainedEarnings { get; set; }
        public string AccumulatedOtherComprehensiveLoss { get; set; }
        public string TotalShareholdersEquity { get; set; }
        public string MinorityInterest { get; set; }
        public string TotalEquity { get; set; }
        public string TotalLiabilitiesAndEquity { get; set; }
        public string OperatingActivities_ { get; set; }
        public string AdjustmentsToOperatingActivities_ { get; set; }
        public string DepreciationAndAmortization { get; set; }
        public string Impairments { get; set; }
        public string LossOnSaleOfAssets { get; set; }
        public string OtherLossesOrGains { get; set; }
        public string DeferredTaxes { get; set; }
        public string ShareBasedCompensationExpense { get; set; }
        public string BenefitPlanContributions { get; set; }
        public string OtherAdjustmentsNet { get; set; }
        public string OtherChangesInAssetsAndLiabilitiesNetOfAcquisitionsAndDistitures_ { get; set; }
        public string AccountsReceivable { get; set; }
        public string OtherAssets { get; set; }
        public string AccountsPayable { get; set; }
        public string OtherLiabilities { get; set; }
        public string OtherTaxAccountsNet { get; set; }
        public string NetCashProvidedByOrUsedInOperatingActivities { get; set; }
        public string InvestingActivities_ { get; set; }
        public string PurchasesOfPropertyPlantAndEquipment { get; set; }
        public string PurchasesOfShortTermInvestments { get; set; }
        public string SaleOfShortTermInvestments { get; set; }
        public string RedemptionsOfShortTermInvestments { get; set; }
        public string PurchasesOfLongTermInvestments { get; set; }
        public string SaleOfLongTermInvestments { get; set; }
        public string BusinessAcquisitions { get; set; }
        public string AcquisitionsOfIntangibleAssets { get; set; }
        public string OtherInvestingActivitiesNet { get; set; }
        public string NetCashProvidedByOrUsedInInvestingActivities { get; set; }
        public string FinancingActivities_ { get; set; }
        public string ProceedsFromShortTermDebt { get; set; }
        public string PrincipalPaymentsOnShortTermDebt { get; set; }
        public string NetPaymentsonOrProceedsFromShortTermBorrowingsWithOrigilMaturitiesOfThreeMonthsOrLess { get; set; }
        public string IssuanceOfLongTermDebt { get; set; }
        public string PrincipalPaymentsOnLongTermDebt { get; set; }
        public string PurchasesOfCommonStock { get; set; }
        public string CashDividendsPaid { get; set; }
        public string ProceedsFromExerciseOfStockOptions { get; set; }
        public string OtherFinancingActivitiesNet { get; set; }
        public string NetCashProvidedByOrUsedInFinancingActivities { get; set; }
        public string ForeignExchangeImpact { get; set; }
        public string DecreaseInCashAndCashEquivalents { get; set; }
        public string OpeningBalanceOfCashAndCashEquivalents { get; set; }
        public string ClosingBalanceOfCashAndCashEquivalents { get; set; }
        public string CashPaidReceivedDuringThePeriodFor_ { get; set; }
        public string IncomeTaxes { get; set; }
        public string InterestRateHedges { get; set; }
        public string Segment1 { get; set; }
        public string Segment2 { get; set; }
        public string Segment3 { get; set; }
        public string Segment4 { get; set; }
        public string Segment5 { get; set; }
        public string TotalReportableSegments { get; set; }
        public string Geography1 { get; set; }
        public string Geography2 { get; set; }
        public string Geography3 { get; set; }
        public string Geography4 { get; set; }
        public string Geography5 { get; set; }
        public string TotalGeographicSegments { get; set; }

        public string GrossProfit { get; set; }
        public string TotalOperatingExpenses { get; set; }
        public string EBITDA { get; set; }
        public string DeferredRevenuesShortTerm { get; set; }
        public string DeferredRevenuesLongTerm { get; set; }
        public string CommonStockRepurchased { get; set; }
        public string CapitalExpenditure { get; set; }
        
        public string UserCreatedById { get; set; }
        public string CompanyId { get; set; }
        public string CurrencyId { get; set; }
        public string ValueConversionId { get; set; }
        public string ErrorNotes { get; set; }


    }
    public class CPFinancialSegmentInformationModel
    {
        public int RowNo { get; set; }
        public string CompanyName { get; set; }
        public string Currency { get; set; }
        public string Units { get; set; }
        public string FinanicialYear { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string UserCreatedById { get; set; }
        public string CompanyId { get; set; }
        public string CurrencyId { get; set; }
        public string ValueConversionId { get; set; }
        public string ErrorNotes { get; set; }
    }

    public class CPFinancialImportInsertModel
    {
        public string TemplateName { get; set; }
        public string ImportFileName { get; set; }
        public string TrendJson { get; set; }
        public string SegmentJson { get; set; }
        public Guid UserCreatedById { get; set; }
        public List<CPFinancialImportModel> CPFinancialImportModel { get; set; }
        public List<CPFinancialSegmentInformationModel> CPFinancialSegmentInformationModel { get; set; }
    }
}
