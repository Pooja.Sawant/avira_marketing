﻿
  
CREATE PROCEDURE [dbo].[SApp_GetAllCurrenciesWithCode]    
 AS  
/*================================================================================  
Procedure Name: [SApp_GetAllCurrenciesWithCode]  
Author: Swami  
Create date: 05/03/2019  
Description: Get list of currencies from table   
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
05/03/2019 Swami   Initial Version  
================================================================================*/  
  
BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
 SELECT Id,
		CurrencyName,
		CONCAT(CurrencyName, ' ', '(', CurrencyCode, ')') as CurrencyNameWithCode,
		CurrencyCode,
		CurrencySymbol,
		Fractionalunit,
		IsDeleted,
		UserCreatedById

	FROM [dbo].[Currency]  
	where ([Currency].IsDeleted = 0)   
 END