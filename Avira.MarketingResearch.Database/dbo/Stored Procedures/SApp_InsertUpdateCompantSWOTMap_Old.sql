﻿--sp_helptext SApp_InsertUpdateCompantSWOTMap

     
CREATE procedure [dbo].[SApp_InsertUpdateCompantSWOTMap_Old]              
(        
 @CompanySwotOpportunitiesMap [dbo].[udtGUIDCompanySwotOpportunitiesMultiValue] readonly,        
 @CompanySwotStrengthMap [dbo].[udtGUIDCompanySwotStrengthMultiValue] readonly,        
 @CompanySwotThreatsMap [dbo].[udtGUIDCompanySwotThreatMultiValue] readonly,        
 @CompanySwotWeeknessesMap [dbo].[udtGUIDCompanySwotWeeknessesMultiValue] readonly,        
 @CompanyId uniqueidentifier,        
 @UserCreatedById uniqueidentifier        
)              
as        
/*================================================================================        
Procedure Name: SApp_InsertUpdateCompantSWOTMap        
Author: Laxmikant        
Create date: 15/05/2019        
Description: Insert or updare Company SWOT By company ID      
Change History        
Date  Developer  Reason        
__________ ____________ ______________________________________________________        
15/05/2019 Laxmikant   Initial Version        
================================================================================*/        
      
BEGIN              
SET NOCOUNT ON;              
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;              
declare @ErrorMsg NVARCHAR(2048)          
            
              
BEGIN TRY                 
         
update [dbo].[CompanySwotOpportunitiesMap]        
set IsDeleted = 1,        
UserDeletedById = @UserCreatedById,        
DeletedOn = GETDATE()        
from [dbo].[CompanySwotOpportunitiesMap]        
where [CompanySwotOpportunitiesMap].CompanyId = @CompanyId        
and not exists (select 1 from @CompanySwotOpportunitiesMap Opportunitiesmap where Opportunitiesmap.Id = [CompanySwotOpportunitiesMap].Id)        
         
update [dbo].[CompanySwotOpportunitiesMap]        
set [dbo].[CompanySwotOpportunitiesMap].CategoryName = Opportunitiesmap1.OpportunitiesCategory,        
[dbo].[CompanySwotOpportunitiesMap].Description = Opportunitiesmap1.Description,         
[dbo].[CompanySwotOpportunitiesMap].ModifiedOn = GETDATE(),        
[dbo].[CompanySwotOpportunitiesMap].UserModifiedById = @UserCreatedById        
from [dbo].[CompanySwotOpportunitiesMap]        
inner join @CompanySwotOpportunitiesMap Opportunitiesmap1         
on Opportunitiesmap1.Id = [CompanySwotOpportunitiesMap].Id        
where [CompanySwotOpportunitiesMap].CompanyId = @CompanyId;        
        
update [dbo].[CompanySwotStrengthMap]        
set [dbo].[CompanySwotStrengthMap].IsDeleted = 1,        
[dbo].[CompanySwotStrengthMap].UserDeletedById = @UserCreatedById,        
[dbo].[CompanySwotStrengthMap].DeletedOn = GETDATE()        
from [dbo].[CompanySwotStrengthMap]        
where [CompanySwotStrengthMap].CompanyId = @CompanyId        
and not exists (select 1 from @CompanySwotStrengthMap Strengthmap where Strengthmap.Id = [CompanySwotStrengthMap].Id)        
        
update [dbo].[CompanySwotStrengthMap]        
set [dbo].[CompanySwotStrengthMap].CategoryName = Strengthmap1.StrenghtCategory,        
[dbo].[CompanySwotStrengthMap].Description = Strengthmap1.Description,         
[dbo].[CompanySwotStrengthMap].ModifiedOn = GETDATE(),        
[dbo].[CompanySwotStrengthMap].UserModifiedById = @UserCreatedById        
from [dbo].[CompanySwotStrengthMap]        
inner join @CompanySwotStrengthMap Strengthmap1         
on Strengthmap1.Id = [CompanySwotStrengthMap].Id        
where [CompanySwotStrengthMap].CompanyId = @CompanyId;        
        
update [dbo].[CompanySwotThreatsMap]        
set [CompanySwotThreatsMap].IsDeleted = 1,        
[CompanySwotThreatsMap].UserDeletedById = @UserCreatedById,        
[CompanySwotThreatsMap].DeletedOn = GETDATE()        
from [dbo].[CompanySwotThreatsMap]        
where [CompanySwotThreatsMap].CompanyId = @CompanyId        
and not exists (select 1 from @CompanySwotThreatsMap Threatsmap where Threatsmap.Id = [CompanySwotThreatsMap].Id)        
        
update [dbo].[CompanySwotThreatsMap]        
set [dbo].[CompanySwotThreatsMap].CategoryName = Threatsmap1.ThreatsCategory,        
[dbo].[CompanySwotThreatsMap].Description = Threatsmap1.Description,         
[dbo].[CompanySwotThreatsMap].ModifiedOn = GETDATE(),        
[dbo].[CompanySwotThreatsMap].UserModifiedById = @UserCreatedById        
from [dbo].[CompanySwotThreatsMap]        
inner join @CompanySwotThreatsMap Threatsmap1         
on Threatsmap1.Id = [CompanySwotThreatsMap].Id        
where [CompanySwotThreatsMap].CompanyId = @CompanyId;        
        
update [dbo].[CompanySwotWeeknessesMap]        
set IsDeleted = 1,        
UserDeletedById = @UserCreatedById,        
DeletedOn = GETDATE()        
from [dbo].[CompanySwotWeeknessesMap]        
where [CompanySwotWeeknessesMap].CompanyId = @CompanyId        
and not exists (select 1 from @CompanySwotWeeknessesMap Weeknessesmap where Weeknessesmap.Id = [CompanySwotWeeknessesMap].Id)        
        
update [dbo].[CompanySwotWeeknessesMap]        
set [dbo].[CompanySwotWeeknessesMap].CategoryName = Weeknessesmap1.WeaknessesCategory,        
[dbo].[CompanySwotWeeknessesMap].Description = Weeknessesmap1.Description,         
[dbo].[CompanySwotWeeknessesMap].ModifiedOn = GETDATE(),        
[dbo].[CompanySwotWeeknessesMap].UserModifiedById = @UserCreatedById        
from [dbo].[CompanySwotWeeknessesMap]        
inner join @CompanySwotWeeknessesMap Weeknessesmap1         
on Weeknessesmap1.Id = [CompanySwotWeeknessesMap].Id        
where [CompanySwotWeeknessesMap].CompanyId = @CompanyId;        
        
--Insert new records        
insert into [dbo].[CompanySwotOpportunitiesMap](        
  [dbo].[CompanySwotOpportunitiesMap].[Id]              
       ,[dbo].[CompanySwotOpportunitiesMap].[CompanyId]        
    ,[dbo].[CompanySwotOpportunitiesMap].[CategoryName]        
       ,[dbo].[CompanySwotOpportunitiesMap].[Description]                
       ,[dbo].[CompanySwotOpportunitiesMap].[IsDeleted]              
       ,[dbo].[CompanySwotOpportunitiesMap].[UserCreatedById]              
       ,[dbo].[CompanySwotOpportunitiesMap].[CreatedOn])              
 select Opportunitiesmap2.Id,        
   Opportunitiesmap2.CompanyId,               
   Opportunitiesmap2.OpportunitiesCategory,        
   Opportunitiesmap2.Description,            
   0,              
   @UserCreatedById,               
   GETDATE()        
 from @CompanySwotOpportunitiesMap Opportunitiesmap2        
 where not exists (select 1 from [dbo].[CompanySwotOpportunitiesMap] Opportunitiesmap3        
     where Opportunitiesmap3.CompanyId = @CompanyId        
     and Opportunitiesmap2.Id = Opportunitiesmap3.Id);               
        
insert into [dbo].[CompanySwotStrengthMap](        
  [dbo].[CompanySwotStrengthMap].[Id]              
       ,[dbo].[CompanySwotStrengthMap].[CompanyId]        
    ,[dbo].[CompanySwotStrengthMap].[CategoryName]        
       ,[dbo].[CompanySwotStrengthMap].[Description]                
       ,[dbo].[CompanySwotStrengthMap].[IsDeleted]              
       ,[dbo].[CompanySwotStrengthMap].[UserCreatedById]              
       ,[dbo].[CompanySwotStrengthMap].[CreatedOn])              
 select Strengthmap2.Id,        
   Strengthmap2.CompanyId,               
   Strengthmap2.StrenghtCategory,        
   Strengthmap2.Description,            
   0,              
   @UserCreatedById,               
   GETDATE()        
 from @CompanySwotStrengthMap Strengthmap2        
 where not exists (select 1 from [dbo].[CompanySwotStrengthMap] Strengthmap3        
     where Strengthmap3.CompanyId = @CompanyId        
     and Strengthmap2.Id = Strengthmap3.Id);         
             
insert into [dbo].[CompanySwotThreatsMap](        
  [dbo].[CompanySwotThreatsMap].[Id]              
       ,[dbo].[CompanySwotThreatsMap].[CompanyId]        
    ,[dbo].[CompanySwotThreatsMap].[CategoryName]        
       ,[dbo].[CompanySwotThreatsMap].[Description]                
       ,[dbo].[CompanySwotThreatsMap].[IsDeleted]              
       ,[dbo].[CompanySwotThreatsMap].[UserCreatedById]              
       ,[dbo].[CompanySwotThreatsMap].[CreatedOn])              
 select Threatsmap2.Id,        
   Threatsmap2.CompanyId,               
   Threatsmap2.ThreatsCategory,        
   Threatsmap2.Description,            
   0,              
   @UserCreatedById,               
   GETDATE()        
 from @CompanySwotThreatsMap Threatsmap2        
 where not exists (select 1 from [dbo].[CompanySwotThreatsMap] Threatsmap3        
     where Threatsmap3.CompanyId = @CompanyId        
     and Threatsmap2.Id = Threatsmap3.Id);         
             
insert into [dbo].[CompanySwotWeeknessesMap](        
  [dbo].[CompanySwotWeeknessesMap].[Id]              
       ,[dbo].[CompanySwotWeeknessesMap].[CompanyId]        
    ,[dbo].[CompanySwotWeeknessesMap].[CategoryName]        
       ,[dbo].[CompanySwotWeeknessesMap].[Description]                
       ,[dbo].[CompanySwotWeeknessesMap].[IsDeleted]              
       ,[dbo].[CompanySwotWeeknessesMap].[UserCreatedById]              
       ,[dbo].[CompanySwotWeeknessesMap].[CreatedOn])              
 select Weeknessesmap2.Id,        
   Weeknessesmap2.CompanyId,               
   Weeknessesmap2.WeaknessesCategory,        
   Weeknessesmap2.Description,            
   0,              
   @UserCreatedById,               
   GETDATE()        
 from @CompanySwotWeeknessesMap Weeknessesmap2        
 where not exists (select 1 from [dbo].[CompanySwotWeeknessesMap] Weeknessesmap3        
     where Weeknessesmap3.CompanyId = @CompanyId        
     and Weeknessesmap2.Id = Weeknessesmap3.Id);               
        
SELECT StoredProcedureName ='SApp_InsertUpdateCompantSWOTMap',Message =@ErrorMsg,Success = 1;               
            
END TRY              
BEGIN CATCH              
    -- Execute the error retrieval routine.              
 DECLARE @ErrorNumber int;              
 DECLARE @ErrorSeverity int;              
 DECLARE @ErrorProcedure varchar(100);                
 DECLARE @ErrorLine int;              
 DECLARE @ErrorMessage varchar(500);              
              
  SELECT @ErrorNumber = ERROR_NUMBER(),              
        @ErrorSeverity = ERROR_SEVERITY(),              
        @ErrorProcedure = ERROR_PROCEDURE(),              
        @ErrorLine = ERROR_LINE(),              
        @ErrorMessage = ERROR_MESSAGE()              
              
 insert into dbo.Errorlog(ErrorNumber,              
       ErrorSeverity,              
       ErrorProcedure,              
       ErrorLine,              
       ErrorMessage,              
       ErrorDate)              
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,ERROR_MESSAGE(),GETDATE());              
            
 set @ErrorMsg = 'Error while insert a new CompanyTrendsMap, please contact Admin for more details.';            
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine            
    ,Message =@ErrorMsg,Success = 0;               
               
END CATCH          
end