﻿CREATE PROCEDURE [dbo].[SFab_GetAllDecisionAlternativeTask] 
(
	@DecisionId UNIQUEIDENTIFIER, --Optional
	@DecisionAlternativeId UNIQUEIDENTIFIER, --Optional
	@Description NVARCHAR(1024) = NULL,--Optional
	@ResourceAssigned NVARCHAR(899) = NULL,--Optional ... list
	@TaskStatusIdList [dbo].[udtUniqueIdentifier] READONLY,--Optional record 0 ... list
	@DecisionStatusIdList [dbo].[udtUniqueIdentifier] READONLY,--Optional record 0 ... list
	@StandpointIdList [dbo].[udtUniqueIdentifier] READONLY,--Optional record 0 ... list
	@CreatedDateOrderby NVARCHAR(1) = 'D',--Optional 'D for descending A for ascending'
	@ModifiedDateOrderby NVARCHAR(1) = 'D',--Optional
	@DueDateOrderby NVARCHAR(1) = 'A',--Optional
	@AviraUserId UNIQUEIDENTIFIER 
 ) AS /*================================================================================
Procedure Name: SFab_GetAllDecisionAlternativeTask
Author: Praveen
Create date: 07-Jan-2020
Description: To return all Tasks of the perticular Alternative
Change History
Date  Developer  Reason
__________ ____________ ______________________________________________________
07-Jan-2020  Praveen   Initial Version
30-Jan-2020  Praveen   Implementation
================================================================================*/ 
BEGIN 
 --Required Tables: Decision, DecisionAlternative, DecisionAlternativeTask, LookupStatus and LookupCategory
 --Case 1: @DecisionAlternativeId is null then return all tasks by the userId
 --Case 2: @DecisionAlternativeId is not null then return all tasks related to perticular alternativeId
 --Notes: Get all tasks of the user and validate records by userId
 

IF len(@ResourceAssigned)>2
SET @ResourceAssigned = '%'+ @ResourceAssigned + '%' ELSE
SET @ResourceAssigned = '' 
DECLARE @ResourceAssignedList [dbo].[udtVarcharValue];
INSERT INTO @ResourceAssignedList
SELECT DISTINCT '%'+ltrim(rtrim(value)) +'%'
FROM STRING_SPLIT(@ResourceAssigned, ' ')
WHERE value != '' ;

IF len(@Description)>2
SET @Description = '%'+ @Description + '%' ELSE
SET @Description = '' 
DECLARE @TaskDescriptionList [dbo].[udtVarcharValue];
INSERT INTO @TaskDescriptionList
SELECT DISTINCT '%'+ltrim(rtrim(value)) +'%'
FROM STRING_SPLIT(@Description, ' ')
WHERE value != '' ;


SELECT DecisionAlternativeTask.Id AS TaskId,Decision.Id as DecisionId,
       CASE WHEN ISNULL(DecisionAlternativeTask.CreatedOn, '') = '' THEN '' ELSE FORMAT(DecisionAlternativeTask.CreatedOn, 'dd/MMM/yyyy ') END CreatedOn,
       CASE WHEN ISNULL(DecisionAlternativeTask.ModifiedOn, '') = '' THEN FORMAT(DecisionAlternativeTask.CreatedOn, 'dd/MMM/yyyy ') ELSE FORMAT(DecisionAlternativeTask.ModifiedOn, 'dd/MMM/yyyy ') END ModifiedOn,
       DecisionAlternativeTask.Description AS TaskDesc,
       DecisionAlternativeTask.ResourceAssigned,
       DecisionAlternativeTask.StatusId,
       LookupStatus.StatusName AS StatusName,
	   CASE WHEN ISNULL(DecisionAlternativeTask.DueDate, '') = '' THEN '' ELSE FORMAT(DecisionAlternativeTask.DueDate, 'dd/MMM/yyyy ') END DueDate,
       DecisionAlternativeTask.StandpointId,

	   CASE WHEN (LookupStatus.StatusName = 'Ongoing' AND CONVERT(date ,DueDate) < CONVERT(date ,GETDATE())) OR  (LookupStatus.StatusName = 'Completed' AND CONVERT(date ,DueDate) < CONVERT(date ,StatusModifiedOn)) THEN 'Overdue'
	   WHEN (LookupStatus.StatusName = 'Completed') AND 
	   (CONVERT(date ,DueDate) >= CONVERT(date ,GETDATE()) OR CONVERT(date ,DueDate) >= CONVERT(date ,StatusModifiedOn) )
	   THEN 'Ontime'
	    WHEN (LookupStatus.StatusName = 'Ongoing') AND CONVERT(date ,DueDate) >= CONVERT(date ,GETDATE()) THEN 'Ontime'
		WHEN (LookupStatus.StatusName = 'To be initiated' OR LookupStatus.StatusName = 'Wishlist' OR LookupStatus.StatusName = 'Not required' OR LookupStatus.StatusName = 'On hold') THEN 'NA'
		ELSE 'NA' END
	    AS Standpoint,

       CASE
           WHEN (DMStatus.StatusName = 'Decision made') THEN 1
           ELSE 0
       END AS IsReadonly, DecisionAlternative.Description As AlternativeDescription, DecisionAlternative.Id As AlternativeId
FROM DecisionAlternativeTask
LEFT JOIN DecisionAlternative ON DecisionAlternativeTask.DecisionAlternativeId = DecisionAlternative.Id
INNER JOIN Decision ON DecisionAlternative.DecisionId = Decision.Id
LEFT JOIN LookupCategory ON DecisionAlternativeTask.StandpointId = LookupCategory.Id AND LookupCategory.CategoryType = 'DMDecisionAlternativeTaskStandpoint'
LEFT JOIN LookupStatus ON DecisionAlternativeTask.StatusId = LookupStatus.Id AND LookupStatus.StatusType='DMDecisionAlternativeTaskStatus'
LEFT JOIN LookupStatus DMStatus ON Decision.StatusId = DMStatus.Id AND DMStatus.StatusType='DMDecisionStatus'
WHERE (Decision.UserId = @AviraUserId)
  AND (ISNULL(@DecisionId,'00000000-0000-0000-0000-000000000000') = '00000000-0000-0000-0000-000000000000' OR Decision.Id = @DecisionId)
  AND (Decision.UserId = @AviraUserId)
  --AND (Decision.Id = @DecisionId)
  AND (ISNULL(@DecisionAlternativeId,'00000000-0000-0000-0000-000000000000') = '00000000-0000-0000-0000-000000000000'
       OR DecisionAlternativeTask.DecisionAlternativeId = @DecisionAlternativeId)
AND (NOT EXISTS
       (SELECT 1
        FROM @TaskDescriptionList)
     OR EXISTS
       (SELECT 1
        FROM @TaskDescriptionList list
        WHERE DecisionAlternativeTask.Description LIKE list.[value]))
		
		AND (NOT EXISTS
       (SELECT 1
        FROM @ResourceAssignedList)
     OR EXISTS
       (SELECT 1
        FROM @ResourceAssignedList list
        WHERE DecisionAlternativeTask.ResourceAssigned LIKE list.[value]))

  AND (NOT exists
         (SELECT 1
          FROM @DecisionStatusIdList)
       OR Decision.StatusId IN
         (SELECT Id
          FROM @DecisionStatusIdList))
  AND (NOT exists
         (SELECT 1
          FROM @TaskStatusIdList)
       OR DecisionAlternativeTask.StatusId IN
         (SELECT Id
          FROM @TaskStatusIdList))
  AND (NOT exists
         (SELECT 1
          FROM @StandpointIdList)
       OR DecisionAlternativeTask.StandpointId IN
         (SELECT Id
          FROM @StandpointIdList))
  
  
ORDER BY DecisionAlternativeTask.Description ASC,
CASE WHEN @CreatedDateOrderby = 'A' THEN DecisionAlternativeTask.CreatedOn END,
CASE WHEN @CreatedDateOrderby = 'D' THEN DecisionAlternativeTask.CreatedOn END DESC,



CASE WHEN @ModifiedDateOrderby = 'A' THEN DecisionAlternativeTask.ModifiedOn END,
CASE WHEN @ModifiedDateOrderby = 'D' THEN DecisionAlternativeTask.ModifiedOn END DESC, 
CASE WHEN @DueDateOrderby = 'A' THEN DecisionAlternativeTask.DueDate END,
CASE WHEN @DueDateOrderby = 'D' THEN DecisionAlternativeTask.DueDate END DESC

END