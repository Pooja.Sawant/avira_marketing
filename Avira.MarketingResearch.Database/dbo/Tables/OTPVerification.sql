﻿CREATE TABLE [dbo].[OTPVerification] (
    [Id]                 UNIQUEIDENTIFIER NOT NULL,
    [OtpType]            VARCHAR (50)     NOT NULL,
    [AviraUserId]        UNIQUEIDENTIFIER NULL,
    [OtpEntity]          NVARCHAR (512)   NOT NULL,
    [OTPCode]            NVARCHAR (512)   NOT NULL,
    [GeneratedDateTime]  DATETIME         CONSTRAINT [DF_OTPVerification_GeneratedDateTime] DEFAULT (getdate()) NOT NULL,
    [ValidateDateTime]   DATETIME         NULL,
    [AttempFailureCount] SMALLINT         CONSTRAINT [DF_OTPVerification_AttempFailureCount] DEFAULT ((0)) NOT NULL,
    [ClientDeviceId]     NVARCHAR (512)   CONSTRAINT [DF_OTPVerification_AttempFailureCount1] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_OTPVerification] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Email / Mobile', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'OTPVerification', @level2type = N'COLUMN', @level2name = N'OtpType';

