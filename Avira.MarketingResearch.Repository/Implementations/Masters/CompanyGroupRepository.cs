﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.CoreInterfaces.Masters;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Avira.MarketingResearch.Repository.Implementations.Masters
{
    public class CompanyGroupRepository : ICompanyGroupRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "CompanyGroup";
        public CompanyGroupRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }


        public CompanyGroupViewModel GetById(Guid guid)
        {
            CompanyGroupViewModel companyGroupViewModel = new CompanyGroupViewModel();
            companyGroupViewModel = GetAllCompanyGroup(guid).FirstOrDefault();
            return companyGroupViewModel;
        }

        public IEnumerable<CompanyGroupViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted)
        {
            return GetAllCompanyGroup(null, PageStart, pageSize, SortDirection, OrdbyByColumnName,Search, includeDeleted);
        }

        private IEnumerable<CompanyGroupViewModel> GetAllCompanyGroup(Guid? guid, int PageStart=0, int pageSize=0, int SortDirection=-1, string OrdbyByColumnName="", string Search = "", bool includeDeleted = false)
        {           

            DataTable companyGroupIdsTable = new DataTable();
            companyGroupIdsTable.Columns.Add("ID", typeof(Guid));

            if (guid != null)
            {
                companyGroupIdsTable.Rows.Add(guid);
            }            
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<CompanyGroupViewModel>(CompanyGroupQueries.StoreProcSelectAll
                            , new { @CompanyGroupIDList = companyGroupIdsTable.AsTableValuedParameter("dbo.udtUniqueIdentifier"), @includeDeleted = includeDeleted, @PageStart= PageStart, @pageSize= pageSize, @SortDirection= SortDirection, @OrdbyByColumnName= OrdbyByColumnName, @CompanyGroupName=Search }
                            ,null, true, null, CommandType.StoredProcedure);
        }

        public Result<SpTransactionMessage> Create(CompanyGroupInsertDbViewModel companyGroupInsertDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(CompanyGroupQueries.StoreProcInsert, companyGroupInsertDbViewModel, CommandType.StoredProcedure);
            return spTransactionMessage;

        }

        public Result<SpTransactionMessage> Update(CompanyGroupUpdateDbViewModel companyGroupUpdateDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(RawMaterialsQueries.StoreProcUpdate, companyGroupUpdateDbViewModel, CommandType.StoredProcedure);
            return spTransactionMessage;
        }



    }
}
