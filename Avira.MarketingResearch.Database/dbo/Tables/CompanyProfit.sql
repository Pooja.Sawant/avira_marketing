﻿CREATE TABLE [dbo].[CompanyProfit] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [CompanyId]        UNIQUEIDENTIFIER NOT NULL,
    [ProjectId]        UNIQUEIDENTIFIER NULL,
    [CurrencyId]       UNIQUEIDENTIFIER NOT NULL,
    [UnitId]           UNIQUEIDENTIFIER NOT NULL,
    [TotalProfit]      DECIMAL (18, 4)  NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_CompanyProfit_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    [Year]             INT              NULL,
    CONSTRAINT [PK_CompanyProfit] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CompanyProfit_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyProfit_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyProfit_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([Id]),
    CONSTRAINT [FK_CompanyProfit_Currency] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[Currency] ([Id]),
    CONSTRAINT [FK_CompanyProfit_Project] FOREIGN KEY ([ProjectId]) REFERENCES [dbo].[Project] ([ID]),
    CONSTRAINT [FK_CompanyProfit_ValueConversion] FOREIGN KEY ([UnitId]) REFERENCES [dbo].[ValueConversion] ([Id])
);

