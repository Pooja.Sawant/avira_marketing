﻿CREATE TABLE [dbo].[StagingCompanySWOT] (
    [Id]            UNIQUEIDENTIFIER NOT NULL,
    [ImportId]      UNIQUEIDENTIFIER NULL,
    [AnalysisPoint] NVARCHAR (1000)  NULL,
    [Market]        NVARCHAR (255)   NULL,
    [Category]      NVARCHAR (255)   NULL,
    [SWOTType]      NVARCHAR (255)   NULL,
    [CategoryId]    UNIQUEIDENTIFIER NULL,
    [SWOTTypeId]    UNIQUEIDENTIFIER NULL,
    [ProjectId]     UNIQUEIDENTIFIER NULL,
    [CompanyId]     UNIQUEIDENTIFIER NULL,
    [ErrorNotes]    NVARCHAR (1000)  NULL,
    [RowNo]         INT              NULL,
    CONSTRAINT [PK_StagingCompanySWOT] PRIMARY KEY CLUSTERED ([Id] ASC)
);



