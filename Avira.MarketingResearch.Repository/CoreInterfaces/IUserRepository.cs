﻿using System;
using System.Collections.Generic;
using System.Text;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.Helpers;

namespace Avira.MarketingResearch.Repository.CoreInterfaces
{
    public interface IUserRepository
    {
        Result<SpTransactionMessage> Create(AviraUserInsertDbViewModel aviraUserInsertDbViewModel);
        AviraUser GetByEmail(string email);
        AviraUser GetByUserId(string userId);
        AviraUser GetUserLogin(string userName);
        IEnumerable<RoleViewModel> GetAllRoles();
        IEnumerable<LocationViewModel> GetAllLocation();

        AviraUser UpdateUserLogin(string userName, bool IsSuccess);

        IEnumerable<RoleViewModel> GetRolesByAviraUserId(Guid aviraUserId);

        PermissionsByUserRoleIdViewModel GetPermissionsByUserRoleId(Guid id);
        IEnumerable<AviraUser> GetAllUsersByRole(string roleName);
        IEnumerable<GetAllAviraUsers> GetAll(Guid? Id, int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false);

        //RoleViewModel GetPermissionsByUserRoleId(Guid id);
    }
}
