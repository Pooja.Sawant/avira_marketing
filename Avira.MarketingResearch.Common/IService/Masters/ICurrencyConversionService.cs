﻿using System;
using System.Collections.Generic;
using System.Text;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Model;
using Avira.MarketingResearch.Model.ViewModels.Masters;

namespace Avira.MarketingResearch.Common.IService.Masters
{
    public interface ICurrencyConversionService
    {
        Result<SpTransactionMessage> Create(CurrencyConversionInsertDbViewModel tfc, AviraUser currentUser);

        IEnumerable<CurrencyConversionInsertDbViewModel> GetById(Guid guid);

        IEnumerable<CurrencyConversionInsertDbViewModel> GetByIdAll(CurrencyConversionInsertDbViewModel tfc);
    }
}
