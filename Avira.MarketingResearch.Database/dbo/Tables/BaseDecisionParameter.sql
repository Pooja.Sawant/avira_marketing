﻿CREATE TABLE [dbo].[BaseDecisionParameter] (
    [Id]               UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [Description]      NVARCHAR (1024)  NOT NULL,
    [CategoryId]       UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]        DATETIME2 (7)    DEFAULT (getdate()) NOT NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([CategoryId]) REFERENCES [dbo].[LookupCategory] ([Id]),
    FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id])
);

