﻿CREATE TABLE [dbo].[FormPermissionMap] (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [FormId]       UNIQUEIDENTIFIER NOT NULL,
    [PermissionId] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_FormPermissionMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_FormPermissionMap_FormId] FOREIGN KEY ([FormId]) REFERENCES [dbo].[Form] ([Id]),
    CONSTRAINT [FK_FormPermissionMap_PermissionId] FOREIGN KEY ([PermissionId]) REFERENCES [dbo].[Permission] ([Id]),
    CONSTRAINT [IX_FormPermissionMap] UNIQUE NONCLUSTERED ([FormId] ASC, [PermissionId] ASC)
);


GO

GO

GO

GO

GO