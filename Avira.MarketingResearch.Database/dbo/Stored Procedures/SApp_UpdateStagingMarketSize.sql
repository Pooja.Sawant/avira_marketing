﻿CREATE procedure [dbo].[SApp_UpdateStagingMarketSize]              
              
as              
/*================================================================================              
Procedure Name: SApp_UpdateStagingMarketSize             
Author: Gopi              
Create date: 05/28/2019              
Description: to update RegionId,ServiceId,RawMaterialId,ApplicationId,IndustryId              
Change History              
Date  Developer  Reason              
__________ ____________ ______________________________________________________              
05/28/2019 Gopi   Initial Version              
================================================================================*/              
BEGIN              
SET NOCOUNT ON;              
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;              
declare @ErrorMsg NVARCHAR(2048)                
              
BEGIN TRY   

	update staging
	set ErrorNotes = isnull(ErrorNotes,'') 
	+ case when IsNull(staging.Region, '') = '' Then 'Region name is mandatory, ' Else '' End																		 
	+ case when IsNull(staging.Country, '') = '' Then 'Country name is mandatory, ' Else '' End
	+ case when IsNull(staging.Market, '') = '' Then 'Market name is mandatory, ' Else '' End	
	+ Case When Len(Staging.Market) > 255 Then 'Market name exceed max length(255),' Else '' End 	
	+ case when IsNull(staging.Project, '') = '' Then 'Project  name is mandatory, ' Else '' End			
    + Case When Len(Staging.Project) > 255 Then 'Project name exceed max length(255),' Else '' End 
	from StagingMarketSizing  staging;

            
              
 ;With cteReg as (              
 select R.Id as RegionId,           
     CASE               
     WHEN R.Id is null  THEN 'Invalid Region'
     ELSE NULL              
     END As RegionIdError,              
     SM.Id as StaggingId                
 from  StagingMarketSizing SM              
 left join Region R              
 on r.RegionName = SM.Region              
  ),    
  cteCountry AS(         
   select C.Id as CountryId,              
   CASE               
     WHEN C.Id is null  THEN 'Invalid country'              
     ELSE NULL              
     END As CountryIdError,              
          SM.Id as StaggingId              
    from  StagingMarketSizing SM              
   left join Country C              
   on C.CountryName = SM.Country       
   ),    
  cteCountryRegionMap AS(    
  select R.Id as RegionId,C.Id as CountyId,SM.Id as StaggingId,    
  case when CRM.id is null AND ISNULL(SM.Country, '') != '' then 'Country Region map does not exist' ELSE NULL end as CountryRegionMapError    
  from StagingMarketSizing SM    
  left join Region R    
  on R.RegionName = SM.Region    
  left join Country C    
  on C.CountryName = SM.Country    
  left join CountryRegionMap CRM    
  on CRM.CountryId = c.id    
  and crm.RegionId = r.Id    
  )                
  ,cteProj AS(        
  select Prj.ID as ProjectId,        
  CASE         
   WHEN Prj.ID is null AND ISNULL(SM.Project, '') ='' THEN 'Invalid ProjectName'
     ELSE NULL         
     END As ProjectIdError,        
     SM.Id as StaggingId              
    from  StagingMarketSizing SM              
   left join Project Prj              
   on Prj.ProjectName = SM.Project            
   )        
   ,cteMark AS(        
  select MK.Id as MarketId,        
  CASE         
   WHEN MK.Id is null THEN 'Invalid  MarketName'          
     ELSE NULL              
     END As MarketIdError,        
     SM.Id as StaggingId              
    from  StagingMarketSizing SM              
   left join Market MK              
   on MK.MarketName = SM.Market            
   )        
   ,cteCurr AS(        
  select CY.Id as CurrencyId,        
  CASE         
   WHEN CY.Id is null  THEN 'Invalid Currency'          
     ELSE NULL              
     END As CurrencyIdError,        
     SM.Id as StaggingId              
    from  StagingMarketSizing SM              
   left join Currency CY              
   on CY.CurrencyCode = SM.CurrencyName            
   )        
   ,cteVal AS(        
  select VC.Id as ValueConversionId,        
  CASE         
   WHEN VC.Id is null AND ISNULL(SM.Value_2018,'') != '' AND ISNULL(SM.Value_2018,'') != '0' THEN 'No ValueConversionId'          
     ELSE NULL              
     END As ValueConversionIdError,        
     SM.Id as StaggingId              
    from  StagingMarketSizing SM              
   left join ValueConversion VC              
   on VC.ValueName = SM.ValueName            
   )     
   ,cteCurrency AS(
   select 
   CASE 
    WHEN CurrencyName is null THEN 'Please provide Currency Code value'
	WHEN CurrencyName<>'USD'  THEN 'Only USD Currency Allowed'
   ELSE NULL
   END as ErrorCurrency,
   SM.Id as StaggingId
   from StagingMarketSizing SM                      
   )    
    ,cteVolumeUnitExist AS(        
  select VC.Id as ValueConversionId,        
  CASE         
   WHEN VC.Id is null AND ISNULL(SM.Volume_2018,'') != '' AND ISNULL(SM.Volume_2018,'') != '0' THEN 'No Volume Unit'          
     ELSE NULL               
     END As VolumeUnitIdError,        
     SM.Id as StaggingId              
    from  StagingMarketSizing SM              
   left join ValueConversion VC              
   on VC.ValueName = SM.VolumeUnit            
   )
   ,cteVolumeUnitNull AS(
   select 
   CASE 
    WHEN VolumeUnit is null AND ISNULL(SM.Volume_2018,'') != '' AND ISNULL(SM.Volume_2018,'') != '0' THEN 'Please provide VolumeUnit value'
   ELSE NULL
   END as ErrorVolumeUnitNull,
   SM.Id as StaggingId
   from StagingMarketSizing SM                      
   )   
   ,cteASPUnitExist AS(        
  select VC.Id as ValueConversionId,        
  CASE         
   WHEN VC.Id is null AND ISNULL(SM.ASP_2018,'') != '' AND ISNULL(SM.ASP_2018,'') != '0' THEN 'No ASPUnit'          
     ELSE NULL              
     END As ASPUnitIdError,        
     SM.Id as StaggingId              
    from  StagingMarketSizing SM              
   left join ValueConversion VC              
   on VC.ValueName = SM.ASPUnit            
   )
   ,cteASPUnitNull AS(
   select SM.ASP_2018,
   CASE 
    WHEN ASPUnit is null AND ISNULL(SM.ASP_2018,'') != '' AND ISNULL(SM.ASP_2018,'') != '0' THEN 'Please provide ASPUnit value'
   ELSE NULL
   END as ErrorASPUnitNull,
   SM.Id as StaggingId
   from StagingMarketSizing SM                      
   )   
  update StagingMarketSizing               
  set 
  RegionId = cteReg.RegionId,         
  CountryId = cteCountry.CountryId,    
   ProjectId = cteProj.ProjectId,         
   MarketId = cteMark.MarketId,        
   CurrencyId = cteCurr.CurrencyId,   
   ValueConversionId = cteVal.ValueConversionId,   
   VolumeUnitId = cteVolumeUnitExist.ValueConversionId,
   ASPUnitId = cteASPUnitExist.ValueConversionId,
   ErrorNotes = ErrorNotes + substring(CONCAT_WS(', ', cteReg.RegionIdError, 
   cteProj.ProjectIdError, 
   cteMark.MarketIdError, 
   cteCurr.CurrencyIdError, 
   cteVal.ValueConversionIdError, 
   cteCountry.CountryIdError, 
   cteCountryRegionMap.CountryRegionMapError, 
   cteCurrency.ErrorCurrency, 
   cteVolumeUnitExist.VolumeUnitIdError, 
   cteVolumeUnitNull.ErrorVolumeUnitNull, 
   cteASPUnitExist.ASPUnitIdError, 
   cteASPUnitNull.ErrorASPUnitNull 
			),1,254)     
  From cteReg              
  Inner join StagingMarketSizing sm              
  On cteReg.StaggingId = sm.Id              
  Inner Join cteProj        
  On cteProj.StaggingId = sm.Id        
  Inner Join cteMark        
  On cteMark.StaggingId = sm.Id        
  Inner Join cteCurr          
  On cteCurr.StaggingId = sm.Id        
  Inner Join cteVal        
  On cteVal.StaggingId = sm.Id        
  Inner Join cteCountry      
  On cteCountry.StaggingId = sm.Id    
  Inner Join cteCountryRegionMap    
  On cteCountryRegionMap.StaggingId  = sm.Id    
  Inner Join cteCurrency
  On cteCurrency.StaggingId = sm.Id 
  Inner Join cteVolumeUnitExist
  On cteVolumeUnitExist.StaggingId = sm.Id
  Inner Join cteVolumeUnitNull
  On cteVolumeUnitNull.StaggingId = sm.Id
  Inner Join cteASPUnitExist
  On cteASPUnitExist.StaggingId = sm.Id
  Inner Join cteASPUnitNull
  On cteASPUnitNull.StaggingId = sm.Id

--Added for segment and subsegment
update staging
set ErrorNotes = isnull(ErrorNotes,'') + 'Invalid Segment "'+ j.[SegmentName] +'",'
from [dbo].[StagingMarketSizing] staging
cross apply OPENJSON(staging.SegmentMapJson)            
    WITH (            
  [SegmentName] [nvarchar](255) ,            
   [SubSegment] [nvarchar](255)) j
where not exists(select 1 from Segment where j.[SegmentName] = Segment.segmentname) 

update staging
set ErrorNotes = isnull(ErrorNotes,'') + 'Invalid SubSegment "'+ j.[SubSegment] +'",'
from [dbo].[StagingMarketSizing] staging
cross apply OPENJSON(staging.SegmentMapJson)            
    WITH (            
  [SegmentName] [nvarchar](255) ,            
   [SubSegment] [nvarchar](255)) j
where not exists(select 1 from SubSegment where j.[SubSegment] = SubSegment.SubSegmentName )
and j.[SubSegment]!= ''


update staging
set ErrorNotes = isnull(ErrorNotes,'') + 'SubSegment "'+ j.[SubSegment] +'" not mapped to Segment "'+ j.[SegmentName]+'",'
from [dbo].[StagingMarketSizing] staging
cross apply OPENJSON(staging.SegmentMapJson)            
    WITH (            
  [SegmentName] [nvarchar](255) ,            
   [SubSegment] [nvarchar](255)) j
inner join Segment
on j.[SegmentName] = Segment.SegmentName
where j.[SubSegment] != ''
and not exists(select 1 from SubSegment where j.[SubSegment] = SubSegment.SubSegmentName and Segment.id = SubSegment.SegmentId)


Declare @Notes int;          
set @Notes = (Select COUNT(*) from StagingMarketSizing where ErrorNotes != '') 
IF(@Notes > 0)          
BEGIN         
   Select Market, RowNo, ErrorNotes, Message = @ErrorMsg, Success = 1, StoredProcedureName ='SApp_UpdateStagingMarketSize' from StagingMarketSizing where ISNULL(ErrorNotes,'') != '' order by 1,2;          
         
 DELETE from StagingMarketSizing;           
END          
ELSE      
BEGIN          
    exec SApp_InsertMarketValue;
    DELETE from StagingMarketSizing;     
	  
END      
END TRY                    
BEGIN CATCH                    
    -- Execute the error retrieval routine.                    
 DECLARE @ErrorNumber int;                    
 DECLARE @ErrorSeverity int;                    
 DECLARE @ErrorProcedure varchar(100);                    
 DECLARE @ErrorLine int;            
 DECLARE @ErrorMessage varchar(500);                    
                    
  SELECT @ErrorNumber = ERROR_NUMBER(),                    
        @ErrorSeverity = ERROR_SEVERITY(),                    
        @ErrorProcedure = ERROR_PROCEDURE(),                    
        @ErrorLine = ERROR_LINE(),                    
        @ErrorMessage = ERROR_MESSAGE()                    
                    
 insert into dbo.Errorlog(ErrorNumber,                    
       ErrorSeverity,                    
       ErrorProcedure,                    
       ErrorLine,                    
       ErrorMessage,                    
       ErrorDate)                    
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());                    
                  
 set @ErrorMsg = 'Error while updating StagingMarketSizing, please contact Admin for more details.';                  
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine                  
,Message =@ErrorMsg,Success = 0;                     
                     
END CATCH                    
              
              
end