﻿using Avira.MarketingResearch.Common.IService.Masters;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces.Masters;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;

namespace Avira.MarketingResearch.Common.Service_Implementation.Masters
{
    public class FundamentalService : IFundamentalService
    {
        private readonly IFundamentalRepository _fundamentalRepository;

        public FundamentalService(IFundamentalRepository fundamentalRepository)
        {
            _fundamentalRepository = fundamentalRepository;
        }

        public Result<SpTransactionMessage> Create(FundamentalInsertDbViewModel tfc, AviraUser currentUser)
        {
            return _fundamentalRepository.Create(tfc);
        }


        public IEnumerable<FundamentalViewModel> GetAll(Guid Id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<FundamentalViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false)
        {
            return _fundamentalRepository.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName,Search, isDeletedInclude);
        }

        public FundamentalViewModel GetById(Guid guid)
        {
            return _fundamentalRepository.GetById(guid);
        }


        public Result<SpTransactionMessage> Update(FundamentalUpdateDbViewModel tfc, AviraUser currenUser)
        {
            return _fundamentalRepository.Update(tfc);
        }
    }
}
