﻿using System;
using System.Collections.Generic;
using System.Text;
using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;

namespace Avira.MarketingResearch.Common.Service_Implementation
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public Result<SpTransactionMessage> Create(AviraUserInsertDbViewModel aviraUserInsertDbViewModel, AviraUser currentUser)
        {
            return _userRepository.Create(aviraUserInsertDbViewModel);
        }

        public IEnumerable<LocationViewModel> GetAllLocation()
        {
            return _userRepository.GetAllLocation();
        }

        public IEnumerable<RoleViewModel> GetAllRoles()
        {
            return _userRepository.GetAllRoles();
        }

        public AviraUser GetByEmail(string email)
        {
            return _userRepository.GetByEmail(email);
        }

        public PermissionsByUserRoleIdViewModel GetPermissionsByUserRoleId(Guid id)
        {
            return _userRepository.GetPermissionsByUserRoleId(id);
        }

        public IEnumerable<RoleViewModel> GetRolesByAviraUserId(Guid aviraUserId)
        {
            return _userRepository.GetRolesByAviraUserId(aviraUserId);

        }

        public AviraUser GetUserLogin(string userName)
        {
            return _userRepository.GetUserLogin(userName);
        }

        public AviraUser UpdateUserLogin(string userName, bool IsSuccess)
        {
            return _userRepository.UpdateUserLogin(userName, IsSuccess);
        }

        public IEnumerable<AviraUser> GetAllUsersByRole(string roleName)
        {
            return _userRepository.GetAllUsersByRole(roleName);
        }

        public IEnumerable<GetAllAviraUsers> GetAll(Guid? Id, int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false)
        {
            return _userRepository.GetAll(Id,PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude);
        }

        public AviraUser GetByUserId(string userId)
        {
            return _userRepository.GetByUserId(userId);
        }
    }
}
