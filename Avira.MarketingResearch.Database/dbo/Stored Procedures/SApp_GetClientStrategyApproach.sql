﻿CREATE PROCEDURE [dbo].[SApp_GetClientStrategyApproach]        
 (        
  @includeDeleted bit = 0        
 )        
 AS        
/*================================================================================        
Procedure Name: [SApp_GetClientStrategyApproach]        
Author: Laxmikant        
Create date: 05/07/2019        
Description: Get list of Client Strategy Approach         
Change History        
Date  Developer  Reason        
__________ ____________ ______________________________________________________        
05/07/2019 Gopi   Initial Version        
================================================================================*/        
        
BEGIN        
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED    
	SELECT [Id],[ApproachName] 
	FROM [dbo].[Approach]
where ([Approach].IsDeleted = 0)         
END