﻿CREATE TABLE [dbo].[ClientStrategyCategory] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [CategoryName]     NVARCHAR (256)   CONSTRAINT [DF_ClientStrategyCategory_CategoryName] DEFAULT ('') NOT NULL,
    [Description]      NVARCHAR (256)   NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_ClientStrategyCategory_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_ClientStrategyCategory] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ClientStrategyCategory_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_ClientStrategyCategory_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id])
);

