﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public class TrendProjectMappingQueries
    {
        public static readonly string StoreProcInsert = "SApp_InsertUpdateTrendProjectMap";

        public static readonly string StoreProcSelectById = "SApp_GetProjectByTrendID";
    }
}
