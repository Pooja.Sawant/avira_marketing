﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Web.Helpers;
using Avira.MarketingResearch.Web.Utils;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;

namespace Avira.MarketingResearch.Web.Controllers
{
    [Authorize]
    public class CompanyFundamentalController : BaseController
    {
        public readonly IServiceRepository _serviceRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;
        string contentRootPath = string.Empty;
        private ILoggerManager _logger;
        private IAPIHelper _iAPIHelper;


        public CompanyFundamentalController(IAPIHelper iAPIHelper, IServiceRepository serviceRepository, ILoggerManager logger, IHostingEnvironment hostingEnvironment, IConfiguration configuration)
        {
            _hostingEnvironment = hostingEnvironment;
            _serviceRepository = serviceRepository;
            _logger = logger;
            _configuration = configuration;
            contentRootPath = _hostingEnvironment.ContentRootPath;
            _iAPIHelper = iAPIHelper;
        }

        CompanyDetailsInsertUpdateViewModel model = new CompanyDetailsInsertUpdateViewModel();

        public IActionResult Index(string CompanyId, string ProjectId, bool IsNewCompany)
        {
            try
            {
                model = GetCompanyDetialsModel(CompanyId, ProjectId);

                GetAllSubsidary(model.CompanyId.ToString());
                BindDropDowns(CompanyId);

                if (model.KeyEmployee == null || model.KeyEmployee.Count == 0)
                {
                    List<KeyEmployees> objList = new List<KeyEmployees>();
                    KeyEmployees objempty = new KeyEmployees();
                    objempty.KeyEmployeeName = "";
                    objempty.KeyEmployeeComments = "";
                    objList.Add(objempty);
                    model.KeyEmployee = objList;
                }

                if (model.BoardOfDirector == null || model.BoardOfDirector.Count == 0)
                {
                    List<BoardOfDirectors> objList = new List<BoardOfDirectors>();
                    BoardOfDirectors objempty = new BoardOfDirectors();
                    objempty.CompanyBoardOfDirecorsName = "";
                    objempty.KeyEmployeeComments = "";
                    objList.Add(objempty);
                    model.BoardOfDirector = objList;
                }

                if (model.ShareHoldingPattern == null || model.ShareHoldingPattern.Count == 0)
                {
                    List<ShareHolding> objList = new List<ShareHolding>();
                    ShareHolding objempty = new ShareHolding();
                    objempty.ShareHoldingName = "";
                    objempty.ShareHoldingPercentage = Convert.ToDecimal(0);
                    objList.Add(objempty);
                    model.ShareHoldingPattern = objList;
                }

                if (model.SubsidaryCompanyDetails == null || model.SubsidaryCompanyDetails.Count == 0)
                {
                    List<CompanySubsidaryDetails> objList = new List<CompanySubsidaryDetails>();
                    CompanySubsidaryDetails objempty = new CompanySubsidaryDetails();
                    objempty.CompanySubsidaryId = new Guid();
                    objempty.SubsidaryCompanyName = "";
                    objempty.SubsidaryCompanyLocation = "";
                    objempty.IsHeadQuarters = false;
                    objList.Add(objempty);
                    model.SubsidaryCompanyDetails = objList;
                }

                CompanyViewModel companyViewModel = new CompanyViewModel();
                if (!string.IsNullOrEmpty(CompanyId))
                {
                    HttpResponseMessage CompanyResponse = _serviceRepository.GetByIdResponse("Company", CompanyId);
                    companyViewModel = CompanyResponse.Content.ReadAsAsync<CompanyViewModel>().Result;
                }
                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, new Guid(ProjectId));
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.CompanyName = companyViewModel.CompanyName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                }

                ViewBag.CompanyId = CompanyId;
                ViewBag.ProjectId = ProjectId;
                ViewBag.CompanyName = model.CompanyName;
                var baseURl = _iAPIHelper.GetBaseURL();
                if (baseURl != null)
                    ViewBag.ApiBaseURL = baseURl;

                //model.IsNewCompany = model.CompanyId == Guid.Empty;

                model.IsNewCompany = IsNewCompany;

                ViewBag.Message = TempData["Message"];
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult Index(CompanyDetailsInsertUpdateViewModel model, string actionName, IFormCollection collection)
        {
            GetAllSubsidary(model.CompanyId.ToString());
            BindDropDowns(model.CompanyId.ToString());
            var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            bool bResult = false;
            model.CompanyId = model.IsNewCompany ? Guid.Empty : model.CompanyId;
            if (ModelState.IsValid)
            {
                try
                {
                    if (model.File != null)
                    {
                        using (var memoryStream = new MemoryStream())
                        {
                            model.File.CopyToAsync(memoryStream);
                            model.fileByte = memoryStream.ToArray();
                        }
                        model.ImageDisplayName = model.File.FileName;
                        model.fileExtension = model.File.FileName.Substring(model.File.FileName.LastIndexOf('.')).ToLower();

                    }

                    if (actionName.Equals("Save"))
                    {
                        bResult = validate(model);
                        if (bResult)
                        {
                            model.File = null;
                            // model.CompanyNoteModel.CompanyStatusName = "Draft";
                            HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                            responseSave.EnsureSuccessStatusCode();
                            ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                            if (viewModel != null && viewModel.IsSuccess)
                            {
                                TempData["Message"] = viewModel.Message;
                                return RedirectToAction("Index", new { CompanyId = viewModel.Id, ProjectId = model.ProjectId.ToString() });
                            }
                            else if (viewModel != null)
                            {
                                ViewBag.Message = viewModel.Message;
                            }
                        }
                    }
                    else if (actionName.Equals("Submitted"))
                    {
                        //model.CompanyNoteModel.CompanyStatusName = "Submitted";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null && viewModel.IsSuccess)
                        {
                            TempData["Message"] = viewModel.Message;
                            return RedirectToAction("Index", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                        }

                    }
                    else if (actionName.Equals("Rejected"))
                    {
                        //mainModel.CompanyNoteViewModel.CompanyStatusName = "Rejected";
                        HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, model);
                        response.EnsureSuccessStatusCode();
                        ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            //ViewBag.Message = viewModel.Message;
                            return RedirectToAction("Index", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                        }
                    }
                    else if (actionName.Equals("Next"))
                        return RedirectToAction("Index", "CompanyOrganization", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                    else if (actionName.Equals("Previous"))
                        return RedirectToAction("Index", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                    else if (actionName.Equals("Cancel"))
                        return RedirectToAction("Index", "#", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                    else if (actionName.Equals("Clear"))
                        return RedirectToAction("Index", "#", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                    else if (actionName.Equals("AddRow"))
                    {
                        KeyEmployees objempty = new KeyEmployees();
                        model.KeyEmployee.Add(objempty);
                        model.KeyEmployee = model.KeyEmployee ?? new List<KeyEmployees>();
                    }
                    else if (actionName.Equals("AddBoD"))
                    {
                        BoardOfDirectors objList = new BoardOfDirectors();
                        model.BoardOfDirector.Add(objList);
                        model.BoardOfDirector = model.BoardOfDirector ?? new List<BoardOfDirectors>();
                    }
                    else if (actionName.Equals("AddRowShare"))
                    {
                        ShareHolding objList = new ShareHolding();
                        model.ShareHoldingPattern.Add(objList);
                        model.ShareHoldingPattern = model.ShareHoldingPattern ?? new List<ShareHolding>();
                    }
                    else if (actionName.Equals("AddCompany"))
                    {
                        CompanySubsidaryDetails company = new CompanySubsidaryDetails();
                        model.SubsidaryCompanyDetails.Add(company);
                        model.SubsidaryCompanyDetails = model.SubsidaryCompanyDetails ?? new List<CompanySubsidaryDetails>();
                    }
                    else
                    {
                        ViewBag.CompanyId = model.CompanyId;
                        ViewBag.ProjectId = model.ProjectId;

                        var baseURl = _iAPIHelper.GetBaseURL();
                        if (baseURl != null)
                            ViewBag.ApiBaseURL = baseURl;


                        HttpResponseMessage CompanyResponse = _serviceRepository.GetByIdResponse("Company", model.CompanyId.ToString());
                        CompanyViewModel companyViewModel = CompanyResponse.Content.ReadAsAsync<CompanyViewModel>().Result;
                        ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, model.ProjectId);
                        if (project != null)
                        {
                            ViewBag.ProjectId = project.Id;
                            ViewBag.ProjectName = project.ProjectName;
                            ViewBag.CompanyName = companyViewModel.CompanyName;
                            ViewBag.ProjectCode = project.ProjectCode;
                            ViewBag.Approver = project.Approver;
                            ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                            ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                        }

                    }

                    model.IsNewCompany = model.CompanyId == Guid.Empty;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                }
            }
            return View("Index", model);
        }

        private void BindDropDowns(string CompanyId)
        {
            var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            try
            {
                HttpResponseMessage designationsResponse = _serviceRepository.GetAllDesignationsResponse(contollerName);
                designationsResponse.EnsureSuccessStatusCode();
                List<DesignationViewModel> designationsList = designationsResponse.Content.ReadAsAsync<List<DesignationViewModel>>().Result;
                if (designationsList.Count > 0)
                {
                    ViewBag.DesignationsList = new SelectList(designationsList, "DesignationId", "Designation");
                }


                HttpResponseMessage companiesResponse = _serviceRepository.GetAllCompaniesResponse(contollerName);
                companiesResponse.EnsureSuccessStatusCode();
                List<AllCompaniesListViewModel> companiesList = companiesResponse.Content.ReadAsAsync<List<AllCompaniesListViewModel>>().Result;

                if (companiesList.Count > 0)
                {
                    ViewBag.CompaniesList = new SelectList(companiesList.OrderBy(e => e.CompanyName), "CompanyId", "CompanyName");
                }
                HttpResponseMessage companyStagesResponse = _serviceRepository.GetAllCompanyStagesResponse(contollerName);
                companyStagesResponse.EnsureSuccessStatusCode();
                List<AllCompanyStageListViewModel> companyStagesList = companyStagesResponse.Content.ReadAsAsync<List<AllCompanyStageListViewModel>>().Result;
                if (companyStagesList.Count > 0)
                {
                    ViewBag.CompanyStagesList = new SelectList(companyStagesList, "Id", "CompanyStageName");
                }

                HttpResponseMessage companyFundamentalResponse = _serviceRepository.GetCompanyFundamentalResponse(contollerName);
                companyFundamentalResponse.EnsureSuccessStatusCode();
                List<CompanyFundamentalViewModel> companyFundamentalList = companyFundamentalResponse.Content.ReadAsAsync<List<CompanyFundamentalViewModel>>().Result;
                if (companyFundamentalList.Count > 0)
                {
                    ViewBag.CompanyFundamentalList = companyFundamentalList;
                }

                HttpResponseMessage companyRegionResponse = _serviceRepository.GetCompanyRegionResponse(contollerName);
                companyRegionResponse.EnsureSuccessStatusCode();
                List<CompanyRegionViewModel> companyRegionList = companyRegionResponse.Content.ReadAsAsync<List<CompanyRegionViewModel>>().Result;

                List<SelectListItem> listItems = new List<SelectListItem>();
                foreach (var item in companyRegionList)
                {
                    listItems.Add(new SelectListItem
                    {
                        Text = item.RegionName,
                        Value = item.RegionId.ToString(),
                        //Selected = companyRegionList.Where(s => s.RegionId.ToString() == );
                    });
                }


                if (companyRegionList != null && companyRegionList.Count > 0)
                {
                    ViewBag.CompanyRegionList = new SelectList(companyRegionList, "RegionId", "RegionName");
                }

                HttpResponseMessage companyEntityResponse = _serviceRepository.GetCompanyEntityResponse(contollerName);
                companyEntityResponse.EnsureSuccessStatusCode();
                List<CompanyEntityViewModel> companyEntitynList = companyEntityResponse.Content.ReadAsAsync<List<CompanyEntityViewModel>>().Result;
                if (companyEntitynList != null && companyEntitynList.Count > 0)
                {
                    ViewBag.CompanyEntitynList = new SelectList(companyEntitynList, "Id", "EntityTypeName");
                }

                HttpResponseMessage companyNatureResponse = _serviceRepository.GetCompanyNatureResponse(contollerName);
                companyNatureResponse.EnsureSuccessStatusCode();
                List<CompanyNatureViewModel> companyNatureList = companyNatureResponse.Content.ReadAsAsync<List<CompanyNatureViewModel>>().Result;
                if (companyNatureList.Count > 0)
                {
                    //ViewBag.CompanyNatureList = new SelectList(companyNatureList, "NatureId", "NatureType");
                    ViewBag.CompanyNatureList = companyNatureList;
                }

                HttpResponseMessage companyManagerResponse = _serviceRepository.GetCompanyManagerResponse(contollerName, CompanyId);
                companyManagerResponse.EnsureSuccessStatusCode();
                List<CompanyManagerViewModel> companyManagerList = companyManagerResponse.Content.ReadAsAsync<List<CompanyManagerViewModel>>().Result;
                if (companyManagerList.Count > 0)
                {
                    ViewBag.CompanyManagerList = new SelectList(companyManagerList, "ManagerId", "ManagerName");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        private void GetAllSubsidary(string CompanyId)
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage companySubsidaryResponse = _serviceRepository.GetCompanySubsidaryResponse(contollerName, CompanyId);
                companySubsidaryResponse.EnsureSuccessStatusCode();
                List<CompanySubsidaryViewModel> companySubsidaryList = companySubsidaryResponse.Content.ReadAsAsync<List<CompanySubsidaryViewModel>>().Result;
                if (companySubsidaryList.Count > 0)
                {
                    ViewBag.CompanySubsidaryList = new SelectList(companySubsidaryList.OrderBy(e => e.CompanySubsidaryLocation), "Id", "CompanySubsidaryLocation");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        private void GetCompanyProfile(string CompanyId, string ProjectId)
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage responseStrength = _serviceRepository.GetCompanyDetailsByGuIdsResponse(contollerName, CompanyId, ProjectId);
                responseStrength.EnsureSuccessStatusCode();
                CompanyProfileDbViewModel companyProfileDbViewModel = responseStrength.Content.ReadAsAsync<CompanyProfileDbViewModel>().Result;
                if (companyProfileDbViewModel != null)
                {
                    ViewBag.CompanyProfileDbViewModel = companyProfileDbViewModel;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        private CompanyDetailsInsertUpdateViewModel GetCompanyDetialsModel(string CompanyId, string ProjectId)
        {
            CompanyDetailsInsertUpdateViewModel companyProfileDbViewModel = new CompanyDetailsInsertUpdateViewModel();
            try
            {
                if (!string.IsNullOrEmpty(CompanyId))
                {
                    var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                    HttpResponseMessage responseStrength = _serviceRepository.GetCompanyDetailsByGuIdsResponse(contollerName, CompanyId, ProjectId);
                    responseStrength.EnsureSuccessStatusCode();
                    companyProfileDbViewModel = responseStrength.Content.ReadAsAsync<CompanyDetailsInsertUpdateViewModel>().Result;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return companyProfileDbViewModel;
        }


        private bool validate(CompanyDetailsInsertUpdateViewModel model)
        {
            int i = 0;
            int BODCount = model.BoardOfDirector.Count;
            int KeyCount = model.KeyEmployee.Count;
            int ShareCount = model.ShareHoldingPattern.Count;
            int flag = 0;
            do
            {
                if (!model.IsNewCompany && (model.CompanyId == null || model.CompanyId == Guid.Empty))
                {
                    ViewBag.Message = "Please select Company Name.";
                    flag++;
                    break;
                }
                else
                    if (model.IsNewCompany && string.IsNullOrEmpty(model.CompanyName))
                    {
                        ViewBag.Message = "Please enter Company Name.";
                        flag++;
                        break;
                    }                
                else if (model.NatureId == null || model.NatureId == Guid.Empty)
                {
                    ViewBag.Message = "Please select type of nature.";
                    flag++;
                    break;
                }

                else if (model.NatureId == null)
                {
                    ViewBag.Message = "Please select type of Nature.";
                    flag++;
                    break;
                }
                //else if (model.CompanyLocation == String.Empty || model.CompanyLocation == "" || model.CompanyLocation == null)
                //{
                //    ViewBag.Message = "Please enter Location HQ";
                //    flag++;
                //    break;
                //}
                //else if (model.Website == String.Empty || model.Website == "" || model.Website == null)
                //{
                //    ViewBag.Message = "Please enter Website";
                //    flag++;
                //    break;
                //}
                //else if (model.YearOfEstb == null || model.YearOfEstb == 0)
                //{
                //    ViewBag.Message = "Please enter Founded Year";
                //    flag++;
                //    break;
                //}
                //else if (model.CompanyStageId == String.Empty || model.CompanyStageId == "" || model.CompanyStageId == null)
                //{
                //    ViewBag.Message = "Please select Company Stage";
                //    flag++;
                //    break;
                //}
                else if (model.RegionIdArrays == null)
                {
                    ViewBag.Message = "Please select Geography";
                    flag++;
                    break;
                }
                //else if (model.NoOfEmployees == 0)
                //{
                //    ViewBag.Message = "Please enter No. of Employees";
                //    flag++;
                //    break;
                //}

                //else if (model.KeyEmployee.FirstOrDefault().KeyEmployeeName == null || model.KeyEmployee.FirstOrDefault().KeyEmployeeEmailId != null)
                //{
                //    bool isEmail = Regex.IsMatch(model.KeyEmployee.FirstOrDefault().KeyEmployeeEmailId, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
                //    if (model.KeyEmployee.FirstOrDefault().KeyEmployeeName == null)
                //    {
                //        ViewBag.Message = "Please enter key employee name" + model.KeyEmployee.FirstOrDefault().KeyEmployeeName;
                //        flag++;
                //        break;
                //    }
                //    else if (isEmail == false)
                //    {
                //        ViewBag.Message = "Invalid email" + model.KeyEmployee.FirstOrDefault().KeyEmployeeEmailId;
                //        flag++;
                //        break;
                //    }
                //}

                //if (model.BoardOfDirector.FirstOrDefault().KeyEmployeeName == null || model.BoardOfDirector.FirstOrDefault().KeyEmployeeEmailId != null)
                //{
                //    bool isEmail = Regex.IsMatch(model.BoardOfDirector.FirstOrDefault().KeyEmployeeEmailId, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
                //    if (model.BoardOfDirector.FirstOrDefault().KeyEmployeeName == null)
                //    {
                //        ViewBag.Message = "Please enter Board company name" + model.BoardOfDirector.FirstOrDefault().KeyEmployeeName;
                //        flag++;
                //        break;
                //    }

                //    //else if (model.BoardOfDirector.FirstOrDefault().DesignationId.ToString() == "00000000-0000-0000-0000-000000000000")
                //    //{
                //    //    ViewBag.Message = "Please enter Board company name" + model.BoardOfDirector.FirstOrDefault().KeyEmployeeName;
                //    //    flag++;
                //    //    break;
                //    //}

                //    else if (isEmail == false)
                //    {
                //        ViewBag.Message = "Invalid email" + model.BoardOfDirector.FirstOrDefault().KeyEmployeeEmailId;
                //        flag++;
                //        break;
                //    }
                //}

                //if (model.RankNumber == null)
                //{
                //    ViewBag.Message = "Please give company rank";
                //    flag++;
                //    break;
                //}

                //else if (model.ShareHoldingPattern.FirstOrDefault().ShareHoldingName == null)
                //{
                //    ViewBag.Message = "Please give Share Holding Name";
                //    flag++;
                //    break;
                //}

                //else if (model.ShareHoldingPattern.FirstOrDefault().ShareHoldingPercentage == 0)
                //{
                //    ViewBag.Message = "Please give Share Holding Percentage";
                //    flag++;
                //    break;
                //}
                //if (model.Email != null)
                //{
                //    bool isEmail = Regex.IsMatch(model.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
                //    if (isEmail == false)
                //    {
                //        ViewBag.Message = "Invalid email" + model.Email.ToString();
                //        flag++;
                //        break;
                //    }
                //}
                //if (model.Email2 != null)
                //{
                //    bool isEmail = Regex.IsMatch(model.Email2, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
                //    if (isEmail == false)
                //    {
                //        ViewBag.Message = "Invalid email" + model.Email2.ToString();
                //        flag++;
                //        break;
                //    }
                //}
                //if (model.Email3 != null)
                //{
                //    bool isEmail = Regex.IsMatch(model.Email3, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
                //    if (isEmail == false)
                //    {
                //        ViewBag.Message = "Invalid email" + model.Email3.ToString();
                //        flag++;
                //        break;
                //    }
                //}
                //if (model.File == null && model.ImageDisplayName == null && model.ImageURL == null)
                //{
                //    ViewBag.Message = "Please select Company Logo";
                //    flag++;
                //    break;
                //}

                i++;
                if (i > KeyCount - 1 && i > BODCount - 1 && i > ShareCount - 1)
                    break;

            } while (true);
            if (flag > 0)
                return false;
            else
                return true;
        }
    }
}

