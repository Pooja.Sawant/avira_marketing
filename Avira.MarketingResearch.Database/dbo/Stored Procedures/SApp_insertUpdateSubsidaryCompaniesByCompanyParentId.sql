﻿

CREATE procedure [dbo].[SApp_insertUpdateSubsidaryCompaniesByCompanyParentId]      
(
	@CompanyId uniqueidentifier,   
	@CompanySubsidaryList [dbo].[udtGUIDCompanySubsidaryDetails] readonly,
	@UserCreatedById uniqueidentifier
)      
as      
/*================================================================================      
Procedure Name: [SApp_insertUpdateSubsidaryCompaniesByCompanyParentId]      
Author: Sai Krishna      
Create date: 05/06/2019      
Description: Insert/Update a sub company list into Company table      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
05/06/2019 Sai Krishna   Initial Version    
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)  

--select Id from company where ParentId = '775dfcf4-32ef-467d-8116-2e0f2f4be11e'



BEGIN TRY         
	Update [dbo].[Company]
		Set CompanyName = list.Name,
			CompanyLocation = list.Location,
			IsHeadQuarters = list.bitValue,
			ParentId = @CompanyId,
			ModifiedOn = GETDATE(),
			UserModifiedById = @UserCreatedById
		from [dbo].Company
		inner join @CompanySubsidaryList list 
		on list.Id = Company.Id;

	update Company
	set ParentId = null,
			ModifiedOn = GETDATE(),
			UserModifiedById = @UserCreatedById
	from [dbo].Company
	where ParentId = @CompanyId
	and not exists (select 1 from @CompanySubsidaryList list 
		where list.Id = Company.Id);

SELECT StoredProcedureName ='SApp_insertUpdateSubsidaryCompaniesByCompanyParentId',Message =@ErrorMsg,Success = 1;       
    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);      
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());      
    
 set @ErrorMsg = 'Error while insert a new TrendMarketMap, please contact Admin for more details.';    
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
       
END CATCH      
      
      
end