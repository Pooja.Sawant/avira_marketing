﻿using Avira.MarketingResearch.Common.IService.ImportExport;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.ImportExport
{
    public class SqlTableSchemaService : ISqlTableSchemaService
    {
        private readonly ISqlTableSchemaRepository _sqlTableSchemaRepository;

        public SqlTableSchemaService(ISqlTableSchemaRepository sqlTableSchemaRepository)
        {
            _sqlTableSchemaRepository = sqlTableSchemaRepository;
        }
        public DataTable GetTableSchema(SchemaModel model)
        {
            return _sqlTableSchemaRepository.GetTableSchema(model);
        }
    }
}
