﻿CREATE TABLE [dbo].[StagingCompanyBasicRevenue] (
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [ImportId]          UNIQUEIDENTIFIER NULL,
    [RowNo]             INT              CONSTRAINT [DF_StagingCompanyBasicRevenue_RowNo] DEFAULT ((0)) NOT NULL,
    [CompanyName]       NVARCHAR (255)   NULL,
    [Currency]          NVARCHAR (255)   NULL,
    [Units]             NVARCHAR (255)   NULL,
    [FinanicialYear]    NVARCHAR (255)   NULL,
    [Revenue]           NVARCHAR (255)   NULL,
    [UserCreatedById]   NVARCHAR (255)   NULL,
    [CompanyId]         NVARCHAR (255)   NULL,
    [CurrencyId]        NVARCHAR (255)   NULL,
    [ValueConversionId] NVARCHAR (255)   NULL,
    [ErrorNotes]        NVARCHAR (1050)  NULL,
    CONSTRAINT [PK_StagingCompanyBasicRevenue] PRIMARY KEY CLUSTERED ([Id] ASC)
);

