﻿
CREATE PROCEDURE [dbo].[SApp_GetEcoSystemPresenceByCompanyID]            
 (@CompanyID uniqueidentifier = null)  
 AS            
/*================================================================================            
Procedure Name:[SApp_GetEcoSystemPresenceByCompanyID]       
Author: Harshal            
Create date: 05/20/2019            
Description: Get list from EcoSystem table by companyId            
Change History            
Date  Developer  Reason            
__________ ____________ ______________________________________________________            
04/22/2019 Harshal   Initial Version 
================================================================================*/           
            
BEGIN            
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED         

begin        
      
 SELECT         
   Segment.[Id] AS [EcoSystemId],            
   Segment.SegmentName as [EcoSystemName],           
   Segment.IsDeleted,        
   CompanyEcosystemPresenceESMMap.isChecked,      
   CompanyEcosystemPresenceESMMap.CompanyId,      
   CompanyEcosystemPresenceESMMap.Id,       
  cast (case when CompanyEcosystemPresenceESMMap.Id is not null then 1 else 0 end as bit) as IsMapped       

FROM [dbo].Segment      
LEFT JOIN dbo.CompanyEcosystemPresenceESMMap       
ON Segment.Id = CompanyEcosystemPresenceESMMap.ESMId
and CompanyEcosystemPresenceESMMap.IsDeleted = 0
and CompanyEcosystemPresenceESMMap.CompanyId = @CompanyID
where ( Segment.IsDeleted = 0)        

END       
        
END