﻿


CREATE procedure [dbo].[SApp_InsertUpdateCompanyProductSegmentMap]      
(
@CompanyProductId uniqueidentifier,   
@CompanyProductSegmentMap dbo.[udtGUIDGUIDMapValue] readonly,
@AviraUserId uniqueidentifier
)      
as      
/*================================================================================      
Procedure Name: SApp_InsertUpdateCompanyProductSegmentMap     
Author: Harshal      
Create date: 04/22/2019      
Description: Insert a record into CompanyProductSegmentMap table      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
04/22/2019 Harshal   Initial Version    
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)  
    
BEGIN TRY         
--mark records as delete which are not in list
update [dbo].[CompanyProductSegmentMap]
set IsDeleted = 1,
UserDeletedById = @AviraUserId,
DeletedOn = GETDATE()
from [dbo].[CompanyProductSegmentMap]
where [CompanyProductSegmentMap].CompanyProductId = @CompanyProductId
and not exists (select 1 from @CompanyProductSegmentMap map where map.id = [CompanyProductSegmentMap].SegmentId)

update [dbo].[CompanyProductSegmentMap]
set SegmentId = map.mapid,
ModifiedOn = GETDATE(),
UserModifiedById = @AviraUserId
from [dbo].[CompanyProductSegmentMap]
inner join @CompanyProductSegmentMap map 
on map.id = [CompanyProductSegmentMap].SegmentId
where [CompanyProductSegmentMap].CompanyProductId = @CompanyProductId;

--Insert new records
insert into [dbo].[CompanyProductSegmentMap]([Id],      
        [CompanyProductId],      
        [SegmentId], 
        [IsDeleted],      
        [UserCreatedById],      
        [CreatedOn])      
 select NEWID(),
   @CompanyProductId,       
   map.ID as [SegmentId],
   0,      
   @AviraUserId,       
   GETDATE()
 from @CompanyProductSegmentMap map
 where not exists (select 1 from [dbo].[CompanyProductSegmentMap] map1
					where map1.CompanyProductId = @CompanyProductId
					and map.ID = map1.SegmentId);       
     
SELECT StoredProcedureName ='SApp_InsertUpdateCompanyProductSegmentMap ',Message =@ErrorMsg,Success = 1;       
    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar;      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar;      
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());      
    
 set @ErrorMsg = 'Error while insert a new CompanyProductSegmentMap, please contact Admin for more details.';    
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
       
END CATCH      
      
      
end