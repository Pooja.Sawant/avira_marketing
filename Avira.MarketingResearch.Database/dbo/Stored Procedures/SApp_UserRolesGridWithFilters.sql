﻿

Create PROCEDURE [dbo].[SApp_UserRolesGridWithFilters]
	(@UserRolesIDList dbo.udtUniqueIdentifier READONLY,
	@UserRoleName nvarchar(256)='',
	@includeDeleted bit = 0,
	@OrdbyByColumnName NVARCHAR(50) ='CreatedOn',
	@SortDirection INT = -1,
	@PageStart INT = 0,
	@PageSize INT = null)
	AS
/*================================================================================
Procedure Name: SApp_UserRolesGridWithFilters
Author: Gopi
Create date: 04/23/2019
Description: Get list from UserRole table by Name or ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
04/23/2019	Sai Krishna			Initial Version
================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
if @includeDeleted is null set @includeDeleted = 0;
if len(@UserRoleName)>2 set @UserRoleName = '%'+ @UserRoleName + '%'
else set @UserRoleName = ''
	if @PageSize is null or @PageSize =0
	set @PageSize = 10
	begin
	;WITH CTE_TBL AS (

	SELECT [UserRole].[Id],
		[UserRole].[UserRoleName],
		[UserRole].[IsDeleted],
		CASE
		WHEN @OrdbyByColumnName = 'UserRoleName' THEN [UserRole].UserRoleName
		ELSE [UserRole].UserRoleName
	END AS SortColumn

FROM [dbo].[UserRole]
where (NOT EXISTS (SELECT * FROM @UserRolesIDList) or  [UserRole].Id  in (select * from @UserRolesIDList))
	AND (@UserRoleName = '' or [UserRole].[UserRoleName] like @UserRoleName)
	AND (@includeDeleted = 1 or [UserRole].IsDeleted = 0)
	)

	SELECT 
		  CTE_TBL.*,
		  tCountResult.TotalItems
		FROM CTE_TBL
		CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult
		ORDER BY 
		  CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,
		  CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC 
		OFFSET @PageStart ROWS
		FETCH NEXT @PageSize ROWS ONLY;
		END
	END