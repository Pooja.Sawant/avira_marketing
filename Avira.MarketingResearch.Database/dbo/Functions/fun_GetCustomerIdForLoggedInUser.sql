﻿/*================================================================================  
Function Name: [fun_GetCustomerIdForLoggedInUser]  
Author: Nitin Tarkar  
Create date: 05-Sep-2019  
Description: Get CustomerID for logged in User
Change History  
Date        Developer     Reason  
__________  ____________  ______________________________________________________  
05-Sep-2019 Nitin Tarkar   Initial Version  
================================================================================*/ 
CREATE   FUNCTION fun_GetCustomerIdForLoggedInUser 
(
	@UserId uniqueidentifier
)
RETURNS uniqueidentifier
AS
Begin

	Declare @CustomerId uniqueidentifier = null
	
	Select @CustomerId = CustomerId
	From AviraUser
	Where Id = @UserId

	return @CustomerId
END