﻿using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.CompanyProfile
{
    public class CompanySWOTService : ICompanySWOTService
    {
        private readonly ICompanySwotRepository _iCompanySwotRepository;
        private readonly ICompanyNoteRepository _iCompanyNoteRepository;

        public CompanySWOTService(ICompanySwotRepository ICompanySwotRepository, ICompanyNoteRepository iCompanyNoteRepository)
        {
            _iCompanySwotRepository = ICompanySwotRepository;
            _iCompanyNoteRepository = iCompanyNoteRepository;
        }

        public MainCompanySWOTViewModel GetAll(Guid CompanyId, Guid projectId)
        {
            MainCompanySWOTViewModel mainCompanySWOTViewModel = new MainCompanySWOTViewModel();
            mainCompanySWOTViewModel.companySWOTOpportunitiesViewModel = new List<CompanySWOTOpportunitiesViewModel>();
            mainCompanySWOTViewModel.companySWOTStrengthViewModel = new List<CompanySWOTStrengthViewModel>();
            mainCompanySWOTViewModel.companySWOTThreatsViewModel = new List<CompanySWOTThreatsViewModel>();
            mainCompanySWOTViewModel.companySWOTWeeknessesViewModel = new List<CompanySWOTWeeknessesViewModel>();
            List<CompanySWOTViewModel> companySWOTViewModel = _iCompanySwotRepository.GetAll(CompanyId, projectId).ToList();
            if (companySWOTViewModel != null && companySWOTViewModel.Count > 0)
            {
                for(int i=0;i< companySWOTViewModel.Count;i++)
                {
                    if(companySWOTViewModel[i].SWOTTypeName== "Opportunities")
                    {
                        mainCompanySWOTViewModel.companySWOTOpportunitiesViewModel.Add(new CompanySWOTOpportunitiesViewModel(){ Id=companySWOTViewModel[i].Id,CompanyId=companySWOTViewModel[i].CompanyId,ProjectId=companySWOTViewModel[i].ProjectId,CategoryId=companySWOTViewModel[i].CategoryId, SWOTTypeId=companySWOTViewModel[i].SWOTTypeId, SWOTTypeName=companySWOTViewModel[i].SWOTTypeName, SWOTDescription=companySWOTViewModel[i].SWOTDescription});
                    }
                    else if(companySWOTViewModel[i].SWOTTypeName == "Strength")
                    {
                        mainCompanySWOTViewModel.companySWOTStrengthViewModel.Add(new CompanySWOTStrengthViewModel() { Id = companySWOTViewModel[i].Id, CompanyId = companySWOTViewModel[i].CompanyId, ProjectId = companySWOTViewModel[i].ProjectId, CategoryId = companySWOTViewModel[i].CategoryId, SWOTTypeId = companySWOTViewModel[i].SWOTTypeId, SWOTTypeName = companySWOTViewModel[i].SWOTTypeName, SWOTDescription = companySWOTViewModel[i].SWOTDescription });
                    }
                    else if(companySWOTViewModel[i].SWOTTypeName == "Threats")
                    {
                        mainCompanySWOTViewModel.companySWOTThreatsViewModel.Add(new CompanySWOTThreatsViewModel() { Id = companySWOTViewModel[i].Id, CompanyId = companySWOTViewModel[i].CompanyId, ProjectId = companySWOTViewModel[i].ProjectId, CategoryId = companySWOTViewModel[i].CategoryId, SWOTTypeId = companySWOTViewModel[i].SWOTTypeId, SWOTTypeName = companySWOTViewModel[i].SWOTTypeName, SWOTDescription = companySWOTViewModel[i].SWOTDescription });
                    }
                    else
                    {
                        mainCompanySWOTViewModel.companySWOTWeeknessesViewModel.Add(new CompanySWOTWeeknessesViewModel() { Id = companySWOTViewModel[i].Id, CompanyId = companySWOTViewModel[i].CompanyId, ProjectId = companySWOTViewModel[i].ProjectId, CategoryId = companySWOTViewModel[i].CategoryId, SWOTTypeId = companySWOTViewModel[i].SWOTTypeId, SWOTTypeName = companySWOTViewModel[i].SWOTTypeName, SWOTDescription = companySWOTViewModel[i].SWOTDescription });
                    }
                }
               
            }
            return mainCompanySWOTViewModel;
        }

        public Result<SpTransactionMessage> InsertUpdate(MainCompanySWOTViewModel viewModel, AviraUser currenUser)
        {
            var res = _iCompanySwotRepository.InsertUpdate(viewModel, currenUser);
            viewModel.CompanyNoteViewModel.Id = Guid.NewGuid();
            viewModel.CompanyNoteViewModel.UserCreatedById = currenUser.Id;
            viewModel.CompanyNoteViewModel.CompanyId = viewModel.CompanyId;
            viewModel.CompanyNoteViewModel.ProjectId = viewModel.ProjectId;
            _iCompanyNoteRepository.Insert(viewModel.CompanyNoteViewModel);

            return res;
        }
    }
}
