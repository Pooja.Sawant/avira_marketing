﻿



CREATE procedure [dbo].[SApp_InsertUpdateTrendProjectMap]      
(
@TrendId uniqueidentifier,   
@TrendProjectmap dbo.[udtGUIDGUIDMapValue] readonly,
@AviraUserId uniqueidentifier
)      
as      
/*================================================================================      
Procedure Name: SApp_InsertTrendProjectMap      
Author: Swami      
Create date: 04/18/2019      
Description: Insert a record into TrendProjectMap table      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
04/18/2019 Sai Krishna   Initial Version    
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)  
    
--Validations      
--IF exists(select 1 from [dbo].[TrendProjectMap]       
--   where TrendId = @TrendId AND ProjectId = @ProjectId AND IsDeleted = 0)      
--begin       
--set @ErrorMsg = 'Project with Id "'+ CONVERT (nvarchar(30), @ProjectId) + '" already exists.';    
--SELECT StoredProcedureName ='SApp_InsertTrendProjectMap',Message =@ErrorMsg,Success = 0;    
--RETURN;    
--end      
--End of Validations      
BEGIN TRY         
--mark records as delete which are not in list
--update [dbo].[TrendProjectMap]
--set IsDeleted = 1,
--UserDeletedById = @AviraUserId,
--DeletedOn = GETDATE()
--from [dbo].[TrendProjectMap]
--where [TrendProjectMap].TrendId = @TrendId
--and not exists (select 1 from @TrendProjectmap map where map.id = [TrendProjectMap].ProjectId)

 --Update records for change
update [dbo].[TrendProjectMap]
set Impact = map.mapid,
EndUser = map.bitvalue,
IsDeleted = CASE WHEN map.mapid IS NULL THEN 1 ELSE IsDeleted END,
ModifiedOn = GETDATE(),
UserModifiedById = @AviraUserId
from [dbo].[TrendProjectMap]
inner join @TrendProjectmap map 
on map.id = [TrendProjectMap].ProjectId
where [TrendProjectMap].TrendId = @TrendId;

--Insert new records
insert into [dbo].[TrendProjectMap]([Id],      
        [TrendId],      
        [ProjectId], 
		[Impact],
		[EndUser],
        [IsDeleted],      
        [UserCreatedById],      
        [CreatedOn])      
 select NEWID(),
   @TrendId,       
   map.ID as [ProjectId],
   map.mapid as [Impact],     
   map.bitvalue,     
   0,      
   @AviraUserId,       
   GETDATE()
 from @TrendProjectmap map
 where not exists (select 1 from [dbo].[TrendProjectMap] map1
					where map1.TrendId = @TrendId
					and map.ID = map1.ProjectId);       
     
SELECT StoredProcedureName ='SApp_InsertTrendProjectMap',Message =@ErrorMsg,Success = 1;       
    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);      
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());      
    
 set @ErrorMsg = 'Error while insert a new TrendProjectMap, please contact Admin for more details.';    
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
       
END CATCH      
      
      
end