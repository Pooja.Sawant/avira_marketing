﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.CompanyProfile
{
    public class CompanyStrategyViewModel
    {
        public Guid Id { get; set; }
        public Guid CompanyId  { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Date   { get; set; }

        public Guid ApproachId { get; set; }
        public string ApproachName { get; set; }
        public string Strategy   { get; set; }
        public Guid CategoryId { get; set; }
        public string CategoryName { get; set; }
        public Guid ImportanceId { get; set; }
        public string ImportanceName { get; set; }
        public Guid ImpactId { get; set; }
        public string ImpactName { get; set; }
        public string Remark   { get; set; } 
        public DateTime CreatedOn   { get; set; }
        public Guid UserCreatedById  { get; set; }
        public DateTime? ModifiedOn   { get; set; }
        public Guid? UserModifiedById   { get; set; }
        public bool? IsDeleted   { get; set; }
        public DateTime? DeletedOn  { get; set; }
        public Guid? UserDeletedById   { get; set; } 
        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "CompanyClientStrategy";
            }
        }
    }
    public class CompanyclientViewModel
    {
        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
        public string ClientName { get; set; }
        public Guid ClientIndustry { get; set; }
        public string  ClientRemark { get; set; }
        public string IndustryName { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "CompanyClientStrategy";
            }
        }
    }

    public class CompanyClientStrategyInsertUpdateDbViewModel
    {
        [Required]
        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
        public Guid ProjectId { get; set; }
        public List<CompanyStrategyViewModel> CompanyStrategyViewModel { get; set; }
        public List<CompanyclientViewModel> CompanyclientViewModel { get; set; }
        public CompanyNoteViewModel CompanyNoteViewModel { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public Guid AviraUserId { get; set; } 
    }
    public class CompanyClientStrategyviewModel
    {
        [Required]
        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
        public List<CompanyStrategyViewModel> CompanyStrategyViewModel { get; set; }
        public List<CompanyclientViewModel> CompanyclientViewModel { get; set; }
        public CompanyNoteViewModel CompanyNoteViewModel { get; set; } 
    }

    public class CompanyClientStrategyDirectionViewModel
    {
        public Guid Id { get; set; }
        public string ImpactDirectionName { get; set; }
        public string ImpactDirectionDescription { get; set; }
        public string RowType
        {
            get
            {
                return "ImpactDirection";
            }
        }
    }
    public class CompanyClientStrategyImportanceViewModel
    {
        public Guid Id { get; set; }
        public string ImportanceName { get; set; }
        public string ImportanceDescription { get; set; } 
    }
    public class CompanyClientStrategyCategoryViewModel
    {
        public Guid Id { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; } 
    }
    public class CompanyClientStrategyApproachViewModel
    {
        public Guid Id { get; set; }
        public string ApproachName { get; set; } 
    }
    public class CompanyClientStrategyIndustryViewModel
    {
        public Guid Id { get; set; }
        public string IndustryName { get; set; }
    }
}        
        