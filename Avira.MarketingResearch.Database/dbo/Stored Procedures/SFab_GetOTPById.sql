﻿
CREATE procedure [dbo].[SFab_GetOTPById]
(@Email nvarchar(100)=null,
@AviraUserID uniqueidentifier = null,
@ClientDeviceId nvarchar(512)=null)
as
/*================================================================================
Procedure Name: SFab_GetOTPById
Author: Pooja Sawant
Create date: 06/07/2019
Description: Get otp details by Id
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
06/07/2019	Pooja			Initial Version
08/31/2019	Praveen			Added DisplayName 
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @DisplayName NVARCHAR(256)
SELECT @DisplayName = DisplayName FROM AviraUser WHERE EmailAddress = @Email
select top 1 *, @DisplayName AS DisplayName from OTPVerification where OtpEntity=@Email and OtpType='Email' and ClientDeviceId=@ClientDeviceId
order by GeneratedDateTime desc
end