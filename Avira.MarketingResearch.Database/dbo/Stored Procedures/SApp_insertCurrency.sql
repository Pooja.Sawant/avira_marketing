﻿

CREATE procedure [dbo].[SApp_insertCurrency]
(@Id uniqueidentifier,
@CurrencyCode nvarchar(10),
@CurrencyName nvarchar(256),
@CurrencySymbol nvarchar(10),
@Fractionalunit nvarchar(50),
@UserCreatedById uniqueidentifier, 
@CreatedOn Datetime)
as
/*================================================================================
Procedure Name: SApp_insertCurrency
Author: Swami
Create date: 10/19/2019
Description: Insert a record into Industry table
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
03/19/2019	Swami			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ErrorMsg NVARCHAR(2048);

--Validations
IF exists(select 1 from [dbo].Currency 
		 where CurrencyName = @CurrencyName AND IsDeleted = 0)
begin
--declare @ErrorMsg NVARCHAR(2048);
set @ErrorMsg = 'Currency with Name "'+ @CurrencyName + '" already exists.';
--THROW 50000,@ErrorMsg,1;
SELECT StoredProcedureName ='SApp_insertCurrency',Message =@ErrorMsg,Success = 0;  
RETURN;  

return(1)
end


IF exists(select 1 from [dbo].Currency 
		 where CurrencyCode = @CurrencyCode AND IsDeleted = 0)
begin
--declare @ErrorMsg NVARCHAR(2048);
set @ErrorMsg = 'Currency with Code "'+ @CurrencyCode + '" already exists.';
--THROW 50000,@ErrorMsg,1;
SELECT StoredProcedureName ='SApp_insertCurrency',Message =@ErrorMsg,Success = 0;  
RETURN;  

return(1)
end
--End of Validations
BEGIN TRY

insert into [dbo].Currency([Id],
								CurrencyCode,
								CurrencyName,
								CurrencySymbol,
								Fractionalunit,
								[IsDeleted],
								[UserCreatedById],
								[CreatedOn])
	values(@Id, 
			@CurrencyCode, 
			@CurrencyName,
			@CurrencySymbol, 
			@Fractionalunit,
			0,
			@UserCreatedById, 
			@CreatedOn);

SELECT StoredProcedureName ='SApp_insertCurrency',Message =@ErrorMsg,Success = 1;     

END TRY
BEGIN CATCH
    -- Execute the error retrieval routine.
	DECLARE @ErrorNumber	int;
	DECLARE @ErrorSeverity	int;
	DECLARE @ErrorProcedure	varchar(1000);
	DECLARE @ErrorLine	int;
	DECLARE @ErrorMessage	varchar(1000);

	 SELECT @ErrorNumber = ERROR_NUMBER(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorProcedure = ERROR_PROCEDURE(),
        @ErrorLine = ERROR_LINE(),
        @ErrorMessage = ERROR_MESSAGE()

	insert into dbo.Errorlog(ErrorNumber,
							ErrorSeverity,
							ErrorProcedure,
							ErrorLine,
							ErrorMessage,
							ErrorDate)
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());

 set @ErrorMsg = 'Error while creating a new Currency, please contact Admin for more details.';    
--THROW 50000,@ErrorMsg,1;      
--return(1)  
SELECT Number = @ErrorNumber, 
		Severity =@ErrorSeverity,
		StoredProcedureName =@ErrorProcedure,
		LineNumber= @ErrorLine,
		Message =@ErrorMsg,
		Success = 0;     
END CATCH
end