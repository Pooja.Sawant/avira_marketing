﻿    
CREATE PROCEDURE [dbo].[SApp_InsertCompanyProfitAndLossAddTable]    
(    
 @Id     uniqueidentifier=null,    
 @ProjectId   uniqueidentifier=null, 
 @CompanyId   uniqueidentifier=null, 
 @TableType   nvarchar(50)=null,        
 @TableName   nvarchar(50)=null,    
 @TableMetadata  nvarchar(512)=null,    
 @TableData  nvarchar(512)=null,    
 @CreatedOn   datetime2(7)=null,    
 @UserCreatedById uniqueidentifier=null,    
 @ModifiedOn   datetime2(7)=null,    
 @UserModifiedById uniqueidentifier=null,    
 @IsDeleted   bit=0,    
 @DeletedOn   datetime2(7)=null,    
 @UserDeletedById uniqueidentifier=null    
)    
as    
/*================================================================================    
Procedure Name: SApp_InsertCompanyProfitAndLossAddTable    
Author: Gopi    
Create date: 06/26/2019    
Description: Insert or update a record in CompanyProfitAndLossTableTab table    
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
06/26/2019 Gopi   Initial Version    
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    
declare @ErrorMsg NVARCHAR(2048);    
IF exists(select 1 from [dbo].[CompanyProfitAndLossTableTab]     
   where ((@Id is null or [Id] !=  @Id)and  TableName = @TableName AND IsDeleted = 0))    
begin    
set @ErrorMsg = 'CompanyProfitAndLoss with Name "'+ @TableName + '" already exists.';    
SELECT StoredProcedureName ='SApp_InsertCompanyProfitAndLossAddTable',Message =@ErrorMsg,Success = 0;      
RETURN;      
end    
BEGIN TRY    
    
INSERT INTO [dbo].[CompanyProfitAndLossTableTab]    
(    
 Id,        
 ProjectId,  
 CompanyId, 
 TableType,   
 TableName,      
 TableMetadata,     
 TableData,     
 CreatedOn,      
 UserCreatedById,    
 ModifiedOn,      
 UserModifiedById,    
 IsDeleted,      
 DeletedOn,      
 UserDeletedById    
)    
values    
(    
 @Id,        
 @ProjectId, 
 @CompanyId, 
 @TableType,    
 @TableName,      
 @TableMetadata,     
 @TableData,     
 @CreatedOn,      
 @UserCreatedById,    
 @ModifiedOn,      
 @UserModifiedById,    
 @IsDeleted,      
 @DeletedOn,      
 @UserDeletedById    
)    
    
SELECT StoredProcedureName ='SApp_InsertCompanyProfitAndLossAddTable',Message =@ErrorMsg,Success = 1;       
END TRY    
BEGIN CATCH    
    -- Execute the error retrieval routine.    
 DECLARE @ErrorNumber int;    
 DECLARE @ErrorSeverity int;    
 DECLARE @ErrorProcedure varchar(100);    
 DECLARE @ErrorLine int;    
 DECLARE @ErrorMessage varchar(500);    
    
  SELECT @ErrorNumber = ERROR_NUMBER(),    
        @ErrorSeverity = ERROR_SEVERITY(),    
        @ErrorProcedure = ERROR_PROCEDURE(),    
        @ErrorLine = ERROR_LINE(),    
        @ErrorMessage = ERROR_MESSAGE()    
    
 insert into dbo.Errorlog(ErrorNumber,    
       ErrorSeverity,    
       ErrorProcedure,    
       ErrorLine,    
       ErrorMessage,    
       ErrorDate)    
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());    
    
 set @ErrorMsg = 'Error while creating a new CompanyProfitAndLoss Table, please contact Admin for more details.';        
   
SELECT Number = @ErrorNumber,     
  Severity =@ErrorSeverity,    
  StoredProcedureName =@ErrorProcedure,    
  LineNumber= @ErrorLine,    
  Message =@ErrorMsg,    
  Success = 0;         
END CATCH    
end