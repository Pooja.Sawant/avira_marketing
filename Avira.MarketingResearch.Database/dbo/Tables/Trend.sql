﻿CREATE TABLE [dbo].[Trend] (
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [TrendName]         VARCHAR (899)    CONSTRAINT [DF_Trend_TrendName] DEFAULT ('') NULL,
    [Description]       NVARCHAR (256)   NULL,
    [ProjectID]         UNIQUEIDENTIFIER NULL,
    [AuthorRemark]      NVARCHAR (512)   NULL,
    [ApproverRemark]    NVARCHAR (512)   NULL,
    [TrendStatusID]     UNIQUEIDENTIFIER NULL,
    [CreatedOn]         DATETIME2 (7)    NULL,
    [UserCreatedById]   UNIQUEIDENTIFIER NULL,
    [ModifiedOn]        DATETIME2 (7)    NULL,
    [UserModifiedById]  UNIQUEIDENTIFIER NULL,
    [IsDeleted]         BIT              CONSTRAINT [DF_Trend_IsDeleted] DEFAULT ((0)) NULL,
    [DeletedOn]         DATETIME2 (7)    NULL,
    [UserDeletedById]   UNIQUEIDENTIFIER NULL,
    [ImportanceId]      UNIQUEIDENTIFIER NULL,
    [ImpactDirectionId] UNIQUEIDENTIFIER NULL,
    [TrendValue]        DECIMAL (19, 4)  NULL,
    CONSTRAINT [PK_Trend] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Trend_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_Trend_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_Trend_ImpactDirection] FOREIGN KEY ([ImpactDirectionId]) REFERENCES [dbo].[ImpactDirection] ([Id]),
    CONSTRAINT [FK_Trend_Importance] FOREIGN KEY ([ImportanceId]) REFERENCES [dbo].[Importance] ([Id]),
    CONSTRAINT [FK_Trend_ProjectID] FOREIGN KEY ([ProjectID]) REFERENCES [dbo].[Project] ([ID])
);






GO

GO

GO

GO

GO

GO

GO

GO

GO

GO

GO

GO

GO
CREATE NONCLUSTERED INDEX [IX_Trend]
    ON [dbo].[Trend]([ProjectID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Trend_1]
    ON [dbo].[Trend]([TrendName] ASC);

