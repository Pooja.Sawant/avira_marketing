﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.Masters;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StatusController : AviraBaseController
    {
        public readonly IStatusService _iStatusService;
        private ILoggerManager _logger;

        public StatusController(
            IStatusService iStatusService,
            ILoggerManager logger)
        {
            _iStatusService = iStatusService;
            _logger = logger;
        }

       
        [HttpGet("{id}")]
        public ActionResult<IEnumerable<StatusViewModel>> GetAllByType(string id)
        {
            List<StatusViewModel> res = null;
            try
            {
                res = _iStatusService.GetAll(id).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

    }
}