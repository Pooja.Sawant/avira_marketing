﻿

CREATE procedure [dbo].[SApp_UpdateProjectSegmentMapping]      
(@ProjectID uniqueidentifier, 
@SegmentType nvarchar(100),
@SegmentIDList [dbo].[udtUniqueIdentifier] readonly, 
@UserModifiedById  uniqueidentifier
)  
as      
/*================================================================================      
Procedure Name: SApp_UpdateProjectSegmentMapping     
Author: Swami      
Create date: 04/10/2019      
Description: update a record in ProjectSegmentMapping table by ID. Also used for delete      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
04/10/2019 Swami   Initial Version     
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
    declare @ErrorMsg NVARCHAR(2048);

--IF exists(select 1 from [dbo].ProjectSegmentMapping       
--   where [Id] !=  @Id  
--   AND SegmentType = @SegmentType 
--   AND ProjectID = @ProjectID 
--   AND SegmentID = @SegmentID
--   AND  IsDeleted = 0)      
--begin      
--declare @ErrorMsg NVARCHAR(2048);      
--set @ErrorMsg = 'ProjectId with Name "'+ CONVERT(nvarchar(50), @ProjectID) + '" already exists.';      
  
--SELECT StoredProcedureName ='SApp_UpdateProjectSegmentMapping','Message' =@ErrorMsg,'Success' = 0;    
--RETURN;  
--end      
    
BEGIN TRY       
--update records for delete      
update [dbo].ProjectSegmentMapping      
set IsDeleted = 1,
UserDeletedById = @UserModifiedById,
DeletedOn = GETDATE()
from  [dbo].ProjectSegmentMapping
where ProjectID = @ProjectID     
and not exists (select 1 from @SegmentIDList map where map.ID = ProjectSegmentMapping.SegmentID);

--Insert new records
insert into [dbo].[ProjectSegmentMapping]([Id],
								ProjectID,
								SegmentType,
								SegmentID,
								[IsDeleted],
								[UserCreatedById],
								[CreatedOn])
select NEWID(), 
			@ProjectID,
			@SegmentType,
			ID,
			0,
			@UserModifiedById, 
			GETDATE()
from @SegmentIDlist list
where not exists (select 1 from [ProjectSegmentMapping] map where map.ProjectID = @ProjectID and list.ID = map.SegmentID);

SELECT StoredProcedureName ='SApp_UpdateProjectSegmentMapping',Message =@ErrorMsg,Success = 1;       
    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);      
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());   
  
set @ErrorMsg = 'Error while updating ProjectSegmentMapping, please contact Admin for more details.'        
   
 SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
END CATCH      
      
    
    
      
end