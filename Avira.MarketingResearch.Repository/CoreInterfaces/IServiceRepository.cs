﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces
{
    public interface IServiceRepository
    {
        ServiceViewModel GetById(Guid guid);
        IEnumerable<ServiceViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted);
        IEnumerable<ServiceViewModel> GetAllParentService();
        Result<SpTransactionMessage> Create(ServiceInsertDbViewModel viewModel);
        Result<SpTransactionMessage> Update(ServiceUpdateDbViewModel viewModel);
    }
}
