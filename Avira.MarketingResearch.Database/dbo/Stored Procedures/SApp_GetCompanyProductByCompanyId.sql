﻿
  
CREATE PROCEDURE [dbo].[SApp_GetCompanyProductByCompanyId]      
 (
-- @LaunchDate datetime=null,  
 @CompanyID UniqueIdentifier =null,  
 @includeDeleted bit = 0      
 )      
 AS      
/*================================================================================      
Procedure Name: SApp_GetCompanyProductByCompanyID      
Author: Harshal      
Create date: 05/02/2019      
Description: Get Details from CompanyProduct table by ID      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
05/02/2019 Harshal   Initial Version  
================================================================================*/      
      
BEGIN      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED      
if @includeDeleted is null set @includeDeleted = 0;  
--if @LaunchDate is null set @LaunchDate = '12/31/2999'  
 SELECT [CompanyProduct].[Id],
  [Project].[ProjectName], 
  [CompanyProduct].CompanyId,
  [CompanyProduct].ProjectId,     
  [CompanyProduct].[ProductName],      
  [CompanyProduct].[LaunchDate] ,  
  [CompanyProduct].[Description], 
  [CompanyProduct].[Patents],  
  [CompanyProduct].[StatusId] AS ProductStatusId,  
  [ProductStatus].[ProductStatusName],
  [CompanyProduct].[ImageURL],
  [CompanyProduct].[ImageDisplayName], 
  [CompanyProduct].[ActualImageName] As ImageActualName, 
  [Currency].[CurrencyName],
  [CompanyProductValue].[CurrencyId] As CurrencyId,
  [CompanyProductValue].[ValueConversionId] As ValueConversionId,
  [ValueConversion].[ValueName],
  --[CompanyProductValue].[Amount],
  dbo.fun_UnitDeConversion([CompanyProductValue].[ValueConversionId],[CompanyProductValue].[Amount]) as [Amount],
  [CompanyProduct].[IsDeleted]
FROM [dbo].[CompanyProduct]  
LEFT JOIN [dbo].[Project] Project
ON [CompanyProduct].[ProjectId]=Project.ID
INNER JOIN [dbo].[ProductStatus] ProductStatus      
ON [CompanyProduct].StatusId = [ProductStatus].Id
LEFT JOIN [dbo].CompanyProductValue CompanyProductValue
ON [CompanyProduct].Id=[CompanyProductValue].CompanyProductId
LEFT JOIN [dbo].Currency Currency
ON [CompanyProductValue].CurrencyId= [Currency].Id
LEFT JOIN [dbo].[ValueConversion] ValueConversion
ON [CompanyProductValue].ValueConversionId= [ValueConversion].Id
WHERE ([CompanyProduct].CompanyId =@CompanyID)      
 AND (@includeDeleted = 1 or [CompanyProduct].IsDeleted = 0)
     
 END