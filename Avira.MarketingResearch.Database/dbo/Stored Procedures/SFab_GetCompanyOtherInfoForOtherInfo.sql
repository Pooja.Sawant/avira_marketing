﻿CREATE PROCEDURE [dbo].[SFab_GetCompanyOtherInfoForOtherInfo]                 
 @CompanyID uniqueidentifier=null,
 @ProjectID uniqueidentifier=null, 
 @AviraUserID uniqueidentifier = null                
AS  
/*================================================================================
Procedure Name: SFab_GetCompanyOtherInfoForOtherInfo
Author: Pooja 
Create date: 07/22/2019
Description: Get list from company otherinfo table by companyid and projectid
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
07/22/2019	Pooja			Initial Version
================================================================================*/          
BEGIN             
 SET NOCOUNT ON;    
 
    DECLARE @CompanyStatusnID uniqueidentifier  
 select @CompanyStatusnID = id from CompanyProfileStatus   
 where CompanyProfileStatus.CompanyStatusName = 'Approved';  
           
SELECT      CompanyOtherInfo.CompanyId, CompanyOtherInfo.TableInfoName, CompanyOtherInfo.Description
FROM            CompanyOtherInfo INNER JOIN
                         CompanyProfileStatus ON CompanyOtherInfo.CompanyStatusId = CompanyProfileStatus.Id
WHERE        (CompanyOtherInfo.CompanyId = @CompanyId)  
AND (CompanyOtherInfo.IsDeleted = 0) 
and (CompanyOtherInfo.TableInfoName like 'Recent Quotes by senior Management%')
			--and CompanyOtherInfo.CompanyStatusId=@CompanyStatusnID 
END