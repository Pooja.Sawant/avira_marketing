﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public class CompanyOtherInfoQueries
    {
        public static readonly string StoreProcInsertUpdate = "SApp_InsertUpdateCompanyOtherInfo";
        public static string StoreProcSelectAll = "SApp_GetCompanyOtherInfo";

    }
}
