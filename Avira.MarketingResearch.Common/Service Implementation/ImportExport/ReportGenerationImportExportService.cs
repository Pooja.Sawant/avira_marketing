﻿using Avira.MarketingResearch.Common.IService.ImportExport;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.ImportExport
{
    public class ReportGenerationImportExportService : IReportGenerationImportExportService
    {
        private readonly IReportGenerationImportExportRepository _reportGenerationImportExportRepository;
        private readonly IConfiguration _configuration;
        private readonly IHostingEnvironment _hostingEnvironment;

        public ReportGenerationImportExportService(IReportGenerationImportExportRepository reportGenerationImportExportRepository, IConfiguration configuration, IHostingEnvironment hostingEnvironment)
        {
            _reportGenerationImportExportRepository = reportGenerationImportExportRepository;
            _hostingEnvironment = hostingEnvironment;
            _configuration = configuration;
        }
        public List<ImportCommonResponseModel> Create(ReportGenerationImportModel viewModel, AviraUser currentUser)
        {
            viewModel.JsonReportGeration = JsonConvert.SerializeObject(viewModel.ReportGenerationViewModel);
            var ImportResponse = _reportGenerationImportExportRepository.Create(viewModel);
            var res = ImportResponse.Where(s => s.ErrorNotes != null).ToList();
            if (res.Count <= 0)
            {
                foreach(ReportGenerationViewModel report in viewModel.ReportGenerationViewModel)
                {
                    if(report.CoverPageImageFileByte != null && report.CoverPageImageFileByte.Length > 0)
                    {
                        var isSuccess= UploadFile(report);
                    }
                       
                }
                
            }
            return ImportResponse;
        }
        public bool UploadFile(ReportGenerationViewModel viewModel)
        {
            bool isCopied = false;
            try
            {
                var fileName = viewModel.CoverPageImageName;
                var path = _configuration.GetSection("AppUploadPath").GetSection("ReportGeneration").GetSection("Images").Value;
                var fullPath = Path.Combine(_hostingEnvironment.ContentRootPath, path, fileName);
                isCopied = FileUpload.ByteArrayToFile(viewModel.CoverPageImageFileByte, fullPath);
                if(viewModel.MethodologyImageFileByte !=null && viewModel.MethodologyImageFileByte.Length > 0)
                {
                    var newPath = Path.Combine(_hostingEnvironment.ContentRootPath, path, viewModel.MethodologyImageName);
                    isCopied = FileUpload.ByteArrayToFile(viewModel.CoverPageImageFileByte, newPath);
                }
            }
            catch (Exception ex)
            {
                //throw ex;
            }
            return isCopied;
        }
    }
}
    