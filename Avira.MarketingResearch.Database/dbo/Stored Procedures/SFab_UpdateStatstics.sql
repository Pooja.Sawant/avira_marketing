﻿-- =============================================
-- Author:		Raju
-- Create date: 27-02-2020
-- Description:	Update Statistics to all the tables
-- =============================================
CREATE PROCEDURE [dbo].[SFab_UpdateStatstics]
	-- Add the parameters for the stored procedure here
@DataBase nvarchar(30)
AS
BEGIN
DECLARE @temp VARCHAR(MAX)
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT @temp = COALESCE(@temp+'UPDATE STATISTICS ' + TABLE_NAME + ' WITH FULLSCAN;','') FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_CATALOG=@DataBase order by TABLE_NAME;
SELECT(@temp)

END