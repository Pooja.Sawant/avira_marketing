﻿CREATE TABLE [dbo].[CountryRegionMap] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [CountryId]        UNIQUEIDENTIFIER NOT NULL,
    [RegionId]         UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_CountryRegionMap_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CountryRegionMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CountryRegionMap_CountryId] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Country] ([Id]),
    CONSTRAINT [FK_CountryRegionMap_CreatedUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CountryRegionMap_ModifiedUser] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CountryRegionMap_RegionId] FOREIGN KEY ([RegionId]) REFERENCES [dbo].[Region] ([Id])
);

