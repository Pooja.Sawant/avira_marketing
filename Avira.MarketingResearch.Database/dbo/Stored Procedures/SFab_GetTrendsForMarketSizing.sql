﻿      
 CREATE PROCEDURE [dbo].[SFab_GetTrendsForMarketSizing]              
 (  
  @ProjectId uniqueidentifier = null,     
  --@TrendKeyWord NVARCHAR(50) = null,  
  @ImpactName nvarchar(60)=null,  
  --@ImpactId uniqueidentifier = null,     
  @OrdbyByColumnName NVARCHAR(50) ='TrendName',          
  @SortDirection INT = -1,          
  @PageStart INT = 0,        
  @TrendName NVARCHAR(512)='',          
  @PageSize INT = null,      
  @AviraUserID uniqueidentifier = null,      
  @AviraUserRoleId uniqueidentifier = null      
 )              
 AS              
/*================================================================================              
Procedure Name: SFab_GetTrendsForMarketSizing         
Author: Pooja Sawant              
Create date: 05/31/2019              
Description: Get list from Trends table based on the customer subscription      
Change History              
Date  Developer  Reason              
__________ ____________ ______________________________________________________              
05/31/2019 Pooja   Initial Version              
================================================================================*/             
              
BEGIN              
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED      
if len(@TrendName)>2 set @TrendName = '%'+ @TrendName + '%'        
else set @TrendName = ''        
if @PageSize is null or @PageSize =0          
set @PageSize = 50          
    
   DECLARE @TrendStatusnID uniqueidentifier  
 select @TrendStatusnID = id from TrendStatus   
 where TrendStatusName = 'Approved';  
  
 declare @ImpactId uniqueidentifier = null  
 if(@ImpactName != '')  
 begin  
  select @ImpactId = id from ImpactType   
 where ImpactTypeName = @ImpactName;  
 end  
 else  
 begin   
 select @ImpactId = id from ImpactType   
 where ImpactTypeName = 'Direct';  
 end  
begin         
   
;WITH CTE_TBL AS (   
          
SELECT DISTINCT top 4  
    Trend.Id, Trend.TrendName, 
	ImpactType.ImpactTypeName, 
	CASE WHEN Importance.ImportanceName = 'High' OR Importance.ImportanceName = 'VeryHigh' THEN 'high-bg' 
	WHEN Importance.ImportanceName = 'Mid' OR Importance.ImportanceName = 'Mid' THEN 'mid-bg' 
	WHEN Importance.ImportanceName = 'Low' OR  
    Importance.ImportanceName = 'VeryLow' THEN 'low-bg' ELSE 'low-bg' END AS ImportanceColorCode,   
    CASE WHEN @OrdbyByColumnName = 'TrendName' THEN [Trend].TrendName WHEN @OrdbyByColumnName = 'Description' THEN [Trend].[Description]   
       ELSE [Trend].TrendName END AS SortColumn  
FROM Trend
INNER JOIN TrendProjectMap 
ON TrendProjectMap.TrendId = Trend.Id  
INNER JOIN  ImpactType 
ON ImpactType.Id = TrendProjectMap.Impact 
INNER JOIN  Importance ON Trend.ImportanceId = Importance.Id 
WHERE        (Trend.IsDeleted = 0)  
  
AND (Trend.TrendName like '%' + @TrendName + '%' OR @TrendName is null or @TrendName = '')  
AND ( Trend.ProjectID = @ProjectId)  
AND  TrendProjectMap.Impact=@ImpactId
--AND @ImpactId is null Or @ImpactId ='00000000-0000-0000-0000-000000000000' or TrendProjectMap.Impact=@ImpactId  
and  Trend.TrendStatusID=@TrendStatusnID  
)  
          
 SELECT           
    CTE_TBL.*,          
    tCountResult.TotalItems          
  FROM CTE_TBL          
  CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult          
  ORDER BY           
    CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,          
    CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC           
  OFFSET @PageStart ROWS          
  --FETCH NEXT @PageSize ROWS ONLY;          
  END          
          
END