﻿using System;
using System.Collections.Generic;
using System.Linq;
using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyClientAndStrategyController : AviraBaseController
    {
        private readonly ICompanyClientAndStrategyService _companyClientAndStrategyService;
        private readonly ICompanyClientsStrategyRepository _companyClientAndStrategyRepository; 
        private ILoggerManager _logger;


        public CompanyClientAndStrategyController(
            ICompanyClientAndStrategyService companyClientAndStrategyService,
            ICompanyClientsStrategyRepository companyClientAndStrategyRepository, 
             ILoggerManager logger)
        {
            _companyClientAndStrategyRepository = companyClientAndStrategyRepository;
            _companyClientAndStrategyService = companyClientAndStrategyService;
            _logger = logger;
        }

        [HttpGet]
        [Route("GetstrategyById")]
        public ActionResult<IEnumerable<CompanyStrategyViewModel>> GetStrategy(Guid? Id)
        {
            List<CompanyStrategyViewModel> res = null;
            try
            { 
            res = _companyClientAndStrategyService.GetstrategyById(Id).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet]
        [Route("GetclientById")]
        public ActionResult<IEnumerable<CompanyclientViewModel>> GetClients(Guid? Id)
        {
            List<CompanyclientViewModel> res = null;
            try{
                res = _companyClientAndStrategyService.GetclientById(Id).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpPost]
        public ActionResult Post([FromBody] CompanyClientStrategyInsertUpdateDbViewModel viewModel)
        {
            viewModel.CreatedOn = System.DateTime.UtcNow;
            viewModel.AviraUserId = CurrentUser.Id;
            viewModel.Id = Guid.NewGuid();

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a adding Company client Strategy mapping  , please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)   
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                var result = _companyClientAndStrategyService.InsertUpdate(viewModel, CurrentUser);
                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Company client strategy is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {        
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }

        [HttpGet]
        [Route("GetAllImportance")]
        public ActionResult<IEnumerable<CompanyClientStrategyImportanceViewModel>> GetAllImportance()
        {
            List<CompanyClientStrategyImportanceViewModel> res = null;
            try
            {
                res = _companyClientAndStrategyService.GetAllImportance().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet]
        [Route("GetAllImpactDirection")]
        public ActionResult<IEnumerable<CompanyClientStrategyDirectionViewModel>> GetAllImpactDirection()
        {
            List<CompanyClientStrategyDirectionViewModel> res = null;
            try
            {
                res = _companyClientAndStrategyService.GetAllImpactDirection().ToList();
        }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet]
        [Route("GetAllApproach")]
        public ActionResult<IEnumerable<CompanyClientStrategyApproachViewModel>> GetAllApproach()
        {
            List<CompanyClientStrategyApproachViewModel> res = null;
            try
            {
                res = _companyClientAndStrategyService.GetAllApproach().ToList();
}
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet]
        [Route("GetAllCategories")]
        public ActionResult<IEnumerable<CompanyClientStrategyCategoryViewModel>> GetAllCategories()
        {
            List<CompanyClientStrategyCategoryViewModel> res = null;
            try
            {
                res = _companyClientAndStrategyService.GetAllCategories().ToList();
}
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet]
        [Route("GetAllIndustries")]
        public ActionResult<IEnumerable<CompanyClientStrategyIndustryViewModel>> GetAllIndustries()
        {
            List<CompanyClientStrategyIndustryViewModel> res = null;
            try
            {
                res = _companyClientAndStrategyService.GetAllIndustries().ToList();
}
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }
    }
}
