﻿      
      
CREATE procedure [dbo].[SApp_insertupdateNews]    
(          
@NewsList dbo.udtGUIDNewsMultiValue READONLY,   
@CompanyId uniqueidentifier, 
@ProjectId uniqueidentifier,    
@UserCreatedById uniqueidentifier        
)          
as          
/*================================================================================          
Procedure Name: SApp_insertupdateNews    
Change History          
Date  Developer  Reason          
__________ ____________ ______________________________________________________          
05/06/2019 Gopi   Initial Version   
------------------------------------------------------------------------------
16-05-2019 Harshal Add update and changes in Delete       
================================================================================*/          
BEGIN          
SET NOCOUNT ON;          
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;        
declare @ErrorMsg NVARCHAR(2048)      
    
BEGIN TRY             
             
update [dbo].[News]
set IsDeleted = 1,
UserDeletedById = @UserCreatedById,
DeletedOn = GETDATE()
from [dbo].[News]
where [News].CompanyId = @CompanyId and [News].ProjectId = @ProjectId
and not exists (select 1 from @NewsList map where map.Id = [News].Id)


update [dbo].[News]
set news = map.news,
Date = map.Date,
ImportanceId = map.ImportanceId,
ImpactDirectionId = map.ImpactDirectionId,
NewsCategoryId = map.NewsCategoryId,
OtherParticipants=map.OtherParticipants,
Remarks=map.Remarks,
ModifiedOn = GETDATE(),
UserModifiedById = @UserCreatedById
from [dbo].[News]
inner join @NewsList map 
on map.Id = [News].Id
where [News].CompanyId = @CompanyId --and [News].ProjectId = @ProjectId;

    
INSERT INTO [dbo].[News]([Id],  
		[CompanyID],
		[ProjectID],        
        [Date],          
		[News],  
		[NewsCategoryId],
		[ImportanceId],
		[ImpactDirectionId],
		[OtherParticipants],
		[Remarks],
        [IsDeleted],          
        [UserCreatedById],          
        [CreatedOn])          
 SELECT Id,   
	@CompanyId, 
	@ProjectId,       
    Date,           
   News, 
   NewsCategoryId,
   ImportanceId,
   ImpactDirectionId,
   OtherParticipants,
   Remarks,
   0,          
   @UserCreatedById,           
   GETDATE()    
   from @NewsList list    
   where not exists (select 1 from [News]     
    where list.ID = [News].[Id])    
    
SELECT StoredProcedureName ='SApp_insertupdateNews',Message =@ErrorMsg,Success = 1;           
        
END TRY          
BEGIN CATCH          
    -- Execute the error retrieval routine.          
 DECLARE @ErrorNumber int;          
 DECLARE @ErrorSeverity int;          
 DECLARE @ErrorProcedure varchar(100);          
 DECLARE @ErrorLine int;          
 DECLARE @ErrorMessage varchar(500);          
          
  SELECT @ErrorNumber = ERROR_NUMBER(),          
        @ErrorSeverity = ERROR_SEVERITY(),          
        @ErrorProcedure = ERROR_PROCEDURE(),          
        @ErrorLine = ERROR_LINE(),          
        @ErrorMessage = ERROR_MESSAGE()          
          
 insert into dbo.Errorlog(ErrorNumber,          
       ErrorSeverity,          
       ErrorProcedure,          
       ErrorLine,          
       ErrorMessage,          
       ErrorDate)          
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());          
        
 set @ErrorMsg = 'Error while creating a new News, please contact Admin for more details.';          
      
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine        
    ,Message =@ErrorMsg,Success = 0;           
           
END CATCH          
          
          
end