﻿CREATE TABLE [dbo].[CompanyTrendsMap] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [TrendName]        VARCHAR (899)    NULL,
    [Department]       NVARCHAR (256)   NULL,
    [ImportanceId]     UNIQUEIDENTIFIER NULL,
    [ImpactId]         UNIQUEIDENTIFIER NULL,
    [CompanyId]        UNIQUEIDENTIFIER NOT NULL,
    [ProjectId]        UNIQUEIDENTIFIER NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_CompanyTrendsMap_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CompanyTrendsMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CompanyTrendsMap_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyTrendsMap_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyTrendsMap_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([Id]),
    CONSTRAINT [FK_CompanyTrendsMap_ImpactDirection] FOREIGN KEY ([ImpactId]) REFERENCES [dbo].[ImpactDirection] ([Id]),
    CONSTRAINT [FK_CompanyTrendsMap_Importance] FOREIGN KEY ([ImportanceId]) REFERENCES [dbo].[Importance] ([Id])
);



