﻿using Avira.MarketingResearch.ImportProcessor.ImportExportUtility;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Avira.MarketingResearch.ImportProcessor
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
        .SetBasePath(Directory.GetCurrentDirectory())
        .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            IConfigurationRoot configuration = builder.Build();
            
            ImportMarketSizing objImportMarketSizing = new ImportMarketSizing(configuration);
            objImportMarketSizing.ProcessMSImport();
        }

    }
}
