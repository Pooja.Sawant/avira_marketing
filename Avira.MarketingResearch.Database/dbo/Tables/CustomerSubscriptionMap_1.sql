﻿CREATE TABLE [dbo].[CustomerSubscriptionMap] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [CustomerId]       UNIQUEIDENTIFIER NOT NULL,
    [SubscriptionId]   UNIQUEIDENTIFIER NOT NULL,
    [AnniversaryDate]  DATETIME2 (7)    NULL,
    [StartDate]        DATETIME2 (7)    NULL,
    [EndDate]          DATETIME2 (7)    NULL,
    [IsCustomized]     BIT              NOT NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_CustomerSubscriptionMap_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    [AviraUserId]      UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_CustomerSubscriptionMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CustomerSubscriptionMap_AviraUserId] FOREIGN KEY ([AviraUserId]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CustomerSubscriptionMap_CreatedUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CustomerSubscriptionMap_CustomerId] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([Id]),
    CONSTRAINT [FK_CustomerSubscriptionMap_DeletedUser] FOREIGN KEY ([UserDeletedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CustomerSubscriptionMap_ModifiedUser] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CustomerSubscriptionMap_SubscriptionId] FOREIGN KEY ([SubscriptionId]) REFERENCES [dbo].[Subscription] ([Id])
);



