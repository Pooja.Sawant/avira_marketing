﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.Trend
{
   public class TrendNoteRepository : ITrendNoteRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "TrendNote";

        public TrendNoteRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public TrendNoteViewModel GetById(Guid guid, string trendType)
        {
            TrendNoteViewModel trendViewModel = new TrendNoteViewModel();
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<TrendNoteViewModel>(TrendNoteQueries.StoreProcSelectAll
                            , new { @TrendID = guid, @TrendType = trendType }
                            , null, true, null, CommandType.StoredProcedure).FirstOrDefault();
            
        }
        public IEnumerable<TrendNoteViewModel> GetAllByTrendId(Guid guid , string StatusName)
        {
            TrendNoteViewModel trendViewModel = new TrendNoteViewModel();
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<TrendNoteViewModel>(TrendNoteQueries.StoreProcSelectAllStatus
                            , new { @TrendID = guid, @TrendStatusName=StatusName }
                            , null, true, null, CommandType.StoredProcedure);

        }
        public Result<SpTransactionMessage> Insert(TrendNoteViewModel trendNoteInsertUpdateDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(TrendNoteQueries.StoreProcInsert, new
            {
                @Id = trendNoteInsertUpdateDbViewModel.Id,
                @TrendId = trendNoteInsertUpdateDbViewModel.TrendId,
                @TrendType = trendNoteInsertUpdateDbViewModel.TrendType,
                @AuthorRemark = trendNoteInsertUpdateDbViewModel.AuthorRemark,                
                @ApproverRemark = trendNoteInsertUpdateDbViewModel.ApproverRemark,
                @TrendStatusName = trendNoteInsertUpdateDbViewModel.TrendStatusName,
                @AviraUserId = trendNoteInsertUpdateDbViewModel.UserCreatedById
            }, CommandType.StoredProcedure);
            return spTransactionMessage;
        }
    }
}
