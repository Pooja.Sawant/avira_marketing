﻿
CREATE procedure [dbo].[SApp_insertUpdateCompanyEcosystemPresenceKeyCompetitersMap]
(
	@CompanyId uniqueidentifier,
	@KeyCompetitersList dbo.udtVarcharValue READONLY,
	@UserCreatedById uniqueidentifier
)
as
/*================================================================================
Procedure Name: [SApp_insertUpdateCompanyEcosystemPresenceKeyCompetitersMap]
Author: Sai Krishna
Create date: 08/05/2019
Description: Insert a record into CompanyEcosystemPresenceKeyCompetitersMap table
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
08/05/2019	Sai Krishna		Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ErrorMsg NVARCHAR(2048);

BEGIN TRY

delete [dbo].[CompanyEcosystemPresenceKeyCompetitersMap]
where [CompanyId] = @CompanyId 

INSERT INTO [dbo].[CompanyEcosystemPresenceKeyCompetitersMap]
           ([Id]
           ,[CompanyId]
           ,[KeyCompetiters]
		   ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted]
           )
   select NEWID(),
   @CompanyId,
   kcl.Value,
   GetDate(),
   @UserCreatedById,
   0
   from @KeyCompetitersList kcl

SELECT StoredProcedureName ='SApp_insertUpdateCompanyEcosystemPresenceKeyCompetitersMap',Message =@ErrorMsg,Success = 1;  
END TRY
BEGIN CATCH
    -- Execute the error retrieval routine.
	DECLARE @ErrorNumber	int;
	DECLARE @ErrorSeverity	int;
	DECLARE @ErrorProcedure	varchar(100);
	DECLARE @ErrorLine	int;
	DECLARE @ErrorMessage	varchar(500);

	 SELECT @ErrorNumber = ERROR_NUMBER(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorProcedure = ERROR_PROCEDURE(),
        @ErrorLine = ERROR_LINE(),
        @ErrorMessage = ERROR_MESSAGE()

	insert into dbo.Errorlog(ErrorNumber,
							ErrorSeverity,
							ErrorProcedure,
							ErrorLine,
							ErrorMessage,
							ErrorDate)
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GetDate());

	set @ErrorMsg = 'Error, please contact Admin for more details.';

SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;

END CATCH


end