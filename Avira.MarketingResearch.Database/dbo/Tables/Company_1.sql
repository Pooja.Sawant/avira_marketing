﻿CREATE TABLE [dbo].[Company] (
    [Id]                      UNIQUEIDENTIFIER NOT NULL,
    [CompanyCode]             NVARCHAR (256)   CONSTRAINT [DF_CompanyConst] DEFAULT ('C'+right('000000'+CONVERT([varchar](10),NEXT VALUE FOR [DBO].[CompanyCode_seq]),(7))) NOT NULL,
    [CompanyName]             NVARCHAR (256)   CONSTRAINT [DF_Company_CompanyName] DEFAULT ('') NOT NULL,
    [Description]             NVARCHAR (512)   NULL,
    [Telephone]               NVARCHAR (20)    NULL,
    [Telephone2]              NVARCHAR (20)    NULL,
    [Email]                   NVARCHAR (100)   NULL,
    [Email2]                  NVARCHAR (100)   NULL,
    [Fax]                     NVARCHAR (20)    NULL,
    [Fax2]                    NVARCHAR (20)    NULL,
    [ProfiledCompanyName]     NVARCHAR (256)   NULL,
    [Website]                 NVARCHAR (128)   NULL,
    [NoOfEmployees]           INT              NULL,
    [YearOfEstb]              INT              NULL,
    [ParentCompanyYearOfEstb] INT              NULL,
    [CreatedOn]               DATETIME2 (7)    NULL,
    [UserCreatedById]         UNIQUEIDENTIFIER NULL,
    [ModifiedOn]              DATETIME2 (7)    NULL,
    [UserModifiedById]        UNIQUEIDENTIFIER NULL,
    [IsDeleted]               BIT              CONSTRAINT [DF_Company_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]               DATETIME2 (7)    NULL,
    [UserDeletedById]         UNIQUEIDENTIFIER NULL,
    [EntityTypeId]            UNIQUEIDENTIFIER NULL,
    [NatureId]                UNIQUEIDENTIFIER NULL,
    [RankNumber]              INT              NULL,
    [RankRationale]           NVARCHAR (100)   NULL,
    [LogoURL]                 NVARCHAR (256)   NULL,
    [DisplayName]             NVARCHAR (100)   NULL,
    [CompanyLocation]         NVARCHAR (256)   NULL,
    [IsHeadQuarters]          BIT              NULL,
    [ParentId]                UNIQUEIDENTIFIER NULL,
    [ParentCompanyName]       NVARCHAR (256)   NULL,
    [Telephone3]              NVARCHAR (20)    NULL,
    [Email3]                  NVARCHAR (100)   NULL,
    [CompanyStageId]          UNIQUEIDENTIFIER NULL,
    [ActualImageName]         NVARCHAR (256)   NULL,
    CONSTRAINT [PK_Company] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [fk_Company_CompanyParentId] FOREIGN KEY ([ParentId]) REFERENCES [dbo].[Company] ([Id]),
    CONSTRAINT [IX_Company] UNIQUE NONCLUSTERED ([CompanyName] ASC),
    CONSTRAINT [IX_Company_1] UNIQUE NONCLUSTERED ([CompanyCode] ASC)
);









