﻿

CREATE PROCEDURE [dbo].[SApp_CompanyOrganizationChartByID]  
 (
	 @CompanyID uniqueidentifier = null
 )  
AS  
/*================================================================================  
Procedure Name: SApp_CompanyOrganizationChartByID  
Author: Laxmikant  
Create date: 05/21/2019  
Description: Get Company Organizatin chart by Id.  
Change History  
Execution : Exec SApp_CompanyOrganizationChartByID 'B7A17749-3D62-49A8-BCA1-DF5C5AF9B8F7'  
================================================================================*/  
BEGIN  
 ;WITH ORG_CTE AS (       
SELECT CompanyKeyEmployeesMap.Id, 
KeyEmployeeName, 
DesignationId, 
Designation.Designation, 
ManagerId, 
cast('' as nvarchar(256)) as ManagerName,
cast(ROW_NUMBER() over (partition by ManagerId order by CompanyKeyEmployeesMap.CreatedOn) as varchar(20)) as EmpRank    
FROM CompanyKeyEmployeesMap    
Left Join Designation on CompanyKeyEmployeesMap.DesignationId = Designation.Id   
WHERE 
ManagerId IS NULL and 
CompanyId = @CompanyID   
UNION ALL     
SELECT emp.ID, 
emp.KeyEmployeeName, 
emp.DesignationId,
Designation.Designation, 
emp.ManagerId, 
ORG_CTE.KeyEmployeeName as ManagerName,
cast(ORG_CTE.EmpRank+ cast(ROW_NUMBER() over (partition by emp.ManagerId order by emp.CreatedOn) as varchar(10)) as varchar(20))  
FROM CompanyKeyEmployeesMap emp    
INNER Join Designation on emp.DesignationId = Designation.Id   
INNER JOIN ORG_CTE  ON emp.ManagerId = ORG_CTE.ID    
where emp.CompanyId = @CompanyID)   
SELECT * FROM ORG_CTE   
END