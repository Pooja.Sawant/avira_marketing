﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.Helpers;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile
{
    public interface ICompanyShareVolumeRepository
    {
        Task<MainCompanyShareVolModel> GetShareVolume(string Id);
        Result<SpTransactionMessage> SaveImageInServer(MainCompanyShareVolModel Model, AviraUser currenUser);
        Result<SpTransactionMessage> InsertUpdate(CompanyShareVolumeListModel trendValueMapUpdateDbViewModel);
        IEnumerable<CurrencyViewModel> GetAllCurrencies();
    }
}
