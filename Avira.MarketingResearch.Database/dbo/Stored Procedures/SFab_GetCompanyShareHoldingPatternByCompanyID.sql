﻿
CREATE procedure [dbo].[SFab_GetCompanyShareHoldingPatternByCompanyID]
(@CompanyID uniqueidentifier)
as
/*================================================================================
Procedure Name: [SFab_GetCompanyShareHoldingPatternByCompanyID]
Author: Harshal
Create date: 07/05/2020
Description: Get list from Company Share holders by ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
07/05/2020	Harshal		Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT  [ShareHoldingPatternMap].[Id],
        [ShareHoldingPatternMap].ShareHoldingName,
		[ShareHoldingPatternMap].EntityTypeId,
		[EntityType].EntityTypeName,
		[ShareHoldingPatternMap].ShareHoldingPercentage
FROM [dbo].[ShareHoldingPatternMap]
LEFT JOIN [EntityType] ON [EntityType].Id=[ShareHoldingPatternMap].EntityTypeId
where [ShareHoldingPatternMap].CompanyId = @CompanyID and [ShareHoldingPatternMap].[IsDeleted] = 0

end