﻿CREATE procedure [dbo].[SApp_GetUserRolesByAviraUserId]    
(@AviraUserId uniqueidentifier)    
as    
/*================================================================================    
Procedure Name: [SApp_GetUserRolesByAviraUserId]    
Author: Gopi    
Create date: 04/10/2019    
Description: Get userRoles from AviraUserRoleMap table    
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
04/10/2019 Gopi   Initial Version    
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    
    
	SELECT UR.USERROLENAME,UR.ID
	FROM [dbo].[AVIRAUSERROLEMAP] AS AURM
	JOIN
	[dbo].[USERROLE] AS UR
	ON AURM.USERROLEID=UR.ID
	AND 
	AURM.AVIRAUSERID=@AviraUserId

END