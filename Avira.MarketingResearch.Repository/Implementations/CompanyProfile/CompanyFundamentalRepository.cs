﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avira.MarketingResearch.Repository.Implementations.CompanyProfile
{
    public class CompanyFundamentalRepository : ICompanyFundamentalRepository
    {
        protected IDbHelper _dbHelper;
        public CompanyFundamentalRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }
        public IEnumerable<CompanyFundamentalViewModel> GetAll()
        {
            //string id = Convert.ToString(Id);
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            //return dapper.Query<CompanyStructureModel>(CompanyStructureQueries.StoreProcSelectAll
            // , new
            // {
            //     @Id= Id
            // }
            // , null, true, null, CommandType.StoredProcedure);

            return dapper.Query<CompanyFundamentalViewModel>(CompanyFundamentalQueries.StoreProcSelectAll, new { }, null, true, null, CommandType.StoredProcedure);
        }

        public IEnumerable<CompanyRegionViewModel> GetAllRegion()
        {
            //string id = Convert.ToString(Id);
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            //return dapper.Query<CompanyStructureModel>(CompanyStructureQueries.StoreProcSelectAll
            // , new
            // {
            //     @Id= Id
            // }
            // , null, true, null, CommandType.StoredProcedure);

            return dapper.Query<CompanyRegionViewModel>(CompanyFundamentalQueries.StoreProcSelectAllForCompanyRegions, new { }, null, true, null, CommandType.StoredProcedure);
        }
        public IEnumerable<CompanyEntityViewModel> GetAllEntities()
        {
            //string id = Convert.ToString(Id);
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            //return dapper.Query<CompanyStructureModel>(CompanyStructureQueries.StoreProcSelectAll
            // , new
            // {
            //     @Id= Id
            // }
            // , null, true, null, CommandType.StoredProcedure);

            var res = dapper.Query<CompanyEntityViewModel>(CompanyFundamentalQueries.StoreProcSelectAllForEntities, new { }, null, true, null, CommandType.StoredProcedure);
            return res;
        }

        public IEnumerable<CompanyManagerViewModel> ManagersList(string companyId)
        {
            Guid CompanyId = new Guid(companyId);
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            var res = dapper.Query<CompanyManagerViewModel>(CompanyFundamentalQueries.StoreProcSelectAllManagers, new
            {
                @companyId = CompanyId
            }, null, true, null, CommandType.StoredProcedure);
            return res;
        }

        public IEnumerable<CompanySubsidaryViewModel> GetCompanySubsidaries(Guid Id)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            var res = dapper.Query<CompanySubsidaryViewModel>(CompanyFundamentalQueries.StoreProcSelectAllForCompanySubsidary, new { @CompanyID = Id }, null, true, null, CommandType.StoredProcedure);
            return res;
        }
        //public IEnumerable<CompanyDetailsViewModel> GetCompanyDetails(Guid CompanyId, Guid ProjectId)
        //{
        //    var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
        //    var res = dapper.Query<CompanyDetailsViewModel>(CompanyFundamentalQueries.StoreProcSelectAllForCompanyDetails, new { @CompanyID = CompanyId, @ProjectId = ProjectId }, null, true, null, CommandType.StoredProcedure);
        //    return res;
        //}

        public async Task<CompanyDetailsInsertUpdateViewModel> GetCompanyDetails(Guid CompanyId, Guid ProjectId)
        {
            CompanyDetailsInsertUpdateViewModel model = new CompanyDetailsInsertUpdateViewModel();
            List<CompanyDetailsListViewModel> compList = new List<CompanyDetailsListViewModel>();
            List<CompanySubsidaryDetails> subcompList = new List<CompanySubsidaryDetails>();
            List<RegionsDbViewModel> regionList = new List<RegionsDbViewModel>();

            List<KeyEmployees> keyempList = new List<KeyEmployees>();
            List<BoardOfDirectors> boardofdirectorsList = new List<BoardOfDirectors>();
            List<ShareHolding> shareholdingList = new List<ShareHolding>();

            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);

            using (var conn = new SqlConnection(dapper.ConnectionString))
            {
                await conn.OpenAsync();

                var res = await conn.QueryMultipleAsync(CompanyFundamentalQueries.StoreProcSelectAllForCompanyDetails, new
                {
                    @CompanyID = CompanyId,
                    @ProjectId = ProjectId
                }, null, null, CommandType.StoredProcedure);

                var table1 = await res.ReadAsync<CompanyDetailsListViewModel>();

                var table2 = await res.ReadAsync<CompanySubsidaryDetails>();

                var table3 = await res.ReadAsync<RegionsDbViewModel>();

                var table4 = await res.ReadAsync<KeyEmployees>();

                var table5 = await res.ReadAsync<BoardOfDirectors>();

                var table6 = await res.ReadAsync<ShareHolding>();

                if (table1 != null)
                {
                    foreach (var item in table1)
                    {
                        //CompanyDetailsListViewModel compModel = new CompanyDetailsListViewModel();
                        model.CompanyId = item.CompanyId;
                        model.CompanyName = item.CompanyName;
                        model.CompanyCode = item.CompanyCode;
                        model.DisplayName = item.DisplayName;
                        model.ParentId = item.ParentId;
                        model.LogoURL = item.LogoURL;
                        model.NatureId = item.NatureId;
                        model.ParentCompanyName = item.ParentCompanyName;
                        model.CompanyLocation = item.CompanyLocation;
                        model.IsHeadQuarters = item.IsHeadQuarters;
                        model.Telephone = item.Telephone;
                        model.Telephone2 = item.Telephone2;
                        model.Telephone3 = item.Telephone3;
                        model.Email = item.Email;
                        model.Email2 = item.Email2;
                        model.Email3 = item.Email3;
                        model.Website = item.Website;
                        model.YearOfEstb = item.YearOfEstb;
                        model.CompanyStageId = item.CompanyStageId.ToString();
                        model.NoOfEmployees = item.NoOfEmployees;
                        model.RankNumber = item.RankNumber;
                        model.RankRationale = item.RankRationale;
                        model.ImageActualName = item.ImageActualName;
                        model.ImageDisplayName = item.DisplayName;
                        model.ImageURL = item.LogoURL;
                        //compList.Add(compModel);
                    }
                }

                if (table2 != null)
                {
                    foreach (var item in table2)
                    {
                        CompanySubsidaryDetails subsidaryModel = new CompanySubsidaryDetails();
                        subsidaryModel.CompanySubsidaryId = item.CompanySubsidaryId;
                        subsidaryModel.SubsidaryCompanyName = item.SubsidaryCompanyName;
                        subsidaryModel.SubsidaryCompanyLocation = item.SubsidaryCompanyLocation;
                        subsidaryModel.IsHeadQuarters = item.IsHeadQuarters;
                        subcompList.Add(subsidaryModel);
                    }
                }
                try
                {
                    List<Guid> guids = new List<Guid>();
                    if (table3 != null)
                    {
                        foreach (var item in table3)
                        {
                            //RegionsDbViewModel regionModel = new RegionsDbViewModel();
                            //regionModel.RegionId = item.RegionId;
                            //regionList.Add(regionModel);

                            guids.Add(item.RegionId);
                        }
                    }
                    model.RegionIdArrays = guids.ToArray();
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                if (table4 != null)
                {
                    foreach (var item in table4)
                    {
                        KeyEmployees keyempModel = new KeyEmployees();
                        keyempModel.Id = item.Id;
                        keyempModel.KeyEmployeeName = item.KeyEmployeeName;
                        keyempModel.DesignationId = item.DesignationId;
                        keyempModel.ManagerId = item.ManagerId;
                        keyempModel.ManagerName = item.ManagerName;
                        keyempModel.KeyEmployeeDesignation = item.KeyEmployeeDesignation;
                        keyempModel.KeyEmployeeEmailId = item.KeyEmployeeEmailId;
                        keyempModel.KeyEmployeeComments = item.KeyEmployeeComments;
                        keyempList.Add(keyempModel);
                    }
                }

                if (table5 != null)
                {
                    foreach (var item in table5)
                    {
                        BoardOfDirectors boardofdireModel = new BoardOfDirectors();
                        boardofdireModel.Id = item.Id;
                        boardofdireModel.CompanyBoardOfDirecorsName = item.CompanyBoardOfDirecorsName;
                        boardofdireModel.KeyEmployeeName = item.KeyEmployeeName;
                        boardofdireModel.DesignationId = item.DesignationId;
                        boardofdireModel.ManagerId = item.ManagerId;
                        boardofdireModel.ManagerName = item.ManagerName;
                        boardofdireModel.CompanyBoardOfDirecorsOtherCompanyName = item.CompanyBoardOfDirecorsOtherCompanyName;
                        boardofdireModel.CompanyBoardOfDirecorsOtherCompanyDesignation = item.CompanyBoardOfDirecorsOtherCompanyDesignation;
                        boardofdireModel.KeyEmployeeEmailId = item.KeyEmployeeEmailId;
                        boardofdirectorsList.Add(boardofdireModel);
                    }
                }

                if (table6 != null)
                {
                    foreach (var item in table6)
                    {
                        ShareHolding shareholdingModel = new ShareHolding();
                        shareholdingModel.ShareHoldingName = item.ShareHoldingName;
                        shareholdingModel.EntityTypeId = item.EntityTypeId;
                        shareholdingModel.ShareHoldingPercentage = item.ShareHoldingPercentage;
                        shareholdingModel.Id = item.Id;
                        shareholdingList.Add(shareholdingModel);
                    }
                }

                //model.compList = compList;
                model.SubsidaryCompanyDetails = subcompList;
                model.RegionsDbModelList = regionList;
                model.KeyEmployee = keyempList;
                model.BoardOfDirector = boardofdirectorsList;
                model.ShareHoldingPattern = shareholdingList;
            }
            return model;
        }

        public IEnumerable<AllCompaniesListViewModel> GetAllCompaniesList()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            var res = dapper.Query<AllCompaniesListViewModel>(CompanyFundamentalQueries.StoreProcSelectAllForAllCompaniesList, new { }, null, true, null, CommandType.StoredProcedure);
            return res;
        }

        public IEnumerable<DesignationViewModel> AllDesignations()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            var res = dapper.Query<DesignationViewModel>(CompanyFundamentalQueries.StoreProcSelectAllDesignations, new { }, null, true, null, CommandType.StoredProcedure);
            return res;
        }

        public IEnumerable<AllCompanyStageListViewModel> GetAllCompanyStageList()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            var res = dapper.Query<AllCompanyStageListViewModel>(CompanyFundamentalQueries.StoreProcSelectCompanyStageList, new { }, null, true, null, CommandType.StoredProcedure);
            return res;
        }

        public Result<SpTransactionMessage> InsertUpdate(CompanyDetailsInsertUpdateViewModel companyDetailsInsertUpdateViewModel, AviraUser currenUser)
        {
            //Data table for Company Subsidary Details.
            DataTable companySubsidaryTable = new DataTable();
            companySubsidaryTable.Columns.Add("Id", typeof(Guid));
            companySubsidaryTable.Columns.Add("Name", typeof(string));
            companySubsidaryTable.Columns.Add("Location", typeof(string));
            companySubsidaryTable.Columns.Add("bitValue", typeof(bool));
            if (companyDetailsInsertUpdateViewModel.SubsidaryCompanyDetails != null & companyDetailsInsertUpdateViewModel.SubsidaryCompanyDetails.Count > 0)
            {
                foreach (var item in companyDetailsInsertUpdateViewModel.SubsidaryCompanyDetails)
                {
                    companySubsidaryTable.Rows.Add(new Object[] {
                        item.CompanySubsidaryId,
                        item.SubsidaryCompanyName,
                        item.SubsidaryCompanyLocation,
                        0
                    });
                }
            }


            //Data table for Company Key Employees Details.
            DataTable companyKeyEmpTable = new DataTable();
            companyKeyEmpTable.Columns.Add("ID", typeof(Guid));
            companyKeyEmpTable.Columns.Add("KeyEmployeeName", typeof(string));
            companyKeyEmpTable.Columns.Add("DesignationId", typeof(Guid));
            companyKeyEmpTable.Columns.Add("KeyEmployeeEmailId", typeof(string));
            companyKeyEmpTable.Columns.Add("KeyEmployeeComments", typeof(string));
            companyKeyEmpTable.Columns.Add("ManagerId", typeof(Guid));
            companyKeyEmpTable.Columns.Add("CompanyBoardOfDirecorsOtherCompanyName", typeof(string));
            companyKeyEmpTable.Columns.Add("CompanyBoardOfDirecorsOtherCompanyDesignation", typeof(string));


            if (companyDetailsInsertUpdateViewModel.KeyEmployee != null & companyDetailsInsertUpdateViewModel.KeyEmployee.Count > 0)
            {
                foreach (var item in companyDetailsInsertUpdateViewModel.KeyEmployee.Where(x => !string.IsNullOrEmpty(x.KeyEmployeeName) && 
                                                                                                x.DesignationId != null))
                {
                    companyKeyEmpTable.Rows.Add(new Object[] {
                        item.Id,
                        item.KeyEmployeeName,
                        item.DesignationId,
                        item.KeyEmployeeEmailId,
                        item.KeyEmployeeComments,
                        item.ManagerId,
                        item.CompanyBoardOfDirecorsOtherCompanyName,
                        item.CompanyBoardOfDirecorsOtherCompanyDesignation,
                    });
                }
            }


            //Data table for Company Board Of Directors Details.
            DataTable companyBoardOfDirectorsTable = new DataTable();
            companyBoardOfDirectorsTable.Columns.Add("ID", typeof(Guid));
            companyBoardOfDirectorsTable.Columns.Add("KeyEmployeeName", typeof(string));
            companyBoardOfDirectorsTable.Columns.Add("DesignationId", typeof(Guid));
            companyBoardOfDirectorsTable.Columns.Add("KeyEmployeeEmailId", typeof(string));
            companyBoardOfDirectorsTable.Columns.Add("KeyEmployeeComments", typeof(string));
            companyBoardOfDirectorsTable.Columns.Add("ManagerId", typeof(Guid));
            companyBoardOfDirectorsTable.Columns.Add("CompanyBoardOfDirecorsOtherCompanyName", typeof(string));
            companyBoardOfDirectorsTable.Columns.Add("CompanyBoardOfDirecorsOtherCompanyDesignation", typeof(string));

            if (companyDetailsInsertUpdateViewModel.BoardOfDirector != null & companyDetailsInsertUpdateViewModel.BoardOfDirector.Count > 0)
            {
                foreach (var item in companyDetailsInsertUpdateViewModel.BoardOfDirector.Where(x => !string.IsNullOrEmpty(x.KeyEmployeeName) &&
                                                                                                x.DesignationId != null))
                {
                    companyBoardOfDirectorsTable.Rows.Add(new Object[] {
                        item.Id,
                        item.KeyEmployeeName,
                        item.DesignationId,
                        item.KeyEmployeeEmailId,
                        item.KeyEmployeeComments,
                        item.ManagerId,
                        item.CompanyBoardOfDirecorsOtherCompanyName,
                        item.CompanyBoardOfDirecorsOtherCompanyDesignation,
                    });
                }
            }


            //Data table for Company Share Holding Pattern Details.
            DataTable companyShareHoldingPatternTable = new DataTable();
            companyShareHoldingPatternTable.Columns.Add("Id", typeof(Guid));
            companyShareHoldingPatternTable.Columns.Add("Name", typeof(string));
            companyShareHoldingPatternTable.Columns.Add("EntityTypeId", typeof(Guid));
            companyShareHoldingPatternTable.Columns.Add("DecimalValue", typeof(decimal));

            if (companyDetailsInsertUpdateViewModel.ShareHoldingPattern != null & companyDetailsInsertUpdateViewModel.ShareHoldingPattern.Count > 0)
            {
                foreach (var item in companyDetailsInsertUpdateViewModel.ShareHoldingPattern.Where(x => !string.IsNullOrEmpty(x.ShareHoldingName) &&
                                                                                                x.EntityTypeId != null))
                {
                    companyShareHoldingPatternTable.Rows.Add(new Object[] {
                        item.Id,
                        item.ShareHoldingName,
                        item.EntityTypeId,
                        item.ShareHoldingPercentage
                    });
                }
            }


            //Data table for Company Geographies Details.
            DataTable companyGeographiesTable = new DataTable();
            companyGeographiesTable.Columns.Add("ID", typeof(Guid));

            companyDetailsInsertUpdateViewModel.RegionId = companyDetailsInsertUpdateViewModel.RegionIdArrays.OfType<Guid>().ToList();

            if (companyDetailsInsertUpdateViewModel.RegionId != null & companyDetailsInsertUpdateViewModel.RegionId.Count > 0)
            {
                foreach (var Id in companyDetailsInsertUpdateViewModel.RegionId)
                {
                    companyGeographiesTable.Rows.Add(new Object[] {
                        Id,
                    });
                }
            }

            //Insert the Company Details.
            var spTransactionMessage = _dbHelper.ExecuteQuery(CompanyFundamentalQueries.StoreProcInsertUpdateCompDetails,
           new
           {
               @CompanyId = companyDetailsInsertUpdateViewModel.CompanyId,
               @CompanyName = companyDetailsInsertUpdateViewModel.CompanyName,
               @CompanyCode = companyDetailsInsertUpdateViewModel.CompanyCode,
               @DisplayName = companyDetailsInsertUpdateViewModel.ImageDisplayName,
               @LogoURL = companyDetailsInsertUpdateViewModel.ImageURL,
               @ActualImageName = companyDetailsInsertUpdateViewModel.ImageActualName,
               @NatureId = companyDetailsInsertUpdateViewModel.NatureId,
               @ParentCompanyName = companyDetailsInsertUpdateViewModel.ParentCompanyName,
               @ParentId = companyDetailsInsertUpdateViewModel.ParentId,
               @CompanyLocation = companyDetailsInsertUpdateViewModel.CompanyLocation,
               @IsHeadQuarters = 1,
               @Telephone = companyDetailsInsertUpdateViewModel.Telephone,
               @Telephone2 = companyDetailsInsertUpdateViewModel.Telephone2,
               @Telephone3 = companyDetailsInsertUpdateViewModel.Telephone3,
               @Email = companyDetailsInsertUpdateViewModel.Email,
               @Email2 = companyDetailsInsertUpdateViewModel.Email2,
               @Email3 = companyDetailsInsertUpdateViewModel.Email3,
               @Website = companyDetailsInsertUpdateViewModel.Website,
               @YearOfEstb = companyDetailsInsertUpdateViewModel.YearOfEstb,
               @CompanyStageId = companyDetailsInsertUpdateViewModel.CompanyStageId,
               @NoOfEmployees = companyDetailsInsertUpdateViewModel.NoOfEmployees,
               @RankNumber = companyDetailsInsertUpdateViewModel.RankNumber,
               @RankRationale = companyDetailsInsertUpdateViewModel.RankRationale,
               @IsDeleted = 0,
               @AviraUserId = currenUser.Id
           }
                , CommandType.StoredProcedure);

            if (spTransactionMessage.IsSuccess == false)
            {
                return spTransactionMessage;
                
            }
            else
            {
                companyDetailsInsertUpdateViewModel.CompanyId = (spTransactionMessage != null && !string.IsNullOrEmpty(spTransactionMessage.Value.Id) ? new Guid(spTransactionMessage.Value.Id) : Guid.Empty);
            }
            

            if (companyDetailsInsertUpdateViewModel.CompanyId == null || companyDetailsInsertUpdateViewModel.CompanyId == new Guid("00000000-0000-0000-0000-000000000000"))
            {
                companyDetailsInsertUpdateViewModel.CompanyId = new Guid(spTransactionMessage.Value.Message);
            }


            //Insert the Company Project Map.
            var spTransactionMessage1 = _dbHelper.ExecuteQuery(CompanyFundamentalQueries.StoreProcInsertUpdateCompProjMap,
               new
               {
                   @ID = new Guid(),
                   @CompanyID = companyDetailsInsertUpdateViewModel.CompanyId,
                   @ProjectID = companyDetailsInsertUpdateViewModel.ProjectId,
                   @IsDeleted = 0,
                   @AviraUserId = currenUser.Id
               }
                    , CommandType.StoredProcedure);

            if (spTransactionMessage1.IsSuccess == false)
            {
                return spTransactionMessage1;
            }

            if (companyDetailsInsertUpdateViewModel.SubsidaryCompanyDetails != null && companyDetailsInsertUpdateViewModel.SubsidaryCompanyDetails.Count(x => x.CompanySubsidaryId != Guid.Empty) > 0)
            {
                //Insert the Subsidary Company Details.
                var spTransactionMessage2 = _dbHelper.ExecuteQuery(CompanyFundamentalQueries.StoreProcInsertUpdateCompSubsidary,
                      new
                      {
                          @CompanyId = companyDetailsInsertUpdateViewModel.CompanyId,
                          @CompanySubsidaryList = companySubsidaryTable,
                          @UserCreatedById = currenUser.Id
                      }
                           , CommandType.StoredProcedure);

                if (spTransactionMessage2.IsSuccess == false)
                {
                    return spTransactionMessage2;
                }
            }



            //Insert the Company Key Employees Map Details.
            var spTransactionMessage3 = _dbHelper.ExecuteQuery(CompanyFundamentalQueries.StoreProcInsertUpdateCompKeyEmp,
                  new
                  {
                      @CompanyId = companyDetailsInsertUpdateViewModel.CompanyId,
                      @KeyEmpListmap = companyKeyEmpTable,
                      @UserCreatedById = currenUser.Id
                  }
                       , CommandType.StoredProcedure);


            if (spTransactionMessage3.IsSuccess == false)
            {
                return spTransactionMessage3;
            }

            //Insert the Company Board Of Directors Map Details.
            var spTransactionMessage4 = _dbHelper.ExecuteQuery(CompanyFundamentalQueries.StoreProcInsertUpdateCompBoardOfDirectories,
                  new
                  {
                      @CompanyId = companyDetailsInsertUpdateViewModel.CompanyId,
                      @KeyBoardOfDirectorsListmap = companyBoardOfDirectorsTable,
                      @UserCreatedById = currenUser.Id
                  }
                       , CommandType.StoredProcedure);


            if (spTransactionMessage4.IsSuccess == false)
            {
                return spTransactionMessage4;
            }

            //Insert the Company Share Holding Pattern Map Details.
            var spTransactionMessage5 = _dbHelper.ExecuteQuery(CompanyFundamentalQueries.StoreProcInsertUpdateCompShareHolding,
                  new
                  {
                      @CompanyId = companyDetailsInsertUpdateViewModel.CompanyId,
                      @ShareHolidingListmap = companyShareHoldingPatternTable,
                      @UserCreatedById = currenUser.Id
                  }
                       , CommandType.StoredProcedure);

            if (spTransactionMessage5.IsSuccess == false)
            {
                return spTransactionMessage5;
            }

            //Insert the Company Geographies Details.
            var spTransactionMessage6 = _dbHelper.ExecuteQuery(CompanyFundamentalQueries.StoreProcInsertUpdateCompGeographies,
                  new
                  {
                      @CompanyId = companyDetailsInsertUpdateViewModel.CompanyId,
                      @GeographiesListmap = companyGeographiesTable,
                      @UserCreatedById = currenUser.Id
                  }
                       , CommandType.StoredProcedure);

            if (spTransactionMessage6.IsSuccess == false)
            {
                return spTransactionMessage6;
            }

            return spTransactionMessage;
        }

        public IEnumerable<CompanyNatureViewModel> NaturesList()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            var res = dapper.Query<CompanyNatureViewModel>(CompanyFundamentalQueries.StoreProcSelectAllNatures, new
            { }, null, true, null, CommandType.StoredProcedure);
            return res;
        }
    }
}
