﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.Masters
{
    public class StatusViewModel
    {
        public Guid Id { get; set; }
        public string StatusName { get; set; }
        public string Description { get; set; }
        public string StatusType { get; set; }
        
    }
}
