﻿CREATE TABLE [dbo].[MarketValuesummary] (
    [Projectid]          UNIQUEIDENTIFIER NOT NULL,
    [Regionid]           UNIQUEIDENTIFIER NOT NULL,
    [Countryid]          UNIQUEIDENTIFIER NOT NULL,
    [SegmentId]          UNIQUEIDENTIFIER NOT NULL,
    [SubSegmentId]       UNIQUEIDENTIFIER NOT NULL,
    [ValueConversionId]  UNIQUEIDENTIFIER NULL,
    [VolumeConversionId] UNIQUEIDENTIFIER NULL,
    [Year]               INT              NOT NULL,
    [Volume]             DECIMAL (38, 2)  NULL,
    [value]              DECIMAL (38, 2)  NULL,
    CONSTRAINT [PK_MarketValuesummary] PRIMARY KEY CLUSTERED ([Projectid] ASC, [Regionid] ASC, [Countryid] ASC, [SegmentId] ASC, [SubSegmentId] ASC, [Year] ASC)
);

