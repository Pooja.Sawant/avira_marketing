﻿  
  
CREATE PROCEDURE [dbo].[SApp_GetIndustriesByBoardIndustryId]
(
	@BoardIndustryIdList dbo.[udtUniqueIdentifier] readonly
)  
AS  
/*================================================================================  
Procedure Name: [SApp_GetIndustriesByBoardIndustryId]  
Author: Sai Krishna  
Create date: 10/05/2019  
Description: Get list of Industries by parentId from table   
Change History  
Date        Developer     Reason  
__________  ____________  ______________________________________________________  
10/05/2019  Sai krishna   Initial Version  
================================================================================*/  
  
BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
declare @SegmentId uniqueidentifier;
select @SegmentId = Id from Segment
where SegmentName = 'End User';

SELECT Ind.Id,
	Ind.SubSegmentName IndustryName,
	Ind.ParentId
FROM SubSegment Ind
INNER JOIN @BoardIndustryIdList t1
ON Ind.ParentId = t1.ID
WHERE IsDeleted = 0 
and ind.SegmentId = @SegmentId
ORDER By Ind.ParentId

END