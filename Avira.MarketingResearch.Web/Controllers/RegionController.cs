﻿using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.Models;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;

namespace Avira.MarketingResearch.Web.Controllers
{
    [NoCache]
    [Authorization]
    public class RegionController : BaseController
    {
        private ILoggerManager _logger;
        public readonly IServiceRepository _serviceRepository;
        public RegionController(IServiceRepository serviceRepository, ILoggerManager logger)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
        }
        public IActionResult Index()
        { 
            return View();
        }
        public ActionResult LoadData(RegionViewModel regionViewModel)
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            int totalRecords = 0;
            List<RegionViewModel> data = null;
            try
            {
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                string search = Request.Form["search[value]"];

                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDir = Request.Form["order[0][dir]"].FirstOrDefault();
                var SortDirection = -1;
                if (sortColumnDir == "asc")
                {
                    SortDirection = 1;
                }
                else
                {
                    SortDirection = -1;
                }
                int skip = start != null ? Convert.ToInt32(start) : 0;

                int pageSize = length.ToString() != null ? Convert.ToInt32(length) : 0;
                int PageStart = start.ToString() != null ? Convert.ToInt32(start) : 0;
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.GetResponse(contollerName, PageStart, pageSize, SortDirection, sortColumn, search);
                response.EnsureSuccessStatusCode();
                List<RegionViewModel> regions = response.Content.ReadAsAsync<List<RegionViewModel>>().Result;
                ViewBag.Title = "All Region";
                data = regions;              
                foreach (var item in regions)
                {
                    totalRecords = item.TotalItems;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data });
        }
        public IActionResult Create()
        {
            try
            {
                HttpResponseMessage response = _serviceRepository.GetSubResponse("Region");
                response.EnsureSuccessStatusCode();
                List<CountryViewModel> country = response.Content.ReadAsAsync<List<CountryViewModel>>().Result;
                ViewData["CountryList"] = new SelectList(country, "CountryId", "CountryName", "---select---");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View();
        }
        [HttpPost]
        public IActionResult Create(RegionInsertDbViewModel region)
        {           
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, region);
                response.EnsureSuccessStatusCode();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return RedirectToAction("Index");
        }         
        [HttpGet]
        public IActionResult Edit(string id)
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage responseRegion = _serviceRepository.GetByIdResponse(contollerName, id);
                responseRegion.EnsureSuccessStatusCode();
                RegionUpdateDbViewModel regionUpdateDbViewModel = responseRegion.Content.ReadAsAsync<RegionUpdateDbViewModel>().Result;                
                HttpResponseMessage CountryResponse = _serviceRepository.GetSubResponse("Region");
                CountryResponse.EnsureSuccessStatusCode();
                List<CountryViewModel> countryViewModel = CountryResponse.Content.ReadAsAsync<List<CountryViewModel>>().Result;
                HttpResponseMessage regionById = _serviceRepository.GetByIdResponse(contollerName, id);
                responseRegion.EnsureSuccessStatusCode();
                RegionViewModel regionViewModel = regionById.Content.ReadAsAsync<RegionViewModel>().Result;
                //regionUpdateDbViewModel.CountryIDList = countryViewModel.Select(x => x.CountryId).ToList();                
                ViewData["CountryList"] = new SelectList(countryViewModel, "CountryId", "CountryName",regionViewModel.CountryList.Select(x => x.CountryId).ToList());
                //--------------               
                var categories = countryViewModel.Select(c => new
                {
                    CountryId = c.CountryId,
                    CountryName = c.CountryName
                }).ToList();
                regionUpdateDbViewModel.Countries = new MultiSelectList(categories, "CountryId", "CountryName", regionViewModel.CountryList.Select(x => x.CountryId).ToArray());
               // regionUpdateDbViewModel.CountryId = new string[] { "798fc43d-f6a3-439d-97ee-001b53f03a80" };
                return View(regionUpdateDbViewModel);  
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                ErrorViewModel err = new ErrorViewModel { RequestId = "1234" };
                //Log the exception 
                return View("Error", err);
            }
        }
        [HttpPost]
        public IActionResult Edit(RegionUpdateDbViewModel region)
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.PutResponse(contollerName, region);
                response.EnsureSuccessStatusCode();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return RedirectToAction("Index");
        }
        [HttpGet]
        public IActionResult Delete(string id)
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage responseRegion = _serviceRepository.GetByIdResponse(contollerName, id);
                responseRegion.EnsureSuccessStatusCode();
                RegionUpdateDbViewModel regionUpdateDbViewModel = responseRegion.Content.ReadAsAsync<RegionUpdateDbViewModel>().Result; 

                HttpResponseMessage regionById = _serviceRepository.GetByIdResponse(contollerName, id);
                responseRegion.EnsureSuccessStatusCode();
                RegionViewModel regionViewModel = regionById.Content.ReadAsAsync<RegionViewModel>().Result;

                regionUpdateDbViewModel.CountryIDList = regionViewModel.CountryList.Select(x => x.CountryId).ToList();//countryViewModel.Select(x => x.CountryId).ToList();
                ViewData["CountryList"] = new SelectList(regionViewModel.CountryList, "CountryId", "CountryName", "---select---");
                return View(regionUpdateDbViewModel);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                ErrorViewModel err = new ErrorViewModel { RequestId = "1234" };
                //Log the exception 
                return View("Error", err);
            }
           
        }
        [HttpPost]
        public IActionResult Delete(RegionUpdateDbViewModel regionUpdateDbViewModel)
        {
            var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            HttpResponseMessage responseRegion = _serviceRepository.GetByIdResponse(contollerName, regionUpdateDbViewModel.Id.ToString());

            HttpResponseMessage regionById = _serviceRepository.GetByIdResponse(contollerName, regionUpdateDbViewModel.Id.ToString());
            responseRegion.EnsureSuccessStatusCode();
            RegionViewModel regionViewModel = regionById.Content.ReadAsAsync<RegionViewModel>().Result;

            regionUpdateDbViewModel.CountryIDList = regionViewModel.CountryList.Select(x => x.CountryId).ToList();
            try
            {   
                HttpResponseMessage response = _serviceRepository.DeleteResponse(contollerName, regionUpdateDbViewModel);
                response.EnsureSuccessStatusCode();
                ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                if (viewModel != null)
                {
                    ViewBag.Message = viewModel.Message;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                ErrorViewModel err = new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier };
                return View("Error", err);
            }
            return RedirectToAction("Index");
        } 
    }
}