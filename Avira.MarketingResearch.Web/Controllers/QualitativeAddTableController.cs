﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Model.ViewModels.QualitativeInfo;
using Avira.MarketingResearch.Web.Utils;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Avira.MarketingResearch.Web.Controllers
{
    public class QualitativeAddTableController : BaseController
    {
        public readonly IServiceRepository _serviceRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;
        string contentRootPath = string.Empty;
        private ILoggerManager _logger;

        public QualitativeAddTableController(IServiceRepository serviceRepository, ILoggerManager logger, IHostingEnvironment hostingEnvironment, IConfiguration configuration)
        {
            _hostingEnvironment = hostingEnvironment;
            _serviceRepository = serviceRepository;
            _logger = logger;
            _configuration = configuration;
            contentRootPath = _hostingEnvironment.ContentRootPath;
        }

        public IActionResult Index(string Id ,string ProjectId)
        {
            QualitativeAddTableViewModel qualitativeAddTableViewModel = new QualitativeAddTableViewModel();
            QualitativeGenerateTableViewModel qualitativeGenerateTableViewModel = new QualitativeGenerateTableViewModel();
            QualitativeNoteViewModel qualitativeNoteViewModel = new QualitativeNoteViewModel();
            QualitativeGenerateTableCRUDViewModel qualitativeGenerateTableCRUDViewModel = new QualitativeGenerateTableCRUDViewModel();
            try
            {
                if(!string.IsNullOrEmpty(Id))
                {
                    HttpResponseMessage response = _serviceRepository.GetByIdResponse("QualitativeAddTable", Id);
                    response.EnsureSuccessStatusCode();
                    List<QualitativeAddTableViewModel> qualitativeAddTableList = response.Content.ReadAsAsync<List<QualitativeAddTableViewModel>>().Result;
                    qualitativeAddTableViewModel = qualitativeAddTableList[0];
                    //qualitativeGenerateTableCRUDViewModel.TableData = JsonConvert.DeserializeObject<List<KeyValueViewModel>>(qualitativeAddTableViewModel.TableData);
                    //qualitativeGenerateTableCRUDViewModel.HeaderData = JsonConvert.DeserializeObject<List<KeyValueViewModel>>(qualitativeAddTableViewModel.TableMetaData);

                    qualitativeGenerateTableCRUDViewModel.TableData= JsonConvert.DeserializeObject<Dictionary<string, string>>(qualitativeAddTableViewModel.TableData);
                    qualitativeGenerateTableCRUDViewModel.HeaderData = JsonConvert.DeserializeObject<Dictionary<string, string>>(qualitativeAddTableViewModel.TableMetaData);
                    string FinalTable = string.Empty;
                    FinalTable = GenerateTableWithData(qualitativeAddTableViewModel.TableName, qualitativeGenerateTableCRUDViewModel);
                    qualitativeGenerateTableViewModel.TableHTML = FinalTable;

                    qualitativeGenerateTableViewModel.TableName = qualitativeAddTableViewModel.TableName;
                    qualitativeGenerateTableViewModel.Rows = 0;
                    qualitativeGenerateTableViewModel.IsHeaderRequired = true;
                    qualitativeGenerateTableViewModel.Columns = 0;
                    qualitativeGenerateTableViewModel.IsImageRequired = false;

                    qualitativeGenerateTableCRUDViewModel.Id = new Guid(Id);
                    HttpResponseMessage qualitativeNoteResponse = _serviceRepository.GetQualitativeNoteByIdResponse("QualitativeNote", Id, ProjectId);
                    qualitativeNoteViewModel = qualitativeNoteResponse.Content.ReadAsAsync<QualitativeNoteViewModel>().Result;
                }
                else
                {
                    qualitativeAddTableViewModel.TableName = "";
                    qualitativeAddTableViewModel.TableData = "";
                    qualitativeAddTableViewModel.TableMetaData = "";
                }
                qualitativeAddTableViewModel.ProjectId = new Guid(ProjectId);
                qualitativeGenerateTableViewModel.ProjectId = new Guid(ProjectId);
                qualitativeGenerateTableCRUDViewModel.qualitativeAddTableViewModel = qualitativeAddTableViewModel;
                qualitativeGenerateTableCRUDViewModel.qualitativeGenerateTableViewModel = qualitativeGenerateTableViewModel;
                qualitativeGenerateTableCRUDViewModel.QualitativeNoteViewModel = qualitativeNoteViewModel ?? new QualitativeNoteViewModel();
                qualitativeGenerateTableCRUDViewModel.ProjectId = new Guid(ProjectId);
                SetRoleList();
                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, new Guid(ProjectId));
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                    ViewBag.StatusName = qualitativeNoteViewModel.QualitativeStatusName;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(qualitativeGenerateTableCRUDViewModel);
        }

        [HttpPost]
        public IActionResult Index(QualitativeGenerateTableCRUDViewModel Model, string actionName)
        {

            QualitativeAddTableViewModel qualitativeAddTableViewModel = new QualitativeAddTableViewModel();
            //QualitativeGenerateTableCRUDViewModel qualitativeGenerateTableCRUDViewModel = new QualitativeGenerateTableCRUDViewModel();
            QualitativeGenerateTableViewModel qualitativeGenerateTableViewModel = new QualitativeGenerateTableViewModel();
            QualitativeNoteViewModel qualitativeNoteViewModel = new QualitativeNoteViewModel();
            try
            {

                if (actionName != null || actionName != string.Empty && actionName == "")
                {
                    string TableName = Model.qualitativeGenerateTableViewModel.TableName;
                    int numrows = Model.qualitativeGenerateTableViewModel.Rows;
                    int numColumns = Model.qualitativeGenerateTableViewModel.Columns;
                    bool isHeader = Model.qualitativeGenerateTableViewModel.IsHeaderRequired;
                    bool isimage = Model.qualitativeGenerateTableViewModel.IsImageRequired;

                    string FinalTable = string.Empty;

                    FinalTable = GenerateTable(TableName, numrows, numColumns, isHeader, isimage, "");


                    //Table end.                
                    //qualitativeGenerateTableViewModel.TableHTML = sb.ToString();
                    qualitativeGenerateTableViewModel.TableHTML = FinalTable;

                    qualitativeGenerateTableViewModel.TableName = TableName;
                    qualitativeGenerateTableViewModel.Rows = numrows;
                    qualitativeGenerateTableViewModel.IsHeaderRequired = isHeader;
                    qualitativeGenerateTableViewModel.Columns = numColumns;
                    qualitativeGenerateTableViewModel.IsImageRequired = isimage;

                    Model.Id = Model.Id;
                    Model.ProjectId = Model.ProjectId;
                    Model.qualitativeAddTableViewModel = qualitativeAddTableViewModel;
                    Model.qualitativeGenerateTableViewModel = qualitativeGenerateTableViewModel;

                    HttpResponseMessage qualitativeNoteResponse = _serviceRepository.GetQualitativeNoteByIdResponse("QualitativeNote", Model.Id.ToString(), Model.ProjectId.ToString());
                    qualitativeNoteViewModel = qualitativeNoteResponse.Content.ReadAsAsync<QualitativeNoteViewModel>().Result;
                    Model.QualitativeNoteViewModel = qualitativeNoteViewModel ?? new QualitativeNoteViewModel();
                    ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, Model.ProjectId);
                    if (project != null)
                    {
                        ViewBag.ProjectId = project.Id;
                        ViewBag.ProjectName = project.ProjectName;
                        ViewBag.ProjectCode = project.ProjectCode;
                        ViewBag.Approver = project.Approver;
                        ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                        ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                        ViewBag.StatusName = qualitativeNoteViewModel.QualitativeStatusName;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(Model);
        }

        public string GenerateTableWithData(string TableName, QualitativeGenerateTableCRUDViewModel Model)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                sb.Append("<tr>");
                foreach(var header in Model.HeaderData)
                {
                    sb.Append("<th style='background-color: #C0C0C0;border: 1px solid #ccc'><input value='"+ header.Value + "' class ='form-control' type='text' name='" + header.Key + "' id='"+ header.Key + "'></th>");
                }
                sb.Append("</tr>");

                var distnctRows = Model.TableData.Select(x => x.Key.Split('_')[0]).Distinct();
                for (var rownum = 0; rownum < distnctRows.Count(); rownum++)
                {
                   sb.Append("<tr>");
                     foreach(var header in Model.HeaderData)
                     {
                         var dataKey = rownum + "_" + header.Key.Split("_")[1].ToString();
                         foreach (var tableDataTD in Model.TableData.Where(x => x.Key.EndsWith(dataKey, StringComparison.OrdinalIgnoreCase)))
                         {
                            sb.Append("<td style='background-color: #D3D3D3;border: 1px solid #ccc'><input value='" + tableDataTD.Value + "' class ='form-control' type='text' name='" + tableDataTD.Key + "' id='" + tableDataTD.Key + "'></td>");
                         }
                     }
                   sb.Append("</tr>");

                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return sb.ToString();
        }

        public string GenerateTable(string TableName, int numrows, int numColumns, bool isHeader, bool isimage, string Json = "")
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                if (isHeader)
                {
                    sb.Append("<tr>");
                    for (var i = 0; i <= numColumns - 1; i++)
                    {
                        sb.Append("<th style='background-color: #C0C0C0;border: 1px solid #ccc'><input value='' class ='form-control' type='text' name='Header_" + i + "' id='Header_" + i + "'></th>");
                    }
                    sb.Append("</tr>");
                }
                for (var r = 0; r <= numrows - 1; r++)
                {
                    sb.Append("<tr>");
                    for (var c = 0; c <= numColumns - 1; c++)
                    {
                        sb.Append("<td style='background-color: #D3D3D3;border: 1px solid #ccc'><input value='' class ='form-control' type='text' name=" + r + "_" + c + " id=" + r + "_" + c + "></td>");
                    }
                    if (isimage)
                    {
                        sb.Append("<td style='background-color: #D3D3D3;border: 1px solid #ccc'><input name='file_" + r + "'  type='file' id='file_" + r + "'></td>");
                    }
                    sb.Append("</tr>");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return sb.ToString();
        }
        
        public JsonResult PostData(string json)
        {
            try
            {
                ViewBag.Message = string.Empty;
                string ActionName = string.Empty;
                string strApproverNotes = string.Empty;
                string strAuthorNotes = string.Empty;

                QualitativeGenerateTableCRUDViewModel qualitativeGenerateTableCRUDViewModel = new QualitativeGenerateTableCRUDViewModel();
                QualitativeAddTableViewModel qualitativeAddTableViewModel = new QualitativeAddTableViewModel();
                QualitativeGenerateTableViewModel qualitativeGenerateTableViewModel = new QualitativeGenerateTableViewModel();
                QualitativeNoteViewModel qualitativeNoteViewModel = new QualitativeNoteViewModel();

                //qualitativeGenerateTableCRUDViewModel.QualitativeNoteViewModel.CompanyType = "QualitativeAddTable";

                List<Dictionary<string, string>> obj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(json);
                Dictionary<string, string> HeaderList = new Dictionary<string, string>();
                Dictionary<string, string> ColumnList = new Dictionary<string, string>();
                foreach (Dictionary<string, string> lst in obj)
                {
                    if (lst["name"].Contains("Header_"))
                    {
                        HeaderList.Add(lst["name"].ToString(), lst["value"].ToString());
                    }
                    else if (lst["name"].Contains("TableName"))
                    {
                        qualitativeAddTableViewModel.TableName = lst["value"].ToString();
                    }
                    else if (lst["name"].Contains("ActionName"))
                    {
                        ActionName = lst["value"].ToString();
                    }
                    else if (lst["name"].Contains("QualitativeNoteViewModel.AuthorRemark"))
                    {
                        strAuthorNotes = lst["value"].ToString();
                        qualitativeNoteViewModel.AuthorRemark = lst["value"].ToString();
                    }
                    else if (lst["name"].Contains("QualitativeNoteViewModel.ApproverRemark"))
                    {
                        strApproverNotes = lst["value"].ToString();
                        qualitativeNoteViewModel.ApproverRemark = lst["value"].ToString();
                    }
                    else if (lst["name"].Contains("ProjectId"))
                    {
                        Guid ProjectId = Guid.Parse(lst["value"].ToString());
                        qualitativeAddTableViewModel.ProjectId = ProjectId;
                        qualitativeGenerateTableCRUDViewModel.ProjectId = ProjectId;
                    }
                    else if(lst["name"].Contains("Id"))
                    {
                        Guid Id = Guid.Parse(lst["value"].ToString());
                        qualitativeGenerateTableCRUDViewModel.Id = Id;
                    }
                    else if (lst["name"] != "QualitativeNoteViewModel.AuthorRemark" && lst["name"] != "QualitativeNoteViewModel.ApproverRemark" && lst["name"] != "ProjectId" && lst["name"] != "TableName" && lst["name"] != "Rows" && lst["name"] != "Columns" && lst["name"] != "IsHeaderRequired" && lst["name"] != "IsImageRequired")
                    {
                        ColumnList.Add(lst["name"].ToString(), lst["value"].ToString());
                    }

                }

                string TableMetadata = DictionaryToJson(HeaderList);
                string TableData = DictionaryToJson(ColumnList);

                qualitativeAddTableViewModel.TableData = TableData;
                qualitativeAddTableViewModel.TableMetaData = TableMetadata;
                qualitativeNoteViewModel.ApproverRemark = strApproverNotes;
                qualitativeNoteViewModel.AuthorRemark = strAuthorNotes;


                qualitativeGenerateTableCRUDViewModel.QualitativeNoteViewModel = qualitativeNoteViewModel;
                qualitativeGenerateTableCRUDViewModel.qualitativeAddTableViewModel = qualitativeAddTableViewModel;

                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();

                if (ActionName.Equals("Save"))
                {
                    qualitativeGenerateTableCRUDViewModel.QualitativeNoteViewModel.QualitativeStatusName = "Draft";
                    HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, qualitativeGenerateTableCRUDViewModel);
                    responseSave.EnsureSuccessStatusCode();
                    ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                    if (viewModel != null)
                    {
                        return Json(viewModel);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Json("error");
        }

        private void SetRoleList()
        {
            try
            {
                var roles = HttpContext.Session.GetString("userRoles");
                var userRoles = JsonConvert.DeserializeObject<List<RoleViewModel>>(roles);
                List<string> RoleListItems = new List<string>();
                foreach (var item in userRoles)
                    RoleListItems.Add(item.UserRoleName);
                ViewBag.UserRoleList = RoleListItems;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        private string DictionaryToJson(Dictionary<string, string> dict)
        {
            string entries = "";
            foreach (KeyValuePair<string, string> entry in dict)
            {
                if (entries.Length > 0)
                    entries = entries + "," + "\"" +entry.Key + "\":\"" +entry.Value.ToString() + "\"";
                else
                    entries = "\"" + entry.Key + "\":\"" + entry.Value.ToString() + "\"";
            }
            return "{" +entries + "}";
        }
    }
}