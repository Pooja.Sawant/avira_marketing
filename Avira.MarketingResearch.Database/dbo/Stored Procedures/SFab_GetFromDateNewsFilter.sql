﻿CREATE procedure [dbo].[SFab_GetFromDateNewsFilter]
(
@ProjectId uniqueidentifier = null,
@AviraUserID uniqueidentifier = null
)
as
/*================================================================================
Procedure Name: [dbo].[SFab_GetFromDateNewsFilter]
Author: Pooja Sawant
Create date: 29/10/2019
Description: Get from date from News table by projectid default
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
29/10/2019	Pooja			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
DECLARE @StartDate DATETIME;
DECLARE @EndDate DATETIME;
declare @ProjectApprovedStatusId uniqueidentifier
select @ProjectApprovedStatusId = ID from ProjectStatus
where ProjectStatusName = 'Completed';

select top 1 CONVERT(varchar(11),date,103) as FromDate from News where 
 (exists (select 1 from project 
			where project.ID = News.ProjectID
			and project.ProjectStatusID = @ProjectApprovedStatusId or ProjectId is null)
			
			) and
ProjectID=@ProjectId order by date asc


end