﻿CREATE TABLE [dbo].[Industry](
	[Id] [uniqueidentifier] NOT NULL,
	[IndustryName] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](256) NULL,
	[ParentId] [uniqueidentifier] NULL,
	[CreatedOn] [datetime2](7) NULL,
	[UserCreatedById] [uniqueidentifier] NULL,
	[ModifiedOn] [datetime2](7) NULL,
	[UserModifiedById] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeletedOn] [datetime2](7) NULL,
	[UserDeletedById] [uniqueidentifier] NULL,
 CONSTRAINT [PK_Industry] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Industry] ADD  CONSTRAINT [DF_Industry_IndustryName]  DEFAULT ('') FOR [IndustryName]
GO
ALTER TABLE [dbo].[Industry] ADD  CONSTRAINT [DF_Industry_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Industry]  WITH CHECK ADD  CONSTRAINT [FK_Industry_AviraUser] FOREIGN KEY([UserCreatedById])
REFERENCES [dbo].[AviraUser] ([Id])
GO
ALTER TABLE [dbo].[Industry] CHECK CONSTRAINT [FK_Industry_AviraUser]
GO
ALTER TABLE [dbo].[Industry]  WITH CHECK ADD  CONSTRAINT [FK_Industry_AviraUser1] FOREIGN KEY([UserModifiedById])
REFERENCES [dbo].[AviraUser] ([Id])
GO
ALTER TABLE [dbo].[Industry] CHECK CONSTRAINT [FK_Industry_AviraUser1]
GO
ALTER TABLE [dbo].[Industry]  WITH CHECK ADD  CONSTRAINT [FK_Industry_ParentId] FOREIGN KEY([ParentId])
REFERENCES [dbo].[Industry] ([Id])
GO
ALTER TABLE [dbo].[Industry] CHECK CONSTRAINT [FK_Industry_ParentId]
GO