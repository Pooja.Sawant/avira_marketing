﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public class CompanyAnalystViewQueries
    {
        public static readonly string StoreProcInsert = "SApp_insertUpdateCompanyAnalystView";

        public static string StoreProcSelectById = "SApp_GetCompanyAnalystViewCompanyID";
    }
}
