﻿
CREATE PROCEDURE [dbo].[SApp_insertUpdateProject]
(@ProjectID		UNIQUEIDENTIFIER,
@ProjectCode	NVARCHAR(50),
@ProjectName	NVARCHAR(256),
@Remark			NVARCHAR(256),
@StartDate		DATETIME,
@EndDate		DATETIME,
@MinTrends		INT,
@MinCompanyProfiles INT,
@MinPrimaries		INT,
@ProjectStatusID	UNIQUEIDENTIFIER,
@IsDeleted		BIT,
@AviraUserId UNIQUEIDENTIFIER
)
as
/*================================================================================
Procedure Name: SApp_insertUpdateProject
Author: Sharath
Create date: 04/11/2019
Description: Insert or updare a record in Project table by ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
04/11/2019	Sharath			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ErrorMsg NVARCHAR(2048);

IF exists(select 1 from [dbo].[Project] 
		 where ((@ProjectID is null or [Id] !=  @ProjectID)
		 and  ProjectName = @ProjectName AND IsDeleted = 0))
begin
set @ErrorMsg = 'Project with Name "'+ @ProjectName + '" already exists.';
THROW 50000,@ErrorMsg,1;
end
IF exists(select 1 from [dbo].[Project] 
		 where ((@ProjectID is null or [Id] !=  @ProjectID)
		 and  ProjectName = @ProjectCode AND IsDeleted = 0))
begin
set @ErrorMsg = 'Project with Code "'+ @ProjectCode + '" already exists.';
THROW 50000,@ErrorMsg,1;
end


IF (@ProjectID is null)
begin
--Insert new record
	set @ProjectID = NEWID();

INSERT INTO [dbo].[Project]
           ([ID]
           ,[ProjectCode]
           ,[ProjectName]
           ,[Remark]
           ,[StartDate]
           ,[EndDate]
           ,[MinTrends]
           ,[MinCompanyProfiles]
           ,[MinPrimaries]
           ,[Completion]
           ,[Efficiency]
           ,[Approved]
           ,[ProjectStatusID]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
	values(@ProjectID, 
			@ProjectCode,
			@ProjectName, 
			@Remark, 
			@StartDate,
			@EndDate,
			@MinTrends,
			@MinCompanyProfiles,
			@MinPrimaries,
			0,--as Completion
			0,--as [Efficiency]
			0,--as [Approved]
			@ProjectStatusID,
			GETDATE(), --as [CreatedOn]
			@AviraUserId, 
			0); --as [IsDeleted]
end
else if @IsDeleted = 1
begin
UPDATE [dbo].[Project]
set [IsDeleted] = @IsDeleted,
      [DeletedOn] = GETDATE(),
      [UserDeletedById] = @AviraUserId
     where ID = @ProjectID
	 and IsDeleted = 0;
end
else
begin
--update record
UPDATE [dbo].[Project]
   SET [ProjectCode] = @ProjectCode,
      [ProjectName] = @ProjectName,
      [Remark] = @Remark, 
      [StartDate] = @StartDate, 
      [EndDate] = @EndDate, 
      [MinTrends] = @MinTrends,
      [MinCompanyProfiles] = @MinCompanyProfiles,
      [MinPrimaries] = @MinPrimaries,
      [ProjectStatusID] = @ProjectStatusID, 
      [ModifiedOn] = GETDATE(),
      [UserModifiedById] = @AviraUserId
     where ID = @ProjectID
	 and IsDeleted = 0;
end

select @ProjectID;
end