﻿CREATE procedure [dbo].[SApp_GetAllProjects]

as
/*================================================================================
Procedure Name: [SApp_GetAllProjects]
Author: Sai Krishna
Create date: 11/05/2019
Description: Get list from Project
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
11/05/2019	Sai Krishna		Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT  id,[Id] as ProjectId, 
		[ProjectName]
	FROM [dbo].[Project]
where ([Project].[IsDeleted] = 0) ORDER BY ProjectName 
end