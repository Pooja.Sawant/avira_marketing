﻿CREATE TABLE [dbo].[StagingCompanyBoardOfDirectors] (
    [Id]                                     UNIQUEIDENTIFIER NOT NULL,
    [ImportId]                               UNIQUEIDENTIFIER NULL,
    [BoardofDirectorName]                    NVARCHAR (255)   NULL,
    [BoardofDirectorEmail]                   NVARCHAR (255)   NULL,
    [BoardofDirectorOtherCompany]            NVARCHAR (255)   NULL,
    [BoardofDirectorOtherCompanyDesignation] NVARCHAR (255)   NULL,
    [CompanyId]                              UNIQUEIDENTIFIER NULL,
    [ErrorNotes]                             NVARCHAR (1000)  NULL,
    [RowNo]                                  INT              NULL,
    CONSTRAINT [PK_StagingCompanyBoardOfDirectors] PRIMARY KEY CLUSTERED ([Id] ASC)
);



