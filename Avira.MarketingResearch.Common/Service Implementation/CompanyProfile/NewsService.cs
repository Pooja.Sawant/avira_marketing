﻿using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.CompanyProfile
{
    public class NewsService : INewsService
    {
        private readonly INewsRepository _newsRepository;
        private readonly ICompanyNoteRepository _iCompanyNoteRepository;

        public NewsService(INewsRepository newsRepository, ICompanyNoteRepository iCompanyNoteRepository)
        {
            _newsRepository = newsRepository;
            _iCompanyNoteRepository = iCompanyNoteRepository;
        }

        public IEnumerable<NewsViewModel> GetAll(Guid companyId,Guid projectId)
        {
            return _newsRepository.GetAll(companyId, projectId);
        }

        public IEnumerable<NewsImpactDirectionViewModel> GetAllImpactDirection()
        {
            return _newsRepository.GetAllImpactDirection();
        }

        public IEnumerable<NewsImportanceViewModel> GetAllImportance()
        {
            return _newsRepository.GetAllImportance();
        }

        public IEnumerable<NewsCategoryViewModel> GetAllNewsCategory()
        {
            return _newsRepository.GetAllNewsCategory();
        }
        public Result<SpTransactionMessage> InsertUpdate(MainNewsInsertUpdateDbViewModel tfc, AviraUser currenUser)
        {

            var res = _newsRepository.InsertUpdate(tfc);
            tfc.CompanyNoteModel.Id = Guid.NewGuid();
            tfc.CompanyNoteModel.UserCreatedById = tfc.UserCreatedById;
            tfc.CompanyNoteModel.CompanyId = tfc.CompanyId;
            _iCompanyNoteRepository.Insert(tfc.CompanyNoteModel);

            return res;
        }
    }
}
