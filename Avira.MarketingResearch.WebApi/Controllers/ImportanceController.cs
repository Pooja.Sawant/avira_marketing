﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImportanceController : AviraBaseController
    {
        public readonly IImportanceService _iImportanceService;
        private ILoggerManager _logger;
        public ImportanceController(
            IImportanceService iImportanceService, ILoggerManager logger)
        {
            _iImportanceService = iImportanceService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ImportanceViewModel>> Get()
        {
            List<ImportanceViewModel> res = null;
            try
            {
                res = _iImportanceService.GetAll().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }
    }
}