﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public class TrendNoteQueries
    {
        public static readonly string StoreProcInsert = "SApp_InsertTrendNotes";

        public static string StoreProcSelectAll = "SApp_GetTrendNoteTrendID";

        public static string StoreProcSelectAllStatus = "SApp_GetTrendAllTabsStatusbyTrendID";
    }
}
