﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public class CompanySwotQueries
    {
        public static readonly string StoreProcSelectAll = "SApp_GetCompanySWOTCompanyID";
        public static readonly string StoreProcSelectOpportunities = "SApp_GetCompanySWOTOpportunitiesCompanyID";
        public static readonly string StoreProcSelectStrenght = "SApp_GetCompanySWOTStrenghtCompanyID";
        public static readonly string StoreProcSelectThreats = "SApp_GetCompanySWOTThreatsCompanyID";
        public static readonly string StoreProcSelectWeeknesses = "SApp_GetCompanySWOTWeeknessesCompanyID";
        public static readonly string StoreProcInsert = "SApp_InsertUpdateCompantSWOTMap";
    }
}
