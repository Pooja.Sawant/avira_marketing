﻿CREATE TYPE [dbo].[udtGUIDVarcharValue] AS TABLE (
    [ID]    UNIQUEIDENTIFIER NOT NULL,
    [Value] NVARCHAR (100)   NOT NULL);

