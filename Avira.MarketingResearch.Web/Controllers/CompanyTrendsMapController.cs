﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.Utils;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Avira.MarketingResearch.Web.Controllers
{
    [Authorize]
    [NoCache]
    public class CompanyTrendsMapController : BaseController
    {
        public readonly IServiceRepository _serviceRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;
        string contentRootPath = string.Empty;
        private ILoggerManager _logger;

        public CompanyTrendsMapController(IServiceRepository serviceRepository, ILoggerManager logger, IHostingEnvironment hostingEnvironment, IConfiguration configuration)
        {
            _hostingEnvironment = hostingEnvironment;
            _serviceRepository = serviceRepository;
            _logger = logger;
            _configuration = configuration;
            contentRootPath = _hostingEnvironment.ContentRootPath;
        }

        public IActionResult Index(string companyId, string projectId)
        {
            SetRoleList();
            BindDropdowns();
            ViewBag.Message = string.Empty;
            MainCompanyTrendsMapInsertUpdateDbViewModel mainCompanyTrendsMapInsertUpdateDbViewModel = new MainCompanyTrendsMapInsertUpdateDbViewModel();
            try
            {
                mainCompanyTrendsMapInsertUpdateDbViewModel.CompanyId = new Guid(companyId);
                mainCompanyTrendsMapInsertUpdateDbViewModel.ProjectId = new Guid(projectId);
                var ContollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage CompanyTrendsMapResponse = _serviceRepository.GetByIdResponse(ContollerName, companyId);
                List<CompanyTrendsMapViewModel> companyTrendsMapViewModel = CompanyTrendsMapResponse.Content.ReadAsAsync<List<CompanyTrendsMapViewModel>>().Result;
                if (companyTrendsMapViewModel.Count == 0)
                {
                    CompanyTrendsMapViewModel objempty = new CompanyTrendsMapViewModel();
                    companyTrendsMapViewModel.Add(objempty);
                }
                mainCompanyTrendsMapInsertUpdateDbViewModel.CompanyTrendsMapInsertUpdate = companyTrendsMapViewModel;
                HttpResponseMessage CompanyNoteResponse = _serviceRepository.GetCompanyNoteByIdResponse("CompanyNote", companyId, "CompanyTrends", null);
                CompanyNoteViewModel companyNoteViewModel = CompanyNoteResponse.Content.ReadAsAsync<CompanyNoteViewModel>().Result;
                mainCompanyTrendsMapInsertUpdateDbViewModel.CompanyNoteModel = companyNoteViewModel ?? new CompanyNoteViewModel();
                HttpResponseMessage CompanyResponse = _serviceRepository.GetByIdResponse("Company", companyId);
                CompanyViewModel companyViewModel = CompanyResponse.Content.ReadAsAsync<CompanyViewModel>().Result;

                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, mainCompanyTrendsMapInsertUpdateDbViewModel.ProjectId);
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                    ViewBag.StatusName = mainCompanyTrendsMapInsertUpdateDbViewModel.CompanyNoteModel.StatusName;
                }
                ViewBag.CompanyId = mainCompanyTrendsMapInsertUpdateDbViewModel.CompanyId;
                ViewBag.CompanyName = companyViewModel.CompanyName;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(mainCompanyTrendsMapInsertUpdateDbViewModel);
        }

        private void BindDropdowns()
        {
            try
            {
                HttpResponseMessage responseImpactType = _serviceRepository.GetResponse("ImpactDirection");
                responseImpactType.EnsureSuccessStatusCode();
                ViewBag.ImpactList = responseImpactType.Content.ReadAsAsync<List<ImpactDirectionViewModel>>().Result;

                HttpResponseMessage responseImportance = _serviceRepository.GetResponse("Importance");
                responseImportance.EnsureSuccessStatusCode();
                ViewBag.ImportanceList = responseImportance.Content.ReadAsAsync<List<ImportanceViewModel>>().Result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        private bool validate(MainCompanyTrendsMapInsertUpdateDbViewModel model)
        {
            int i = 0;
            int count = model.CompanyTrendsMapInsertUpdate.Count;
            int flag = 0;  
            do
            {
                if (model.CompanyTrendsMapInsertUpdate[i].TrendName == null)
                {
                    ViewBag.Message = "Please enter Trends for the Company mapping.";
                    flag++;
                    break;
                }
                if (model.CompanyTrendsMapInsertUpdate[i].Department == null)
                {
                    ViewBag.Message = "Please enter department for the Company mapping.";
                    flag++;
                    break;
                }
                if (model.CompanyTrendsMapInsertUpdate[i].ImpactId == Guid.Empty)
                {
                    ViewBag.Message = "Please select impact for the company mapping";
                    flag++;
                    break;
                }
                if (model.CompanyTrendsMapInsertUpdate[i].ImportanceId == Guid.Empty)
                {
                    ViewBag.Message = "Please select important for the company mapping";
                    flag++;
                    break;
                }
                if (model.CompanyNoteModel.AuthorRemark == string.Empty || model.CompanyNoteModel.AuthorRemark == null)
                {
                    ViewBag.Message = "Please enter Author notes!!";
                    return false;
                }
                i++; 
                if(i > count - 1)
                   break;

            } while (true); 
            if (flag > 0)
                return false;
            else
                return true;
        }

        private void SetRoleList()
        {
            try
            {
                var roles = HttpContext.Session.GetString("userRoles");
                var userRoles = JsonConvert.DeserializeObject<List<RoleViewModel>>(roles);
                List<string> RoleListItems = new List<string>();
                foreach (var item in userRoles)
                    RoleListItems.Add(item.UserRoleName);
                ViewBag.UserRoleList = RoleListItems;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult Index(MainCompanyTrendsMapInsertUpdateDbViewModel model, string actionName)
        {
            SetRoleList();
            ViewBag.Message = string.Empty;
            model.CompanyNoteModel.CompanyType = "CompanyTrends";
            bool bResult = false;
            try
            { 
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                if (actionName.Equals("Save"))
                {  
                    bResult = validate(model);
                    if (bResult)
                    {
                        model.CompanyNoteModel.CompanyStatusName = "Draft";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            return RedirectToAction("Index", new { companyId = model.CompanyId.ToString(), projectId = model.ProjectId.ToString() });
                        }
                    }
                }
                else if (actionName.Equals("Submitted"))
                {  
                    bResult = validate(model);
                    if (bResult)
                    {
                        model.CompanyNoteModel.CompanyStatusName = "Submitted";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            return RedirectToAction("Index", new { companyId = model.CompanyId.ToString(), projectId = model.ProjectId.ToString() });
                        }
                    }

                }
                else if (actionName.Equals("Reject"))
                {
                    bResult = validate(model);
                    if (bResult)
                    {
                        model.CompanyNoteModel.CompanyStatusName = "Rejected";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            return RedirectToAction("Index", new { companyId = model.CompanyId.ToString(), projectId = model.ProjectId.ToString() });
                        }
                    }
                }
                else if (actionName.Equals("Approve"))
                {
                    bResult = validate(model);
                    if (bResult)
                    {
                        model.CompanyNoteModel.CompanyStatusName = "Approved";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            return RedirectToAction("Index", new { companyId = model.CompanyId.ToString(), projectId = model.ProjectId.ToString() });
                        }
                    }
                }
                else if (actionName.Equals("Next"))
                    return RedirectToAction("Index", "CompanyOtherInfo", new { companyId = model.CompanyId.ToString(), projectId = model.ProjectId.ToString() });
                else if (actionName.Equals("Previous"))
                    return RedirectToAction("Index", "CompanySWOT", new { companyId = model.CompanyId.ToString(), projectId = model.ProjectId.ToString() });
                else if (actionName.Equals("Cancel"))
                    return RedirectToAction("Index", "#", new { companyId = model.CompanyId.ToString(), projectId = model.ProjectId.ToString() });
                else if (actionName.Equals("AddRow"))
                {
                    CompanyTrendsMapViewModel objempty = new CompanyTrendsMapViewModel();
                    model.CompanyTrendsMapInsertUpdate.Add(objempty);
                    model.CompanyNoteModel = model.CompanyNoteModel ?? new CompanyNoteViewModel();
                }

                BindDropdowns();
                HttpResponseMessage CompanyNoteResponse = _serviceRepository.GetCompanyNoteByIdResponse("CompanyNote", model.CompanyId.ToString(), "CompanyTrends", null);
                CompanyNoteViewModel companyNoteViewModel = CompanyNoteResponse.Content.ReadAsAsync<CompanyNoteViewModel>().Result;
                companyNoteViewModel = companyNoteViewModel ?? new CompanyNoteViewModel();
                HttpResponseMessage CompanyResponse = _serviceRepository.GetByIdResponse("Company", model.CompanyId.ToString());
                CompanyViewModel companyViewModel = CompanyResponse.Content.ReadAsAsync<CompanyViewModel>().Result;

                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, model.ProjectId);
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                    ViewBag.StatusName = companyNoteViewModel.StatusName ?? string.Empty;
                }
                ViewBag.CompanyName = companyViewModel.CompanyName;
                ViewBag.CompanyId = model.CompanyId;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(model);
        }

    }
}