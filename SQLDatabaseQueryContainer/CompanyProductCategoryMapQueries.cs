﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public class CompanyProductCategoryMapQueries
    {
        public static readonly string StoreProcInsertUpdate = "SApp_InsertUpdateCompanyProductCategoryMap";

        public static string StoreProcSelectAll = "SApp_GetProductCategoryByCompanyProductID";
    }
}
