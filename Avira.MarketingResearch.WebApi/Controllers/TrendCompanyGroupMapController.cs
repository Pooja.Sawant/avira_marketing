﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;


namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrendCompanyGroupMapController : AviraBaseController
    {
        private readonly ITrendCompanyGroupMapService _trendCompanyGroupMapService;
        private readonly ITrendCompanyGroupMapRepository _trendCompanyGroupMapRepository;
        private ILoggerManager _logger;
        public TrendCompanyGroupMapController(
            ITrendCompanyGroupMapService trendCompanyGroupMapService,
            ITrendCompanyGroupMapRepository trendCompanyGroupMapRepository,
             ILoggerManager logger)
        {
            _trendCompanyGroupMapRepository = trendCompanyGroupMapRepository;
            _trendCompanyGroupMapService = trendCompanyGroupMapService;
            _logger = logger;

        }

        [HttpGet]
        public ActionResult<IEnumerable<TrendCompanyGroupMapViewModel>> Get(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search = "", bool isDeletedInclude = false, string id = "")
        {
            List<TrendCompanyGroupMapViewModel> res = null;
            try
            {
                res = _trendCompanyGroupMapService.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude, id).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpPost]
        public ActionResult Post([FromBody] MainTrendCompanyGroupMapInsertUpdateDbViewModel viewModel)
        {
            viewModel.UserCreatedById = CurrentUser.Id;
            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new Trend Company Group Map, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                var result = _trendCompanyGroupMapService.InsertUpdate(viewModel, CurrentUser);
                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Trend Company Group Map is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }
    }
}