﻿CREATE TABLE [dbo].[BaseDecisionBusinessRole] (
    [BaseDecisionId] UNIQUEIDENTIFIER NOT NULL,
    [BusinessRoleId] UNIQUEIDENTIFIER NULL,
    FOREIGN KEY ([BusinessRoleId]) REFERENCES [dbo].[LookupCategory] ([Id]),
    CONSTRAINT [FK__BaseDecis__BaseD__609ED11C] FOREIGN KEY ([BaseDecisionId]) REFERENCES [dbo].[BaseDecision] ([Id])
);

