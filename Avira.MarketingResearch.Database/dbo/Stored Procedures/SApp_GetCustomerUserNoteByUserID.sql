﻿CREATE procedure [dbo].[SApp_GetCustomerUserNoteByUserID]    
(@UserId uniqueidentifier,
@ProjectId uniqueidentifier=null,
@Module NVARCHAR(512))    
as    
/*================================================================================    
Procedure Name: SApp_GetCustomerUserNoteByUserID    
Author: Sharath    
Create date: 07/12/2019    
Description: Get Customer note for a project and module by User id    
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
07/12/2019 Sharath   Initial Version    
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    

SELECT CNote.[Id],
		CNote.[UserId],
		au.UserName,
		AU.CustomerId,
		Cu.CustomerName,
		CNote.[ProjectId],
		p.ProjectName,
		CNote.[Module],
		CNote.[CompanyId],
		c.CompanyName,
		CNote.[MarketID],
		m.MarketName,
		CNote.[TrendID],
		t.TrendName,
		CNote.[Notes],
		CNote.[IsDeleted]
FROM [dbo].[CustomerUserNotes] CNote
inner join Project P
on CNote.ProjectId = p.ID
inner join AviraUser AU
on CNote.UserId = Au.Id
left join Customer Cu
on AU.CustomerId = cu.Id
left join Company c
on CNote.CompanyId = c.Id
left join Market M
on CNote.MarketID = M.Id
left join Trend T
on CNote.TrendID = T.Id
where  UserId = @UserId 
AND (@ProjectId is null or CNote.ProjectId = @ProjectId)
and (@Module = '' or @Module is null or CNote.Module = @Module)

END