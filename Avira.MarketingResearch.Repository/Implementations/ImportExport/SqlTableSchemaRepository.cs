﻿using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.ImportExport
{
    public class SqlTableSchemaRepository : ISqlTableSchemaRepository
    {
        protected IDbHelper _dbHelper;
        public SqlTableSchemaRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }
        public DataTable GetTableSchema(SchemaModel model)
        {
            SqlConnection conn = new SqlConnection(_dbHelper.GetConnectionString(_dbHelper.DatabaseAviraDB));
            conn.Open();
            DataTable dtSchema = conn.GetSchema("columns",new[] { "AviraDev", null, "Segment" });
            conn.Close();
            return dtSchema;
        }
    }
}
