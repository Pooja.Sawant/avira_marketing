﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.Helpers;
using System.Collections.Generic;
using System;
using System.Data;
using Avira.MarketingResearch.Model.ViewModels.Masters;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.Masters
{
    public interface ICompanyGroupRepository
    {
        CompanyGroupViewModel GetById(Guid guid);
        IEnumerable<CompanyGroupViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted);
        Result<SpTransactionMessage> Create(CompanyGroupInsertDbViewModel viewModel);
        Result<SpTransactionMessage> Update(CompanyGroupUpdateDbViewModel viewModel);
    }
}
