﻿using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Avira.MarketingResearch.Web.ImportExportUtility
{
    public class ImportStrategyInstance : IExcelImport
    {
        public readonly IServiceRepository _serviceRepository;
        private ILoggerManager _logger;
        public ImportStrategyInstance(IServiceRepository serviceRepository, ILoggerManager logger)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
        }

        public ImportResponse Import(IFormFile formFile)
        {
            var res = new ImportResponse();
            var inputModel = new StrategyIntanceImportModel();
            StringBuilder sb;
            using (var stream = new MemoryStream())
            {
                formFile.CopyTo(stream);

                using (var package = new ExcelPackage(stream))
                {
                    try
                    {
                        if (package.Workbook.Worksheets["Corporate Strategy"] != null)
                        {
                            inputModel.StrategyList = GetCorporateStrategy(package.Workbook.Worksheets["Corporate Strategy"]);
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'Corporate Strategy' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'Corporate Strategy' worksheet is missing.");
                            return res;
                        }
                    }
                    catch (Exception ex)
                    {
                        res.ResponseStatus = "Error";
                        res.ResponseMessage = "Error while reading data. Error Message: " + ex.Message + ". Please check input excel. More details logged in error log.";
                        res.TotalFailedRecords = 1;
                        _logger.LogError(ex.Message);
                        return res;
                    }
                }
            }

            //--------Call api to post data            
            inputModel.TemplateName = "Strategy Instance Import Template";
            inputModel.ImportFileName = formFile.FileName;

            try
            {

                HttpResponseMessage response = _serviceRepository.PostResponse("StrategyInstanceImportExport", inputModel);
                response.EnsureSuccessStatusCode();
                ImportCommonResponseMainModel viewModel = response.Content.ReadAsAsync<ImportCommonResponseMainModel>().Result;

                res.ResponseStatus = viewModel.IsSuccess ? "Success" : "Failed";
                res.ResponseMessage = viewModel.Message;
                res.TotalRecords = inputModel.StrategyList.Count();
                if (viewModel.ResponseModel == null)
                {
                    res.ResponseStatus = "Success";
                    res.TotalSuccessRecords = 1;
                    res.TotalFailedRecords = 0;
                }
                else
                {
                    //res.ResponseMessage = "Please check downloaded file for inserted record";
                    //res.TotalSuccessRecords = ((inputModel.NewsList == null ? 0 : inputModel.NewsList.Count())) - (viewModel.NewsList.Count());
                    res.TotalFailedRecords = viewModel.ResponseModel.Count();
                }
                res.ResponseModel = viewModel.ResponseModel;
            }
            catch (Exception ex)
            {
                res.ResponseStatus = "Error";
                res.ResponseMessage = "Error while reading data. Error Message: " + ex.Message + ". Please check input excel. More details logged in error log.";
                res.TotalFailedRecords = 1;
                _logger.LogError(ex.Message);
            }

            return res;
        }

        private IEnumerable<StrategyIntanceModel> GetCorporateStrategy(ExcelWorksheet worksheet)
        {
            List<StrategyIntanceModel> StrategyInstanceList = new List<StrategyIntanceModel>();

            var rowCount = worksheet.Dimension.Rows;

            int colIndexMarket_Name = GetColIndex(worksheet, "Market_Name");
            int colIndexDate = GetColIndex(worksheet, "Date");
            int colIndexCompany_Name = GetColIndex(worksheet, "Company_Name");
            int colIndexCompany_Segment = GetColIndex(worksheet, "Company_Segment");
            int colIndexStrategy_Type = GetColIndex(worksheet, "Strategy_Type");
            int colIndexStrategyImpact = GetColIndex(worksheet, "StrategyImpact");
            int colIndexStrategyImportance = GetColIndex(worksheet, "StrategyImportance");
            int colIndexStrategyDescription = GetColIndex(worksheet, "StrayegyDescription");
            for (int row = 7; row <= rowCount; row++)
            {
                StrategyIntanceModel StrategyModel = null;
                var cn = worksheet.Cells[row, 3].Value?.ToString().Trim();

                if (!string.IsNullOrEmpty(cn))
                {
                    StrategyModel = new StrategyIntanceModel
                    {
                        RowNo = row,
                        MarketName = worksheet.Cells[row, colIndexMarket_Name].Value?.ToString().Trim(),
                        Date = Convert.ToDateTime(worksheet.Cells[row, colIndexDate].Value?.ToString().Trim()),
                        CompanyName = worksheet.Cells[row, colIndexCompany_Name].Value?.ToString().Trim(),
                        SegmentName = worksheet.Cells[row, colIndexCompany_Segment].Value?.ToString().Trim(),
                        StrategyType = worksheet.Cells[row, colIndexStrategy_Type].Value?.ToString().Trim(),
                        Impact = worksheet.Cells[row, colIndexStrategyImpact].Value?.ToString().Trim(),
                        Importance = worksheet.Cells[row, colIndexStrategyImportance].Value?.ToString().Trim(),
                        StrategyDescription = worksheet.Cells[row, colIndexStrategyDescription].Value?.ToString().Trim(),
                    };
                    StrategyInstanceList.Add(StrategyModel);
                }
            }
            return StrategyInstanceList;

        }
        private int GetColIndex(ExcelWorksheet worksheet, string colName)
        {
            int colIndex = worksheet.Cells["6:6"].First(c => c.Value.ToString() == colName).Start.Column;

            return colIndex;
        }

    }
}
