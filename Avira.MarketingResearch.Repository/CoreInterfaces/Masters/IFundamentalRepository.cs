﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.Masters
{
    public interface IFundamentalRepository
    {
        FundamentalViewModel GetById(Guid guid);
        IEnumerable<FundamentalViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted);
        Result<SpTransactionMessage> Create(FundamentalInsertDbViewModel viewModel);
        Result<SpTransactionMessage> Update(FundamentalUpdateDbViewModel viewModel);
       
    }
}
