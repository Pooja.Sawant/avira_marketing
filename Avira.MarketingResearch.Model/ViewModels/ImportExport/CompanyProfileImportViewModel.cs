﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.ImportExport
{
    public class CompanyProfileImportViewModel
    {
        public string CompanyName { get; set; }
        public string Nature { get; set; }
        public string CompanySubsdiaryName { get; set; }
        public string CompanySubsdiaryLocation { get; set; }
        public string CompanyParentName { get; set; }
        public string HQ { get; set; }
        public string PhoneNumber1 { get; set; }
        public string PhoneNumber2 { get; set; }
        public string PhoneNumber3 { get; set; }
        public string Mail1 { get; set; }
        public string Mail2 { get; set; }
        public string Mail3 { get; set; }
        public string Website { get; set; }
        public string FoundedYear { get; set; }
        public string CompanyStage { get; set; }
        public string Geographies { get; set; }
        public string NumberofEmployees { get; set; }
        public string AsonDate { get; set; }
        public string KeyEmployeeName { get; set; }
        public string KeyEmplyeeDesignation { get; set; }
        public string KeyEmployeeEmail { get; set; }
        public string KeyEmployeeComments { get; set; }
        public string BoardofDirectorName { get; set; }
        public string BoardofDirectorEmail { get; set; }
        public string BoardofDirectorOtherCompany { get; set; }
        public string BoardofDirectorOtherCompanyDesignation { get; set; }
        public string CompanyRank_Rank { get; set; }
        public string CompanyRank_Rationale { get; set; }
        public string Name { get; set; }
        public string Entity { get; set; }
        public string Holding { get; set; }
        public string Level { get; set; }
        public string Designation { get; set; }
        public string Product_ServiceName { get; set; }
        public string LaunchDate { get; set; }
        public string ProductCategory { get; set; }
        public string Segment { get; set; }
        public string Revenue { get; set; }
        public string Units { get; set; }
        public string Currency { get; set; }
        public string TargetedGeogrpahy { get; set; }
        public string Patents { get; set; }
        public string Decription_Remarks { get; set; }
        public string Status { get; set; }
        public string Image { get; set; }
        public string Currency1 { get; set; }
        public string Unit { get; set; }
        public string TableType { get; set; }
        public string TotalRevenue { get; set; }
        public string Revenue_2018 { get; set; }
        public string Revenue_2017 { get; set; }
        public string Revenue_2016 { get; set; }
        public string Revenue_2015 { get; set; }
        public string Revenue_2014 { get; set; }
        public string Profit { get; set; }
        public string Profit_2018 { get; set; }
        public string Profit_2017 { get; set; }
        public string Profit_2016 { get; set; }
        public string Profit_2015 { get; set; }
        public string Profit_2014 { get; set; }
        public string ExpenseUnit { get; set; }
        public string ExpenseCurency { get; set; }
        public string OtherFinancialTableType { get; set; }
        public string TotalExpense { get; set; }
        public string Expence_2018 { get; set; }
        public string Expence_2017 { get; set; }
        public string Expence_2016 { get; set; }
        public string Expence_2015 { get; set; }
        public string Expence_2014 { get; set; }
        public string Currency11 { get; set; }
        public string Period { get; set; }
        public string Base_Date { get; set; }
        public string Date1 { get; set; }
        public string Price { get; set; }
        public string Volume { get; set; }
        public string Date2 { get; set; }
        public string Organic_Inorganic { get; set; }
        public string Strategy { get; set; }
        public string Category { get; set; }
        public string ImportanceH_M_L { get; set; }
        public string ClientName { get; set; }
        public string ClientIndustry { get; set; }
        public string Remarks { get; set; }
        public string ImportDate { get; set; }
        public string News { get; set; }
        public string NewsCategory { get; set; }
        public string ImportanceH_M_L1 { get; set; }
        public string Impact_positive_Negative { get; set; }
        public string OtherParticipants { get; set; }
        public string Remarks1 { get; set; }
        public string ESMPresence { get; set; }
        public string SectorPresence { get; set; }
        public string KeyCompetitor { get; set; }
        public string Strenght { get; set; }
        public string Weakness { get; set; }
        public string Opportunity { get; set; }
        public string Threats { get; set; }
        public string Trends { get; set; }
        public string Department { get; set; }
        public string Importance { get; set; }
        public string Impact { get; set; }
        public string AddVendorInfoTable { get; set; }
        public string AddLitigationsAndOtherLegalInfo { get; set; }
        public string AddPatentRelatedInfo { get; set; }
        public string AddAnyotherinfo { get; set; }
        public string AddTable { get; set; }
        public string AnalystViewText { get; set; }
        public string ErrorNotes { get; set; }
        public string ProjectName { get; set; }


    }
    public class CompanyProfileImportInsertViewModel
    {
        public string TemplateName { get; set; }
        public string ImportFileName { get; set; }
        public string JsonFundamentals { get; set; }
        public string JsonShareHolding { get; set; }
        public string JsonSubsidiaries { get; set; }
        public string JsonKeyEmployees { get; set; }
        public string JsonBoardOfDirectors { get; set; }
        public string JsonProduct { get; set; }
        public string JsonStrategy { get; set; }
        public string JsonClients { get; set; }
        public string JsonNews { get; set; }
        public string JsonSWOT { get; set; }
        public string JsonEcosystemPresence { get; set; }
        public string JsonTrends { get; set; }
        public string JsonAnalystView { get; set; }
        public string JsonPriceVolume { get; set; }
        public string JsonESMSegment { get; set; }
        public Guid UserCreatedById { get; set; }
        public List<CompanyProfileImportViewModel> CompanyProfileImportViewModel { get; set; }

        public List<CompanyProfileFundamentalsImportModel> CompanyProfileFundamentalsImportModel { get; set; }
        public List<CompanyProfileShareHoldingImportModel> CompanyProfileShareHoldingImportModel { get; set; }
        public List<CompanyProfileSubsidiariesImportModel> CompanyProfileSubsidiariesImportModel { get; set; }
        public List<CompanyKeyEmployeesImportModel> CompanyKeyEmployeesImportModel { get; set; }
        public List<CompanyBoardOfDirectorsImportModel> CompanyBoardOfDirectorsImportModel { get; set; }
        public List<CompanyProductImportModel> CompanyProductImportModel { get; set; }
        public List<CompanyStrategyImportModel> CompanyStrategyImportModel { get; set; }
        public List<CompanyClientsImportModel> CompanyClientsImportModel { get; set; }
        public List<CompanyNewsImportModel> CompanyNewsImportModel { get; set; }
        public List<CompanyEcosystemPresenceImportModel> CompanyEcosystemPresenceImportModel { get; set; }
        public List<CompanySWOTImportModel> CompanySWOTImportModel { get; set; }
        public List<CompanyTrendsImportModel> CompanyTrendsImportModel { get; set; }
        public List<CompanyAnalystViewImportModel> CompanyAnalystViewImportModel { get; set; }
        public List<PriceVolumeViewImportModel> PriceVolumeViewImportModel { get; set; }
        public List<CompanyProfilesESMSegmentImportModel> CompanyProfilesESMSegmentListImportModel { get; set; }
    }

    public class ExcelTemplate
    {
        public string ColumnName { get; set; }
        public int ColumnIndex { get; set; }
    }

    public class CompanyProflieFundamentalsTemplate
    {

    }

    //public class CompanyProfileImportResponseModel
    //{
    //    public string Module { get; set; }
    //    public string ErrorNotes { get; set; }
    //    public string Success { get; set; }
       
    //}

    public class CompanyProfileFundamentalsImportModel
    {
        public string Id { get; set; }

        public string ImportId { get; set; }

        public int RowNo { get; set; }

        public string CompanyName { get; set; }

        public string Description { get; set; }

        public string Headquarter { get; set; }

        public string Foundedyear { get; set; }

        public string Nature { get; set; }

        public string CompanyStage { get; set; }

        public string NumberOfEmployee { get; set; }

        public DateTime? AsonDate { get; set; }

        public string Website { get; set; }

        public string CompanyRank_Rank { get; set; }

        public string CompanyRank_Rationale { get; set; }

        public string Currency1 { get; set; }

        public string PhoneNumber1 { get; set; }

        public string PhoneNumber2 { get; set; }

        public string PhoneNumber3 { get; set; }

        public string GeographicPresence { get; set; }

        public string CurrencyId { get; set; }

        public string ErrorNotes { get; set; }
    }

    public class CompanyProfileShareHoldingImportModel
    {
        public string Id { get; set; }

        public string ImportId { get; set; }

        public int RowNo { get; set; }

        public string Name { get; set; }

        public string Entity { get; set; }

        public string Holding { get; set; }

        public string EntityTypeId { get; set; }

        public string CompanyId { get; set; }

        public string ErrorNotes { get; set; }
    }

    public class CompanyProfileSubsidiariesImportModel
    {
        public string Id { get; set; }

        public string ImportId { get; set; }

        public int RowNo { get; set; }

        public string CompanySubsdiaryName { get; set; }

        public string CompanySubsdiaryLocation { get; set; }

        public string CompanyId { get; set; }

        public string ErrorNotes { get; set; }
    }

    public class CompanyKeyEmployeesImportModel
    {
        public Guid Id { get; set; }

        public Guid? ImportId { get; set; }

        public int RowNo { get; set; }

        public string KeyEmployeeName { get; set; }

        public string KeyEmployeeDesignation { get; set; }

        public string KeyEmployeeEmail { get; set; }

        public string ReportingManager { get; set; }

        public Guid? CompanyId { get; set; }

        public string ErrorNotes { get; set; }
    }

    public class CompanyBoardOfDirectorsImportModel
    {
        public Guid Id { get; set; }

        public Guid? ImportId { get; set; }

        public int RowNo { get; set; }

        public string BoardofDirectorName { get; set; }

        public string BoardofDirectorEmail { get; set; }

        public string BoardofDirectorOtherCompany { get; set; }

        public string BoardofDirectorOtherCompanyDesignation { get; set; }

        public Guid? CompanyId { get; set; }

        public string ErrorNotes { get; set; }

    }

    public class CompanyProductImportModel
    {
        public Guid Id { get; set; }

        public Guid? ImportId { get; set; }

        public int RowNo { get; set; }

        public string ProductServiceName { get; set; }

        public string Market { get; set; }

        public DateTime? LaunchDate { get; set; }

        public string Specification { get; set; }

        public string EndUserIndustriesTargeted { get; set; }

        public string ProductCategory { get; set; }

        public string Segment { get; set; }

        public decimal? Revenue { get; set; }

        public string Units { get; set; }

        public string Currency { get; set; }

        public string TargetedGeogrpahy { get; set; }

        public string Patents { get; set; }

        public string DecriptionRemarks { get; set; }

        public string Status { get; set; }

        public Guid? SegmentId { get; set; }

        public Guid? CompanyProductCategoryId { get; set; }

        public Guid? CompanyId { get; set; }

        public string ErrorNotes { get; set; }

    }

    public class CompanyStrategyImportModel
    {
        public Guid Id { get; set; }

        public Guid? ImportId { get; set; }

        public int RowNo { get; set; }

        public DateTime? Date { get; set; }

        public string OrganicInorganic { get; set; }

        public string Strategy { get; set; }

        public string Category { get; set; }

        public string Importance { get; set; }

        public string ImpactDirection { get; set; }

        public string Remarks { get; set; }

        public Guid? ApproachId { get; set; }

        public Guid? CategoryId { get; set; }

        public Guid? ImportanceId { get; set; }

        public Guid? ImpactId { get; set; }

        public Guid? CompanyId { get; set; }

        public string ErrorNotes { get; set; }

    }

    public class CompanyClientsImportModel
    {
        public Guid Id { get; set; }

        public Guid? ImportId { get; set; }

        public int RowNo { get; set; }

        public string ClientName { get; set; }

        public string ClientIndustry { get; set; }

        public string Remarks { get; set; }

        public Guid? CompanyId { get; set; }

        public string ErrorNotes { get; set; }

    }

    public class CompanyNewsImportModel
    {
        public Guid Id { get; set; }

        public Guid? ImportId { get; set; }

        public int RowNo { get; set; }

        public DateTime? NewsDate { get; set; }

        public string News { get; set; }

        public string NewsCategory { get; set; }

        public string Importance { get; set; }

        public string Impact { get; set; }

        public string OtherParticipants { get; set; }

        public string Remarks { get; set; }

        public Guid? CompanyId { get; set; }

        public string ErrorNotes { get; set; }

    }

    public class CompanyEcosystemPresenceImportModel
    {
        public Guid Id { get; set; }

        public Guid? ImportId { get; set; }

        public int RowNo { get; set; }

        public string ESMPresence { get; set; }

        public string SectorPresence { get; set; }

        public string KeyCompetitor { get; set; }

        public Guid? CompanyId { get; set; }

        public string ErrorNotes { get; set; }

    }

    public class CompanyProfilesESMSegmentImportModel
    {
        public Guid Id { get; set; }

        public Guid? ImportId { get; set; }

        public int RowNo { get; set; }

        public Guid? CompanyId { get; set; }

        public string ESMSegmentName { get; set; }

        public string ESMSubSegmentName { get; set; }

        public string ErrorNotes { get; set; }
    }

    public class CompanySWOTImportModel
    {
        public Guid Id { get; set; }

        public Guid? ImportId { get; set; }

        public int RowNo { get; set; }

        public string AnalysisPoint { get; set; }

        public string Market { get; set; }

        public string Category { get; set; }

        public string SWOTType { get; set; }

        public Guid? CompanyId { get; set; }

        public Guid? ProjectId { get; set; }

        public string ErrorNotes { get; set; }

    }

    public class CompanyTrendsImportModel
    {
        public Guid Id { get; set; }

        public Guid? ImportId { get; set; }

        public int RowNo { get; set; }

        public string Trends { get; set; }

        public string Department { get; set; }

        public string Category { get; set; }

        public string Ecosystem { get; set; }

        public string Importance { get; set; }

        public string Impact { get; set; }

        public Guid? CompanyId { get; set; }

        public string ErrorNotes { get; set; }

    }

    public class CompanyAnalystViewImportModel
    {
        public Guid Id { get; set; }

        public Guid? ImportId { get; set; }

        public int RowNo { get; set; }

        public string Market { get; set; }

        public string AnalystView { get; set; }

        public DateTime? DateOfUpdate { get; set; }

        public string Category { get; set; }

        public string Importance { get; set; }

        public Guid? CompanyId { get; set; }

        public Guid? ProjectId { get; set; }

        public string ErrorNotes { get; set; }

    }

    public class PriceVolumeViewImportModel
    {
        public Guid Id { get; set; }
        public Guid? ImportId { get; set; }
        public int RowNo { get; set; }
        public DateTime? Date { get; set; }
        public decimal? Price { get; set; }
        public string Currency { get; set; }
        public string CurrencyUnit { get; set; }
        public decimal? Volume { get; set; }
        public string VolumeUnit { get; set; }
        public Guid? CompanyId { get; set; }
        public Guid? ProjectId { get; set; }
        public string ErrorNotes { get; set; }

    }

}
