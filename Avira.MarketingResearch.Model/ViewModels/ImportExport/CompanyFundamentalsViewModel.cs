﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.ImportExport
{
    public class CompanyFundamentalsViewModel
    {
        public string TemplateName { get; set; }
        public string ImportFileName { get; set; }
        public string JsonCompanyBasic { get; set; }
        public string JsonRevenue { get; set; }
        public string JsonCompanySegment { get; set; }
        public string JsonCompanyAnalystView { get; set; }
        public string JsonCompanyKeyword { get; set; }
        public Guid UserCreatedById { get; set; }
        public List<CompanyFundamentalsBasicInfoImportModel> CompanyBasicInfoList { get; set; }
        public List<CompanyFundamentalsSegmentImportModel> CompanySegmentList { get; set; }
        public List<CompanyFundamentalsAnalystViewImportModel> CompanyAnalystViewList { get; set; }
        public List<CompanyFundamentalsKeywordsImportModel>CompanyKeywordList { get; set; }
        public List<CompanyFundamentalsRevenueImportModel> CompanyRevenueList { get; set; }

    }
    public class CompanyFundamentalsBasicInfoImportModel
    {
        public int RowNo { get; set; }
        public string ProjectName { get; set; }
        public string CompanyName { get; set; }
        public string CompanyStage { get; set; }
        public string Headquarter { get; set; }
        public string Nature { get; set; }
        public string GeographicPresence { get; set; }
        public string Currency { get; set; }
        public string Units { get; set; }
        public string Year1 { get; set; }
        public string RevenueYear1 { get; set; }
        public string Year2 { get; set; }
        public string RevenueYear2 { get; set; }
    }
       
     public class CompanyFundamentalsRevenueImportModel
    {
        public int RowNo { get; set; }
        public string CompanyName { get; set; }
        public string Currency { get; set; }
        public string Units { get; set; }
        public string Year { get; set; }
        public string Revenue { get; set; }
    }

    public class CompanyFundamentalsSegmentImportModel
    {
        public int RowNo { get; set; }
        public string ProjectName { get; set; }
        public string CompanyName { get; set; }
        public string CompanySegment { get; set; }
        public string CompanySubsegment { get; set; }
    }

    public class CompanyFundamentalsKeywordsImportModel
    {
        public int RowNo { get; set; }
        public string ProjectName { get; set; }
        public string CompanyName { get; set; }
        public string Keywords { get; set; }
    }

    public class CompanyFundamentalsAnalystViewImportModel
    {
        public Guid Id { get; set; }
        public Guid? ImportId { get; set; }
        public int RowNo { get; set; }
        public string Company { get; set; }
        public string AnalystView { get; set; }
        public DateTime? DateOfUpdate { get; set; }
        public string Category { get; set; }
        public string Importance { get; set; }
        public Guid? CompanyId { get; set; }
        public Guid? ProjectId { get; set; }
        public string ErrorNotes { get; set; }
    }

}
