﻿
  
CREATE PROCEDURE [dbo].[SFab_UpdateDeleteMyMarketData]
(
	@CustomerId UNIQUEIDENTIFIER=null,
	@ProjectId UNIQUEIDENTIFIER=null,
	@TrendId UNIQUEIDENTIFIER=null,
	@MarketId UNIQUEIDENTIFIER = null
)  
AS  
/*================================================================================  
Procedure Name: [SFab_UpdateDeleteMyMarketData]  
Author: Swami Aluri  
Create date: 07/31/2019  
Description: Deleting and updating my market from 3 differenct tables  
Change History  
Date        Developer     Reason  
__________  ____________  ______________________________________________________  
07/31/2019   Swami Aluri   Initial Version  
================================================================================*/  

BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	DECLARE @DeletedRank INT 
	SELECT @DeletedRank = CustomMarketRank FROM MakeMyMarket WHERE Id = @MarketId

	--DELETE FROM MakeMyMarket WHERE Id = @MarketId
	UPDATE MakeMyMarket
	SET IsDeleted = 1
	WHERE Id = @MarketId

	UPDATE MakeMyMarketImpact
	SET IsDeleted = 1
	--DELETE FROM MakeMyMarketImpact 
	WHERE MakeMyMarketId = @MarketId
	AND ProjectId = @ProjectId
	AND CustomerId = @CustomerId
 
	UPDATE MakeMyMarketMyView
	SET IsDeleted = 1 
	WHERE MarketId = @MarketId
	AND CustomerId = @CustomerId
	AND ProjectId = @ProjectId


	UPDATE MakeMyMarket
	SET CustomMarketRank = CustomMarketRank - 1 
	WHERE 
	CustomMarketRank > @DeletedRank
	AND CustomerId = @CustomerId
	AND TrendId = @TrendId
	AND ProjectId = @ProjectId

END