﻿using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation
{
    public class CompanyService: ICompanyService
    {
        private readonly ICompanyRepository _companyRepository;

        public CompanyService(ICompanyRepository buildLevelRepository)
        {
            _companyRepository = buildLevelRepository;
        }

        public Result<SpTransactionMessage> Create(CompanyInsertDbViewModel tfc, AviraUser currentUser)
        {
            return _companyRepository.Create(tfc);
        }


        public IEnumerable<CompanyViewModel> GetAll(Guid Id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<CompanyViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false)
        {
            return _companyRepository.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName,Search, isDeletedInclude);
        }

        public CompanyViewModel GetById(Guid guid)
        {
            return _companyRepository.GetById(guid);
        }


        public Result<SpTransactionMessage> Update(CompanyUpdateDbViewModel tfc, AviraUser currenUser)
        {
            return _companyRepository.Update(tfc);
        }
    }
}
