﻿CREATE procedure [dbo].[SApp_GetRolebyID]
(@Id uniqueidentifier,
@includeDeleted bit = 0)
as
/*================================================================================
Procedure Name: SApp_GetRolebyID
Author: Gopi
Create date: 04/08/2019
Description: Get details from UserRole table by ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
04/08/2019	Gopi			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if @includeDeleted is null set @includeDeleted = 0

SELECT [Id], 
		[UserRoleName],
		[UserType],
		[IsDeleted]
FROM [dbo].[UserRole]
where (@Id is null or Id = @Id)
and (@includeDeleted = 1 or IsDeleted = 0)  
end