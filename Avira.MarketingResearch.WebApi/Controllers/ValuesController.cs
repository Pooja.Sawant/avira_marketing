﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.ImportExport;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        public readonly ISqlTableSchemaService _iSqlTableSchemaService;
        private ILoggerManager _logger;

        public ValuesController(ISqlTableSchemaService iSqlTableSchemaService, ILoggerManager logger)
        {
            _iSqlTableSchemaService = iSqlTableSchemaService;
            _logger = logger;
        }
        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            //DataTable dt = new DataTable();
            //dt = _iSqlTableSchemaService.GetTableSchema(new SchemaModel());
           return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
