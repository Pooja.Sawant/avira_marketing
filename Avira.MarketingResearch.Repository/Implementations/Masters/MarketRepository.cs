﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces.Masters;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Avira.MarketingResearch.Repository.Implementations.Masters
{
    public class MarketRepository : IMarketRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "Market";
        public MarketRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }


        public MarketViewModel GetById(Guid guid)
        {
            MarketViewModel marketViewModel = new MarketViewModel();
            marketViewModel = GetAllMarket(guid).FirstOrDefault();
            return marketViewModel;
        }

        public IEnumerable<MarketViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted)
        {
            return GetAllMarket(null, PageStart, pageSize, SortDirection, OrdbyByColumnName,Search, includeDeleted);
        }

        private IEnumerable<MarketViewModel> GetAllMarket(Guid? guid, int PageStart = 0, int pageSize = 0, int SortDirection = -1, string OrdbyByColumnName = "", string Search = "", bool includeDeleted = false)
        {           

            DataTable marketIdsTable = new DataTable();
            marketIdsTable.Columns.Add("ID", typeof(Guid));

            if (guid != null)
            {
                marketIdsTable.Rows.Add(guid);

            }


            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<MarketViewModel>(MarketQueries.StoreProcSelectAll
                            , new { @MarketIDList = marketIdsTable.AsTableValuedParameter("dbo.udtUniqueIdentifier"), @includeDeleted = includeDeleted, @PageStart = PageStart, @pageSize = pageSize, @SortDirection = SortDirection, @OrdbyByColumnName = OrdbyByColumnName, @MarketName = Search }
                            , null, true, null, CommandType.StoredProcedure);
        }
        public IEnumerable<MarketViewModel> GetAllParentMarket()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<MarketViewModel>(MarketQueries.StoreProcSelectAllParent
                            , null
                            , null, true, null, CommandType.StoredProcedure);
        }

        public Result<SpTransactionMessage> Create(MarketInsertDbViewModel marketInsertDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(MarketQueries.StoreProcInsert, marketInsertDbViewModel, CommandType.StoredProcedure);
            return spTransactionMessage;

        }

        public Result<SpTransactionMessage> Update(MarketUpdateDbViewModel marketUpdateDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(MarketQueries.StoreProcUpdate, marketUpdateDbViewModel, CommandType.StoredProcedure);
            return spTransactionMessage;
        }


    }
}
