﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.Trend
{
    public class TrendRegionMapRepository : ITrendRegionMapRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "TrendRegionMapping";

        public TrendRegionMapRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }
        /// <summary>
        /// To get all Region with mapped/unmapped status by trend id
        /// </summary>
        /// <param name="TrendIdint"></param>
        /// <param name="PageStart"></param>
        /// <param name="pageSize"></param>
        /// <param name="SortDirection"></param>
        /// <param name="OrdbyByColumnName"></param>
        /// <param name="Search"></param>
        /// <param name="isDeletedInclude"></param>
        /// <returns></returns>
        public IEnumerable<TrendRegionMapViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            var result = dapper.Query<TrendRegionMapViewModel>(TrendRegionMapQueries.StoreProcSelectAllRegions
                            , null
                            , null, true, null, CommandType.StoredProcedure);

            DataTable regionIdsTable = new DataTable();
            regionIdsTable.Columns.Add("ID", typeof(Guid));
            foreach (var item in result)
            {
                if (item.RegionId != null)
                {
                    regionIdsTable.Rows.Add(item.RegionId);
                }
            }

            var countries = dapper.Query<CountryViewModel>(TrendRegionMapQueries.StoreProcSelectAllCountries
                    , new
                    {
                        @TrendID = Search,
                        @RegionIDList = regionIdsTable.AsTableValuedParameter("dbo.udtUniqueIdentifier")
                    }
                    , null, true, null, CommandType.StoredProcedure).ToList();

            List<RegionCountryMapModel> ListRVM = new List<RegionCountryMapModel>();
            List<CountryViewModel> ListCVM = new List<CountryViewModel>();

            foreach (var trendRegion in result.ToList())
            {
                foreach (var country in countries)
                {
                    if (trendRegion.RegionId == country.RegionId)
                    {
                        CountryViewModel CVM = new CountryViewModel();
                        CVM.CountryCode = country.CountryCode;
                        CVM.CountryId = country.CountryId;
                        CVM.CountryName = country.CountryName;
                        CVM.IsAssigned = country.IsAssigned;
                        CVM.CreatedOn = country.CreatedOn;
                        CVM.EndUser = country.EndUser;
                        CVM.Impact = country.Impact;
                        CVM.ParentRegionId = country.ParentRegionId;
                        CVM.RegionId = country.RegionId;
                        CVM.RegionLevel = country.RegionLevel;
                        CVM.RegionName = country.RegionName;
                        CVM.TrendId = country.TrendId;
                        CVM.TrendRegionMapId = country.TrendRegionMapId;
                        ListCVM.Add(CVM);
                    }
                }

                if (ListCVM != null && !ListCVM.Equals(null))
                {
                    trendRegion.CountryList = new List<CountryViewModel>();
                    trendRegion.CountryList.AddRange(ListCVM);
                    ListCVM.Clear();
                }
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="trendRegionMapInsertDbViewModel"></param>
        /// <returns></returns>
        public Result<SpTransactionMessage> InsertUpdate(MainTrendRegionMapInsertUpdateDbViewModel trendRegionMapInsertDbViewModel)
        {

            DataTable trendRegionMapTable = new DataTable();
            trendRegionMapTable.Columns.Add("ID", typeof(Guid));
            trendRegionMapTable.Columns.Add("MapId", typeof(Guid));
            trendRegionMapTable.Columns.Add("bitValue", typeof(bool));
            if (trendRegionMapInsertDbViewModel.TrendRegionMapInsertUpdate != null & trendRegionMapInsertDbViewModel.TrendRegionMapInsertUpdate.Count > 0)
            {
                foreach (var item in trendRegionMapInsertDbViewModel.TrendRegionMapInsertUpdate)
                {
                    trendRegionMapTable.Rows.Add(new Object[] { item.RegionId, item.ImpactId, item.IsMapped });
                }
            }

            try
            {
                var spTransactionMessage = _dbHelper.ExecuteQuery(TrendRegionMapQueries.StoreProcInsertUpdate,
                new
                {
                    @TrendId = trendRegionMapInsertDbViewModel.TrendId,
                    @TrendRegionmap = trendRegionMapTable,
                    @AviraUserId = trendRegionMapInsertDbViewModel.AviraUserId
                }
                     , CommandType.StoredProcedure);

                if (spTransactionMessage.IsSuccess)
                {
                    DataTable countries = new DataTable();
                    countries.Columns.Add("ID", typeof(Guid));
                    countries.Columns.Add("MapId", typeof(Guid));
                    countries.Columns.Add("bitValue", typeof(bool));

                    if (trendRegionMapInsertDbViewModel.TrendRegionMapInsertUpdate != null & trendRegionMapInsertDbViewModel.TrendRegionMapInsertUpdate.Count > 0)
                    {
                        foreach (var item in trendRegionMapInsertDbViewModel.TrendRegionMapInsertUpdate)
                        {
                            if (item.CountryList != null && item.CountryList.Count > 0)
                            {
                                foreach (var country in item.CountryList)
                                {
                                    if (country.Impact != Guid.Empty && country.Impact != null)
                                    {
                                        countries.Rows.Add(new Object[] { country.CountryId, country.Impact, country.EndUser });
                                    }
                                }
                            }
                        }
                    }


                    var spTransMessage = _dbHelper.ExecuteQuery(TrendRegionMapQueries.StoreProcInsertUpdateCountries,
                    new
                    {
                        @TrendId = trendRegionMapInsertDbViewModel.TrendId,
                        @TrendCountryMap = countries,
                        @AviraUserId = trendRegionMapInsertDbViewModel.AviraUserId
                    }
                     , CommandType.StoredProcedure);
                }
                return spTransactionMessage;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public IEnumerable<RegionCountryModel> GetRegionByTrendID(Guid trendId)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            var result = dapper.Query<RegionCountryModel>(TrendRegionMapQueries.StoreProcSelectAllRegions
                            , null
                            , null, true, null, CommandType.StoredProcedure);

            DataTable regionIdsTable = new DataTable();
            regionIdsTable.Columns.Add("ID", typeof(Guid));
            foreach (var item in result)
            {
                if (item.RegionId != null)
                {
                    regionIdsTable.Rows.Add(item.RegionId);
                }
            }

            var countries = dapper.Query<CountryViewModel>(TrendRegionMapQueries.StoreProcSelectAllCountries
                    , new
                    {
                        @TrendID = trendId,
                        @RegionIDList = regionIdsTable.AsTableValuedParameter("dbo.udtUniqueIdentifier")
                    }
                    , null, true, null, CommandType.StoredProcedure).ToList();

            List<RegionCountryModel> ListRVM = new List<RegionCountryModel>();
            List<CountryViewModel> ListCVM = new List<CountryViewModel>();

            foreach (var region in result.ToList())
            {
                foreach (var country in countries)
                {
                    if (region.RegionId == country.RegionId)
                    {
                        CountryViewModel CVM = new CountryViewModel();
                        CVM.CountryCode = country.CountryCode;
                        CVM.CountryId = country.CountryId;
                        CVM.CountryName = country.CountryName;
                        CVM.IsAssigned = country.IsAssigned;
                        CVM.CreatedOn = country.CreatedOn;
                        CVM.EndUser = country.EndUser;
                        CVM.Impact = country.Impact;
                        CVM.ParentRegionId = country.ParentRegionId;
                        CVM.RegionId = country.RegionId;
                        CVM.RegionLevel = country.RegionLevel;
                        CVM.RegionName = country.RegionName;
                        CVM.TrendId = country.TrendId;
                        CVM.TrendRegionMapId = country.TrendRegionMapId;
                        ListCVM.Add(CVM);
                    }
                }

                if (ListCVM != null && !ListCVM.Equals(null))
                {
                    region.CountryList = new List<CountryViewModel>();
                    region.CountryList.AddRange(ListCVM);
                    ListCVM.Clear();
                }
            }

            return result;
        }
    }
}
