﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.Trend
{
    public class TrendKeyCompanyMapViewModel
    {
        public Guid TrendId { get; set; }
        public Guid CompanyId { get; set; }
        public string CompanyName { get; set; }
        public bool IsMapped { get; set; }
        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "KeyCompanyMap";
            }
        }
    }


    public class MainTrendKeyCompanyMapInsertUpdateDbViewModel
    {
        public Guid TrendId { get; set; }
        public string CompanyName { get; set; }
        public List<TrendKeyCompanyMapViewModel> TrendKeyCompanyMapInsertUpdate { get; set; }
        public Guid [] CompanyIds { get; set; }
        public Guid AviraUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public TrendViewModel TrendModel { get; set; }
        public TrendNoteViewModel TrendNoteModel { get; set; }
        //public CompanyInsertDbViewModel CompanyModel { get; set; }
    }
}
