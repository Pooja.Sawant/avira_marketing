﻿CREATE TABLE [dbo].[RawMaterial](
	[Id] [uniqueidentifier] NOT NULL,
	[RawMaterialName] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](256) NULL,
	[CreatedOn] [datetime2](7) NULL,
	[UserCreatedById] [uniqueidentifier] NULL,
	[ModifiedOn] [datetime2](7) NULL,
	[UserModifiedById] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeletedOn] [datetime2](7) NULL,
	[UserDeletedById] [uniqueidentifier] NULL,
 CONSTRAINT [PK_RawMaterial] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RawMaterial] ADD  CONSTRAINT [DF_RawMaterial_RawMaterialName]  DEFAULT ('') FOR [RawMaterialName]
GO
ALTER TABLE [dbo].[RawMaterial] ADD  CONSTRAINT [DF_RawMaterial_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[RawMaterial]  WITH CHECK ADD  CONSTRAINT [FK_RawMaterial_AviraUser] FOREIGN KEY([UserCreatedById])
REFERENCES [dbo].[AviraUser] ([Id])
GO
ALTER TABLE [dbo].[RawMaterial] CHECK CONSTRAINT [FK_RawMaterial_AviraUser]
GO
ALTER TABLE [dbo].[RawMaterial]  WITH CHECK ADD  CONSTRAINT [FK_RawMaterial_AviraUser1] FOREIGN KEY([UserModifiedById])
REFERENCES [dbo].[AviraUser] ([Id])
GO
ALTER TABLE [dbo].[RawMaterial] CHECK CONSTRAINT [FK_RawMaterial_AviraUser1]
GO