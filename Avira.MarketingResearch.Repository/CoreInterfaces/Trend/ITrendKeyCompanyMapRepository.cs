﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.Trend
{
    public interface ITrendKeyCompanyMapRepository
    {
      /// <summary>
      /// Get Un-Mapped Company
      /// </summary>
      /// <param name="PageStart"></param>
      /// <param name="pageSize"></param>
      /// <param name="SortDirection"></param>
      /// <param name="OrdbyByColumnName"></param>
      /// <param name="Search"></param>
      /// <param name="isDeletedInclude"></param>
      /// <returns></returns>
        IEnumerable<TrendKeyCompanyMapViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false, string id = "");

        /// <summary>
        /// 
        /// </summary>
        /// <param name="trendKeyCompanyMapInsertDbViewModel"></param>
        /// <returns></returns>
        Result<SpTransactionMessage> InsertUpdate(MainTrendKeyCompanyMapInsertUpdateDbViewModel trendKeyCompanyMapInsertDbViewModel);

        Result<SpTransactionMessage> Insert(MainTrendKeyCompanyMapInsertUpdateDbViewModel trendKeyCompanyMapInsertDbViewModel);

        Result<SpTransactionMessage> Delete(MainTrendKeyCompanyMapInsertUpdateDbViewModel trendKeyCompanyMapInsertDbViewModel);
    }
}
