﻿CREATE TABLE [dbo].[QualitativeNote] (
    [Id]                  UNIQUEIDENTIFIER NOT NULL,
    [QualitativeId]       UNIQUEIDENTIFIER NOT NULL,
    [ProjectId]           UNIQUEIDENTIFIER NULL,
    [AuthorRemark]        NVARCHAR (512)   NULL,
    [ApproverRemark]      NVARCHAR (512)   NULL,
    [QualitativeStatusID] UNIQUEIDENTIFIER NULL,
    [CreatedOn]           DATETIME2 (7)    NULL,
    [UserCreatedById]     UNIQUEIDENTIFIER NULL,
    [ModifiedOn]          DATETIME2 (7)    NULL,
    [UserModifiedById]    UNIQUEIDENTIFIER NULL,
    [IsDeleted]           BIT              NOT NULL,
    [DeletedOn]           DATETIME2 (7)    NULL,
    [UserDeletedById]     UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_QualitativeNote] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_QualitativeNote_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_QualitativeNote_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id])
);

