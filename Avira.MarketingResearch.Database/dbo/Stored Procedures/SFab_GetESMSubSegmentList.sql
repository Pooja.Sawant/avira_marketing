﻿CREATE PROCEDURE [dbo].[SFab_GetESMSubSegmentList]    
(  
	@ProjectId uniqueidentifier, 
	@SegmentId uniqueidentifier,
	@SubSegmentIdList [dbo].[udtUniqueIdentifier] READONLY,--Optional
	@AviraUserID uniqueidentifier = null    
)    
as    
/*================================================================================    
Procedure Name: SFab_GetESMSubSegmentList   
Author: Praveen    
Create date: 12/09/2019    
Description: To return all ecosystem subsegemnt based on the segmentId and projectId. And optional imput values for subsegmentIds
Change History    
Date  Developer  Reason  
__________ ____________ ______________________________________________________    
12/09/2019  Praveen   Initial Version   
13/09/2019  Harshal   Done Implemention As per Wireframe
================================================================================*/    
BEGIN    
------------------
	--Join these table SubSegment, ESMSegmentMapping
	--Case 1: if @SubSegmentIdList is not null then it should return data based on the segmentID and SubSegmentIdList
	--Case 2: if @SubSegmentIdList is null then it should return data based on the segmentID level	
	--Expected Columns : SubSegmentnameId and SubSegmentname
	
	SELECT SubSegment.Id,SubSegment.SubSegmentName as SegmentName FROM SubSegment
    INNER JOIN ESMSegmentMapping ON ESMSegmentMapping.SubSegmentId=SubSegment.Id
    WHERE ESMSegmentMapping.ProjectId=@ProjectId
    AND  ESMSegmentMapping.SegmentId=@SegmentId
    AND (not exists (select ID from @SubSegmentIdList) 
        or SubSegment.id In (select ID from @SubSegmentIdList))
    Order By SubSegment.SubSegmentName 

END