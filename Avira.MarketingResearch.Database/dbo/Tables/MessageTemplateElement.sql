﻿CREATE TABLE [dbo].[MessageTemplateElement] (
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [MessageTemplateId] UNIQUEIDENTIFIER NOT NULL,
    [ElementType]       NVARCHAR (50)    CONSTRAINT [DF_MessageTemplateElement_ElementType] DEFAULT (N'subject is for subject elemnt, Body is for message body elements and attachment is for attachment fiies') NULL,
    [Element]           NVARCHAR (500)   NULL,
    [IsDeleted]         BIT              CONSTRAINT [DF_MessageTemplateElement_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]         DATETIME2 (7)    NULL,
    [UserCreatedById]   UNIQUEIDENTIFIER NULL,
    [ModifiedOn]        DATETIME2 (7)    NULL,
    [UserModifiedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_MessageTemplateElement] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_MessageTemplateElement_MessageTemplate] FOREIGN KEY ([MessageTemplateId]) REFERENCES [dbo].[MessageTemplate] ([Id])
);

