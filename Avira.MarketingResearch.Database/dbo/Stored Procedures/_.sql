﻿CREATE PROCEDURE [dbo].[ ]      
(    
 @ProjectId UNIQUEIDENTIFIER,
 @ModuleName nvarchar(100)
)  
As
/*================================================================================      
Procedure Name: SFab_GetDownloadStaticData     
Author: Nitin      
Create date: 29-Feb-2020
Description: To return download static data 
Change History      
Date  Developer  Reason    
__________ ____________ ______________________________________________________      
29-Feb-2020  Praveen   Initial Version  

================================================================================*/      
BEGIN      
------------------  
    --Required tables: DownloadsExportData, ModuleType
	--Notes: Get all download static data for project and moduletype
	--Expected Columns : Header, Footer, ReportTitle, ImageURL, ImageName, Overview, KeyPoints, Methodology, AnalystName, AnalystContactNo, AnalystEmailId

       Select Header,
		  Footer,
		  ReportTitle,
		  CoverPageImageLink,
          CoverPageImageName,
		  Overview,
		  KeyPoints,
		  Methodology,
		  MethodologyImageLink,
          ContactUsAnalystName,
          ContactUsOfficeContactNo,
          ContactUsAnalystEmailId
			 From DownloadsExportData Inner Join ModuleType
			 ON moduletype.id = DownloadsExportData.moduletypeId  
			 Where ProjectId = @ProjectId  and Modulename = @ModuleName
END