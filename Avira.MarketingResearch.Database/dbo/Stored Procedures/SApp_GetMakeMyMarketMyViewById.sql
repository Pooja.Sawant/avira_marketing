﻿      
      
      
CREATE procedure [dbo].[SApp_GetMakeMyMarketMyViewById]      
(
	@CustomerId uniqueidentifier,
	@ProjectId uniqueidentifier,
	@MarketId uniqueidentifier,
	@SegmentId uniqueidentifier      
)      
as      
/*================================================================================      
Procedure Name: [SApp_GetMakeMyMarketMyViewById]      
Author: krishna      
Create date: 25/07/2019      
Description: Get My View Data From MakeMyMarketMyView Table      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
25/07/2019 krishna   Initial Version      
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
    
	SELECT MMV.Id,
		   MMV.ProjectId,	
		   MMV.CustomerId,
		   MMV.MarketId,
		   MMV.SegmentId,
		   MMV.Year,
		   MMV.MyViewValue,
		   MMV.CAGR,
		   MMM.CustomMarketName
	FROM MakeMyMarketMyView MMV  
		Inner Join MakeMyMarket MMM 
			On MMV.MarketId = MMM.Id
	WHERE MMV.CustomerId = @CustomerId
		AND MMV.ProjectId = @ProjectId   
		AND MMV.MarketId = @MarketId  
		AND MMV.SegmentId = @SegmentId    
		AND MMV.IsDeleted = 0
	ORDER BY MMV.Year 
    
END