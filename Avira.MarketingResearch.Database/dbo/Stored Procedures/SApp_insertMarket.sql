﻿CREATE procedure [dbo].[SApp_insertMarket]      
(      
@MarketName nvarchar(256),      
@Description nvarchar(256),      
@UserCreatedById uniqueidentifier,    
@Id uniqueidentifier,
@ParentId uniqueidentifier,    
@CreatedOn Datetime  
)      
as      
/*================================================================================      
Procedure Name: SApp_insertMarket      
Author: Pooja      
Create date: 03/25/2019      
Description: Insert a record into Market table      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
03/25/2019 Pooja   Initial Version    
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)  
    
--Validations      
IF exists(select 1 from [dbo].[Market]       
   where MarketName = @MarketName AND IsDeleted = 0)      
begin       
set @ErrorMsg = 'Market with Name "'+ @MarketName + '" already exists.';    
SELECT StoredProcedureName ='SApp_insertMarket',Message =@ErrorMsg,Success = 0;    
RETURN;    
end      
--End of Validations      
BEGIN TRY         
         
insert into [dbo].[Market]([Id],      
        [MarketName],      
        [Description], 
		[ParentId],
        [IsDeleted],      
        [UserCreatedById],      
        [CreatedOn])      
 values(@Id,       
   @MarketName,       
   @Description,
   @ParentId,     
   0,      
   @UserCreatedById,       
   @CreatedOn);       
     
SELECT StoredProcedureName ='SApp_insertMarket',Message =@ErrorMsg,Success = 1;       
    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);      
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,@CreatedOn);      
    
 set @ErrorMsg = 'Error while creating a new Market, please contact Admin for more details.';    
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
       
END CATCH      
      
      
end