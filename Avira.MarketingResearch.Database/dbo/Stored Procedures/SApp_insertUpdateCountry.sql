﻿CREATE procedure [dbo].[SApp_insertUpdateCountry]  
(@CountryID uniqueidentifier = NULL,  
@CountryCode nvarchar(3),  
@CountryName nvarchar(256),  
@IsDeleted bit,  
@AviraUserId uniqueidentifier)  
as  
/*================================================================================  
Procedure Name: SApp_DeleteCountrybyID  
Author: Sharath  
Create date: 02/11/2019  
Description: soft delete a record from Country table by ID  
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
02/11/2019 Sharath   Initial Version  
================================================================================*/  
BEGIN  
SET NOCOUNT ON;  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
  
IF exists(select 1 from [dbo].[Country]   
   where ((@CountryID is null or [Id] !=  @CountryID)  
   and  CountryCode= @CountryCode AND IsDeleted = 0))  
begin  
declare @ErrorMsg NVARCHAR(2048);  
set @ErrorMsg = 'Country with Code "'+ @CountryCode + '" already exists.';  
THROW 50000,@ErrorMsg,1;  
end  
  
IF (@CountryID is null)  
begin  
--Insert new record  
 set @CountryID = NEWID();  
  
 insert into [dbo].[Country]([Id],  
        [CountryCode],  
        [CountryName],  
        [IsDeleted],  
        [UserCreatedById],  
        [CreatedOn])  
 values(@CountryID,   
   @CountryCode,   
   @CountryName,   
   0,  
   @AviraUserId,   
   getdate());  
end  
else  
begin  
--update record  
 update [dbo].[Country]  
 set [CountryCode] = @CountryCode,  
  [CountryName] = @CountryName,  
  [ModifiedOn] = getdate(),  
  DeletedOn = case when IsDeleted = 0 and @IsDeleted = 1 then getdate() else DeletedOn end,  
  UserDeletedById = case when IsDeleted = 0 and @IsDeleted = 1 then @AviraUserId else UserDeletedById end,  
  IsDeleted = isnull(@IsDeleted, IsDeleted),  
  [UserModifiedById] = @AviraUserId  
     where ID = @CountryID  
end  
  
select @CountryID;  
end