﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyTrackerController : AviraBaseController
    {
        public readonly ICompanyTrackerRepository _companyTrackerRepository;
        public readonly ICompanyTrackerService _companyTrackerService;
        private ILoggerManager _logger;

        public CompanyTrackerController(
            ICompanyTrackerService companyTrackerService,
            ICompanyTrackerRepository companyTrackerRepository,
            ILoggerManager logger)
        {
            _companyTrackerRepository = companyTrackerRepository;
            _companyTrackerService = companyTrackerService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<MainCompanyTrackerViewModel> Get(Guid Id, int PageStart = 0, int pageSize = 0, int SortDirection = 1, string OrdbyByColumnName = "CreatedDate", string Search = "", bool isDeletedInclude = false)
        {
            MainCompanyTrackerViewModel res = null;
            try
            {
                res = _companyTrackerService.GetAll(Id, PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }


        [HttpGet("{id}")]
        public ActionResult<CompanyTrackerViewModel> Get(Guid id)
        {
            CompanyTrackerViewModel companyTracker = null;
            try
            {
                companyTracker = _companyTrackerService.GetById(id);
                if (companyTracker == null)
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return companyTracker;
        }
    }
}