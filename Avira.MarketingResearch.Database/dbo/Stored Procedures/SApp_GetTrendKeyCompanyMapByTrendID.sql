﻿      
 CREATE PROCEDURE [dbo].[SApp_GetTrendKeyCompanyMapByTrendID]            
 (@TrendID uniqueidentifier = null,    
 @CompanyName nvarchar(256)='',    
 @includeDeleted bit = 0,        
 @OrdbyByColumnName NVARCHAR(50) ='CompanyName',        
 @SortDirection INT = -1,        
 @PageStart INT = 0,        
 @PageSize INT = null)            
 AS            
/*================================================================================            
Procedure Name: SApp_GetTrendKeyCompanyMapByTrendID        
Author: Praveen             
Create date: 04/22/2019            
Description: Get list from Key Company Map table by TrendID            
Change History            
Date  Developer  Reason            
__________ ____________ ______________________________________________________            
04/12/2019 Praveen   Initial Version 
================================================================================*/           
            
BEGIN            
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED         
if @PageSize is null or @PageSize =0        
set @PageSize = 100        
if isnull(@CompanyName,'') = '' set @CompanyName = '' 
else set @CompanyName = '%'+ @CompanyName +'%';
begin        
;WITH CTE_TBL AS (        
 SELECT         
 --@TrendID as TrendID,          
 [Company].[Id] AS [CompanyId],            
  [Company].[CompanyName],           
   [Company].[Description],        
   [Company].IsDeleted,
   TrendKeyCompanyMap.TrendId,      
   TrendKeyCompanyMap.Id as TrendKeyCompanyMapId,      
  cast (case when exists(select 1 from dbo.TrendKeyCompanyMap         
     where TrendKeyCompanyMap.TrendId = @TrendID        
     and TrendKeyCompanyMap.IsDeleted = 0        
     and TrendKeyCompanyMap.CompanyId = Company.Id) then 1 else 0 end as bit) as IsMapped,        
 CASE        
  WHEN @OrdbyByColumnName = 'CompanyName' THEN [Company].CompanyName        
  WHEN @OrdbyByColumnName = 'Description' THEN [Company].[Description]
  when @OrdbyByColumnName = 'IsMapped' THEN cast(case when TrendKeyCompanyMap.TrendId is null then 1 else 0 end as varchar(2)) 
  ELSE [Company].CompanyName        
 END AS SortColumn        
        
FROM [dbo].Company      
LEFT JOIN dbo.TrendKeyCompanyMap       
ON Company.Id = TrendKeyCompanyMap.CompanyId
and TrendKeyCompanyMap.TrendId = @TrendID
and TrendKeyCompanyMap.IsDeleted = 0
where (@CompanyName = '' or [Company].[CompanyName] LIKE @CompanyName)
AND (@includeDeleted = 1 or Company.IsDeleted = 0)        
)        
        
 SELECT         
    CTE_TBL.*,        
    tCountResult.TotalItems        
  FROM CTE_TBL        
  CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult        
  ORDER BY         
    CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,        
    CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC         
  OFFSET @PageStart ROWS        
  --FETCH NEXT @PageSize ROWS ONLY;        
  END        
        
END