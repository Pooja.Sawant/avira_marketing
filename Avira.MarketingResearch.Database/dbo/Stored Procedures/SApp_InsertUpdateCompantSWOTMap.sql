﻿
--sp_helptext SApp_InsertUpdateCompantSWOTMap

     
CREATE procedure [dbo].[SApp_InsertUpdateCompantSWOTMap]              
(        
 @CompanySwotMap [dbo].[udtGUIDCompanySwotMapMultiValue] readonly,        
 @CompanyId uniqueidentifier,
 @ProjectId uniqueidentifier,         
 @UserCreatedById uniqueidentifier        
)              
as        
/*================================================================================        
Procedure Name: SApp_InsertUpdateCompantSWOTMap        
Author: Harshal        
Create date: 06/05/2019        
Description: Insert or updare Company SWOT By company ID and ProjectId      
Change History        
Date  Developer  Reason        
__________ ____________ ______________________________________________________        
06/05/2019 Harshal   Initial Version        
================================================================================*/        
      
BEGIN              
SET NOCOUNT ON;              
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;              
declare @ErrorMsg NVARCHAR(2048)          
            
              
BEGIN TRY                 
         
update [dbo].[CompanySWOTMap]        
set IsDeleted = 1,        
UserDeletedById = @UserCreatedById,        
DeletedOn = GETDATE()        
from [dbo].[CompanySWOTMap]        
where [CompanySWOTMap].CompanyId = @CompanyId AND [CompanySWOTMap].ProjectId=@ProjectId      
and not exists (select 1 from @CompanySwotMap map where map.Id = [CompanySWOTMap].Id)        
        
update [dbo].[CompanySWOTMap]        
set [dbo].[CompanySwotMap].CategoryId = map1.CategoryId, 
[dbo].[CompanySwotMap].SWOTDescription = map1.SWOTDescription,
[dbo].[CompanySwotMap].ModifiedOn = GETDATE(),        
[dbo].[CompanySwotMap].UserModifiedById = @UserCreatedById        
from [dbo].[CompanySWOTMap]        
inner join @CompanySwotMap map1         
on map1.Id = [CompanySWOTMap].Id        
where [CompanySWOTMap].CompanyId = @CompanyId AND [CompanySWOTMap].ProjectId = @ProjectId;        
        
--Insert new records        
insert into [dbo].[CompanySWOTMap](        
        [dbo].[CompanySWOTMap].[Id]              
       ,[dbo].[CompanySWOTMap].[CompanyId] 
	   ,[dbo].[CompanySWOTMap].[ProjectId]       
       ,[dbo].[CompanySWOTMap].[CategoryId] 
	   ,[dbo].[CompanySWOTMap].[SWOTTypeId]       
       ,[dbo].[CompanySWOTMap].[SWOTDescription]                
       ,[dbo].[CompanySWOTMap].[IsDeleted]              
       ,[dbo].[CompanySWOTMap].[UserCreatedById]              
       ,[dbo].[CompanySWOTMap].[CreatedOn])              
 select map.Id,        
   map.CompanyId,
   map.ProjectId,
   map.CategoryId,
   st.Id,
   map.SWOTDescription,               
   0,              
   @UserCreatedById,               
   GETDATE()        
 from @CompanySwotMap map        
 inner join swotType st on map.SWOTTypeName = st.SWOTTypeName
  where not exists (select 1 from [dbo].[CompanySWOTMap]         
     where [CompanySWOTMap].CompanyId = @CompanyId AND [CompanySWOTMap].ProjectId = @ProjectId and [CompanySWOTMap].Id=map.Id )             
        
              
        
SELECT StoredProcedureName ='SApp_InsertUpdateCompantSWOTMap',Message =@ErrorMsg,Success = 1;               
            
END TRY              
BEGIN CATCH              
    -- Execute the error retrieval routine.              
 DECLARE @ErrorNumber int;              
 DECLARE @ErrorSeverity int;              
 DECLARE @ErrorProcedure varchar(100);                
 DECLARE @ErrorLine int;              
 DECLARE @ErrorMessage varchar(500);              
              
  SELECT @ErrorNumber = ERROR_NUMBER(),              
        @ErrorSeverity = ERROR_SEVERITY(),              
        @ErrorProcedure = ERROR_PROCEDURE(),              
        @ErrorLine = ERROR_LINE(),              
        @ErrorMessage = ERROR_MESSAGE()              
              
 insert into dbo.Errorlog(ErrorNumber,              
       ErrorSeverity,              
       ErrorProcedure,              
       ErrorLine,              
       ErrorMessage,              
       ErrorDate)              
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,ERROR_MESSAGE(),GETDATE());              
            
 set @ErrorMsg = 'Error while insert a new CompanyTrendsMap, please contact Admin for more details.';            
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine            
    ,Message =@ErrorMsg,Success = 0;               
               
END CATCH          
end