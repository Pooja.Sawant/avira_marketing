﻿CREATE procedure [dbo].[SApp_DeleteRegionbyID]
(@RegionID uniqueidentifier,
@AviraUserId uniqueidentifier)
as
/*================================================================================
Procedure Name: SApp_DeleteRegionbyID
Author: Laxmikant
Create date: 04/03/2019
Description: soft delete a record from Region table by ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
04/03/2019	Laxmikant			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


UPDATE [dbo].[Region]
   SET [IsDeleted] = 1,
   [DeletedOn] = getdate(),
   [UserDeletedById] = @AviraUserId
where ID = @RegionID
and IsDeleted = 0 -- no need to delete record which is already deleted

end