﻿using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using Avira.MarketingResearch.Repository.Helpers;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport
{
    public interface ITrendImportExportRepository
    {
        List<ImportCommonResponseModel> Create(TrendImportInsertViewModel viewModel);
    }
}
