﻿CREATE TABLE [dbo].[CompanySegmentInformation] (
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [CompanyId]         UNIQUEIDENTIFIER NOT NULL,
    [CurrencyId]        UNIQUEIDENTIFIER NOT NULL,
    [ValueConversionId] UNIQUEIDENTIFIER NOT NULL,
    [Year]              INT              NOT NULL,
    [Type]              NVARCHAR (255)   NOT NULL,
    [Name]              NVARCHAR (255)   NOT NULL,
    [Value]             DECIMAL (19, 4)  NOT NULL,
    [CreatedOn]         DATETIME2 (7)    NULL,
    [UserCreatedById]   UNIQUEIDENTIFIER NULL,
    [ModifiedOn]        DATETIME2 (7)    NULL,
    [UserModifiedById]  UNIQUEIDENTIFIER NULL,
    [IsDeleted]         BIT              NOT NULL,
    [DeletedOn]         DATETIME2 (7)    NULL,
    [UserDeletedById]   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CompanySegmentInformation] PRIMARY KEY CLUSTERED ([Id] ASC)
);



