﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.Models;
using Avira.MarketingResearch.Web.Utils;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Avira.MarketingResearch.Web.Controllers
{
    [NoCache]
    [Authorization]
    public class TrendController : BaseController
    {
        public readonly IServiceRepository _serviceRepository;
        private ILoggerManager _logger;
        private readonly IConfiguration _configuration;

        public TrendController(IServiceRepository serviceRepository, ILoggerManager logger, IConfiguration configuration)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
            _configuration = configuration;
        }

        public ActionResult Index(Guid Id)
        {
            try
            {
                ViewBag.Message = string.Empty;
                int pageSize = int.Parse(_configuration.GetSection("Pagination")["TrendPagingSize"]);
                int PageStart = 0;
                var SortDirection = -1;
                var sortColumn = "";
                string search = "";
                var LoginId = HttpContext.Session.GetString("UserId");
                HttpResponseMessage response = _serviceRepository.GetResponseByUserId("Project", Id, new Guid(LoginId), PageStart, pageSize, SortDirection, sortColumn, search);
                response.EnsureSuccessStatusCode();
                List<ProjectViewModel> project = response.Content.ReadAsAsync<List<ProjectViewModel>>().Result;
                foreach (var proj in project)
                {
                    if (!string.IsNullOrEmpty(proj.MApproverApprover))
                    {
                        var jApprover = JsonConvert.DeserializeObject<List<object>>(proj.MApproverApprover);
                        var definition = new[] { new { Id = "", ResourceName = "" } };
                        var customer = JsonConvert.DeserializeAnonymousType(proj.MApproverApprover, definition);
                        var name = customer.Select(x => x.ResourceName).ToList();
                        proj.Approver = (string)name[0];
                    }
                    if (!string.IsNullOrEmpty(proj.TrendsFormsAuthor))
                    {
                        var jApprover = JsonConvert.DeserializeObject<List<object>>(proj.TrendsFormsAuthor);
                        var definition = new[] { new { Id = "", ResourceName = "" } };
                        var customer = JsonConvert.DeserializeAnonymousType(proj.TrendsFormsAuthor, definition);
                        var name = customer.Select(x => x.ResourceName).ToList();
                        proj.ProjectAuthorName = (string)name[0];
                    }
                    if (!string.IsNullOrEmpty(proj.TrendsFormsCoAuthor))
                    {
                        var jApprover = JsonConvert.DeserializeObject<List<object>>(proj.TrendsFormsCoAuthor);
                        var definition = new[] { new { Id = "", ResourceName = "" } };
                        var customer = JsonConvert.DeserializeAnonymousType(proj.TrendsFormsCoAuthor, definition);
                        var name = customer.Select(x => x.ResourceName).ToList();
                        proj.ProjectCoAuthorName = (string)name[0];
                    }
                }
                ViewBag.ProjectId = Id;
                if (project != null && project.Count > 0)
                {
                    ViewBag.ProjectName = project[0].ProjectName;
                    ViewBag.ProjectCode = project[0].ProjectCode;
                    ViewBag.Approver = project[0].Approver;
                    ViewBag.ProjectAuthorName = project[0].ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project[0].ProjectCoAuthorName;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View();
        }

        public ActionResult LoadData(TrendViewModel trendViewModel)
        {
            ViewBag.Message = string.Empty;
            var ProjectId = Request.Form["ProjectId"].FirstOrDefault();
            var draw = Request.Form["draw"].FirstOrDefault();
            int totalRecords = 0;
            List<TrendViewModel> data = null;
            try
            {

                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                string search = Request.Form["search[value]"];
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDir = Request.Form["order[0][dir]"].FirstOrDefault();
                var SortDirection = -1;
                if (sortColumnDir == "asc")
                {
                    SortDirection = 1;
                }
                else
                {
                    SortDirection = -1;
                }

                int skip = start != null ? Convert.ToInt32(start) : 0;
                int pageSize = length.ToString() != null ? Convert.ToInt32(length) : 0;
                int PageStart = start.ToString() != null ? Convert.ToInt32(start) : 0;
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();

                HttpResponseMessage response = _serviceRepository.GetResponse(contollerName, trendViewModel.ProjectID, PageStart, pageSize, SortDirection, sortColumn, search);
                response.EnsureSuccessStatusCode();
                List<TrendViewModel> trends = response.Content.ReadAsAsync<List<TrendViewModel>>().Result;
                ViewBag.Title = "All Trends";
                data = trends;
                
                foreach (var item in trends)
                {
                    totalRecords = item.TotalItems;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data });

        }


        // GET: Trend/Create
        public ActionResult Create(Guid Id)
        {
            TrendInsertDbViewModel trendInsertDbViewModel = new TrendInsertDbViewModel();
            try
            {
                trendInsertDbViewModel.ProjectID = Id;
                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, trendInsertDbViewModel.ProjectID);
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                    ViewBag.StatusName = string.Empty;
                }
                ViewBag.ProjectID = Id;
                ViewBag.Message = string.Empty;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(trendInsertDbViewModel);
        }

        [HttpPost]
        public IActionResult Create(TrendInsertDbViewModel trend, string actionName)
        {
            ViewBag.Message = string.Empty;
            bool bResult = false;
            try
            {
                trend.ProjectID = trend.Id; 
                if (actionName.Equals("Submitted"))
                {
                    trend.TrendStatusName = "Submitted";
                    bResult = validate(trend);
                    if (bResult)
                    {
                        var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                        HttpResponseMessage response = _serviceRepository.PutResponse(contollerName, trend);
                        response.EnsureSuccessStatusCode();
                        ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            ViewBag.Message = viewModel.Message;
                        }
                    }
                }
                else if (actionName.Equals("Approve"))
                {
                    trend.TrendStatusName = "Approved";
                    bResult = validate(trend);
                    if (bResult)
                    {
                        var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                        HttpResponseMessage response = _serviceRepository.PutResponse(contollerName, trend);
                        response.EnsureSuccessStatusCode();
                        ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            ViewBag.Message = viewModel.Message;
                        }
                    }
                }
                else if (actionName.Equals("Rejected"))
                {
                    trend.TrendStatusName = "Rejected";
                    bResult = validate(trend);
                    if (bResult)
                    {
                        var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                        HttpResponseMessage response = _serviceRepository.PutResponse(contollerName, trend);
                        response.EnsureSuccessStatusCode();
                        ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            ViewBag.Message = viewModel.Message;
                        }
                    }
                }
               
                else
                {
                    trend.TrendStatusName = "Draft";
                    bResult = validate(trend);
                    if (bResult)
                    {
                        var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                        HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, trend);
                        response.EnsureSuccessStatusCode();
                        ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            ViewBag.Message = viewModel.Message;
                        }
                    }
                }
                ViewBag.StatusName = trend.TrendStatusName;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            ViewBag.ProjectID = trend.ProjectID;

            return View(trend);
        }

        [HttpGet]
        public IActionResult Edit(string id)
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.GetByIdResponse(contollerName, id);
                response.EnsureSuccessStatusCode();
                TrendUpdateDbViewModel trend = response.Content.ReadAsAsync<TrendUpdateDbViewModel>().Result;
                HttpResponseMessage Noteresponse = _serviceRepository.GetResponse("TrendNote", id,null);
                Noteresponse.EnsureSuccessStatusCode();
                List<TrendNoteViewModel> StatusNote = Noteresponse.Content.ReadAsAsync<List<TrendNoteViewModel>>().Result;
                trend.StatusList = StatusNote;
                ViewBag.ProjectID = trend.ProjectID;
                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, trend.ProjectID);
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                    ViewBag.StatusName = trend.TrendStatusName;
                }
                return View(trend);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                ErrorViewModel err = new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier };
                return View("Error", err);
            }
        }

        

        [HttpPost]
        public IActionResult Edit(TrendUpdateDbViewModel trend, string actionName)
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage responseNote = _serviceRepository.GetResponse("TrendNote", trend.Id.ToString(), null);
                responseNote.EnsureSuccessStatusCode();
                List<TrendNoteViewModel> StatusNoteList = responseNote.Content.ReadAsAsync<List<TrendNoteViewModel>>().Result;

                if (actionName.Equals("Submitted"))
                {
                    trend.TrendStatusName = "Submitted";
                    HttpResponseMessage Noteresponse = _serviceRepository.GetResponse("TrendNote", trend.Id.ToString(), "Submitted");
                    Noteresponse.EnsureSuccessStatusCode();
                    List<TrendNoteViewModel> StatusNote = Noteresponse.Content.ReadAsAsync<List<TrendNoteViewModel>>().Result;
                    if(StatusNote.Count<10)
                    {
                        ViewBag.Message = "Please Submit All Form Of Selected Trend";
                        HttpResponseMessage respGet = _serviceRepository.GetByIdResponse(contollerName, trend.Id.ToString());
                        respGet.EnsureSuccessStatusCode();
                        TrendUpdateDbViewModel trends = respGet.Content.ReadAsAsync<TrendUpdateDbViewModel>().Result;
                        ProjectViewModel proj = ProjectDetails.GetProjectDetails(_serviceRepository, trend.ProjectID);
                        if (proj != null)
                        {
                            ViewBag.ProjectId = proj.Id;
                            ViewBag.ProjectName = proj.ProjectName;
                            ViewBag.ProjectCode = proj.ProjectCode;
                            ViewBag.Approver = proj.Approver;
                            ViewBag.ProjectAuthorName = proj.ProjectAuthorName;
                            ViewBag.ProjectCoAuthorName = proj.ProjectCoAuthorName;
                            ViewBag.StatusName = trends.TrendStatusName;
                        }
                        trend.StatusList = StatusNoteList;
                        return View(trend);
                    }

                }
                else if(actionName.Equals("Approve"))
                {
                    trend.TrendStatusName = "Approved";
                    HttpResponseMessage Noteresponse = _serviceRepository.GetResponse("TrendNote", trend.Id.ToString(), "Approved");
                    Noteresponse.EnsureSuccessStatusCode();
                    List<TrendNoteViewModel> StatusNote = Noteresponse.Content.ReadAsAsync<List<TrendNoteViewModel>>().Result;
                    if (StatusNote.Count < 10)
                    {
                        ViewBag.Message = "Please Approve All Form Of Selected Trend ";
                        HttpResponseMessage respGet = _serviceRepository.GetByIdResponse(contollerName, trend.Id.ToString());
                        respGet.EnsureSuccessStatusCode();
                        TrendUpdateDbViewModel trends = respGet.Content.ReadAsAsync<TrendUpdateDbViewModel>().Result;
                        ProjectViewModel proj = ProjectDetails.GetProjectDetails(_serviceRepository, trend.ProjectID);
                        if (proj != null)
                        {
                            ViewBag.ProjectId = proj.Id;
                            ViewBag.ProjectName = proj.ProjectName;
                            ViewBag.ProjectCode = proj.ProjectCode;
                            ViewBag.Approver = proj.Approver;
                            ViewBag.ProjectAuthorName = proj.ProjectAuthorName;
                            ViewBag.ProjectCoAuthorName = proj.ProjectCoAuthorName;
                            ViewBag.StatusName = trends.TrendStatusName;
                        }
                        trend.StatusList = StatusNoteList;
                        return View(trend);
                    }
                }
                else if(actionName.Equals("Rejected"))
                {
                    trend.TrendStatusName = "Rejected";
                }
                else if (actionName.Equals("Next"))
                {
                    return RedirectToAction("GetTrendKeywords", "TrendKeyWordMap", new { id = trend.Id.ToString() });
                }
                else if (actionName.Equals("Previous"))
                {
                    return RedirectToAction("Index", "Trend", new { id = trend.ProjectID.ToString() });
                }
                else
                {
                    trend.TrendStatusName = "Draft";
                }
               
                HttpResponseMessage response = _serviceRepository.PutResponse(contollerName, trend);
                ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                if (viewModel != null)
                {
                    ViewBag.Message = viewModel.Message;
                }
                response.EnsureSuccessStatusCode();
                
                HttpResponseMessage responseGet = _serviceRepository.GetByIdResponse(contollerName, trend.Id.ToString());
                responseGet.EnsureSuccessStatusCode();
                TrendUpdateDbViewModel trendModel = responseGet.Content.ReadAsAsync<TrendUpdateDbViewModel>().Result;
                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, trend.ProjectID);
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                    ViewBag.StatusName = trend.TrendStatusName;
                }
                trend.StatusList = StatusNoteList;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(trend);
        }

        /// <summary>
        /// Method to validate Form
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private bool validate(TrendInsertDbViewModel model)
        {
            if (model.TrendName == null || model.TrendName == "" || model.TrendName == string.Empty)
            {
                ViewBag.Message = "Please enter Trend Name!!";
                return false;
            }
            if (model.AuthorRemark == null || model.AuthorRemark == "" || model.AuthorRemark == string.Empty)
            {
                ViewBag.Message = "Please enter Author Remark!!";
                return false;
            }
            else
            {
                ViewBag.Message = string.Empty;
                return true;
            }
        }
    }
}