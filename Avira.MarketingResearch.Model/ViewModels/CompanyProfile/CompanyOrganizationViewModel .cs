﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.CompanyProfile
{
    /// <summary>
    /// Using CompanyKeyEmployeesMap to get Employee parent child relationship
    /// </summary>
    public class CompanyOrganizationViewModel
    {
        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
        public Guid ProjectId { get; set; }
        public string CompanyName { get; set; }  
        public string KeyEmployeeName { get; set; }
        public Guid DesignationId { get; set; }
        public string Designation { get; set; }
        public int IsDirector { get; set; } 
        public Guid ManagerId { get; set; }
        public string ManagerName { get; set; }
        public bool IsVisible { get; set; }
        public string ManagetName { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; } 
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
    }

    public class GetAllEmployeeDetails
    {
        List<CompanyOrganizationViewModel> CompanyOrganizationViewModel { get; set; } 
    }
}
