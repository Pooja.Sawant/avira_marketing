﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;

namespace Avira.MarketingResearch.Common.IService.Masters
{
    public interface ICompanyGroupService
    {
        IEnumerable<CompanyGroupViewModel> GetAll(Guid Id);
        IEnumerable<CompanyGroupViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false);
        CompanyGroupViewModel GetById(Guid guid);

        Result<SpTransactionMessage> Create(CompanyGroupInsertDbViewModel tfc, AviraUser currentUser);
        Result<SpTransactionMessage> Update(CompanyGroupUpdateDbViewModel tfc, AviraUser currenUser);

    }
}
