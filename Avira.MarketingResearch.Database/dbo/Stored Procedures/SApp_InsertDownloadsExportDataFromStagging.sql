﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SApp_InsertDownloadsExportDataFromStagging] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)  
declare @ProjectId UNIQUEIDENTIFIER   
 BEGIN TRY
 DELETE FROM [DownloadsExportData] WHERE ProjectId=(SELECT Distinct ProjectId From StagingDownloadsExportData)

INSERT INTO [dbo].[DownloadsExportData]
           ([Id]
           ,[ProjectId]
           ,[ModuleTypeId]
           ,[Header]
           ,[Footer]
           ,[ReportTitle]
           ,[CoverPageImageLink]
		   ,[CoverPageImageName]
           ,[Overview]
           ,[KeyPoints]
           ,[Methodology]
           ,[MethodologyImageLink]
		   ,[MethodologyImageName]
           ,[ContactUsAnalystName]
           ,[ContactUsOfficeContactNo]
           ,[ContactUsAnalystEmailId]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
  Select newid(),
          SDED.ProjectId,
		  SDED.ModuleTypeId,
		  SDED.Header,
		  SDED.Footer,
		  SDED.ReportTitle,
		  SDED.CoverPageImageLink,
		  SDED.CoverPageImageName,
          SDED.Overview,
		  SDED.KeyPoints,
		  SDED.Methodology,
		  SDED.MethodologyImageLink,
		  SDED.MethodologyImageName,
		  SDED.ContactUsAnalystName,
          SDED.ContactUsOfficeContactNo,
          SDED.ContactUsAnalystEmailId,
		  GetDate(),
		  SDED.UserCreatedById,
		  0		  
		 From StagingDownloadsExportData SDED

END TRY
BEGIN CATCH      
  exec SApp_LogErrorInfo
 set @ErrorMsg = 'Error while insert a DownloadExportData, please contact Admin for more details.';    
SELECT StoredProcedureName ='SApp_InsertDownloadsExportDataFromStagging',Message =@ErrorMsg,Success = 0;  
END CATCH   		 	  
END