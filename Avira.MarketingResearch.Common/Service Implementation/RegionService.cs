﻿using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;

namespace Avira.MarketingResearch.Common.Service_Implementation
{
    public class RegionService : IRegionService
    {
        private readonly IRegionRepository _regionRepository;

        public RegionService(IRegionRepository buildLevelRepository)
        {
            _regionRepository = buildLevelRepository;
        }

        public Result<SpTransactionMessage> Create(RegionInsertDbViewModel tfc, AviraUser currentUser)
        {
            return _regionRepository.Create(tfc);
        }

        public IEnumerable<RegionViewModel> GetAll(Guid Id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<RegionViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false)
        {
            return _regionRepository.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude);
        }

        public RegionViewModel GetById(Guid guid)
        {
            return _regionRepository.GetById(guid);
        }

        public Result<SpTransactionMessage> Update(RegionUpdateDbViewModel tfc, AviraUser currenUser)
        {
            return _regionRepository.Update(tfc);
        }

        public IEnumerable<RegionCountryModel> GetRegionByTrendID(Guid trendId)
        {
            return _regionRepository.GetRegionByTrendID(trendId);
        }
    }
}
