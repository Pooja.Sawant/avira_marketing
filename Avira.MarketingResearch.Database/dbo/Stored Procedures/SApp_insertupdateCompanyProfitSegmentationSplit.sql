﻿          
          
CREATE procedure [dbo].[SApp_insertupdateCompanyProfitSegmentationSplit]        
(              
@SegmentationList dbo.udtGUIDSegmentationSplitMultiValue READONLY,       
@CompanyProfitId uniqueidentifier,     
@CompanyId uniqueidentifier,      
@Split int,      
@Year int,    
@UserCreatedById uniqueidentifier           
--@CreatedOn Datetime          
)              
as              
/*================================================================================              
Procedure Name: SApp_insertupdateCompanyProfitSegmentationSplit        
Change History              
Date  Developer  Reason              
__________ ____________ ______________________________________________________              
05/06/2019 Gopi   Initial Version              
================================================================================*/              
BEGIN              
SET NOCOUNT ON;              
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;            
declare @ErrorMsg NVARCHAR(2048)          
        
BEGIN TRY                 
                 
--DELETE FROM [DBO].[CompanyProfitSegmentationSplitMap]       
--WHERE [CompanyProfitId] = @CompanyProfitId    
--AND NOT EXISTS(SELECT 1 FROM @SegmentationList LIST     
--             WHERE LIST.Segmentation = [CompanyProfitSegmentationSplitMap].[Segmentation])    
DECLARE @UnitId UNIQUEIDENTIFIER;

SELECT @UnitId = UnitId
FROM CompanyProfit  
WHERE id = @CompanyProfitId;
  
  DELETE FROM [DBO].[CompanyProfitSegmentationSplitMap]       
  WHERE [CompanyId] = @CompanyId      
  
        
INSERT INTO [dbo].[CompanyProfitSegmentationSplitMap]([Id],      
  [CompanyProfitId],      
  [CompanyId],   
  [Split],          
  [Segmentation],    
  [SegmentValue],    
  [Year],    
        [IsDeleted],              
        [UserCreatedById],              
        [CreatedOn])              
 SELECT NEWID(),       
 @CompanyProfitId,     
 @CompanyId,    
 @Split,          
   Segmentation,
   dbo.fun_UnitConversion(@UnitId,SegmentValue) as SegmentValue,
   @Year,    
   0,              
   @UserCreatedById,               
   GETDATE()        
   from @SegmentationList list        
   where not exists (select 1 from [CompanyProfitSegmentationSplitMap]         
    where list.Segmentation = [CompanyProfitSegmentationSplitMap].[Segmentation])        
        
SELECT StoredProcedureName ='SApp_insertupdateCompanyProfitSegmentationSplit',Message =@ErrorMsg,Success = 1;               
            
END TRY              
BEGIN CATCH              
    -- Execute the error retrieval routine.              
 DECLARE @ErrorNumber int;              
 DECLARE @ErrorSeverity int;              
 DECLARE @ErrorProcedure varchar(100);              
 DECLARE @ErrorLine int;              
 DECLARE @ErrorMessage varchar(500);              
              
  SELECT @ErrorNumber = ERROR_NUMBER(),              
        @ErrorSeverity = ERROR_SEVERITY(),              
        @ErrorProcedure = ERROR_PROCEDURE(),              
        @ErrorLine = ERROR_LINE(),              
        @ErrorMessage = ERROR_MESSAGE()              
              
 insert into dbo.Errorlog(ErrorNumber,              
       ErrorSeverity,              
       ErrorProcedure,              
       ErrorLine,              
       ErrorMessage,              
       ErrorDate)              
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());              
            
 set @ErrorMsg = 'Error while creating a new CompanyProfitSegmentationSplit, please contact Admin for more details.';              
          
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine            
    ,Message =@ErrorMsg,Success = 0;               
               
END CATCH              
              
              
end