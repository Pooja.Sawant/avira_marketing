﻿CREATE TABLE [dbo].[CompanyTransactions] (
    [ID]                 UNIQUEIDENTIFIER NOT NULL,
    [CompanyFinancialID] UNIQUEIDENTIFIER NOT NULL,
    [CategoryId]         UNIQUEIDENTIFIER NULL,
    [Value]              DECIMAL (18, 4)  NULL,
    [OtherParty]         NVARCHAR (200)   NULL,
    [Date]               DATETIME         NULL,
    [Rationale]          NVARCHAR (MAX)   NULL,
    [CreatedOn]          DATETIME         NULL,
    [UserCreatedById]    UNIQUEIDENTIFIER NULL,
    [ModifiedOn]         DATETIME         NULL,
    [UserModifiedById]   UNIQUEIDENTIFIER NULL,
    [IsDeleted]          BIT              NULL,
    [DeletedOn]          DATETIME         NULL,
    [UserDeletedById]    UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CompanyTransactions] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_CompanyTransactions_Category] FOREIGN KEY ([CategoryId]) REFERENCES [dbo].[Category] ([Id]),
    CONSTRAINT [FK_CompanyTransactions_CompanyFinancialAnalysis] FOREIGN KEY ([CompanyFinancialID]) REFERENCES [dbo].[CompanyFinancialAnalysis] ([ID])
);

