﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : AviraBaseController
    {
        public readonly IUserService _userService;
        private ILoggerManager _logger;
        public UserController(
            IUserService userService, ILoggerManager logger)
        {
            _userService = userService;
            _logger = logger;
        }

        //UserRegistration//public ActionResult Post([FromBody]AviraUserInsertDbViewModel userViewModel)
        [HttpPost]
        public ActionResult Post([FromBody]AviraUserInsertDbViewModel userViewModel)
        {
            userViewModel.CreatedOn = System.DateTime.UtcNow;
            userViewModel.UserCreatedById = CurrentUser.Id;
            userViewModel.AviraUserID = Guid.NewGuid();
            userViewModel.salt = "5";

            byte[] array = Encoding.ASCII.GetBytes("5");
            userViewModel.PassWord = PasswordHash.ComputeHash(userViewModel.PassWord, "SHA512", array);

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new User, please contact Admin for more details."
            };
            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                var result = _userService.Create(userViewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "User " + userViewModel.Username + " is created successfully.";
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Ok(viewData); ;
        }

        [HttpGet]
        [Route("GetAllRoles")]
        public ActionResult<IEnumerable<RoleViewModel>> GetAllRoles()
        {
            List<RoleViewModel> res = null;
            try
            {
                res = _userService.GetAllRoles().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet]
        [Route("GetAllLocations")]
        public ActionResult<IEnumerable<LocationViewModel>> GetAllLocations()
        {
            List<LocationViewModel> res = null;
            try
            {
                res = _userService.GetAllLocation().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet]
        [Route("GetAllUserByRole")]
        public ActionResult<IEnumerable<AviraUser>> GetAllUserByRole(string roleName)
        {
            List<AviraUser> res = null;
            try
            {
                res = _userService.GetAllUsersByRole(roleName).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet]
        public ActionResult<IEnumerable<GetAllAviraUsers>> Get(Guid? Id, int PageStart = 0, int pageSize = 0, int SortDirection = 1, string OrdbyByColumnName = "CreatedDate", string Search = "", bool isDeletedInclude = false)
        {
            List<GetAllAviraUsers> res = null;
            try
            {
                res = _userService.GetAll(Id, PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        //[HttpGet("id")]
        public ActionResult<PermissionsByUserRoleIdViewModel> GetPermissionsByUserRoleId(Guid id)
        {
            PermissionsByUserRoleIdViewModel res = null;
            try
            {
                res = _userService.GetPermissionsByUserRoleId(id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }
    }
}