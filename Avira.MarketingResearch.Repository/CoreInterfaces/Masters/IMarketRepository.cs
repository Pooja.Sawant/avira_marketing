﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.Masters
{
    public interface IMarketRepository
    {
        MarketViewModel GetById(Guid guid);
        IEnumerable<MarketViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted);
        Result<SpTransactionMessage> Create(MarketInsertDbViewModel viewModel);
        Result<SpTransactionMessage> Update(MarketUpdateDbViewModel viewModel);
        IEnumerable<MarketViewModel> GetAllParentMarket();
    }
}
