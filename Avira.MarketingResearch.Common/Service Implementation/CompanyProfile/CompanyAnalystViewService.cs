﻿using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.CompanyProfile
{
    public class CompanyAnalystViewService : ICompanyAnalystViewService
    {
        private readonly ICompanyAnalystViewRepository _companyAnalystViewRepository;
        private readonly ICompanyNoteRepository _iCompanyNoteRepository;

        public CompanyAnalystViewService(ICompanyAnalystViewRepository companyAnalystViewRepository, ICompanyNoteRepository iCompanyNoteRepository)
        {
            _companyAnalystViewRepository = companyAnalystViewRepository;
            _iCompanyNoteRepository = iCompanyNoteRepository;
        }

        public CompanyAnalystViewModel GetById(Guid companyId, Guid projectId)
        {
            return _companyAnalystViewRepository.GetById(companyId,projectId);
        }

        public Result<SpTransactionMessage> InsertUpdate(MainCompanyAnalystViewModel tfc, AviraUser currenUser)
        {

            var res = _companyAnalystViewRepository.InsertUpdate(tfc);
            tfc.CompanyNoteViewModel.Id = Guid.NewGuid();
            tfc.CompanyNoteViewModel.UserCreatedById = tfc.UserCreatedById;
            tfc.CompanyNoteViewModel.CompanyId = tfc.CompanyId;
            tfc.CompanyNoteViewModel.ProjectId = tfc.ProjectId;
            _iCompanyNoteRepository.Insert(tfc.CompanyNoteViewModel);

            return res;
        }
    }
}
