﻿CREATE TABLE [dbo].[MarketValuesummary_dup] (
    [Projectid]    UNIQUEIDENTIFIER NULL,
    [Regionid]     UNIQUEIDENTIFIER NOT NULL,
    [Countryid]    UNIQUEIDENTIFIER NULL,
    [SegmentId]    UNIQUEIDENTIFIER NOT NULL,
    [SubSegmentId] UNIQUEIDENTIFIER NOT NULL,
    [Year]         INT              NOT NULL,
    [Volume]       DECIMAL (38, 4)  NULL,
    [value]        DECIMAL (38, 4)  NULL
);

