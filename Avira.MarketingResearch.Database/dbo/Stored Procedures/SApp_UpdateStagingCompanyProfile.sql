﻿CREATE procedure [dbo].[SApp_UpdateStagingCompanyProfile]        
        
as        
/*================================================================================        
Procedure Name: SApp_UpdateStagingMarketSize1       
Author: Gopi        
Create date: 05/28/2019        
Description: to update RegionId,ServiceId,RawMaterialId,ApplicationId,IndustryId        
Change History        
Date  Developer  Reason        
__________ ____________ ______________________________________________________        
05/28/2019 Gopi   Initial Version        
================================================================================*/        
BEGIN        
SET NOCOUNT ON;        
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;        
declare @ErrorMsg NVARCHAR(2048)          
        
BEGIN TRY         
        
  ;With cteComp as (           --- ctereg   
 select SM.CompanyName as CompanyName,     
    CASE 
	 WHEN ISNULL(SM.CompanyName, '') = '' THEN 'No CompanyName'
     WHEN C.Id is NOT null THEN 'CompanyName already exists'        
     ELSE Null        
     END As CompanyNameError,        
     SM.Id as StaggingId          
 from  StagingCompanyProfile SM        
 left join Company C        
 on C.CompanyName = SM.CompanyName        
  )        
  ,cteNature AS(           ----- cteProcess
   select N.Id as Nature,        
   CASE         
     WHEN N.Id is null THEN 'No Nature'        
     ELSE Null        
     END As NatureError,        
    SM.Id as StaggingId        
   from  StagingCompanyProfile SM        
  left join Nature N        
  on N.NatureTypeName = SM.Nature         
  )        
  --,cteSubsdiaryName AS(                                 -----cteRaw 
  -- select SUBNAME.Id as CompanySubsdiaryName,        
  -- CASE         
  --   WHEN SUBNAME.Id is null THEN 'No CompanySubsdiaryName'        
  --   ELSE Null        
  --   END As CompanySubsdiaryNameError,        
  --        SM.Id as StaggingId        
  --  from  StagingCompanyProfile SM        
  -- left join Company SUBNAME        
  -- on SUBNAME.CompanyName = SM.CompanySubsdiaryName        
  --)        
  --,cteSubsdiaryLocation AS(                             ------------cteApp
  -- select SL.Id as CompanySubsdiaryLocation,        
  -- CASE         
  --   WHEN SL.Id is null THEN 'No CompanySubsdiaryLocation'        
  --   ELSE Null        
  --   END As CompanySubsdiaryLocationError,        
  --        SM.Id as StaggingId        
  --  from  StagingCompanyProfile SM        
  -- left join Company SL        
  -- on SL.CompanyLocation = SM.CompanySubsdiaryLocation        
  --)        
  --,cteParent AS(      ------cteInd    
  -- select Pid.Id as CompanyParentName,        
  -- CASE         
  --   WHEN Pid.id is null THEN 'No CompanyParentName'        
  --   ELSE Null        
  --   END As CompanyParentNameError,        
  --        SM.Id as StaggingId        
  --  from  StagingCompanyProfile SM        
  -- left join Company Pid        
  -- on Pid.ParentCompanyName = SM.CompanyParentName
  --)        
  ,cteHQ AS(									---------cteProj
  select SM.HQ as HQ,  
  CASE   
   WHEN ISNULL(SM.HQ,'') = '' THEN 'No HQ'    
     ELSE Null        
     END As HQError,  
     SM.Id as StaggingId        
    from  StagingCompanyProfile SM        
   --left join Company CompHQ        
   --on CompHQ.CompanyName = SM.HQ
   --Where CompHQ.IsHeadQuarters =1 AND ISNULL(SM.HQ, '') != ''
   )  
  -- ,ctePhone1 AS(  
  --select ph.Id as PhoneNumber1,  
  --CASE   
  -- WHEN ph.Id is null THEN 'No PhoneNumber1'    
  --   ELSE Null        
  --   END As PhoneNumber1Error,  
  --   SM.Id as StaggingId        
  --  from  StagingCompanyProfile SM        
  -- left join Company ph        
  -- on ph.Telephone = SM.PhoneNumber1      
  -- ) 
  --   ,ctePhone2 AS(  
  --select ph.Id as PhoneNumber2,  
  --CASE   
  -- WHEN ph.Id is null THEN 'No PhoneNumber2'    
  --   ELSE Null        
  --   END As PhoneNumber2Error,  
  --   SM.Id as StaggingId        
  --  from  StagingCompanyProfile SM        
  -- left join Company ph        
  -- on ph.Telephone2 = SM.PhoneNumber2      
  -- ) 
  --,ctePhone3 AS(  
  --select ph.Id as PhoneNumber3,  
  --CASE   
  -- WHEN ph.Id is null THEN 'No PhoneNumber3'    
  --   ELSE Null        
  --   END As PhoneNumber3Error,  
  --   SM.Id as StaggingId        
  --  from  StagingCompanyProfile SM        
  -- left join Company ph        
  -- on ph.Telephone3 = SM.PhoneNumber3      
  -- ) 
    
  -- ,cteMail1 AS(  
  --select M.Id as Mail1,  
  --CASE   
  -- WHEN M.Id is null THEN 'No Mail1'    
  --   ELSE Null        
  --   END As Mail1Error,  
  --   SM.Id as StaggingId        
  --  from  StagingCompanyProfile SM        
  -- left join Company M        
  -- on M.Email = SM.Mail1      
  -- ) 
  -- ,cteMail2 AS(  
  --select M2.Id as Mail2,  
  --CASE   
  -- WHEN M2.Id is null THEN 'No Mail2'    
  --   ELSE Null        
  --   END As Mail2Error,  
  --   SM.Id as StaggingId        
  --  from  StagingCompanyProfile SM        
  -- left join Company M2        
  -- on M2.Email2 = SM.Mail2      
  -- )  
  --     ,cteMail3 AS(  
  --select M3.Id as Mail3,  
  --CASE   
  -- WHEN M3.Id is null THEN 'No Mail3'    
  --   ELSE Null        
  --   END As Mail3Error,  
  --   SM.Id as StaggingId        
  --  from  StagingCompanyProfile SM        
  -- left join Company M3        
  -- on M3.Email3 = SM.Mail3      
  -- )  
   ,cteWebsite AS(  
  select SM.Website as Website,  
  CASE   
   WHEN ISNULL(SM.Website, '') = '' THEN 'No Website'    
     ELSE Null        
     END As WebsiteError,  
     SM.Id as StaggingId        
    from  StagingCompanyProfile SM        
   --left join Company WEB        
   --on WEB.Website = SM.Website      
   ) 
    ,cteYearOfEstb AS(  
  select SM.FoundedYear as FoundedYear,  
  CASE   
   WHEN ISNULL(SM.FoundedYear, '') = '' THEN 'No FoundedYear'    
     ELSE Null        
     END As FoundedYearError,  
     SM.Id as StaggingId        
    from  StagingCompanyProfile SM        
   --left join Company Estb        
   --on Estb.YearOfEstb = SM.FoundedYear      
   ) 
   ,cteCompanyStage AS(  
  select cpstg.Id as CompanyStage,  
  CASE   
   WHEN cpstg.Id is null THEN 'No CompanyStage'    
     ELSE Null        
     END As CompanyStageError,  
     SM.Id as StaggingId        
    from  StagingCompanyProfile SM        
   left join CompanyStage cpstg        
   on cpstg.CompanyStageName = SM.CompanyStage      
   ) 
    ,cteCmpRnkNO AS(  
  select SM.CompanyRank_Rank  as CompanyRank_Rank,  
  CASE   
   WHEN ISNULL(SM.CompanyRank_Rank,'') = '' OR ISNULL(SM.CompanyRank_Rank,'0') = '0' THEN 'No CompanyRank_Rank'    
     ELSE Null        
     END As CompanyRank_RankError,  
     SM.Id as StaggingId        
    from  StagingCompanyProfile SM        
   --left join Company cmprnk        
   --on cmprnk.RankNumber = SM.CompanyRank_Rank      
   ) 
  -- ,cteCmpRnkRl AS(  
  --select cmprnk.Id as CompanyRank_Rationale,  
  --CASE   
  -- WHEN cmprnk.Id is null THEN 'No CompanyRank_Rationale'    
  --   ELSE Null        
  --   END As CompanyRank_RationaleError,  
  --   SM.Id as StaggingId        
  --  from  StagingCompanyProfile SM        
  -- left join Company cmprnk        
  -- on cmprnk.RankRationale = SM.CompanyRank_Rationale      
  -- ) 
     ,cteCmpPrd AS(  
  select SM.Product_ServiceName as Product_ServiceName,  
  CASE   
   WHEN ISNULL(SM.Product_ServiceName,'') = '' THEN 'No Product_ServiceName'    
     ELSE Null        
     END As Product_ServiceNameError,  
     SM.Id as StaggingId        
    from  StagingCompanyProfile SM        
   --left join CompanyProduct cmprd        
   --on cmprd.ProductName = SM.Product_ServiceName      
   )
  
   ,cteCmpld AS(  
  select CONVERT(datetime,SM.LaunchDate,103) as LaunchDate,  
  CASE   
   WHEN ISDATE(CONVERT(datetime,SM.LaunchDate,103)) = 0 THEN 'No LaunchDate'    
     ELSE Null        
     END As LaunchDateError,  
     SM.Id as StaggingId        
    from  StagingCompanyProfile SM        
   --left join CompanyProduct cmpLD        
   --on cmpLD.LaunchDate = CONVERT(datetime,SM.LaunchDate,103)      
   ),
   cteprdcg AS(  
  select pdctg.Id as ProductCategory,  
  CASE   
   WHEN pdctg.Id is null THEN 'No ProductCategory'    
     ELSE Null        
     END As ProductCategoryError,  
     SM.Id as StaggingId        
    from  StagingCompanyProfile SM        
   left join ProductCategory pdctg        
   on pdctg.ProductCategoryName = SM.ProductCategory      
   ),
   cteseg AS(  
  select seg.Id as Segment,  
  CASE   
   WHEN seg.Id is null THEN 'No Segment'    
     ELSE Null        
     END As SegmentError,  
     SM.Id as StaggingId        
    from  StagingCompanyProfile SM        
   left join Segment seg        
   on seg.SegmentName = SM.Segment      
   ),
   cteTotRev AS(  
  select SM.TotalRevenue as TotalRevenue,  
  CASE   
   WHEN ISNULL(SM.TotalRevenue,'') = '' THEN 'No TotalRevenue'    
     ELSE Null        
     END As TotalRevenueError,  
     SM.Id as StaggingId        
    from  StagingCompanyProfile SM        
   --left join CompanyRevenue cmpRev        
   --on cmpRev.TotalRevenue = SM.TotalRevenue      
   ),
    cteUnit AS(  
  select un.Id as Unit,  
  CASE   
   WHEN un.Id is null THEN 'No Currency'    
     ELSE Null        
     END As UnitError,  
     SM.Id as StaggingId        
    from  StagingCompanyProfile SM        
   left join Currency un        
   on un.CurrencyName = SM.Currency      
   ) ,
    cteCurrency AS(  
  select curr.Id as Currency,  
  CASE   
   WHEN curr.Id is null THEN 'No CurrencyId'    
     ELSE Null        
     END As CurrencyIdError,  
     SM.Id as StaggingId        
    from  StagingCompanyProfile SM        
   left join Currency curr        
   on curr.CurrencyName = SM.Currency1      
   ),
  --  ctePatents AS(  
  --select cmpprd.Id as Patents,  
  --CASE   
  -- WHEN cmpprd.Id is null THEN 'No Patents'    
  --   ELSE Null        
  --   END As PatentsError,  
  --   SM.Id as StaggingId        
  --  from  StagingCompanyProfile SM        
  -- left join CompanyProduct cmpprd        
  -- on cmpprd.Patents = SM.Patents      
  -- ),
  --  ctePrddec AS(  
  --select cmpprdrmk.Id as Decription_Remarks,  
  --CASE   
  -- WHEN cmpprdrmk.Id is null THEN 'No Decription_Remarks'    
  --   ELSE Null        
  --   END As Decription_RemarksError,  
  --   SM.Id as StaggingId        
  --  from  StagingCompanyProfile SM        
  -- left join CompanyProduct cmpprdrmk        
  -- on cmpprdrmk.description = SM.Decription_Remarks      
  -- ),
cteESMPresence AS(  
  select ESMPresence.Id as ESMPresence,  
  CASE   
   WHEN ESMPresence.Id is null THEN 'No Eco System Presence'    
     ELSE Null        
     END As ESMPresenceError,  
     SM.Id as StaggingId        
    from  StagingCompanyProfile SM        
   left join Segment ESMPresence        
   on ESMPresence.SegmentName = SM.ESMPresence
   left join CompanyEcosystemPresenceESMMap ESMMap        
   on ESMMap.ESMId = ESMPresence.Id   
      inner join Company on Company.Id = ESMMap.CompanyId
   WHERE Company.CompanyName = SM.CompanyName  
   ),
cteSectorPresence AS(  
  select SecPresence.Id as SectorPresence,  
  CASE   
   WHEN SecPresence.Id is null THEN 'No Sector Presence'    
     ELSE Null        
     END As SectorPresenceError,  
     SM.Id as StaggingId        
    from  StagingCompanyProfile SM        
   left join Segment SecPresence        
   on SecPresence.SegmentName = SM.ESMPresence
   left join CompanyEcosystemPresenceSectorMap secMap        
   on secMap.Id = SecPresence.Id   
    inner join Company on Company.Id = secMap.CompanyId
   WHERE Company.CompanyName = SM.CompanyName    
   )
	                                 
        
  update StagingCompanyProfile         
 set

 CompanyName = cteComp.CompanyName,        
   Nature = cteNature.Nature,        
   --CompanySubsdiaryName = cteSubsdiaryName.CompanySubsdiaryName,        
   --CompanySubsdiaryLocation = cteSubsdiaryLocation.CompanySubsdiaryLocation,        
   --CompanyParentName = cteParent.CompanyParentName,   
   HQ = cteHQ.HQ,   
  -- PhoneNumber1 = ctePhone1.PhoneNumber1,  
  -- PhoneNumber2 = ctePhone2.PhoneNumber2,  
  -- PhoneNumber3 = ctePhone3.PhoneNumber3,
  -- Mail1 = cteMail1.Mail1,
  --  Mail2 = cteMail2.Mail2,
	 --Mail3 = cteMail3.Mail3,
     Website = cteWebsite.Website,
	 FoundedYear =cteYearOfEstb.FoundedYear,
	 CompanyStage =cteCompanyStage.CompanyStage,
	 CompanyRank_Rank = cteCmpRnkNO.CompanyRank_Rank,
--CompanyRank_Rationale= cteCmpRnkRl.CompanyRank_Rationale,
Product_ServiceName =cteCmpPrd.Product_ServiceName,
LaunchDate= cteCmpld.LaunchDate,
ProductCategory = cteprdcg.ProductCategory,
 Segment= cteseg.Segment,
 TotalRevenue=cteTotRev.TotalRevenue,
Unit =cteUnit.Unit,
Currency=cteCurrency.Currency,
--Patents=ctePatents.Patents,
--Decription_Remarks=ctePrddec.Decription_Remarks,
ESMPresence = cteESMPresence.ESMPresence,
SectorPresence = cteSectorPresence.SectorPresence,

   ErrorNotes = CONCAT_WS(', ', cteComp.CompanyNameError,cteNature.NatureError,
   --cteSubsdiaryName.CompanySubsdiaryNameError,cteSubsdiaryLocation.CompanySubsdiaryLocationError,cteParent.CompanyParentNameError,
cteHQ.HQError,
--ctePhone1.PhoneNumber1Error,ctePhone2.PhoneNumber2Error,ctePhone3.PhoneNumber3Error,cteMail1.Mail1Error,cteMail2.Mail2Error,cteMail3.Mail3Error,
cteWebsite.WebsiteError,
cteYearOfEstb.FoundedYearError,cteCompanyStage.CompanyStageError,cteCmpRnkNO.CompanyRank_RankError,
--cteCmpRnkRl.CompanyRank_RationaleError,
cteCmpPrd.Product_ServiceNameError,cteCmpld.LaunchDateError,
cteprdcg.ProductCategoryError,cteseg.SegmentError,cteTotRev.TotalRevenueError,cteUnit.UnitError,cteCurrency.CurrencyIdError,
--ctePatents.PatentsError,ctePrddec.Decription_RemarksError,
cteESMPresence.ESMPresenceError,cteSectorPresence.SectorPresence)      
  From cteComp        
  Inner join StagingCompanyProfile sm        
  On cteComp.StaggingId = sm.Id
  Inner Join cteNature         
  On cteNature.StaggingId = sm.Id        
  --Inner Join cteSubsdiaryName         
  --On cteSubsdiaryName.StaggingId = sm.Id        
  --Inner Join cteSubsdiaryLocation         
  --On cteSubsdiaryLocation.StaggingId = sm.Id        
  --Inner Join cteParent        
  --On cteParent.StaggingId = sm.Id        
  Inner Join cteHQ        
  On cteHQ.StaggingId = sm.Id     
  --Inner Join ctePhone1  
  --On ctePhone1.StaggingId = sm.Id  
  --Inner Join ctePhone2  
  --On ctePhone2.StaggingId = sm.Id  
  --Inner Join ctePhone3    
  --On ctePhone3.StaggingId = sm.Id  
  --Inner Join cteMail1  
  --On cteMail1.StaggingId = sm.Id
  --  Inner Join cteMail2  
  --On cteMail2.StaggingId = sm.Id
  --  Inner Join cteMail3  
  --On cteMail3.StaggingId = sm.Id
    Inner Join cteWebsite  
  On cteWebsite.StaggingId = sm.Id
     Inner Join cteYearOfEstb  
  On cteYearOfEstb.StaggingId = sm.Id
      Inner Join cteCompanyStage  
  On cteCompanyStage.StaggingId = sm.Id
    Inner Join cteCmpRnkNO  
  On cteCmpRnkNO.StaggingId = sm.Id
  --   Inner Join cteCmpRnkRl  
  --On cteCmpRnkRl.StaggingId = sm.Id
  Inner Join cteCmpPrd  
  On cteCmpPrd.StaggingId = sm.Id
    Inner Join cteCmpld  
  On cteCmpld.StaggingId = sm.Id
    Inner Join cteprdcg  
  On cteprdcg.StaggingId = sm.Id
  Inner Join cteseg  
  On cteseg.StaggingId = sm.Id
  Inner Join cteTotRev  
  On cteTotRev.StaggingId = sm.Id
   Inner Join cteUnit  
  On cteUnit.StaggingId = sm.Id
   Inner Join cteCurrency  
  On cteCurrency.StaggingId = sm.Id
  --Inner Join ctePatents  
  --On ctePatents.StaggingId = sm.Id
  -- Inner Join ctePrddec  
  --On ctePrddec.StaggingId = sm.Id
   Inner Join cteESMPresence  
  On cteESMPresence.StaggingId = sm.Id
     Inner Join cteSectorPresence  
  On cteSectorPresence.StaggingId = sm.Id

Declare @Notes int;    
set @Notes = (Select COUNT(*) from StagingCompanyProfile where ErrorNotes != '' and ErrorNotes is not null )  

IF(@Notes > 0)    
BEGIN   
   Select *, Message = @ErrorMsg, Success = 1, StoredProcedureName ='SApp_UpdateStagingCompanyProfile' from StagingCompanyProfile;    
   Delete from ImportData where Id in (select Distinct ImportId from StagingCompanyProfile sm);    
   DELETE from StagingCompanyProfile;     
END  
ELSE  
Begin
	--SELECT StoredProcedureName ='SApp_UpdateStagingTrend',Message =@ErrorMsg,Success = 1;
	Exec [dbo].[SApp_insertAllCompanyProfileFromStagging];
	--Delete from ImportData where Id in (select Distinct ImportId from StagingCompanyProfile sm); 
 --   DELETE from StagingCompanyProfile;
End 
END TRY              
BEGIN CATCH              
    -- Execute the error retrieval routine.              
 DECLARE @ErrorNumber int;              
 DECLARE @ErrorSeverity int;              
 DECLARE @ErrorProcedure varchar(100);              
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);              
              
  SELECT @ErrorNumber = ERROR_NUMBER(),              
        @ErrorSeverity = ERROR_SEVERITY(),              
        @ErrorProcedure = ERROR_PROCEDURE(),              
        @ErrorLine = ERROR_LINE(),              
        @ErrorMessage = ERROR_MESSAGE()              
              
 insert into dbo.Errorlog(ErrorNumber,              
       ErrorSeverity,              
       ErrorProcedure,              
       ErrorLine,              
       ErrorMessage,              
       ErrorDate)              
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());              
            
 set @ErrorMsg = 'Error while updating StagingCompanyProfile, please contact Admin for more details.';            
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine            
,Message =@ErrorMsg,Success = 0;               
               
END CATCH              
        
        
end