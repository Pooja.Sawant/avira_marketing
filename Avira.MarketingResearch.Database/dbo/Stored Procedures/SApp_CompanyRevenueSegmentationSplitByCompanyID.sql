﻿-- =============================================          
-- Author:  Laxmikant          
-- Create date: 05/29/2019          
-- Description: Get Profit Company total revenue by Company Id    
-- =============================================          
CREATE PROCEDURE [dbo].[SApp_CompanyRevenueSegmentationSplitByCompanyID]               
 @ComapanyID uniqueidentifier = null,    
 @includeDeleted bit = 0               
AS          
BEGIN           
 SET NOCOUNT ON;            
 SELECT   
 [CompanyRevenueSegmentationSplitMap].[Id]    
 ,[CompanyRevenueSegmentationSplitMap].[CompanyRevenueId]    
 ,[CompanyRevenueSegmentationSplitMap].[Split]    
 ,[CompanyRevenueSegmentationSplitMap].[Segmentation]    
 ,[CompanyRevenueSegmentationSplitMap].[SegmentValue]    
 ,[CompanyRevenueSegmentationSplitMap].[Year]    
 ,[CompanyRevenueSegmentationSplitMap].[CreatedOn]    
 ,[CompanyRevenueSegmentationSplitMap].[UserCreatedById]    
 ,[CompanyRevenueSegmentationSplitMap].[ModifiedOn]    
 ,[CompanyRevenueSegmentationSplitMap].[UserModifiedById]    
 ,[CompanyRevenueSegmentationSplitMap].[IsDeleted]    
 ,[CompanyRevenueSegmentationSplitMap].[DeletedOn]    
 ,[CompanyRevenueSegmentationSplitMap].[UserDeletedById]    
  FROM [dbo].[CompanyRevenueSegmentationSplitMap]          
       
  WHERE ([CompanyRevenueSegmentationSplitMap].CompanyId = @ComapanyID) --and [CompanyRevenue].[projectId] = @ProjectID)                
 AND (@includeDeleted = 0 or [CompanyRevenueSegmentationSplitMap].IsDeleted = 1)            
END