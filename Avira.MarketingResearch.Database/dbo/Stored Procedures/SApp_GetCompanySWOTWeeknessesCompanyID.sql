﻿-- =============================================            
-- Author:  Laxmikant            
-- Create date: 05/15/2019            
-- Description: Get Company swot Weeknesses by CompanyId             
-- =============================================            
CREATE PROCEDURE [dbo].[SApp_GetCompanySWOTWeeknessesCompanyID]                 
 @ComapanyID uniqueidentifier = null,             
 @includeDeleted bit = 0                 
AS            
BEGIN             
 SET NOCOUNT ON;              
 SELECT[CompanySwotWeeknessesMap].[Id]            
      ,[CompanySwotWeeknessesMap].[CompanyId]              
      ,[CompanySwotWeeknessesMap].[CategoryName]                      
      ,[CompanySwotWeeknessesMap].[Description]            
      ,[CompanySwotWeeknessesMap].[CreatedOn]            
      ,[CompanySwotWeeknessesMap].[UserCreatedById]            
      ,[CompanySwotWeeknessesMap].[ModifiedOn]            
      ,[CompanySwotWeeknessesMap].[UserModifiedById]            
      ,[CompanySwotWeeknessesMap].[IsDeleted]            
      ,[CompanySwotWeeknessesMap].[DeletedOn]            
      ,[CompanySwotWeeknessesMap].[UserDeletedById]            
  FROM [dbo].[CompanySwotWeeknessesMap]        
  LEFT JOIN [dbo].[Category] Category ON [CompanySwotWeeknessesMap].CategoryName = Category.Id          
  WHERE ([CompanySwotWeeknessesMap].CompanyId = @ComapanyID)                  
 AND (@includeDeleted = 0 or [CompanySwotWeeknessesMap].IsDeleted = 0)              
END