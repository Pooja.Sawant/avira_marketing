﻿
CREATE PROCEDURE [dbo].[SFab_GetCompanyAnalystViewByID]             
 @ComapanyID uniqueidentifier = null,     
  @ProjectID uniqueidentifier = null,  
 @includeDeleted bit = 0,
 @AviraUserID uniqueidentifier = null          
AS      
/*================================================================================  
Procedure Name: [SFab_GetCompanyAnalystViewByID]  
Author: Pooja Sawant
Create date: 07/12/2019  
Description: Get list of AnalystView from table   
Change History  
Date        Developer     Reason  
__________  ____________  ______________________________________________________  
07/12/2019  Pooja Sawant   Initial Version  
================================================================================*/    
BEGIN         
	SET NOCOUNT ON;   
	declare @ProjectApprovedStatusId uniqueidentifier
	
	select @ProjectApprovedStatusId = ID from ProjectStatus
	where ProjectStatusName = 'Completed';

	    
	SELECT CAV.ID, CompanyID, ProjectID, '<div class="cp-analystview-text-wrapper"><div class="overview-bullet analystview-bullet"><i class="fas fa-square"></i></div><div class="analystview-text-container">'+REPLACE(REPLACE(CAV.AnalystView, CHAR(13), ''), CHAR(10), ' </div></div><div class="cp-analystview-text-wrapper"><div class="overview-bullet analystview-bullet"><i class="fas fa-square"></i></div><div class="analystview-text-container">')  + '</div></div>' As AnalystView,
	Category.CategoryName,Importance.GroupName
	FROM CompanyAnalystView CAV
	Left Join Category ON  CAV.CategoryId=Category.Id
	Left Join Importance ON CAV.ImportanceId=Importance.Id
	WHERE CAV.CompanyId = @ComapanyID and CAV.ProjectId=@ProjectID
		and exists (select 1 from project 
					where project.ID = CAV.ProjectID
						and project.ProjectStatusID = @ProjectApprovedStatusId)

		AND CAV.IsDeleted = 0      
		order by AnalystView  
END