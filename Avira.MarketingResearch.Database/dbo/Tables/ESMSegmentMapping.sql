﻿CREATE TABLE [dbo].[ESMSegmentMapping] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [ProjectId]        UNIQUEIDENTIFIER NOT NULL,
    [MarketId]         UNIQUEIDENTIFIER NULL,
    [SegmentId]        UNIQUEIDENTIFIER NOT NULL,
    [SubSegmentId]     UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_ESMSegmentMapping] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ESMSegmentMapping_Project] FOREIGN KEY ([ProjectId]) REFERENCES [dbo].[Project] ([ID]),
    CONSTRAINT [FK_ESMSegmentMapping_Segment] FOREIGN KEY ([SegmentId]) REFERENCES [dbo].[Segment] ([Id]),
    CONSTRAINT [FK_ESMSegmentMapping_SubSegment] FOREIGN KEY ([SubSegmentId]) REFERENCES [dbo].[SubSegment] ([Id]),
    CONSTRAINT [IX_ESMSegmentMapping] UNIQUE NONCLUSTERED ([ProjectId] ASC, [SegmentId] ASC, [SubSegmentId] ASC)
);

