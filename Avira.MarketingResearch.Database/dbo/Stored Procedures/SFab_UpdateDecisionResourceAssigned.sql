﻿CREATE PROCEDURE [dbo].[SFab_UpdateDecisionResourceAssigned]
(
	@BaseDecisionId uniqueidentifier,
	@DecisionId uniqueidentifier,
	@ResourceAssigned nvarchar(512),
	@UserId	uniqueidentifier
)
As   

/*================================================================================    
Procedure Name: SFab_UpdateDecisionResourceAssigned   
Author: Praveen    
Create date: 07-Jan-2020
Description: Update decision ResourceAssigned by user
Change History    
Date  Developer  Reason  
__________ ____________ ______________________________________________________    
07-Jan-2020  Praveen   Initial Version
08-Jan-2020  Harshal   Added Statement in SP    
================================================================================*/    
BEGIN  
Declare @ErrorMsg NVARCHAR(2048);
	BEGIN TRY
		Begin  
		------------------
			--Required Tables: Decision
			--Notes: N/A
			--Case 1: N/A
			--Expected Result :SELECT StoredProcedureName ='SFab_UpdateDecisionResourceAssigned',Message = @ErrorMsg,Success = 1/0;
	 
			If Not Exists(Select Id From Decision Where ID = @DecisionId and UserId = @UserId)
			Begin
		
				Execute SFab_CopyDecisionFromBase @InDecisionId = @BaseDecisionId, @UserId = @UserId, @CopyDecisionParametersFromBase = 0, @OutDecisionId = @DecisionId OUTPUT
			End
			UPDATE Decision SET ResourceAssigned=@ResourceAssigned, ModifiedOn = GetDate(), UserModifiedById = @UserId 
			WHERE Id=@DecisionId and UserId=@UserId
		END
		SELECT StoredProcedureName ='SFab_UpdateDecisionResourceAssigned',Message =@ErrorMsg,Success = 1;
	END TRY
	BEGIN CATCH
		-- Execute the error retrieval routine.
		DECLARE @ErrorNumber	int;
		DECLARE @ErrorSeverity	int;
		DECLARE @ErrorProcedure	varchar(100);
		DECLARE @ErrorLine	int;
		DECLARE @ErrorMessage	varchar(500);

			SELECT @ErrorNumber = ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorProcedure = ERROR_PROCEDURE(),
			@ErrorLine = ERROR_LINE(),
			@ErrorMessage = ERROR_MESSAGE()

		insert into dbo.Errorlog(ErrorNumber,
								ErrorSeverity,
								ErrorProcedure,
								ErrorLine,
								ErrorMessage,
								ErrorDate)
			values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());

		Set @ErrorMsg = 'Error while Update Decision, please contact Admin for more details.';    
 
		SELECT Number = @ErrorNumber, 
			Severity =@ErrorSeverity,
			StoredProcedureName =@ErrorProcedure,
			LineNumber= @ErrorLine,
			Message =@ErrorMsg,
			Success = 0;     
	END CATCH 
END