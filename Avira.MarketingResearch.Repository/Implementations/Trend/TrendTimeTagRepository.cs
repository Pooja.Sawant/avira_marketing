﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.Trend
{
    public class TrendTimeTagRepository : ITrendTimeTagRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "TrendTimeTag";

        public TrendTimeTagRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public IEnumerable<TrendTimeTagViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search = "", bool isDeletedInclude = false, string id = "")
        {
            //var x = new Guid(id);            
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<TrendTimeTagViewModel>(TrendTimeTagQueries.StoreProcSelectAll
                            , new
                            {
                                @TimeTagName = Search,
                                @TrendID = id,
                                //@TrendID = x,
                                @includeDeleted = false,
                                @OrdbyByColumnName = @OrdbyByColumnName,
                                @PageStart = PageStart,
                                @pageSize = pageSize,
                                @SortDirection = SortDirection
                            }
                            , null, true, null, CommandType.StoredProcedure);
            //throw new NotImplementedException();
        }

        public Result<SpTransactionMessage> InsertUpdate(MainTrendTimeTagInsertUpdateDbViewModel trendTimeTagUpdateDbViewModel)
        {
            DataTable trendTimeTagTable = new DataTable();
            trendTimeTagTable.Columns.Add("ID", typeof(Guid));
            trendTimeTagTable.Columns.Add("MapId", typeof(Guid));
            trendTimeTagTable.Columns.Add("bitValue", typeof(bool));
            if (trendTimeTagUpdateDbViewModel.TrendTimeTagInsertUpdate != null & trendTimeTagUpdateDbViewModel.TrendTimeTagInsertUpdate.Count > 0)
            {
                foreach (var item in trendTimeTagUpdateDbViewModel.TrendTimeTagInsertUpdate.Where(x => x.ImpactId != Guid.Empty))
                {
                    trendTimeTagTable.Rows.Add(new Object[] { item.TimeTagId, item.ImpactId, item.EndUser });
                }
            }

            var spTransactionMessage = _dbHelper.ExecuteQuery(TrendTimeTagQueries.StoreProcInsertUpdate,
               new
               {
                   @TrendId = trendTimeTagUpdateDbViewModel.TrendId,
                   @TrendTimeTag = trendTimeTagTable.AsTableValuedParameter("dbo.udtGUIDGUIDMapValue"),
                   @UserCreatedById = trendTimeTagUpdateDbViewModel.UserCreatedById,
               }
                    , CommandType.StoredProcedure);
            return spTransactionMessage;
            //throw new NotImplementedException();
        }
    }
    
    
}
