﻿CREATE procedure [dbo].[SApp_GetAllIndustries]

as
/*================================================================================
Procedure Name: [SApp_GetAllIndustries]
Author: Sai Krishna
Create date: 08/05/2019
Description: Get list from Industry
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
08/05/2019	Sai Krishna		Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @SegmentId uniqueidentifier;
select @SegmentId = Id from Segment
where SegmentName = 'End User';

SELECT [Id], 
		SubSegmentName as [IndustryName]
	FROM [dbo].SubSegment
where (SubSegment.[IsDeleted] = 0) 
and SegmentId = @SegmentId
order by IndustryName 
end