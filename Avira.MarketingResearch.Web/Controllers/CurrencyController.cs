﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.Models;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Newtonsoft.Json;

namespace Avira.MarketingResearch.Web.Controllers
{
    [NoCache]
    [Authorization]
    public class CurrencyController : BaseController
    {
        private ILoggerManager _logger;
        public readonly IServiceRepository _serviceRepository;
        public CurrencyController(IServiceRepository serviceRepository, ILoggerManager logger)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
        }

        //[Route("Currencies/Index")]
        public IActionResult Index()
        {
            return View();
        }

        public ActionResult LoadData(CurrencyViewModel currency)
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            int totalRecords = 0;
            List<CurrencyViewModel> data = null;
            try
            {
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                string search = Request.Form["search[value]"];

                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDir = Request.Form["order[0][dir]"].FirstOrDefault();
                var SortDirection = -1;
                if (sortColumnDir == "asc")
                {
                    SortDirection = 1;
                }
                else
                {
                    SortDirection = -1;
                }

                int skip = start != null ? Convert.ToInt32(start) : 0;
                int pageSize = length.ToString() != null ? Convert.ToInt32(length) : 0;
                int PageStart = start.ToString() != null ? Convert.ToInt32(start) : 0;
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();

                HttpResponseMessage response = _serviceRepository.GetResponse(contollerName, PageStart, pageSize, SortDirection, sortColumn, search);
                response.EnsureSuccessStatusCode();
                List<CurrencyViewModel> currencies = response.Content.ReadAsAsync<List<CurrencyViewModel>>().Result;

                //HttpResponseMessage response1 = _serviceRepository.GetSubResponse(contollerName);
                //response1.EnsureSuccessStatusCode();
                //List<CurrencyViewModel> currencie = response1.Content.ReadAsAsync<List<CurrencyViewModel>>().Result;

                HttpContext.Session.SetString("currencyData", JsonConvert.SerializeObject(currencies));

                ViewBag.Title = "All Currency";
                data = currencies;
           
                foreach (var item in currencies)
                {
                    totalRecords = item.TotalItems;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data });

        }
    }
}