﻿
  
  
CREATE PROCEDURE [dbo].[SApp_GetAllCurrencies]    
 AS  
/*================================================================================  
Procedure Name: [SApp_GetAllCurrencies]  
Author: Gopi  
Create date: 04/03/2019  
Description: Get list of Location from table   
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
04/23/2019 Sai Krishna   Initial Version  
================================================================================*/  
  
BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
 SELECT *  
	FROM [dbo].[Currency]  
	where ([Currency].IsDeleted = 0)   
 END