﻿CREATE TABLE [dbo].[Service] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [ServiceName]      NVARCHAR (256)   CONSTRAINT [DF_Service_ServiceName] DEFAULT ('') NOT NULL,
    [Description]      NVARCHAR (256)   NULL,
    [ParentServiceId]  UNIQUEIDENTIFIER NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_Service_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_Service] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Service_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_Service_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_Service_ParentServiceId] FOREIGN KEY ([ParentServiceId]) REFERENCES [dbo].[Service] ([Id])
);

