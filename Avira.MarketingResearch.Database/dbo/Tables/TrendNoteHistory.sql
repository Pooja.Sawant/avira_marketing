﻿CREATE TABLE [dbo].[TrendNoteHistory] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [TrendNoteID]      UNIQUEIDENTIFIER NOT NULL,
    [TrendId]          UNIQUEIDENTIFIER NOT NULL,
    [TrendType]        NCHAR (512)      NOT NULL,
    [AuthorRemark]     NVARCHAR (512)   NULL,
    [ApproverRemark]   NVARCHAR (512)   NULL,
    [TrendStatusID]    UNIQUEIDENTIFIER NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_TrendNoteHistory] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TrendNoteHistory_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_TrendNoteHistory_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_TrendNoteHistory_Trend] FOREIGN KEY ([TrendId]) REFERENCES [dbo].[Trend] ([Id])
);




GO

GO

GO

GO

GO

GO

GO