﻿CREATE procedure [dbo].[SFab_insertUpdateCustomerCompanyInterest]  
(@CompanyId uniqueidentifier,  
@CustomerId uniqueidentifier,  
@CustomerRemark nvarchar(256) = '',  
@IsDeleted bit = 0,  
@AviraUserId uniqueidentifier)  
as  
/*================================================================================  
Procedure Name: SApp_insertUpdateCustomerCompanyInterest  
Author: Sharath  
Create date: 07/03/2019  
Description: Inseret or soft delete a record from [CustomerCompanyInterest] table by ID  
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
07/03/2019 Sharath   Initial Version  
================================================================================*/  
BEGIN  
SET NOCOUNT ON;  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
declare @ErrorMsg NVARCHAR(2048);

BEGIN TRY

if not exists (select 1 from [CustomerCompanyInterest] where CompanyId = @CompanyId and CustomerId = @CustomerId)
begin  

 INSERT INTO [dbo].[CustomerCompanyInterest]
           ([Id]
           ,[CustomerId]
           ,[CompanyId]
           ,[CustomerRemark]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted]
           )
select NEWID(),
@CustomerId,
@CompanyId,
isnull(@CustomerRemark,''),
getdate(),
@AviraUserId,
0;

end  
else  
begin  
--update record  
 update [dbo].[CustomerCompanyInterest]  
 set [CustomerRemark] = case when isnull(@CustomerRemark,'') = '' then [CustomerRemark] else @CustomerRemark end,  
  [ModifiedOn] = getdate(),  
  DeletedOn = case when IsDeleted = 0 and @IsDeleted = 1 then getdate() else DeletedOn end,  
  UserDeletedById = case when IsDeleted = 0 and @IsDeleted = 1 then @AviraUserId else UserDeletedById end,  
  IsDeleted = isnull(@IsDeleted, IsDeleted),  
  [UserModifiedById] = @AviraUserId  
  where [CustomerId] = @CustomerId
  and CompanyId = @CompanyId  
end  
  
SELECT StoredProcedureName ='SApp_insertUpdateCustomerCompanyInterest',Message =@ErrorMsg,Success = 1;     
END TRY
BEGIN CATCH
    -- Execute the error retrieval routine.
	DECLARE @ErrorNumber	int;
	DECLARE @ErrorSeverity	int;
	DECLARE @ErrorProcedure	varchar(100);
	DECLARE @ErrorLine	int;
	DECLARE @ErrorMessage	varchar(500);

	 SELECT @ErrorNumber = ERROR_NUMBER(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorProcedure = ERROR_PROCEDURE(),
        @ErrorLine = ERROR_LINE(),
        @ErrorMessage = ERROR_MESSAGE()

	insert into dbo.Errorlog(ErrorNumber,
							ErrorSeverity,
							ErrorProcedure,
							ErrorLine,
							ErrorMessage,
							ErrorDate)
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());

 set @ErrorMsg = 'Error while insert/Update Customer Company Interest, please contact Admin for more details.';    
 
SELECT Number = @ErrorNumber, 
		Severity =@ErrorSeverity,
		StoredProcedureName =@ErrorProcedure,
		LineNumber= @ErrorLine,
		Message =@ErrorMsg,
		Success = 0;     
END CATCH 
end