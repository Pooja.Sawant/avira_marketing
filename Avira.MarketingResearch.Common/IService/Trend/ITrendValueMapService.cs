﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;

namespace Avira.MarketingResearch.Common.IService.Trend
{
    public interface ITrendValueMapService
    {

        IEnumerable<TrendValueViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false, string id = "");

        IEnumerable<ValueConversionViewModel> GetAllValueConversion();

        IEnumerable<ImportanceViewModel> GetAllImportance();

        IEnumerable<ImpactDirectionViewModel> GetAllImpactDirection();

        Result<SpTransactionMessage> InsertUpdate(MainTrendValueInsertUpdateDbViewModel tfc, AviraUser currenUser);

        IEnumerable<TrendValuePreviewViewModel> GetById(Guid Id);
    }
}
