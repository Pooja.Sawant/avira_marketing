﻿
CREATE procedure [dbo].[SFab_UpdateUserResetPasswordLinkCreatedOn]    
( 
@UserName nvarchar(100) 
)    
as    
/*================================================================================    
Procedure Name: SFab_UpdateUserResetPasswordLinkCreatedOn   
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
08/21/2019 Praveen   Initial Version    
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
declare @ErrorMsg NVARCHAR(2048)
  
BEGIN TRY       

UPDATE [dbo].[AviraUser] SET      
       ResetPasswordLinkCreatedOn=GETDATE() 
	   WHERE UserName = @UserName --AND UserTypeId = 2
  
SELECT StoredProcedureName ='SFab_UpdateUserResetPasswordLinkCreatedOn',Message =@ErrorMsg,Success = 1; 
   
END TRY    
BEGIN CATCH    
    -- Execute the error retrieval routine.    
 DECLARE @ErrorNumber int;    
 DECLARE @ErrorSeverity int;    
 DECLARE @ErrorProcedure varchar(100);    
 DECLARE @ErrorLine int;    
 DECLARE @ErrorMessage varchar(500);    
    
  SELECT @ErrorNumber = ERROR_NUMBER(),    
        @ErrorSeverity = ERROR_SEVERITY(),    
        @ErrorProcedure = ERROR_PROCEDURE(),    
        @ErrorLine = ERROR_LINE(),    
        @ErrorMessage = ERROR_MESSAGE()    
    
 insert into dbo.Errorlog(ErrorNumber,    
       ErrorSeverity,    
       ErrorProcedure,    
       ErrorLine,    
       ErrorMessage,    
       ErrorDate)    
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());    
  
 set @ErrorMsg = 'Error while Update a User ResetPasswordLinkCreatedOn, please contact Admin for more details.';    
--THROW 50000,@ErrorMsg,1;      
--return(1)  
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;     
     
END CATCH    
    
    
end