﻿CREATE TABLE [dbo].[UserQueriesStatus] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [StatusName]       NCHAR (10)       NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_UserQueriesStatus_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_UserQueriesStatus] PRIMARY KEY CLUSTERED ([Id] ASC)
);

