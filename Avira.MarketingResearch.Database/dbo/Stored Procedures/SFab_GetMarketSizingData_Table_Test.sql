﻿
  
CREATE PROCEDURE [dbo].[SFab_GetMarketSizingData_Table_Test]
(
 @SegmentID uniqueidentifier=null,
 @ProjectID uniqueidentifier=null,
 @SegmentIdList dbo.[udtUniqueIdentifier] readonly,
 @RegionIdList dbo.[udtUniqueIdentifier] readonly,
 @CountryIdList dbo.[udtUniqueIdentifier] readonly,
 @TimeTagId uniqueidentifier = null,
 @CurrencyId uniqueidentifier = null,
 @MatricType nvarchar(100)=null,
 @ValueName	nvarchar(100)= null out,
  @CaptureJson bit = 0
)  
AS  
/*================================================================================  
Procedure Name: [SFab_GetMarketSizingData_Table]  
Author: Swami Aluri  
Create date: 06/04/2019  
Description: Get list of Marketing sizing from table   
Change History  
Date        Developer     Reason  
__________  ____________  ______________________________________________________  
06/04/2019   Swami Aluri   Initial Version  
================================================================================*/  

BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	Declare @SegmentType NVARCHAR(100);
	SELECT @SegmentType = SegmentName FROM Segment
	WHERE Segment.Id = @SegmentID
	declare @USDCurrencyId uniqueidentifier	
	select @USDCurrencyId = Id from Currency
	where CurrencyName = 'United States dollar';
	If @CurrencyId is null 
	set @CurrencyId = @USDCurrencyId;
	declare @Today datetime = getdate();
	IF (@SegmentType is null or @SegmentType = '')
	SET @SegmentType = 'End User'
	declare @RegionLevel tinyint;
	set @RegionLevel = 1;
	DECLARE @RegionList dbo.[udtUniqueIdentifier];

	IF not exists (select 1 from @RegionIdList)
	begin
	insert into @RegionList
	SELECT id FROM Region WHERE RegionName = 'Global'
	end
	else
	begin
	insert into @RegionList 
	select id from @RegionIdList;
	end

	while (@RegionLevel < = 3)
	begin
	set @RegionLevel = @RegionLevel +1;

	insert into @RegionList 
	select ID
	from Region
	where RegionLevel = @RegionLevel
	and exists (select 1 from @RegionList ParentRegion 
				where Region.ParentRegionId = ParentRegion.ID)

	end


	declare @CountryIdFilterList dbo.[udtUniqueIdentifier];

	if exists (select 1 from @CountryIdList)
	begin
	insert into @CountryIdFilterList
	select Id
	from country
	where exists(select 1 from @CountryIdList list
				inner join CountryRegionMap CRM
				on list.ID = CRM.CountryId
				inner join @RegionList RList
				on CRM.RegionId = RList.ID
				where country.id = CRM.CountryId
				);
	end
	else
	begin
	insert into @CountryIdFilterList
	select Id
	from country
	where exists(select 1 from CountryRegionMap CRM
				inner join @RegionList RList
				on CRM.RegionId = RList.ID
				where country.id = CRM.CountryId
				);
	end
    DECLARE @Years int;
    DECLARE @YearMin int;
    DECLARE @YearMax int;
    if @TimeTagId is not null
	begin
    SELECT @Years = DurationYears FROM TimeTag WHERE Id =@TimeTagId;
	end
	else
	begin
	SELECT @Years = DurationYears FROM TimeTag WHERE TimeTagName = 'LongTerm';
	end
    select @YearMin = BaseYear from BaseYearMaster
    select @YearMax = @YearMin + @Years;

	--CREATE table #tmp_Market(SegmentName nvarchar(256),
	--					[Value] DECIMAL(18,2),
	--					GrowthRatePercentage DECIMAL(18,2),
	--					[Year] int);
	CREATE TABLE #tmp_MarketValue(
						[Value] [decimal](18, 4),
						[Year] [int],
						[SegmentId] [uniqueidentifier]);

	CREATE table #tmp_Segment (SegmentId uniqueidentifier,
						SegmentName nvarchar(256));
	insert into #tmp_Segment
		select id as SegmentId,
		SubSegmentName as SegmentName
		from SubSegment
		where SegmentId = @SegmentID
		and (id IN (SELECT id FROM @SegmentIdList) OR not exists(select 1 from @SegmentIdList ))
		and IsDeleted = 0
		--and @SegmentType not in ('Raw materials', 'End User', 'Applications', 'Services', 'Components')
declare @UnitsId uniqueidentifier;
--declare @ValueName	nvarchar(100);
select top 1 @UnitsId  = ID,
@ValueName = ValueName from ValueConversion VC
where exists (select 1 FROM MarketValue as MV
   WHERE mv.Year between @YearMin and @YearMax 
   AND mv.ProjectId =  @ProjectID  --06/17/2019 comment for testing by Pooja
   --Either map country from list or map regions and sub-regions with no country mentioned
   and (MV.CountryId in (select ID from @CountryIdFilterList)
		OR (MV.CountryId is null and MV.RegionId in( select ID from @RegionList)))
and (((@MatricType is null or @MatricType = '' or @MatricType = 'Value') and vc.Id = mv.ValueConversionId)
or (@MatricType = 'Volume' and vc.Id = mv.VolumeUnitId)
or (@MatricType = 'ASP' and vc.Id = mv.ASPUnitId)
))
order by vc.Conversion desc;


insert into #tmp_MarketValue([Value], [Year], SegmentId)
SELECT	 sum(case when @MatricType = 'Volume' Then [dbo].[fun_UnitDeConversion](@UnitsId,MV.Volume)
			  when @MatricType = 'Growth%' then MV.GrowthRatePercentage
		   -- when @MatricType = 'ASP' then [dbo].[fun_CurrencyConversion]([dbo].[fun_UnitDeConversion](@UnitsId,MV.AvgSalePrice), @USDCurrencyId, @CurrencyId,@Today)
			  when @MatricType = 'ASP' then [dbo].[fun_CurrencyConversion](MV.AvgSalePrice, @USDCurrencyId, @CurrencyId,@Today)
			  else  [dbo].[fun_CurrencyConversion]([dbo].[fun_UnitDeConversion](@UnitsId, MV.[Value]), @USDCurrencyId, @CurrencyId,@Today) end)   AS [Value], 

		   MV.[Year] AS Year,
		    MVSM.SubSegmentId as SegmentId

   FROM MarketValue as MV
   inner join MarketValueSegmentMapping MVSM
   on MVSM.MarketValueId = mv.Id
   and MVSM.SegmentId = @SegmentID
   
   --and MVSM.SubSegmentId in (SELECT id FROM @SegmentIdList)
   --and (MVSM.SubSegmentId IN (SELECT id FROM @SegmentIdList) OR not exists(select 1 from @SegmentIdList ))
   WHERE mv.Year between @YearMin and @YearMax 
   AND mv.ProjectId =  @ProjectID  --06/17/2019 comment for testing by Pooja
  -- --Either map country from list or map regions and sub-regions with no country mentioned
   and ((MV.CountryId is null and MV.RegionId in( select ID from @RegionList))
		OR MV.CountryId in (select ID from @CountryIdFilterList))
group by MV.[Year], MVSM.SubSegmentId;

insert into #tmp_Market(SegmentName, [Value], [Year])
SELECT segCTE.SegmentName,
SUM(CTE_MV.[Value]) AS [Value],
CTE_MV.[Year]
FROM #tmp_MarketValue CTE_MV 
inner JOIN #tmp_Segment segCTE 
ON CTE_MV.SegmentId = segCTE.SegmentId
GROUP BY  segCTE.SegmentName,
CTE_MV.[Year];

Declare @ColumnList nvarchar(4000);
Declare @TotalCol nvarchar(4000);
declare @SelectColList nvarchar(4000);
declare @sql nvarchar(4000);
declare @baseYear int;
declare @yearSpan int;
select @baseYear = BaseYear +1
--, @yearSpan = @YearMax - (BaseYear +1)
from BaseYearMaster ;

select @YearMax = MAX([Year])
from #tmp_Market
where Value != 0;

select @yearSpan =  @YearMax - @baseYear;
--set @TotalCol = '100*ABS(POWER((['+ cast(@YearMax AS nvarchar(100)) +']/[' + CAST(@baseYear AS nvarchar(100)) +']),(1/' + CAST(@yearSpan AS nvarchar(100)) +'))-1)';

set @TotalCol = 'case when CAST([' + CAST(@baseYear AS nvarchar(100)) +'] AS DECIMAL(18,4)) is null or CAST([' + CAST(@baseYear AS nvarchar(100)) +'] AS DECIMAL(18,4)) = 0 then 0 
else CAST(100*ABS(POWER((CAST(['+ cast(@YearMax AS nvarchar(100)) +'] AS DECIMAL(18,4))  /CAST([' + CAST(@baseYear AS nvarchar(100)) +'] AS DECIMAL(18,4))),(CAST(1 AS DECIMAL(18,4))/' 
+ CAST(@yearSpan AS nvarchar(100)) +'))-1) AS DECIMAL(18,4)) end '
 --cast(100 * CAST(ABS(POWER(CAST((@aA/@bB) AS DECIMAL(18,4)),(cast(@a/@b as DECIMAL(18,4))))-1) AS DECIMAL(18,4)) as DECIMAL(18,4)) AS Average

;WITH CTEYear AS (SELECT @YearMin AS [Year]
			UNION ALL
			SELECT [Year]+1 FROM CTEYear WHERE [Year]+1<=@YearMax)
select @ColumnList = STRING_AGG(QUOTENAME([Year]), ','),
@SelectColList = STRING_AGG('CAST('+QUOTENAME([Year]) + 'AS VARCHAR(20)) AS '+ QUOTENAME([Year]), ',')
from CTEYear
where [Year] in (select Year from #tmp_Market);

create table #Tmp_json
(JsonString nvarchar(max));

print '@SelectColList -->' 
print @SelectColList
print '@TotalCol -->' 
print @TotalCol
print '@ColumnList -->' 
print @ColumnList

if @CaptureJson = 1
begin
set @sql = 'insert into #Tmp_json 
select (SELECT cast([SegmentName] as varchar(100)) as Name, '+ @SelectColList + ', '+ @TotalCol+' AS Average FROM 
(SELECT SegmentName, [Value], [Year]
FROM #tmp_Market) p  
PIVOT  
(  
SUM([Value])  
FOR [Year] IN  
( ' + @ColumnList + ')  
) AS pvt  
ORDER BY case when pvt.[SegmentName] = ''Others'' then ''zzzz'' else pvt.[SegmentName] end FOR JSON PATH)'

exec sp_executesql @sql;
select JsonString from #Tmp_json 
end
else
begin

set @sql = 'SELECT cast([SegmentName] as varchar(100)) as Name, '+ @SelectColList + ', '+ @TotalCol+' AS Average FROM 
(SELECT SegmentName, [Value], [Year]
FROM #tmp_Market) p  
PIVOT  
(  
SUM([Value])  
FOR [Year] IN  
( ' + @ColumnList + ')  
) AS pvt  
ORDER BY case when pvt.[SegmentName] = ''Others'' then ''zzzz'' else pvt.[SegmentName] end FOR JSON PATH ;'

 --insert into dbo.Errorlog(    
 --      ErrorMessage
 --      )    
 --       values(@sql);

exec sp_executesql @sql;
end

print '@sql -->'
print @sql;

--drop table #tmp_Market;
drop table #tmp_Segment;
drop table #tmp_MarketValue;
drop table #Tmp_json;
END