﻿

Create procedure [dbo].[SFab_GetAllSupportSubCategoryByID]
@SupportCategoryId uniqueidentifier = null
as
/*================================================================================
Procedure Name: [SFab_GetAllSupportSubCategoryByID]
Author: Harshal
Create date: 08/21/2019
Description: Get list from SupportSubCategory
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
08/21/2019	Harshal 	Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT [Id], 
		[SubCategoryName]
	FROM [dbo].[SupportSubCategory]
where (SupportSubCategory.SupportCategoryId=@SupportCategoryId AND [SupportSubCategory].[IsDeleted] = 0)  
end