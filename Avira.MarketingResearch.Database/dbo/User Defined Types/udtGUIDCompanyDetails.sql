﻿CREATE TYPE [dbo].[udtGUIDCompanyDetails] AS TABLE (
    [ID]       UNIQUEIDENTIFIER NOT NULL,
    [Name]     NVARCHAR (256)   NULL,
    [Code]     NVARCHAR (256)   NULL,
    [Location] NVARCHAR (MAX)   NULL,
    [bitValue] BIT              NOT NULL);

