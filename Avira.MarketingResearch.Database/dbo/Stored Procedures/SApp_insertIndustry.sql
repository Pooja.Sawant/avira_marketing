﻿

CREATE procedure [dbo].[SApp_insertIndustry]
(@Id uniqueidentifier,
@IndustryName nvarchar(256),
@Description nvarchar(256),
@ParentId uniqueidentifier,
@UserCreatedById uniqueidentifier, 
@CreatedOn Datetime)
as
/*================================================================================
Procedure Name: SApp_insertComponent
Author: Harshal
Create date: 03/19/2019
Description: Insert a record into Industry table
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
03/19/2019	Harshal			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ErrorMsg NVARCHAR(2048);
declare @SegmentId uniqueidentifier;
select @SegmentId = Id from Segment
where SegmentName = 'End User';
 


--Validations
IF exists(select 1 from [dbo].SubSegment 
		 where SubSegmentName = @IndustryName
		 and SegmentId = @SegmentId
		 and (ParentId = @ParentId or (ParentId is null and @ParentId is null) AND IsDeleted = 0
   ))
begin
--declare @ErrorMsg NVARCHAR(2048);
set @ErrorMsg = 'Industry with Name "'+ @IndustryName + '" already exists.';
--THROW 50000,@ErrorMsg,1;
SELECT StoredProcedureName ='SApp_InsertIndustry',Message =@ErrorMsg,Success = 0;  
RETURN;  

return(1)
end
--End of Validations
BEGIN TRY

insert into [dbo].SubSegment([Id],
								[SegmentId],
								[SubSegmentName],
								[Description],
								ParentId,
								[IsDeleted],
								[UserCreatedById],
								[CreatedOn])
	values(@Id, 
	        @SegmentId,
			@IndustryName, 
			@Description,
			@ParentId, 
			0,
			@UserCreatedById, 
			@CreatedOn);

SELECT StoredProcedureName ='SApp_InsertIndustry',Message =@ErrorMsg,Success = 1;     

END TRY
BEGIN CATCH
    -- Execute the error retrieval routine.
	DECLARE @ErrorNumber	int;
	DECLARE @ErrorSeverity	int;
	DECLARE @ErrorProcedure	varchar;
	DECLARE @ErrorLine	int;
	DECLARE @ErrorMessage	varchar;

	 SELECT @ErrorNumber = ERROR_NUMBER(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorProcedure = ERROR_PROCEDURE(),
        @ErrorLine = ERROR_LINE(),
        @ErrorMessage = ERROR_MESSAGE()

	insert into dbo.Errorlog(ErrorNumber,
							ErrorSeverity,
							ErrorProcedure,
							ErrorLine,
							ErrorMessage,
							ErrorDate)
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());

 set @ErrorMsg = 'Error while creating a new Industry, please contact Admin for more details.';    
--THROW 50000,@ErrorMsg,1;      
--return(1)  
SELECT Number = @ErrorNumber, 
		Severity =@ErrorSeverity,
		StoredProcedureName =@ErrorProcedure,
		LineNumber= @ErrorLine,
		Message =@ErrorMsg,
		Success = 0;     
END CATCH
end