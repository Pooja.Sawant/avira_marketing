﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.Trend
{
    public class TrendRepository: ITrendRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "Trend";

        public TrendRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public TrendViewModel GetById(Guid guid)
        {
            TrendViewModel trendViewModel = new TrendViewModel();
            trendViewModel = GetTrendByTrenID(guid).FirstOrDefault();
            return trendViewModel;
        }

        public IEnumerable<TrendViewModel> GetAll(Guid ProjectId,int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted)
        {
            return GetAllTrendByProjectID(ProjectId, PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, includeDeleted);
        }

        public IEnumerable<TrendViewModel> GetAllTrendByProjectID(Guid? guid, int PageStart = 0, int pageSize = 0, int SortDirection = -1, string OrdbyByColumnName = "", string Search = "", bool includeDeleted = false)
        {
            DataTable trendIdsTable = new DataTable();
            trendIdsTable.Columns.Add("ProjectID", typeof(Guid));

            if (guid != null)
            {
                trendIdsTable.Rows.Add(guid);
            }

            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<TrendViewModel>(TrendQueries.StoreProcSelectAllTrendByProjectId
                            , new {@ProjectID=guid, @includeDeleted = includeDeleted, @PageStart = PageStart, @pageSize = pageSize, @SortDirection = SortDirection, @OrdbyByColumnName = OrdbyByColumnName, @TrendName = Search }
                            , null, true, null, CommandType.StoredProcedure);
        }
        public IEnumerable<TrendViewModel> GetTrendByTrenID(Guid? guid, int PageStart = 0, int pageSize = 0, int SortDirection = -1, string OrdbyByColumnName = "", string Search = "", bool includeDeleted = false)
        {
          

            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<TrendViewModel>(TrendQueries.StoreProcSelectTrendByTrendId
                            , new { @TrendID = guid, @includeDeleted = includeDeleted, @PageStart = PageStart, @pageSize = pageSize, @SortDirection = SortDirection, @OrdbyByColumnName = OrdbyByColumnName, @TrendName = Search }
                            , null, true, null, CommandType.StoredProcedure);
        }

        public Result<SpTransactionMessage> Create(TrendInsertDbViewModel trendInsertDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(TrendQueries.StoreProcInsert,
                new{
                @Id = trendInsertDbViewModel.Id,
                @TrendName = trendInsertDbViewModel.TrendName,
                @Description = trendInsertDbViewModel.Description,
                @ProjectID = trendInsertDbViewModel.ProjectID,
                @AuthorRemark = trendInsertDbViewModel.AuthorRemark,
                @ApproverRemark = trendInsertDbViewModel.ApproverRemark,
                @TrendStatusID = trendInsertDbViewModel.TrendStatusID,
                @TrendStatusName = trendInsertDbViewModel.TrendStatusName,
                @UserCreatedById = trendInsertDbViewModel.UserCreatedById,
                @CreatedOn = trendInsertDbViewModel.CreatedOn
                }
                , CommandType.StoredProcedure);
            return spTransactionMessage;

        }

        public Result<SpTransactionMessage> Update(TrendUpdateDbViewModel trendUpdateDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(TrendQueries.StoreProcUpdate, 
                new {@Id= trendUpdateDbViewModel.Id,
                @TrendName = trendUpdateDbViewModel.TrendName,
                @Description = trendUpdateDbViewModel.Description,
                @ProjectID= trendUpdateDbViewModel.ProjectID,
                @AuthorRemark = trendUpdateDbViewModel.AuthorRemark,
                @ApproverRemark = trendUpdateDbViewModel.ApproverRemark,
                @TrendStatusID = trendUpdateDbViewModel.TrendStatusID,
                @TrendStatusName = trendUpdateDbViewModel.TrendStatusName,
                @IsDeleted = trendUpdateDbViewModel.IsDeleted,
                @UserModifiedById = trendUpdateDbViewModel.UserModifiedById,
                @DeletedOn = trendUpdateDbViewModel.DeletedOn,
                @UserDeletedById = trendUpdateDbViewModel.UserDeletedById,
                @ModifiedOn = trendUpdateDbViewModel.ModifiedOn
            }, CommandType.StoredProcedure);
            return spTransactionMessage;
        }
    }
}
