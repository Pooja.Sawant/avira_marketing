﻿
CREATE PROCEDURE [dbo].[SFab_mmmGetAllCustomMarkets] (
@ProjectID uniqueidentifier ,
@UserId uniqueidentifier,
@Id uniqueidentifier=null)  
AS  
/*================================================================================  
Procedure Name: SFab_MMMGetAllCustomMarkets  
Author: Harshal   
Create date: 06/01/2020  
Description: Get data from MakeMyMarkets table 
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
06/01/2020 Harshal   Initial Version  
================================================================================*/  
BEGIN  
  SET NOCOUNT ON;  
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
  SELECT Id,UserId,ProjectId,Name,Description,SequenceNo 
  FROM MakeMyMarkets
  WHERE ProjectID=@ProjectID 
        AND UserId=@UserId
		AND(@Id is null OR Id=@Id)
  Order by SequenceNo
END