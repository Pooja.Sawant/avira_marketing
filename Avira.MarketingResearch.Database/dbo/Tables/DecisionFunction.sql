﻿CREATE TABLE [dbo].[DecisionFunction] (
    [DecisionId]         UNIQUEIDENTIFIER NOT NULL,
    [BusinessFunctionId] UNIQUEIDENTIFIER NOT NULL,
    FOREIGN KEY ([BusinessFunctionId]) REFERENCES [dbo].[LookupCategory] ([Id]),
    CONSTRAINT [FK__DecisionF__Decis__6EECF073] FOREIGN KEY ([DecisionId]) REFERENCES [dbo].[Decision] ([Id]),
    CONSTRAINT [Decision_Function] UNIQUE NONCLUSTERED ([DecisionId] ASC, [BusinessFunctionId] ASC)
);

