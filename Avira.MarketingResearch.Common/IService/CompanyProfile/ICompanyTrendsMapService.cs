﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.CompanyProfile
{
    public interface ICompanyTrendsMapService
    {
        IEnumerable<CompanyTrendsMapViewModel> GetAll(Guid companyId);
        Result<SpTransactionMessage> InsertUpdate(MainCompanyTrendsMapInsertUpdateDbViewModel tfc, AviraUser currenUser);
    }
}
