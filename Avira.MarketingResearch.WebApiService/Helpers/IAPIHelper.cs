﻿namespace Avira.MarketingResearch.Web.Helpers
{
    public interface IAPIHelper
    {
        string GetBaseURL();
        string GetCreateSegmentURL(string segmentName);
        string GetReadSegmentURL(string segmentName);
        string GetReadSegmentURL(string segmentName, string methodName);
        string GetReadProjectSegmentURL(string segmentName);
        string GetReadAllParentSegmentURL(string segmentName);
        string GetReadByIdSegmentURL(string segmentName);
        string GetReadByTrendIdSegmentURL(string segmentName);
        string GetUpdateSegmentURL(string segmentName);
        string GetDeleteSegmentURL(string segmentName);
        string GetSubSegmentURL(string segmentName);
        string GetReadAllUserByRoleSegmentURL(string segmentName);
        string GetIndustrySegmentListIdSegmentURL(string segmentName);
        string GetMarketSegmentListIdSegmentURL(string segmentName);
        string GetUpdateStatusSegmentURL(string segmentName);
        string GetByAllResponseSegmentURL(string segmentName);

        string GetImportanceResponse(string segmentName);
        string GetImpactDirectionResponse(string segmentName);
        string GetShareVolByCompanyIdSegmentURL(string segmentName);
        string SaveImagebyCompanyIdSegmentURL(string segmentName);

        string GetAllImportanceByCompanyIdSegmentURL(string segmentName);
        string GetAllImpactDirectionByCompanyIdSegmentURL(string segmentName);
        string GetAllApproachByCompanyIdSegmentURL(string segmentName);
        string GetAllCategoriesByCompanyIdSegmentURL(string segmentName);
        string GetAllIndustryByCompanyIdSegmentURL(string segmentName);
        string GetCompanyclientAndStrategyByIdURL(string segmentName);
        string GetAllParentSegmentURL(string segmentName);
        string GetCompanyStrategyByIdURL(string segmentName);
        string GetCompanyClientByIdURL(string segmentName);

        string GetCompanySWOTStrengthURL(string segmentName);
        string GetCompanySWOTWeeknessesURL(string segmentName);
        string GetCompanySWOTOpportunitiesURL(string segmentName);
        string GetCompanySWOTThreatsURL(string segmentName);

        string GetEcoSystemPresenceKeyCompByCompanyIdURL(string segmentName);
        string GetSectorPresenceMapByCompanyIdURL(string segmentName);
        string GetEcoSystemPresenceMapByCompanyIdURL(string segmentName);

        string GetAllCategorySegmentURL(string segmentName);
        string GetAllCompanyDetailsURL(string segment);

        string GetAllCompaniesResponse(string segment);
        string GetAllDesignationsResponse(string segment);
        string GetAllCompanyStagesResponse(string segment);
        string GetCompanyFundamentalResponse(string segment);
        string GetCompanyRegionResponse(string segment);
        string GetCompanyEntityResponse(string segment);
        string GetCompanySubsidaryResponse(string segment);
        string GetCompanyOrganizationChartURL(string segmentName);
        string GetCompanyOtherInfoByCompanyIdURL(string segmentName);
        string GetCompanyManagerResponse(string segment);

        string GetAllCurrenciesURL(string segmentName);
        string GetAllRegionsURL(string segmentName);
        string GetAllValueConversionURL(string segmentName);
        string GetAllCategoryURL(string segmentName);
        //string GetDynamicURL(string segment, string method);
        string GetDynamicURL(string segment);
        string GetCompanyNatureResponse(string segment);
        string GetRevenueProfitBaseYearURL(string segmentName);

    }
}
