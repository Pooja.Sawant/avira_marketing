﻿CREATE procedure [dbo].[SFab_GetCompanybyID]
(@CompanyID uniqueidentifier=null,
@includeDeleted bit = 0,
@AviraUserID uniqueidentifier = null)
as
/*================================================================================
Procedure Name: SFab_GetCompanybyID
Author: Pooja Sawant
Create date: 05/25/2019
Description: Get list from Company table by Name or ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
04/25/2019	Pooja			Initial Version
07/11/2019	Pooja			Added [RankNumber] column
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if @includeDeleted is null set @includeDeleted = 0

SELECT [Id],
		[CompanyCode],
		[CompanyName],
		[Description],
		[Telephone],
		[Telephone2],
		[Email],
		[Email2],
		[Fax],
		[Fax2],
		[Website],
		[ParentCompanyName],
		[ProfiledCompanyName],
		[NoOfEmployees],
		[YearOfEstb],
		[ParentCompanyYearOfEstb],
		[RankNumber]
FROM [dbo].[Company]
where (@CompanyID is null or ID = @CompanyID)
and (@includeDeleted = 1 or IsDeleted = 0) order by CompanyName



end