﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.Trend
{
    public class TrendProjectMapRepository : ITrendProjectMapRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "TrendProjectMapping";

        public TrendProjectMapRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public IEnumerable<TrendProjectMapViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search = "", bool isDeletedInclude = false, string id = "")
        {
            TrendProjectMapViewModel trnd = new TrendProjectMapViewModel();
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<TrendProjectMapViewModel>(TrendProjectMappingQueries.StoreProcSelectById
                            , new
                            {
                                @ProjectName = Search,
                                @TrendID = id,
                                @includeDeleted = false,
                                @OrdbyByColumnName = @OrdbyByColumnName,
                                @PageStart = PageStart,
                                @pageSize = pageSize,
                                @SortDirection = SortDirection
                            }
                            , null, true, null, CommandType.StoredProcedure);
        }

        public Result<SpTransactionMessage> InsertUpdate(MainTrendProjectMapInsertUpdateDbViewModel trendCompanyGroupMapUpdateDbViewModel)
        {

            DataTable trendProjectMapTable = new DataTable();
            trendProjectMapTable.Columns.Add("ID", typeof(Guid));
            trendProjectMapTable.Columns.Add("MapId", typeof(Guid));
            trendProjectMapTable.Columns.Add("bitValue", typeof(bool));
            if (trendCompanyGroupMapUpdateDbViewModel.TrendProjectMapInsertUpdate != null & trendCompanyGroupMapUpdateDbViewModel.TrendProjectMapInsertUpdate.Count > 0)
            {
                foreach (var item in trendCompanyGroupMapUpdateDbViewModel.TrendProjectMapInsertUpdate)//.Where(x => x.ImpactId != Guid.Empty)
                {
                    trendProjectMapTable.Rows.Add(new Object[] { item.ProjectId, item.ImpactId, item.EndUser });
                }
            }

            var spTransactionMessage = _dbHelper.ExecuteQuery(TrendProjectMappingQueries.StoreProcInsert,
               new
               {
                   @TrendId = trendCompanyGroupMapUpdateDbViewModel.TrendId,
                   @TrendProjectmap = trendProjectMapTable,
                   @AviraUserId = trendCompanyGroupMapUpdateDbViewModel.UserCreatedById,
               }
                    , CommandType.StoredProcedure);
            return spTransactionMessage;
        }
    }
}
