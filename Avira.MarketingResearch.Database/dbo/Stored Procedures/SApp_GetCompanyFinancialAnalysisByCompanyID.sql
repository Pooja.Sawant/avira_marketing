﻿
-- =============================================      
-- Author:  Swami      
-- Create date: 05/13/2019      
-- Description: Get Company Financial Analysis by CompanyId       
-- =============================================   

   
CREATE PROCEDURE [dbo].[SApp_GetCompanyFinancialAnalysisByCompanyID]           
 @CompanyID uniqueidentifier = null, 
 @includeDeleted bit = 0           
AS      
BEGIN       
 SET NOCOUNT ON; 
  DECLARE @CompanyFinancialID UNIQUEIDENTIFIER = null;
  DECLARE @UnitId UNIQUEIDENTIFIER = null;
 SELECT @CompanyFinancialID = Id,
 @UnitId = UnitId
 FROM dbo.CompanyFinancialAnalysis
		WHERE CompanyId = @CompanyID AND IsDeleted = 0
--
 --declare @CompanyFinancialID uniqueidentifier = 'F4E8AA6E-DE7C-48D0-904A-22F5CF8F869A'
 declare @Yearlist nvarchar(1000) = '';
 declare @YearAliaslist nvarchar(1000) = '';

 declare @sql nvarchar(4000)
 declare @Param nvarchar(1000)
 set @Param = '@CompanyFinancialID uniqueidentifier, @UnitId uniqueidentifier'
 ;with seqcte as (
 select top 5 ROW_NUMBER() over(order by name) as seq
 from sys.columns)
 select @Yearlist = @Yearlist + ',['+cast(BaseYear - seq +1 as nvarchar(100))+']',
 @YearAliaslist = @YearAliaslist + ',['+cast(BaseYear - seq +1 as nvarchar(100))+'] as [BaseYearMinus_'+cast(seq as nvarchar(100))+']'
 from BaseYearMaster
 cross apply seqcte

 --print @YearAliaslist	
		SELECT
		Id,
		CompanyId,
		[CurrencyId], 
		[UnitId] as ValueConversionId,
		[CompanyStatusId],
        [AuthorNotes],      
        [ApproverNotes],
        [UserCreatedById],
		[CreatedOn],
        [IsDeleted]
		FROM dbo.CompanyFinancialAnalysis
		WHERE CompanyId = @CompanyID AND IsDeleted = 0
	
			set @sql = '
			SELECT CompanyFinancialID,
			ExpensesType, 
			IsDeleted '+ @YearAliaslist + ' 
			FROM (
			SELECT  CBRK.[CompanyFinancialID],      
			CBRK.[ExpensesType], 
			CBRK.[Year],
			dbo.fun_UnitDeConversion(@UnitId,CBRK.ActualAmount) AS ActualAmount,
			CBRK.[IsDeleted]
			FROM dbo.CompanyFinancialAnalysis AS CFA
			INNER JOIN [CompanyExpensesBreakdown] AS CBRK ON CFA.ID = CBRK.CompanyFinancialID 
			WHERE CBRK.CompanyFinancialID = @CompanyFinancialID
			AND CBRK.IsDeleted = 0 AND CFA.IsDeleted = 0) 
			AS SourceTable PIVOT(AVG([ActualAmount]) 
			FOR [Year] IN('+ substring(@Yearlist, 2,100) +')) AS PivotTable;'
			exec sp_executesql @sql, @Param, @CompanyFinancialID = @CompanyFinancialID, @UnitId =@UnitId;;

			--select * from CompanyExpensesBreakdown
			set @sql = '
			SELECT CompanyFinancialID,
			AssetsType, 
			IsDeleted '+ @YearAliaslist + ' 
			FROM (
			SELECT
			CAL.[CompanyFinancialID],      
			CAL.[AssetsType], 
			CAL.[Year],
			dbo.fun_UnitDeConversion(@UnitId,CAL.ActualAmount) AS ActualAmount,
			CAL.[IsDeleted]
			FROM dbo.CompanyFinancialAnalysis AS CFA
			INNER JOIN [CompanyAssetsLiabilities] AS CAL ON CFA.ID = CAL.CompanyFinancialID
			WHERE CAL.CompanyFinancialID = @CompanyFinancialID AND CAL.IsDeleted = 0 AND CFA.IsDeleted = 0)
			AS SourceTable PIVOT(AVG([ActualAmount]) 
			FOR [Year] IN('+ substring(@Yearlist, 2,100) +')) AS PivotTable;'
			exec sp_executesql @sql, @Param, @CompanyFinancialID = @CompanyFinancialID, @UnitId =@UnitId;


			set @sql = '
			SELECT CompanyFinancialID,
				CashFlowType, 
				IsDeleted '+ @YearAliaslist + ' 
				FROM(
			SELECT
			CCF.[CompanyFinancialID],      
			CCF.[CashFlowType], 
			CCF.[Year],
			dbo.fun_UnitDeConversion(@UnitId,CCF.ActualAmount) AS ActualAmount,
			CCF.[IsDeleted]
			FROM dbo.CompanyFinancialAnalysis AS CFA
			INNER JOIN [CompanyCashFlows] AS CCF ON CFA.ID = CCF.CompanyFinancialID
			WHERE CCF.CompanyFinancialID = @CompanyFinancialID AND CCF.IsDeleted = 0 AND CFA.IsDeleted = 0)
			AS SourceTable PIVOT(AVG([ActualAmount]) 
			FOR [Year] IN('+ substring(@Yearlist, 2,100) +')) AS PivotTable;'
			exec sp_executesql @sql, @Param, @CompanyFinancialID = @CompanyFinancialID, @UnitId = @UnitId;

	
		SELECT
		CT.ID,
		CT.[CompanyFinancialID],      
        CT.[CategoryId], 
		CT.[Value],
		CT.[OtherParty],
		CT.[Date],
		CT.[Rationale],
		CT.[UserCreatedById],
		CT.[CreatedOn],
        CT.[IsDeleted]
		FROM dbo.CompanyFinancialAnalysis AS CFA
		INNER JOIN [CompanyTransactions] AS CT ON CFA.ID = CT.CompanyFinancialID     
		WHERE CT.CompanyFinancialID = @CompanyFinancialID AND CT.IsDeleted = 0 AND CFA.IsDeleted = 0
		
		SELECT Id, BaseYear FROM BaseYearMaster   
END