﻿

CREATE PROCEDURE [dbo].[SFabChart_GetCompanyOrgChartByCompanyID]
(
	@CompanyID uniqueidentifier = null,
	@AvirauserId UniqueIdentifier = null
)
as
/*================================================================================
Procedure Name: [SFabChart_GetCompanyOrgChartByCompanyID]
Author: Harshal
Create date: 09/07/2019
Description: Get list from Company Organization Struture by Company Id
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
09/07/2019	Harshal		Initial Version
================================================================================*/
BEGIN  
 ;WITH ORG_CTE AS (       
SELECT CompanyKeyEmployeesMap.Id, 
KeyEmployeeName, 
DesignationId, 
Designation.Designation, 
ManagerId, 
cast('' as nvarchar(256)) as ManagerName,
cast(ROW_NUMBER() over (partition by ManagerId order by CompanyKeyEmployeesMap.CreatedOn) as varchar(20)) as EmpRank    
FROM CompanyKeyEmployeesMap    
Left Join Designation on CompanyKeyEmployeesMap.DesignationId = Designation.Id   
WHERE 
ManagerId IS NULL and 
CompanyId = @CompanyID   
UNION ALL     
SELECT emp.ID, 
emp.KeyEmployeeName, 
emp.DesignationId,
Designation.Designation, 
emp.ManagerId, 
ORG_CTE.KeyEmployeeName as ManagerName,
cast(ORG_CTE.EmpRank+ cast(ROW_NUMBER() over (partition by emp.ManagerId order by emp.CreatedOn) as varchar(10)) as varchar(20))  
FROM CompanyKeyEmployeesMap emp    
INNER Join Designation on emp.DesignationId = Designation.Id   
INNER JOIN ORG_CTE  ON emp.ManagerId = ORG_CTE.ID    
where emp.CompanyId = @CompanyID)   
SELECT * FROM ORG_CTE   
END