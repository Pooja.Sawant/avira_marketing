﻿CREATE PROCEDURE [dbo].[SFab_GetCountriesByRegionId]
(
	@RegionLevel tinyint,
	@RegionId Uniqueidentifier
)
AS
BEGIN
    Set @RegionLevel = 3;

	select reg.Id, 
	c.Id as CountryId,
	reg.RegionName,  
	c.CountryName 
	from Country c 
	INNER JOIN CountryRegionMap crmap
	ON c.Id = crmap.CountryId and crmap.RegionId = @RegionId
	INNER JOIN Region reg
	ON crmap.RegionId = reg.Id 
	where reg.RegionLevel = @RegionLevel
	ORDER BY reg.Id
END