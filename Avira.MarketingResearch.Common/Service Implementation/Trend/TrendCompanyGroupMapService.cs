﻿using Avira.MarketingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.Trend
{
    public class TrendCompanyGroupMapService : ITrendCompanyGroupMapService
    {
        private readonly ITrendCompanyGroupMapRepository _trendCompanyGroupRepository;
        private readonly ITrendNoteRepository _iTrendNoteRepository;

        public TrendCompanyGroupMapService(ITrendCompanyGroupMapRepository trendCompanyGroupMapRepository, ITrendNoteRepository iTrendNoteRepository)
        {
            _trendCompanyGroupRepository = trendCompanyGroupMapRepository;
            _iTrendNoteRepository = iTrendNoteRepository;
        }
        /// <summary>
        /// To get all market with mapped/unmapped status by trend id
        /// </summary>
        /// <param name="TrendIdint"></param>
        /// <param name="PageStart"></param>
        /// <param name="pageSize"></param>
        /// <param name="SortDirection"></param>
        /// <param name="OrdbyByColumnName"></param>
        /// <param name="Search"></param>
        /// <param name="isDeletedInclude"></param>
        /// <returns></returns>
        public IEnumerable<TrendCompanyGroupMapViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false, string id = "")
        {
            return _trendCompanyGroupRepository.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude = false, id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tfc"></param>
        /// <param name="currenUser"></param>
        /// <returns></returns>
        public Result<SpTransactionMessage> InsertUpdate(MainTrendCompanyGroupMapInsertUpdateDbViewModel tfc, AviraUser currenUser)
        {

            var res = _trendCompanyGroupRepository.InsertUpdate(tfc);
            if (res.IsSuccess == true)
            {
                tfc.TrendNoteModel.Id = Guid.NewGuid();
                tfc.TrendNoteModel.UserCreatedById = tfc.UserCreatedById;
                tfc.TrendNoteModel.TrendId = tfc.TrendId;
                _iTrendNoteRepository.Insert(tfc.TrendNoteModel);
            }

            return res;
        }
    }
}
