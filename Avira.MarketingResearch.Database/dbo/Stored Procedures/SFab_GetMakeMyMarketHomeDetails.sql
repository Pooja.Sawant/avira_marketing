﻿/*================================================================================  
Procedure Name: [SFab_GetMakeMyMarketHomeDetails]  
Author: Nitin Tarkar  
Create date: 04-Sep-2019  
Description: Get Make my market home details
Change History  
Date        Developer     Reason  
__________  ____________  ______________________________________________________  
04-Sep-2019 Nitin Tarkar   Initial Version  
================================================================================*/  
CREATE   PROCEDURE SFab_GetMakeMyMarketHomeDetails
(
	@UserId uniqueidentifier,
	@ProjectId uniqueidentifier,
	@TrendId uniqueidentifier=null
)  
AS  
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Declare @CustomerId uniqueidentifier
	Set @CustomerId = dbo.fun_GetCustomerIdForLoggedInUser(@UserId)

	Select @CustomerId As CustomerId, @ProjectId As ProjectId, MMM.TrendId, 
		   MMM.Id, MMM.CustomMarketRank, MMM.CustomMarketName, [Description] As Description,
		   MMM.CustomizationVariableType, MMM.CustomizationLevelType,
		   MMM.SegmentId,
		   --'ASP' As CustomizationVariableType, 'Segment Level' As CustomizationLevelType,
		   MMM.BroadLevelSegmentId, MMM.BroadLevelSegmentName, MMM.SegmentLevelSegmentId, MMM.SegmentLevelSegmentName, MMM.SegmentLevelSubSegmentName, 
		   --Cast('51CBB038-3875-4D6A-A6C4-84EE93AA616F' As UNIQUEIDENTIFIER ) As BroadLevelSegmentId, 'Services' As BroadLevelSegmentName, MMM.SegmentLevelSegmentId, MMM.SegmentLevelSegmentName, MMM.SegmentLevelSubSegmentName, 
		   MMM.ConstantCustomizationLevel, MMM.ChoiceOfChangeBoardLevel, MMM.ChoiceOfChangeOverallMarket, MMM.ConstantOtherBoardLevel,
		   --'Value' ConstantCustomizationLevel, MMM.ChoiceOfChangeBoardLevel, 'Volume' As ConstantOtherBoardLevel,
		   MMM.UserCreatedById As UserId
	From MakeMyMarket MMM 
	Where MMM.IsDeleted = 0 And
	      MMM.ProjectId = @ProjectId And	
		  MMM.CustomerId = @CustomerId And		   
			(
				(@TrendId Is Null And MMM.TrendId Is Null) Or 
				(@TrendId Is Not Null And MMM.TrendId = @TrendId)
			)
	Order By MMM.CustomMarketName 
			
END