﻿CREATE TABLE [dbo].[CompanySwotWeeknessesMap] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [CompanyId]        UNIQUEIDENTIFIER NULL,
    [CategoryName]     UNIQUEIDENTIFIER NULL,
    [Description]      NVARCHAR (512)   NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CompanySwotWeeknessesMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CompanySwotWeeknessesMap_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanySwotWeeknessesMap_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanySwotWeeknessesMap_AviraUser2] FOREIGN KEY ([UserDeletedById]) REFERENCES [dbo].[AviraUser] ([Id])
);

