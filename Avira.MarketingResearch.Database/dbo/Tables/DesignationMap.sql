﻿CREATE TABLE [dbo].[DesignationMap] (
    [Id]                  UNIQUEIDENTIFIER NOT NULL,
    [DesignationParentId] UNIQUEIDENTIFIER NOT NULL,
    [DesignationChildId]  UNIQUEIDENTIFIER NULL,
    [CreatedOn]           DATETIME2 (7)    NULL,
    [UserCreatedById]     UNIQUEIDENTIFIER NULL,
    [ModifiedOn]          DATETIME2 (7)    NULL,
    [UserModifiedById]    UNIQUEIDENTIFIER NULL,
    [IsDeleted]           BIT              NULL,
    [DeletedOn]           DATETIME2 (7)    NULL,
    [UserDeletedById]     UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_DesignationMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_DesignationMap_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_DesignationMap_AviraUser1] FOREIGN KEY ([UserDeletedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_DesignationMap_AviraUser2] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_DesignationMap_Designation] FOREIGN KEY ([DesignationParentId]) REFERENCES [dbo].[Designation] ([Id]),
    CONSTRAINT [FK_DesignationMap_Designation1] FOREIGN KEY ([DesignationChildId]) REFERENCES [dbo].[Designation] ([Id])
);

