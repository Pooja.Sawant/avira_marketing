﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces
{
    public interface IIndustryRepository
    {
        IndustryViewModel GetById(Guid guid);
        //IEnumerable<IndustryViewModel> GetAll(string industryIDList, bool includeDeleted);
        IEnumerable<IndustryViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted);
        IEnumerable<IndustryViewModel> GetAllParentIndustry();
        Result<SpTransactionMessage> Create(IndustryInsertDbViewModel viewModel);
        Result<SpTransactionMessage> Update(IndustryUpdateDbViewModel viewModel);
    }
}
