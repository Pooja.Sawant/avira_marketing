﻿
CREATE procedure [dbo].[SApp_UpdateCompanyProduct]      
(@Id	UNIQUEIDENTIFIER,
@ProductName	NVARCHAR(256),
@Description	NVARCHAR(512)=null,
@Patents	    NVARCHAR(512)=null,
@LaunchDate		DATETIME=null,
@ImageURL	NVARCHAR(256)=null,
@ImageDisplayName	NVARCHAR(256)=null,
@ActualImageName	NVARCHAR(256)=null,
@StatusId	UNIQUEIDENTIFIER=null,
@CompanyId	UNIQUEIDENTIFIER=null,
@ProjectId	UNIQUEIDENTIFIER=null,
@Amount decimal(18,2)=null,
@CurrencyId UNIQUEIDENTIFIER=null,
@ValueConversionId UNIQUEIDENTIFIER=null,
@Year INT=null,
@UserModifiedById  uniqueidentifier
)  
as      
/*================================================================================      
Procedure Name: SApp_UpdateCompanyProduct     
Author: Harshal      
Create date: 05/08/2019      
Description: update a record in Company Product table by ID  and update Product value  
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
05/08/2019 Harshal   Initial Version     
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
         
declare @ErrorMsg NVARCHAR(2048);  
BEGIN TRY       
--update record  
UPDATE [dbo].[CompanyProduct]      
SET [ProductName]=@ProductName
           ,[LaunchDate]=@LaunchDate
           ,[Description]=@Description
           ,[Patents]=@Patents
           ,[StatusId]=@StatusId
           ,[ImageURL]=@ImageURL
           ,[ImageDisplayName]=@ImageDisplayName
		   ,[ActualImageName]=@ActualImageName
           ,[CompanyId]=@CompanyId
		   ,[ProjectId]=@ProjectId
           ,[ModifiedOn]=GETDATE()
           ,[UserModifiedById]=@UserModifiedById
           ,[IsDeleted]=0 
  where ID = @Id 
  
  --update Record of CompanyProductValue 
UPDATE [dbo].[CompanyProductValue]  
SET CurrencyId=@CurrencyId,
    ValueConversionId=@ValueConversionId,
	Amount=dbo.fun_UnitConversion(@ValueConversionId, @Amount)
	--Year=@Year
where CompanyProductId=@Id	  
     
SELECT StoredProcedureName ='SApp_UpdateCompanyProduct',Message =@ErrorMsg,Success = 1;       
    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);      
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());   
  
set @ErrorMsg = 'Error while updating Comapny Product, please contact Admin for more details.'        
   
 SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
END CATCH      
      
    
    
      
end