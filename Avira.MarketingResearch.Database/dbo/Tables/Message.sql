﻿CREATE TABLE [dbo].[Message] (
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [MessageTemplateId] UNIQUEIDENTIFIER NOT NULL,
    [SenderFrom]        NVARCHAR (500)   NULL,
    [ReceiverTos]       NVARCHAR (500)   NULL,
    [ReceiverCcs]       NVARCHAR (500)   NULL,
    [ReceiverBccs]      NVARCHAR (500)   NULL,
    [Subject]           NVARCHAR (500)   NULL,
    [Message]           NVARCHAR (MAX)   NULL,
    [StatusId]          UNIQUEIDENTIFIER NULL,
    [IsDeleted]         BIT              CONSTRAINT [DF_Message_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]         DATETIME2 (7)    NULL,
    [UserCreatedById]   UNIQUEIDENTIFIER NULL,
    [ModifiedOn]        DATETIME2 (7)    NULL,
    [UserModifiedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_Message] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Message_MessageTemplate] FOREIGN KEY ([MessageTemplateId]) REFERENCES [dbo].[MessageTemplate] ([Id])
);



