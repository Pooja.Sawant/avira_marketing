﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Repository.CoreInterfaces.ProjectDashBoard;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.ProjectDashBoard
{
    public class ProjectRepository : IProjectRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "Project";
        public ProjectRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }


        //public DisplayProjectDBViewModel GetById(Guid guid, Guid AviraUserId)
        //{
        //    DisplayProjectDBViewModel projectViewModel = new DisplayProjectDBViewModel();
      
        //    projectViewModel = GetProjectDetailById(guid, AviraUserId).FirstOrDefault();
           
        //    SegmentnameViewModel segmentnameViewModel = new SegmentnameViewModel();
        //    segmentnameViewModel = GetAllSegmentByProject(guid).FirstOrDefault();
        //    projectViewModel.TrendsFormsApprover = segmentnameViewModel.BroadIndustries + "$" + segmentnameViewModel.Industries + "$" + segmentnameViewModel.Markets + "$" + segmentnameViewModel.SubMarkets;
        //    return projectViewModel;
        //}

        public IEnumerable<ProjectViewModel> GetAll(Guid? guid, Guid AviraUserId, int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted)
        {
            IEnumerable<ProjectViewModel> Vmodel=null;
            Vmodel=GetAllProject(guid, AviraUserId, PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, includeDeleted);
            SegmentnameViewModel segmentnameViewModel = new SegmentnameViewModel();
            segmentnameViewModel = GetAllSegmentByProject(guid).FirstOrDefault();
            if (Vmodel != null)
            {
                foreach (ProjectViewModel vm in Vmodel)

                    if (segmentnameViewModel != null)
                    {
                        vm.TrendsFormsApprover = segmentnameViewModel.BroadIndustries + "$" + segmentnameViewModel.Industries + "$" + segmentnameViewModel.Markets + "$" + segmentnameViewModel.SubMarkets;
                    }
                    else
                    {
                        vm.TrendsFormsApprover = "";
                    }
                }           
            
            return Vmodel;
        }

        private IEnumerable<ProjectViewModel> GetAllProject(Guid? guid, Guid AviraUserId, int PageStart = 0, int pageSize = 0, int SortDirection = -1, string OrdbyByColumnName = "", string Search = "", bool includeDeleted = false)
        {

            DataTable projectIdsTable = new DataTable();
            projectIdsTable.Columns.Add("ID", typeof(Guid));
            if (guid != null && guid != Guid.Empty)
            {
                projectIdsTable.Rows.Add(guid);
            }
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<ProjectViewModel>(ProjectQueries.StoreProcSelectAll
                            , new {
                                @ProjectIDList = projectIdsTable.AsTableValuedParameter("dbo.udtUniqueIdentifier"),
                                @includeDeleted = includeDeleted, @PageStart = PageStart, @pageSize = pageSize,
                                @SortDirection = SortDirection, @OrdbyByColumnName = OrdbyByColumnName,
                                @ProjectName = Search,
                                @AviraUserId = AviraUserId
                            }
                            , null, true, null, CommandType.StoredProcedure);
        }

        public IEnumerable<ProjectViewModel> GetAllProject()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
          
                return dapper.Query<ProjectViewModel>(ProjectQueries.StoreProcSelectAllProject, null, null, true, null,
               CommandType.StoredProcedure);           
        }

        private IEnumerable<SegmentnameViewModel> GetAllSegmentByProject(Guid? guid)
        {

            DataTable projectIdsTable = new DataTable();
            projectIdsTable.Columns.Add("ID", typeof(Guid));
            if (guid != null)
            {
                projectIdsTable.Rows.Add(guid);
            }
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<SegmentnameViewModel>(ProjectQueries.StoreProcGetSegment
                            , new { @ProjectId = guid }
                            , null, true, null, CommandType.StoredProcedure);
        }

        private IEnumerable<DisplayProjectDBViewModel> GetProjectDetailById(Guid? guid,Guid AviraUserId, int PageStart = 0, int pageSize = 0, int SortDirection = -1, string OrdbyByColumnName = "", string Search = "", bool includeDeleted = false)
        {

            DataTable projectIdsTable = new DataTable();
            projectIdsTable.Columns.Add("ID", typeof(Guid));

            if (guid != null)
            {
                projectIdsTable.Rows.Add(guid);
            }
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<DisplayProjectDBViewModel>(ProjectQueries.StoreProcSelectAll
                            , new { @ProjectIDList = projectIdsTable.AsTableValuedParameter("dbo.udtUniqueIdentifier"), @includeDeleted = includeDeleted, @PageStart = PageStart, @pageSize = pageSize, @SortDirection = SortDirection, @OrdbyByColumnName = OrdbyByColumnName, @ProjectName = Search, @AviraUserId = AviraUserId }
                            , null, true, null, CommandType.StoredProcedure);
        }

        public Result<SpTransactionMessage> Create(ProjectInsertDbViewModel projectInsertDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(ProjectQueries.StoreProcInsert, projectInsertDbViewModel, CommandType.StoredProcedure);
            return spTransactionMessage;

        }

        public Result<SpTransactionMessage> Update(ProjectUpdateDbViewModel projectUpdateDbViewModel)
        {
            Result<SpTransactionMessage> spTransactionMessage = null;
            if (projectUpdateDbViewModel.ProjectStatusChange == "OK")
            {
                spTransactionMessage = _dbHelper.ExecuteQuery(ProjectQueries.StoreProcUpdteStatus,new { @Id= projectUpdateDbViewModel .ProjectID}, CommandType.StoredProcedure);
            }
            else
            {
                 spTransactionMessage = _dbHelper.ExecuteQuery(ProjectQueries.StoreProcUpdate, projectUpdateDbViewModel, CommandType.StoredProcedure);
               
            }
            return spTransactionMessage;
        }

        public IEnumerable<IndustryViewModel> GetIndustrySegmentList(string id, bool includeParentOnly = false)
        {

            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            if (id == null || id == "0")
            {
                return dapper.Query<IndustryViewModel>(ProjectQueries.StoreProcSelectAllIndustry, new { @IncludeParentOnly = includeParentOnly }, null, true, null,
               CommandType.StoredProcedure);
            }
            else
            {
                return dapper.Query<IndustryViewModel>(ProjectQueries.StoreProcSelectAllIndustry, new { @ParentIndustryID = id, @IncludeParentOnly = includeParentOnly }, null, true, null,
                    CommandType.StoredProcedure);
            }
        }
        public IEnumerable<MarketViewModel> GetMarketSegmentList(string id, bool includeParentOnly = false)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            if (id == null || id == "0")
            {
                return dapper.Query<MarketViewModel>(ProjectQueries.StoreProcSelectAllMarket, new { @IncludeParentOnly = includeParentOnly }, null, true, null,
                    CommandType.StoredProcedure);
            }
            else
            {
               
                return dapper.Query<MarketViewModel>(ProjectQueries.StoreProcSelectAllMarket, new { @ParentMarketID = id, @IncludeParentOnly = includeParentOnly }, null, true, null,
                    CommandType.StoredProcedure);
            }
        }


       
    }
}
