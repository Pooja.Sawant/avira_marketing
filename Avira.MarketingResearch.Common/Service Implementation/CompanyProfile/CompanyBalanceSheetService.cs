﻿using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.CompanyProfile
{
    public class CompanyBalanceSheetService : ICompanyBalanceSheetService
    {
        private readonly ICompanyBalanceSheetRepository _companyBalanceSheetRepository;
        private readonly ICompanyNoteRepository _iCompanyNoteRepository;

        public CompanyBalanceSheetService(ICompanyBalanceSheetRepository companyBalanceSheetRepository, ICompanyNoteRepository CompanyNoteRepository)
        {
            _companyBalanceSheetRepository = companyBalanceSheetRepository;
            _iCompanyNoteRepository = CompanyNoteRepository;
        }

        public IEnumerable<CompanyBalanceSheetViewModel> GetCompanyBalanceSheetData(Guid CompanyId, AviraUser user)
        {
            var res = _companyBalanceSheetRepository.GetCompanyBalanceSheetData(CompanyId, user);
            return res;
        }
    }
}
