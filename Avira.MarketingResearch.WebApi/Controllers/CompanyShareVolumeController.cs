﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyShareVolumeController : AviraBaseController
    {
        private readonly ICompanyShareVolumeService _companyShareVolumeService;
        private readonly ICompanyShareVolumeRepository _companyShareVolumeRepository;
        private ILoggerManager _logger;

        public CompanyShareVolumeController(ICompanyShareVolumeService companyShareVolumeService, ICompanyShareVolumeRepository companyShareVolumeRepository, ILoggerManager loggerManager)
        {
            _companyShareVolumeService = companyShareVolumeService;
            _companyShareVolumeRepository = companyShareVolumeRepository;
            _logger = loggerManager;
        }

        [HttpGet]
        [Route("GetShareVolume")]
        public async Task<ActionResult<MainCompanyShareVolModel>> GetShareVolume(string CompanyId)
        {
            var res = await _companyShareVolumeService.GetShareVolume(CompanyId);
            return res;
        }

        [HttpGet]
        [Route("GetAllCurrencies")]
        public ActionResult<IEnumerable<CurrencyViewModel>> GetAll_Currencies()
        {
            List<CurrencyViewModel> res = null;
            try
            { 
             res = _companyShareVolumeService.GetAllCurrencies().ToList();
        }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
            //var id =  CurrentUser.Id;
        }

        [HttpPost]
        public ActionResult Post([FromBody] CompanyShareVolumeListModel viewModel)
        {
            viewModel.companyShareVolumeModels.FirstOrDefault().AviraUserId = CurrentUser.Id;

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new CompanyShareVolume, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                var result = _companyShareVolumeService.InsertUpdate(viewModel, CurrentUser);
                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "CompanyShareVolume is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }

        [HttpPost]
        [Route("SaveImageInServer")]
        public IActionResult UploadImage([FromBody] MainCompanyShareVolModel Model)
        {
            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while uploading a new image logo, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                var result = _companyShareVolumeService.SaveImageInServer(Model, CurrentUser);
                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Data Updated Successfully.";
                }
                else
                {
                    viewData.Message = result.Error;
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Ok(viewData);
        }
    }
}