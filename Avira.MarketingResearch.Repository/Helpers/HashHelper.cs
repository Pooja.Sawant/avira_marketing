﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Titan.Repository.Extensions
{
    public class HashHelper
    {
        public static string HashPasswordWithSalt(string password, string username)
        {
            byte[] passwordBytes = Encoding.ASCII.GetBytes(password);
            byte[] saltBytes = Encoding.ASCII.GetBytes(username);

            RNGCryptoServiceProvider rncCsp = new RNGCryptoServiceProvider();
            rncCsp.GetBytes(saltBytes);


            byte[] dst = new byte[saltBytes.Length + passwordBytes.Length];

            Buffer.BlockCopy(saltBytes, 0, dst, 0, saltBytes.Length);
            Buffer.BlockCopy(passwordBytes, 0, dst, 0, passwordBytes.Length);
            HashAlgorithm algorithm = HashAlgorithm.Create("SHA256");
            byte[] inarray = algorithm.ComputeHash(dst);
            return Convert.ToBase64String(inarray);
        }
    }
}
