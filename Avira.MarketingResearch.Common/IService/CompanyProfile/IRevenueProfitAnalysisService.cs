﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.CompanyProfile
{
    public interface IRevenueProfitAnalysisService
    {
        IEnumerable<CurrencyMapViewModel> GetAllCurrencies();
        IEnumerable<ValueConversionViewModel> GetAllValueConversion(); 
        IEnumerable<RegionMapViewModel> GetAllRegions();
        Result<SpTransactionMessage> InsertUpdate(MainRevenueProfitAnalysisInsertUpdateDbViewModel tfc, AviraUser currenUser);

    }
}
