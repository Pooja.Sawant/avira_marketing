﻿CREATE procedure [dbo].[SFab_GetCompanyShareVolumeByCompanyID]
(	
	@CompanyId uniqueidentifier,
	@UserId uniqueidentifier
)      
as      
/*================================================================================      
Procedure Name: [SFab_GetCompanyShareVolumeByCompanyID] 
Author: Nitin      
Create date: 31-Mar-2020      
Description: Get ShareHoldingPatternMap      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
31-Mar-2020 Nitin   Initial Version    
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
    
Declare @ErrorMsg NVARCHAR(2048)  
    
BEGIN TRY         
  
	Select SHPM.Id, SHPM.CompanyId, SHPM.EntityTypeId, ET.EntityTypeName, SHPM.ShareHoldingName
	From ShareHoldingPatternMap SHPM 
		Inner Join EntityType ET On 
			SHPM.EntityTypeId = ET.Id
	Where SHPM.CompanyId = @CompanyId
     
	SELECT StoredProcedureName ='SFab_GetCompanyShareVolumeByCompanyID',Message =@ErrorMsg,Success = 1;       
    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);      
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());      
    
 set @ErrorMsg = 'Error while insert a new SFab_GetCompanyShareVolumeByCompanyID, please contact Admin for more details.';    
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
       
END CATCH      
      
      
end