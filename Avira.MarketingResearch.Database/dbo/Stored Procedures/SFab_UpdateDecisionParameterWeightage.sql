﻿CREATE PROCEDURE [dbo].[SFab_UpdateDecisionParameterWeightage]
(
	@DecisionParameterId uniqueidentifier,
	@DecisionId	uniqueidentifier,	
	@Weightage	decimal(18, 2),
	@UserId	uniqueidentifier
)
As   

/*================================================================================    
Procedure Name: SFab_UpdateDecisionParameterWeightage   
Author: Praveen    
Create date: 07-Jan-2020
Description: Update decision parameter Weightage
Change History    
Date  Developer  Reason  
__________ ____________ ______________________________________________________    
07-Jan-2020  Praveen   Initial Version    
================================================================================*/    
BEGIN    
------------------
	--Required Tables: DecisionParameter 
	--Notes: DecisionID parameter is check whether this parameter belongs to logged-in user or not
	--Case 1: Weightage will update only if Total Weightage will be 100%.
	--Expected Result :SELECT StoredProcedureName ='SFab_UpdateDecisionParameterWeightage',Message = @ErrorMsg,Success = 1/0;
   
Declare @ErrorMsg NVARCHAR(2048);

BEGIN TRY
Begin

	Execute SFab_CopyDecisionFromBase @InDecisionId = @DecisionId, @UserId = @UserId, @CopyDecisionParametersFromBase = 1, @OutDecisionId = @DecisionId OUTPUT
	
	If Not Exists(Select Id From DecisionParameter Where Id = @DecisionParameterId)
	Begin

		Select @DecisionParameterId = Id From DecisionParameter 
		Where BaseDecisionParameterId = @DecisionParameterId And 
			DecisionId = @DecisionId		
	End

	Update DecisionParameter
	Set Weightage = @Weightage, ModifiedOn = GetDate()	
	Where Id = @DecisionParameterId
		
	SELECT StoredProcedureName ='SFab_UpdateDecisionParameterWeightage',Message =@ErrorMsg,Success = 1;     
End
END TRY
BEGIN CATCH
    -- Execute the error retrieval routine.
EXEC SApp_LogErrorInfo
 set @ErrorMsg = 'Error while Update DecisionParameter Weightage, please contact Admin for more details.'; 
 SELECT StoredProcedureName ='SFab_UpdateDecisionParameterWeightage',Message =@ErrorMsg,Success = 0;        
END CATCH 
END