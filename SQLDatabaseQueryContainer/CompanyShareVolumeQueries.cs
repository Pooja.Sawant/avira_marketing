﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public class CompanyShareVolumeQueries
    {
        public static string StoreProcCompanyShareVolume = "SApp_GetCompanyShareVolumeByCompanyID";
        public static string StoreProcInsertUpdate = "SApp_InsertUpdateCompanyShareVolumeMap";
        public static string StoreProcGetAllCurrencies = "SApp_GetAllCurrenciesWithCode";
        public static string StoreProcGetSaveImageLogo = "SApp_InsertCompanyShareVolumeImage";
    }
}
