﻿using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.CompanyProfile
{
    public class CompanyProductCategoryMapService : ICompanyProductCategoryMapService
    {
        private readonly ICompanyProductCategoryMapRepository _companyProductCategoryMapRepository;

        public CompanyProductCategoryMapService(ICompanyProductCategoryMapRepository companyProductCategoryMapRepository)
        {
            _companyProductCategoryMapRepository = companyProductCategoryMapRepository;
        }
        public IEnumerable<CompanyProductCategoryMapViewModel> GetAll(Guid? Id)
        {
            return _companyProductCategoryMapRepository.GetAll(Id);
        }

        public Result<SpTransactionMessage> InsertUpdate(CompanyProductViewMoel tfc, AviraUser currenUser)
        {
            var res = _companyProductCategoryMapRepository.InsertUpdate(tfc);
            return res;
        }
    }
}
