﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;

namespace Avira.MarketingResearch.Common.IService
{
    public interface IRegionService
    {  
        IEnumerable<RegionViewModel> GetAll(Guid Id);
        IEnumerable<RegionViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false);
        RegionViewModel GetById(Guid guid);
        IEnumerable<RegionCountryModel> GetRegionByTrendID(Guid trendId);

        Result<SpTransactionMessage> Create(RegionInsertDbViewModel tfc, AviraUser currentUser);
        Result<SpTransactionMessage> Update(RegionUpdateDbViewModel tfc, AviraUser currenUser);
    }
}
