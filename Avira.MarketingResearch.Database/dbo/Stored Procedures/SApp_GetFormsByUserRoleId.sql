﻿CREATE procedure [dbo].[SApp_GetFormsByUserRoleId]        
(@UserRoleId uniqueidentifier)        
as        
/*================================================================================        
Procedure Name: [SApp_GetFormsByUserRolesId]        
Author: Gopi        
Create date: 04/10/2019        
Description: Get Forms Based on logged in User Role    
Change History        
Date  Developer  Reason        
__________ ____________ ______________________________________________________        
04/10/2019 Gopi   Initial Version        
================================================================================*/        
BEGIN        
SET NOCOUNT ON;        
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;        
--Forms    
select Form.Id as FormId,    
Form.FormName,
MenuFormMap.MenuId As MenuId,
Menu.MenuName as MenuName    
from Form
LEFT join MenuFormMap on form.Id = MenuFormMap.FormId
LEFT Join Menu on menu.Id = MenuFormMap.MenuId    
where exists (select 1 from FormPermissionMap     
   inner join UserRolePermissionMap    
   on FormPermissionMap.PermissionId = UserRolePermissionMap.PermissionId    
   where UserRolePermissionMap.UserRoleId = @UserRoleId    
   and form.Id = FormPermissionMap.FormId
   --in (select  FormId from FormPermissionMap fpm  where PermissionId in 
   --(select PermissionId from UserRolePermissionMap where UserRoleId = @UserRoleId)) 
   
   )   
END