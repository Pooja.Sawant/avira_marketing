﻿CREATE TABLE [dbo].[StagingCompanyProfileShareHolding] (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [ImportId]     UNIQUEIDENTIFIER NULL,
    [Name]         NVARCHAR (255)   NULL,
    [Entity]       NVARCHAR (255)   NULL,
    [Holding]      DECIMAL (9, 4)   NULL,
    [EntityTypeId] UNIQUEIDENTIFIER NULL,
    [CompanyId]    UNIQUEIDENTIFIER NULL,
    [ErrorNotes]   NVARCHAR (1000)  NULL,
    [RowNo]        INT              NULL,
    CONSTRAINT [PK_StagingCompanyProfileShareHolding] PRIMARY KEY CLUSTERED ([Id] ASC)
);



