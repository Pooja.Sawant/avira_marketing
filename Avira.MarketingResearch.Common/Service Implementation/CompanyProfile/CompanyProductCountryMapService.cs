﻿using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using static Avira.MarketingResearch.Model.ViewModels.CompanyProfile.CompanyProductCountryMapViewModel;

namespace Avira.MarketingResearch.Common.Service_Implementation.CompanyProfile
{
    public class CompanyProductCountryMapService : ICompanyProductCountryMapService
    {
        private readonly ICompanyProductCountryMapRepository _companyProductCountryMapRepository;

        public CompanyProductCountryMapService(ICompanyProductCountryMapRepository companyProductCountryMapRepository)
        {
            _companyProductCountryMapRepository = companyProductCountryMapRepository;
        }
        public IEnumerable<CompanyProductCountryMapViewModel> GetAll(Guid? Id)
        {
            return _companyProductCountryMapRepository.GetAll(Id);
        }

        public Result<SpTransactionMessage> InsertUpdate(CompanyProductViewMoel tfc, AviraUser currenUser)
        {
            var res = _companyProductCountryMapRepository.InsertUpdate(tfc);
            return res;
        }
    }
}
