﻿CREATE TABLE [dbo].[Subscription] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [SubscriptionName] NVARCHAR (256)   CONSTRAINT [DF_Subscription_SubscriptionName] DEFAULT ('') NOT NULL,
    [Description]      NVARCHAR (256)   NULL,
    [StartDate]        DATETIME2 (7)    NULL,
    [EndDate]          DATETIME2 (7)    NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_Subscription_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_Subscription] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Subscription_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_Subscription_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id])
);

