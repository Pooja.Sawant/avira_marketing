﻿CREATE TABLE [dbo].[StagingESMSegmentMapping] (
    [Id]             UNIQUEIDENTIFIER NOT NULL,
    [ImportId]       UNIQUEIDENTIFIER NULL,
    [RowNo]          INT              NULL,
    [MarketName]     NVARCHAR (255)   NULL,
    [SegmentName]    NVARCHAR (255)   NULL,
    [SubSegmentName] NVARCHAR (260)   NULL,
    [MarketId]       UNIQUEIDENTIFIER NULL,
    [SegmentId]      UNIQUEIDENTIFIER NULL,
    [SubSegmentId]   UNIQUEIDENTIFIER NULL,
    [ProjectId]      UNIQUEIDENTIFIER NULL,
    [ErrorNotes]     NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_StagingESMSegmentMapping] PRIMARY KEY CLUSTERED ([Id] ASC)
);

