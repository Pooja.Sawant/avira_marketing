﻿CREATE procedure [dbo].[SApp_GetInfographicsDelete]
(

@Id	uniqueidentifier
)
as
/*================================================================================
Procedure Name: SApp_GetInfographicsDelete
Author: Praveen
Create date: 10/25/2019
Description: delete Infographics
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
10/25/2019	Praveen			Initial Version
================================================================================*/
declare @ErrorMsg NVARCHAR(2048)
BEGIN
	
	BEGIN TRY   


DELETE from Infographics WHERE Id = @Id
SELECT StoredProcedureName ='SApp_GetInfographicsDelete',Message =@ErrorMsg,Success = 1;   
END TRY    
BEGIN CATCH    
    -- Execute the error retrieval routine.    
 DECLARE @ErrorNumber int;            
 DECLARE @ErrorSeverity int;            
 DECLARE @ErrorProcedure varchar(100);            
 DECLARE @ErrorLine int;    
 DECLARE @ErrorMessage varchar(500);   
    
  SELECT @ErrorNumber = ERROR_NUMBER(),    
        @ErrorSeverity = ERROR_SEVERITY(),    
        @ErrorProcedure = ERROR_PROCEDURE(),    
        @ErrorLine = ERROR_LINE(),    
        @ErrorMessage = ERROR_MESSAGE()    
    
 insert into dbo.Errorlog(ErrorNumber,    
       ErrorSeverity,    
       ErrorProcedure,    
       ErrorLine,    
       ErrorMessage,    
       ErrorDate)    
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());    
  
 set @ErrorMsg = 'Error while importing trend, please contact Admin for more details.';    
--THROW 50000,@ErrorMsg,1;      
--return(1)  
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;     
     
END CATCH     


END