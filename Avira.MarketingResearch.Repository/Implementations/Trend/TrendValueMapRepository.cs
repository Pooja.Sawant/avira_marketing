﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.Trend
{
    public class TrendValueMapRepository : ITrendValueMapRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "TrendValueMapping";

        public TrendValueMapRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public IEnumerable<TrendValueViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search = "", bool isDeletedInclude = false, string id = "")
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<TrendValueViewModel>(TrendValueMapQueries.StoreProcSelectAll
                            , new { @TrendID = Search, @includeDeleted = false }
                            , null, true, null, CommandType.StoredProcedure);
        }


        public IEnumerable<ValueConversionViewModel> GetAllValueConversion()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<ValueConversionViewModel>(TrendValueMapQueries.GetAllValueConversion
                            , null
                            , null, true, null, CommandType.StoredProcedure);
        }

        public IEnumerable<ImportanceViewModel> GetAllImportance()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<ImportanceViewModel>(TrendValueMapQueries.GetAllImportance
                            , null
                            , null, true, null, CommandType.StoredProcedure);
        }

        public IEnumerable<ImpactDirectionViewModel> GetAllImpactDirection()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<ImpactDirectionViewModel>(TrendValueMapQueries.GetAllImpactDirection
                            , null
                            , null, true, null, CommandType.StoredProcedure);
        }


        public Result<SpTransactionMessage> InsertUpdate(MainTrendValueInsertUpdateDbViewModel trendValueMapInsertDbViewModel)
        {

            DataTable trendValueMapTable = new DataTable();
            trendValueMapTable.Columns.Add("Id", typeof(Guid));
            trendValueMapTable.Columns.Add("IntValue", typeof(int));
            trendValueMapTable.Columns.Add("DecimalValue", typeof(decimal));
            trendValueMapTable.Columns.Add("VarcharValue", typeof(string));
            if (trendValueMapInsertDbViewModel.TrendValueInsertUpdate != null & trendValueMapInsertDbViewModel.TrendValueInsertUpdate.Count > 0)
            {
                foreach (var item in trendValueMapInsertDbViewModel.TrendValueInsertUpdate.Where(x => x.TimeTagId != Guid.Empty))
                {
                    if (item.Rationale == null)
                    {
                        item.Rationale = "";
                    }
                    trendValueMapTable.Rows.Add(new Object[] {
                        item.TimeTagId,
                        item.Year,
                        item.Amount,
                        item.Rationale });
                }
            }

            var spTransactionMessage = _dbHelper.ExecuteQuery(TrendValueMapQueries.StoreProcInsertUpdate,
               new
               {
                   @TrendId = trendValueMapInsertDbViewModel.TrendId,
                   @ValueConversionId = trendValueMapInsertDbViewModel.ValueConversionId,
                   @ImportanceId = trendValueMapInsertDbViewModel.ImportanceId,
                   @ImpactDirectionId = trendValueMapInsertDbViewModel.ImpactDirectionId,
                   @TrendValuemap = trendValueMapTable,
                   @AviraUserId = trendValueMapInsertDbViewModel.AviraUserId
               }
                    , CommandType.StoredProcedure);
            return spTransactionMessage;
        }

        public IEnumerable<TrendValuePreviewViewModel> GetById(Guid Id)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<TrendValuePreviewViewModel>(TrendValueMapQueries.GetById
                            , new { @TrendID = Id}
                            , null, true, null, CommandType.StoredProcedure);
        }

    }
}
