﻿


CREATE procedure [dbo].[SApp_InsertUpdateStaggingTrendValuesIntoTables]

as
/*================================================================================
Procedure Name: [SApp_InsertUpdateStaggingTrendValuesIntoTables]
Author: Sai Krishna
Create date: 18/06/2019
Description: update a record in Trend table by ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
18/06/2019	Sai Krishna			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ErrorMsg NVARCHAR(2048);

BEGIN TRY 
--InsertUpdate record
--CteInsert For TrendTable
;With cteGetTrendIds as (
	SELECT st.Id as StaggingId,
	   st.TrendName,
	   st.Description,
	   CAST(p.ID as uniqueidentifier) as ProjectId,
	   CAST(ts.Id as uniqueidentifier) as TrendStatusId,
	   CAST(imp.Id as uniqueidentifier) as ImportanceId,
	   CAST(impdir.Id as uniqueidentifier) as ImpactdirectionId,
	   CAST(st.TrendValue as decimal(18,2)) as TrendValue
FROM StaginTrend st
Left Join Project p 
On st.Project = p.ProjectName and p.IsDeleted = 0
Left Join TrendStatus ts
On st.TrendStatus = ts.TrendStatusName and ts.IsDeleted = 0
Left Join Importance imp
On st.Importance = imp.ImportanceName and imp.IsDeleted = 0
Left Join ImpactDirection impdir 
ON st.ImpactDirection = impdir.ImpactDirectionName and impdir.IsDeleted = 0
)
Insert Into Trend(
			Id,
			TrendName,
			Description,
			ProjectID,
			TrendStatusID,
			CreatedOn,
			UserCreatedById,
			IsDeleted,
			ImportanceId,
			ImpactDirectionId,
			TrendValue
)
Select NEWID(),
	   cgti.TrendName,
	   cgti.Description,
	   cgti.ProjectId,
	   cgti.TrendStatusId,
	   GETDATE(),
	   '6B5A19C0-45E6-4FFC-815F-75F7C3D22B79',
	   0,
	   cgti.ImportanceId,
	   cgti.ImpactdirectionId,
	   cgti.TrendValue
From cteGetTrendIds cgti
Inner Join StaginTrend st
On st.Id = cgti.StaggingId;

--Delete OldMapping Records.
Delete from [dbo].[TrendKeywords]
Where [TrendKeywords].[TrendId] in 
(Select t.Id 
from Trend t 
Inner Join StaginTrend st 
on t.TrendName = st.TrendName 
and t.IsDeleted = 0) 

--CteInsert For TrendKeywordsTable
;With cteGetTrendKeywordIds as (
	SELECT st.Id as StaggingId,
	   CAST(t.Id as uniqueidentifier) as TrendId,
	   tk.value as TrendKeyWord
FROM StaginTrend st
Left Join Trend t 
On st.TrendName = t.TrendName and t.IsDeleted = 0
Cross apply string_split(st.TrendKeyWord,',') as TK
)
Insert Into TrendKeywords(
			Id,
			TrendKeyWord,
			TrendId,
			CreatedOn,
			UserCreatedById,
			IsDeleted
)
Select NEWID(),
	   cgtk.TrendKeyWord,
	   cgtk.TrendId,
	   GETDATE(),
	   '6B5A19C0-45E6-4FFC-815F-75F7C3D22B79',
	   0
From cteGetTrendKeywordIds cgtk
Inner Join StaginTrend st
On st.Id = cgtk.StaggingId;


--Delete OldMapping Records.
Delete from [dbo].[TrendIndustryMap]
Where [TrendIndustryMap].[TrendId] in 
(Select t.Id 
from Trend t 
Inner Join StaginTrend st 
on t.TrendName = st.TrendName 
and t.IsDeleted = 0) 

declare @IndustrySegmentId uniqueidentifier;
select @IndustrySegmentId = Id from Segment
where SegmentName = 'End User';

--CteInsert For TrendIndustryMapTable
;With cteSplitIndustryNames as (
SELECT st.Id as StaggingId,
	   I.value as IndustryName,
	   I.Position as IndustryPosition,
	   st.TrendName
FROM StaginTrend st
Left Join SubSegment ind 
On st.Industry = ind.SubSegmentName 
and ind.IsDeleted = 0
and ind.SegmentId = @IndustrySegmentId
Cross apply FUN_STRING_TOKENIZER(st.Industry,',') as I
),
cteGetIndustryIds As(
Select cspin.StaggingId,
       cspin.IndustryPosition,
       ind.Id as IndustryId,
	   cspin.IndustryName,
	   t.Id as TrendId 
from cteSplitIndustryNames cspin
Left Join SubSegment ind
On cspin.IndustryName = ind.SubSegmentName 
and ind.IsDeleted = 0
and ind.SegmentId = @IndustrySegmentId
Left Join Trend t
On t.TrendName = cspin.TrendName
),
cteIndustryImpact As(
SELECT st.Id as StaggingId,
	   II.value as IndustryImpact,
	   II.Position as IndustryImpactPosition
FROM StaginTrend st
Left Join ImpactType IT
On st.IndustryImpact = IT.ImpactTypeName and IT.IsDeleted = 0
Cross apply FUN_STRING_TOKENIZER(st.IndustryImpact,',') as II
),
cteIndustryImpactList As(
Select cte.StaggingId,
       cte.IndustryImpactPosition,
	   cte1.Id as ImpactId,
	   cte.IndustryImpact
from cteIndustryImpact cte
Inner Join ImpactType cte1
On cte.IndustryImpact = cte1.ImpactTypeName
Inner Join StaginTrend st
On st.Id = cte.StaggingId
),
cteIndustryEnduser As(
SELECT st.Id as StaggingId,
	   IEU.value as IndustryEndUser,
	   IEU.Position as IndustryEndUserPosition
FROM StaginTrend st
Left Join SubSegment I
On st.Industry = I.SubSegmentName 
and I.IsDeleted = 0
and I.SegmentId = @IndustrySegmentId
Cross apply FUN_STRING_TOKENIZER(st.IndustryEndUser,',') as IEU
),
cteEndUserList As(
Select cte1.StaggingId,
       cte1.IndustryPosition as IndustryEndUserPosition,
CASE
	When cte.StaggingId is not null Then 1
	Else 0
	End As IndustryEndUser
from cteIndustryEnduser cte
Right Join cteSplitIndustryNames cte1
On cte.IndustryEndUser = cte1.IndustryName 
and cte.StaggingId = cte1.StaggingId
)
Insert Into TrendIndustryMap(
	Id,
	TrendId,
	IndustryId,
	Impact,
	EndUser,
	CreatedOn,
	UserCreatedById,
	IsDeleted
)
Select NEWID(),
       CI.TrendId,
	   CI.IndustryId,
	   CIL.ImpactId,
	   CIE.IndustryEndUser,
	   GETDATE(),
	   '6B5A19C0-45E6-4FFC-815F-75F7C3D22B79',
	   0
From cteGetIndustryIds CI
Inner Join cteIndustryImpactList CIL
On CIL.StaggingId = CI.StaggingId
And CIL.IndustryImpactPosition = CI.IndustryPosition
Inner Join cteEndUserList CIE
On CIE.StaggingId = CI.StaggingId
And CIE.IndustryEndUserPosition = CI.IndustryPosition


--Delete OldMapping Records.
Delete from [dbo].[TrendMarketMap]
Where [TrendMarketMap].[TrendId] in 
(Select t.Id 
from Trend t 
Inner Join StaginTrend st 
on t.TrendName = st.TrendName 
and t.IsDeleted = 0)

--CteInsert For TrendMarketMapTable
;With cteSplitIndustryNames as (
SELECT st.Id as StaggingId,
	   I.value as MarketName,
	   I.Position as MarketPosition,
	   st.TrendName
FROM StaginTrend st
Left Join Market ind 
On st.Market = ind.MarketName and ind.IsDeleted = 0
Cross apply FUN_STRING_TOKENIZER(st.Market,',') as I
),
cteGetIndustryIds As(
Select cspin.StaggingId,
       cspin.MarketPosition,
       ind.Id as MarketId,
	   cspin.MarketName,
	   t.Id as TrendId 
from cteSplitIndustryNames cspin
Left Join Market ind
On cspin.MarketName = ind.MarketName and ind.IsDeleted = 0
Left Join Trend t
On t.TrendName = cspin.TrendName
),
cteIndustryImpact As(
SELECT st.Id as StaggingId,
	   II.value as MarketImpact,
	   II.Position as MarketImpactPosition
FROM StaginTrend st
Left Join ImpactType IT
On st.MarketImpact = IT.ImpactTypeName and IT.IsDeleted = 0
Cross apply FUN_STRING_TOKENIZER(st.MarketImpact,',') as II
),
cteIndustryImpactList As(
Select cte.StaggingId,
       cte.MarketImpactPosition,
	   cte1.Id as ImpactId,
	   cte.MarketImpact
from cteIndustryImpact cte
Inner Join ImpactType cte1
On cte.MarketImpact = cte1.ImpactTypeName
Inner Join StaginTrend st
On st.Id = cte.StaggingId
),
cteIndustryEnduser As(
SELECT st.Id as StaggingId,
	   IEU.value as MarketEndUser,
	   IEU.Position as MarketEndUserPosition
FROM StaginTrend st
Left Join Market I
On st.Market = I.MarketName and I.IsDeleted = 0
Cross apply FUN_STRING_TOKENIZER(st.MarketEndUser,',') as IEU
),
cteEndUserList As(
Select cte1.StaggingId,
       cte1.MarketPosition as MarketEndUserPosition,
CASE
	When cte.StaggingId is not null Then 1
	Else 0
	End As MarketEndUser
from cteIndustryEnduser cte
Right Join cteSplitIndustryNames cte1
On cte.MarketEndUser = cte1.MarketName 
and cte.StaggingId = cte1.StaggingId
)
Insert Into TrendMarketMap(
	Id,
	TrendId,
	MarketId,
	Impact,
	EndUser,
	CreatedOn,
	UserCreatedById,
	IsDeleted
)
Select NEWID(),
       CI.TrendId,
	   CI.MarketId,
	   CIL.ImpactId,
	   CIE.MarketEndUser,
	   GETDATE(),
	   '6B5A19C0-45E6-4FFC-815F-75F7C3D22B79',
	   0
From cteGetIndustryIds CI
Inner Join cteIndustryImpactList CIL
On CIL.StaggingId = CI.StaggingId
And CIL.MarketImpactPosition = CI.MarketPosition
Inner Join cteEndUserList CIE
On CIE.StaggingId = CI.StaggingId
And CIE.MarketEndUserPosition = CI.MarketPosition


--Delete OldMapping Records.
Delete from [dbo].[TrendProjectMap]
Where [TrendProjectMap].[TrendId] in 
(Select t.Id 
from Trend t 
Inner Join StaginTrend st 
on t.TrendName = st.TrendName 
and t.IsDeleted = 0)


--CteInsert For TrendProjectMapTable
;With cteSplitIndustryNames as (
SELECT st.Id as StaggingId,
	   I.value as ProjectName,
	   I.Position as ProjectPosition,
	   st.TrendName
FROM StaginTrend st
Left Join Project ind 
On st.Project1 = ind.ProjectName and ind.IsDeleted = 0
Cross apply FUN_STRING_TOKENIZER(st.Project1,',') as I
),
cteGetIndustryIds As(
Select cspin.StaggingId,
       cspin.ProjectPosition,
       ind.Id as ProjectId,
	   cspin.ProjectName,
	   t.Id as TrendId 
from cteSplitIndustryNames cspin
Left Join Project ind
On cspin.ProjectName = ind.ProjectName and ind.IsDeleted = 0
Left Join Trend t
On t.TrendName = cspin.TrendName
),
cteIndustryImpact As(
SELECT st.Id as StaggingId,
	   II.value as ProjectImpact,
	   II.Position as ProjectImpactPosition
FROM StaginTrend st
Left Join ImpactType IT
On st.ProjectImpact = IT.ImpactTypeName and IT.IsDeleted = 0
Cross apply FUN_STRING_TOKENIZER(st.ProjectImpact,',') as II
),
cteIndustryImpactList As(
Select cte.StaggingId,
       cte.ProjectImpactPosition,
	   cte1.Id as ImpactId,
	   cte.ProjectImpact
from cteIndustryImpact cte
Inner Join ImpactType cte1
On cte.ProjectImpact = cte1.ImpactTypeName
Inner Join StaginTrend st
On st.Id = cte.StaggingId
),
cteIndustryEnduser As(
SELECT st.Id as StaggingId,
	   IEU.value as ProjectEndUser,
	   IEU.Position as ProjectEndUserPosition
FROM StaginTrend st
Left Join Project I
On st.Project1 = I.ProjectName and I.IsDeleted = 0
Cross apply FUN_STRING_TOKENIZER(st.ProjectEndUser,',') as IEU
),
cteEndUserList As(
Select cte1.StaggingId,
       cte1.ProjectPosition as ProjectEndUserPosition,
CASE
	When cte.StaggingId is not null Then 1
	Else 0
	End As ProjectEndUser
from cteIndustryEnduser cte
Right Join cteSplitIndustryNames cte1
On cte.ProjectEndUser = cte1.ProjectName 
and cte.StaggingId = cte1.StaggingId
)
Insert Into TrendProjectMap(
	Id,
	TrendId,
	ProjectId,
	Impact,
	EndUser,
	CreatedOn,
	UserCreatedById,
	IsDeleted
)
Select NEWID(),
       CI.TrendId,
	   CI.ProjectId,
	   CIL.ImpactId,
	   CIE.ProjectEndUser,
	   GETDATE(),
	   '6B5A19C0-45E6-4FFC-815F-75F7C3D22B79',
	   0
From cteGetIndustryIds CI
Inner Join cteIndustryImpactList CIL
On CIL.StaggingId = CI.StaggingId
And CIL.ProjectImpactPosition = CI.ProjectPosition
Inner Join cteEndUserList CIE
On CIE.StaggingId = CI.StaggingId
And CIE.ProjectEndUserPosition = CI.ProjectPosition


--Delete OldMapping Records.
Delete from [dbo].[TrendCompanyGroupMap]
Where [TrendCompanyGroupMap].[TrendId] in 
(Select t.Id 
from Trend t 
Inner Join StaginTrend st 
on t.TrendName = st.TrendName 
and t.IsDeleted = 0)

--CteInsert For TrendCompanyGroupMapTable
;With cteSplitIndustryNames as (
SELECT st.Id as StaggingId,
	   I.value as CompanyGroupName,
	   I.Position as CompanyGroupPosition,
	   st.TrendName
FROM StaginTrend st
Left Join CompanyGroup ind 
On st.CompanyGroup = ind.CompanyGroupName and ind.IsDeleted = 0
Cross apply FUN_STRING_TOKENIZER(st.CompanyGroup,',') as I
),
cteGetIndustryIds As(
Select cspin.StaggingId,
       cspin.CompanyGroupPosition,
       ind.Id as CompanyGroupId,
	   cspin.CompanyGroupName,
	   t.Id as TrendId 
from cteSplitIndustryNames cspin
Left Join CompanyGroup ind
On cspin.CompanyGroupName = ind.CompanyGroupName and ind.IsDeleted = 0
Left Join Trend t
On t.TrendName = cspin.TrendName
),
cteIndustryImpact As(
SELECT st.Id as StaggingId,
	   II.value as CompanyGroupImpact,
	   II.Position as CompanyGroupImpactPosition
FROM StaginTrend st
Left Join ImpactType IT
On st.CompanyGroupImpact = IT.ImpactTypeName and IT.IsDeleted = 0
Cross apply FUN_STRING_TOKENIZER(st.CompanyGroupImpact,',') as II
),
cteIndustryImpactList As(
Select cte.StaggingId,
       cte.CompanyGroupImpactPosition,
	   cte1.Id as ImpactId,
	   cte.CompanyGroupImpact
from cteIndustryImpact cte
Inner Join ImpactType cte1
On cte.CompanyGroupImpact = cte1.ImpactTypeName
Inner Join StaginTrend st
On st.Id = cte.StaggingId
),
cteIndustryEnduser As(
SELECT st.Id as StaggingId,
	   IEU.value as CompanyGroupEndUser,
	   IEU.Position as CompanyGroupEndUserPosition
FROM StaginTrend st
Left Join CompanyGroup I
On st.CompanyGroup = I.CompanyGroupName and I.IsDeleted = 0
Cross apply FUN_STRING_TOKENIZER(st.CompanyGroupEndUser,',') as IEU
),
cteEndUserList As(
Select cte1.StaggingId,
       cte1.CompanyGroupPosition as CompanyGroupEndUserPosition,
CASE
	When cte.StaggingId is not null Then 1
	Else 0
	End As CompanyGroupEndUser
from cteIndustryEnduser cte
Right Join cteSplitIndustryNames cte1
On cte.CompanyGroupEndUser = cte1.CompanyGroupName 
and cte.StaggingId = cte1.StaggingId
)
Insert Into TrendCompanyGroupMap(
	Id,
	TrendId,
	CompanyGroupId,
	Impact,
	EndUser,
	CreatedOn,
	UserCreatedById,
	IsDeleted
)
Select NEWID(),
       CI.TrendId,
	   CI.CompanyGroupId,
	   CIL.ImpactId,
	   CIE.CompanyGroupEndUser,
	   GETDATE(),
	   '6B5A19C0-45E6-4FFC-815F-75F7C3D22B79',
	   0
From cteGetIndustryIds CI
Inner Join cteIndustryImpactList CIL
On CIL.StaggingId = CI.StaggingId
And CIL.CompanyGroupImpactPosition = CI.CompanyGroupPosition
Inner Join cteEndUserList CIE
On CIE.StaggingId = CI.StaggingId
And CIE.CompanyGroupEndUserPosition = CI.CompanyGroupPosition


--Delete OldMapping Records.
Delete from [dbo].[TrendTimeTag]
Where [TrendTimeTag].[TrendId] in 
(Select t.Id 
from Trend t 
Inner Join StaginTrend st 
on t.TrendName = st.TrendName 
and t.IsDeleted = 0)

--CteInsert For TrendTimeTagTable
;With cteSplitIndustryNames as (
SELECT st.Id as StaggingId,
	   I.value as TimeTagName,
	   I.Position as TimeTagPosition,
	   st.TrendName
FROM StaginTrend st
Left Join TimeTag ind 
On st.TimeTag = ind.TimeTagName and ind.IsDeleted = 0
Cross apply FUN_STRING_TOKENIZER(st.TimeTag,',') as I
),
cteGetIndustryIds As(
Select cspin.StaggingId,
       cspin.TimeTagPosition,
       ind.Id as TimeTagId,
	   cspin.TimeTagName,
	   t.Id as TrendId 
from cteSplitIndustryNames cspin
Left Join TimeTag ind
On cspin.TimeTagName = ind.TimeTagName and ind.IsDeleted = 0
Left Join Trend t
On t.TrendName = cspin.TrendName
),
cteIndustryImpact As(
SELECT st.Id as StaggingId,
	   II.value as TimeTagImpact,
	   II.Position as TimeTagImpactPosition
FROM StaginTrend st
Left Join ImpactType IT
On st.TimeTagImpact = IT.ImpactTypeName and IT.IsDeleted = 0
Cross apply FUN_STRING_TOKENIZER(st.TimeTagImpact,',') as II
),
cteIndustryImpactList As(
Select cte.StaggingId,
       cte.TimeTagImpactPosition,
	   cte1.Id as ImpactId,
	   cte.TimeTagImpact
from cteIndustryImpact cte
Inner Join ImpactType cte1
On cte.TimeTagImpact = cte1.ImpactTypeName
Inner Join StaginTrend st
On st.Id = cte.StaggingId
),
cteIndustryEnduser As(
SELECT st.Id as StaggingId,
	   IEU.value as TimeTagEndUser,
	   IEU.Position as TimeTagEndUserPosition
FROM StaginTrend st
Left Join TimeTag I
On st.TimeTag = I.TimeTagName and I.IsDeleted = 0
Cross apply FUN_STRING_TOKENIZER(st.TimeTagEndUser,',') as IEU
),
cteEndUserList As(
Select cte1.StaggingId,
       cte1.TimeTagPosition as TimeTagEndUserPosition,
CASE
	When cte.StaggingId is not null Then 1
	Else 0
	End As TimeTagEndUser
from cteIndustryEnduser cte
Right Join cteSplitIndustryNames cte1
On cte.TimeTagEndUser = cte1.TimeTagName 
and cte.StaggingId = cte1.StaggingId
)
Insert Into TrendTimeTag(
	Id,
	TrendId,
	TimeTagId,
	Impact,
	EndUser,
	CreatedOn,
	UserCreatedById,
	IsDeleted
)
Select NEWID(),
       CI.TrendId,
	   CI.TimeTagId,
	   CIL.ImpactId,
	   CIE.TimeTagEndUser,
	   GETDATE(),
	   '6B5A19C0-45E6-4FFC-815F-75F7C3D22B79',
	   0
From cteGetIndustryIds CI
Inner Join cteIndustryImpactList CIL
On CIL.StaggingId = CI.StaggingId
And CIL.TimeTagImpactPosition = CI.TimeTagPosition
Inner Join cteEndUserList CIE
On CIE.StaggingId = CI.StaggingId
And CIE.TimeTagEndUserPosition = CI.TimeTagPosition

--Delete OldMapping Records.
Delete from [dbo].[TrendEcoSystemMap]
Where [TrendEcoSystemMap].[TrendId] in 
(Select t.Id 
from Trend t 
Inner Join StaginTrend st 
on t.TrendName = st.TrendName 
and t.IsDeleted = 0)

--CteInsert For TrendEcoSystemMap Table
;With cteSplitIndustryNames as (
SELECT st.Id as StaggingId,
	   I.value as EcoSystemName,
	   I.Position as EcoSystemPosition,
	   st.TrendName
FROM StaginTrend st
Left Join Segment ind 
On st.EcoSystem = ind.SegmentName and ind.IsDeleted = 0
Cross apply FUN_STRING_TOKENIZER(st.EcoSystem,',') as I
),
cteGetIndustryIds As(
Select cspin.StaggingId,
       cspin.EcoSystemPosition,
       ind.Id as EcoSystemId,
	   cspin.EcoSystemName,
	   t.Id as TrendId 
from cteSplitIndustryNames cspin
Left Join Segment ind
On cspin.EcoSystemName = ind.SegmentName and ind.IsDeleted = 0
Left Join Trend t
On t.TrendName = cspin.TrendName
),
cteIndustryImpact As(
SELECT st.Id as StaggingId,
	   II.value as EcoSystemImpact,
	   II.Position as EcoSystemImpactPosition
FROM StaginTrend st
Left Join ImpactType IT
On st.EcoSystemImpact = IT.ImpactTypeName and IT.IsDeleted = 0
Cross apply FUN_STRING_TOKENIZER(st.EcoSystemImpact,',') as II
),
cteIndustryImpactList As(
Select cte.StaggingId,
       cte.EcoSystemImpactPosition,
	   cte1.Id as ImpactId,
	   cte.EcoSystemImpact
from cteIndustryImpact cte
Inner Join ImpactType cte1
On cte.EcoSystemImpact = cte1.ImpactTypeName
Inner Join StaginTrend st
On st.Id = cte.StaggingId
),
cteIndustryEnduser As(
SELECT st.Id as StaggingId,
	   IEU.value as EcoSystemEndUser,
	   IEU.Position as EcoSystemEndUserPosition
FROM StaginTrend st
Left Join Segment I
On st.EcoSystem = I.SegmentName and I.IsDeleted = 0
Cross apply FUN_STRING_TOKENIZER(st.EcoSystemEndUser,',') as IEU
),
cteEndUserList As(
Select cte1.StaggingId,
       cte1.EcoSystemPosition as EcoSystemEndUserPosition,
CASE
	When cte.StaggingId is not null Then 1
	Else 0
	End As EcoSystemEndUser
from cteIndustryEnduser cte
Right Join cteSplitIndustryNames cte1
On cte.EcoSystemEndUser = cte1.EcoSystemName 
and cte.StaggingId = cte1.StaggingId
)
Insert Into TrendEcoSystemMap(
	Id,
	TrendId,
	EcoSystemId,
	Impact,
	EndUser,
	CreatedOn,
	UserCreatedById,
	IsDeleted
)
Select NEWID(),
       CI.TrendId,
	   CI.EcoSystemId,
	   CIL.ImpactId,
	   CIE.EcoSystemEndUser,
	   GETDATE(),
	   '6B5A19C0-45E6-4FFC-815F-75F7C3D22B79',
	   0
From cteGetIndustryIds CI
Inner Join cteIndustryImpactList CIL
On CIL.StaggingId = CI.StaggingId
And CIL.EcoSystemImpactPosition = CI.EcoSystemPosition
Inner Join cteEndUserList CIE
On CIE.StaggingId = CI.StaggingId
And CIE.EcoSystemEndUserPosition = CI.EcoSystemPosition


--Delete OldMapping Records.
Delete from [dbo].[TrendKeyCompanyMap]
Where [TrendKeyCompanyMap].[TrendId] in 
(Select t.Id 
from Trend t 
Inner Join StaginTrend st 
on t.TrendName = st.TrendName 
and t.IsDeleted = 0)

--CteInsert For TrendKeyCompanyMap Table
;With cteSplitIndustryNames as (
SELECT st.Id as StaggingId,
	   I.value as KeyCompanyName,
	   I.Position as KeyCompanyPosition,
	   st.TrendName
FROM StaginTrend st
Left Join Company ind 
On st.Company = ind.CompanyName and ind.IsDeleted = 0
Cross apply FUN_STRING_TOKENIZER(st.Company,',') as I
),
cteGetIndustryIds As(
Select cspin.StaggingId,
       cspin.KeyCompanyPosition,
       ind.Id as KeyCompanyId,
	   cspin.KeyCompanyName,
	   t.Id as TrendId 
from cteSplitIndustryNames cspin
Left Join Company ind
On cspin.KeyCompanyName = ind.CompanyName and ind.IsDeleted = 0
Left Join Trend t
On t.TrendName = cspin.TrendName
)
Insert Into TrendKeyCompanyMap(
	Id,
	TrendId,
	CompanyId,
	CreatedOn,
	UserCreatedById,
	IsDeleted
)
Select NEWID(),
       CI.TrendId,
	   CI.KeyCompanyId,
	   GETDATE(),
	   '6B5A19C0-45E6-4FFC-815F-75F7C3D22B79',
	   0
From cteGetIndustryIds CI
Inner Join StaginTrend st
ON Ci.StaggingId = st.Id


--Delete OldMapping Records.
Delete from [dbo].[TrendRegionMap]
Where [TrendRegionMap].[TrendId] in 
(Select t.Id 
from Trend t 
Inner Join StaginTrend st 
on t.TrendName = st.TrendName 
and t.IsDeleted = 0)

--CteInsert For TrendRegionMapTable
;With cteSplitCountryNames as (
SELECT st.Id as StaggingId,
       ind.Id as CountryId,
	   I.value as CountryName,
	   I.Position as CountryPosition,
	   t.Id as TrendId,
	   st.TrendName
FROM StaginTrend st
Cross apply FUN_STRING_TOKENIZER(st.Country,',') as I
Left Join Country ind 
On I.value = ind.CountryName and ind.IsDeleted = 0
Left Join Trend t
On t.TrendName = st.TrendName
),
cteCountryImpact As(
SELECT st.Id as StaggingId,
	   II.value as CountryImpact,
	   II.Position as CountryImpactPosition
FROM StaginTrend st
Left Join ImpactType IT
On st.CountryImpact = IT.ImpactTypeName and IT.IsDeleted = 0
Cross apply FUN_STRING_TOKENIZER(st.CountryImpact,',') as II
),
cteCountryImpactList As(
Select cte.StaggingId,
       cte.CountryImpactPosition,
	   cte1.Id as ImpactId,
	   cte.CountryImpact
from cteCountryImpact cte
Inner Join ImpactType cte1
On cte.CountryImpact = cte1.ImpactTypeName
Inner Join StaginTrend st
On st.Id = cte.StaggingId
),
cteCountryEnduser As(
SELECT st.Id as StaggingId,
	   IEU.value as CountryEndUser,
	   IEU.Position as CountryEndUserPosition
FROM StaginTrend st
Left Join Country I
On st.Country = I.CountryName and I.IsDeleted = 0
Cross apply FUN_STRING_TOKENIZER(st.CountryEndUser,',') as IEU
),
cteEndUserList As(
Select cte1.StaggingId,
       cte1.CountryPosition as CountryEndUserPosition,
CASE
	When cte.StaggingId is not null Then 1
	Else 0
	End As CountryEndUser
from cteCountryEnduser cte
Right Join cteSplitCountryNames cte1
On cte.CountryEndUser = cte1.CountryName 
and cte.StaggingId = cte1.StaggingId
)

Insert Into TrendRegionMap(
	Id,
	TrendId,
	RegionId,
	CountryId,
	Impact,
	EndUser,
	CreatedOn,
	UserCreatedById,
	IsDeleted
)
Select NEWID(),
       CI.TrendId,
	   '00000000-0000-0000-0000-000000000000',
	   CI.CountryId,
	   CIL.ImpactId,
	   CIE.CountryEndUser,
	   GETDATE(),
	   '6B5A19C0-45E6-4FFC-815F-75F7C3D22B79',
	   0
From cteSplitCountryNames CI
Inner Join cteCountryImpactList CIL
On CIL.StaggingId = CI.StaggingId
and CIL.CountryImpactPosition = CI.CountryPosition
Inner Join cteEndUserList CIE
On CIE.StaggingId = CI.StaggingId
And CIE.CountryEndUserPosition = CI.CountryPosition


SELECT StoredProcedureName ='SApp_InsertUpdateStaggingTrendValuesIntoTables',Message =@ErrorMsg,Success = 1;     
  
END TRY    
BEGIN CATCH    
    -- Execute the error retrieval routine.    
 DECLARE @ErrorNumber int;    
 DECLARE @ErrorSeverity int;    
 DECLARE @ErrorProcedure varchar(100);    
 DECLARE @ErrorLine int;    
 DECLARE @ErrorMessage varchar(500);    
    
  SELECT @ErrorNumber = ERROR_NUMBER(),    
        @ErrorSeverity = ERROR_SEVERITY(),    
        @ErrorProcedure = ERROR_PROCEDURE(),    
        @ErrorLine = ERROR_LINE(),    
        @ErrorMessage = ERROR_MESSAGE()    
    
 insert into dbo.Errorlog(ErrorNumber,    
       ErrorSeverity,    
       ErrorProcedure,    
       ErrorLine,    
       ErrorMessage,    
       ErrorDate)    
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE()); 

set @ErrorMsg = 'Error while updating Trend, please contact Admin for more details.' 		   
 --set @ErrorMsg = 'Unspecified Error';    
--THROW 50000,@ErrorMsg,1;    
 --return(1)  
 SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;     
END CATCH   
end