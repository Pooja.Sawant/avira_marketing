﻿CREATE TABLE [dbo].[CompanyOtherInfo] (
    [ID]               UNIQUEIDENTIFIER NOT NULL,
    [CompanyId]        UNIQUEIDENTIFIER NULL,
    [TableInfoName]    NVARCHAR (100)   NULL,
    [Description]      NVARCHAR (4000)  NULL,
    [CompanyStatusId]  UNIQUEIDENTIFIER NULL,
    [CreatedOn]        DATETIME         NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME         NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NULL,
    [DeletedOn]        DATETIME         NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CompanyOtherInfo] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_CompanyOtherInfo_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([Id])
);

