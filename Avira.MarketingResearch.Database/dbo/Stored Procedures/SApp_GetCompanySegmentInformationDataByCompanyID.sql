﻿

CREATE PROCEDURE [dbo].[SApp_GetCompanySegmentInformationDataByCompanyID]             
 @CompanyID uniqueidentifier = null,         
 @includeDeleted bit = 0             
AS
/*================================================================================  
Procedure Name:[SApp_GetCompanySegmentInformationDataByCompanyID]  
Author: Harshal 
Create date: 08/09/2019  
Description:Getting Data From Table WRT CompanyId 
Change History  
Date        Developer     Reason  
__________  ____________  ______________________________________________________  
08/09/2019  Harshal   Initial Version  
================================================================================*/          
BEGIN         
 SET NOCOUNT ON;          
 SELECT  Id,
CompanyId,
CurrencyId,
ValueConversionId,
--CompanyName,
--Currency,
--Units,
Year
--Segment1,
--Segment2,
--Segment3,
--Segment4,
--Segment5,
--TotalReportableSegments,
--Geography1,
--Geography2,
--Geography3,
--Geography4,
--Geography5,
--TotalGeographicSegments
FROM [dbo].[CompanySegmentInformation]        
  
  WHERE ([CompanySegmentInformation].CompanyId = @CompanyID)              
 AND (@includeDeleted = 0 or [CompanySegmentInformation].IsDeleted = 0)          
END