﻿CREATE procedure [dbo].[SFab_GetCompanyDetailsbyProjectID]
(
	@CompanyId uniqueidentifier,
	@ProjectId uniqueidentifier,
	@AviraUserID uniqueidentifier = null
)
as
/*================================================================================
Procedure Name: [SFab_GetCompanyDetailsbyProjectID]
Author: Jagan
Create date: 26/06/2019
Description: Get list from company table by project ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
26/06/2019	jagan	    Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ProjectApprovedStatusId uniqueidentifier
select @ProjectApprovedStatusId = ID from ProjectStatus
where ProjectStatusName = 'Completed';
declare @IsMapped bit
if exists (select 1 from CompanyProjectMap cpmap
			inner join project proj
			on cpmap.ProjectId = proj.ID
			where cpmap.CompanyId = @CompanyId
			and proj.ProjectStatusID = @ProjectApprovedStatusId
			and cpmap.ProjectId = @ProjectId)
begin
set @IsMapped = 1
end
else 
begin
set @IsMapped = 0
end
	 select comp.Id as CompanyId,
	 comp.CompanyName,
	 comp.CompanyCode,
	 comp.DisplayName,
	 comp.LogoURL,
	 comp.NatureId,
	 comp.ParentCompanyName,
	 comp.ParentId,
	 comp.CompanyLocation,
	 comp.IsHeadQuarters,
	 comp.Telephone,
	 comp.Telephone2,
	 comp.Telephone3,
	 comp.Email,
	 comp.Email2,
	 comp.Email3,
	 comp.Website,
	 comp.YearOfEstb,
	 comp.CompanyStageId,
	 comp.NoOfEmployees,
	 comp.RankNumber,
	 comp.RankRationale,
	 comp.DisplayName,
	 comp.LogoURL,
	 comp.ActualImageName as ImageActualName
	from Company comp
	
	where comp.IsDeleted = 0 and comp.Id = @CompanyId
	and @IsMapped = 1;

	--Subsidary Companies
	SELECT [Id] as CompanySubsidaryId,
		[CompanyName] as SubsidaryCompanyName,
		[CompanyLocation] as SubsidaryCompanyLocation,		
		[IsHeadQuarters]
		FROM [dbo].[Company]
		where [Company].ParentId = @CompanyID 
		and [Company].[IsDeleted] = 0
		and [Company].IsHeadQuarters = 0
		and @IsMapped = 1;


	--Regions
	select 
	crm.RegionId
	from CompanyRegionMap crm
	where crm.CompanyId = @CompanyId
	and crm.IsDeleted = 0
	and @IsMapped = 1;
--Key employees
	select 
	ckem.Id,
	 ckem.KeyEmployeeName,
	 ckem.DesignationId,
	 ckem.[ManagerId],
	 mgr.KeyEmployeeName as ManagerName,
	 Designation.Designation as KeyEmployeeDesignation,
	 ckem.KeyEmployeeEmailId,
	 ckem.KeyEmployeeComments
	from CompanyKeyEmployeesMap ckem
	inner join Designation
	on Designation.Id = ckem.DesignationId
	left join CompanyKeyEmployeesMap mgr
	on ckem.[ManagerId] = mgr.id
	where ckem.CompanyId = @CompanyId
	and ckem.[IsDirector] = 0 
	and ckem.IsDeleted = 0
	and @IsMapped = 1;

--Directors
	select 
	 cbdm.Id ,
	 cbdm.DesignationId,
	 cbdm.[ManagerId],
	 cbdm.KeyEmployeeName,
	 cbdm.CompanyBoardOfDirecorsOtherCompanyName,
	 cbdm.CompanyBoardOfDirecorsOtherCompanyDesignation,
	 cbdm.KeyEmployeeEmailId,
	 cbdm.[ManagerId],
	 mgr.KeyEmployeeName as ManagerName
	from CompanyKeyEmployeesMap cbdm
	inner join Designation
	on Designation.Id = cbdm.DesignationId
	left join CompanyKeyEmployeesMap mgr
	on cbdm.[ManagerId] = mgr.id
	where cbdm.CompanyId = @CompanyId
	and cbdm.[IsDirector] = 1
	and cbdm.IsDeleted = 0
	and @IsMapped = 1;

--Share Holding
	select 
	cshpm.Id,
	cshpm.ShareHoldingName,
	 cshpm.EntityTypeId,
	 cshpm.ShareHoldingPercentage
	from ShareHoldingPatternMap cshpm
	where cshpm.CompanyId = @CompanyId
	and @IsMapped = 1;
end