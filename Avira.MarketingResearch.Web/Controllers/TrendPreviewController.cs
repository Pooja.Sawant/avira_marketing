﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.Utils;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Avira.MarketingResearch.Web.Controllers
{
    [NoCache]
    [Authorization]
    public class TrendPreviewController : BaseController
    {
        public readonly IServiceRepository _serviceRepository;
        private ILoggerManager _logger;

        public TrendPreviewController(IServiceRepository serviceRepository, ILoggerManager logger)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
        }

        public ActionResult Index(string Id)
        {
            SetRoleList();
            MainTrendPreviewViewModel model = new MainTrendPreviewViewModel();
            try
            {
                model.TrendId = new Guid(Id);
                HttpResponseMessage responseTrend = _serviceRepository.GetByIdResponse("Trend", Id);
                TrendViewModel trend = responseTrend.Content.ReadAsAsync<TrendViewModel>().Result;
                model.TrendModel = trend;

                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.GetByIdResponse(contollerName, Id);
                response.EnsureSuccessStatusCode();
                TrendPreviewViewModel trendPreviewViewModel = response.Content.ReadAsAsync<TrendPreviewViewModel>().Result;
                model.TrendPreviewModel = trendPreviewViewModel;

                HttpResponseMessage responseValue = _serviceRepository.GetByIdResponse("TrendValueMap", Id);
                response.EnsureSuccessStatusCode();
                List<TrendValuePreviewViewModel> trendValueViewModel = responseValue.Content.ReadAsAsync<List<TrendValuePreviewViewModel>>().Result;
                model.TrendValueViewModel = trendValueViewModel;
                model.TrendModel = trend;

                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, model.TrendModel.ProjectID);
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                }

                ViewBag.Id = model.TrendId;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(model);
        }

        private void SetRoleList()
        {
            try
            {
                var roles = HttpContext.Session.GetString("userRoles");
                var userRoles = JsonConvert.DeserializeObject<List<RoleViewModel>>(roles);
                List<string> RoleListItems = new List<string>();
                foreach (var item in userRoles)
                    RoleListItems.Add(item.UserRoleName);
                ViewBag.UserRoleList = RoleListItems;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

    }
}