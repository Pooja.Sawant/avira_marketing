﻿using Avira.MarketingResearch;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Avira.MarketingResearch.Model.ViewModels;
using SQLDatabaseQueryContainer;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using System.Collections;

namespace Avira.MarketingResearch.Repository.Implementations
{
    public class CountryRepository : ICountryRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "Country";
        public CountryRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public IEnumerable<CountryViewModel> GetAll(bool includeDeleted)
        {
            return GetAllCountry(null, includeDeleted);
        }

        public CountryViewModel GetById(Guid guid)
        {
            CountryViewModel countryViewModel = new CountryViewModel();
            countryViewModel = GetAllCountry(guid).FirstOrDefault();
            return countryViewModel;
        }

        private IEnumerable<CountryViewModel> GetAllCountry(Guid? guid, bool includeDeleted = false)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);

            var Country = dapper.Query<CountryViewModel>(CountryQueries.StoreProcSelectAll
                , new
                {
                    @RegionID = guid
                    ,
                    @includeDeleted = includeDeleted
                    ,
                    @IncludeUnassigned = true
                },null,true,null,CommandType.StoredProcedure
                );
            return Country;
        }
    }
}
