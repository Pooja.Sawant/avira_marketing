﻿using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MenuController : AviraBaseController
    {
        public readonly IMenuRepository _menuRepository;
        public readonly IMenuService _menuService;
        private ILoggerManager _logger;

        public MenuController(IMenuService menuService,IMenuRepository menuRepository, ILoggerManager logger)
        {
            _menuRepository = menuRepository;
            _menuService = menuService;
            _logger = logger;
        }
        [HttpGet]
        public ActionResult<IEnumerable<MenuViewModel>> Get()
        {
            List<MenuViewModel> res = null;
            try
            {
                res = _menuRepository.GetAll().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpPost]
        public ActionResult Post([FromBody] MenuInsertDbViewModel viewModel)
        {
            viewModel.CreatedOn = System.DateTime.UtcNow;
            viewModel.UserCreatedById = CurrentUser.Id;
            viewModel.Id = Guid.NewGuid();
            //
           
                if (ModelState.IsValid)
                {

                }
                IViewModel viewData = new ViewData()
                {
                    IsSuccess = false,
                    Message = "Error creating a new menu"
                };
                try
                {
                var result = _menuService.Create(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Success";
                }
                else
                {
                    viewData.Message = result.Error;
                    IViewModel duplicateViewData = new ViewData()
                    {
                        IsSuccess = false,
                        Message = "Menu name alredy exists"
                    };
                    return Ok(duplicateViewData);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }
    }
}