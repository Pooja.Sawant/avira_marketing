﻿using Avira.MarketingResearch.Common.IService.ProjectDashBoard;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Repository.CoreInterfaces.ProjectDashBoard;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.ProjectDashBoard
{
    public class ProjectSegmentMappingService : IProjectSegmentMappingService
    {
        private readonly IProjectSegmentMappingRepository _projectSegmentMappingRepository;
        public ProjectSegmentMappingService(IProjectSegmentMappingRepository buildLevelRepository)
        {
            _projectSegmentMappingRepository = buildLevelRepository;
        }

        public Result<SpTransactionMessage> Create(ProjectSegmentMappingInsertDbViewModel tfc, AviraUser currenUser)
        {
            return _projectSegmentMappingRepository.Create(tfc);
        }
        public Result<SpTransactionMessage> CreateProjectCode(ProjectSegmentMappingInsertDbViewModel tfc, AviraUser currenUser)
        {
            return _projectSegmentMappingRepository.CreateProjectCode(tfc);
        }

        public IEnumerable<ProjectSegmentMappingViewModel> GetAll(Guid Id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ProjectSegmentMappingViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false)
        {
            return _projectSegmentMappingRepository.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude);
        }

        public ProjectSegmentMappingViewModel GetById(Guid guid)
        {
            return _projectSegmentMappingRepository.GetById(guid);
        }


        public Result<SpTransactionMessage> Update(ProjectSegmentMappingUpdateDbViewModel tfc, AviraUser currenUser)
        {
            return _projectSegmentMappingRepository.Update(tfc);
        }
    }
}
