﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.CompanyProfile
{
    public class CompanyTrendsMapRepository : ICompanyTrendsMapRepository
    {
        protected IDbHelper _dbHelper;

        public CompanyTrendsMapRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }
        public IEnumerable<CompanyTrendsMapViewModel> GetAll(Guid companyId)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<CompanyTrendsMapViewModel>(CompanyTrendsMapQueries.StoreProcSelectById
                            , new {@CompanyID = companyId,@includeDeleted=false }
                            , null, true, null, CommandType.StoredProcedure);
        }

        public Result<SpTransactionMessage> InsertUpdate(MainCompanyTrendsMapInsertUpdateDbViewModel companyTrendsMapViewMoel)
        {
            DataTable CompanyTrendsTable = new DataTable();
            CompanyTrendsTable.Columns.Add("Id", typeof(Guid));
            CompanyTrendsTable.Columns.Add("TrendName", typeof(string));
            CompanyTrendsTable.Columns.Add("Department", typeof(string));
            CompanyTrendsTable.Columns.Add("CompanyId", typeof(Guid));
            CompanyTrendsTable.Columns.Add("ImportanceId", typeof(Guid));
            CompanyTrendsTable.Columns.Add("ImpactId", typeof(Guid));
            if (companyTrendsMapViewMoel.CompanyTrendsMapInsertUpdate != null & companyTrendsMapViewMoel.CompanyTrendsMapInsertUpdate.Count > 0)
            {
                foreach (var item in companyTrendsMapViewMoel.CompanyTrendsMapInsertUpdate)
                {
                    if(item.Id== Guid.Empty)
                    {
                        item.Id = Guid.NewGuid();
                    }
                    item.CompanyId = companyTrendsMapViewMoel.CompanyId;
                    CompanyTrendsTable.Rows.Add(new Object[]{item.Id, item.TrendName, item.Department, item.CompanyId, item.ImportanceId, item.ImpactId });
                }
            }


            var spTransactionMessage = _dbHelper.ExecuteQuery(CompanyTrendsMapQueries.StoreProcInsert,
               new
               {
                   @CompanyId = companyTrendsMapViewMoel.CompanyId,
                   @CompanyTrendsMap = CompanyTrendsTable,
                   @UserCreatedById = companyTrendsMapViewMoel.AviraUserId
               }
                    , CommandType.StoredProcedure);
            return spTransactionMessage;
        }
    }
}
