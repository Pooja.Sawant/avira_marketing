﻿CREATE procedure [dbo].[SApp_insertUpdateIndustry]  
(@IndustryID uniqueidentifier,  
@Description nvarchar(256),  
@IndustryName nvarchar(256),  
@ParentIndustryID uniqueidentifier =null,  
@IsDeleted bit,  
@AviraUserId uniqueidentifier)  
as  
/*================================================================================  
Procedure Name: SApp_DeleteIndustrybyID  
Author: Sharath  
Create date: 02/19/2019  
Description: soft delete a record from Industry table by ID  
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
02/19/2019 Sharath   Initial Version  
================================================================================*/  
BEGIN  
SET NOCOUNT ON;  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
declare @SegmentId uniqueidentifier;
select @SegmentId = Id from Segment
where SegmentName = 'End User';

IF exists(select 1 from [dbo].SubSegment   
   where ((@IndustryID is null or [Id] !=  @IndustryID) 
   and SegmentId = @SegmentId
   and  SubSegmentName = @IndustryName AND IsDeleted = 0))  
begin  
declare @ErrorMsg NVARCHAR(2048);  
set @ErrorMsg = 'Industry with Name "'+ @IndustryName + '" already exists.';  
THROW 50000,@ErrorMsg,1;  
end  
  
IF (@IndustryID is null)  
begin  
--Insert new record  
 set @IndustryID = NEWID();  
  
 insert into [dbo].SubSegment([Id],  
		SegmentId,
        [SubSegmentName],  
        [Description],  
        [ParentId],  
        [IsDeleted],  
        [UserCreatedById],  
        [CreatedOn])  
 values(@IndustryID, 
	@SegmentId,
   @IndustryName,   
   @Description,   
   @ParentIndustryID,  
   0,  
   @AviraUserId,   
   getdate());  
end  
else  
begin  
--update record  
 update [dbo].SubSegment
 set SubSegmentName = @IndustryName,  
  [Description] = @Description,
  SegmentId =@SegmentId,
  ParentID = @ParentIndustryID,  
  [ModifiedOn] = getdate(),  
  DeletedOn = case when IsDeleted = 0 and @IsDeleted = 1 then getdate() else DeletedOn end,  
  UserDeletedById = case when IsDeleted = 0 and @IsDeleted = 1 then @AviraUserId else UserDeletedById end,  
  IsDeleted = isnull(@IsDeleted, IsDeleted),  
  [UserModifiedById] = @AviraUserId  
     where ID = @IndustryID
end  
  
select @IndustryID;  
end