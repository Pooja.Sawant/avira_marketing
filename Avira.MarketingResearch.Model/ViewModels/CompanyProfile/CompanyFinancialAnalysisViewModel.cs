﻿using Avira.MarketingResearch.Model.ViewModels.Trend;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.CompanyProfile
{
    public class CompanyFinancialAnalysisViewModel
    {
        public Guid CompanyId { get; set; }
        public Guid CurrencyId { get; set; }
        public Guid UnitId { get; set; }
        public Guid CompanyStatusId { get; set; }
        public string AuthorNotes { get; set; }
        public string ApproverNotes { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public Guid Id { get; set; }
        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "CompanyFinancialAnalysis";
            }
        }
    }
    public class CompanyExpensesBreakdownViewModel
    {
        public Guid CompanyFinancialID { get; set; }
        public string ExpensesType { get; set; }
        public int Year { get; set; }
        public decimal ActualAmount { get; set; }
        public decimal ConversionAmount { get; set; }

        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public Guid Id { get; set; }
        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "CompanyExpensesBreakdown";
            }
        }
    }
    public class CompanyCashFlowsViewModel
    {
        public Guid CompanyFinancialID { get; set; }
        public string CashFlowType { get; set; }
        public int Year { get; set; }
        public decimal ActualAmount { get; set; }
        public decimal ConversionAmount { get; set; }

        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public Guid Id { get; set; }
        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "CompanyCashFlows";
            }
        }
    }
    public class CompanyAssetsLiabilitiesViewModel
    {
        public Guid CompanyFinancialID { get; set; }
        public string AssetsType { get; set; }
        public int Year { get; set; }
        public decimal ActualAmount { get; set; }
        public decimal ConversionAmount { get; set; }

        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public Guid Id { get; set; }
        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "CompanyAssetsLiabilities";
            }
        }
    }
    public class CompanyTransactionsViewModel
    {
        public Guid CompanyFinancialID { get; set; }
        public Guid CategoryId { get; set; }
        public decimal Value { get; set; }
        public string OtherParty { get; set; }
        public DateTime Date { get; set; }
        public string Rationale { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public Guid Id { get; set; }
        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "CompanyTransactions";
            }
        }
    }

    public class CompanyFinancialAnalysisInsertUpdateViewModel
    {
        public Guid CompanyId { get; set; }
        public Guid CurrencyId { get; set; }
        public Guid ValueConversionId { get; set; }
        //public Guid UnitId { get; set; }
        public Guid CompanyStatusId { get; set; }
        [Required]
        [Display(Name = "Author Notes")]
        public string AuthorNotes { get; set; }
        public string ApproverNotes { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public Guid Id { get; set; }

    }
    public class CompanyExpensesBreakdownInsertUpdateViewModel
    {
        public Guid CompanyFinancialID { get; set; }
        public string ExpensesType { get; set; }
        public int Year { get; set; }
        public decimal ActualAmount { get; set; }
        public decimal ConversionAmount { get; set; }

        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public Guid Id { get; set; }

    }

    public class ExpensesBreakdownViewModel
    {
        //public Guid CompanyFinancialID { get; set; }
        public string ExpensesType { get; set; }
        public int Year { get; set; }
        public decimal? BaseYearMinus_1 { get; set; }
        public decimal? BaseYearMinus_2 { get; set; }
        public decimal? BaseYearMinus_3 { get; set; }
        public decimal? BaseYearMinus_4 { get; set; }
        public decimal? BaseYearMinus_5 { get; set; }
        public decimal ActualAmount { get; set; }
        public decimal ConversionAmount { get; set; }
        //public Guid Id { get; set; }
    }

    public class AssetsLiabilitiesViewModel
    {
        //public Guid CompanyFinancialID { get; set; }
        public string AssetsType { get; set; }
        public decimal ActualAmount { get; set; }
        public decimal ConversionAmount { get; set; }
        public decimal? BaseYearMinus_1 { get; set; }
        public decimal? BaseYearMinus_2 { get; set; }
        public decimal? BaseYearMinus_3 { get; set; }
        public decimal? BaseYearMinus_4 { get; set; }
        public decimal? BaseYearMinus_5 { get; set; }
        //public Guid Id { get; set; }
    }

    public class CashFlowsViewModel
    {
        //public Guid CompanyFinancialID { get; set; }
        public string CashFlowType { get; set; }
        public decimal ActualAmount { get; set; }
        public decimal ConversionAmount { get; set; }
        public decimal? BaseYearMinus_1 { get; set; }
        public decimal? BaseYearMinus_2 { get; set; }
        public decimal? BaseYearMinus_3 { get; set; }
        public decimal? BaseYearMinus_4 { get; set; }
        public decimal? BaseYearMinus_5 { get; set; }
        //public Guid Id { get; set; }

    }

    public class CompanyCashFlowsInsertUpdateViewModel
    {
        public Guid CompanyFinancialID { get; set; }
        public string CashFlowType { get; set; }
        public int Year { get; set; }
        public decimal ActualAmount { get; set; }
        public decimal ConversionAmount { get; set; }

        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public Guid Id { get; set; }

    }
    public class CompanyAssetsLiabilitiesInsertUpdateViewModel
    {
        public Guid CompanyFinancialID { get; set; }
        public string AssetsType { get; set; }
        public int Year { get; set; }
        public decimal ActualAmount { get; set; }
        public decimal ConversionAmount { get; set; }

        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public Guid Id { get; set; }

    }
    public class CompanyTransactionsInsertUpdateViewModel
    {
        public Guid CategoryId { get; set; }
        public decimal Value { get; set; }
        public string OtherParty { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
        public string Rationale { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public Guid Id { get; set; }

    }

    public class MainCompanyFinancialAnalysisInsertUpdateViewModel
    {
        [Required]
        public string AuthorNotes { get; set; }
        [Required]
        public string ApproverNotes { get; set; }
        public BaseYearModel baseYearModel { get; set; }
        public int RowCount { get; set; }
        public Guid ValueConversionId { get; set; }
        public Guid ProjectId { get; set; }
        public CompanyNoteViewModel CompanyNoteViewModel { get; set; }
        public CompanyFinancialAnalysisInsertUpdateViewModel companyFinancialAnalysisInsertUpdateViewModel { get; set; }
        public List<CompanyExpensesBreakdownInsertUpdateViewModel> companyExpensesBreakdownInsertUpdateViewModel { get; set; }
        public List<CompanyCashFlowsInsertUpdateViewModel> companyCashFlowsInsertUpdateViewModel { get; set; }
        public List<CompanyAssetsLiabilitiesInsertUpdateViewModel> companyAssetsLiabilitiesInsertUpdateViewModel { get; set; }
        public List<CompanyTransactionsInsertUpdateViewModel> companyTransactionsInsertUpdateViewModel { get; set; }
        public List<AssetsLiabilitiesViewModel> assetsLiabilitiesViewModels { get; set; }
        public List<CashFlowsViewModel> cashFlowsViewModel { get; set; }
        public List<ExpensesBreakdownViewModel> expensesBreakdownViewModel { get; set; }
        public string jsonAssets { get; set; }
        public string jsonExpence { get; set; }
        public string jsonCash { get; set; }
        public string jsonTrans { get; set; }
        public Guid? UserCreatedById { get; set; }
    }
    public class CompanyFinancialAnalysisInsertUpdateModel
    {
        public BaseYearModel baseYearModel { get; set; }
        [Required]
        public string AuthorNotes { get; set; }
        [Required]
        public string ApproverNotes { get; set; }
        public int RowCount { get; set; }
        public Guid CompanyId { get; set; }
        public Guid CompanyFinancialId { get; set; }
        public Guid Id { get; set; }
        public Guid CurrencyId { get; set; }
        public Guid ValueConversionId { get; set; }
        //public CompanyNoteViewModel CompanyNoteViewModel { get; set; }
        //public CompanyFinancialAnalysisInsertUpdateViewModel companyFinancialAnalysisInsertUpdateViewModel { get; set; }
        public List<CompanyTransactionsInsertUpdateViewModel> companyTransactionsInsertUpdateViewModel { get; set; }
        public List<AssetsLiabilitiesViewModel> assetsLiabilitiesViewModels { get; set; }
        public List<CashFlowsViewModel> cashFlowsViewModel { get; set; }
        public List<ExpensesBreakdownViewModel> expensesBreakdownViewModel { get; set; }
        public string jsonAssets { get; set; }
        public string jsonExpence { get; set; }
        public string jsonCash { get; set; }
        public string jsonTrans { get; set; }
        public Guid? UserCreatedById { get; set; }
    }
    public class BaseYearModel
    {
        public int Id { get; set; }
        public int BaseYear { get; set; }
    }
}