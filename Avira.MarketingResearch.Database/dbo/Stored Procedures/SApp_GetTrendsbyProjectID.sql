﻿ CREATE PROCEDURE [dbo].[SApp_GetTrendsbyProjectID]        
 (@ProjectID uniqueidentifier = null,    
 @includeDeleted bit = 0,    
 @OrdbyByColumnName NVARCHAR(50) ='TrendName',    
 @SortDirection INT = -1,    
 @PageStart INT = 0,  
 @TrendName NVARCHAR(512)='',    
 @PageSize INT = null)        
 AS        
/*================================================================================        
Procedure Name: SApp_GetTrendsbyProjectID   
Author: Harshal        
Create date: 04/12/2019        
Description: Get list from Trends table by ProjectID        
Change History        
Date  Developer  Reason        
__________ ____________ ______________________________________________________        
04/12/2019 Harshal   Initial Version        
================================================================================*/       
        
BEGIN        
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED     
if @includeDeleted is null set @includeDeleted = 0;  
if len(@TrendName)>2 set @TrendName = '%'+ @TrendName + '%'  
else set @TrendName = ''  
if @PageSize is null or @PageSize =0    
set @PageSize = 10    
     
begin    
;WITH CTE_TBL AS (    
SELECT        Trend.Id, Trend.TrendName, Trend.IsDeleted, Project.ProjectName, Project.ProjectCode, 
                         CASE WHEN @OrdbyByColumnName = 'TrendName' THEN [Trend].TrendName WHEN @OrdbyByColumnName = 'Description' THEN [Trend].[Description] ELSE [Trend].TrendName END AS SortColumn, 
                         TrendStatus.TrendStatusName, TrendStatus.Id AS TrendStatusID
FROM            Trend INNER JOIN
                         Project ON Project.ID = Trend.ProjectID INNER JOIN
                         TrendStatus ON Trend.TrendStatusID = TrendStatus.Id
WHERE        (Trend.ProjectID = @ProjectID) AND (@TrendName = '') AND (@includeDeleted = 1) OR
                         (Trend.ProjectID = @ProjectID) AND (@includeDeleted = 1) AND (Trend.TrendName LIKE @TrendName) OR
                         (Trend.ProjectID = @ProjectID) AND (@TrendName = '') AND (Trend.IsDeleted = 0) OR
                         (Trend.ProjectID = @ProjectID) AND (Trend.TrendName LIKE @TrendName) AND (Trend.IsDeleted = 0)   
)    
    
 SELECT     
    CTE_TBL.*,    
    tCountResult.TotalItems    
  FROM CTE_TBL    
  CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult    
  ORDER BY     
    CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,    
    CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC     
  OFFSET @PageStart ROWS    
  FETCH NEXT @PageSize ROWS ONLY;    
  END    
    
END