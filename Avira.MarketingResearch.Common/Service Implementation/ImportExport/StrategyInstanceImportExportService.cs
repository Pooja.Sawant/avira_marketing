﻿using Avira.MarketingResearch.Common.IService.ImportExport;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.ImportExport
{
    public class StrategyInstanceImportExportService : IStrategyInstanceImportExportService
    {
        private readonly IStrategyInstanceImportExportRepository _StrategyIntanceImportExportRepository;

        public StrategyInstanceImportExportService(IStrategyInstanceImportExportRepository buildLevelRepository)
        {
            _StrategyIntanceImportExportRepository = buildLevelRepository;
        }

        public IEnumerable<ImportCommonResponseModel> Create(StrategyIntanceImportModel viewModel)
        {
            viewModel.JsonStrategyList = JsonConvert.SerializeObject(viewModel.StrategyList);
            var ImportResponse = _StrategyIntanceImportExportRepository.Create(viewModel);
            return ImportResponse;
        }
    }
}
