﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.ImportExport;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportGenerationImportExportController : AviraBaseController
    {
        private ILoggerManager _logger;
        public readonly IReportGenerationImportExportService _reportGenerationImportExportService;

        public ReportGenerationImportExportController(
            IReportGenerationImportExportService reportGenerationImportExportService,
            ILoggerManager logger)
        {
            _reportGenerationImportExportService = reportGenerationImportExportService;
            _logger = logger;
        }

        [HttpPost]
        public ActionResult Post([FromBody] ReportGenerationImportModel viewModel)
        {
            viewModel.UserCreatedById = CurrentUser.Id;
            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new Report Generation, please contact Admin for more details."
            };
            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                var result = _reportGenerationImportExportService.Create(viewModel, CurrentUser);

                var res = result.Where(s => s.ErrorNotes != null).ToList();

                if (res.Count <= 0)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Report Generation template imported successfully.";
                }
                else
                {
                    viewData.Message = "Report Generation template Import has some errors";
                    viewData.cpData = result.ToList();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);

                viewData = new ViewData()
                {
                    IsSuccess = false,
                    Message = "Error while creating a new Report Genertion. Error Details: " + ex.Message + ". please contact Admin for more details."
                };
            }
            return Ok(viewData);
        }
    }
}