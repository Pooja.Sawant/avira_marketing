﻿using Avira.MarketingResearch.Common.IService.QualitativeInfo;
using Avira.MarketingResearch.Model.ViewModels.QualitativeInfo;
using Avira.MarketingResearch.Repository.CoreInterfaces.QualitativeInfo;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.QualitativeInfo
{
    public class QualitativeInfoTrackerService : IQualitativeInfoTrackerService
    {
        private readonly IQualitativeInfoTrackerRepository _QualitativeInfoTrackerRepository;

        public QualitativeInfoTrackerService(IQualitativeInfoTrackerRepository QualitativeInfoTrackerRepository)
        {
            _QualitativeInfoTrackerRepository = QualitativeInfoTrackerRepository;
        }
        public IEnumerable<QualitativeInfoTrackerViewModel> GetAll(Guid? Id, int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false)
        {
            return _QualitativeInfoTrackerRepository.GetAll(Id, PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude);
        }
    }
}
