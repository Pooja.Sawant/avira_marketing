﻿using Avira.MarketingResearch;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Avira.MarketingResearch.Model.ViewModels;
using SQLDatabaseQueryContainer;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using System.Collections;

namespace Avira.MarketingResearch.Repository
{
    public class RegionRepository : IRegionRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "Region";
        public RegionRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public IEnumerable<RegionCountryModel> GetRegionByTrendID(Guid trendId)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            var result = dapper.Query<RegionCountryModel>(RegionQueries.StoreProcSelectAllRegions
                            , null
                            , null, true, null, CommandType.StoredProcedure);

            DataTable regionIdsTable = new DataTable();
            regionIdsTable.Columns.Add("ID", typeof(Guid));
            foreach (var item in result)
            {
                if (item.RegionId != null)
                {
                    regionIdsTable.Rows.Add(item.RegionId);
                }
            }

            var countries = dapper.Query<CountryViewModel>(RegionQueries.StoreProcSelectAllCountries
                    , new
                    {
                        @TrendID = trendId,
                        @RegionIDList = regionIdsTable.AsTableValuedParameter("dbo.udtUniqueIdentifier")
                    }
                    , null, true, null, CommandType.StoredProcedure).ToList();

            List<RegionCountryModel> ListRVM = new List<RegionCountryModel>();
            List<CountryViewModel> ListCVM = new List<CountryViewModel>();
            
            foreach (var region in result.ToList())
            {
                foreach (var country in countries)
                {
                    if (region.RegionId == country.RegionId)
                    {
                        CountryViewModel CVM = new CountryViewModel();
                        CVM.CountryCode = country.CountryCode;
                        CVM.CountryId = country.CountryId;
                        CVM.CountryName = country.CountryName;
                        CVM.IsAssigned = country.IsAssigned;
                        CVM.CreatedOn = country.CreatedOn;
                        CVM.EndUser = country.EndUser;
                        CVM.Impact = country.Impact;
                        CVM.ParentRegionId = country.ParentRegionId;
                        CVM.RegionId = country.RegionId;
                        CVM.RegionLevel = country.RegionLevel;
                        CVM.RegionName = country.RegionName;
                        CVM.TrendId = country.TrendId;
                        CVM.TrendRegionMapId = country.TrendRegionMapId;
                        ListCVM.Add(CVM);
                    }
                }

                if (ListCVM != null && !ListCVM.Equals(null))
                {
                    region.CountryList = new List<CountryViewModel>();
                    region.CountryList.AddRange(ListCVM);
                    ListCVM.Clear();
                }
            }

            return result;
        }

        public RegionViewModel GetById(Guid guid)
        {
            RegionViewModel regionViewModel = new RegionViewModel();
            regionViewModel = GetAllRegion(guid).FirstOrDefault();
            return regionViewModel;
        }

        public IEnumerable<RegionViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted)
        {
            return GetAllRegion(null, includeDeleted);
        }

        public Result<SpTransactionMessage> Create(RegionInsertDbViewModel viewModel)
        {
            DataTable countryIdsTable = new DataTable();
            countryIdsTable.Columns.Add("ID", typeof(Guid));
            if (viewModel.CountryIDList.Count > 0)
            {
                foreach (var item in viewModel.CountryIDList)
                {
                    countryIdsTable.Rows.Add(item);
                }
            }

            var spTransactionMessage = _dbHelper.ExecuteQuery(RegionQueries.StoreProcInsert,
                 new
                 {
                     @Id = viewModel.Id,
                     @RegionName = viewModel.RegionName,
                     @RegionLevel = 1,
                     @ParentRegionId = (Guid?)null,
                     @CountryIDList = countryIdsTable.AsTableValuedParameter("dbo.udtUniqueIdentifier"),
                     @UserCreatedById = viewModel.UserCreatedById,
                     @CreatedOn = viewModel.CreatedOn
                 }, CommandType.StoredProcedure);

            return spTransactionMessage;
        }

        public Result<SpTransactionMessage> Update(RegionUpdateDbViewModel viewModel)
        {
            DataTable countryIdsTable = new DataTable();
            countryIdsTable.Columns.Add("ID", typeof(Guid));
            if (viewModel.CountryIDList.Count > 0)
            {
                foreach (var item in viewModel.CountryIDList)
                {
                    countryIdsTable.Rows.Add(item);
                }
            }

            var spTransactionMessage = _dbHelper.ExecuteQuery(RegionQueries.StoreProcUpdate,
                 new
                 {
                     @Id = viewModel.Id,
                     @RegionName = viewModel.RegionName,
                     @RegionLevel = 1,
                     @ParentRegionId = (Guid?)null,
                     @CountryIDList = countryIdsTable.AsTableValuedParameter("dbo.udtUniqueIdentifier"),
                     @UserModifiedById = viewModel.UserModifiedById,
                     @UserDeletedById = viewModel.UserDeletedById,
                     @IsDeleted = viewModel.IsDeleted,
                     @ModifiedOn = viewModel.ModifiedOn,
                     @DeletedOn = viewModel.DeletedOn
                 }, CommandType.StoredProcedure);

            return spTransactionMessage;
        }

        private IEnumerable<RegionViewModel> GetAllRegion(Guid? guid, bool includeDeleted = false)
        {
            DataTable regionIdsTable = new DataTable();
            regionIdsTable.Columns.Add("ID", typeof(Guid));
            if (guid != null)
            {
                regionIdsTable.Rows.Add(guid);
            }

            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            var Region = dapper.Query<RegionViewModel>(RegionQueries.StoreProcSelectAll
                , new
                {
                    @RegionIDList = regionIdsTable.AsTableValuedParameter("dbo.udtUniqueIdentifier")
                    ,
                    @ParentRegionID = (Guid?)null
                    ,
                    @RegionName = string.Empty
                    ,
                    @RegionLevel = (int?)null
                    ,
                    @includeDeleted = includeDeleted
                }
                , null, true, null, CommandType.StoredProcedure);

            foreach (var item in Region)
            {
                var CountryMapping = dapper.Query<CountryViewModel>(RegionQueries.StoreProcCountryMapping
                        , new
                        {
                            @RegionID = item.Id,
                            @includeDeleted = includeDeleted,
                            @IncludeUnassigned = 0
                        }
                        , null, true, null, CommandType.StoredProcedure);
                if (CountryMapping != null)
                {
                    Region.Where(c => c.CountryList == null).FirstOrDefault().CountryList = CountryMapping.ToList();
                }
            }

            return Region;
        }
    }
}
