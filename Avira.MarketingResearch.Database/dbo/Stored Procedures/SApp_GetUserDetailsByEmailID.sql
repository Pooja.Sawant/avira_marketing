﻿CREATE procedure [dbo].[SApp_GetUserDetailsByEmailID]      
(@EmailAddress nvarchar(128))      
as      
/*================================================================================      
Procedure Name: [SApp_GetUserDetailsByEmailID]      
Author: Gopi      
Create date: 04/01/2019      
Description: Get userdetails from AviraUserCountry table      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
04/01/2019 Gopi   Initial Version     
27/05/2019 Sai Krishna  Getting the UserRolename along with all the other details.  
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
      
SELECT ur.UserRoleName,  
    au.*   
FROM [dbo].[AviraUser] au  
Inner Join UserRole ur  
On au.UserRoleId = ur.Id    
WHERE au.EmailAddress = @EmailAddress   
AND au.IsDeleted = 0  
END