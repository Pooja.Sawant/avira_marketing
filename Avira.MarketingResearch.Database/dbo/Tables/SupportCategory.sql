﻿CREATE TABLE [dbo].[SupportCategory] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [CategoryName]     NVARCHAR (256)   CONSTRAINT [DF_SupportCategory_CategoryName] DEFAULT ('') NOT NULL,
    [Description]      NVARCHAR (256)   NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_SupportCategory_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_SupportCategory] PRIMARY KEY CLUSTERED ([Id] ASC)
);

