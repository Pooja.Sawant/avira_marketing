﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductStatusController : AviraBaseController
    {
        public readonly IProductStatusRepository _iProductStatusRepository;
        public readonly IProductStatusService _iProductStatusService;
        private ILoggerManager _logger;

        public ProductStatusController(
            IProductStatusService iProductStatusService,
            IProductStatusRepository iProductStatusRepository,
            ILoggerManager logger)
        {
            _iProductStatusRepository = iProductStatusRepository;
            _iProductStatusService = iProductStatusService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ProductStatusViewModel>> Get()
        {
            List<ProductStatusViewModel> res = null;
            try
            {
                res = _iProductStatusService.GetAll().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }
    }
}