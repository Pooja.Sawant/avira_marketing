﻿CREATE procedure [dbo].[SApp_DeleteIndustrybyID]
(@IndustryID uniqueidentifier,
@AviraUserId uniqueidentifier)
as
/*================================================================================
Procedure Name: SApp_DeleteIndustrybyID
Author: Sharath
Create date: 02/21/2019
Description: soft delete a record from Industry table by ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
02/21/2019	Sharath			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @SegmentId uniqueidentifier;
select @SegmentId = Id from Segment
where SegmentName = 'End User';


UPDATE [dbo].[SubSegment]
   SET [IsDeleted] = 1,
   [DeletedOn] = getdate(),
   [UserDeletedById] = @AviraUserId
where ID = @IndustryID
and SegmentId = @SegmentId
and IsDeleted = 0 -- no need to delete record which is already deleted

end