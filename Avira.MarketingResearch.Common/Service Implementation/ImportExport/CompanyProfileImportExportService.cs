﻿using Avira.MarketingResearch.Common.IService.ImportExport;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport;
using Avira.MarketingResearch.Repository.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.ImportExport
{
    public class CompanyProfileImportExportService : ICompanyProfileImportExportService
    {
        private readonly ICompanyProfileImportExportRepository _companyProfileImportExportRepository;

        public CompanyProfileImportExportService(ICompanyProfileImportExportRepository buildLevelRepository)
        {
            _companyProfileImportExportRepository = buildLevelRepository;
        }
        public List<ImportCommonResponseModel> Create(CompanyProfileImportInsertViewModel viewModel, AviraUser currentUser)
        {            
            viewModel.JsonFundamentals = JsonConvert.SerializeObject(viewModel.CompanyProfileFundamentalsImportModel);
            viewModel.JsonShareHolding = JsonConvert.SerializeObject(viewModel.CompanyProfileShareHoldingImportModel);
            viewModel.JsonSubsidiaries = JsonConvert.SerializeObject(viewModel.CompanyProfileSubsidiariesImportModel);
            viewModel.JsonKeyEmployees = JsonConvert.SerializeObject(viewModel.CompanyKeyEmployeesImportModel);
            viewModel.JsonBoardOfDirectors = JsonConvert.SerializeObject(viewModel.CompanyBoardOfDirectorsImportModel);
            viewModel.JsonProduct = JsonConvert.SerializeObject(viewModel.CompanyProductImportModel);
            viewModel.JsonStrategy = JsonConvert.SerializeObject(viewModel.CompanyStrategyImportModel);
            viewModel.JsonClients = JsonConvert.SerializeObject(viewModel.CompanyClientsImportModel);            
            viewModel.JsonNews = JsonConvert.SerializeObject(viewModel.CompanyNewsImportModel);
            viewModel.JsonSWOT = JsonConvert.SerializeObject(viewModel.CompanySWOTImportModel);
            viewModel.JsonEcosystemPresence = JsonConvert.SerializeObject(viewModel.CompanyEcosystemPresenceImportModel);
            viewModel.JsonTrends = JsonConvert.SerializeObject(viewModel.CompanyTrendsImportModel);
            //viewModel.JsonAnalystView = JsonConvert.SerializeObject(viewModel.CompanyAnalystViewImportModel);
            viewModel.JsonPriceVolume = JsonConvert.SerializeObject(viewModel.PriceVolumeViewImportModel);
            //viewModel.JsonESMSegment = JsonConvert.SerializeObject(viewModel.CompanyProfilesESMSegmentListImportModel);
            var trendImportResponse = _companyProfileImportExportRepository.Create(viewModel);
            return trendImportResponse;
        }
    }
}
