﻿CREATE procedure [dbo].[SApp_UpdateStagingDownloads_practice]      
as      
/*================================================================================      
Procedure Name: SApp_UpdateStagingDownloads     
Author: Raju    
Create date: 13/06/2019      
Description: to update StagingDownloads          
Date       Developer    Reason      
__________ ____________ ______________________________________________________      
12/03/2020   Raju      Initial Version      
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)
DECLARE @ProjectId uniqueidentifier  
SELECT @ProjectId = Id FROM  project 
WHERE ProjectName = (select top 1 ProjectName from StagingDownloadsExportData SD)        
DECLARE @ModuleTypeId uniqueidentifier  
SELECT @ModuleTypeId = Id FROM  ModuleType 
where ModuleName = (select top 1 ModuleName from StagingDownloadsExportData SD)

       
Begin try
UPDATE [dbo].[StagingDownloadsExportData]
      SET 
      [ProjectName] = ProjectName,
      [ModuleName] = ModuleName, 
      [Header] = Header, 
      [Footer] = Footer, 
      [ReportTitle] = ReportTitle,
      [CoverPageImageLink] = CoverPageImageLink,
      [CoverPageImageName] = CoverPageImageName, 
      [Overview] = Overview, 
      [KeyPoints] = KeyPoints, 
      [Methodology] = Methodology,
      [MethodologyImageLink] = MethodologyImageLink, 
      [ContactUsAnalystName] = ContactUsAnalystName, 
      [ContactUsOfficeContactNo] = ContactUsOfficeContactNo,
      [ContactUsAnalystEmailId] = ContactUsAnalystEmailId, 
      [CreatedOn] = CreatedOn, 
      [UserCreatedById] = UserCreatedById,
      [ErrorNotes] = ErrorNotes;

;With cteEcoSystem as (           
 select SD.ProjectName,     
    CASE WHEN SD.ProjectName IS NULL 
	THEN 'please enter project name' 
	ELSE Null  
	END As ProjectNameError, 
	Sd.id as stagingid,     
  CASE WHEN  SD.header IS NULL 
  THEN 'Please provide Header Name'  
  ELSE NULL 
  END AS HeaderNameError,
  CASE  WHEN  SD.Footer IS NULL 
  THEN 'Please provide Footer Name'  
  ELSE NULL END AS FooterNameError,
  CASE 
  WHEN  SD.ReportTitle IS NULL 
  THEN 'Please provide ReportTitle Name' 
  ELSE NULL
  END AS ReportTitleNameError,
  CASE 
  WHEN SD.CoverPageImageLink IS NULL 
  THEN 'Please provide CoverPageImageLink' 
  ELSE NULL
  END AS CoverPageImageLinkError,
  CASE 
  WHEN SD.Methodology IS NULL 
  THEN 'Please provide Methodology' 
  ELSE NULL
  END AS methodologyNameError,
  CASE 
  WHEN SD.Overview IS NULL 
  THEN 'Please provide overview' 
  ELSE NULL
  END AS OverViewNameError,
  CASE 
  WHEN SD.KeyPoints IS NULL 
  THEN 'Please provide keypoints' 
  ELSE NULL
  END AS KeyPointsError,
  CASE 
  WHEN SD.ContactUsAnalystName 
  IS NULL THEN 'Please enter analystname' 
  ELSE NULL
  END AS AnalystNameError,
  CASE 
  WHEN SD.ContactUsOfficeContactNo IS NULL 
  THEN 'Please enter office contact number' 
  ELSE NULL
  END AS ConatactnumError,
  CASE 
  WHEN SD.ContactUsAnalystEmailId IS NULL 
  THEN 'Please enter analystemailid' 
  ELSE NULL
  END AS AnalystEmailIdError
 from  StagingDownloadsExportData SD 
 Where ProjectId = @ProjectId  and ModuleTypeId = @ModuleTypeId
 )
Update StagingDownloadsExportData
SET ErrorNotes = ISNULL(cteEcoSystem.ProjectNameError,'') 
+ IsNull(cteEcoSystem.HeaderNameError, '') + IsNull(cteEcoSystem.FooterNameError, '')
+ IsNull(cteEcoSystem.ReportTitleNameError, '') + IsNull(cteEcoSystem.CoverPageImageLinkError, '')
+ IsNull(cteEcoSystem.methodologyNameError, '') + IsNull(cteEcoSystem.OverViewNameError, '')
+ IsNull(cteEcoSystem.KeyPointsError, '') + IsNull(cteEcoSystem.AnalystNameError, '')
+ IsNull(cteEcoSystem.AnalystEmailIdError, '') + IsNull(cteEcoSystem.ConatactnumError, '')
From cteEcoSystem
Inner Join StagingDownloadsExportData sd
On cteEcoSystem.stagingid = sd.Id;

;With cteCP as (           
 select SD.ProjectName,     
    CASE WHEN SD.ProjectName IS NULL 
	THEN 'please enter project name' 
	ELSE Null  
	END As ProjectNameError, 
	Sd.id as stagingid,     
  CASE WHEN  SD.header IS NULL 
  THEN 'Please provide Header Name'  
  ELSE NULL 
  END AS HeaderNameError,
  CASE  WHEN  SD.Footer IS NULL 
  THEN 'Please provide Footer Name'  
  ELSE NULL END AS FooterNameError,
  CASE 
  WHEN  SD.ReportTitle IS NULL 
  THEN 'Please provide ReportTitle Name' 
  ELSE NULL
  END AS ReportTitleNameError,
  CASE 
  WHEN SD.CoverPageImageLink IS NULL 
  THEN 'Please provide CoverPageImageLink' 
  ELSE NULL
  END AS CoverPageImageLinkError,
  CASE 
  WHEN SD.Methodology IS NULL 
  THEN 'Please provide Methodology' 
  ELSE NULL
  END AS methodologyNameError,
  CASE 
  WHEN SD.ContactUsAnalystName 
  IS NULL THEN 'Please enter analystname' 
  ELSE NULL
  END AS AnalystNameError,
  CASE 
  WHEN SD.ContactUsOfficeContactNo IS NULL 
  THEN 'Please enter office contact number' 
  ELSE NULL
  END AS ConatactnumError,
  CASE 
  WHEN SD.ContactUsAnalystEmailId IS NULL 
  THEN 'Please enter analystemailid' 
  ELSE NULL
  END AS AnalystEmailIdError
 from  StagingDownloadsExportData SD 
 Where ProjectId = @ProjectId  and ModuleTypeId = @ModuleTypeId
 )
Update StagingDownloadsExportData
SET ErrorNotes = ISNULL(cteCP.ProjectNameError,'') 
+ IsNull(cteCP.HeaderNameError, '') + IsNull(cteCP.FooterNameError, '')
+ IsNull(cteCP.ReportTitleNameError, '') + IsNull(cteCP.CoverPageImageLinkError, '')
+ IsNull(cteCP.methodologyNameError, '') + IsNull(cteCP.AnalystNameError, '')
+ IsNull(cteCP.AnalystEmailIdError, '') + IsNull(cteCP.ConatactnumError, '')
From cteCP
Inner Join StagingDownloadsExportData sd
On cteCP.stagingid = sd.Id;
;With cteTrends as (           
 select SD.ProjectName,     
    CASE WHEN SD.ProjectName IS NULL 
	THEN 'please enter project name' 
	ELSE Null  
	END As ProjectNameError, 
	Sd.id as stagingid,     
  CASE WHEN  SD.header IS NULL 
  THEN 'Please provide Header Name'  
  ELSE NULL 
  END AS HeaderNameError,
  CASE  WHEN  SD.Footer IS NULL 
  THEN 'Please provide Footer Name'  
  ELSE NULL END AS FooterNameError,
  CASE 
  WHEN  SD.ReportTitle IS NULL 
  THEN 'Please provide ReportTitle Name' 
  ELSE NULL
  END AS ReportTitleNameError,
  CASE 
  WHEN SD.CoverPageImageLink IS NULL 
  THEN 'Please provide CoverPageImageLink' 
  ELSE NULL
  END AS CoverPageImageLinkError,
  CASE 
  WHEN SD.Methodology IS NULL 
  THEN 'Please provide Methodology' 
  ELSE NULL
  END AS methodologyNameError,
  CASE 
  WHEN SD.ContactUsAnalystName 
  IS NULL THEN 'Please enter analystname' 
  ELSE NULL
  END AS AnalystNameError,
  CASE 
  WHEN SD.ContactUsOfficeContactNo IS NULL 
  THEN 'Please enter office contact number' 
  ELSE NULL
  END AS ConatactnumError,
  CASE 
  WHEN SD.ContactUsAnalystEmailId IS NULL 
  THEN 'Please enter analystemailid' 
  ELSE NULL
  END AS AnalystEmailIdError
 from  StagingDownloadsExportData SD 
 Where ProjectId = @ProjectId  and ModuleTypeId = @ModuleTypeId
 )
Update StagingDownloadsExportData
SET ErrorNotes = ISNULL(cteTrends.ProjectNameError,'') 
+ IsNull(cteTrends.HeaderNameError, '') + IsNull(cteTrends.FooterNameError, '')
+ IsNull(cteTrends.ReportTitleNameError, '') + IsNull(cteTrends.CoverPageImageLinkError, '')
+ IsNull(cteTrends.methodologyNameError, '')+ IsNull(cteTrends.AnalystNameError, '')
+ IsNull(cteTrends.AnalystEmailIdError, '') + IsNull(cteTrends.ConatactnumError, '')

From cteTrends
Inner Join StagingDownloadsExportData sd
On cteTrends.stagingid = sd.Id;
;With cteInfographics as (           
 select SD.ProjectName,     
    CASE WHEN SD.ProjectName IS NULL 
	THEN 'please enter project name' 
	ELSE Null  
	END As ProjectNameError, 
	Sd.id as stagingid,     
  CASE WHEN  SD.header IS NULL 
  THEN 'Please provide Header Name'  
  ELSE NULL 
  END AS HeaderNameError,
  CASE  WHEN  SD.Footer IS NULL 
  THEN 'Please provide Footer Name'  
  ELSE NULL END AS FooterNameError,
  CASE 
  WHEN  SD.ReportTitle IS NULL 
  THEN 'Please provide ReportTitle Name' 
  ELSE NULL
  END AS ReportTitleNameError,
  CASE 
  WHEN SD.CoverPageImageLink IS NULL 
  THEN 'Please provide CoverPageImageLink' 
  ELSE NULL
  END AS CoverPageImageLinkError,
  CASE 
  WHEN SD.Methodology IS NULL 
  THEN 'Please provide Methodology' 
  ELSE NULL
  END AS methodologyNameError,
  CASE 
  WHEN SD.ContactUsAnalystName 
  IS NULL THEN 'Please enter analystname' 
  ELSE NULL
  END AS AnalystNameError,
  CASE 
  WHEN SD.ContactUsOfficeContactNo IS NULL 
  THEN 'Please enter office contact number' 
  ELSE NULL
  END AS ConatactnumError,
  CASE 
  WHEN SD.ContactUsAnalystEmailId IS NULL 
  THEN 'Please enter analystemailid' 
  ELSE NULL
  END AS AnalystEmailIdError
 from  StagingDownloadsExportData SD 
 Where ProjectId = @ProjectId  and ModuleTypeId = @ModuleTypeId
 )
Update StagingDownloadsExportData
SET ErrorNotes = ISNULL(cteInfographics.ProjectNameError,'') 
+ IsNull(cteInfographics.HeaderNameError, '') + IsNull(cteInfographics.FooterNameError, '')
+ IsNull(cteInfographics.ReportTitleNameError, '') + IsNull(cteInfographics.CoverPageImageLinkError, '')
+ IsNull(cteInfographics.methodologyNameError, '')+ IsNull(cteInfographics.AnalystNameError, '')
+ IsNull(cteInfographics.AnalystEmailIdError, '') + IsNull(cteInfographics.ConatactnumError, '')
From cteInfographics
Inner Join StagingDownloadsExportData sd
On cteInfographics.stagingid = sd.Id;

;With cteMarketTree as (           
 select SD.ProjectName,     
    CASE WHEN SD.ProjectName IS NULL 
	THEN 'please enter project name' 
	ELSE Null  
	END As ProjectNameError, 
	Sd.id as stagingid,     
  CASE WHEN  SD.header IS NULL 
  THEN 'Please provide Header Name'  
  ELSE NULL 
  END AS HeaderNameError,
  CASE  WHEN  SD.Footer IS NULL 
  THEN 'Please provide Footer Name'  
  ELSE NULL END AS FooterNameError,
  CASE 
  WHEN  SD.ReportTitle IS NULL 
  THEN 'Please provide ReportTitle Name' 
  ELSE NULL
  END AS ReportTitleNameError,
  CASE 
  WHEN SD.CoverPageImageLink IS NULL 
  THEN 'Please provide CoverPageImageLink' 
  ELSE NULL
  END AS CoverPageImageLinkError,
  CASE 
  WHEN SD.Methodology IS NULL 
  THEN 'Please provide Methodology' 
  ELSE NULL
  END AS methodologyNameError,
  CASE 
  WHEN SD.Overview IS NULL 
  THEN 'Please provide overview' 
  ELSE NULL
  END AS OverViewNameError,
  CASE 
  WHEN SD.KeyPoints IS NULL 
  THEN 'Please provide keypoints' 
  ELSE NULL
  END AS KeyPointsError,
  CASE 
  WHEN SD.ContactUsAnalystName 
  IS NULL THEN 'Please enter analystname' 
  ELSE NULL
  END AS AnalystNameError,
  CASE 
  WHEN SD.ContactUsOfficeContactNo IS NULL 
  THEN 'Please enter office contact number' 
  ELSE NULL
  END AS ConatactnumError,
  CASE 
  WHEN SD.ContactUsAnalystEmailId IS NULL 
  THEN 'Please enter analystemailid' 
  ELSE NULL
  END AS AnalystEmailIdError
 from  StagingDownloadsExportData SD 
 Where ProjectId = @ProjectId  and ModuleTypeId = @ModuleTypeId
 )
Update StagingDownloadsExportData
SET ErrorNotes = ISNULL(cteMarketTree.ProjectNameError,'') 
+ IsNull(cteMarketTree.HeaderNameError, '') + IsNull(cteMarketTree.FooterNameError, '')
+ IsNull(cteMarketTree.ReportTitleNameError, '') + IsNull(cteMarketTree.CoverPageImageLinkError, '')
+ IsNull(cteMarketTree.methodologyNameError, '') + IsNull(cteMarketTree.OverViewNameError, '')
+ IsNull(cteMarketTree.KeyPointsError, '') + IsNull(cteMarketTree.AnalystNameError, '')
+ IsNull(cteMarketTree.AnalystEmailIdError, '') + IsNull(cteMarketTree.ConatactnumError, '')
From cteMarketTree
Inner Join StagingDownloadsExportData sd
On cteMarketTree.stagingid = sd.Id;
IF(EXISTS(SELECT 1 FROM StagingDownloadsExportData WHERE ErrorNotes != ''))   
BEGIN   
Select RowNo, ErrorNotes , CAST(0 AS BIT) AS Success FROM StagingDownloadsExportData WHERE ErrorNotes != '' 
ORDER BY RowNo

DELETE FROM ImportData where Id in (select ImportId from StagingDownloadsExportData sd);    
DELETE FROM StagingDownloadsExportData;

END  
ELSE  
Begin
	Exec SFab_InsertDownloadsExportData;
	DELETE FROM StagingDownloadsExportData;	
	
End 
End try
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 exec SApp_LogErrorInfo
 set @ErrorMsg = 'Error while update Downloads, please contact Admin for more details.';    
SELECT StoredProcedureName ='SApp_UpdateStagingDownloads',Message =@ErrorMsg,Success = 0;
END CATCH              
end