﻿

--drop Procedure SFab_GetCICompanyFinancialAnalysisHeaderData

--execute [SFab_GetCICompanyFinancialAnalysisHeaderData]  @CompanyID = '7037b62e-098d-4399-b422-62cd3df4379f'

Create   Procedure [dbo].[SFab_GetCICompanyFinancialAnalysisHeaderData] 
(
	@CompanyID uniqueidentifier = null
)
As
/*================================================================================  
Procedure Name: [SFab_GetCICompanyFinancialAnalysisHeaderData]  
Author: Nitin 
Create date: 05-Nov-2019  
Description: Get CI Company Financial Analysis Header Data
Change History  
Date         Developer     Reason  
____________ __________  ______________________________________________________  
05-Nov-2019  Nitin       Initial Version  
================================================================================*/  
BEGIN

	--Declare @CompanyID uniqueidentifier = '7037b62e-098d-4399-b422-62cd3df4379f'
	Declare @CompanyName NVARCHAR(512) = '', @FiscalYearEnds NVARCHAR(512) = ''

	Select @CompanyName = CompanyName
	From Company C
	Where C.Id = @CompanyID

	Declare @UnitId uniqueidentifier
	
	--Select Max(CPLS.[Year]) 
	--From CompanyPLStatement	CPLS 
	--Where CPLS.CompanyId = @CompanyID
	
	Select @FiscalYearEnds = 'December'	--Need to get year end month from Database. Right now we are storing only year in CP Financial Import.

	Select @CompanyID As CompanyId, @CompanyName As CompanyName, @FiscalYearEnds As FiscalYearEnds

END