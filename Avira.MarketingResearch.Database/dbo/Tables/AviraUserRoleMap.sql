﻿CREATE TABLE [dbo].[AviraUserRoleMap](
    [Id] [uniqueidentifier] NOT NULL,
    [AviraUserId] [uniqueidentifier] NOT NULL,
    [UserRoleId] [uniqueidentifier] NOT NULL,
    [CreatedOn] [datetime2](7) NULL,
    [UserCreatedById] [uniqueidentifier] NULL,
    [ModifiedOn] [datetime2](7) NULL,
    [UserModifiedById] [uniqueidentifier] NULL,
    [IsDeleted] [bit] NOT NULL,
    [DeletedOn] [datetime2](7) NULL,
    [UserDeletedById] [uniqueidentifier] NULL,
    [IsPrimary] [bit] NOT NULL,
 CONSTRAINT [PK_AviraUserRoleMap] PRIMARY KEY CLUSTERED 
(
    [Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AviraUserRoleMap] ADD  CONSTRAINT [DF_AviraUserRoleMap_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[AviraUserRoleMap] ADD  CONSTRAINT [DF__AviraUser__IsPri__2F8501C7]  DEFAULT ((0)) FOR [IsPrimary]
GO
ALTER TABLE [dbo].[AviraUserRoleMap]  WITH CHECK ADD  CONSTRAINT [FK_AviraUserRoleMap_CreatedUser] FOREIGN KEY([UserCreatedById])
REFERENCES [dbo].[AviraUser] ([Id])
GO
ALTER TABLE [dbo].[AviraUserRoleMap] CHECK CONSTRAINT [FK_AviraUserRoleMap_CreatedUser]
GO
ALTER TABLE [dbo].[AviraUserRoleMap]  WITH CHECK ADD  CONSTRAINT [FK_AviraUserRoleMap_DeletedUser] FOREIGN KEY([UserDeletedById])
REFERENCES [dbo].[AviraUser] ([Id])
GO
ALTER TABLE [dbo].[AviraUserRoleMap] CHECK CONSTRAINT [FK_AviraUserRoleMap_DeletedUser]
GO
ALTER TABLE [dbo].[AviraUserRoleMap]  WITH CHECK ADD  CONSTRAINT [FK_AviraUserRoleMap_ModifiedUser] FOREIGN KEY([UserModifiedById])
REFERENCES [dbo].[AviraUser] ([Id])
GO
ALTER TABLE [dbo].[AviraUserRoleMap] CHECK CONSTRAINT [FK_AviraUserRoleMap_ModifiedUser]
GO
ALTER TABLE [dbo].[AviraUserRoleMap]  WITH CHECK ADD  CONSTRAINT [FK_AviraUserRoleMap_UserId] FOREIGN KEY([AviraUserId])
REFERENCES [dbo].[AviraUser] ([Id])
GO
ALTER TABLE [dbo].[AviraUserRoleMap] CHECK CONSTRAINT [FK_AviraUserRoleMap_UserId]
GO
ALTER TABLE [dbo].[AviraUserRoleMap]  WITH CHECK ADD  CONSTRAINT [FK_AviraUserRoleMap_UserRoleId] FOREIGN KEY([UserRoleId])
REFERENCES [dbo].[UserRole] ([Id])
GO
ALTER TABLE [dbo].[AviraUserRoleMap] CHECK CONSTRAINT [FK_AviraUserRoleMap_UserRoleId]
GO