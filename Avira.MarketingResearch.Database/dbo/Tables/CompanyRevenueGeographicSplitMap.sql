﻿CREATE TABLE [dbo].[CompanyRevenueGeographicSplitMap] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [CompanyRevenueId] UNIQUEIDENTIFIER NOT NULL,
    [RegionId]         UNIQUEIDENTIFIER NOT NULL,
    [RegionValue]      DECIMAL (18, 4)  NULL,
    [Year]             INT              NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_CompanyRevenueGeographicSplitMap_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    [CompanyId]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CompanyRevenueGeographicSplitMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CompanyRevenueGeographicSplitMap_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyRevenueGeographicSplitMap_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyRevenueGeographicSplitMap_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([Id]),
    CONSTRAINT [FK_CompanyRevenueGeographicSplitMap_CompanyRevenue] FOREIGN KEY ([CompanyRevenueId]) REFERENCES [dbo].[CompanyRevenue] ([Id]),
    CONSTRAINT [FK_CompanyRevenueGeographicSplitMap_Region] FOREIGN KEY ([RegionId]) REFERENCES [dbo].[Region] ([Id])
);

