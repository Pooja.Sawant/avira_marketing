﻿CREATE TABLE [dbo].[CompanyRevenueSegmentationSplitMap] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [CompanyRevenueId] UNIQUEIDENTIFIER NOT NULL,
    [Split]            INT              NULL,
    [Segmentation]     NVARCHAR (200)   NULL,
    [SegmentValue]     DECIMAL (18, 4)  NULL,
    [Year]             INT              NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_CompanyRevenueSegmentationSplitMap_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    [CompanyId]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CompanyRevenueSegmentationSplitMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CompanyRevenueSegmentationSplitMap_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyRevenueSegmentationSplitMap_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyRevenueSegmentationSplitMap_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([Id]),
    CONSTRAINT [FK_CompanyRevenueSegmentationSplitMap_CompanyRevenue] FOREIGN KEY ([CompanyRevenueId]) REFERENCES [dbo].[CompanyRevenue] ([Id])
);

