﻿
--Execute [SFab_GetAllDecisionParameters] @DecisionId = '2FEB7866-F757-46D6-8FBD-B537BBB5FE37', @UserID = '1570416B-443A-4833-AA8F-941E159DD90F'

CREATE PROCEDURE [dbo].[SFab_GetAllDecisionParameters]
(		
	@DecisionId uniqueidentifier,
	@UserID uniqueidentifier
)
AS

--Declare @DecisionId uniqueidentifier = '68FC2F48-1630-4682-8EE5-84F61DE9F5CD',
--	@UserID uniqueidentifier = '1570416B-443A-4833-AA8F-941E159DD90F'

/*================================================================================
Procedure Name: [dbo].[SFab_GetAllDecisionParameters]
Author: Nitin
Create date: 07/01/2020
Description: Get list from decision parameter table by userid and decision id
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
07/01/2020	Nitin			Initial Version
13-Feb-2020 Nitin			CR - Default rating changed to 1
================================================================================*/

BEGIN

	If Not Exists(Select Id From Decision Where Id = @DecisionId)
	Begin

		Select @DecisionId = Id From Decision Where BaseDecisionId = @DecisionId And UserId = @UserID
	End

	Declare @DecisionParameters Table (Id uniqueidentifier, DecisionId uniqueidentifier, BaseDecisionParameterId uniqueidentifier, 
			[Description] nvarchar(1024), 
			CategoryId uniqueidentifier, Category nvarchar(512), 
			RatingId uniqueidentifier, Rating int, Weightage [decimal](18,2))

	Declare @DecisionParameterCount int
	Declare @BaseDecisionParameterCount int

	Select @DecisionParameterCount = Count(*) From DecisionParameter Where DecisionId = @DecisionId
	Select @BaseDecisionParameterCount = count(*) From BaseDecisionParameter

	Declare @DefaultWeightage decimal(18, 2) = 0

	If (@DecisionParameterCount = 0)
	Begin

		Set @DefaultWeightage = 100.00 / @BaseDecisionParameterCount
	End
	Else
	Begin

		Set @DefaultWeightage = 0
	End
	
	Declare @RatingId1 uniqueidentifier
	Select @RatingId1 = Id from LookupCategory Where CategoryType = 'DMDecisionParameterRating' And CategoryName = '1'

	Insert Into @DecisionParameters (ID, DecisionId, BaseDecisionParameterId, Description, CategoryId, Category, RatingId, Rating, Weightage) 		
	Select DP.Id As Id, DP.DecisionId, DP.BaseDecisionParameterId, DP.[Description], C.Id, C.CategoryName, 
		   DP.RatingId, Cast(R.CategoryName As Int) As Rating, DP.Weightage
	From DecisionParameter DP 
		Inner Join LookupCategory C On 
			DP.CategoryId = C.Id And
			C.CategoryType = 'DMDecisionParameterCategory'
		Inner Join LookupCategory R On 
			DP.RatingId = R.Id And
			R.CategoryType = 'DMDecisionParameterRating'
	Where DP.DecisionId = @DecisionId

	Insert Into @DecisionParameters (ID, DecisionId, BaseDecisionParameterId, Description, CategoryId, Category, RatingId, Rating, Weightage) 		
	Select Null, Null, BDP.Id As BaseDecisionParameterId, BDP.[Description], LC.Id, LC.CategoryName, @RatingId1, 1, @DefaultWeightage
	From BaseDecisionParameter BDP 
		Inner Join LookupCategory LC On 
			BDP.CategoryId = LC.Id And
			LC.CategoryType = 'DMDecisionParameterCategory'
	Where BDP.Id Not in 
		(Select BaseDecisionParameterId From DecisionParameter 
			Where DecisionId = @DecisionId
		)	

	Select ID, DecisionId, BaseDecisionParameterId, Description, CategoryId, Category, RatingId, Rating, Weightage
	From @DecisionParameters
	Order By Category, [Description]

END

--Execute SFab_GetAllDecisionParameters