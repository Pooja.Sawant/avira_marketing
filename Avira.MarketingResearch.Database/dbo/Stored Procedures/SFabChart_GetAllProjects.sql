﻿CREATE procedure [dbo].[SFabChart_GetAllProjects]
(
	@AvirauserId UniqueIdentifier
)
as
/*================================================================================
Procedure Name: [SFabChart_GetAllProjects]
Author: Sai Krishna
Create date: 28/05/2019
Description: Get Project list from UserSegmentMap
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
28/05/2019	Sai Krishna		Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ProjectApprovedStatusId uniqueidentifier;
select @ProjectApprovedStatusId = ID from ProjectStatus Where ProjectStatusName = 'Completed'
Declare @UserRoleName nvarchar(50) = null;

SET @UserRoleName = (SELECT UserRoleName FROM UserRole 
WHERE Id = @AvirauserId)

IF(@UserRoleName = 'Admin')
Begin
SELECT Id as ProjectId,
	   ProjectName
FROM Project PRJ
WHERE IsDeleted = 0 AND ProjectStatusID=@ProjectApprovedStatusId
END
ELSE
BEGIN
SELECT USM.SegmentId as ProjectId,
	   PRJ.ProjectName
FROM UserSegmentMap USM
INNER JOIN Project PRJ
ON USM.SegmentId = PRJ.ID
WHERE SegmentType = 'Project'
AND CustomerUserId = @AvirauserId
AND PRJ.IsDeleted = 0
AND PRJ.ProjectStatusID=@ProjectApprovedStatusId
END  
END