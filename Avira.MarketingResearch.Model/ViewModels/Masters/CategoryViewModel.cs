﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.Masters
{
    public class CategoryViewModel
    {
        public Guid Id { get; set; }
        public string CategoryType { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public Guid CategoryId { get; set; }
        public string RowType
        {
            get
            {
                return "Category";
            }
        }
    }
}
