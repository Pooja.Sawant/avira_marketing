﻿      
      
CREATE PROCEDURE [dbo].[SApp_GetBaseYearMaster]      
 AS      
/*================================================================================      
Procedure Name: [SApp_GetBaseYearMaster]      
Author: Gopi      
Create date: 06/19/2019      
Description: Get Base year from table       
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
06/19/2019 Gopi   Initial Version      
================================================================================*/      
      
BEGIN    
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED      
      
 SELECT Id, BaseYear FROM BaseYearMaster  
 END