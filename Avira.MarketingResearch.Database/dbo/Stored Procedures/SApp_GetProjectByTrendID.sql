﻿


      
 CREATE PROCEDURE [dbo].[SApp_GetProjectByTrendID]            
 (@TrendID uniqueidentifier = null,    
 @ProjectName nvarchar(256)='',    
 @includeDeleted bit = 0,        
 @OrdbyByColumnName NVARCHAR(50) ='ProjectName',        
 @SortDirection INT = -1,        
 @PageStart INT = 0,        
 @PageSize INT = null)            
 AS            
/*================================================================================            
Procedure Name: SApp_GetProjectByTrendID        
Author: Sai Krishna            
Create date: 04/18/2019            
Description: Get list from Project table by TrendID            
Change History            
Date  Developer  Reason            
__________ ____________ ______________________________________________________            
04/18/2019 Sai Krishna   Initial Version 
================================================================================*/           
            
BEGIN            
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED         
declare @ProjectID	uniqueidentifier;

if @PageSize is null or @PageSize =0        
set @PageSize = 10    

if isnull(@ProjectName,'') = '' set @ProjectName = '' 
else set @ProjectName = '%'+ @ProjectName +'%';    
select @ProjectID = ProjectID
from Trend
where id = @TrendID;
       
begin        
;WITH CTE_TBL AS (        
 SELECT         
 --@TrendID as TrendID,          
   [Project].[Id] AS [ProjectId],            
   [Project].[ProjectName],           
   [Project].IsDeleted,        
   TrendProjectMap.Impact as ImpactId,      
   TrendProjectMap.EndUser,      
   TrendProjectMap.TrendId,      
   TrendProjectMap.Id as TrendProjectMapId,      
  cast (case when exists(select 1 from dbo.TrendProjectMap         
     where TrendProjectMap.TrendId = @TrendID        
     and TrendProjectMap.IsDeleted = 0        
     and TrendProjectMap.ProjectId = Project.Id) then 1 else 0 end as bit) as IsMapped,        
 CASE        
  WHEN @OrdbyByColumnName = 'ProjectName' THEN [Project].ProjectName     
   WHEN @OrdbyByColumnName = 'ProjectCode' THEN [Project].[ProjectCode]       
  when @OrdbyByColumnName = 'IsMapped' THEN cast(case when TrendProjectMap.TrendId is null then 1 else 0 end as varchar(2))                   
  ELSE [Project].ProjectName        
 END AS SortColumn        
        
FROM [dbo].Project      
LEFT JOIN dbo.TrendProjectMap       
ON Project.Id = TrendProjectMap.ProjectId  
and TrendProjectMap.TrendId = @TrendID    
and TrendProjectMap.IsDeleted = 0 
--where [Project].[ProjectName] LIKE '%'+ ISNULL(@ProjectName,'') + '%' 
where (@ProjectName = '' or [Project].[ProjectName] LIKE @ProjectName)
AND (@includeDeleted = 1 or Project.IsDeleted = 0) 
--and [Project].ID != @ProjectID    
)        
        
 SELECT         
    CTE_TBL.*,        
    tCountResult.TotalItems        
  FROM CTE_TBL        
  CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult        
  ORDER BY         
    CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,        
    CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC         
  OFFSET @PageStart ROWS        
  FETCH NEXT @PageSize ROWS ONLY;        
  END        
        
END