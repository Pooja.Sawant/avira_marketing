﻿

CREATE procedure [dbo].[SFab_GetQualitativeDescriptionDatabyID]
(@ProjectID uniqueidentifier=null,
@MarketID uniqueidentifier=null,
@includeDeleted bit = 0,
@AviraUserID uniqueidentifier = null)
as
/*================================================================================
Procedure Name: SFab_GetQualitativeDescriptionDatabyID
Author: Harshal 
Create date: 09/04/2019
Description: Get list from QualitativeDescription table by Filters
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
09/04/2019	Harshal			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if @includeDeleted is null set @includeDeleted = 0

SELECT [QualitativeDescription].[Id]
      ,[QualitativeDescription].[ProjectId]
	  ,[ProjectName]
      ,[QualitativeDescription].[MarketId]
	  ,[MarketName]
      ,[QualitativeAnalysisTypeId]
	  ,[QualitativeAnalysisTypeName]
      ,[QualitativeAnalysisSubTypeId]
      ,[DescriptionText]
		
FROM [dbo].[QualitativeDescription]
INNER JOIN Project Project ON Project.ID=QualitativeDescription.ProjectId
INNER JOIN QualitativeAnalysisType ON QualitativeAnalysisType.Id = QualitativeDescription.QualitativeAnalysisTypeId
INNER JOIN Market Market ON Market.Id=QualitativeDescription.MarketId
where (@ProjectId is null or QualitativeDescription.ProjectId = @ProjectID)
AND (@MarketID is null or QualitativeDescription.MarketId = @MarketID)
and (@includeDeleted = 1 or QualitativeDescription.IsDeleted = 0)

end