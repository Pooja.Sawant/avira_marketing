﻿CREATE procedure [dbo].[SApp_insertUpdateCompanyProfile]
(
	@CompanyId uniqueidentifier,
	@CompanyName nvarchar(256),
	@CompanyCode nvarchar(256),
	@DisplayName nvarchar(256),
	@ActualImageName nvarchar(256),
	@LogoURL nvarchar(256),
	@NatureId uniqueidentifier,
	@ParentCompanyName nvarchar(256),
	@ParentId uniqueidentifier,
	@CompanyLocation nvarchar(256),
	@IsHeadQuarters bit,
	@Telephone nvarchar(20),
	@Telephone2 nvarchar(20),
	@Telephone3 nvarchar(20),
	@Email nvarchar(100),
	@Email2 nvarchar(100),
	@Email3 nvarchar(100),
	@Website nvarchar(128),
	@YearOfEstb int,
	@CompanyStageId uniqueidentifier,
	@NoOfEmployees int,
	@RankNumber int,
	@RankRationale nvarchar(256),
	@IsDeleted bit,
	@AviraUserId uniqueidentifier
)
as
/*================================================================================
Procedure Name: [SApp_insertUpdateCompanyProfile]
Author: Sai Krishna
Create date: 05/06/2019
Description: Insert/Update the company profile details.
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
05/06/2019	Sai Krishna		Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

--Validations
IF exists(select 1 from [dbo].Company 
		 where Company.CompanyName = ISNULL(@CompanyName,'') AND IsDeleted = 0 and Id != @CompanyId)
begin
declare @ErrorMsg NVARCHAR(2048);
set @ErrorMsg = 'Company with Name "'+ ISNULL(@CompanyName,'') + '" already exists.';
SELECT StoredProcedureName ='SApp_insertUpdateCompanyProfile',Message =@ErrorMsg,Success = 0;  
RETURN;  
return(1)
end

IF (@CompanyId is null or @CompanyId = '00000000-0000-0000-0000-000000000000')
begin
--Insert new record
	set @CompanyId = NEWID();

	insert into [dbo].[Company](
								Id,
								CompanyName,
								--CompanyCode,
								DisplayName,
								ActualImageName,
								LogoURL,
								NatureId,
								ParentCompanyName,
								ParentId,
								CompanyLocation,
								IsHeadQuarters,
								Telephone,
								Telephone2,
								Telephone3,
								Email,
								Email2,
								Email3,
								Website,
								YearOfEstb,
								CompanyStageId,
								NoOfEmployees,
								RankNumber,
								RankRationale,
								IsDeleted,
								UserCreatedById,
								CreatedOn)
					values(
								@CompanyId,
								@CompanyName,
								--dbo.fun_NextCompanyCode(),
								@DisplayName,
								@ActualImageName,
								@LogoURL,
								@NatureId,
								@ParentCompanyName,
								@ParentId,
								@CompanyLocation,
								@IsHeadQuarters,
								@Telephone,
								@Telephone2,
								@Telephone3,
								@Email,
								@Email2,
								@Email3,
								@Website,
								@YearOfEstb,
								@CompanyStageId,
								@NoOfEmployees,
								@RankNumber,
								@RankRationale,
								0,
								@AviraUserId, 
								getdate());
end
else
begin
--update record
			update [dbo].[Company]
							--set	CompanyName = @CompanyName,
								--CompanyCode = @CompanyCode,
							set	DisplayName = @DisplayName,
								ActualImageName = @ActualImageName,
								LogoURL = @LogoURL,
								NatureId = @NatureId,
								--ParentCompanyName = @ParentCompanyName,
								ParentId = @ParentId,
								CompanyLocation = @CompanyLocation,
								IsHeadQuarters = @IsHeadQuarters,
								Telephone = @Telephone,
								Telephone2 = @Telephone2,
								Telephone3 = @Telephone3,
								Email = @Email,
								Email2 = @Email2,
								Email3 = @Email3,
								Website = @Website,
								YearOfEstb = @YearOfEstb,
								CompanyStageId = @CompanyStageId,
								NoOfEmployees = @NoOfEmployees,
								RankNumber = @RankNumber,
								RankRationale = @RankRationale,
								[ModifiedOn] = getdate(),
								DeletedOn = case when IsDeleted = 0 and @IsDeleted = 1 then getdate() else DeletedOn end,
								UserDeletedById = case when IsDeleted = 0 and @IsDeleted = 1 then @AviraUserId else UserDeletedById end,
								IsDeleted = isnull(@IsDeleted, IsDeleted),
								[UserModifiedById] = @AviraUserId
						where ID = @CompanyId
end

select Id = CAST(@CompanyId AS nvarchar(36)), Message = 'Success', Success = 1;
end