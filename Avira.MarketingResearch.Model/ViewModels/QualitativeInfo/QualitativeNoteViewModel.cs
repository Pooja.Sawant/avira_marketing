﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.QualitativeInfo
{
    public class QualitativeNoteViewModel
    {
        public Guid Id { get; set; }
        public Guid QualitativeId { get; set; }
        public Guid? ProjectId { get; set; }        
        public string QualitativeStatusName { get; set; }
        public string AuthorRemark { get; set; }
        public string ApproverRemark { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "QualitativeNote";
            }
        }
    }
}
