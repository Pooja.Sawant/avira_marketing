﻿--sp_helptext SApp_GetCompanyOtherInfo
        
 CREATE PROCEDURE [dbo].[SApp_GetCompanyOtherInfo]              
 (  
 @CompanyId UniqueIdentifier)              
 AS              
/*================================================================================              
Procedure Name: SApp_GetCompanyOtherInfo          
Author: Pooja Sawant              
Create date: 05/08/2019              
Description: Get list from CompanyOtherInfo table              
Change History              
Date  Developer  Reason              
__________ ____________ ______________________________________________________              
05/08/2019 Pooja   Initial Version    
__________ ____________ ______________________________________________________
05/22/2019 Laxmikant Get Other Info Bu company ID,.
================================================================================*/             
              
BEGIN              
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED    
 SELECT  
   [CompanyOtherInfo].[ID],              
   [CompanyOtherInfo].[CompanyId],    
   [CompanyOtherInfo].[TableInfoName],            
   [CompanyOtherInfo].[Description],          
   [CompanyOtherInfo].IsDeleted,   
   [CompanyOtherInfo].[CompanyStatusId]
FROM [dbo].CompanyOtherInfo     
where [CompanyOtherInfo].[CompanyId] = @CompanyId    
END