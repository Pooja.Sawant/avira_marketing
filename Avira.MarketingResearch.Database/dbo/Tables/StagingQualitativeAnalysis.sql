﻿CREATE TABLE [dbo].[StagingQualitativeAnalysis] (
    [Id]                           UNIQUEIDENTIFIER NOT NULL,
    [ImportId]                     UNIQUEIDENTIFIER NULL,
    [RowNo]                        INT              NULL,
    [MarketName]                   NVARCHAR (255)   NULL,
    [SubMarketName]                NVARCHAR (255)   NULL,
    [AnalysisCategory]             NVARCHAR (255)   NULL,
    [AnalysisSubCategory]          NVARCHAR (255)   NULL,
    [AnalysisText]                 NVARCHAR (4000)  NULL,
    [Importance]                   NVARCHAR (255)   NULL,
    [Impact]                       NVARCHAR (255)   NULL,
    [MarketId]                     UNIQUEIDENTIFIER NULL,
    [SubMarketId]                  UNIQUEIDENTIFIER NULL,
    [ProjectId]                    UNIQUEIDENTIFIER NULL,
    [QualitativeAnalysisTypeId]    UNIQUEIDENTIFIER NULL,
    [QualitativeAnalysisSubTypeId] UNIQUEIDENTIFIER NULL,
    [ImportanceId]                 UNIQUEIDENTIFIER NULL,
    [ImpactId]                     UNIQUEIDENTIFIER NULL,
    [ErrorNotes]                   NVARCHAR (1000)  NULL,
    CONSTRAINT [PK_StagingQualitativeAnalysis] PRIMARY KEY CLUSTERED ([Id] ASC)
);



