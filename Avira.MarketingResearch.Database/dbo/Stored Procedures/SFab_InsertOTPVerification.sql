﻿
CREATE PROCEDURE [dbo].[SFab_InsertOTPVerification]
(@ID		UNIQUEIDENTIFIER=null,
@OtpType	varchar(50)=null,
@AviraUserId	UNIQUEIDENTIFIER=null,
@OtpEntity	NVARCHAR(512)=null,
@OTPCode	NVARCHAR(512)=null,
@AttempFailureCount		int=null,
--@MaxOTPResendCount		int=null,
@ClientDeviceId nvarchar(512)=null
)
as
/*================================================================================
Procedure Name: SFab_InsertOTPVerification
Author: Pooja Sawant
Create date: 06/06/2019
Description: Insert a record in otpvalidation table 
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
06/06/2019	Pooja			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ErrorMsg NVARCHAR(2048);

BEGIN TRY

INSERT INTO [dbo].[OTPVerification]
           ([Id]
      ,[OtpType]
      ,[AviraUserId]
      ,[OtpEntity]
      ,[OTPCode]
      ,[GeneratedDateTime]     
      ,[AttempFailureCount]

	  ,[ClientDeviceId]
           )
     VALUES
           (@ID
           ,@OtpType
           ,@AviraUserId
           ,@OtpEntity
           ,@OTPCode
           ,GETDATE()
          
           ,@AttempFailureCount
        
		   ,@ClientDeviceId
         )

SELECT StoredProcedureName ='SFab_InsertOTPVerification',Message =@ErrorMsg,Success = 1;     

END TRY
BEGIN CATCH
    -- Execute the error retrieval routine.
	DECLARE @ErrorNumber	int;
	DECLARE @ErrorSeverity	int;
	DECLARE @ErrorProcedure	varchar(100);
	DECLARE @ErrorLine	int;
	DECLARE @ErrorMessage	varchar(500);

	 SELECT @ErrorNumber = ERROR_NUMBER(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorProcedure = ERROR_PROCEDURE(),
        @ErrorLine = ERROR_LINE(),
        @ErrorMessage = ERROR_MESSAGE()

	insert into dbo.Errorlog(ErrorNumber,
							ErrorSeverity,
							ErrorProcedure,
							ErrorLine,
							ErrorMessage,
							ErrorDate)
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());

 set @ErrorMsg = 'Error while creating a new OTPVerification, please contact Admin for more details.';    
 
SELECT Number = @ErrorNumber, 
		Severity =@ErrorSeverity,
		StoredProcedureName =@ErrorProcedure,
		LineNumber= @ErrorLine,
		Message =@ErrorMsg,
		Success = 0;     
END CATCH
end