﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.KeyCompanyingResearch.Common.IService.Trend
{
    public interface ITrendKeyCompanyMapService
    {
        /// <summary>
     /// To get all KeyCompany with mapped/unmapped status by trend id
     /// </summary> 
     /// <param name="PageStart"></param>
     /// <param name="pageSize"></param>
     /// <param name="SortDirection"></param>
     /// <param name="OrdbyByColumnName"></param>
     /// <param name="Search"></param>
     /// <param name="isDeletedInclude"></param>
     /// <returns></returns>
        IEnumerable<TrendKeyCompanyMapViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false, string id = "");
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tfc"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        /// 
        Result<SpTransactionMessage> InsertUpdate(MainTrendKeyCompanyMapInsertUpdateDbViewModel tfc, AviraUser currenUser);

        Result<SpTransactionMessage> Insert(MainTrendKeyCompanyMapInsertUpdateDbViewModel tfc, AviraUser currenUser);

        Result<SpTransactionMessage> Delete(MainTrendKeyCompanyMapInsertUpdateDbViewModel tfc, AviraUser currenUser);
    }
}
