﻿CREATE TABLE [dbo].[StandardHours] (
    [Id]                 UNIQUEIDENTIFIER NOT NULL,
    [StandardHoursName]  NVARCHAR (256)   CONSTRAINT [DF_StandardHours_StandardHoursName] DEFAULT ('') NOT NULL,
    [StandardHoursValue] DECIMAL (9, 2)   NULL,
    [CreatedOn]          DATETIME2 (7)    NULL,
    [UserCreatedById]    UNIQUEIDENTIFIER NULL,
    [ModifiedOn]         DATETIME2 (7)    NULL,
    [UserModifiedById]   UNIQUEIDENTIFIER NULL,
    [IsDeleted]          BIT              CONSTRAINT [DF_StandardHours_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]          DATETIME2 (7)    NULL,
    [UserDeletedById]    UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_StandardHours] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_StandardHours_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_StandardHours_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id])
);


GO


GO


GO


GO


GO


GO


GO