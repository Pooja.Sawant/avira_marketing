﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard
{
    public class ProjectSegmentMappingViewModel
    {
        public Guid Id { get; set; }
        public Guid ProjectID { get; set; }
        public string SegmentType { get; set; }
        public List<Guid> SegmentID { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }      
        public int TotalItems { get; set; }
        public string RowType
        { 
            get
            {
                return "ProjectSegmentMapping";
            }
        }
    }
    public class ProjectSegmentMappingInsertDbViewModel
    {
        [Required]
        [Display(Name = "Segment Name")]       
        public string SegmentType { get; set; }
        public List<Guid> SegmentID { get; set; }
        public Guid ProjectID { get; set; }
    
        public Guid UserCreatedById { get; set; }
        public Guid Id { get; set; }
        public Guid[] UserIDs { get; set; }
    }

    public class ProjectSegmentMappingUpdateDbViewModel
    {
        [Required]
        [Display(Name = "Segment Name")]
        public string SegmentType { get; set; }
        public List<Guid> SegmentID { get; set; }
        public Guid ProjectID { get; set; }
      
        [Required]
        public Guid Id { get; set; }
        public Guid[] UserIDs { get; set; }
    }
}
