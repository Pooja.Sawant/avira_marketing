﻿
CREATE procedure [dbo].[SApp_UpdateQualitativeAddTable]      
(
 @Id     uniqueidentifier=null,  
 @ProjectId   uniqueidentifier=null,  
 @TableName   nvarchar(50)=null,  
 @TableMetadata  nvarchar(512)=null,  
 @TableData  nvarchar(512)=null,     
 @IsDeleted bit=0,    
 @UserModifiedById  uniqueidentifier,    
 @DeletedOn datetime = null,    
 @UserDeletedById uniqueidentifier = null,    
 @ModifiedOn Datetime     
)  
as      
/*================================================================================      
Procedure Name: [SApp_UpdateQualitativeAddTable]      
Author: Harshal      
Create date: 07/06/2019      
Description: update a record in QualitativeTableTab  by ID. Also used for delete      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
07/06/2019 Harshal   Initial Version    
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
    
IF exists(select 1 from [dbo].[QualitativeTableTab]        
   where [Id] !=  @Id      
   and  TableName = @TableName AND IsDeleted = 0)      
begin      
declare @ErrorMsg NVARCHAR(2048);      
set @ErrorMsg = 'Table Name "'+ @TableName + '" already exists.';      
  
SELECT StoredProcedureName ='SApp_UpdateQualitativeAddTable','Message' =@ErrorMsg,'Success' = 0;    
RETURN;  
end      
    
BEGIN TRY       
--update record      
update [dbo].[QualitativeTableTab]       
set 
 TableName=@TableName,    
 TableMetadata=@TableMetadata,   
 TableData=@TableData,    
 [ModifiedOn] = getdate(),      
 DeletedOn = case when IsDeleted = 0 and @IsDeleted = 1 then getdate() else DeletedOn end,      
 UserDeletedById = case when IsDeleted = 0 and @IsDeleted = 1 then @UserModifiedById else UserDeletedById end,      
 IsDeleted = isnull(@IsDeleted, IsDeleted),      
 [UserModifiedById] = @UserModifiedById      
 where ID = @Id  and ProjectId=@ProjectId   
     
SELECT StoredProcedureName ='SApp_UpdateQualitativeAddTable',Message =@ErrorMsg,Success = 1;       
    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);      
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());   
  
set @ErrorMsg = 'Error while updating SApp_UpdateQualitativeAddTable, please contact Admin for more details.'        
   
 SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
END CATCH      
      
END