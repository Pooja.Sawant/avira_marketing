﻿CREATE TYPE [dbo].[udtGUIDIntValue] AS TABLE (
    [ID]    UNIQUEIDENTIFIER NOT NULL,
    [Value] INT              NOT NULL);

