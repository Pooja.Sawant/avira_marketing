﻿CREATE TABLE [dbo].[CompanyShareVolumeImage] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [CompanyId]        UNIQUEIDENTIFIER NOT NULL,
    [ParentCompanyId]  UNIQUEIDENTIFIER NOT NULL,
    [ImageFilePath]    NVARCHAR (500)   NOT NULL,
    [DisplayFileName]  NVARCHAR (256)   NOT NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_ShareVolume] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CompanyShareVolumeImage_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyShareVolumeImage_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [fk_CompanyShareVolumeImage_CompanyParentId] FOREIGN KEY ([ParentCompanyId]) REFERENCES [dbo].[CompanyShareVolumeImage] ([Id])
);

