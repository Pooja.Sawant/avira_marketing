﻿using System;
using System.Collections.Generic;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile
{
    public interface ICompanyStructureRepository
    {
        IEnumerable<CompanyStructureModel> GetAll(Guid Id);
        //IEnumerable<CompanyStructureModel> GetAll(Guid Id);
    }
}
