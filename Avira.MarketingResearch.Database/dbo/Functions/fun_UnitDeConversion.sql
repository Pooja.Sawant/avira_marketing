﻿-- =============================================  
-- Author:  <Praveen>  
-- Create date: <05-07-2019 ,>  
-- Description: <function for de currency conversion>  
-- =============================================  
CREATE FUNCTION [dbo].[fun_UnitDeConversion]  
(  
 -- Add the parameters for the function here  
 @UnitId UNIQUEIDENTIFIER, @ConvertedValue DECIMAL(18,4)  
)  
RETURNS DECIMAL(18,4)  
AS  
BEGIN  
DECLARE @ActualValue DECIMAL(18,4)  
  
 SET @ActualValue = 
 case when @UnitId is null then @ConvertedValue
	  else  @ConvertedValue / (select top 1  CASE WHEN (ValueName = 'Percentage' OR ValueName = 'Fraction') THEN 1  
												 ELSE Conversion END  
												 FROM ValueConversion 
												 WHERE Id = @UnitId )
end
  
 -- Return the result of the function  
 RETURN @ActualValue  
  
END