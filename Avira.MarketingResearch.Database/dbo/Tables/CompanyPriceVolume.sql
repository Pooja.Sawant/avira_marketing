﻿CREATE TABLE [dbo].[CompanyPriceVolume] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [Date]             DATETIME2 (7)    NULL,
    [Price]            DECIMAL (19, 4)  NULL,
    [CurrencyId]       UNIQUEIDENTIFIER NULL,
    [CurrencyUnitId]   UNIQUEIDENTIFIER NULL,
    [Volume]           DECIMAL (19, 4)  NULL,
    [VolumeUnitId]     UNIQUEIDENTIFIER NULL,
    [CompanyId]        UNIQUEIDENTIFIER NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_CompanyPriceVolume_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CompanyPriceVolume] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CompanyPriceVolume_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([Id]),
    CONSTRAINT [FK_CompanyPriceVolume_CompanyPriceVolume] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[Currency] ([Id]),
    CONSTRAINT [FK_CompanyPriceVolume_ValueConversion] FOREIGN KEY ([CurrencyUnitId]) REFERENCES [dbo].[ValueConversion] ([Id]),
    CONSTRAINT [FK_CompanyPriceVolume_ValueConversion1] FOREIGN KEY ([VolumeUnitId]) REFERENCES [dbo].[ValueConversion] ([Id])
);

