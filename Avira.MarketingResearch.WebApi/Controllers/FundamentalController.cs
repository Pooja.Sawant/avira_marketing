﻿using Avira.MarketingResearch.Common.IService.Masters;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces.Masters;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Utility;
using Avira.MarketingResearch.WebApi;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FundamentalController : AviraBaseController
    {
        private ILoggerManager _logger;
        public readonly IFundamentalRepository _fundamentalRepository;
        public readonly IFundamentalService _fundamentalService;
        public FundamentalController(
            IFundamentalService fundamentalService,
            IFundamentalRepository fundamentalRepository,
            ILoggerManager logger)
        {
            _fundamentalRepository = fundamentalRepository;
            _fundamentalService = fundamentalService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<FundamentalViewModel>> Get(int PageStart = 0, int pageSize = 0, int SortDirection = 1, string OrdbyByColumnName = "CreatedDate", string Search = "", bool isDeletedInclude = false)
        {
            List<FundamentalViewModel> res = null;
            try
            {
                res= _fundamentalService.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }


        [HttpGet("{id}")]
        public ActionResult<FundamentalViewModel> Get(Guid id)
        {
            FundamentalViewModel fundamental = null;
            try
            {
                fundamental = _fundamentalService.GetById(id);
                if (fundamental == null)
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return fundamental;
        }

        [HttpPost]
        public ActionResult Post([FromBody] FundamentalInsertDbViewModel viewModel)
        {
            viewModel.CreatedOn = System.DateTime.UtcNow;
            viewModel.UserCreatedById = CurrentUser.Id;
            viewModel.Id = Guid.NewGuid();


            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new Fundamental, please contact Admin for more details."
            };

            try
            {

                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                var result = _fundamentalService.Create(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Fundamental Name " + viewModel.FundamentalName + " is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }

            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);

        }


        [HttpPut]
        public ActionResult Put([FromBody] FundamentalUpdateDbViewModel viewModel)
        {

            viewModel.ModifiedOn = System.DateTime.UtcNow;
            viewModel.UserModifiedById = CurrentUser.Id;


            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while updating a  Fundamental, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                Result<SpTransactionMessage> result = _fundamentalService.Update(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Fundamental Name " + viewModel.FundamentalName + " is updated successfully.";

                }
                else
                {
                    viewData.Message = result.Error;
                }
            }

            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }

        [Route("DeleteSegment")]
        [HttpPut]
        public ActionResult DeleteSegment([FromBody] FundamentalUpdateDbViewModel viewModel)
        {

            viewModel.DeletedOn = System.DateTime.UtcNow;
            viewModel.UserDeletedById = CurrentUser.Id;
            viewModel.IsDeleted = true;


            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while deleting a new Fundamental, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                Result<SpTransactionMessage> result = _fundamentalService.Update(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;

                    viewData.Message = "Fundamental Name " + viewModel.FundamentalName + " is deleted.";

                }
                else
                {
                    viewData.Message = result.Error;
                }
            }

            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }

    }
}