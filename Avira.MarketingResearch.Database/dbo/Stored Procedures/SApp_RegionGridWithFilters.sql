﻿CREATE PROCEDURE [dbo].[SApp_RegionGridWithFilters]    
 (@RegionIDList dbo.udtUniqueIdentifier  READONLY,    
 @ParentRegionID uniqueidentifier = null,    
 @RegionName nvarchar(256)='',    
 @RegionLevel TinyInt=0,    
 @includeDeleted bit = 0,    
 @OrdbyByColumnName NVARCHAR(50) ='CreatedDate',    
 @SortDirection INT = -1,    
@PageStart INT = 0,
	@PageSize INT = Null)    
 AS    
/*================================================================================    
Procedure Name: SApp_RegionGridWithFilters    
Author: Sharath    
Create date: 03/19/2019    
Description: Get list from Region table by Name or ID    
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
03/19/2019 Sharath   Initial Version  
__________	____________	______________________________________________________
04/22/2019	Pooja			For Client side paging comment pagesize and null init  
================================================================================*/    
    
BEGIN    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED    
if @includeDeleted is null 
	BEGIN
		set @includeDeleted = 0;
	END
	    
if len(@RegionName)>2
	Begin 
		set @RegionName = '%'+ @RegionName + '%'  
	END  
else 
	BEGIN
		set @RegionName = ''  
	END 
	 
if @RegionLevel<=0 
	BEGIN
		set @RegionLevel = null;
	END    
     
	if @PageSize is null or @PageSize =0
	set @PageSize = 15
 ;WITH CTE_TBL AS (    
    
 SELECT [Region].[Id],    
  [Region].[RegionName],    
  [Region].[RegionLevel],    
  [Region].[ParentRegionId],    
  ParentRegion.[RegionName] as ParentRegionName,    
  ParentRegion.[RegionLevel] as ParentRegionLevel,    
  [Region].[IsDeleted],    
  CASE    
  WHEN @OrdbyByColumnName = 'RegionName' THEN [Region].RegionName    
  WHEN @OrdbyByColumnName = 'RegionLevel' THEN CAST([Region].[RegionLevel] as varchar(10))    
  WHEN @OrdbyByColumnName = 'ParentRegionName' THEN ParentRegion.RegionName    
  ELSE [Region].[RegionName]    
 END AS SortColumn    
    
FROM [dbo].[Region]    
left join [dbo].[Region] ParentRegion    
on [Region].[ParentRegionId] = ParentRegion.Id    
where (NOT EXISTS (SELECT * FROM @RegionIDList) or  [Region].Id  in (select * from @RegionIDList))    
 AND (@RegionName = '' or [Region].[RegionName] like @RegionName)    
 AND (@RegionLevel is null or [Region].[RegionLevel] = @RegionLevel)    
 and (@ParentRegionID is null or [Region].[ParentRegionId] = @ParentRegionID)    
 AND (@includeDeleted = 1 or [Region].IsDeleted = 0)    
 )    
    
 SELECT     
    CTE_TBL.*,    
    tCountResult.TotalItems    
  FROM CTE_TBL    
  CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult    
  ORDER BY     
    CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,    
    CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC     
  OFFSET @PageStart ROWS    
  FETCH NEXT @PageSize ROWS ONLY;    
    
 END