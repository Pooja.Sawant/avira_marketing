﻿create procedure [dbo].[SFab_GetMarketRegionTree]
(@ProjectId uniqueidentifier )
as
/*================================================================================
Procedure Name: [SApp_GetAllRegions]
Author: Sai Krishna
Create date: 05/02/2019
Description: Get list from Region
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
05/02/2019	Sai Krishna			Initial Version
================================================================================*/
BEGIN

declare @NoCountryRegionIdList	dbo.udtUniqueIdentifier;
declare @CountryIdList	dbo.udtUniqueIdentifier;

insert into @NoCountryRegionIdList
select id 
from Region
where exists (select 1 from MarketValue MV
where mv.ProjectId = @ProjectId
and Region.id = mv.RegionId
and CountryId is null);

insert into @CountryIdList
select Id
from Country
where exists (select 1 from MarketValue MV
where mv.ProjectId = @ProjectId
and Country.id = mv.CountryId);


;WITH CTE_TBL AS (    
    
 SELECT [Region].[Id],    
  [Region].[RegionName],    
  [Region].[RegionLevel],    
  [Region].[ParentRegionId],    
  ParentRegion.[RegionName] as ParentRegionName,    
  ParentRegion.[RegionLevel] as ParentRegionLevel    
FROM [dbo].[Region]    
left join [dbo].[Region] ParentRegion    
on [Region].[ParentRegionId] = ParentRegion.Id    
where exists (select 1 from @NoCountryRegionIdList Rlist where [Region].Id = Rlist.ID --or ParentRegion.Id = rlist.ID
)
or exists (select 1 from CountryRegionMap CRM 
			inner join @CountryIdList Clist
			on CRM.CountryId = Clist.ID
			where crm.RegionId = Region.ID
			--or crm.RegionId = ParentRegion.Id 
			)
) 
select CTE_TBL.[Id],    
  CTE_TBL.[RegionName],    
  CTE_TBL.[RegionLevel],    
  CTE_TBL.[ParentRegionId],    
  CTE_TBL.ParentRegionName 
from  CTE_TBL
union all

select Country.[Id],    
  Country.CountryName as [RegionName],    
  '4' as [RegionLevel],    
  CTE_TBL1.[Id] as [ParentRegionId],    
  CTE_TBL1.[RegionName] as ParentRegionName
 
from CountryRegionMap CRM 
inner join Country
on CRM.CountryId = Country.Id
inner join CTE_TBL CTE_TBL1
on CRM.RegionId = CTE_TBL1.Id
and CTE_TBL1.RegionLevel = 3
and (exists (select 1 from @NoCountryRegionIdList Rlist where CRM.RegionId = Rlist.ID)or 
exists (select 1 from @CountryIdList Clist where Clist.ID = Country.ID))
order by RegionLevel, [RegionName]

end