﻿CREATE PROCEDURE [dbo].[SFab_RawMaterialGridWithFilters]
	(@RawMaterialIDList dbo.udtUniqueIdentifier READONLY,
	@RawMaterialName nvarchar(256)='',
	@Description nvarchar(256)='',
	@includeDeleted bit = 0,
	@OrdbyByColumnName NVARCHAR(50) ='CreatedDate',
	@SortDirection INT = -1,
	@PageStart INT = 0,
	@PageSize INT = null,
	@AviraUserID uniqueidentifier = null)
	AS
/*================================================================================
Procedure Name: SApp_GetRawMaterialbyID
Author: Sharath
Create date: 02/19/2019
Description: Get list from RawMaterial table by Name or ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
02/19/2019	Sharath			Initial Version
__________	____________	______________________________________________________
04/01/2019	Pooja			For Client side paging comment pagesize and null init
================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
if @includeDeleted is null set @includeDeleted = 0;
if len(@RawMaterialName)>2 set @RawMaterialName = '%'+ @RawMaterialName + '%'
else set @RawMaterialName = ''

declare @SegmentId uniqueidentifier;
select @SegmentId = Id from Segment
where SegmentName = 'Raw materials';

	if @PageSize is null or @PageSize =0
	set @PageSize = 10
	
	;WITH CTE_TBL AS (

	SELECT [SubSegment].[Id],
		[SubSegment].SubSegmentName as [RawMaterialName],
		[SubSegment].[Description],
		[SubSegment].[IsDeleted],
		CASE
		WHEN @OrdbyByColumnName = 'RawMaterialName' THEN [SubSegment].SubSegmentName
		WHEN @OrdbyByColumnName = 'Description' THEN [SubSegment].[Description]
		ELSE [SubSegment].SubSegmentName
	END AS SortColumn

FROM [dbo].[SubSegment]
where (NOT EXISTS (SELECT * FROM @RawMaterialIDList) or  [SubSegment].Id  in (select * from @RawMaterialIDList))
	AND (@RawMaterialName = '' or [SubSegment].SubSegmentName like @RawMaterialName)
	AND (@Description = '' or [SubSegment].[Description] like @Description)
	AND (@includeDeleted = 1 or [SubSegment].IsDeleted = 0)
	AND [SubSegment].SegmentId = @SegmentId
	)

	SELECT 
		  CTE_TBL.*,
		  tCountResult.TotalItems
		FROM CTE_TBL
		CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult
		ORDER BY 
		  CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,
		  CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC 
		OFFSET @PageStart ROWS
		FETCH NEXT @PageSize ROWS ONLY;
END