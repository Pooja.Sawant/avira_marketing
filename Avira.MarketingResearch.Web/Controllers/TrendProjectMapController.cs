﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Web.CustomFilter;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Web.Utils;

namespace Avira.MarketingResearch.Web.Controllers
{
    [NoCache]
    [Authorization]
    public class TrendProjectMapController : BaseController
    {
        private readonly IServiceRepository _serviceRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;
        string contentRootPath = string.Empty;
        private ILoggerManager _logger;
        public TrendProjectMapController(IServiceRepository serviceRepository, ILoggerManager logger, IHostingEnvironment hostingEnvironment, IConfiguration configuration)
        {
            _hostingEnvironment = hostingEnvironment;
            _serviceRepository = serviceRepository;
            _logger = logger;
            _configuration = configuration;
            contentRootPath = _hostingEnvironment.ContentRootPath;
        }

        public ActionResult Index(string id, string currentFilter, string search, int PageStart, string Direction, int totalRecords, int currentpage)
        {
            MainTrendProjectMapInsertUpdateDbViewModel model = new MainTrendProjectMapInsertUpdateDbViewModel();
            try
            {
                SetRoleList();
                model.TrendId = new Guid(id);
                model.ProjectName = search;
                //int PageStart = 0;
                int pagesize = int.Parse(_configuration.GetSection("Pagination")["TrendPagingSize"]);
                int SortDirection = 1;
                ViewBag.Message = string.Empty;

                if (Direction == "Next" && (PageStart == 0 || PageStart < (totalRecords - pagesize)))
                {
                    PageStart = PageStart + pagesize;
                }
                else if (Direction == "Previous")
                {
                    if (PageStart >= pagesize)
                        PageStart = PageStart - pagesize;
                    else
                    {
                        PageStart = 0;
                    }
                }
                if (string.IsNullOrEmpty(Direction) && currentpage > 0)
                {
                    PageStart = currentpage * pagesize;
                    PageStart = (PageStart - pagesize);
                }
                else
                {
                    //PageStart = PageStart ?? 0 + pagesize;
                }

                //------Get Static master data for Impact           
                HttpResponseMessage responseImpactType = _serviceRepository.GetResponse("ImpactType");
                responseImpactType.EnsureSuccessStatusCode();
                ViewBag.ImpactList = responseImpactType.Content.ReadAsAsync<List<ImpactTypeViewModel>>().Result;


                HttpResponseMessage responseTrend = _serviceRepository.GetByIdResponse("Trend", id);
                responseTrend.EnsureSuccessStatusCode();
                TrendViewModel trend = responseTrend.Content.ReadAsAsync<TrendViewModel>().Result;
                if (trend != null)
                    model.TrendModel = trend;

                //------Get trend note by trend Id           
                HttpResponseMessage responseTrendNote = _serviceRepository.GetByIdResponse("TrendNote", id, "Project", "id");
                responseTrendNote.EnsureSuccessStatusCode();
                var tnm = responseTrendNote.Content.ReadAsAsync<TrendNoteViewModel>().Result;
                model.TrendNoteModel = tnm ?? new TrendNoteViewModel();
                model.TrendModel.TrendStatusName = model.TrendNoteModel.TrendStatusName;
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.GetResponse(contollerName, model.TrendId, PageStart, pagesize, SortDirection, "IsMapped", search);
                response.EnsureSuccessStatusCode();
                List<TrendProjectMapViewModel> trendProjectMap = response.Content.ReadAsAsync<List<TrendProjectMapViewModel>>().Result;
                //Guid prjId = model.TrendModel.ProjectID;
                //var trendProjList = trendProjectMap.Where(p => p.ProjectId != prjId).ToList();

                model.TrendProjectMapInsertUpdate = trendProjectMap;

                if (trendProjectMap != null && trendProjectMap.Count > 0)
                {
                    totalRecords = trendProjectMap.FirstOrDefault().TotalItems;
                    ViewBag.totalPage = (int)Math.Ceiling(totalRecords / (double)pagesize);
                }

                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, model.TrendModel.ProjectID);
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                }


                ViewBag.totalRecords = totalRecords;
                ViewBag.PageStart = PageStart;
                ViewBag.currentpage = currentpage;
                ViewBag.Id = model.TrendId;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(model);
        }

        private void SetRoleList()
        {
            try
            {
                var roles = HttpContext.Session.GetString("userRoles");
                var userRoles = JsonConvert.DeserializeObject<List<RoleViewModel>>(roles);
                List<string> RoleListItems = new List<string>();
                foreach (var item in userRoles)
                    RoleListItems.Add(item.UserRoleName);
                ViewBag.UserRoleList = RoleListItems;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }


        [HttpPost]
        public IActionResult Index(MainTrendProjectMapInsertUpdateDbViewModel model, string actionName, int PageStart, string Direction, int totalRecords, int currentpage)
        {
            SetRoleList();
            ViewBag.Message = string.Empty;
            int pagesize = int.Parse(_configuration.GetSection("Pagination")["TrendPagingSize"]);
            int SortDirection = 1; 
            bool bResult = false;
            try
            {
                model.TrendNoteModel.TrendType = "Project";
                if (actionName.Equals("save"))
                {

                    bResult = validate(model);
                    if (bResult)
                    {
                        model.TrendNoteModel.TrendStatusName = "Draft";
                        var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                        HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, model);
                        response.EnsureSuccessStatusCode();
                        ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            //ViewBag.Message = viewModel.Message;
                            return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.ProjectName });
                        }
                    }

                }
                else if (actionName.Equals("Submitted"))
                {

                    bResult = validate(model);
                    if (bResult)
                    {
                        model.TrendNoteModel.TrendStatusName = "Submitted";
                        var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                        HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, model);
                        response.EnsureSuccessStatusCode();
                        ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            //ViewBag.Message = viewModel.Message;
                            return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.ProjectName });
                        }
                    }

                }
                else if (actionName.Equals("Rejected"))
                {
                    bResult = validate(model);
                    if (bResult)
                    {
                        model.TrendNoteModel.TrendStatusName = "Rejected";
                        var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                        HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, model);
                        response.EnsureSuccessStatusCode();
                        ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            //ViewBag.Message = viewModel.Message;
                            return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.ProjectName });
                        }
                    }

                }
                else if (actionName.Equals("Approve"))
                {
                    bResult = validate(model);
                    if (bResult)
                    {
                        model.TrendNoteModel.TrendStatusName = "Approved";
                        var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                        HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, model);
                        response.EnsureSuccessStatusCode();
                        ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            //ViewBag.Message = viewModel.Message;
                            return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.ProjectName });
                        }
                    }

                }
                else if (actionName.Equals("search"))
                {
                    return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.ProjectName });
                }
                else if (actionName.Equals("Next"))
                {
                    return RedirectToAction("Index", "TrendRegionMap", new { id = model.TrendId.ToString() });
                }
                else if (actionName.Equals("Previous"))
                {
                    return RedirectToAction("Index", "TrendMarketMap", new { id = model.TrendId.ToString() });
                }
                else if (actionName.Equals("Cancel"))
                {
                    return RedirectToAction("Index", "TrendProjectMap", new { id = model.TrendId.ToString() });
                }
                HttpResponseMessage responseImpactType = _serviceRepository.GetResponse("ImpactType");
                responseImpactType.EnsureSuccessStatusCode();
                ViewBag.ImpactList = responseImpactType.Content.ReadAsAsync<List<ImpactTypeViewModel>>().Result;

                HttpResponseMessage responseTrend = _serviceRepository.GetByIdResponse("Trend", model.TrendId.ToString());

                TrendViewModel trend = responseTrend.Content.ReadAsAsync<TrendViewModel>().Result;
                model.TrendModel = trend ?? new TrendViewModel();

                HttpResponseMessage responseTrendNote = _serviceRepository.GetByIdResponse("TrendNote", model.TrendId.ToString(), "Company Group", "id");
                responseTrendNote.EnsureSuccessStatusCode();
                var tnm = responseTrendNote.Content.ReadAsAsync<TrendNoteViewModel>().Result;
                model.TrendNoteModel = tnm ?? new TrendNoteViewModel();
                model.TrendModel.TrendStatusName = model.TrendNoteModel.TrendStatusName;

                var contollerName2 = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response1 = _serviceRepository.GetResponse(contollerName2, model.TrendId, PageStart, pagesize, SortDirection, "IsMapped", string.Empty);
                response1.EnsureSuccessStatusCode();
                List<TrendProjectMapViewModel> trendCompGroupMap = response1.Content.ReadAsAsync<List<TrendProjectMapViewModel>>().Result;

                //Guid prjId = model.TrendModel.ProjectID;
                //var trendProjList = trendCompGroupMap.Where(p => p.ProjectId != prjId).ToList();

                model.TrendProjectMapInsertUpdate = trendCompGroupMap;
                if (trendCompGroupMap != null && trendCompGroupMap.Count > 0)
                {
                    totalRecords = trendCompGroupMap.FirstOrDefault().TotalItems;
                    ViewBag.totalPage = totalRecords / pagesize;
                }

                ViewBag.totalRecords = totalRecords;
                ViewBag.PageStart = PageStart;
                ViewBag.currentpage = currentpage;
                ViewBag.Id = model.TrendId;

                View(model);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return View(model);
        }

        /// <summary>
        /// Method to validate Form
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private bool validate(MainTrendProjectMapInsertUpdateDbViewModel model)
        {
            
            if (model.TrendProjectMapInsertUpdate != null && model.TrendProjectMapInsertUpdate.Count(x => x.ImpactId != Guid.Empty) < 2)
            {
                ViewBag.Message = "Please select atleast two Projects for the trend mapping.";
                return false;
            }
            else if (model.TrendProjectMapInsertUpdate != null && model.TrendProjectMapInsertUpdate.Count(x => (x.ImpactId != Guid.Empty && x.EndUser)) < 1)
            {
                ViewBag.Message = "Please select atleast one end user for the trend mapping.";
                return false;
            }
            else if (model.TrendProjectMapInsertUpdate != null && model.TrendProjectMapInsertUpdate.Where(x => x.ImpactId != Guid.Empty).Select(x => x.ImpactId).Distinct().Count() == 1)
            {
                ViewBag.Message = "Minimum 1 Direct and Minimum 1 Indirect to be selected.";
                return false;
            }
            else if (model.TrendProjectMapInsertUpdate != null && model.TrendProjectMapInsertUpdate.Any(x => x.ImpactId == Guid.Empty && x.EndUser))
            {
                ViewBag.Message = "Please select direct/indirect impact value.";
                return false;
            }
            if (model.TrendNoteModel.AuthorRemark == null || model.TrendNoteModel.AuthorRemark == "" || model.TrendNoteModel.AuthorRemark == string.Empty)
            {
                ViewBag.Message = "Please enter Author Remark!!";
                return false;
            }
            else
            {
                ViewBag.Message = string.Empty;
                return true;
            }
        }
    }
}