﻿CREATE procedure [dbo].[SApp_GetAllMarkets]

as
/*================================================================================
Procedure Name: [SApp_GetAllMarkets]
Author: Sai Krishna
Create date: 09/05/2019
Description: Get list from Markets
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
09/05/2019	Sai Krishna		Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT [Id] as MarketId, 
		[MarketName]
	FROM [dbo].[Market]
where ([Market].[IsDeleted] = 0)  order by MarketName
end