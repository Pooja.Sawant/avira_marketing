﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.Trend
{
    public class TrendProjectMapViewModel
    {
        public Guid TrendId { get; set; }
        public Guid ProjectId { get; set; }
        public Guid ImpactId { get; set; }
        public string ProjectName { get; set; }
        public bool IsMapped { get; set; }
        public bool EndUser { get; set; }
        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "ProjectMap";
            }
        }
    }

    public class MainTrendProjectMapInsertUpdateDbViewModel
    {
        public Guid TrendId { get; set; }
        public string ProjectName { get; set; }
        public List<TrendProjectMapViewModel> TrendProjectMapInsertUpdate { get; set; }
        public Guid UserCreatedById { get; set; }
        public TrendNoteInsertUpdateDbViewModel TrendNote { get; set; }
        public TrendViewModel TrendModel { get; set; }
        public TrendNoteViewModel TrendNoteModel { get; set; }
        public bool IsDeleted { get; set; }
    }
}
