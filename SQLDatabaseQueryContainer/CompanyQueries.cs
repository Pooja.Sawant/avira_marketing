﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public static class CompanyQueries
    {
        public static readonly string StoreProcInsert = "SApp_insertCompany";

        public static readonly string StoreProcUpdate = "SApp_UpdateCompany";

        public static string StoreProcSelectAll = "SApp_CompanyGridWithFilters";
    }
}
