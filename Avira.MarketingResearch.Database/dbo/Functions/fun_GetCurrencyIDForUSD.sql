﻿CREATE FUNCTION [dbo].[fun_GetCurrencyIDForUSD]
(
)
/*================================================================================  
Function Name: [fun_GetCurrencyIDForUSD] 
Author: Nitin  
Create date: 11-Oct-2019  
Description: Get USD Currency ID
Change History  
Date        Developer     Reason  
__________  ____________  ______________________________________________________  
11-Oct-2019 Nitin Tarkar   Initial Version  
================================================================================*/ 
RETURNS uniqueidentifier 
AS
BEGIN
	
	Declare @USDCurrencyId uniqueidentifier	

	Select @USDCurrencyId = Id 
	From Currency
	Where CurrencyName = 'United States dollar';

	Return @USDCurrencyId
END