﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.QualitativeInfo;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.QualitativeInfo
{
    public interface IQualitativeAddTableRepository
    {
        IEnumerable<QualitativeAddTableViewModel> GetAll(Guid Id);
        Result<SpTransactionMessage> Insert(QualitativeGenerateTableCRUDViewModel viewModel);
        Result<SpTransactionMessage> Update(QualitativeGenerateTableCRUDViewModel viewModel);
    }
}
