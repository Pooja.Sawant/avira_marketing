﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Avira.MarketingResearch.Repository.Implementations
{
    public class RawMaterialRepository : IRawMaterialRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "RawMaterial";
        public RawMaterialRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }


        public RawMaterialViewModel GetById(Guid guid)
        {
            RawMaterialViewModel rawMaterialViewModel = new RawMaterialViewModel();
           rawMaterialViewModel = GetAllRawMaterial(guid).FirstOrDefault();
            return rawMaterialViewModel;
        }

        public IEnumerable<RawMaterialViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search,bool includeDeleted)
        {
            return GetAllRawMaterial(null, PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, includeDeleted);
        }

        private IEnumerable<RawMaterialViewModel> GetAllRawMaterial(Guid? guid, int PageStart=0, int pageSize=0, int SortDirection=-1, string OrdbyByColumnName="", string Search="", bool includeDeleted = false)
        {           

            DataTable ramMaterialIdsTable = new DataTable();
            ramMaterialIdsTable.Columns.Add("ID", typeof(Guid));

            if (guid != null)
            {
                ramMaterialIdsTable.Rows.Add(guid);
            }            
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<RawMaterialViewModel>(RawMaterialsQueries.StoreProcSelectAll
                            , new { @RawMaterialIDList = ramMaterialIdsTable.AsTableValuedParameter("dbo.udtUniqueIdentifier"), @includeDeleted = includeDeleted, @PageStart = PageStart, @pageSize = pageSize, @SortDirection = SortDirection, @OrdbyByColumnName = OrdbyByColumnName, @RawMaterialName = Search }
                            ,null, true, null, CommandType.StoredProcedure);
        }

        public Result<SpTransactionMessage> Create(RawMaterialInsertDbViewModel rawMaterialInsertDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(RawMaterialsQueries.StoreProcInsert, rawMaterialInsertDbViewModel, CommandType.StoredProcedure);
            return spTransactionMessage;

        }

        public Result<SpTransactionMessage> Update(RawMaterialUpdateDbViewModel rawMaterialUpdateDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(RawMaterialsQueries.StoreProcUpdate, rawMaterialUpdateDbViewModel, CommandType.StoredProcedure);
            return spTransactionMessage;
        }



    }
}
