﻿using Avira.MarketingResearch.Model.ViewModels.Masters;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.Trend
{
    public class TrendEcoSystemMapViewModel
    {
        public Guid Id { get; set; }
        public Guid TrendId { get; set; }
        public Guid EcoSystemId { get; set; }
        public Guid ImpactId { get; set; }
        public string EcoSystemName { get; set; }
        public bool EndUser { get; set; }
        public bool IsMapped { get; set; }
        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "TrendEcoSystemMap";
            }
        }
    }

    public class MainTrendEcoSystemMapInsertUpdateDbViewModel
    {
        public Guid TrendId { get; set; }
        public string EcoSystemName { get; set; }
        public List<TrendEcoSystemMapViewModel> TrendEcoSystemMapInsertUpdate { get; set; }
        public Guid AviraUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public TrendViewModel TrendModel { get; set; }
        public TrendNoteViewModel TrendNoteModel { get; set; }
    }

}
