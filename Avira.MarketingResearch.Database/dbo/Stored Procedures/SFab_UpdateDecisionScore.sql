﻿CREATE PROCEDURE [dbo].[SFab_UpdateDecisionScore]
(
	@DecisionId uniqueidentifier,
	@UserId	uniqueidentifier
)
As   

--declare @DecisionId uniqueidentifier = 'a22ec61e-8e75-479d-b3df-3173d4eb3de7',
--	@UserId	uniqueidentifier = '1570416b-443a-4833-aa8f-941e159dd90f'


/*================================================================================    
Procedure Name: SFab_UpdateDecisionScore   
Author: Praveen    
Create date: 07-Jan-2020
Description: Update decision status by user
Change History    
Date  Developer  Reason  
__________ ____________ ______________________________________________________    
07-Jan-2020  Praveen   Initial Version 
08-Jan-2020  Harshal   Added Statement in SP
================================================================================*/    
BEGIN  
Declare @ErrorMsg NVARCHAR(2048);
BEGIN TRY
Begin    
------------------
--Required Tables: Decision
--Notes: N/A
--Case 1: N/A
--Expected Result :SELECT StoredProcedureName ='SFab_UpdateDecisionScore',Message = @ErrorMsg,Success = 1/0;
	Execute SFab_CopyDecisionFromBase @InDecisionId = @DecisionId, @UserId = @UserId, @CopyDecisionParametersFromBase = 0, @OutDecisionId = @DecisionId OUTPUT
	 
	Declare @ParamCount Int
	Declare @Weightage Decimal (19, 2)
	Declare @Score Decimal (19, 2)

	
	Select @ParamCount = Count(*), @Weightage = Round(Sum(Weightage),0), 
		@Score = Round(Sum(Weightage/100 * Cast(CategoryName As Int)), 2)
	From DecisionParameter DP
		Inner Join [LookupCategory] LC On 
			DP.RatingId = LC.Id
	Where DecisionId = @DecisionId
	 
	If (@Weightage = 100)
	Begin

		if (@Score > 5) 
		Begin

			Set @Score = 5.00
		End
		 UPDATE Decision 
		 SET Score = @Score 
		 WHERE Id = @DecisionId And 
			   UserId = @UserId
	End
	SELECT StoredProcedureName ='SFab_UpdateDecisionScore',Message =@ErrorMsg,Success = 1;   
END
END TRY
BEGIN CATCH
    -- Execute the error retrieval routine.
EXEC SApp_LogErrorInfo

 set @ErrorMsg = 'Error while Update Decision score, please contact Admin for more details.';    
 SELECT StoredProcedureName ='SFab_UpdateDecisionScore',Message =@ErrorMsg,Success = 0;   

END CATCH 
END