﻿CREATE procedure [dbo].[SApp_GetMarketbyID]
(@MarketID uniqueidentifier=null,
@ParentMarketID uniqueidentifier = null,
@includeDeleted bit = 0,
@IncludeParentOnly bit = 0)
as
/*================================================================================
Procedure Name: SApp_GetMarketbyID
Author: Sharath
Create date: 02/19/2019
Description: Get list from Market table by Name or ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
02/19/2019	Sharath			Initial Version
__________	____________	______________________________________________________
04/11/2019	Pooja			Change for all list of market and sub market
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if @includeDeleted is null set @includeDeleted = 0

SELECT [Id],
		[MarketName],
		[Description],
		[ParentId]
FROM [dbo].[Market]
where (@MarketID is null or ID = @MarketID)
and (@ParentMarketID is null or ParentId = @ParentMarketID)
and (@includeDeleted = 1 or IsDeleted = 0)
and (@IncludeParentOnly = 0 or ParentId is null)

end