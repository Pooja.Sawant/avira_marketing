﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.ImportExport;
using Avira.MarketingResearch.Common.Service_Implementation.ImportExport;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QualitativeAnalysisImportExportController : AviraBaseController
    {
        private ILoggerManager _logger;
        public readonly IQualitativeAnalysisImportExportService _qualitativeAnalysisImportExportService;

        public QualitativeAnalysisImportExportController(IQualitativeAnalysisImportExportService qualitativeAnalysisImportExportService, ILoggerManager logger)
        {
            _qualitativeAnalysisImportExportService = qualitativeAnalysisImportExportService;
            _logger = logger;
        }
        
        [HttpPost]
        public ActionResult Post([FromBody] QualitativeAnalysisImportModel viewModel)
        {
            ImportCommonResponseMainModel viewData = new ImportCommonResponseMainModel()
            {
                IsSuccess = false,
                Message = "Error while creating a new Qualitative Analysis, please contact Admin for more details."
            };
            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                var result = _qualitativeAnalysisImportExportService.Create(viewModel);

                var res = result.Where(s => s.ErrorNotes != null).ToList();

                if (res.Count <= 0)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Import Qualitative Analysis successfully.";
                }
                else
                {
                    viewData.Message = "Import Qualitative Analysis have some errors";
                    //Export to Excel data.
                    //foreach (var item in result.ToList())
                    //{
                    //    item.ErrorNotes = item.ErrorNotes.Replace(",", " ");
                    //}
                    viewData.ResponseModel = result;

                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);

                viewData = new ImportCommonResponseMainModel()
                {
                    IsSuccess = false,
                    Message = "Error while creating a new Qualitative Analysis. Error Details: " + ex.Message + ". please contact Admin for more details."
                };
            }
            return Ok(viewData);
        }

    }
}