﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.Helpers;
using Avira.MarketingResearch.Web.Utils;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Avira.MarketingResearch.Web.Controllers
{
    [Authorize]
    [NoCache]
    public class CompanyProductController : BaseController
    {
        public readonly IServiceRepository _serviceRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;
        string contentRootPath = string.Empty;
        private ILoggerManager _logger;
        private IAPIHelper _iAPIHelper;

        public CompanyProductController(IAPIHelper iAPIHelper, IServiceRepository serviceRepository, ILoggerManager logger, IHostingEnvironment hostingEnvironment, IConfiguration configuration)
        {
            _hostingEnvironment = hostingEnvironment;
            _serviceRepository = serviceRepository;
            _logger = logger;
            _configuration = configuration;
            contentRootPath = _hostingEnvironment.ContentRootPath;
            _iAPIHelper = iAPIHelper;
        }

        public IActionResult Index(string companyId, string projectId)
        {
            SetRoleList();
            BindDropdowns(); 
            MainCompanyProductViewMoel mainCompanyProductViewMoel = new MainCompanyProductViewMoel();
            try
            {
                var ContollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage CompanyProductResponse = _serviceRepository.GetByIdResponse(ContollerName, companyId);
                mainCompanyProductViewMoel = CompanyProductResponse.Content.ReadAsAsync<MainCompanyProductViewMoel>().Result;
                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, new Guid(projectId));
                if (mainCompanyProductViewMoel.CompanyProductViewMoel.Count == 0)
                {
                    CompanyProductViewMoel objempty = new CompanyProductViewMoel();
                    objempty.ProjectName = project.ProjectName;
                    objempty.LaunchDate = DateTime.Now;
                    mainCompanyProductViewMoel.CompanyProductViewMoel.Add(objempty);
                }
                mainCompanyProductViewMoel.CompanyId = new Guid(companyId);
                mainCompanyProductViewMoel.ProjectId = new Guid(projectId);
                HttpResponseMessage CompanyNoteResponse = _serviceRepository.GetCompanyNoteByIdResponse("CompanyNote", companyId, "CompanyProduct", projectId);
                CompanyNoteViewModel companyNoteViewModel = CompanyNoteResponse.Content.ReadAsAsync<CompanyNoteViewModel>().Result;
                mainCompanyProductViewMoel.CompanyNoteViewModel = companyNoteViewModel ?? new CompanyNoteViewModel();
                HttpResponseMessage CompanyResponse = _serviceRepository.GetByIdResponse("Company", companyId);
                CompanyViewModel companyViewModel = CompanyResponse.Content.ReadAsAsync<CompanyViewModel>().Result;
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                    ViewBag.StatusName = mainCompanyProductViewMoel.CompanyNoteViewModel.StatusName;
                }
                ViewBag.CompanyId = mainCompanyProductViewMoel.CompanyId;
                ViewBag.CompanyName = companyViewModel.CompanyName;
                ViewBag.ApiBaseURL = _iAPIHelper.GetBaseURL();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(mainCompanyProductViewMoel);
        }

        private void BindDropdowns()
        {
            try
            {
                HttpResponseMessage responseProductCategory = _serviceRepository.GetResponse("ProductCategory");
                responseProductCategory.EnsureSuccessStatusCode();
                ViewBag.ProductCategoryList = responseProductCategory.Content.ReadAsAsync<List<ProductCategoryViewModel>>().Result;

                HttpResponseMessage responseSegment = _serviceRepository.GetResponse("Segment");
                responseSegment.EnsureSuccessStatusCode();
                ViewBag.SegmentList = responseSegment.Content.ReadAsAsync<List<SegmentViewModel>>().Result;

                HttpResponseMessage responseCountry = _serviceRepository.GetResponse("Country");
                responseCountry.EnsureSuccessStatusCode();
                ViewBag.CountryList = responseCountry.Content.ReadAsAsync<List<CountryViewModel>>().Result;

                HttpResponseMessage responseProductStatus = _serviceRepository.GetResponse("ProductStatus");
                responseProductStatus.EnsureSuccessStatusCode();
                ViewBag.ProductStatusList = responseProductStatus.Content.ReadAsAsync<List<ProductStatusViewModel>>().Result;

                HttpResponseMessage responseCurrency = _serviceRepository.GetResponse("Currency");
                responseCurrency.EnsureSuccessStatusCode();
                ViewBag.CurrencyList = responseCurrency.Content.ReadAsAsync<List<CurrencyViewModel>>().Result;

                HttpResponseMessage valueResponse = _serviceRepository.GetAllParentResponse("TrendValueMap");
                valueResponse.EnsureSuccessStatusCode();
                ViewBag.ValueList = valueResponse.Content.ReadAsAsync<List<ValueConversionViewModel>>().Result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        private void SetRoleList()
        {
            try
            {
                var roles = HttpContext.Session.GetString("userRoles");
                var userRoles = JsonConvert.DeserializeObject<List<RoleViewModel>>(roles);
                List<string> RoleListItems = new List<string>();
                foreach (var item in userRoles)
                    RoleListItems.Add(item.UserRoleName);
                ViewBag.UserRoleList = RoleListItems;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        /// <summary>
        /// Method to validate Form
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns> 
        private bool validate(MainCompanyProductViewMoel model)
        {
            int i = 0;
            int count = model.CompanyProductViewMoel.Count;
            int flag = 0;
            do
            {
                if (model.CompanyProductViewMoel[i].ProductName == String.Empty || model.CompanyProductViewMoel[i].ProductName == "" || model.CompanyProductViewMoel[i].ProductName == null)
                {
                    ViewBag.Message = "Please enter Product Name.";
                    flag++;
                    break;
                }
                if (model.CompanyProductViewMoel[i].LaunchDate == DateTime.MinValue)
                {
                    ViewBag.Message = "Please select correct date.";
                    flag++;
                    break;
                }
                else if (model.CompanyProductViewMoel[i].ProductCategoryId == null)
                {
                    ViewBag.Message = "Please select product category";
                    flag++;
                    break;
                }
                else if (model.CompanyProductViewMoel[i].ProductSegmentId == null)
                {
                    ViewBag.Message = "Please select segment";
                    flag++;
                    break;
                }
                else if (model.CompanyProductViewMoel[i].ProductCountryId == null)
                {
                    ViewBag.Message = "Please select Country";
                    flag++;
                    break;
                }
                else if (model.CompanyProductViewMoel[i].ProductCountryId == null)
                {
                    ViewBag.Message = "Please select segment";
                    flag++;
                    break;
                }
                else if (model.CompanyProductViewMoel[i].ProductStatusId == null)
                {
                    ViewBag.Message = "Please select Status";
                    flag++;
                    break;
                }
                else if (model.CompanyNoteViewModel.AuthorRemark == null)
                {
                    ViewBag.Message = "Please select Author Notes!!";
                    flag++;
                    break;
                }
                i++;
                if (i > count - 1)
                    break;

            } while (true);
            if (flag > 0)
                return false;
            else
                return true;
        }

        [HttpPost]
        public IActionResult Index(MainCompanyProductViewMoel model, string actionName)
        {
            SetRoleList();
            bool bResult = false;
            model.CompanyNoteViewModel.CompanyType = "CompanyProduct";
            try
            {
                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, model.ProjectId);
                if (ModelState.IsValid)
                {
                    var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                    if (actionName.Equals("Save"))
                    {
                        bResult = validate(model);
                        if (bResult)
                        {
                            if (model.CompanyProductViewMoel != null && model.CompanyProductViewMoel.Count > 0)
                            {
                                for (int i = 0; i < model.CompanyProductViewMoel.Count; i++)
                                {
                                    if (model.CompanyProductViewMoel[i].ImageFile != null)
                                    {

                                        using (var memoryStream = new MemoryStream())
                                        {
                                            model.CompanyProductViewMoel[i].ImageFile.CopyToAsync(memoryStream);
                                            model.CompanyProductViewMoel[i].fileByte = memoryStream.ToArray();
                                        }
                                        model.CompanyProductViewMoel[i].ImageDisplayName = model.CompanyProductViewMoel[i].ImageFile.FileName;
                                        model.CompanyProductViewMoel[i].fileExtension = model.CompanyProductViewMoel[i].ImageFile.FileName.Substring(model.CompanyProductViewMoel[i].ImageFile.FileName.LastIndexOf('.')).ToLower();
                                        model.CompanyProductViewMoel[i].ImageFile = null;
                                    }
                                }
                            }
                            model.CompanyNoteViewModel.CompanyStatusName = "Draft";
                            HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                            responseSave.EnsureSuccessStatusCode();
                            ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                            if (viewModel != null)
                            {
                                ViewBag.Message = viewModel.Message.ToString();
                                return RedirectToAction("Index", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                            }
                        }
                    }
                    else if (actionName.Equals("Submitted"))
                    {
                        bResult = validate(model);
                        if (bResult)
                        {
                            if (model.CompanyProductViewMoel != null && model.CompanyProductViewMoel.Count > 0)
                            {
                                for (int i = 0; i < model.CompanyProductViewMoel.Count; i++)
                                {
                                    if (model.CompanyProductViewMoel[i].ImageFile != null)
                                    {

                                        using (var memoryStream = new MemoryStream())
                                        {
                                            model.CompanyProductViewMoel[i].ImageFile.CopyToAsync(memoryStream);
                                            model.CompanyProductViewMoel[i].fileByte = memoryStream.ToArray();
                                        }
                                        model.CompanyProductViewMoel[i].ImageDisplayName = model.CompanyProductViewMoel[i].ImageFile.FileName;
                                        model.CompanyProductViewMoel[i].fileExtension = model.CompanyProductViewMoel[i].ImageFile.FileName.Substring(model.CompanyProductViewMoel[i].ImageFile.FileName.LastIndexOf('.')).ToLower();
                                        model.CompanyProductViewMoel[i].ImageFile = null;
                                    }
                                }
                            }
                            model.CompanyNoteViewModel.CompanyStatusName = "Submitted";
                            HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                            responseSave.EnsureSuccessStatusCode();
                            ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                            if (viewModel != null)
                            {
                                return RedirectToAction("Index", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                            }
                        }

                    }
                    else if (actionName.Equals("Reject"))
                    {
                        bResult = validate(model);
                        if (bResult)
                        {
                            model.CompanyNoteViewModel.CompanyStatusName = "Rejected";
                            HttpResponseMessage responseSave = _serviceRepository.PostResponseWithFile(contollerName, model);
                            responseSave.EnsureSuccessStatusCode();
                            ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                            if (viewModel != null)
                            {
                                return RedirectToAction("Index", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                            }
                        }
                    }
                    else if (actionName.Equals("Approve"))
                    {
                        bResult = validate(model);
                        if (bResult)
                        {
                            model.CompanyNoteViewModel.CompanyStatusName = "Approved";
                            HttpResponseMessage responseSave = _serviceRepository.PostResponseWithFile(contollerName, model);
                            responseSave.EnsureSuccessStatusCode();
                            ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                            if (viewModel != null)
                            {
                                return RedirectToAction("Index", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                            }
                        }
                    }

                    else if (actionName.Equals("Next"))
                        return RedirectToAction("Index", "CompanyProfitAndLoss", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                    else if (actionName.Equals("Previous"))
                        return RedirectToAction("Index", "CompanyOrganization", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                    else if (actionName.Equals("Cancel"))
                        return RedirectToAction("Index", "#", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                    else if (actionName.Equals("AddRow"))
                    {
                        CompanyProductViewMoel objempty = new CompanyProductViewMoel();
                        objempty.ProjectName = project.ProjectName;
                        objempty.LaunchDate = DateTime.Now;
                        model.CompanyProductViewMoel.Add(objempty);
                        model.CompanyNoteViewModel = model.CompanyNoteViewModel ?? new CompanyNoteViewModel();
                    }

                    BindDropdowns();

                    ViewBag.CompanyId = model.CompanyId;
                    ViewBag.ProjectId = model.ProjectId;
                }
                else
                {
                    BindDropdowns();
                    if (actionName.Equals("AddRow"))
                    {
                        CompanyProductViewMoel objempty = new CompanyProductViewMoel();
                        objempty.ProjectName = project.ProjectName;
                        objempty.LaunchDate = DateTime.Now;
                        model.CompanyProductViewMoel.Add(objempty);
                        model.CompanyNoteViewModel = model.CompanyNoteViewModel ?? new CompanyNoteViewModel();
                    }
                    ViewBag.CompanyId = model.CompanyId;
                    ViewBag.ProjectId = model.ProjectId;
                }
                HttpResponseMessage CompanyNoteResponse = _serviceRepository.GetCompanyNoteByIdResponse("CompanyNote", model.CompanyId.ToString(), "CompanyProduct", model.ProjectId.ToString());
                CompanyNoteViewModel companyNoteViewModel = CompanyNoteResponse.Content.ReadAsAsync<CompanyNoteViewModel>().Result;
                companyNoteViewModel = companyNoteViewModel ?? new CompanyNoteViewModel();
                HttpResponseMessage CompanyResponse = _serviceRepository.GetByIdResponse("Company", model.CompanyId.ToString());
                CompanyViewModel companyViewModel = CompanyResponse.Content.ReadAsAsync<CompanyViewModel>().Result;
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                    ViewBag.StatusName = companyNoteViewModel.StatusName??string.Empty;
                }
                
                ViewBag.CompanyName = companyViewModel.CompanyName;
                ViewBag.ApiBaseURL = _iAPIHelper.GetBaseURL();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(model);
        }

    }
}