﻿CREATE procedure [dbo].[SFab_GetCountryWithCode]
(@CountryID uniqueidentifier=null,
@includeDeleted bit = 0)
as
/*================================================================================
Procedure Name: SFab_GetCountryWithCode 
Author: Pooja
Create date: 05/10/2019
Description: Get list from Country table with SD code
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
05/10/2019	Pooja			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if @includeDeleted is null set @includeDeleted = 0

SELECT [Id],
	[STDCode],
      [CountryName]
FROM [dbo].[Country]
where (@CountryID is null or ID = @CountryID)
and (@includeDeleted = 1 or IsDeleted = 0) and CountryCode != 'N/A'
order by CountryName asc
end