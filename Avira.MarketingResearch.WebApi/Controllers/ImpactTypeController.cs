﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImpactTypeController : AviraBaseController
    {
        public readonly IImpactTypeService _iImpactTypeService;
        private ILoggerManager _logger;
        public ImpactTypeController(
            IImpactTypeService impactTypeService, ILoggerManager logger)
        {
            _iImpactTypeService = impactTypeService;
            _logger = logger;
        }

        [HttpGet]       
        public ActionResult<IEnumerable<ImpactTypeViewModel>> Get()
        {
            List<ImpactTypeViewModel> res = null;
            try
            {
                res = _iImpactTypeService.GetAll().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

    }
}