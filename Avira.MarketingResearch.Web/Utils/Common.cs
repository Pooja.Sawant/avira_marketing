﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Avira.MarketingResearch.Web.Utils
{
    public class Common
    {
        public static int TrendPagingSize = 10;
        public static List<List<T>> splitList<T>(List<T> locations, int nSize = 30)
        {
            var list = new List<List<T>>();

            for (int i = 0; i < locations.Count; i += nSize)
            {
                list.Add(locations.GetRange(i, Math.Min(nSize, locations.Count - i)));
            }

            return list;
        }
        public static IEnumerable<List<T>> splitList_Old<T>(List<T> locations, int nSize = 30)
        {
            for (int i = 0; i < locations.Count; i += nSize)
            {
                yield return locations.GetRange(i, Math.Min(nSize, locations.Count - i));
            }
        }

        public static bool IsValidExcelRow(ExcelWorksheet worksheet, int row, int colCount)
        {
            for (int colIndex=1; colIndex <= colCount; colIndex++)
            {
                if(worksheet.Cells[row, colIndex].Value != null || ((string) worksheet.Cells[row, colIndex].Value) == string.Empty)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
