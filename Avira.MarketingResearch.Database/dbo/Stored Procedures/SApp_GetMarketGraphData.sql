﻿
  
CREATE PROCEDURE [dbo].[SApp_GetMarketGraphData]    
	@SegmentID uniqueidentifier,
	@ProjectID uniqueidentifier,
	@MetricType nvarchar(100)
 AS  
/*================================================================================  
Procedure Name: [SApp_GenerateMarketGraphData]  
Author: Sharath  
Create date: 11/07/2019  
Description: Get list of conversion values from table  
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
04/26/2019 Swami   Initial Version  
================================================================================*/  
  
BEGIN
DECLARE  @ValueName	nvarchar(100)
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	IF NOT EXISTS(SELECT 1 FROM Segment WHERE Segment.Id = @SegmentID)
	BEGIN			
			SELECT @SegmentID = Id FROM Segment
			WHERE SegmentName = 'End User'
	END
SELECT TOP 1 @ValueName = ValueName 
from ValueConversion VC
where exists (select 1 FROM MarketValue as MV
WHERE mv.ProjectId =  @ProjectID  --06/17/2019 comment for testing by Pooja
and (((@MetricType is null or @MetricType = '' or @MetricType = 'Value') and vc.Id = mv.ValueConversionId)
or (@MetricType = 'Volume' and vc.Id = mv.VolumeUnitId)
or (@MetricType = 'ASP' and vc.Id = mv.ASPUnitId)
))
order by vc.Conversion desc;

	SELECT [BarChartJson]
      ,[MapJson]
      ,[TableJson],
	  @ValueName as ValueConversionName
  FROM [dbo].[MarketGraphData]
  where  [ProjectID] = @ProjectID
  and [SegmentID] = @SegmentID
  and MetricType = @MetricType;
END