﻿using Avira.MarketingResearch.Model;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.WebApi.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Net.Http;

namespace Avira.MarketingResearch.WebApi
{
    public class AviraBaseController : ControllerBase
    {
        //Hardcoding the userid for demo purposes
        public AviraUser CurrentUser
        {
            get
            {
                AviraUser u = new AviraUser
                {
                    Id = Guid.Parse("6B5A19C0-45E6-4FFC-815F-75F7C3D22B79"),
                };
                return u;
            }
        }

        //protected HttpResponseMessage Error(string errorMessage)
        //{
        //    return Request.CreateResponse(HttpStatusCode.BadRequest, Envelope.Error(errorMessage));
        //}


        //protected new HttpResponseMessage Ok()
        //{

        //    return Request.CreateResponse(HttpStatusCode.OK, Envelope.Ok());
        //}

        //protected new HttpResponseMessage Ok<T>(T result)
        //{

        //    return Request.CreateResponse(HttpStatusCode.OK, Envelope.Ok(result));
        //}

        //protected new HttpResponseMessage Ok<T>(T result, IViewModel viewModel)
        //{

        //    return Request.CreateResponse(HttpStatusCode.OK, Envelope.Ok(result, viewModel.Message, viewModel.IsSuccess));
        //}

        //protected HttpResponseMessage Ok(IViewModel viewModel)
        //{
        //    return Request.CreateResponse(HttpStatusCode.OK, Envelope.Ok(viewModel, viewModel.Message, viewModel.IsSuccess));
        //}

    }
}
