﻿  
  
CREATE PROCEDURE [dbo].[SApp_GetLocation]  
 (  
  @includeDeleted bit = 0  
 )  
 AS  
/*================================================================================  
Procedure Name: [SApp_GetLocation]  
Author: Gopi  
Create date: 04/03/2019  
Description: Get list of Location from table   
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
04/03/2019 Gopi   Initial Version  
================================================================================*/  
  
BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
 SELECT [Location].[Id],  
  [Location].[LocationName],  
  [Location].[Description],  
  [Location].[IsDeleted]  
FROM [dbo].[Location]  
where ([Location].IsDeleted = 0)   
 END