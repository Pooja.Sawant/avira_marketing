﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InfographicsController : AviraBaseController
    {
        public readonly IInfographicsService _iInfographicsService;
        private ILoggerManager _logger;
        public InfographicsController(IInfographicsService iInfographicsService, ILoggerManager logger)
        {
            _iInfographicsService = iInfographicsService;
            _logger = logger;
        }

        [HttpPost]        
        public ActionResult Post([FromBody] InfographicsViewModel viewModel)
        {
            
            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while saving Infographics, please contact Admin for more details."
            };

            if (!ModelState.IsValid)
            {
                viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                return Ok(viewData);
            }

            viewData = _iInfographicsService.CreateUpdate(viewModel);
            
            return Ok(viewData);
        }

        [HttpGet]
        [Route("GetById")]
        public ActionResult<InfographicsViewModel> GetById(Guid Id)
        {
            InfographicsViewModel res = null;
            try
            {
                res = _iInfographicsService.GetById(Id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet]
        public ActionResult<IEnumerable<InfographicsViewModel>> Get(int PageStart = 0, int pageSize = 0, int SortDirection = 1, string OrdbyByColumnName = "CreatedDate", string Search = "", bool isDeletedInclude = false, Guid? projectId = null, Guid? lookupCategoryId = null)
        {
            List<InfographicsViewModel> res = null;
            try
            {
                res = _iInfographicsService.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, projectId, lookupCategoryId).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet]
        [Route("Delete")]
        public ActionResult Delete(Guid Id)
        {

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while deleting Infographics, please contact Admin for more details."
            };

            if (!ModelState.IsValid)
            {
                viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                return Ok(viewData);
            }

            viewData = _iInfographicsService.Delete(Id);

            return Ok(viewData);
        }
    }
}