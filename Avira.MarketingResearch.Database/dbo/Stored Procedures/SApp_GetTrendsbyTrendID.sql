﻿ CREATE PROCEDURE [dbo].[SApp_GetTrendsbyTrendID]          
 (    
 @TrendID uniqueidentifier = null,      
 @includeDeleted bit = 0,      
 @OrdbyByColumnName NVARCHAR(50) ='TrendName',      
 @SortDirection INT = -1,      
 @PageStart INT = 0,    
 @TrendName NVARCHAR(512)='',      
 @PageSize INT = null)          
 AS          
/*================================================================================          
Procedure Name: SApp_GetTrendsbyTrendID     
Author: Harshal          
Create date: 04/12/2019          
Description: Get list from Trends table by TrendID        
Change History          
Date  Developer  Reason          
__________ ____________ ______________________________________________________          
04/12/2019 Harshal   Initial Version          
================================================================================*/         
          
BEGIN          
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED       
if @PageSize is null or @PageSize =0      
set @PageSize = 10      
       
begin      
;WITH CTE_TBL AS (      
 SELECT       
  [Trend].[Id] ,          
  [Trend].[TrendName],         
  [Trend].IsDeleted,    
  [Trend].ProjectID,    
  [Trend].TrendStatusID,    
  [Trend].AuthorRemark,    
  [Trend].ApproverRemark,       
  [Project].ProjectName, 
  [TrendStatus].TrendStatusName,   
  [Project].ProjectCode,    
 CASE      
  WHEN @OrdbyByColumnName = 'TrendName' THEN [Trend].TrendName    
  WHEN @OrdbyByColumnName = 'Description' THEN [Trend].[Description]      
  ELSE [Trend].TrendName       
 END AS SortColumn      
      
FROM [dbo].[Trend]     
INNER JOIN [dbo].[Project] ON [Project].ID=[Trend].ProjectID
INNER JOIN [dbo].[TrendStatus] ON [TrendStatus].Id=[Trend].TrendStatusID      
where [Trend].Id=@TrendID     
      AND (@includeDeleted = 1 or [Trend].IsDeleted = 0)      
)      
      
 SELECT       
    CTE_TBL.*,      
    tCountResult.TotalItems      
  FROM CTE_TBL      
  CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult      
  ORDER BY       
    CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,      
    CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC       
  OFFSET @PageStart ROWS      
  FETCH NEXT @PageSize ROWS ONLY;      
  END      
      
END