﻿
CREATE procedure [dbo].[SApp_GetESMSubSegmentByID]    
(  
@ProjectId uniqueidentifier,
@SegmentId uniqueidentifier)    
as    
/*================================================================================    
Procedure Name: SApp_GetESMSubSegmentByID   
Author: Harshal    
Create date: 12/05/2019    
Description: Get data from respective segment Table  
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
12/05/2019  Harshal   Initial Version    
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
   	  Select Subsegment.Id As SubsegId,
      SubSegmentName As SegName,
      Subsegment.Description As SegDespcription
      FROM ESMSegmentMapping
	  INNER JOIN SubSegment ON ESMSegmentMapping.SubSegmentId=SubSegment.Id
      WHERE Subsegment.isDeleted=0
      AND ESMSegmentMapping.SegmentId=@SegmentId
      and ESMSegmentMapping.ProjectId = @ProjectId
      order by  SegName
END