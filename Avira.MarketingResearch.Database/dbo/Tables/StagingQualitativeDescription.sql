﻿CREATE TABLE [dbo].[StagingQualitativeDescription] (
    [Id]                           UNIQUEIDENTIFIER NOT NULL,
    [ImportId]                     UNIQUEIDENTIFIER NULL,
    [RowNo]                        INT              NULL,
    [MarketName]                   NVARCHAR (255)   NULL,
    [SubMarketName]                NVARCHAR (255)   NULL,
    [DescriptionCategory]          NVARCHAR (255)   NULL,
    [DescriptionSubCategory]       NVARCHAR (255)   NULL,
    [DescriptionText]              NVARCHAR (4000)  NULL,
    [MarketId]                     UNIQUEIDENTIFIER NULL,
    [SubMarketId]                  UNIQUEIDENTIFIER NULL,
    [ProjectId]                    UNIQUEIDENTIFIER NULL,
    [QualitativeAnalysisTypeId]    UNIQUEIDENTIFIER NULL,
    [QualitativeAnalysisSubTypeId] UNIQUEIDENTIFIER NULL,
    [ErrorNotes]                   NVARCHAR (1000)  NULL,
    CONSTRAINT [PK_StagingQualitativeDescription] PRIMARY KEY CLUSTERED ([Id] ASC)
);



