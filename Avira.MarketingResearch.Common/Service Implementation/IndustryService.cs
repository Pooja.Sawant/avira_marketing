﻿using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation
{
    public class IndustryService : IIndustryService
    {
        private readonly IIndustryRepository _iIndustryRepository;

        public IndustryService(IIndustryRepository iIndustryRepository)
        {
            _iIndustryRepository = iIndustryRepository;
        }

        public Result<SpTransactionMessage> Create(IndustryInsertDbViewModel tfc, AviraUser currentUser)
        {
            return _iIndustryRepository.Create(tfc);
        }


        public IEnumerable<IndustryViewModel> GetAll(Guid Id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IndustryViewModel> GetAllParentIndustry()
        {
            return _iIndustryRepository.GetAllParentIndustry();
        }

        public IEnumerable<IndustryViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false)
        {
            return _iIndustryRepository.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName,Search, isDeletedInclude);
        }

        public IndustryViewModel GetById(Guid guid)
        {
            return _iIndustryRepository.GetById(guid);
        }

        public Result<SpTransactionMessage> Update(IndustryUpdateDbViewModel tfc, AviraUser currenUser)
        {
            return _iIndustryRepository.Update(tfc);
        }
    }
}
