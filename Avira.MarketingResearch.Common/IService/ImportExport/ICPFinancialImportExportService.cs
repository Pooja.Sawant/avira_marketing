﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.ImportExport
{
    public interface ICPFinancialImportExportService
    {
        List<ImportCommonResponseModel> Create(CPFinancialImportInsertModel viewModel, AviraUser currentUser);
    }
}
