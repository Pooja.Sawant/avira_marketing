﻿using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.CompanyProfile
{
    public class CompanyOtherInfoService: ICompanyOtherInfoService
    {
        private readonly ICompanyOtherInfoRepository _companyOtherInfoRepository;
        private readonly ICompanyNoteRepository _iCompanyNoteRepository;

        public CompanyOtherInfoService(ICompanyOtherInfoRepository companyOtherInfoRepository, ICompanyNoteRepository iCompanyNoteRepository)
        {
            _companyOtherInfoRepository = companyOtherInfoRepository;
            _iCompanyNoteRepository = iCompanyNoteRepository;
        }
       
        public IEnumerable<CompanyOtherInfoInsertUpdateViewModel> GetAll(Guid? id)
        {
            return _companyOtherInfoRepository.GetAll(id);
        }

        public Result<SpTransactionMessage> InsertUpdate(MainCompanyOtherInfoInsertUpdateViewModel tfc, AviraUser currenUser)
        {
            tfc.companyOtherInfoInsertUpdateViewModel.ForEach(i => i.Id = (i.Id == Guid.Empty ? Guid.NewGuid() : i.Id));
            tfc.companyOtherInfoInsertUpdateViewModel.ForEach(i => i.UserCreatedById = currenUser.Id);   
            tfc.jsonOtherInfo = JsonConvert.SerializeObject(tfc.companyOtherInfoInsertUpdateViewModel);
            var res = _companyOtherInfoRepository.InsertUpdate(tfc);

            tfc.CompanyNoteViewModel.Id = Guid.NewGuid();
            tfc.CompanyNoteViewModel.UserCreatedById = currenUser.Id;
            tfc.CompanyNoteViewModel.CompanyId = tfc.CompanyId;
            _iCompanyNoteRepository.Insert(tfc.CompanyNoteViewModel);

            return res;
        }
    }
}
