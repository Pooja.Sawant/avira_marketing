﻿CREATE procedure [dbo].[SApp_DeleteRawMaterialbyID]
(@RawMaterialID uniqueidentifier,
@AviraUserId uniqueidentifier)
as
/*================================================================================
Procedure Name: SApp_DeleteRawMaterialbyID
Author: Sharath
Create date: 02/21/2019
Description: soft delete a record from RawMaterial table by ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
02/21/2019	Sharath			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


UPDATE [dbo].[SubSegment]
   SET [IsDeleted] = 1,
   [DeletedOn] = getdate(),
   [UserDeletedById] = @AviraUserId
where ID = @RawMaterialID
and IsDeleted = 0 -- no need to delete record which is already deleted

end