﻿CREATE procedure [dbo].[SApp_insertUpdateCompany]
(@CompanyID uniqueidentifier,
@CompanyCode nvarchar(256),
@CompanyName nvarchar(256),
@Description nvarchar(256),
@Telephone nvarchar(20),
@Telephone2 nvarchar(20),
@Email nvarchar(100),
@Email2 nvarchar(100),
@Fax nvarchar(20),
@Fax2 nvarchar(20),
@Website nvarchar(128),
@ParentCompanyName nvarchar(256),
@ProfiledCompanyName nvarchar(256),
@NoOfEmployees int,
@YearOfEstb int,
@ParentCompanyYearOfEstb int,
@IsDeleted bit,
@AviraUserId uniqueidentifier)
as
/*================================================================================
Procedure Name: SApp_insertUpdateCompany
Author: Sharath
Create date: 02/11/2019
Description: soft delete a record from Company table by ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
02/11/2019	Sharath			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

IF exists(select 1 from [dbo].[Company] 
		 where ((@CompanyID is null or [Id] !=  @CompanyID)
		 and  CompanyCode= @CompanyCode AND IsDeleted = 0))
begin
declare @ErrorMsg NVARCHAR(2048);
set @ErrorMsg = 'Company with Code "'+ @CompanyCode + '" already exists.';
THROW 50000,@ErrorMsg,1;
end

IF (@CompanyID is null)
begin
--Insert new record
	set @CompanyID = NEWID();

	insert into [dbo].[Company]([Id],
								--[CompanyCode],
								[CompanyName],
								[Description],
								[Telephone],
								[Telephone2],
								[Email],
								[Email2],
								[Fax],
								[Fax2],
								[ParentCompanyName],
								[ProfiledCompanyName],
								[Website],
								[NoOfEmployees],
								[YearOfEstb],
								[ParentCompanyYearOfEstb],
								[IsDeleted],
								[CreatedOn],
								[UserCreatedById])
	values(@CompanyID, 
			--@CompanyCode, 
			@CompanyName, 
			@Description,
			@Telephone,
			@Telephone2,
			@Email,
			@Email2,
			@Fax,
			@Fax2,
			@ParentCompanyName,
			@ProfiledCompanyName,
			@Website,
			@NoOfEmployees,
			@YearOfEstb,
			@ParentCompanyYearOfEstb,
			0,
			getdate(),
			@AviraUserId);

end
else
begin
--update record
	update [dbo].[Company]
	set [CompanyCode] = @CompanyCode, 
		[CompanyName] = @CompanyName, 
		[Description] = @Description,
		[Telephone] = @Telephone,
		[Telephone2] = @Telephone2,
		[Email] = @Email,
		[Email2] = @Email2,
		[Fax] = @Fax,
		[Fax2] = @Fax2,
		[Website] = @Website,
		[ParentCompanyName] = @ParentCompanyName,
		[ProfiledCompanyName] = @ProfiledCompanyName,
		[NoOfEmployees] = @NoOfEmployees,
		[YearOfEstb] = @YearOfEstb,
		[ParentCompanyYearOfEstb] = @ParentCompanyYearOfEstb,
		[ModifiedOn] = getdate(),
		DeletedOn = case when IsDeleted = 0 and @IsDeleted = 1 then getdate() else DeletedOn end,
		UserDeletedById = case when IsDeleted = 0 and @IsDeleted = 1 then @AviraUserId else UserDeletedById end,
		IsDeleted = isnull(@IsDeleted, IsDeleted),
		[UserModifiedById] = @AviraUserId
     where ID = @CompanyID



end

select @CompanyID;
end