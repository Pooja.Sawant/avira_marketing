﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.ProjectDashBoard;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Repository.CoreInterfaces.ProjectDashBoard;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : AviraBaseController
    {
        private ILoggerManager _logger;
        public readonly IProjectRepository _projectRepository;
        public readonly IProjectService _projectService;
        public ProjectController(
            IProjectService projectService,
            IProjectRepository projectRepository,
            ILoggerManager logger)
        {
            _projectRepository = projectRepository;
            _projectService = projectService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ProjectViewModel>> Get(Guid? Id, Guid AviraUserId, int PageStart = 0, int pageSize = 0, int SortDirection = 1, string OrdbyByColumnName = "CreatedDate", string Search = "", bool isDeletedInclude = false)
        {
            List<ProjectViewModel> res = null;
            try
            {
                res = _projectService.GetAll(Id, AviraUserId, PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }


        [HttpGet]
        [Route("GetAllProject")]
        public ActionResult<IEnumerable<ProjectViewModel>> GetAllProject()
        {
            List<ProjectViewModel> res = null;
            try
            {
                res = _projectService.GetAllProject().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        //[HttpGet]
        //[Route("GetGetProjectDetailsIdURL")]
        //public ActionResult<DisplayProjectDBViewModel> GetProjectDetailsById(Guid id, Guid AviraUserId)
        //{
        //    DisplayProjectDBViewModel project = null;
        //    try
        //    {
        //        project = _projectService.GetById(id, AviraUserId);
        //        if (project == null)
        //        {
        //            return NotFound();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError(ex.Message);
        //    }
        //    return project;
        //}

        [HttpPost]
        public ActionResult Post([FromBody] MainProjectInsertDBViewModel viewModel)
        {
           
            viewModel.projectInsertDbViewModel.EndDate = DateTime.MaxValue;
            viewModel.projectInsertDbViewModel.AuthorUserID = CurrentUser.Id;
            var projectid= Guid.NewGuid();
            viewModel.projectInsertDbViewModel.ProjectID = projectid;           
            var usercreatedby= CurrentUser.Id;
            var projectidtbl= projectid;
            
            viewModel.projectAnalistDetailsInsertTrendDbViewModel.UserCreatedById = usercreatedby;
            viewModel.projectAnalistDetailsInsertTrendDbViewModel.ProjectID = projectid;          
            viewModel.projectAnalistDetailsInsertCompnyDbViewModel.UserCreatedById = usercreatedby;
            viewModel.projectAnalistDetailsInsertCompnyDbViewModel.ProjectID = projectid;            
            viewModel.projectAnalistDetailsInsertMarketDbViewModel.UserCreatedById = usercreatedby;
            viewModel.projectAnalistDetailsInsertMarketDbViewModel.ProjectID = projectid;            
            viewModel.projectAnalistDetailsInsertQuantDbViewModel.UserCreatedById = usercreatedby;
            viewModel.projectAnalistDetailsInsertQuantDbViewModel.ProjectID = projectid;            
            viewModel.projectAnalistDetailsInsertPrimarDbViewModel.UserCreatedById = usercreatedby;
            viewModel.projectAnalistDetailsInsertPrimarDbViewModel.ProjectID = projectid;            
            viewModel.projectAnalistDetailsInsertApproverDbViewModel.UserCreatedById = usercreatedby;
            viewModel.projectAnalistDetailsInsertApproverDbViewModel.ProjectID = projectid;            
            viewModel.projectAnalistDetailsInsertCoApproDbViewModel.UserCreatedById = usercreatedby;
            viewModel.projectAnalistDetailsInsertCoApproDbViewModel.ProjectID = projectid;            
            viewModel.projectSegmentMappingInsertInduDbViewModel.UserCreatedById = usercreatedby;
            viewModel.projectSegmentMappingInsertInduDbViewModel.ProjectID = projectid;            
            viewModel.projectSegmentMappingInsertSubInduDbViewModel.UserCreatedById = usercreatedby;
            viewModel.projectSegmentMappingInsertSubInduDbViewModel.ProjectID = projectid;            
            viewModel.projectSegmentMappingInsertMarketDbViewModel.UserCreatedById = usercreatedby;
            viewModel.projectSegmentMappingInsertMarketDbViewModel.ProjectID = projectid;           
            viewModel.projectSegmentMappingInsertSubMarkDbViewModel.UserCreatedById = usercreatedby;
            viewModel.projectSegmentMappingInsertSubMarkDbViewModel.ProjectID = projectid;
          
            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new Project, please contact Admin for more details."
            };

            try
            {

                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                var result = _projectService.Create(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Project " + viewModel.projectInsertDbViewModel.ProjectName + " is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }

            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);

        }


        [HttpPut]
        public ActionResult Put([FromBody] MainProjectUpdateDBViewModel viewModel)
        {
            
                viewModel.projectAnalistDetailsUpdateApproverDbViewModel.UserModifiedById = CurrentUser.Id;
                viewModel.projectAnalistDetailsUpdateCoApproverDbViewModel.UserModifiedById = CurrentUser.Id;
                viewModel.projectAnalistDetailsUpdateCompanyDbViewModel.UserModifiedById = CurrentUser.Id;
                viewModel.projectAnalistDetailsUpdateMarketDbViewModel.UserModifiedById = CurrentUser.Id;
                viewModel.projectAnalistDetailsUpdatePrimiDbViewModel.UserModifiedById = CurrentUser.Id;
                viewModel.projectAnalistDetailsUpdateQuantDbViewModel.UserModifiedById = CurrentUser.Id;
                viewModel.projectAnalistDetailsUpdateTrendDbViewModel.UserModifiedById = CurrentUser.Id;
                viewModel.projectAnalistDetailsUpdateTrendDbViewModel.ProjectID = viewModel.projectUpdateDbViewModel.ProjectID;
                viewModel.projectAnalistDetailsUpdateQuantDbViewModel.ProjectID = viewModel.projectUpdateDbViewModel.ProjectID;
                viewModel.projectAnalistDetailsUpdatePrimiDbViewModel.ProjectID = viewModel.projectUpdateDbViewModel.ProjectID;
                viewModel.projectAnalistDetailsUpdateMarketDbViewModel.ProjectID = viewModel.projectUpdateDbViewModel.ProjectID;
                viewModel.projectAnalistDetailsUpdateCompanyDbViewModel.ProjectID = viewModel.projectUpdateDbViewModel.ProjectID;
                viewModel.projectAnalistDetailsUpdateCoApproverDbViewModel.ProjectID = viewModel.projectUpdateDbViewModel.ProjectID;
                viewModel.projectAnalistDetailsUpdateApproverDbViewModel.ProjectID = viewModel.projectUpdateDbViewModel.ProjectID;

                IViewModel viewData = new ViewData()
                {
                    IsSuccess = false,
                    Message = "Error while updating a new Project, please contact Admin for more details."
                };

                try
                {
                    if (!ModelState.IsValid)
                    {
                        viewData.Message = string.Join("; ", ModelState.Values
                            .SelectMany(x => x.Errors)
                            .Select(x => x.ErrorMessage));
                        return Ok(viewData);
                    }

                    Result<SpTransactionMessage> result = _projectService.Update(viewModel, CurrentUser);

                    if (result != null && result.IsSuccess)
                    {
                        viewData.IsSuccess = true;
                        viewData.Message = "Project is updated successfully.";

                    }
                    else
                    {
                        viewData.Message = result.Error;
                    }
                }

                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                }
                       

            return Ok(viewData);
        }

        [HttpGet]
        [Route("GetAllsegmentIndustryIdURL")]
        public ActionResult<IEnumerable<IndustryViewModel>> GetIndustrySegmentList(string id, bool includeParentOnly)
        {
            List<IndustryViewModel> res = null;
            try
            {
                res = _projectRepository.GetIndustrySegmentList(id, includeParentOnly).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }
        [HttpGet]
        [Route("GetAllsegmentMarketIdURL")]
        public ActionResult<IEnumerable<MarketViewModel>> GetMarketSegmentList(string id, bool includeParentOnly)
        {
            List<MarketViewModel> res = null;
            try
            {
                res= _projectRepository.GetMarketSegmentList(id, includeParentOnly).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet]
        [Route("UpdateStatusURL")]
        public ActionResult PutProjectStatus(string id)
        {
            IViewModel viewData = new ViewData();         
            var Projecid = new Guid(id);
            MainProjectUpdateDBViewModel viewModel = new MainProjectUpdateDBViewModel()
                {
                    projectUpdateDbViewModel = new ProjectUpdateDbViewModel(),
                    projectAnalistDetailsUpdateApproverDbViewModel = new ProjectAnalistDetailsUpdateDbViewModel()
    ,
                    projectAnalistDetailsUpdateCoApproverDbViewModel = new ProjectAnalistDetailsUpdateDbViewModel(),
                    projectAnalistDetailsUpdateTrendDbViewModel = new ProjectAnalistDetailsUpdateDbViewModel(),
                    projectAnalistDetailsUpdateQuantDbViewModel = new ProjectAnalistDetailsUpdateDbViewModel(),
                    projectAnalistDetailsUpdateCompanyDbViewModel = new ProjectAnalistDetailsUpdateDbViewModel()
    ,
                    projectAnalistDetailsUpdateMarketDbViewModel = new ProjectAnalistDetailsUpdateDbViewModel(),
                    projectAnalistDetailsUpdatePrimiDbViewModel = new ProjectAnalistDetailsUpdateDbViewModel()
                };
            viewModel.projectUpdateDbViewModel.ProjectID = Projecid;
            viewModel.projectUpdateDbViewModel.ProjectStatusChange = "OK";
            viewModel.projectUpdateDbViewModel.EndDate = DateTime.MaxValue;
            viewModel.projectUpdateDbViewModel.StartDate = DateTime.MaxValue;
            if (viewModel.projectUpdateDbViewModel.ProjectStatusChange == "OK")
            {
                viewData = new ViewData()
                {
                    IsSuccess = false,
                    Message = "Error while updating a new Project, please contact Admin for more details."
                };

                try
                {
                    if (!ModelState.IsValid)
                    {
                        viewData.Message = string.Join("; ", ModelState.Values
                            .SelectMany(x => x.Errors)
                            .Select(x => x.ErrorMessage));
                        return Ok(viewData);
                    }

                    Result<SpTransactionMessage> result = _projectService.UpdateProjectStatus(viewModel, CurrentUser);

                    if (result != null && result.IsSuccess)
                    {
                        viewData.IsSuccess = true;
                        viewData.Message = "Project Status is updated successfully.";

                    }
                    else
                    {
                        viewData.Message = result.Error;
                    }
                }

                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                }
            }
          
            return Ok(viewData);
        }
    }
}