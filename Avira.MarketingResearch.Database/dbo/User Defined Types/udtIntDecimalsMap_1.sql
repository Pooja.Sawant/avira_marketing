﻿CREATE TYPE [dbo].[udtIntDecimalsMap] AS TABLE (
    [Id]         UNIQUEIDENTIFIER NULL,
    [IntValue]   INT              NOT NULL,
    [DecimalOne] DECIMAL (19, 4)  NOT NULL,
    [DecimalTwo] DECIMAL (19, 4)  NOT NULL);

