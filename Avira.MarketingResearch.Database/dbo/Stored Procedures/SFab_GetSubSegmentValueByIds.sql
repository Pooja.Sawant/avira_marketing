﻿
  
CREATE PROCEDURE [dbo].[SFab_GetSubSegmentValueByIds]
(
 @SegmentId uniqueidentifier=null,
 @SubSegmentId uniqueidentifier=null,
 @ProjectId uniqueidentifier=null,
 @AviraUserID uniqueidentifier = null 
)  
AS  
/*================================================================================  
Procedure Name: [SFab_GetSubSegmentValue]  
Author: Swami Aluri  
Create date: 22/07/2019  
Description: Get list of Sub Segment Value from table   
Change History  
Date        Developer     Reason  
__________  ____________  ______________________________________________________  
22/07/2019   Swami Aluri   Initial Version  
01/11/2019   Praveen       Consolidate all segment  
================================================================================*/  

BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	Declare @SegmentType NVARCHAR(100);

	SELECT @SegmentType = SegmentName FROM Segment
	WHERE Segment.Id = @SegmentId

	CREATE TABLE #SubSegmentList 
	(
		SubSegmentId uniqueidentifier null,
		SegmentName NVARCHAR(100) null
	);


	INSERT INTO #SubSegmentList 
	SELECT SubSegment.Id,SegmentName FROM SubSegment
	INNER JOIN Segment ON Segment.Id = SubSegment.SegmentId
	WHERE Segment.Id = @SegmentId And 
		  (@SubSegmentId is Null Or SubSegment.Id = @SubSegmentId)
	order by SegmentName asc

	;with CTE_Years as ( 
	select YearColList= [Year]   
	FROM (SELECT [Year] FROM MarketValue WHERE ProjectId = @ProjectId
	GROUP BY [Year]) AS Y )

    --SELECT SUM(Value) as Value, mv.Year, rm.RawMaterialName as Name, rm.Id, @SegmentType As 'SegmentName'
	SELECT SUM([dbo].[fun_UnitDeConversion](MV.ValueConversionId, MV.Value)) as Value, mv.Year, SubSegment.SubSegmentName as Name, SubSegment.Id, @SegmentType As 'SegmentName'
	from MarketValue mv
	inner JOIN MarketValueSegmentMapping on MarketValueSegmentMapping.MarketValueId = mv.Id
	INNER JOIN SubSegment ON SubSegment.Id = MarketValueSegmentMapping.SubSegmentId
	INNER JOIN Segment ON Segment.Id = SubSegment.SegmentId
    INNER JOIN CTE_Years on mv.Year = CTE_Years.YearColList    
    
	WHERE Segment.Id = @SegmentId And 
		  (@SubSegmentId is Null Or SubSegment.Id = @SubSegmentId)
    AND mv.ProjectId = @ProjectId
    GROUP BY YEAR, SubSegment.SubSegmentName, SubSegment.Id



END