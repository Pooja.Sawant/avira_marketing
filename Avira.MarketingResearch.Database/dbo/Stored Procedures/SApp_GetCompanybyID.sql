﻿CREATE procedure [dbo].[SApp_GetCompanybyID]
(@CompanyID uniqueidentifier=null,
@includeDeleted bit = 0)
as
/*================================================================================
Procedure Name: SApp_GetCompanybyID
Author: Sharath
Create date: 02/11/2019
Description: Get list from Company table by Name or ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
02/11/2019	Sharath			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if @includeDeleted is null set @includeDeleted = 0

SELECT [Id],
		[CompanyCode],
		[CompanyName],
		[Description],
		[Telephone],
		[Telephone2],
		[Email],
		[Email2],
		[Fax],
		[Fax2],
		[Website],
		[ParentCompanyName],
		[ProfiledCompanyName],
		[NoOfEmployees],
		[YearOfEstb],
		[ParentCompanyYearOfEstb]
FROM [dbo].[Company]
where (@CompanyID is null or ID = @CompanyID)
and (@includeDeleted = 1 or IsDeleted = 0)



end