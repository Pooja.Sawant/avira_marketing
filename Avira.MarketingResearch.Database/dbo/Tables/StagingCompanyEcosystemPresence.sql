﻿CREATE TABLE [dbo].[StagingCompanyEcosystemPresence] (
    [Id]             UNIQUEIDENTIFIER NOT NULL,
    [ImportId]       UNIQUEIDENTIFIER NULL,
    [ESMPresence]    NVARCHAR (255)   NULL,
    [SectorPresence] NVARCHAR (512)   NULL,
    [KeyCompetitor]  NVARCHAR (512)   NULL,
    [CompanyId]      UNIQUEIDENTIFIER NULL,
    [ErrorNotes]     NVARCHAR (1000)  NULL,
    [RowNo]          INT              NULL,
    CONSTRAINT [PK_StagingCompanyEcosystemPresence] PRIMARY KEY CLUSTERED ([Id] ASC)
);



