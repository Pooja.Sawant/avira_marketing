﻿CREATE procedure [dbo].[SApp_UpdateStagingQualitativeAnalysis]
@AviraUserId uniqueidentifier =null
as        
/*================================================================================        
Procedure Name: SApp_UpdateStagingQualitativeAnalysis       
Author: Sharath        
Create date: 08/22/2019        
Description: to update Company Profiles 
Change History        
Date  Developer  Reason        
__________ ____________ ______________________________________________________        
08/22/2019 Sharath   Initial Version        
================================================================================*/        
BEGIN        
SET NOCOUNT ON;        
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;        
declare @ErrorMsg NVARCHAR(MAX)          
        
BEGIN TRY         
if (@AviraUserId is null)
BEGIN
declare @ImportId	uniqueidentifier;
set @ImportId = (select top 1 ImportId from [StagingQualitativeNews]);
set @AviraUserId = (select Id From AviraUser where UserName='VishwanathG' and UserTypeId=1 );
END

;with CTE_market as (select SubMarketName from [StagingQualitativeNews]  union
select SubMarketName from StagingQualitativeAnalysis union
select SubMarketName from StagingQualitativeDescription union
select SubMarketName from StagingQualitativeQuote )
insert into [dbo].[Market]([Id], [MarketName], [Description], [IsDeleted], [UserCreatedById], [CreatedOn])      
select NEWID(), SubMarketName, SubMarketName, 0, @AviraUserId, GETDATE()
from CTE_market
where not exists (select 1 from Market where Market.MarketName = CTE_market.SubMarketName)
and CTE_market.SubMarketName != '';

--Qualitative News verification
update staging
set ErrorNotes = isnull(ErrorNotes,'') 
+ case when  IsNull(staging.MarketName, '') = '' Then 'Project name is mandatory,' else '' end 
+ case when not exists (select 1 from Project where Project.ProjectName = staging.MarketName) then 'Invalid Project/Market,' else '' end
--+ case when not exists (select 1 from Market where Market.MarketName = staging.SubMarketName) and staging.SubMarketName != '' then 'Invalid Sub-MarketName, ' else '' end
+ case when len(staging.SubMarketName)>0 then
 case when Len(staging.SubMarketName) > 256 then 'sub market name exceeded max length(256)' else '' end
 else '' end
+ case when  IsNull(staging.NewsCategory, '') = '' Then 'News category is mandatory,' else '' end
+ case when not exists (select 1 from NewsCategory where NewsCategory.NewsCategoryName = staging.NewsCategory) then 'Invalid News Category,' else '' end
+ case when not exists (select 1 from Importance where Importance.ImportanceName = staging.Importance) and staging.Importance !='' then 'Invalid Importance Name,' else '' end
+ case when not exists (select 1 from ImpactDirection where ImpactDirection.ImpactDirectionName = staging.Impact) and isnull(staging.Impact,'') != '' then 'Invalid Impact,' else '' end
+ case when  IsNull(staging.News, '') = '' Then 'News text is mandatory,' else '' end 
, ProjectId = (select top 1 id from Project where Project.ProjectName = staging.MarketName) 
, SubMarketId = (select top 1 id from Market where Market.MarketName = staging.SubMarketName)
, ImportanceId = (select top 1 id from Importance where Importance.ImportanceName = staging.Importance) 
, ImpactId = (select top 1 id from ImpactDirection where ImpactDirection.ImpactDirectionName = staging.Impact)
, CategoryId = (select top 1 id from NewsCategory where NewsCategory.NewsCategoryName = staging.NewsCategory)
 from [StagingQualitativeNews] staging

--Staging Qualitative Analysis
update staging
set ErrorNotes = isnull(ErrorNotes,'') 
+ case when  IsNull(staging.MarketName, '') = '' Then 'Market/project Name is Mandatory,' else '' end
+ case when not exists (select 1 from Project where Project.ProjectName = staging.MarketName) then 'Invalid Project/Market,' else '' end
--+ case when not exists (select 1 from Market where Market.MarketName = staging.SubMarketName) and staging.SubMarketName != '' then 'Invalid Sub-MarketName, ' else '' end
+ case when len(staging.SubMarketName)>0 then
 case when Len(staging.SubMarketName) > 256 then 'sub market name exceeded max length(256)' else '' end
 else '' end
+ case when not exists (select 1 from QualitativeAnalysisType where QualitativeAnalysisType.QualitativeAnalysisTypeName = staging.AnalysisCategory) then 'Invalid Analysis Category,' else '' end
+ case when not exists (select 1 from QualitativeAnalysisSubType where QualitativeAnalysisSubType.[QualitativeAnalysisSubTypeName] = staging.Analysissubcategory) 
						and staging.Analysissubcategory!= '' then 'Invalid Analysis Sub-Category, ' else '' end
+ case when  IsNull(staging.AnalysisText, '') = '' Then 'Analysis Text is Mandatory,' else '' end
+ Case When Len(staging.AnalysisText) > 4000 Then 'Analysis Text exceed max length(4000),' Else '' End 
+ case when not exists (select 1 from Importance where Importance.ImportanceName = staging.Importance) and staging.Importance !='' then 'Invalid Importance Name,' else '' end
+ case when not exists (select 1 from ImpactDirection where ImpactDirection.ImpactDirectionName = staging.Impact) and isnull(staging.Impact,'') != '' then 'Invalid Impact,' else '' end
, ProjectId = (select top 1 id from Project where Project.ProjectName = staging.MarketName) 
, SubMarketId = (select top 1 id from Market where Market.MarketName = staging.SubMarketName)
, ImportanceId = (select top 1 id from Importance where Importance.ImportanceName = staging.Importance) 
, ImpactId = (select top 1 id from ImpactDirection where ImpactDirection.ImpactDirectionName = staging.Impact)
, QualitativeAnalysisTypeId = (select top 1 id from QualitativeAnalysisType where QualitativeAnalysisType.QualitativeAnalysisTypeName = staging.AnalysisCategory)
, QualitativeAnalysisSubTypeId = (select top 1 id from QualitativeAnalysisSubType where QualitativeAnalysisSubType.[QualitativeAnalysisSubTypeName] = staging.Analysissubcategory) 

from StagingQualitativeAnalysis staging

--Staging Qualitative Description
update staging
set ErrorNotes = isnull(ErrorNotes,'') 
+ case when  IsNull(staging.MarketName, '') = '' Then 'Market/Project Name is mandatory,' else '' end
+ case when not exists (select 1 from Project where Project.ProjectName = staging.MarketName) then 'Invalid Project/Market,' else '' end
--+ case when not exists (select 1 from Market where Market.MarketName = staging.SubMarketName) and staging.SubMarketName != '' then 'Invalid Sub-MarketName, ' else '' end
+ case when len(staging.SubMarketName)>0 then
 case when Len(staging.SubMarketName) > 256 then 'sub market name exceeded max length(256)' else '' end
 else '' end
+ case when not exists (select 1 from QualitativeAnalysisType where QualitativeAnalysisType.QualitativeAnalysisTypeName = staging.DescriptionCategory) then 'Invalid Analysis Category,' else '' end
+ case when not exists (select 1 from QualitativeAnalysisSubType where QualitativeAnalysisSubType.[QualitativeAnalysisSubTypeName] = staging.DescriptionSubcategory) 
						and staging.DescriptionSubcategory!= '' then 'Invalid Analysis Sub-Category,' else '' end
+ case when  IsNull(staging.DescriptionText, '') = '' Then 'Description text is mandatory,' else '' end
+ Case When Len(staging.DescriptionText) > 4000 Then 'DescriptionText exceed max length(4000),' Else '' End
, ProjectId = (select top 1 id from Project where Project.ProjectName = staging.MarketName) 
, SubMarketId = (select top 1 id from Market where Market.MarketName = staging.SubMarketName)
, QualitativeAnalysisTypeId = (select top 1 id from QualitativeAnalysisType where QualitativeAnalysisType.QualitativeAnalysisTypeName = staging.DescriptionCategory)
, QualitativeAnalysisSubTypeId = (select top 1 id from QualitativeAnalysisSubType where QualitativeAnalysisSubType.[QualitativeAnalysisSubTypeName] = staging.DescriptionSubcategory) 
from StagingQualitativeDescription staging

--Staging Qualitative Quote
update staging
set ErrorNotes = isnull(ErrorNotes,'') 
+ case when  IsNull(staging.MarketName, '') = '' Then 'Market/Project Name is mandatory,' else '' end
+ case when not exists (select 1 from Project where Project.ProjectName = staging.MarketName) then 'Invalid Project/Market, ' else '' end
--+ case when not exists (select 1 from Market where Market.MarketName = staging.SubMarketName) and staging.SubMarketName != '' then 'Invalid Sub-MarketName, ' else '' end
--+ case when not exists (select 1 from Designation where Designation.Designation = staging.ResourceDesignation) then 'Invalid Resource Designation, ' else '' end
--+ case when not exists (select 1 from Company where Company.[CompanyName] = staging.ResourceCompany) then 'Invalid Company' else '' end
+ case when len(staging.SubMarketName)>0 then
 case when Len(staging.SubMarketName) > 256 then 'sub market name exceeded max length(256)' else '' end
 else '' end
 + case when  IsNull(staging.ResourceName, '') = '' Then 'Resource Name is mandatory,' else '' end
+ Case When Len(staging.ResourceName) > 255 Then 'resource name exceed max length(255),' Else '' End
+ case when  IsNull(staging.ResourceCompany, '') = '' Then 'Resource Company Name is mandatory,' else '' end
+ Case When Len(staging.ResourceCompany) > 255 Then 'resource company name exceed max length(255),' Else '' End
+ case when  IsNull(staging.Quote, '') = '' Then 'Quote Text is mandatory,' else '' end
+ Case When Len(staging.Quote) > 4000 Then 'quote text exceed max length(255)' Else '' End
+ case when len(staging.ResourceDesignation) > 0 then 
               case when len(staging.ResourceDesignation) > 256 then 'ResourceDesignation exceed max length(256)' else ''end
			   else '' end
+ case when len(staging.OtherRemarks) > 0 then 
               case when len(staging.OtherRemarks) > 256 then 'Other Remarks exceed max length(4000)' else ''end
			   else '' end
, ProjectId = (select top 1 id from Project where Project.ProjectName = staging.MarketName) 
, SubMarketId = (select top 1 id from Market where Market.MarketName = staging.SubMarketName)
, ResourceDesignationId = (select top 1 id from Designation where Designation.Designation = staging.ResourceDesignation)
, CompanyId = (select top 1 id from Company where Company.[CompanyName] = staging.ResourceCompany) 
from StagingQualitativeQuote staging

--StagingQualitativeSegmentMapping
update staging
set ErrorNotes = isnull(ErrorNotes,'') 
+ case when not exists (select 1 from Project where Project.ProjectName = staging.MarketName) then 'Invalid Project/Market,' else '' end
+ case when not exists (select 1 from Segment where Segment.SegmentName = staging.SegmentName) then 'Invalid Segment Name,' else '' end
+ case when (exists(select 1 from SubSegment WHERE SubSegment.SubSegmentName=staging.SubSegmentName) AND 
                       not exists(select 1 from StagingQualitativeSegmentMapping INNER JOIN segment ON StagingQualitativeSegmentMapping.SegmentName=segment.SegmentName
                       INNER JOIN SubSegment ON StagingQualitativeSegmentMapping.SubSegmentName=SubSegment.SubSegmentName 
					   AND SubSegment.SegmentId=Segment.Id WHERE StagingQualitativeSegmentMapping.SubSegmentName=staging.SubSegmentName)) 
					   Then 'Subsegment name not mapped with segment '+staging.SegmentName+',' else '' end
, ProjectId = (select top 1 id from Project where Project.ProjectName = staging.MarketName) 
, SegmentID = (select top 1 id from Segment where Segment.SegmentName = staging.SegmentName)
from StagingQualitativeSegmentMapping staging


;WITH subSegemntCTE as (

select  distinct
		SegmentID,
		staging.SubSegmentName
from StagingQualitativeSegmentMapping staging
where not exists (select 1 from [SubSegment] 
			 where [SubSegment].SubSegmentName= staging.SubSegmentName 
			 and [SubSegment].IsDeleted = 0 
			 and [SubSegment].SegmentId = staging.SegmentId)
)

INSERT INTO [dbo].[SubSegment]
           ([Id]
		   , SegmentId
           ,[SubSegmentName]
           ,[Description]
           ,[CreatedOn]
           ,[IsDeleted])

select NEWID(),
		SegmentID,
		SubSegmentName,
		SubSegmentName,
		GETDATE(),
		0
FROM subSegemntCTE where SegmentID is not null


update staging
set SubSegmentId = (select top 1 SubSegment.Id 
		from SubSegment 
		inner join segment 
		on subsegment.segmentid = segment.id 
		where staging.SubSegmentName = SubSegment.SubSegmentName 
		and SubSegment.IsDeleted = 0 
		and staging.SegmentName = Segment.SegmentName 
		and segment.IsDeleted = 0)
from StagingQualitativeSegmentMapping staging


--StagingQualitativeRegionMapping
update staging
set ErrorNotes = isnull(ErrorNotes,'') 
+ case when not exists (select 1 from Project where Project.ProjectName = staging.MarketName) then 'Invalid Project/Market,' else '' end
+ case when not exists (select 1 from Region where Region.RegionName = staging.RegionName) then 'Invalid Region Name,' else '' end
, ProjectId = (select top 1 id from Project where Project.ProjectName = staging.MarketName) 
, RegionId = (select top 1 id from Region where Region.RegionName = staging.RegionName) 
--, CountryId = (select top 1 id from Country where Country.CountryName = staging.CountryName) 
from StagingQualitativeRegionMapping staging

;with CTE_Val as (select SQRM.*, t.Value as Country from StagingQualitativeRegionMapping SQRM
cross apply dbo.FUN_STRING_TOKENIZER(SQRM.CountryName, ',') T)
update staging
set ErrorNotes = isnull(staging.ErrorNotes,'') 
+ case when not exists (select 1 from Country where Country.CountryName = CTE_Val.Country) 
						and CTE_Val.Country!= '' and CTE_Val.Country not like 'Rest Of%'
						then 'Invalid Country Name, ' else '' end
from StagingQualitativeRegionMapping staging
inner join CTE_Val
on CTE_Val.id = staging.Id

--- Staging ESM Segment Mapping
update staging
set ErrorNotes = isnull(ErrorNotes,'')
+ case when  IsNull(staging.MarketName, '') = '' Then 'Project name is mandatory,' else '' end 
+ case when not exists (select 1 from Project where Project.ProjectName = staging.MarketName) then 'Invalid Project/Market,' else '' end
+ case when not exists (select 1 from Segment where Segment.SegmentName = staging.SegmentName) then 'Invalid Segment Name,' else '' end
+ case when (SELECT Count(StagingESMSegmentMapping.SegmentName) FROM StagingESMSegmentMapping where StagingESMSegmentMapping.MarketName !='' ) < 0 then 'Minimum 1 Segment data is required,'else'' end
+ case when len(staging.SubSegmentName) > 0 then
case when len(staging.SubSegmentName) > 255  then 'Max limit for Subsegment is 255 character' else '' end
else '' end
+ case when (SELECT Count(distinct(StagingESMSegmentMapping.SegmentName)) FROM StagingESMSegmentMapping 
			where StagingESMSegmentMapping.MarketName !='')> 10
			--AND StagingESMSegmentMapping.SegmentName=staging.SegmentName  
	then 'Maximum 10 segemnt data allowed for single project,' else'' end
+ case when exists(select 1 from StagingESMSegmentMapping 
					where SubSegmentName = staging.SubSegmentName group by StagingESMSegmentMapping.SubSegmentName having count(*) >1) 
					then 'Duplicate subsegment found,' else '' end
--+ case when exists (SELECT 1 FROM StagingESMSegmentMapping group by StagingESMSegmentMapping.SegmentName having Count(StagingESMSegmentMapping.SubSegmentName) >15) then 'Maximum 15 Subsegemnt data allowed for single segment,' else'' end
+ case when (exists(select 1 from SubSegment WHERE SubSegment.SubSegmentName=staging.SubSegmentName) AND 
                       not exists(select 1 from StagingESMSegmentMapping INNER JOIN segment ON StagingESMSegmentMapping.SegmentName=segment.SegmentName
                       INNER JOIN SubSegment ON StagingESMSegmentMapping.SubSegmentName=SubSegment.SubSegmentName 
					   AND SubSegment.SegmentId=Segment.Id WHERE StagingESMSegmentMapping.SubSegmentName=staging.SubSegmentName)) 
					   Then 'Subsegment name not mapped with segment '+staging.SegmentName+',' else '' end
, ProjectId = (select top 1 id from Project where Project.ProjectName = staging.MarketName) 
,SegmentID = (select top 1 id from Segment where Segment.SegmentName = staging.SegmentName)
from StagingESMSegmentMapping staging

if(NOT EXISTS(SELECT 1 FROM StagingESMSegmentMapping WHERE isnull(ErrorNotes,'') != ''))
begin
;WITH EsmsubSegmentCTE as (

select  distinct
		SegmentID,
		staging.SubSegmentName
from StagingESMSegmentMapping staging
where not exists (select 1 from [SubSegment] 
			 where [SubSegment].SubSegmentName= staging.SubSegmentName 
			 and [SubSegment].IsDeleted = 0 )
			 --and [SubSegment].SegmentId = staging.SegmentId)
)

INSERT INTO [dbo].[SubSegment]
           ([Id]
		   , SegmentId
           ,[SubSegmentName]
           ,[Description]
           ,[CreatedOn]
           ,[IsDeleted])

select NEWID(),
		SegmentID,
		SubSegmentName,
		SubSegmentName,
		GETDATE(),
		0
FROM EsmsubSegmentCTE where SegmentID is not null
End

update staging
set SubSegmentId = (select top 1 SubSegment.Id 
		from SubSegment 
		inner join segment 
		on subsegment.segmentid = segment.id 
		where staging.SubSegmentName = SubSegment.SubSegmentName 
		and SubSegment.IsDeleted = 0 
		--and staging.SegmentName = Segment.SegmentName 
		and segment.IsDeleted = 0)
from StagingESMSegmentMapping staging

-----Staging ESM Analysis
update staging
set ErrorNotes = isnull(ErrorNotes,'') 
+ case when  IsNull(staging.MarketName, '') = '' Then 'Project name is mandatory,' else '' end
+ case when not exists (select 1 from Project where Project.ProjectName = staging.MarketName) then 'Invalid Project/Market,' else '' end
+ case when  IsNull(staging.AnalysisCategory, '') = '' Then 'Analysis Category is mandatory,' else '' end
+ case when not exists (select 1 from StagingESMAnalysis where trim(staging.AnalysisCategory) = 'Analyst views') then 'Invalid Analysis Category,' else '' end
+ case when not exists (select 1 from Importance where Importance.ImportanceName = staging.Importance) and staging.Importance !='' then 'Invalid Importance Name,' else '' end
+ case when not exists (select 1 from ImpactDirection where ImpactDirection.ImpactDirectionName = staging.Impact) and isnull(staging.Impact,'') != '' then 'Invalid Impact,' else '' end
+ case when  IsNull(staging.SegmentName, '') = '' Then 'Segment name is mandatory,' else '' end
+ case when not exists (select 1 from StagingESMSegmentMapping where StagingESMSegmentMapping.SegmentName= staging.SegmentName AND StagingESMSegmentMapping.MarketName=staging.MarketName)then 'Segment not mapped in ESMSegment,' else '' end
+ case when not exists (select 1 from Segment where Segment.SegmentName = staging.SegmentName) then 'Invalid Segment Name,' else '' end
+ case when (Isnull(staging.SubSegmentName,'') <>'' AND  not exists (select 1 from StagingESMSegmentMapping where StagingESMSegmentMapping.SubSegmentName= staging.SubSegmentName AND StagingESMSegmentMapping.SegmentName= staging.SegmentName AND StagingESMSegmentMapping.MarketName=staging.MarketName))then 'Sub Segment not mapped in ESMSegment,' else '' end
--+ case when exists (select 1 from StagingESMAnalysis where Isnull(staging.SubSegmentName,'') <>'' and StagingESMAnalysis.SubSegmentName=staging.SubSegmentName  group by StagingESMAnalysis.MarketName,StagingESMAnalysis.SegmentName,StagingESMAnalysis.SubSegmentName having count(*)>1) then 'Duplicate records found for SubsegmentName' else '' end
+ case when  IsNull(staging.AnalysisText, '') = '' Then 'AnalysisText is mandatory,' else '' end
+ case when len(staging.AnalysisText) > 4000 then 'Max limit for AnalysisText is 4000 character,' else '' end
, ProjectId = (select top 1 id from Project where Project.ProjectName = staging.MarketName) 
, ImportanceId = (select top 1 id from Importance where Importance.ImportanceName = staging.Importance) 
, ImpactId = (select top 1 id from ImpactDirection where ImpactDirection.ImpactDirectionName = staging.Impact)
, QualitativeAnalysisTypeId = (select top 1 id from QualitativeAnalysisType where QualitativeAnalysisType.QualitativeAnalysisTypeName = staging.AnalysisCategory)
, SegmentId = (select top 1 id from Segment where Segment.SegmentName = staging.SegmentName)
,SubSegmentId=(select top 1 id from SubSegment where SubSegment.SubSegmentName = staging.SubSegmentName and SubSegment.SegmentId=SegmentId)
from StagingESMAnalysis staging

IF( EXISTS(SELECT 1 FROM StagingQualitativeNews WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingQualitativeAnalysis WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingQualitativeDescription WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingQualitativeQuote WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingQualitativeSegmentMapping WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingQualitativeRegionMapping WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingESMAnalysis WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingESMSegmentMapping WHERE ErrorNotes != '')
)    
BEGIN   
Select 'News' as Module,RowNo, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingQualitativeAnalysis' AS StoredProcedureName   FROM StagingQualitativeNews WHERE ErrorNotes != '' UNION ALL
Select 'Qualitative Analysis' as Module,RowNo, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingQualitativeAnalysis' AS StoredProcedureName fROM StagingQualitativeAnalysis WHERE ErrorNotes != '' UNION ALL
Select 'Qualitative Description' as Module,RowNo, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingQualitativeAnalysis' AS StoredProcedureName FROM StagingQualitativeDescription WHERE ErrorNotes != '' UNION ALL
Select 'Quotes' as Module,RowNo, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingQualitativeAnalysis' AS StoredProcedureName   FROM StagingQualitativeQuote WHERE ErrorNotes != '' UNION ALL
Select 'Segment Mapping' as Module,RowNo, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingQualitativeAnalysis' AS StoredProcedureName   FROM StagingQualitativeSegmentMapping WHERE ErrorNotes != '' UNION ALL
Select 'Region Mapping' as Module,RowNo, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingQualitativeAnalysis' AS StoredProcedureName   FROM StagingQualitativeRegionMapping WHERE ErrorNotes != '' UNION ALL
Select distinct 'Ecosystem Segment Mapping' as Module,RowNo, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingQualitativeAnalysis' AS StoredProcedureName   FROM StagingESMSegmentMapping WHERE ErrorNotes != '' UNION ALL
Select 'Analyst Views' as Module,RowNo, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingQualitativeAnalysis' AS StoredProcedureName   FROM StagingESMAnalysis WHERE ErrorNotes != '' order by 1,2

DELETE FROM ImportData where Id in (select ImportId from StagingQualitativeAnalysis sm);    
DELETE FROM StagingQualitativeNews;
DELETE FROM StagingQualitativeAnalysis;
DELETE FROM StagingQualitativeDescription;
DELETE FROM StagingQualitativeQuote;
DELETE FROM StagingQualitativeSegmentMapping;
DELETE FROM StagingQualitativeRegionMapping;
DELETE FROM StagingESMSegmentMapping;
DELETE FROM StagingESMAnalysis;



END  
ELSE  
Begin
Exec [dbo].[SApp_insertQualitativeAnalysisFromStagging];
DELETE FROM StagingQualitativeNews;
DELETE FROM StagingQualitativeAnalysis;
DELETE FROM StagingQualitativeDescription;
DELETE FROM StagingQualitativeQuote;
DELETE FROM StagingQualitativeSegmentMapping;
DELETE FROM StagingQualitativeRegionMapping;
DELETE FROM StagingESMSegmentMapping;
DELETE FROM StagingESMAnalysis;

End 

END TRY              
BEGIN CATCH              
    -- Execute the error retrieval routine.              
 DECLARE @ErrorNumber int;              
 DECLARE @ErrorSeverity int;              
 DECLARE @ErrorProcedure varchar(100);              
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);              
              
  SELECT @ErrorNumber = ERROR_NUMBER(),              
        @ErrorSeverity = ERROR_SEVERITY(),              
        @ErrorProcedure = ERROR_PROCEDURE(),              
        @ErrorLine = ERROR_LINE(),              
        @ErrorMessage = ERROR_MESSAGE()              
              
 insert into dbo.Errorlog(ErrorNumber,              
       ErrorSeverity,              
       ErrorProcedure,              
       ErrorLine,              
       ErrorMessage,              
       ErrorDate)              
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());              
            
 set @ErrorMsg = 'Error while updating StagingESM tables, please contact Admin for more details.';            
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine            
,Message =@ErrorMsg,Success = 0;               
               
END CATCH              
        
        
end