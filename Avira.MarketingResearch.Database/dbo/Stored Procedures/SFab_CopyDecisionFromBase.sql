﻿

CREATE PROCEDURE [dbo].[SFab_CopyDecisionFromBase]
(		
	@InDecisionId uniqueidentifier,
	@UserId uniqueidentifier, --'1570416B-443A-4833-AA8F-941E159DD90F'
	@CopyDecisionParametersFromBase bit = 0,
	@OutDecisionId uniqueidentifier Out
)
AS

--Declare @InDecisionId uniqueidentifier = '4fbb31aa-0365-4d3b-8540-112e500339f3', @UserId uniqueidentifier = '1570416B-443A-4833-AA8F-941E159DD90F',
--	@OutDecisionId uniqueidentifier, @CopyDecisionParametersFromBase bit = 0

/*================================================================================
Procedure Name: [dbo].[SFab_CopyDecisionFromBase]
Author: Nitin
Create date: 14/01/2020
Description: Copy all default decision parameters to Decision table
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
14/01/2020	Nitin			Initial Version
================================================================================*/

BEGIN

Declare @ErrorMsg NVARCHAR(2048)   

	BEGIN TRY
		Declare @StatusId uniqueidentifier

		Select @StatusId = Id From [LookupStatus] 
		Where [StatusType] = 'DMDecisionStatus' And StatusName = 'To be initiated'

		If Exists(Select Id From Decision Where BaseDecisionId = @InDecisionId and UserId = @UserId)
		Begin

			Select @OutDecisionId = Id 
			From Decision 
			Where BaseDecisionId = @InDecisionId 
				And UserId = @UserId
		End
		Else
		Begin

			If Not Exists(Select Id From Decision Where Id = @InDecisionId and UserId = @UserId)
			Begin

				INSERT INTO [dbo].[Decision] ([Description],[ResourceAssigned],[UserId],
											  [BaseDecisionId],[StatusId], [Score],[DecisionTypeId],[UserCreatedById])
				Select B.Description, '', @UserId, @InDecisionId, @StatusId, [Score],[DecisionTypeId],@UserId
				From BaseDecision B Where B.Id = @InDecisionId
		
				Select @OutDecisionId = Id From Decision Where BaseDecisionId = @InDecisionId And UserId = @UserId

				Delete From DecisionFunction Where DecisionId = @OutDecisionId

				Insert Into DecisionFunction (DecisionId, BusinessFunctionId)
				Select @OutDecisionId, BusinessFunctionId
				From BaseDecisionFunction
				Where BaseDecisionId = @InDecisionId

				Delete From DecisionBusinessRole Where DecisionId = @OutDecisionId

				Insert Into DecisionBusinessRole (DecisionId, BusinessRoleId)
				Select @OutDecisionId, BusinessRoleId
				From BaseDecisionBusinessRole
				Where BaseDecisionId = @InDecisionId

				Delete From DecisionBusinessStage Where DecisionId = @OutDecisionId

				Insert Into DecisionBusinessStage (DecisionId, BusinessStageId)
				Select @OutDecisionId, BusinessStageId
				From BaseDecisionBusinessStage
				Where BaseDecisionId = @InDecisionId
			End
			Execute SFab_CopyDefaultDecisionParameters @DecisionId = @OutDecisionId, @UserId = @UserId		
		End
	END TRY
	BEGIN CATCH 
		DECLARE @ErrorNumber int;      
		DECLARE @ErrorSeverity int;      
		DECLARE @ErrorProcedure varchar(100);       
		DECLARE @ErrorLine int;      
		DECLARE @ErrorMessage varchar(500);   
      
		SELECT @ErrorNumber = ERROR_NUMBER(),      
			@ErrorSeverity = ERROR_SEVERITY(),      
			@ErrorProcedure = ERROR_PROCEDURE(),      
			@ErrorLine = ERROR_LINE(),      
			@ErrorMessage = ERROR_MESSAGE()      
      
		Insert Into dbo.Errorlog(ErrorNumber,      
			ErrorSeverity,      
			ErrorProcedure,      
			ErrorLine,      
			ErrorMessage,      
			ErrorDate)      
			values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());      
    
		Set @ErrorMsg = 'Error while inserting Decision, please contact Admin for more details.';    
		SELECT Number = @ErrorNumber, Severity = @ErrorSeverity, 
				StoredProcedureName = @ErrorProcedure, LineNumber = @ErrorLine, Message = @ErrorMsg,
				Success = 0;       
       
	END CATCH 
	--print  @OutDecisionId

	--If (@CopyDecisionParametersFromBase = 1)
	--Begin
	----Copy decision parameters for decision
	--	Execute SFab_CopyDefaultDecisionParameters @DecisionId = @OutDecisionId, @UserId = @UserId
	--End
END