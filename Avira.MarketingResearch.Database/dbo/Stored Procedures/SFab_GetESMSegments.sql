﻿CREATE PROCEDURE [dbo].[SFab_GetESMSegments] (@ProjectID uniqueidentifier ,@SegmentId  uniqueidentifier = NULL,
@AviraUserID uniqueidentifier = NULL)  
AS  
/*================================================================================  
Procedure Name: SFab_GetESMSegmentsbyID  
Author: Harshal   
Create date: 12/05/2019  
Description: Get list from ESMSegmentMapping table by Filters  
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
12/05/2019 Harshal   Initial Version  
12/09/2019 Paveen   Remove project Id condition for null  
================================================================================*/  
BEGIN  
  SET NOCOUNT ON;  
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
  SET @SegmentId = ISNULL(@SegmentId, '00000000-0000-0000-0000-000000000000')
 
  SELECT  
  Id   ,
    [SegmentName]    
  FROM  Segment
  WHERE 
 @SegmentId = CASE WHEN @SegmentId = NULL or @SegmentId = '00000000-0000-0000-0000-000000000000' THEN @SegmentId ELSE Id END AND
  EXISTS(SELECT 1 FROM [dbo].[ESMSegmentMapping] ESM WHERE ESM.ProjectId = @ProjectID AND Segment.Id = ESM.SegmentId)
  ORDER BY SegmentName  
  
END