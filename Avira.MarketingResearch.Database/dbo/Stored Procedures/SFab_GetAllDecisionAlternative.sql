﻿CREATE PROCEDURE [dbo].[SFab_GetAllDecisionAlternative]      
(    
 @DecisionId UNIQUEIDENTIFIER= NULL,--Optional  
 @DecisionStatusIdList [dbo].[udtUniqueIdentifier] READONLY,--Optional record 0 ... list
 @AviraUserId UNIQUEIDENTIFIER  
)  
As      
  
/*================================================================================      
Procedure Name: SFab_GetAllDecisionAlternative     
Author: Pooja Sawant      
Create date: 07-Jan-2020  
Description: To return all Decision Alternative by decisionId of the user  
Change History      
Date  Developer  Reason    
__________ ____________ ______________________________________________________      
07-Jan-2020  Pooja S   Initial Version      
================================================================================*/      
BEGIN      
  
 --Required Tables: Decision, DecisionAlternative,LookupStatus and LookupCategory  
 --Case 1: If @DecisionId is null then list all alternative of the all decision based on the decision statusId
 --Notes: Get all alternative by tbe decisionId of the user and validate records by userId  

 declare @DecisionStatusId uniqueidentifier
select @DecisionStatusId = ID from LookupStatus
where StatusName = 'Decision made' and StatusType='DMDecisionStatus';
SELECT        DecisionAlternative.DecisionId, DecisionAlternative.Id AS AlternativeId, DecisionAlternative.Description, DecisionAlternative.ResourceAssigned, DecisionAlternative.ResourceAssigned as ResourceAssignedStr,
DecisionAlternative.PriorityId, DecisionAlternative.StatusId, Decision.UserId, 
                         LookupCategory.CategoryName as PriorityName, LookupStatus.StatusName as StatusName
						 ,case when (Decision.StatusId = @DecisionStatusId) then 'True' else 'False' end as IsReadonly

FROM            DecisionAlternative INNER JOIN
                         Decision ON DecisionAlternative.DecisionId = Decision.Id INNER JOIN
                         LookupCategory ON DecisionAlternative.PriorityId = LookupCategory.Id INNER JOIN
                         LookupStatus ON DecisionAlternative.StatusId = LookupStatus.Id
WHERE        (Decision.UserId = @AviraUserId) 
AND (ISNULL(@DecisionId,'00000000-0000-0000-0000-000000000000') = '00000000-0000-0000-0000-000000000000' OR Decision.Id = @DecisionId)
AND (LookupCategory.CategoryType = 'DecisionAlternativePriority')
and LookupStatus.StatusType='DMDecisionAlternativeStatus'
and (NOT exists(select 1 from @DecisionStatusIdList) or Decision.StatusId In (Select Id From @DecisionStatusIdList))
order by LookupCategory.SeqNumber,DecisionAlternative.Description   
    
END