﻿using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Common.IService.Masters;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.CoreInterfaces.Masters;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;

namespace Avira.MarketingResearch.Common.Service_Implementation.Masters
{
    public class CompanyGroupService : ICompanyGroupService
    {
        private readonly ICompanyGroupRepository _companyGroupRepository;

        public CompanyGroupService(ICompanyGroupRepository buildLevelRepository)
        {
            _companyGroupRepository = buildLevelRepository;
        }

        public Result<SpTransactionMessage> Create(CompanyGroupInsertDbViewModel tfc, AviraUser currentUser)
        {
            return _companyGroupRepository.Create(tfc);
        }


        public IEnumerable<CompanyGroupViewModel> GetAll(Guid Id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<CompanyGroupViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false)
        {
            return _companyGroupRepository.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName,Search, isDeletedInclude);
        }

        public CompanyGroupViewModel GetById(Guid guid)
        {
            return _companyGroupRepository.GetById(guid);
        }


        public Result<SpTransactionMessage> Update(CompanyGroupUpdateDbViewModel tfc, AviraUser currenUser)
        {
            return _companyGroupRepository.Update(tfc);
        }
    }
}
