﻿CREATE PROCEDURE [dbo].[SFab_MarketTree]
	(
	@ParentId uniqueidentifier= null,
	@includeDeleted bit = 0,
	@AviraUserID uniqueidentifier = null,
	@OrdbyByColumnName NVARCHAR(50) ='CreatedDate'
)
	AS
/*================================================================================
Procedure Name:[SFab_MarketTree]
Author: Adarsh
Create date: 07/03/2019
Description: Get list from Market table by Name or ID
Change History
Date		Developer		Reason

================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SELECT [M].[Id],
		[M].[MarketName],
		[M].[Description],
		[M].[ParentId],
		ParentMarket.[MarketName] as ParentMarketName,
		[M].[IsDeleted]
	FROM [dbo].[Market] M
	LEFT JOIN [dbo].[Market] ParentMarket
on  [M].ParentId = ParentMarket.Id
where (@ParentId is null or [M].[ParentId]=@ParentId)
	AND (@includeDeleted = 1 or [M].IsDeleted = 0)
	
	END