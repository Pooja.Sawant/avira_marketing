﻿
--drop Procedure SFab_GetCIFinancialStatementCashFlowTabData

--execute [SFab_GetCIFinancialStatementCashFlowTabData]  @CompanyID = '2e51f475-663b-4fbf-9b75-1b9418a292e6'

Create   Procedure [dbo].[SFab_GetCIFinancialStatementCashFlowTabData] 
(
	@CompanyID uniqueidentifier = null
)
As
/*================================================================================  
Procedure Name: [SFab_GetCIFinancialStatementCashFlowTabData]  
Author: Nitin 
Create date: 12-Nov-2019  
Description: Get CI Company Financial Statement - Cash Flow Tab Data
Change History  
Date         Developer     Reason  
____________ __________  ______________________________________________________  
12-Nov-2019  Nitin       Initial Version  
================================================================================*/  
BEGIN

	--Declare @CompanyID uniqueidentifier = '97ad6d5a-56a8-4af7-961a-e220725c52f1'

	Declare @UnitId uniqueidentifier

	Declare @Years Table (CompanyID uniqueidentifier, [Year] int)
	Declare @MaxYear int, @MinYear int

	Select @MaxYear = Max([Year]), @MinYear = Min([Year])
	From CompanyPLStatement
	Where CompanyId = @CompanyID;	

	If (@MaxYear Is Not Null And @MinYear Is Not Null)
	Begin
		WITH CTE
			 AS (SELECT @CompanyID As CompanyID, @MinYear As Year1
				 UNION ALL
				 SELECT @CompanyID As CompanyID, Year1 + 1 
				 FROM   CTE
				 WHERE  Year1 < @MaxYear)
		Insert Into @Years
		SELECT *
		FROM   CTE 
	End
	
	--Select * From @Years Order by 1 desc;

	Declare @CICompanyFinancialStatementCashFlowTabData Table(
		CompanyId uniqueidentifier,
		[Year] int,
		OperatingActivities_			decimal (19, 4),
		AdjustmentsToOperatingActivities_															decimal (19, 4),
		DepreciationAndAmortization																	decimal (19, 4),
		Impairments																					decimal (19, 4),
		LossOnSaleOfAssets																			decimal (19, 4),
		OtherLossesOrGains																			decimal (19, 4),
		DeferredTaxes																				decimal (19, 4),
		ShareBasedCompensationExpense																decimal (19, 4),
		BenefitPlanContributions																	decimal (19, 4),
		OtherAdjustmentsNet																			decimal (19, 4),
		OtherChangesInAssetsAndLiabilitiesNetOfAcquisitionsAndDistitures_							decimal (19, 4),
		AccountsReceivable																			decimal (19, 4),
		OtherAssets																					decimal (19, 4),
		AccountsPayable																				decimal (19, 4),
		OtherLiabilities																			decimal (19, 4),
		OtherTaxAccountsNet																			decimal (19, 4),
		NetCashProvidedByOrUsedInOperatingActivities												decimal (19, 4),
		InvestingActivities_																		decimal (19, 4),
		PurchasesOfPropertyPlantAndEquipment														decimal (19, 4),
		PurchasesOfShortTermInvestments																decimal (19, 4),
		SaleOfShortTermInvestments																	decimal (19, 4),
		RedemptionsOfShortTermInvestments															decimal (19, 4),
		PurchasesOfLongTermInvestments																decimal (19, 4),
		SaleOfLongTermInvestments																	decimal (19, 4),
		BusinessAcquisitions																		decimal (19, 4),
		AcquisitionsOfIntangibleAssets																decimal (19, 4),
		OtherInvestingActivitiesNet																	decimal (19, 4),
		NetCashProvidedByOrUsedInInvestingActivities												decimal (19, 4),
		FinancingActivities_																		decimal (19, 4),
		ProceedsFromShortTermDebt																	decimal (19, 4),
		PrincipalPaymentsOnShortTermDebt															decimal (19, 4),
		NetPaymentsonOrProceedsFromShortTermBorrowingsWithOrigilMaturitiesOfThreeMonthsOrLess		decimal (19, 4),
		IssuanceOfLongTermDebt																		decimal (19, 4),
		PrincipalPaymentsOnLongTermDebt																decimal (19, 4),
		PurchasesOfCommonStock																		decimal (19, 4),
		CommonStockRepurchased																		decimal (19, 4),
		CashDividendsPaid																			decimal (19, 4),
		ProceedsFromExerciseOfStockOptions															decimal (19, 4),
		OtherFinancingActivitiesNet																	decimal (19, 4),
		CapitalExpenditure																			decimal (19, 4),
		NetCashProvidedByOrUsedInFinancingActivities												decimal (19, 4),
		ForeignExchangeImpact																		decimal (19, 4),
		DecreaseInCashAndCashEquivalents															decimal (19, 4),
		OpeningBalanceOfCashAndCashEquivalents														decimal (19, 4),
		ClosingBalanceOfCashAndCashEquivalents														decimal (19, 4),
		CashPaidReceivedDuringThePeriodFor_	decimal (19, 4)
													
	)

	Select @UnitId = Id 
	From ValueConversion
	Where ValueName = 'Millions';

	Insert Into @CICompanyFinancialStatementCashFlowTabData (CompanyId, [Year], 		
		OperatingActivities_,
		AdjustmentsToOperatingActivities_,
		DepreciationAndAmortization,
		Impairments,
		LossOnSaleOfAssets,
		OtherLossesOrGains,
		DeferredTaxes,
		ShareBasedCompensationExpense,
		BenefitPlanContributions,
		OtherAdjustmentsNet,
		OtherChangesInAssetsAndLiabilitiesNetOfAcquisitionsAndDistitures_,
		AccountsReceivable,
		OtherAssets,
		AccountsPayable,
		OtherLiabilities,
		OtherTaxAccountsNet,
		NetCashProvidedByOrUsedInOperatingActivities,
		InvestingActivities_,
		PurchasesOfPropertyPlantAndEquipment,
		PurchasesOfShortTermInvestments,
		SaleOfShortTermInvestments,
		RedemptionsOfShortTermInvestments,
		PurchasesOfLongTermInvestments,
		SaleOfLongTermInvestments,
		BusinessAcquisitions,
		AcquisitionsOfIntangibleAssets,
		OtherInvestingActivitiesNet,
		NetCashProvidedByOrUsedInInvestingActivities,
		FinancingActivities_,
		ProceedsFromShortTermDebt,
		PrincipalPaymentsOnShortTermDebt,
		NetPaymentsonOrProceedsFromShortTermBorrowingsWithOrigilMaturitiesOfThreeMonthsOrLess,
		IssuanceOfLongTermDebt,
		PrincipalPaymentsOnLongTermDebt,
		PurchasesOfCommonStock,
		CommonStockRepurchased,
		CashDividendsPaid,
		ProceedsFromExerciseOfStockOptions,
		OtherFinancingActivitiesNet,
		CapitalExpenditure,
		NetCashProvidedByOrUsedInFinancingActivities,
		ForeignExchangeImpact,
		DecreaseInCashAndCashEquivalents,
		OpeningBalanceOfCashAndCashEquivalents,
		ClosingBalanceOfCashAndCashEquivalents,
		CashPaidReceivedDuringThePeriodFor_
	)
	Select Y.CompanyId, Y.[Year],			
			[dbo].[fun_UnitDeConversion](@UnitId, OperatingActivities_),
			[dbo].[fun_UnitDeConversion](@UnitId, AdjustmentsToOperatingActivities_),
			[dbo].[fun_UnitDeConversion](@UnitId, DepreciationAndAmortization),
			[dbo].[fun_UnitDeConversion](@UnitId, Impairments),
			[dbo].[fun_UnitDeConversion](@UnitId, LossOnSaleOfAssets),
			[dbo].[fun_UnitDeConversion](@UnitId, OtherLossesOrGains),
			[dbo].[fun_UnitDeConversion](@UnitId, DeferredTaxes),
			[dbo].[fun_UnitDeConversion](@UnitId, ShareBasedCompensationExpense),
			[dbo].[fun_UnitDeConversion](@UnitId, BenefitPlanContributions),
			[dbo].[fun_UnitDeConversion](@UnitId, OtherAdjustmentsNet),
			[dbo].[fun_UnitDeConversion](@UnitId, OtherChangesInAssetsAndLiabilitiesNetOfAcquisitionsAndDistitures_),
			[dbo].[fun_UnitDeConversion](@UnitId, AccountsReceivable),
			[dbo].[fun_UnitDeConversion](@UnitId, OtherAssets),
			[dbo].[fun_UnitDeConversion](@UnitId, AccountsPayable),
			[dbo].[fun_UnitDeConversion](@UnitId, OtherLiabilities),
			[dbo].[fun_UnitDeConversion](@UnitId, OtherTaxAccountsNet),
			[dbo].[fun_UnitDeConversion](@UnitId, NetCashProvidedByOrUsedInOperatingActivities),
			[dbo].[fun_UnitDeConversion](@UnitId, InvestingActivities_),
			[dbo].[fun_UnitDeConversion](@UnitId, PurchasesOfPropertyPlantAndEquipment),
			[dbo].[fun_UnitDeConversion](@UnitId, PurchasesOfShortTermInvestments),
			[dbo].[fun_UnitDeConversion](@UnitId, SaleOfShortTermInvestments),
			[dbo].[fun_UnitDeConversion](@UnitId, RedemptionsOfShortTermInvestments),
			[dbo].[fun_UnitDeConversion](@UnitId, PurchasesOfLongTermInvestments),
			[dbo].[fun_UnitDeConversion](@UnitId, SaleOfLongTermInvestments),
			[dbo].[fun_UnitDeConversion](@UnitId, BusinessAcquisitions),
			[dbo].[fun_UnitDeConversion](@UnitId, AcquisitionsOfIntangibleAssets),
			[dbo].[fun_UnitDeConversion](@UnitId, OtherInvestingActivitiesNet),
			[dbo].[fun_UnitDeConversion](@UnitId, NetCashProvidedByOrUsedInInvestingActivities),
			[dbo].[fun_UnitDeConversion](@UnitId, FinancingActivities_),
			[dbo].[fun_UnitDeConversion](@UnitId, ProceedsFromShortTermDebt),
			[dbo].[fun_UnitDeConversion](@UnitId, PrincipalPaymentsOnShortTermDebt),
			[dbo].[fun_UnitDeConversion](@UnitId, NetPaymentsonOrProceedsFromShortTermBorrowingsWithOrigilMaturitiesOfThreeMonthsOrLess),
			[dbo].[fun_UnitDeConversion](@UnitId, IssuanceOfLongTermDebt),
			[dbo].[fun_UnitDeConversion](@UnitId, PrincipalPaymentsOnLongTermDebt),
			[dbo].[fun_UnitDeConversion](@UnitId, PurchasesOfCommonStock),
			[dbo].[fun_UnitDeConversion](@UnitId, CommonStockRepurchased),
			[dbo].[fun_UnitDeConversion](@UnitId, CashDividendsPaid),
			[dbo].[fun_UnitDeConversion](@UnitId, ProceedsFromExerciseOfStockOptions),
			[dbo].[fun_UnitDeConversion](@UnitId, OtherFinancingActivitiesNet),
			[dbo].[fun_UnitDeConversion](@UnitId, CapitalExpenditure),
			[dbo].[fun_UnitDeConversion](@UnitId, NetCashProvidedByOrUsedInFinancingActivities),
			[dbo].[fun_UnitDeConversion](@UnitId, ForeignExchangeImpact),
			[dbo].[fun_UnitDeConversion](@UnitId, DecreaseInCashAndCashEquivalents),
			[dbo].[fun_UnitDeConversion](@UnitId, OpeningBalanceOfCashAndCashEquivalents),
			[dbo].[fun_UnitDeConversion](@UnitId, ClosingBalanceOfCashAndCashEquivalents),
			[dbo].[fun_UnitDeConversion](@UnitId, CashPaidReceivedDuringThePeriodFor_)
	From @Years Y Left Join CompanyCashFlowStatement CCFS On Y.[Year] = CCFS.[Year] And CCFS.CompanyId = @CompanyID
	Order By Y.[Year] 

	Select top 7 *
	From @CICompanyFinancialStatementCashFlowTabData
	Order By [Year] Desc 
END