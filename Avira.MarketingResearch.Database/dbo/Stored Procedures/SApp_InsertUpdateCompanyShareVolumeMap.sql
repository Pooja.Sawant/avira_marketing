﻿


CREATE PROCEDURE [dbo].[SApp_InsertUpdateCompanyShareVolumeMap]      
(
@CompanyId uniqueidentifier,  
@CurrencyId uniqueidentifier,  
@Period int,
@BaseDate datetime,
@ShareVolumeData [dbo].[udtIntDecimalsMap] READONLY,
@AviraUserId uniqueidentifier
)      
as      
/*================================================================================      
Procedure Name: SApp_InsertUpdateCompanyShareVolumeMap      
Author: Swami      
Create date: 05/03/2019      
Description: Insert a record into CompanyShareVolumeMap table      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
05/03/2019 Swami   Initial Version    
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)  
    
BEGIN TRY         
--mark records as delete which are not in list
update [dbo].[CompanyShareVolume]
set IsDeleted = 1,
UserDeletedById = @AviraUserId,
DeletedOn = GETDATE()
from [dbo].[CompanyShareVolume]
where [CompanyShareVolume].[CompanyId] = @CompanyId
and exists (SELECT 1 FROM dbo.[CompanyShareVolume] where [CompanyId] = @CompanyId and IsDeleted = 0)


--Insert new records
insert into [dbo].[CompanyShareVolume]([Id], 
		[CompanyId]    , 
        [CurrencyId],
		[Period],
		[BaseDate],
		[Date],
		[Price],
		[Volume],
        [IsDeleted],      
        [UserCreatedById],      
        [CreatedOn])      
 select NEWID(),
		@CompanyId,  
		@CurrencyId,  
		@Period,
		@BaseDate,
		map.[IntValue],
		map.[DecimalOne],
		map.[DecimalTwo],
		0, 
		@AviraUserId,       
		GETDATE()
 from @ShareVolumeData map
 where not exists (select 1 from [dbo].[CompanyShareVolume] map1
					where map1.[CompanyId] = @CompanyId and map1.IsDeleted = 0);       
     
SELECT StoredProcedureName ='SApp_InsertUpdateCompanyShareVolumeMap ',Message =@ErrorMsg,Success = 1;       
    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);         
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);         
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());      
    
 set @ErrorMsg = 'Error while insert a new CompanyShareVolumeMap, please contact Admin for more details.';    
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
       
END CATCH      
      
      
end