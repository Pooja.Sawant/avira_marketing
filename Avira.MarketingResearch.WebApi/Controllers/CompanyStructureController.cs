﻿using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Utility;
using Avira.MarketingResearch.WebApi;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.Repository.CoreInterfaces.Masters;
using Avira.MarketingResearch.Common.IService.Masters;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using System;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Common.IService.CompanyProfile;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyStructureController : AviraBaseController
    {
        private ILoggerManager _logger;
        public readonly ICompanyStructureService _companyStructureService;
        public readonly ICompanyStructureRepository _companyStructureRepository;

        public CompanyStructureController(
            ICompanyStructureService companyStructureService,
            ICompanyStructureRepository CompanyStructureRepository,
            ILoggerManager logger)
        {
            _companyStructureRepository = CompanyStructureRepository;
            _companyStructureService = companyStructureService;
            _logger = logger;
        }

        [HttpGet("{Id}")]
        //[Route("CompanyStructure")]
        public ActionResult<IEnumerable<CompanyStructureModel>> Get(string Id)
        {
            List<CompanyStructureModel> res = null;
            try
            {
                Guid id = new Guid(Id);
                //List<CompanyStructureModel> obj = new List<CompanyStructureModel>();
                //obj = _companyStructureService.GetAll(Id);//_companyStructureService.GetAll(Id);
                //return obj;
                res = _companyStructureService.GetAll(id).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

    }
}