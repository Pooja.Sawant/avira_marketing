﻿using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces.Masters;
using Avira.MarketingResearch.Utility;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation
{
    public class LookupCategoryService : ILookupCategoryService
    {
        private readonly ILookupCategoryRepository _iLookupCategoryRepository;
        private ILoggerManager _logger;

        public LookupCategoryService(ILookupCategoryRepository iLookupCategoryRepository, ILoggerManager logger)
        {
            _iLookupCategoryRepository = iLookupCategoryRepository;
            _logger = logger;
        }

        public IEnumerable<CategoryViewModel> GetAll(string categoryType)
        {
            return _iLookupCategoryRepository.GetAll(categoryType);
        }
    }
}
