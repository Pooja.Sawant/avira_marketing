﻿CREATE PROCEDURE [dbo].[SFab_UpdateDecisionAlternative]    
(  
	@AlternativeId UNIQUEIDENTIFIER,	
	@DecisionId UNIQUEIDENTIFIER,	
	@ResourceAssigned NVARCHAR(899),
	@StatusId UNIQUEIDENTIFIER,
	@PriorityId UNIQUEIDENTIFIER,
	@AviraUserId UNIQUEIDENTIFIER=null

)
As    

/*================================================================================    
Procedure Name: SFab_UpdateDecisionAlternative   
Author: Praveen  /Pooja S
Create date: 08-Jan-2020
Description: To update existing Decision Alternative by user
Change History    
Date  Developer  Reason  
__________ ____________ ______________________________________________________    
08-Jan-2020  Praveen   Initial Version    
15-Jan-2020  Pooja S   Implementation with changes in input parameter  
================================================================================*/    
BEGIN  
SET NOCOUNT ON;  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
declare @ErrorMsg NVARCHAR(2048);

BEGIN TRY

	--Required Tables: DecisionAlternative
	--Notes: update Resource Assigned, Priority and status only of perticular alternative.
	--Expected Result :SELECT StoredProcedureName ='SFab_UpdateDecisionAlternative',Message = @ErrorMsg,Success = 1/0;
	Declare @DecisionStatusDecisionMadeId uniqueidentifier
	Declare @DecisionStatusDecisionId uniqueidentifier
	Select @DecisionStatusDecisionMadeId = ID From LookupStatus
	Where StatusName = 'Decision made' And StatusType = 'DMDecisionStatus'

	Select @DecisionStatusDecisionId = ID From LookupStatus
	Where StatusName = 'Ongoing' And StatusType = 'DMDecisionStatus'
	
	If Exists (Select ID from Decision Where Id = @DecisionId and UserId= @AviraUserID and StatusId=@DecisionStatusDecisionMadeId)
        Begin
        	set @ErrorMsg = 'Can not edit any existing details since this decision has been made.';   
        		SELECT StoredProcedureName ='SFab_InsertDecisionAlternative','Message' =@ErrorMsg,'Success' = 0;  
        end
	else 
	If Exists (Select ID from Decision Where Id = @DecisionId and UserId= @AviraUserID and (StatusId != @DecisionStatusDecisionMadeId and StatusId != @DecisionStatusDecisionId))
	    Begin
	    	set @ErrorMsg = 'Please change decision status to Ongoing in order to edit existing details.';   
	    		SELECT StoredProcedureName ='SFab_InsertDecisionAlternative','Message' =@ErrorMsg,'Success' = 0;    
	    end
	else

begin  

 update [dbo].[DecisionAlternative]  
	 set ResourceAssigned=trim(@ResourceAssigned), StatusId= @StatusId, PriorityId= @PriorityId
	 where id= @AlternativeId 
 and DecisionId=@DecisionId
 SELECT StoredProcedureName ='SFab_UpdateDecisionAlternative',Message =@ErrorMsg,Success = 1;     
end 
		

END TRY
BEGIN CATCH
    -- Execute the error retrieval routine.
	EXEC SApp_LogErrorInfo
    set @ErrorMsg = 'Error while update Decision Alternative, please contact Admin for more details.';    
	SELECT StoredProcedureName ='SFab_UpdateDecisionAlternative', Message = @ErrorMsg, Success = 0;     
       
END CATCH 
end