﻿CREATE TABLE [dbo].[LookupStatus] (
    [Id]          UNIQUEIDENTIFIER NOT NULL,
    [StatusName]  NVARCHAR (100)   NOT NULL,
    [Description] NVARCHAR (300)   NULL,
    [StatusType]  NVARCHAR (200)   NULL,
    [CreatedOn]   DATETIME2 (7)    NULL,
    [CreatedBy]   UNIQUEIDENTIFIER NULL,
    [SeqNumber]   INT              NULL,
    CONSTRAINT [PK_LookupStatus] PRIMARY KEY CLUSTERED ([Id] ASC)
);



