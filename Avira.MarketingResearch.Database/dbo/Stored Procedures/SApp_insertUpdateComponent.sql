﻿CREATE procedure [dbo].[SApp_insertUpdateComponent]
(@ComponentID uniqueidentifier,
@Description nvarchar(256),
@ComponentName nvarchar(256),
@IsDeleted bit,
@AviraUserId uniqueidentifier)
as
/*================================================================================
Procedure Name: SApp_DeleteComponentbyID
Author: Sharath
Create date: 02/19/2019
Description: soft delete a record from Component table by ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
02/19/2019	Sharath			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @SegmentId uniqueidentifier;
select @SegmentId = Id from Segment
where SegmentName = 'Components';

IF exists(select 1 from [dbo].SubSegment 
		 where ((@ComponentID is null or [Id] !=  @ComponentID)
		 and SegmentId = @SegmentId
		 and  SubSegmentName = @ComponentName AND IsDeleted = 0))
begin
declare @ErrorMsg NVARCHAR(2048);
set @ErrorMsg = 'Component with Name "'+ @ComponentName + '" already exists.';
THROW 50000,@ErrorMsg,1;
end

IF (@ComponentID is null)
begin
--Insert new record
	set @ComponentID = NEWID();

	insert into [dbo].SubSegment([Id],
								SegmentId,
								[SubSegmentName],
								[Description],
								[IsDeleted],
								[UserCreatedById],
								[CreatedOn])
	values(@ComponentID, 
			@SegmentId,
			@ComponentName, 
			@Description, 
			0,
			@AviraUserId, 
			getdate());
end
else
begin
--update record
	update [dbo].SubSegment
	set SegmentId = @SegmentId,
		SubSegmentName = @ComponentName,
		[Description] = @Description,
		[ModifiedOn] = getdate(),
		DeletedOn = case when IsDeleted = 0 and @IsDeleted = 1 then getdate() else DeletedOn end,
		UserDeletedById = case when IsDeleted = 0 and @IsDeleted = 1 then @AviraUserId else UserDeletedById end,
		IsDeleted = isnull(@IsDeleted, IsDeleted),
		[UserModifiedById] = @AviraUserId
     where ID = @ComponentID
	 
end

select @ComponentID;
end