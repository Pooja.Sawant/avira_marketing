﻿
 CREATE PROCEDURE [dbo].[SApp_GetTrendPreviewbyTrendID]          
 (    
 @TrendID uniqueidentifier = null
 )          
 AS          
/*================================================================================          
Procedure Name: SApp_GetTrendPreviewbyTrendID    
Author: Harshal          
Create date: 04/26/2019          
Description: Get trend Mapping Status of by TrendID        
Change History          
Date  Developer  Reason          
__________ ____________ ______________________________________________________          
04/26/2019 Harshal   Initial Version 

29/06/2019 Sai Krishna    Changed the Region to country         
================================================================================*/         
          
BEGIN          
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED    
 declare @SubmittedStatusID uniqueidentifier;
 select @SubmittedStatusID = id from TrendStatus
 where TrendStatusName = 'Submitted'
 declare @IndustrySegmentId uniqueidentifier;
select @IndustrySegmentId = Id from Segment
where SegmentName = 'End User';

;WITH CTE_TST as (select TrendType, 
[Submitted] as SubmittedDate,
[Approved] as ApprovedDate
from (
select 
TrendStatusTracking.TrendType,
TrendStatus.TrendStatusName,
TrendStatusTracking.CreatedOn as StatusDate
from TrendStatusTracking
inner join TrendStatus
on TrendStatusTracking.TrendStatusID = TrendStatus.Id
where TrendStatusTracking.TrendId = @TrendID
) src
pivot
(max(StatusDate)
  for TrendStatusName in ([Submitted], [Approved])) piv)

SELECT Trend.Id,
Trend.TrendName,
STUFF((SELECT ','+TrendKeyWord  FROM TrendKeywords
	  WHERE TrendKeywords.TrendId = Trend.Id AND TrendKeywords.IsDeleted=0
	   FOR XML PATH('')), 1, 1, '') AS Keywords,
	  (SELECT SubmittedDate FROM CTE_TST WHERE  TrendType='KeyWords' ) As KeywordsSubmittedDate,
	  (SELECT ApprovedDate FROM CTE_TST WHERE  TrendType='KeyWords' )  As KeywordsApprovedDate,
STUFF((SELECT ','+ SubSegment.SubSegmentName
		from [dbo].[TrendIndustryMap]
		inner join [dbo].SubSegment
		on [TrendIndustryMap].IndustryId = SubSegment.Id
		where [TrendIndustryMap].TrendId = Trend.Id 
		and [TrendIndustryMap].Impact ! = '00000000-0000-0000-0000-000000000000' 
		AND [TrendIndustryMap].IsDeleted=0
		and SubSegment.SegmentId = @IndustrySegmentId
		FOR XML PATH('')), 1, 1, '') AS Industry,
		(SELECT SubmittedDate FROM CTE_TST WHERE  TrendType='Industry' ) As IndustrySubmittedDate,
		(SELECT ApprovedDate FROM CTE_TST WHERE  TrendType='Industry' )  As IndustryApprovedDate,
STUFF((SELECT ','+ Market.MarketName 
		from [dbo].[TrendMarketMap]
		inner join [dbo].[Market]
		on [TrendMarketMap].MarketId = Market.Id
		where [TrendMarketMap].TrendId = Trend.Id AND [TrendMarketMap].Impact ! = '00000000-0000-0000-0000-000000000000' AND [TrendMarketMap].IsDeleted=0
		FOR XML PATH('')), 1, 1, '') AS Market,
		(SELECT SubmittedDate FROM CTE_TST WHERE  TrendType='Market' ) As MarketSubmittedDate,
		(SELECT ApprovedDate FROM CTE_TST WHERE  TrendType='Market' )  As MarketApprovedDate,
STUFF((SELECT ','+ Project.ProjectName 
		from [dbo].[TrendProjectMap]
		inner join [dbo].[Project]
		on [TrendProjectMap].ProjectId = Project.Id
		where [TrendProjectMap].TrendId = Trend.Id AND [TrendProjectMap].Impact ! = '00000000-0000-0000-0000-000000000000' AND [TrendProjectMap].IsDeleted=0
		FOR XML PATH('')), 1, 1, '') AS Project,
		(SELECT SubmittedDate FROM CTE_TST WHERE  TrendType='Project' ) As ProjectSubmittedDate,
		(SELECT ApprovedDate FROM CTE_TST WHERE  TrendType='Project' )  As ProjectApprovedDate,
--STUFF((SELECT ','+ Region.RegionName 
--		from [dbo].[TrendRegionMap]
--		inner join [dbo].[Region]
--		on [TrendRegionMap].RegionId = Region.Id
--		where [TrendRegionMap].TrendId = Trend.Id
--		FOR XML PATH('')), 1, 1, '') AS Geography,
--		(SELECT SubmittedDate FROM CTE_TST WHERE  TrendType='Region' ) As GeographySubmittedDate,
--		(SELECT ApprovedDate FROM CTE_TST WHERE  TrendType='Region' )  As GeographyApprovedDate,
STUFF((SELECT ','+ Country.CountryName 
		from [dbo].[TrendCountryMap]
		inner join [dbo].[Country]
		on [TrendCountryMap].CountryId = Country.Id
		where [TrendCountryMap].TrendId = Trend.Id AND [TrendCountryMap].Impact ! = '00000000-0000-0000-0000-000000000000' AND [TrendCountryMap].IsDeleted=0
		FOR XML PATH('')), 1, 1, '') AS Geography,
		(SELECT SubmittedDate FROM CTE_TST WHERE  TrendType='Region' ) As GeographySubmittedDate,
		(SELECT ApprovedDate FROM CTE_TST WHERE  TrendType='Region' )  As GeographyApprovedDate,
STUFF((SELECT ','+ CompanyGroup.CompanyGroupName 
		from [dbo].[TrendCompanyGroupMap]
		inner join [dbo].[CompanyGroup]
		on [TrendCompanyGroupMap].CompanyGroupId = CompanyGroup.Id
		where [TrendCompanyGroupMap].TrendId = Trend.Id AND [TrendCompanyGroupMap].Impact ! = '00000000-0000-0000-0000-000000000000'  AND [TrendCompanyGroupMap].IsDeleted=0
		FOR XML PATH('')), 1, 1, '') AS CompanyGroups,
		(SELECT SubmittedDate FROM CTE_TST WHERE  TrendType='Company Group' ) As CompanyGroupsSubmittedDate,
		(SELECT ApprovedDate FROM CTE_TST WHERE  TrendType='Company Group' )  As CompanyGroupsApprovedDate,
STUFF((SELECT ','+ Timetag.TimetagName 
		from [dbo].[TrendTimetag]
		inner join [dbo].[Timetag]
		on [TrendTimetag].TimetagId = Timetag.Id
		where [TrendTimetag].TrendId = Trend.Id AND [TrendTimetag].Impact ! = '00000000-0000-0000-0000-000000000000' AND [TrendTimetag].IsDeleted=0
		FOR XML PATH('')), 1, 1, '') AS Timetag,
		(SELECT SubmittedDate FROM CTE_TST WHERE  TrendType='Time Tag' ) As TimeTagSubmittedDate,
		(SELECT ApprovedDate FROM CTE_TST WHERE  TrendType='Time Tag' )  As TimeTagApprovedDate,
STUFF((SELECT ','+ Segment.SegmentName
		from [dbo].[TrendEcoSystemMap]
		inner join [dbo].Segment
		on [TrendEcoSystemMap].EcoSystemId = Segment.Id
		where [TrendEcoSystemMap].TrendId = Trend.Id AND [TrendEcoSystemMap].Impact ! = '00000000-0000-0000-0000-000000000000' AND [TrendEcoSystemMap].IsDeleted=0
		FOR XML PATH('')), 1, 1, '') AS EcoSystem,
		(SELECT SubmittedDate FROM CTE_TST WHERE  TrendType='EcoSystem' ) As EcoSystemSubmittedDate,
		(SELECT ApprovedDate FROM CTE_TST WHERE  TrendType='EcoSystem' )  As EcoSystemApprovedDate,
STUFF((SELECT ','+ Company.CompanyName 
		from [dbo].[TrendKeyCompanyMap]
		inner join [dbo].[Company]
		on [TrendKeyCompanyMap].CompanyId = Company.Id 
		where [TrendKeyCompanyMap].TrendId = Trend.Id AND [TrendKeyCompanyMap].IsDeleted=0
		FOR XML PATH('')), 1, 1, '') AS KeyCompanies,
		(SELECT SubmittedDate FROM CTE_TST WHERE  TrendType='KeyCompany' ) As KeyCompaniesSubmittedDate,
		(SELECT ApprovedDate FROM CTE_TST WHERE  TrendType='KeyCompany' )  As KeyCompaniesApprovedDate

	   FROM Trend Trend
where Trend.Id = @TrendId     
      
END