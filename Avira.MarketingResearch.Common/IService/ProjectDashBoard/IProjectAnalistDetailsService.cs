﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.ProjectDashBoard
{
    public interface IProjectAnalistDetailsService
    {
        IEnumerable<ProjectAnalistDetailsViewModel> GetAll(Guid Id);
        IEnumerable<ProjectAnalistDetailsViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false);
        ProjectAnalistDetailsViewModel GetById(Guid guid);


        Result<SpTransactionMessage> Create(ProjectAnalistDetailsInsertDbViewModel tfc, AviraUser currentUser);
        Result<SpTransactionMessage> Update(ProjectAnalistDetailsUpdateDbViewModel tfc, AviraUser currenUser);
    }
}
