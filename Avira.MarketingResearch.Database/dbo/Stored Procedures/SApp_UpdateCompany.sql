﻿
CREATE procedure [dbo].[SApp_UpdateCompany]    
(@Id uniqueidentifier,    
@CompanyCode nvarchar(256),
@CompanyName  nvarchar(256),    
@Description nvarchar(256),
@Telephone nvarchar(20),
@Telephone2 nvarchar(20),
@Email nvarchar(100),
@Email2 nvarchar(100),
@Fax nvarchar(20),
@Fax2 nvarchar(20),
@ParentCompanyName nvarchar(256), 
@ProfiledCompanyName nvarchar(256), 
@Website  nvarchar(128), 
@NoOfEmployees int,
@YearOfEstb int,
@ParentCompanyYearOfEstb int,    
@IsDeleted bit=0,  
@UserModifiedById  uniqueidentifier,  
@DeletedOn datetime = null,  
@UserDeletedById uniqueidentifier = null,  
@ModifiedOn Datetime   
)
as    
/*================================================================================    
Procedure Name:[SApp_UpdateCompany]    
Author: Harshal    
Create date: 03/26/2019    
Description: update a record in Comapny table by ID. Also used for delete    
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
03/26/2019 Harshal   Initial Version  
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
  
IF exists(select 1 from [dbo].[Company]     
   where [Id] !=  @Id    
   and  CompanyName = @CompanyName AND IsDeleted = 0)    
begin    
declare @ErrorMsg NVARCHAR(2048);    
set @ErrorMsg = 'Company with Name "'+ @CompanyName + '" already exists.';    
--SELECT 50000,@ErrorMsg,1;   
--return(1);   
SELECT StoredProcedureName ='SApp_UpdateCompany','Message' =@ErrorMsg,'Success' = 0;  
RETURN;
end    
  
BEGIN TRY     
--update record    
update [dbo].[Company]    
set 
 --[CompanyCode]=@CompanyCode,
 [CompanyName] = @CompanyName,    
 [Description] = @Description,   
 [Telephone]=@Telephone,
 [Telephone2]=@Telephone2,
 [Email]=@Email,
 [Email2]=@Email2,
 [Fax]=@Fax,
 [Fax2]=@Fax2,
 [ParentCompanyName]=@ParentCompanyName,
 [ProfiledCompanyName]=@ProfiledCompanyName,
 [Website]=@Website,
 [NoOfEmployees]=@NoOfEmployees,
 [YearOfEstb]=@YearOfEstb,
 [ParentCompanyYearOfEstb]=@ParentCompanyYearOfEstb, 
 [ModifiedOn] = @ModifiedOn,    
 DeletedOn = case when IsDeleted = 0 and @IsDeleted = 1 then @ModifiedOn else DeletedOn end,    
 UserDeletedById = case when IsDeleted = 0 and @IsDeleted = 1 then @UserModifiedById else UserDeletedById end,    
 IsDeleted = isnull(@IsDeleted, IsDeleted),    
 [UserModifiedById] = @UserModifiedById    
    where ID = @Id   
   
SELECT StoredProcedureName ='SApp_UpdateCompany',Message =@ErrorMsg,Success = 1;     
  
END TRY    
BEGIN CATCH    
    -- Execute the error retrieval routine.    
 DECLARE @ErrorNumber int;    
 DECLARE @ErrorSeverity int;    
 DECLARE @ErrorProcedure varchar(100);    
 DECLARE @ErrorLine int;    
 DECLARE @ErrorMessage varchar(500);    
    
  SELECT @ErrorNumber = ERROR_NUMBER(),    
        @ErrorSeverity = ERROR_SEVERITY(),    
        @ErrorProcedure = ERROR_PROCEDURE(),    
        @ErrorLine = ERROR_LINE(),    
        @ErrorMessage = ERROR_MESSAGE()    
    
 insert into dbo.Errorlog(ErrorNumber,    
       ErrorSeverity,    
       ErrorProcedure,    
       ErrorLine,    
       ErrorMessage,    
       ErrorDate)    
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE()); 

set @ErrorMsg = 'Error while updating company, please contact Admin for more details.' 		   
 --set @ErrorMsg = 'Unspecified Error';    
--THROW 50000,@ErrorMsg,1;    
 --return(1)  
 SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;     
END CATCH    
    
  
  
    
end