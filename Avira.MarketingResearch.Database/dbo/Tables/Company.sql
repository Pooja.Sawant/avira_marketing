﻿CREATE TABLE [dbo].[Company] (
    [Id]                      UNIQUEIDENTIFIER NOT NULL,
    [CompanyCode]             NVARCHAR (256)   NOT NULL,
    [CompanyName]             NVARCHAR (256)   CONSTRAINT [DF_Company_CompanyName] DEFAULT ('') NOT NULL,
    [Description]             NVARCHAR (256)   NULL,
    [Telephone]               NVARCHAR (20)    NULL,
    [Telephone2]              NVARCHAR (20)    NULL,
    [Email]                   NVARCHAR (20)    NULL,
    [Email2]                  NVARCHAR (20)    NULL,
    [Fax]                     NVARCHAR (20)    NULL,
    [Fax2]                    NVARCHAR (20)    NULL,
    [ParentCompanyName]       NVARCHAR (256)   NULL,
    [ProfiledCompanyName]     NVARCHAR (256)   NULL,
    [Website]                 NVARCHAR (128)   NULL,
    [NoOfEmployees]           INT              NULL,
    [YearOfEstb]              INT              NULL,
    [ParentCompanyYearOfEstb] INT              NULL,
    [CreatedOn]               DATETIME2 (7)    NULL,
    [UserCreatedById]         UNIQUEIDENTIFIER NULL,
    [ModifiedOn]              DATETIME2 (7)    NULL,
    [UserModifiedById]        UNIQUEIDENTIFIER NULL,
    [IsDeleted]               BIT              CONSTRAINT [DF_Company_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]               DATETIME2 (7)    NULL,
    [UserDeletedById]         UNIQUEIDENTIFIER NULL,
    [EntityTypeId]            UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_Company] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Company_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_Company_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id])
);



