﻿
CREATE procedure [dbo].[SApp_InsertUpdateCompanyTrendsMap]      
(
	@CompanyTrendsMap [dbo].[udtGUIDCompanyTrendMapMultiValue] readonly,
	@CompanyId uniqueidentifier,
	@UserCreatedById uniqueidentifier
)      
as      
/*================================================================================      
Procedure Name: [SApp_InsertUpdateCompanyTrendsMap]      
Author: Harshal     
Create date: 05/08/2019      
Description: Insert a record into CompanyTrendsMap table      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
05/08/2019 Harshal   Initial Version    
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)  
    
      
BEGIN TRY         
--mark records as delete which are not in list
update [dbo].[CompanyTrendsMap]
set IsDeleted = 1,
UserDeletedById = @UserCreatedById,
DeletedOn = GETDATE()
from [dbo].[CompanyTrendsMap]
where ([CompanyTrendsMap].CompanyId = @CompanyId )
and not exists (select 1 from @CompanyTrendsMap map where map.Id = [CompanyTrendsMap].Id)

 --Update records for change
update [dbo].[CompanyTrendsMap]
set TrendName = map.TrendName,
Department = map.Department,
ImportanceId = map.ImportanceId,
ImpactId = map.ImpactId,
ModifiedOn = GETDATE(),
UserModifiedById = @UserCreatedById
from [dbo].[CompanyTrendsMap]
inner join @CompanyTrendsMap map 
on map.Id = [CompanyTrendsMap].Id
where [CompanyTrendsMap].CompanyId = @CompanyId;

--Insert new records
insert into [dbo].[CompanyTrendsMap](
		[Id]      
       ,[TrendName]
	   ,[Department]
       ,[CompanyId]
       ,[ImportanceId]
	   ,[ImpactId]
       ,[IsDeleted]      
       ,[UserCreatedById]      
       ,[CreatedOn])      
 select map.Id,
   map.TrendName,       
   map.Department,
   map.CompanyId,
   map.ImportanceId,
   map.ImpactId,
   0,      
   @UserCreatedById,       
   GETDATE()
 from @CompanyTrendsMap map
 where not exists (select 1 from [dbo].[CompanyTrendsMap] map1
					where map1.CompanyId = @CompanyId  
					and map.Id = map1.Id);       
     
SELECT StoredProcedureName ='SApp_InsertUpdateCompanyTrendsMap',Message =@ErrorMsg,Success = 1;       
    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(1000);      
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,ERROR_MESSAGE(),GETDATE());      
    
 set @ErrorMsg = 'Error while insert a new CompanyTrendsMap, please contact Admin for more details.';    
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
       
END CATCH      
      
      
end