﻿using Avira.MarketingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.Trend
{
    public class TrendNoteService : ITrendNoteService
    {
        private readonly ITrendNoteRepository _iTrendNoteRepository;

        public TrendNoteService(ITrendNoteRepository iTrendNoteRepository)
        {
            _iTrendNoteRepository = iTrendNoteRepository;
        }

        public TrendNoteViewModel GetById(Guid id,string noteType)
        {
            return _iTrendNoteRepository.GetById(id, noteType);
        }

        public IEnumerable<TrendNoteViewModel> GetAllByTrendId(Guid TrendId, string StatusName)
        {
            return _iTrendNoteRepository.GetAllByTrendId(TrendId,StatusName);
        }
    }
}
