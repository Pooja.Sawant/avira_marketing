﻿CREATE TYPE [dbo].[udtGUIDProjectDetails] AS TABLE (
    [CoAuthorUserID]   UNIQUEIDENTIFIER NULL,
    [CoApproverUserID] UNIQUEIDENTIFIER NULL,
    [SegmentID]        UNIQUEIDENTIFIER NULL);

