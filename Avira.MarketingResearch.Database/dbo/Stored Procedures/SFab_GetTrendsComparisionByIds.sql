﻿CREATE PROCEDURE [dbo].[SFab_GetTrendsComparisionByIds]
(@TrendIdList [dbo].[udtUniqueIdentifier] READONLY,
@ProjectId uniqueidentifier = null, 
@AviraUserID uniqueidentifier = null,  
@AviraUserRoleId uniqueidentifier = null)
as
/*================================================================================
Procedure Name: [SFab_GetTrendsComparisionByIds]
Author: Praveen
Create date: 22/05/2019
Description: Get trend comparision list
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
22/05/2019	Praveen 		Initial Version
10/09/2019  Harshal         Added ProjectId
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @TrendApprovedStatusId uniqueidentifier;
DECLARE @BaseYear int;
declare @CustomerId uniqueidentifier
select @CustomerId = CustomerId from AviraUser where id=@AviraUserID;
	select @TrendApprovedStatusId = Id
	from TrendStatus
	where TrendStatusName = 'Approved';

   select @BaseYear = BaseYear from BaseYearMaster
select trend.Id as TrendId,
Trend.TrendName,
Trend.Description,
Trend.ImpactDirectionId,
ImpactDirection.ImpactDirectionName,
trend.ImportanceId,
Importance.GroupName As ImportanceName,

STUFF
  (
    (
select ', ' + Industry.SubSegmentName
 from SubSegment as Industry
INNER JOIN Segment ON Industry.SegmentId = Segment.Id AND Segment.SegmentName = 'End User'
inner join TrendIndustryMap on TrendIndustryMap.IndustryId = Industry.Id
where TrendIndustryMap.TrendId = Trend.Id AND TrendIndustryMap.IsDeleted=0 AND TrendIndustryMap.Impact ! = '00000000-0000-0000-0000-000000000000'  
FOR XML PATH('')
    ), 1, 2, N''
  )
as IndustryList,



(select top 1 TimeTag.TimeTagName from TimeTag 
inner join TrendTimeTag on timetag.Id = TrendTimeTag.TimeTagId
where TrendTimeTag.IsDeleted = 0
and TrendTimeTag.TrendId = Trend.Id AND TrendTimeTag.Impact ! = '00000000-0000-0000-0000-000000000000'
order by TimeTag.Seq desc) as TimeLine,

STUFF
  (
    (
select '| ' + Company.CompanyName
from TrendKeyCompanyMap
inner join Company
on TrendKeyCompanyMap.CompanyId = Company.id
where TrendKeyCompanyMap.TrendId = Trend.Id AND TrendKeyCompanyMap.IsDeleted=0 
FOR XML PATH('')
    ), 1, 2, N''
  )


 as CompanyList,

STUFF
  (
    (select distinct ',' + Segment.SegmentName
from TrendEcoSystemMap
inner join Segment
on TrendEcoSystemMap.EcoSystemId = Segment.id
where TrendEcoSystemMap.TrendId = Trend.Id AND TrendEcoSystemMap.IsDeleted=0 AND TrendEcoSystemMap.Impact ! = '00000000-0000-0000-0000-000000000000'
FOR XML PATH('')
    ), 1, 1, N''
  )


as EcoSystemList,
Trend.TrendValue,
  CASE       
  WHEN Importance.GroupName = 'High' THEN 'high-bg'      
  WHEN Importance.GroupName = 'Mid' THEN 'mid-bg'      
  WHEN Importance.GroupName = 'Low' THEN 'low-bg' ELSE 'low-bg' END      
  AS ImportanceColorCode,
  (case when ISNULL(TrendBuildMyViewPoint.TrendValue,0) = 0 then Trend.TrendValue else TrendBuildMyViewPoint.TrendValue end) as MakeMyViewPoint
from Trend
left join ImpactDirection
on Trend.ImpactDirectionId = ImpactDirection.Id
left join Importance
on trend.ImportanceId = Importance.Id
left join TrendBuildMyViewPoint
on TrendBuildMyViewPoint.TrendId=Trend.id and TrendBuildMyViewPoint.Year=@BaseYear and TrendBuildMyViewPoint.CustomerId=@CustomerId
where --trend.id = '29C5FF4B-D602-4943-865E-45581C1BCCAF'
trend.id in (select ID from @TrendIdList)
AND trend.TrendStatusID=@TrendApprovedStatusId
AND trend.ProjectID=@ProjectId



END
--select * from TrendBuildMyViewPoint