﻿using Avira.MarketingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.Trend
{
    public class TrendIndustryMapService : ITrendIndustryMapService
    {
        private readonly ITrendIndustryMapRepository _trendIndustryMapRepository;
        private readonly ITrendNoteRepository _iTrendNoteRepository;
        public TrendIndustryMapService(ITrendIndustryMapRepository iTrendIndustryMapRepository, ITrendNoteRepository iTrendNoteRepository)
        {
            _trendIndustryMapRepository = iTrendIndustryMapRepository;
            _iTrendNoteRepository = iTrendNoteRepository;
        }

        public IEnumerable<TrendIndustryMapViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false, string id = "")
        {
            return _trendIndustryMapRepository.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude = false, id);
        }

        public Result<SpTransactionMessage> Create(TrendIndustryMapInsertDbViewModel tfc, AviraUser currentUser)
        {
            var Result = _trendIndustryMapRepository.Create(tfc);
            tfc.TrendNoteModel.Id = Guid.NewGuid();
            tfc.TrendNoteModel.UserCreatedById = tfc.AviraUserId;
            tfc.TrendNoteModel.TrendId = tfc.TrendId;
            _iTrendNoteRepository.Insert(tfc.TrendNoteModel);

            return Result;
        } 
    }
}
