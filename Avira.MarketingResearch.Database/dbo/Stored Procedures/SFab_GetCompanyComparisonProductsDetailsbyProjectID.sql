﻿
CREATE procedure [dbo].[SFab_GetCompanyComparisonProductsDetailsbyProjectID]
(
	@SegmentID uniqueidentifier=null,
	@SegmentList dbo.[udtUniqueIdentifier] readonly,
	--@RegionId uniqueidentifier = null,
	@RegionIdList dbo.[udtUniqueIdentifier] readonly,
	@CountryIdList dbo.[udtUniqueIdentifier] readonly,
	--@CompanystageId UniqueIdentifier=null,
	@CompanystageId [dbo].[udtUniqueIdentifier] READONLY ,
	@StratergyList [dbo].[udtUniqueIdentifier] READONLY,
	@Years nvarchar(100),
	@ProjectId uniqueidentifier,
	@CompanyIdList dbo.[udtUniqueIdentifier] readonly,
	@AviraUserID uniqueidentifier = null
)
as
/*================================================================================
Procedure Name: [SFab_GetCompanyComparisonProductsDetailsbyProjectID]
Author: Harshal
Create date: 07/29/2019
Description: Get list from company table by project ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
07/29/2019	Harshal	    Initial Version
================================================================================*/
BEGIN

	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	Declare @USDCurrencyId uniqueidentifier;
	Declare @ProjectApprovedStatusId uniqueidentifier
	
	Select @USDCurrencyId = Id From Currency Where CurrencyName = 'United States dollar';
	Select @ProjectApprovedStatusId = ID From ProjectStatus Where ProjectStatusName = 'Completed';

	declare @RegionList dbo.[udtUniqueIdentifier];
	declare @YearList table (Year int);

	Insert Into @YearList
	Select Value
	From string_split(@Years,',')

	Insert Into @RegionList
	select Id
	from Region
	where id in(select ID from @RegionIdList)
	or exists (select 1
				from CountryRegionMap CRM
				inner join @CountryIdList Loc
				on CRM.CountryId = Loc.ID
				and Region.id = CRM.RegionId);

	;With Clist As 
		(Select comp.Id as CompanyId, comp.CompanyName, comp.YearOfEstb
		 From Company comp
		 Where comp.IsDeleted = 0 and exists (select 1 from CompanyProjectMap cpmap
					inner join project proj
					on cpmap.ProjectId = proj.ID
					where comp.Id = cpmap.CompanyId 
					and proj.ProjectStatusID = @ProjectApprovedStatusId
		and cpmap.ProjectId = @ProjectId)
		and (not exists(select 1 from @CompanyIdList) or comp.Id in (select ID from @CompanyIdList))
		and (not exists (select * from @RegionList) 
			or exists(select 1 from CompanyRegionMap CRM
					  inner join @RegionList Rlist
					  on CRM.RegionId = Rlist.ID))
		--and (@CompanystageId is null or comp.CompanyStageId = @CompanystageId)
			and (not exists (select * from @CompanystageId) 
		or exists(select 1 from [CompanyStage] CST
				inner join  @CompanystageId CSTAList
				on CST.Id = CSTAList.ID
				where comp.CompanyStageId = CSTAList.ID))
		and (not exists (select * from @StratergyList) 
			or exists(select 1 from [CompanyStrategyMapping] CSM
					inner join  @StratergyList CSlist
					on CSM.Category = CSlist.ID
					where comp.Id = CSM.CompanyId))
		--and (not exists(select 1 from @SegmentList)
		--or exists(select 1 from CompanyProduct CP
		--			inner join CompanyProductSegmentMap CPSM
		--			on cp.Id=cpsm.CompanyProductId 
		--			inner join @SegmentList list
		--			on list.ID = CPSM.SegmentId
		--			where cp.CompanyId = comp.Id))
		and (not exists(select 1 from @SegmentList)
        or exists(SELECT 1 FROM CompanyEcosystemPresenceESMMap CPSM                   
                    inner join @SegmentList list
                    on list.ID = CPSM.ESMId
                    where CPSM.CompanyId = comp.Id))
		and (@Years is null or @Years = '' 
			or exists(select 1 from CompanyRevenue
						inner join @YearList Ylist
						on Ylist.Year = CompanyRevenue.Year
						and CompanyRevenue.CompanyId = comp.Id))
	)
select cp.CompanyId,
Clist.CompanyName,
Clist.YearOfEstb,
cp.Id as CompanyProductId,
cp.ProductName,
--cp.ImageURL,
'' as ImageURL,
cp.ImageDisplayName,
cp.ActualImageName,
cp.StatusId,
ps.ProductStatusName,
cp.Description,
cp.LaunchDate,
--(select sum(CPV.[Amount]) from [CompanyProductValue] CPV where  CP.id = CPV.CompanyProductId and Year in (select year from @YearList)) As Revenue,
(Select dbo.fun_UnitDeConversion(CPV.ValueConversionId, dbo.fun_CurrencyConversion(CPV.Amount, CurrencyId, @USDCurrencyId, '01/01/'+cast([Year] as varchar))) 
 --From [CompanyProductValue] CPV Where  CP.id = CPV.CompanyProductId and Year in (select year from @YearList)) As Revenue,
 From [CompanyProductValue] CPV Where  CP.id = CPV.CompanyProductId ) As Revenue,
 (Select top 1 ValueConversion.ValueName From [CompanyProductValue] INNER JOIN ValueConversion ON [CompanyProductValue].ValueConversionId=ValueConversion.Id   Where  CP.id = CompanyProductValue.CompanyProductId) as ValueName,

--'' As TargetedGeography,
 (SELECT string_agg(Country.CountryName, ', ')
 FROM Country 
 where exists (select 1 from CompanyProductCountryMap  CPCM
			where Country.Id=CPCM.CountryId
			and CPCM.CompanyProductId = CP.Id)) as ProductCountries,
(SELECT  STRING_AGG(pc.ProductCategoryName,', ') FROM ProductCategory pc 
where exists (select 1 from CompanyProductCategoryMap cpm 
			 where pc.Id = cpm.ProductCategoryId 
			 and cpm.CompanyProductId = CP.id )) As ProductCategories,
cp.Patents

--,(select string_agg(Country.CountryName, ',') from [dbo].[CompanyProductCountryMap] CPCM
--inner join Country
--on CPCM.CountryId = Country.Id
--where CPCM.CompanyProductId = CP.Id) as ProductCountries

from Clist
inner join  [dbo].[CompanyProduct] CP
on Clist.CompanyId = CP.CompanyId
inner join dbo.ProductStatus PS
on cp.StatusId = ps.Id
where CP.IsDeleted =0;

end