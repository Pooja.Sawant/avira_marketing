﻿
CREATE PROCEDURE [dbo].[SFab_mmmSaveVariablesToCustomize]
(
	@UserId	uniqueidentifier,
	@MarketID uniqueidentifier,
	@ProjectId uniqueidentifier,
	@JsonVariables NVARCHAR(MAX)
)
As   

/*================================================================================    
Procedure Name: SFab_mmmSaveVariablesToCustomize   
Author: Nagi   
Create date: 08-June-2020
Description: Save the edit settings of MMM
Change History    
Date  Developer  Reason  
__________ ____________ ______________________________________________________    
02-June-20  Nagi   Initial Version    
================================================================================*/    
BEGIN  


--DECLARE @JsonVariables1 NVARCHAR(MAX) = N'[ 
--	{  
--      "SectionId":"aa0", Type = "AAAA"
--    },
--	{  
--      "SectionId":"aa1" ,  Type = "BB1"   
--    }
--	{  
--      "SectionId":"aa2" ,  Type = "CCCC"
--    }
--]'  
  


-------------
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      

declare @ErrorMsg NVARCHAR(2048);
Declare @NewId uniqueidentifier;

declare 

	@SectionName varchar(255),
	@PageLevel uniqueidentifier,
	@Type varchar(255),
	@TypeVariableValue Bit 

BEGIN TRY 


		BEGIN


			DELETE FROM MMM_VariableSelection  WHERE
				EXISTS ( SELECT 1  FROM ( select SelectionID, Type from OPENJSON ( @JsonVariables ) with ( SelectionID uniqueidentifier '$.SelectionID', Type Varchar(255) '$.Type' ))	S 
					WHERE S.TYPE = MMM_VariableSelection.SelectionType ) and MMM_VariableSelection.userid = @UserId  and MMM_VariableSelection.ProjectId = @ProjectId  and MMM_VariableSelection.MarketID = @MarketID;
		    
			merge MMM_VariableSelection as T  
			using (select  SelectionID, Type from OPENJSON ( @JsonVariables )
						with 
						(   
							SelectionID			uniqueidentifier '$.SelectionID',
							Type				Varchar(255) '$.Type'
						)) S 
			on (T.SelectionID = S.SelectionID and T.userid = @UserId and T.ProjectId = @ProjectId and  T.MarketID = @MarketID)  --and T.Type = S.Type
			when NOT MATCHED THEN
			INSERT (ID, UserId, ProjectId, MarketID, SelectionID, CreatedOn,  UserCreatedByid, SelectionType)  VALUES (NewID(), @UserId, @ProjectId, @MarketID, S.SelectionID, GetDate (), @UserId, S.Type);

		End


END TRY

BEGIN CATCH      
	exec SApp_LogErrorInfo
	set @ErrorMsg = 'Error while save the edit settings, please contact Admin for more details.';    
	SELECT StoredProcedureName ='SFab_mmmSaveVariablesToCustomize',Message =@ErrorMsg,Success = 0;  
END CATCH   
		
END