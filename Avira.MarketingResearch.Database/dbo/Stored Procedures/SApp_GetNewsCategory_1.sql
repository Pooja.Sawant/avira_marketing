﻿    
    
CREATE PROCEDURE [dbo].[SApp_GetNewsCategory]    
 (    
  @includeDeleted bit = 0    
 )    
 AS    
/*================================================================================    
Procedure Name: [SApp_GetNewsCategory]    
Author: Gopi    
Create date: 05/02/2019    
Description: Get list of NewsCategory from table     
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
05/02/2019 Gopi   Initial Version    
================================================================================*/    
    
BEGIN    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED    
    
 SELECT [NewsCategory].[Id],    
  [NewsCategory].[NewsCategoryName],    
  [NewsCategory].[Description],    
  [NewsCategory].[IsDeleted]    
FROM [dbo].[NewsCategory]    
where ([NewsCategory].IsDeleted = 0)     
 END