﻿
CREATE PROCEDURE [dbo].[SFabChart_GetMarketImpactByCustomMarket]  
(  
	 @ProjectId UniqueIdentifier , 
	 @MakeMyMarketId UniqueIdentifier = null,
	 @SegmentId UniqueIdentifier = null,
	 @SubSegmentId UniqueIdentifier = null,  
	 @AvirauserId UniqueIdentifier = null
)  
as  
/*================================================================================  
Procedure Name: [SFabChart_GetMarketImpactByCustomMarket]  
Author: Harshal  
Create date: 05/06/2020  
Description: Get data for market impact chart based on Custom Market
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
05/06/2020 Harshal      Initial Version  
================================================================================*/  
BEGIN  
	SET NOCOUNT ON;  
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	if(@SegmentId is null or @SegmentId='00000000-0000-0000-0000-000000000000')
	Begin
	  SELECT DISTINCT TOP (1) @SegmentId=MarketValuesummary.SegmentId FROM 
	  MarketValuesummary 
	  where Projectid = @ProjectId;
	end
	if(@MakeMyMarketId='00000000-0000-0000-0000-000000000000')
	SET @MakeMyMarketId=null;
	if(@SubSegmentId='00000000-0000-0000-0000-000000000000')
	SET @SubSegmentId=null;

	declare @ProjectApprovedStatusId uniqueidentifier
	select @ProjectApprovedStatusId = ID from ProjectStatus
	where ProjectStatusName = 'Completed'

	;with MV as (
	select Year,
	sum(dbo.fun_UnitDeConversion(MarketValueSummary.ValueConversionId,value)) as val,
	ValueConversion.ValueName 
	from MarketValueSummary
	LEFT JOIN  ValueConversion ON MarketValuesummary.ValueConversionId = ValueConversion.Id 
	where MarketValueSummary.ProjectId = @ProjectID
	AND MarketValuesummary.SegmentId=@SegmentId
	AND (@SubSegmentId is null OR MarketValuesummary.SubSegmentId=@SubSegmentId)
	and exists (select 1 from Project 
				where Project.ProjectStatusID = @ProjectApprovedStatusId
				and project.ID = MarketValueSummary.ProjectId)
	--AND
	--(MarketValueSummary.Year = (SELECT TOP 1 BaseYear FROM BaseYearMaster))
	group by Year,ValueConversion.ValueName)
--select * from mv

	Select MV.Year, 
	MV.Val As IndustryViewpoint, 
	MakeMyMarket_MarketSizes.Value as MyViewPoint,
	MV.ValueName as ValueConversionName
	From MV 
	Left Join MakeMyMarket_MarketSizes ON MV.Year =MakeMyMarket_MarketSizes.Year
	AND(@MakeMyMarketId is null OR MakeMyMarket_MarketSizes.MakeMyMarketID=@MakeMyMarketId)
	Order By MV.Year 
End