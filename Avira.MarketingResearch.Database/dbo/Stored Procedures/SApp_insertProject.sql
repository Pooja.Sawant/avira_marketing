﻿
CREATE PROCEDURE [dbo].[SApp_insertProject]
(@ProjectID		UNIQUEIDENTIFIER=null,
@ProjectCode	NVARCHAR(50)=null,
@ProjectName	NVARCHAR(256),
@Remark			NVARCHAR(256)=null,
@StartDate		DATETIME=null,
@EndDate		DATETIME=null,
@MinTrends		INT,
@MinCompanyProfiles INT,
@MinPrimaries		INT,
@ProjectStatusID	UNIQUEIDENTIFIER=null,
--@IsDeleted		BIT=0,
@AuthorUserID UNIQUEIDENTIFIER
)
as
/*================================================================================
Procedure Name: SApp_insertProject
Author: Sharath
Create date: 04/11/2019
Description: Insert or updare a record in Project table by ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
04/11/2019	Sharath			Initial Version
__________	____________	______________________________________________________
04/25/2019	Pooja			Project status change from draft to To Be Launched
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ErrorMsg NVARCHAR(2048);

IF exists(select 1 from [dbo].[Project] 
		 where ((@ProjectID is null or [Id] !=  @ProjectID)
		 and  ProjectName = @ProjectName AND IsDeleted = 0))
begin
set @ErrorMsg = 'Project with Name "'+ @ProjectName + '" already exists.';
SELECT StoredProcedureName ='SApp_insertProject',Message =@ErrorMsg,Success = 0;  
RETURN;  
--return(1)
end
--IF exists(select 1 from [dbo].[Project] 
--		 where ((@ProjectID is null or [Id] !=  @ProjectID)
--		 and  ProjectName = @ProjectCode AND IsDeleted = 0))
--begin
--set @ErrorMsg = 'Project with Code "'+ @ProjectCode + '" already exists.';
----THROW 50000,@ErrorMsg,1;
--SELECT StoredProcedureName ='SApp_insertProject',Message =@ErrorMsg,Success = 0;  
--RETURN;  
----return(1)
--end
--Default to draft status
if @ProjectStatusID is null
set @ProjectStatusID=(select Id from ProjectStatus
where ProjectStatusName = 'To Be Launched')

--End of Validations
BEGIN TRY

INSERT INTO [dbo].[Project]
           ([ID]
           ,[ProjectCode]
           ,[ProjectName]
           ,[Remark]
           ,[StartDate]
           ,[EndDate]
           ,[MinTrends]
           ,[MinCompanyProfiles]
           ,[MinPrimaries]
           ,[Completion]
           ,[Efficiency]
           ,[Approved]
           ,[ProjectStatusID]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
	values(@ProjectID, 
			@ProjectCode,
			@ProjectName, 
			@Remark, 
			@StartDate,
			@EndDate,
			@MinTrends,
			@MinCompanyProfiles,
			@MinPrimaries,
			0,--as Completion
			0,--as [Efficiency]
			0,--as [Approved]
			@ProjectStatusID,
			GETDATE(), --as [CreatedOn]
			@AuthorUserID, 
			0); --as [IsDeleted]

SELECT StoredProcedureName ='SApp_insertProject',Message =@ErrorMsg,Success = 1;     

END TRY
BEGIN CATCH
    -- Execute the error retrieval routine.
	DECLARE @ErrorNumber	int;
	DECLARE @ErrorSeverity	int;
	DECLARE @ErrorProcedure	varchar(100);
	DECLARE @ErrorLine	int;
	DECLARE @ErrorMessage	varchar(500);

	 SELECT @ErrorNumber = ERROR_NUMBER(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorProcedure = ERROR_PROCEDURE(),
        @ErrorLine = ERROR_LINE(),
        @ErrorMessage = ERROR_MESSAGE()

	insert into dbo.Errorlog(ErrorNumber,
							ErrorSeverity,
							ErrorProcedure,
							ErrorLine,
							ErrorMessage,
							ErrorDate)
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());

 set @ErrorMsg = 'Error while creating a new Project, please contact Admin for more details.';    
--THROW 50000,@ErrorMsg,1;      
--return(1)  
SELECT Number = @ErrorNumber, 
		Severity =@ErrorSeverity,
		StoredProcedureName =@ErrorProcedure,
		LineNumber= @ErrorLine,
		Message =@ErrorMsg,
		Success = 0;     
END CATCH
end