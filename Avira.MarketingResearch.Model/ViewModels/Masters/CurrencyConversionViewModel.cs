﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.Masters
{
    public class CurrencyConversionViewModel
    {
        public string CurrencyCode { get; set; }
        public Guid CurrencyId { get; set; }
        public Int16 ConversionRangeType { get; set; }
        public decimal ConversionRate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Year { get; set; }
        public string Quarter { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public Guid Id { get; set; }
        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "Currency";
            }
        }
    }
    public class CurrencyConversionInsertDbViewModel
    {
        public Guid CurrencyConversionId { get; set; }
        public string CurrencyCode { get; set; }
        public Guid CurrencyId { get; set; }
        [Display(Name = "Currency Name")]
        public string CurrencyName { get; set; }
        public Int16 ConversionRangeType { get; set; }
        public decimal ConversionRate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? Year { get; set; }
        public string Quarter { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public int TotalItems { get; set; }
    }
    public class CurrencyConversionUpdateDbViewModel
    {
        public string CurrencyCode { get; set; }
        public Guid CurrencyId { get; set; }
        [Display(Name = "Currency Name")]
        public string CurrencyName { get; set; }
        public Int16 ConversionRangeType { get; set; }
        public decimal ConversionRate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Year { get; set; }
        public string Quarter { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        [Required]
        public Guid Id { get; set; }
    }
}
