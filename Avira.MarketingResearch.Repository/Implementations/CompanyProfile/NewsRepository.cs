﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.CompanyProfile
{
    public class NewsRepository : INewsRepository
    {
        protected IDbHelper _dbHelper;
        public NewsRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public IEnumerable<NewsViewModel> GetAll(Guid companyId, Guid projectId)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<NewsViewModel>(NewsQueries.StoreProcSelectById
                            , new { @CompanyID = companyId,@ProjectID= projectId, @includeDeleted = false }
                            , null, true, null, CommandType.StoredProcedure);
        }

        public IEnumerable<NewsImpactDirectionViewModel> GetAllImpactDirection()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<NewsImpactDirectionViewModel>(NewsQueries.StoreProcSelectAllImpactDirection
                            , null
                            , null, true, null, CommandType.StoredProcedure);
        }

        public IEnumerable<NewsImportanceViewModel> GetAllImportance()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<NewsImportanceViewModel>(NewsQueries.StoreProcSelectAllImportance
                            , null
                            , null, true, null, CommandType.StoredProcedure);
        }

        public IEnumerable<NewsCategoryViewModel> GetAllNewsCategory()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<NewsCategoryViewModel>(NewsQueries.StoreProcSelectAllNewsCategory
                            , null
                            , null, true, null, CommandType.StoredProcedure);
        }

        public Result<SpTransactionMessage> InsertUpdate(MainNewsInsertUpdateDbViewModel newsUpdateDbViewModel)
        {
            DataTable newsTable = new DataTable();
            newsTable.Columns.Add("ID", typeof(Guid));
            newsTable.Columns.Add("Date", typeof(DateTime));
            newsTable.Columns.Add("News", typeof(string));
            newsTable.Columns.Add("NewsCategoryId", typeof(Guid));
            newsTable.Columns.Add("ImportanceId", typeof(Guid));
            newsTable.Columns.Add("ImpactDirectionId", typeof(Guid));
            newsTable.Columns.Add("OtherParticipants", typeof(string));
            newsTable.Columns.Add("Remarks", typeof(string));
            if (newsUpdateDbViewModel.NewsInsertUpdate != null & newsUpdateDbViewModel.NewsInsertUpdate.Count > 0)
            {
                foreach (var item in newsUpdateDbViewModel.NewsInsertUpdate)
                {
                    if(item.Id==Guid.Empty)
                    {
                        item.Id = Guid.NewGuid();
                    }
                    newsTable.Rows.Add(new Object[] { item.Id, item.Date, item.News, item.NewsCategoryId, item.ImportanceId, item.ImpactDirectionId, item.OtherParticipants, item.Remarks });
                }
            }


            var spTransactionMessage = _dbHelper.ExecuteQuery(NewsQueries.StoreProcInsertUpdate,
               new
               {
                   @CompanyId = newsUpdateDbViewModel.CompanyId,
                   @ProjectId = newsUpdateDbViewModel.ProjectId,
                   @NewsList = newsTable.AsTableValuedParameter("dbo.udtGUIDNewsMultiValue"),
                   @UserCreatedById = newsUpdateDbViewModel.UserCreatedById
               }
                    , CommandType.StoredProcedure);
            return spTransactionMessage;
        }
    }
}
