﻿

CREATE procedure [dbo].[SFab_GetDownloadHeaderFooterData]
(	
	@ProjectId uniqueidentifier,
	@ModuleName nvarchar(256)
)      
as      
/*================================================================================      
Procedure Name: [SFab_GetDownloadHeaderFooterData] 
Author: Nitin      
Create date: 14-Apr-2020      
Description: Get DownloadExportData      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
14-Apr-2020 Nitin   Initial Version    
13-May-2020 Harshal  Restruecturd SP As per requirement
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 
declare @ModuleId uniqueidentifier

SELECT @ModuleId=Id FROM ModuleType Where ModuleName=@ModuleName
      
	Select D.ProjectID, D.ModuleTypeID, M.ModuleName, D.Header, D.Footer, 
	       D.ReportTitle, D.CoverPageImageLink, D.CoverPageImageName,
	       D.Overview, D.KeyPoints, D.Methodology,
		   D.MethodologyImageLink, D.MethodologyImageName, 
	       D.ContactUsAnalystName, D.ContactUsOfficeContactNo, 
		   D.ContactUsAnalystEmailId
	 From DownloadsExportData D 
		Inner Join ModuleType M ON D.ModuleTypeId = M.Id
        Where D.ProjectId=@ProjectId and ModuleTypeId=@ModuleId

end