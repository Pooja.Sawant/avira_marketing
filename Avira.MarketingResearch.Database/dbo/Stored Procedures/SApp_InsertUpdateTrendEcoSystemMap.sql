﻿
CREATE procedure [dbo].[SApp_InsertUpdateTrendEcoSystemMap]      
(
@TrendId uniqueidentifier,   
@TrendEcoSytemMap dbo.[udtGUIDGUIDMapValue] readonly,
@AviraUserId uniqueidentifier
)      
as      
/*================================================================================      
Procedure Name: SApp_SApp_InsertUpdateTrendEcoSystemMap      
Author: Harshal      
Create date: 04/22/2019      
Description: Insert a record into TrendEcoSystemMap table      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
04/22/2019 Harshal   Initial Version    
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)  
    
--Validations      
--IF exists(select 1 from [dbo].[TrendMarketMap]       
--   where TrendId = @TrendId AND MarketId = @MarketId AND IsDeleted = 0)      
--begin       
--set @ErrorMsg = 'Market with Id "'+ CONVERT (nvarchar(30), @MarketId) + '" already exists.';    
--SELECT StoredProcedureName ='SApp_InsertTrendMarketMap',Message =@ErrorMsg,Success = 0;    
--RETURN;    
--end      
--End of Validations      
BEGIN TRY         
--mark records as delete which are not in list
--update [dbo].[TrendEcoSystemMap]
--set IsDeleted = 1,
--UserDeletedById = @AviraUserId,
--DeletedOn = GETDATE()
--from [dbo].[TrendEcoSystemMap]
--where [TrendEcoSystemMap].TrendId = @TrendId
--and not exists (select 1 from @TrendEcoSytemMap map where map.id = [TrendEcoSystemMap].EcoSystemId)

 --Update records for change
update [dbo].[TrendEcoSystemMap]
set Impact = map.mapid,
EndUser = map.bitvalue,
IsDeleted = CASE WHEN map.mapid IS NULL THEN 1 ELSE IsDeleted END,
ModifiedOn = GETDATE(),
UserModifiedById = @AviraUserId
from [dbo].[TrendEcoSystemMap]
inner join @TrendEcoSytemMap map 
on map.id = [TrendEcoSystemMap].EcoSystemId
where [TrendEcoSystemMap].TrendId = @TrendId;

--Insert new records
insert into [dbo].[TrendEcoSystemMap]([Id],      
        [TrendId],      
        [EcoSystemId], 
		[Impact],
		[EndUser],
        [IsDeleted],      
        [UserCreatedById],      
        [CreatedOn])      
 select NEWID(),
   @TrendId,       
   map.ID as [MarketId],
   map.mapid as [Impact],     
   map.bitvalue,     
   0,      
   @AviraUserId,       
   GETDATE()
 from @TrendEcoSytemMap map
 where not exists (select 1 from [dbo].[TrendEcoSystemMap] map1
					where map1.TrendId = @TrendId
					and map.ID = map1.EcoSystemId);       
     
SELECT StoredProcedureName ='SApp_InsertUpdateTrendEcoSystemMap',Message =@ErrorMsg,Success = 1;       
    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);      
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());      
    
 set @ErrorMsg = 'Error while insert a new TrendEcoSytemMap, please contact Admin for more details.';    
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
       
END CATCH      
      
      
end