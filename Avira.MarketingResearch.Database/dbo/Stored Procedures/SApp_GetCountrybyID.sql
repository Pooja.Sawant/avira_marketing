﻿CREATE procedure [dbo].[SApp_GetCountrybyID]
(@CountryID uniqueidentifier,
@includeDeleted bit = 0)
as
/*================================================================================
Procedure Name: SApp_GetCountrybyID
Author: Sharath
Create date: 02/11/2019
Description: Get list from Country table by Name or ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
02/11/2019	Sharath			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if @includeDeleted is null set @includeDeleted = 0

SELECT [Id],
	[CountryCode],
      [CountryName]
FROM [dbo].[Country]
where (@CountryID is null or ID = @CountryID)
and (@includeDeleted = 1 or IsDeleted = 0)

end