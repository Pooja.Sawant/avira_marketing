﻿CREATE PROCEDURE [dbo].[SFab_InsertUpdateDecisionAlternativeTask]  
(  
@DecisionAlternativeTaskId uniqueidentifier,
@DecisionId uniqueidentifier,
@DecisionAlternativeId uniqueidentifier,
@Description nvarchar(1024),
@ResourceAssigned nvarchar(899),
@StatusId uniqueidentifier,
@DueDate datetime2(7),
@UserId uniqueidentifier  
)  
As     
  
/*================================================================================      
Procedure Name: SFab_InsertDecisionAlternativeTask     
Author: Praveen      
Create date: 07-Jan-2020  
Description: Add upadate new task of the alternative
Change History      
Date  Developer  Reason    
__________ ____________ ______________________________________________________      
07-Jan-2020  Praveen   Initial Version 
30-Jan-2020  Harshal   Added QueryStatement
================================================================================*/      
BEGIN      

------------------  
 --Required Tables: Decision, DecisionAlternativeTask and LookupStatus
 --Notes: N/A  
 --Case 1: If @DecisionAlternativeTaskId is null then add new task
 --Case 2: If @DecisionAlternativeTaskId is not null then edit all fields of this perticular task 
 --Expected Result :SELECT StoredProcedureName ='SFab_InsertDecisionAlternativeTask',Message = @ErrorMsg,Success = 1/0;  
Declare @ErrorMsg NVARCHAR(2048)
Declare @DecisionStatusDecisionMadeId uniqueidentifier
Select @DecisionStatusDecisionMadeId = ID From LookupStatus
	Where StatusName = 'Decision made' And StatusType = 'DMDecisionStatus'
Declare @DecisionStatusDecisionOngoingId uniqueidentifier
Select @DecisionStatusDecisionOngoingId = ID From LookupStatus
	Where StatusName = 'Ongoing' And StatusType = 'DMDecisionStatus'	
	   
BEGIN TRY
If Exists (Select ID from Decision Where Id = @DecisionId and UserId= @UserID and StatusId=@DecisionStatusDecisionMadeId)
        Begin
        	set @ErrorMsg = 'Can not add/edit any new details since this decision has been made.';   
        	SELECT StoredProcedureName ='SFab_InsertDecisionAlternativeTask','Message' =@ErrorMsg,'Success' = 0;  
        end
else If NOT Exists (Select ID from Decision Where Id = @DecisionId and UserId= @UserID and StatusId=@DecisionStatusDecisionOngoingId)
      Begin
	      set @ErrorMsg = 'Please change decision status to Ongoing in order to add/edit details.';   
          SELECT StoredProcedureName ='SFab_InsertDecisionAlternativeTask','Message' =@ErrorMsg,'Success' = 0;  
      end
else 
		If Exists(Select Id From DecisionAlternativeTask Where Id = @DecisionAlternativeTaskId AND DecisionAlternativeId = @DecisionAlternativeId)
       Begin
				UPDATE DecisionAlternativeTask SET
				Description=@Description,
				ResourceAssigned=@ResourceAssigned,
				StatusId=@StatusId,
				DueDate=@DueDate,
				ModifiedOn=GETDATE(),
				UserModifiedById=@UserId,
				StatusModifiedOn= case when @StatusId=statusid then StatusModifiedOn else getdate() end
				WHERE Id = @DecisionAlternativeTaskId 
				AND DecisionAlternativeId = @DecisionAlternativeId
				AND DecisionId=@DecisionId
      End
   Else
		Begin
		        INSERT INTO DecisionAlternativeTask ([Id],[DecisionId],[DecisionAlternativeId],[Description],[ResourceAssigned],[StatusId],
											  [DueDate],[CreatedOn],[UserCreatedById],StatusModifiedOn)
											 VALUES(NEWID(),@DecisionId,@DecisionAlternativeId,@Description,@ResourceAssigned,@StatusId,
											 @DueDate,GETDATE(),@UserId,GETDATE())
		End
		SELECT StoredProcedureName ='SFab_InsertDecisionAlternativeTask',Message =@ErrorMsg,Success = 1;
END TRY
BEGIN CATCH 
        EXEC SApp_LogErrorInfo
    
			set @ErrorMsg = 'Error while inserting Decision alternative task, please contact Admin for more details.';    
			
		SELECT StoredProcedureName ='SFab_InsertDecisionAlterNativeTask', Message = @ErrorMsg, Success = 0;     
          
END CATCH 
END