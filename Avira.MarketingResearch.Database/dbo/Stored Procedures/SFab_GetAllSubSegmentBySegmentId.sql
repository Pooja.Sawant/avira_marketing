﻿

CREATE PROCEDURE [dbo].[SFab_GetAllSubSegmentBySegmentId]
	(
		@includeParentOnly bit = 0,
		@id uniqueidentifier=null,
		@AviraUserID uniqueidentifier = null
	)
	AS
/*================================================================================
Procedure Name: [dbo].[SFab_GetAllSubSegmentBySegmentId]
Author: Pooja Sawant
Create date: 07/01/2019
Description: Get list from sub Segment table by SegmentId
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
07/01/2019	Pooja S			Initial Version
================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	select id,
	SubSegmentName as SegmentName
	from SubSegment
	where SegmentId = @id
	and IsDeleted=0 
	order by SubSegmentName;
END