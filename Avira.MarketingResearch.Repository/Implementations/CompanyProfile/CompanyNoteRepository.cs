﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;

using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;

using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.CompanyProfile
{
   public class CompanyNoteRepository:ICompanyNoteRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "CompanyNote";

        public CompanyNoteRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }
        public CompanyNoteViewModel GetById(Guid companyId, string companyType, Guid? ProjectId)
        {
            CompanyNoteViewModel companyViewModel = new CompanyNoteViewModel();
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<CompanyNoteViewModel>(CompanyNoteQueries.StoreProcSelectAll
                            , new { @CompanyID = companyId,@ProjectId= ProjectId,@CompanyType = companyType }
                            , null, true, null, CommandType.StoredProcedure).FirstOrDefault();

        }
        public Result<SpTransactionMessage> Insert(CompanyNoteViewModel companyNoteInsertUpdateDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(CompanyNoteQueries.StoreProcInsert, new
            {
                @Id = companyNoteInsertUpdateDbViewModel.Id,
                @CompanyId = companyNoteInsertUpdateDbViewModel.CompanyId,
                @ProjectId = companyNoteInsertUpdateDbViewModel.ProjectId,
                @CompanyType = companyNoteInsertUpdateDbViewModel.CompanyType,
                @AuthorRemark = companyNoteInsertUpdateDbViewModel.AuthorRemark,
                @ApproverRemark = companyNoteInsertUpdateDbViewModel.ApproverRemark,
                @CompanyStatusName = companyNoteInsertUpdateDbViewModel.CompanyStatusName,
                @AviraUserId = companyNoteInsertUpdateDbViewModel.UserCreatedById
            }, CommandType.StoredProcedure);
            return spTransactionMessage;
        }
    }
}
