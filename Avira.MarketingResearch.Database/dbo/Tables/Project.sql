﻿CREATE TABLE [dbo].[Project](
    [ID] [uniqueidentifier] NOT NULL,
    [ProjectCode] [nvarchar](50) NULL,
    [ProjectName] [nvarchar](256) NULL,
    [Remark] [nvarchar](500) NULL,
    [StartDate] [datetime] NULL,
    [EndDate] [datetime] NULL,
    [MinTrends] [int] NULL,
    [MinCompanyProfiles] [int] NULL,
    [MinPrimaries] [int] NULL,
    [Completion] [decimal](18, 0) NULL,
    [Efficiency] [decimal](18, 0) NULL,
    [Approved] [decimal](18, 0) NULL,
    [ActualHoursSpent] [int] NULL,
    [ProjectStatusID] [uniqueidentifier] NULL,
    [CreatedOn] [datetime2](7) NULL,
    [UserCreatedById] [uniqueidentifier] NULL,
    [ModifiedOn] [datetime2](7) NULL,
    [UserModifiedById] [uniqueidentifier] NULL,
    [IsDeleted] [bit] NULL,
    [DeletedOn] [datetime2](7) NULL,
    [UserDeletedById] [uniqueidentifier] NULL,
 CONSTRAINT [PK_dbo]].[project] PRIMARY KEY CLUSTERED 
(
    [ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Project] ADD  CONSTRAINT [DF_Project_Complition]  DEFAULT ((0)) FOR [Completion]
GO
ALTER TABLE [dbo].[Project] ADD  CONSTRAINT [DF_Project_Efficiency]  DEFAULT ((0)) FOR [Efficiency]
GO
ALTER TABLE [dbo].[Project] ADD  CONSTRAINT [DF_Project_Approved]  DEFAULT ((0)) FOR [Approved]
GO
ALTER TABLE [dbo].[Project] ADD  CONSTRAINT [DF_Project_ActualHoursSpent]  DEFAULT ((0)) FOR [ActualHoursSpent]
GO
ALTER TABLE [dbo].[Project]  WITH CHECK ADD  CONSTRAINT [FK_Project_ProjectStatus] FOREIGN KEY([ProjectStatusID])
REFERENCES [dbo].[ProjectStatus] ([Id])
GO
ALTER TABLE [dbo].[Project] CHECK CONSTRAINT [FK_Project_ProjectStatus]
GO