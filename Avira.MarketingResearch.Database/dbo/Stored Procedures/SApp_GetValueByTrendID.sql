﻿/****** Object:  StoredProcedure [dbo].[SApp_GetValueByTrendID]    Script Date: 23-04-2019 16:55:56 ******/

      
 CREATE PROCEDURE [dbo].[SApp_GetValueByTrendID]            
 (@TrendID uniqueidentifier = null,    
 @includeDeleted bit = 0)            
 AS            
/*================================================================================            
Procedure Name: SApp_GetValuebyTrendID        
Author: Swami            
Create date: 04/25/2019            
Description: Get list from Value table by TrendID            
Change History            
Date  Developer  Reason            
__________ ____________ ______________________________________________________            
04/15/2019 Swami   Initial Version 
================================================================================*/           
            
BEGIN            
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED         
declare @Year int
SELECT @Year = YEAR(CreatedOn)
FROM Trend 
WHERE Trend.Id = @TrendID

;With YearList([Year]) as
(
    Select @Year as [Year]
        union all
    Select [Year] + 1
        from YearList
        where [Year] < @Year +20
),
TagYearMap as (
select 
case when seq = 1 then 0
else 1 + 3 *(seq-2) end + @Year as yearmin,
case when seq = 1 then 0
else  3 *(seq-1) end  + @Year as yearmax,
* from TimeTag)
,TT as (
select YearList.*, TagYearMap.*
from TagYearMap
inner join YearList
on YearList.[Year] between TagYearMap.yearmin and TagYearMap.yearmax
)

--@TrendID as TrendID,          
SELECT Trend.Id,
	   TTT.EndUser,
	   Trend.[ImportanceId],
	   I.ImportanceName,
	   I.[Description] AS ImportanceDescription,
	   Trend.[ImpactDirectionId],
	   ID.ImpactDirectionName,
	   ID.[Description] AS ImpactDirectionDescription,
	   Trend.[TrendValue],
	   TT.Id AS TimeTagId,
	   TT.TimeTagName,
	   TT.[Description] AS TimeTagDescription,
	   dbo.fun_UnitDeConversion(TVM.ValueConversionId, TVM.Amount) as Amount, 
	   isnull(TVM.[Year],tt.[Year]) as [Year], 
	   TVM.Rationale,
	   VC.Id AS ValueConversionId,
	   VC.ValueName,
	   VC.Conversion,
	   IT.Id AS ImpactTypeId,
	   IT.ImpactTypeName,
	   IT.[Description] AS ImpactDescription,
	   TVM.IsDeleted     
	       
FROM TT
INNER JOIN TrendTimeTag TTT on tt.Id = TTT.TimeTagId AND ttt.IsDeleted = 0
INNER JOIN ImpactType IT ON TTT.Impact = IT.Id
LEFT JOIN TrendValueMap TVM
INNER JOIN ValueConversion VC ON TVM.ValueConversionId = VC.Id
INNER JOIN Trend ON TVM.TrendId = Trend.Id
LEFT JOIN Importance I ON Trend.ImportanceId = I.Id
LEFT JOIN ImpactDirection ID 

ON Trend.ImpactDirectionId = ID.Id
ON TVM.TimeTagId = TTT.TimeTagId 
AND TVM.TrendId = TTT.TrendId
AND TVM.[Year] = TT.[Year]
AND (@includeDeleted = 1 OR TVM.isDeleted = 0)
WHERE TTT.TrendId = @TrendID
order by tt.[Year];        
END