﻿using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.ImportExport
{
    public class QualitativeAnalysisImportExportRepository : IQualitativeAnalysisImportExportRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "QualitativeAnalysisImportExport";

        public QualitativeAnalysisImportExportRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }
        
        public IEnumerable<ImportCommonResponseModel> Create(QualitativeAnalysisImportModel viewModel)
        {
            
            var spTransactionMessage = _dbHelper.ExecuteQuery(ImportExportQueries.StoreProcInsertQualitativeAnalysis, new
            {
                @TemplateName = viewModel.TemplateName,
                @ImportFileName = viewModel.ImportFileName,

                @JsonQualitativeNews = viewModel.JsonNewsList,
                @JsonQualitativeAnalysis = viewModel.JsonAnalysisList,
                @JsonQualitativeDescription = viewModel.JsonDescriptionList,
                @JsonQualitativeQuote = viewModel.JsonQuoteList,
                @JsonQualitativeSegments = viewModel.JsonSegmentsList,
                @JsonQualitativeRegions = viewModel.JsonRegionList,
                @JsonESMSegments= viewModel.JsonESMSegmentList,
                @JsonESMAnalysis = viewModel.JsonESMAnalysisList,
                @UserCreatedById = viewModel.UserCreatedById
            }, CommandType.StoredProcedure);

            IEnumerable<ImportCommonResponseModel> spTransactionMessage1 = new List<ImportCommonResponseModel>();
            if (spTransactionMessage.IsSuccess == true)
            {
                //var spTransactionMessage1 = _dbHelper.ExecuteQuery(ImportExportQueries.StoreProcUpdateMarketSizing1, null, CommandType.StoredProcedure);
                var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
                var spTransactionMain = dapper.Query<ImportCommonResponseModel>(ImportExportQueries.StoreProcUpdateQualitativeAnalysis
                            , null
                            , null, true, null, CommandType.StoredProcedure);

                if (spTransactionMessage1 != null)
                {
                    spTransactionMessage1 = spTransactionMain;
                }
            }
            return spTransactionMessage1;
        }


    }
}
