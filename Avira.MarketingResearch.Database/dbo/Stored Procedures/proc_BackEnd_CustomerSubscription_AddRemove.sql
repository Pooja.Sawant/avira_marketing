﻿CREATE PROCEDURE [dbo].[proc_BackEnd_CustomerSubscription_AddRemove]
(
 @ActionType VARCHAR(10),
 @UserName nvarchar(100), 
 @ProjectName nvarchar(512)
)
AS
/*================================================================================
Procedure Name: proc_BackEnd_CustomerSubscription_AddRemove
Author: Praveen
Create date: 09/21/2019
Description: To insert Form from backend
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
09/21/2019	Praveen			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
         
declare @ErrorMsg NVARCHAR(2048);  
BEGIN TRY 

IF NOT EXISTS(SELECT 1 FROM AviraUser where AviraUser.UserName = @UserName AND AviraUser.UserTypeId = 2 AND AviraUser.IsDeleted = 0)
BEGIN	
	SELECT @UserName + ' is invalid user.' AS ErrorMessage		
END
ELSE IF NOT EXISTS(SELECT 1 FROM project WHERE ProjectName = @ProjectName AND IsDeleted = 0)
BEGIN	
	SELECT @ProjectName + ' is invalid project.' AS ErrorMessage	
END
ELSE
BEGIN
		DECLARE @SubscriptionId uniqueidentifier,@SegmentId uniqueidentifier, @UserCreatedById uniqueidentifier
		select @SegmentId = ID from project where ProjectName = @ProjectName

		select @SubscriptionId = SubscriptionId, @UserCreatedById = CustomerSubscriptionMap.UserCreatedById
		from CustomerSubscriptionMap 
		INNER JOIN AviraUser ON AviraUser.CustomerId = CustomerSubscriptionMap.CustomerId
		where AviraUser.UserName = @UserName AND AviraUser.UserTypeId = 2 AND AviraUser.IsDeleted = 0 AND CustomerSubscriptionMap.IsDeleted = 0

		IF(@ActionType = 'Add')
		BEGIN


		IF EXISTS(SELECT 1 FROM [dbo].[SubscriptionSegmentMap] WHERE SubscriptionId = @SubscriptionId AND SegmentId = @SegmentId And IsDeleted = 0)
		BEGIN
			SELECT 'This subscription is already exists.' AS ErrorMessage
		END
		ELSE
		BEGIN
			INSERT INTO [dbo].[SubscriptionSegmentMap]
				   ([Id]
				   ,[SubscriptionId]
				   ,[SegmentType]
				   ,[SegmentId]
				   ,[CreatedOn]
				   ,[UserCreatedById]           
				   ,[IsDeleted]
				   )
			 VALUES
				   (newid()
				   ,@SubscriptionId
				   ,'Project'
				   ,@SegmentId
				   ,getdate()
				   ,@UserCreatedById          
				   ,0 )

				   SELECT 'Customer subscription successfully added.' AS ErrorMessage	
				   END
		END
		ELSE IF(@ActionType = 'Remove')
		BEGIN

		IF NOT EXISTS(SELECT 1 FROM [dbo].[SubscriptionSegmentMap] WHERE SubscriptionId = @SubscriptionId AND SegmentId = @SegmentId And IsDeleted = 0)
		BEGIN
			SELECT 'This subscription is not found to delete.' AS ErrorMessage
		END
		ELSE
		BEGIN
		--delete from SubscriptionSegmentMap where SubscriptionId = @SubscriptionId AND SegmentId = @SegmentId 
			UPDATE SubscriptionSegmentMap SET IsDeleted = 1 WHERE SubscriptionId = @SubscriptionId AND SegmentId = @SegmentId 
			SELECT 'Customer subscription successfully removed.' AS ErrorMessage	
			END
		END
		ELSE
		BEGIN			
			SELECT @ActionType + ' is invalid Action. The Action value should be either Add or Remove.' AS ErrorMessage	
		END
END
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);      
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());   
  
set @ErrorMsg = 'Error while updating project, please contact Admin for more details.'        
   
 SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
END CATCH     
END