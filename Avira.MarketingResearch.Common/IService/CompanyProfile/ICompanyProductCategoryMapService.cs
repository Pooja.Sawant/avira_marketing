﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.CompanyProfile
{
    public interface ICompanyProductCategoryMapService
    {
        IEnumerable<CompanyProductCategoryMapViewModel> GetAll(Guid? Id);
        Result<SpTransactionMessage> InsertUpdate(CompanyProductViewMoel tfc, AviraUser currenUser);
    }
}
