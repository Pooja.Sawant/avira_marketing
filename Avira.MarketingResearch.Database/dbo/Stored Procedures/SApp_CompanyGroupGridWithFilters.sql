﻿CREATE PROCEDURE [dbo].[SApp_CompanyGroupGridWithFilters]
	(@CompanyGroupIDList dbo.udtUniqueIdentifier READONLY,
	@CompanyGroupName nvarchar(256)='',
	@Description nvarchar(256)='',
	@includeDeleted bit = 0,
	@OrdbyByColumnName NVARCHAR(50) ='CreatedDate',
	@SortDirection INT = -1,
	@PageStart INT = 0,
	@PageSize INT = null)
	AS
/*================================================================================
Procedure Name: SApp_CompanyGroupGridWithFilters
Author: Pooja
Create date: 04/03/2019
Description: Get list from CompanyGroup table by Name or ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
04/03/2019	Pooja			Initial Version
================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
if @includeDeleted is null set @includeDeleted = 0;
if len(@CompanyGroupName)>2 set @CompanyGroupName = '%'+ @CompanyGroupName + '%'
else set @CompanyGroupName = ''
if len(@Description)>2 set @Description = '%'+ @Description + '%';
else set @Description = '';
	
	if @PageSize is null or @PageSize =0
		set @PageSize = 10
	begin
	;WITH CTE_TBL AS (

	SELECT [CompanyGroup].[Id],
		[CompanyGroup].[CompanyGroupName],
		[CompanyGroup].[Description],
		[CompanyGroup].[IsDeleted],
		CASE
		WHEN @OrdbyByColumnName = 'CompanyGroupName' THEN [CompanyGroup].CompanyGroupName
		WHEN @OrdbyByColumnName = 'Description' THEN [CompanyGroup].[Description]
		ELSE [CompanyGroup].[CompanyGroupName]
	END AS SortColumn

FROM [dbo].[CompanyGroup]
where (NOT EXISTS (SELECT * FROM @CompanyGroupIDList) or  [CompanyGroup].Id  in (select * from @CompanyGroupIDList))
	AND (@CompanyGroupName = '' or [CompanyGroup].[CompanyGroupName] like @CompanyGroupName)
	AND (@Description = '' or [CompanyGroup].[Description] like @Description)
	AND (@includeDeleted = 1 or [CompanyGroup].IsDeleted = 0)
	)

	SELECT 
		  CTE_TBL.*,
		  tCountResult.TotalItems
		FROM CTE_TBL
		CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult
		ORDER BY 
		  CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,
		  CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC 
		OFFSET @PageStart ROWS
		FETCH NEXT @PageSize ROWS ONLY;
	end
	END