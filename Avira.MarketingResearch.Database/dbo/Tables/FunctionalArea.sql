﻿CREATE TABLE [dbo].[FunctionalArea] (
    [Id]                 UNIQUEIDENTIFIER NOT NULL,
    [FunctionalAreaName] NVARCHAR (256)   CONSTRAINT [DF_Table_1_ImportanceName] DEFAULT ('') NOT NULL,
    [Description]        NVARCHAR (256)   NULL,
    [CreatedOn]          DATETIME2 (7)    NULL,
    [UserCreatedById]    UNIQUEIDENTIFIER NULL,
    [ModifiedOn]         DATETIME2 (7)    NULL,
    [UserModifiedById]   UNIQUEIDENTIFIER NULL,
    [IsDeleted]          BIT              CONSTRAINT [DF_FunctionalArea_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]          DATETIME2 (7)    NULL,
    [UserDeletedById]    UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_FunctionalArea] PRIMARY KEY CLUSTERED ([Id] ASC)
);

