﻿using Avira.MarketingResearch.Common.IService.Masters;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces.Masters;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.Masters
{
    public class StatusService : IStatusService
    {
        private readonly IStatusRepository _iStatusRepository;

        public StatusService(IStatusRepository iStatusRepository)
        {
            _iStatusRepository = iStatusRepository;
        }
        public IEnumerable<StatusViewModel> GetAll(string statusType)
        {
            return _iStatusRepository.GetAll(statusType);
        }

    }
}
