﻿CREATE TABLE [dbo].[CompanyStage] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [CompanyStageName] NVARCHAR (256)   CONSTRAINT [DF_CompanyStage_CompanyStageName] DEFAULT ('') NOT NULL,
    [Description]      NVARCHAR (256)   NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_CompanyStage_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CompanyStage] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CompanyStage_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyStage_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id])
);

