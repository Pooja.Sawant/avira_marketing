﻿
CREATE procedure [dbo].[SApp_InsertUpdateTrendRegionMap]      
(
@TrendId uniqueidentifier,   
@TrendRegionmap dbo.[udtGUIDGUIDMapValue] readonly,
@AviraUserId uniqueidentifier
)      
as      
/*================================================================================      
Procedure Name: SApp_InsertTrendRegionMap      
Author: Swami      
Create date: 04/18/2019      
Description: Insert a record into TrendRegionMap table      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
04/18/2019 Swami   Initial Version    
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)  
    
--Validations      
--IF exists(select 1 from [dbo].[TrendRegionMap]       
--   where TrendId = @TrendId AND RegionId = @RegionId AND IsDeleted = 0)      
--begin       
--set @ErrorMsg = 'Region with Id "'+ CONVERT (nvarchar(30), @RegionId) + '" already exists.';    
--SELECT StoredProcedureName ='SApp_InsertTrendRegionMap',Message =@ErrorMsg,Success = 0;    
--RETURN;    
--end      
--End of Validations      
BEGIN TRY         
--mark records as delete which are not in list
update [dbo].[TrendRegionMap]
set IsDeleted = 1,
UserDeletedById = @AviraUserId,
DeletedOn = GETDATE()
from [dbo].[TrendRegionMap]
where [TrendRegionMap].TrendId = @TrendId
and IsDeleted = 0
and not exists (select 1 from @TrendRegionmap map where map.id = [TrendRegionMap].RegionId)

 --Update records for change
update [dbo].[TrendRegionMap]
set Impact = map.mapid,
EndUser = map.bitvalue,
ModifiedOn = GETDATE(),
UserModifiedById = @AviraUserId,
IsDeleted = 0
from [dbo].[TrendRegionMap]
inner join @TrendRegionmap map 
on map.id = [TrendRegionMap].RegionId
where [TrendRegionMap].TrendId = @TrendId;

--Insert new records
insert into [dbo].[TrendRegionMap]([Id],      
        [TrendId],      
        [RegionId], 
		[Impact],
		[EndUser],
        [IsDeleted],      
        [UserCreatedById],      
        [CreatedOn])      
 select NEWID(),
   @TrendId,       
   map.ID as [RegionId],
   map.mapid as [Impact],     
   map.bitvalue,     
   0,      
   @AviraUserId,       
   GETDATE()
 from @TrendRegionmap map
 where not exists (select 1 from [dbo].[TrendRegionMap] map1
					where map1.TrendId = @TrendId
					and map.ID = map1.RegionId);       

/*
--Insert/update countries
declare @TrendCountryMap dbo.[udtGUIDGUIDMapValue];

insert into @TrendCountryMap(ID, MapId,bitValue)
select distinct CountryRegionMap.CountryId,  
map.MapId,
map.bitValue
from CountryRegionMap
inner join @TrendRegionmap map
on CountryRegionMap.RegionId = map.ID 

exec [dbo].[SApp_InsertUpdateTrendCountryMap]      
@TrendId = @TrendId,   
@TrendCountryMap = @TrendCountryMap,
@AviraUserId =@AviraUserId;
*/
SELECT StoredProcedureName ='SApp_InsertUpdateTrendRegionMap',Message =@ErrorMsg,Success = 1;       
   
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);       
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);   
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());      
    
 set @ErrorMsg = 'Error while insert a new TrendRegionMap, please contact Admin for more details.';    
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
       
END CATCH      
      
      
end