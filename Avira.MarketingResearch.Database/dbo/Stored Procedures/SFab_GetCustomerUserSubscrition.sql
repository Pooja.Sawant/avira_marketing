﻿

CREATE PROCEDURE [dbo].[SFab_GetCustomerUserSubscrition]
(
	@CustomerId uniqueidentifier,
	@CustomerUserId uniqueidentifier
)
	AS
/*================================================================================
Procedure Name: [SFab_GetCustomerUserSubscrition]
Author: Sai Krishna
Create date: 25/05/2019
Description: Get list from Subscription table by CustomerId and userId
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
25/05/2019	Sai Krishna		Initial Version
================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SELECT usm.Id,
	   usm.SegmentId,
	   (
			CASE
				WHEN usm.SegmentType = 'BoardIndustries' then 'Industries'
				WHEN usm.SegmentType = 'Industries' then 'Industries'
				WHEN usm.SegmentType = 'Market' then 'Market'
				ELSE 'Project'
			END
	   ) AS SegmentType
FROM UserSegmentMap usm
WHERE usm.CustomerId = @CustomerId
AND usm.CustomerUserId = @CustomerUserId
AND IsDeleted = 0
ORDER BY usm.SegmentType

END