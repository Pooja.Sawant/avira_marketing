﻿CREATE TABLE [dbo].[StagingCompanyPriceVolume] (
    [Id]             UNIQUEIDENTIFIER NOT NULL,
    [ImportId]       UNIQUEIDENTIFIER NULL,
    [Date]           DATETIME         NULL,
    [Price]          DECIMAL (19, 4)  NULL,
    [Currency]       NVARCHAR (3)     NULL,
    [CurrencyUnit]   NVARCHAR (255)   NULL,
    [Volume]         DECIMAL (19, 4)  NULL,
    [VolumeUnit]     NVARCHAR (255)   NULL,
    [CompanyId]      UNIQUEIDENTIFIER NULL,
    [CurrencyId]     UNIQUEIDENTIFIER NULL,
    [CurrencyUnitId] UNIQUEIDENTIFIER NULL,
    [VolumeUnitId]   UNIQUEIDENTIFIER NULL,
    [ErrorNotes]     NVARCHAR (1000)  NULL,
    [RowNo]          INT              NULL,
    CONSTRAINT [PK_StagingCompanyPriceVolume] PRIMARY KEY CLUSTERED ([Id] ASC)
);



