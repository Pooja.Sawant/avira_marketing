﻿CREATE TABLE [dbo].[StagingCompanySegmentInformation] (
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [ImportId]          UNIQUEIDENTIFIER NULL,
    [RowNo]             INT              CONSTRAINT [DF_StagingCompanySegmentInformation_RowNo] DEFAULT ((0)) NOT NULL,
    [CompanyName]       NVARCHAR (255)   NULL,
    [Currency]          NVARCHAR (255)   NULL,
    [Units]             NVARCHAR (255)   NULL,
    [FinanicialYear]    NVARCHAR (255)   NULL,
    [Type]              NVARCHAR (255)   NULL,
    [Name]              NVARCHAR (255)   NULL,
    [Value]             NVARCHAR (255)   NULL,
    [CompanyId]         UNIQUEIDENTIFIER NULL,
    [CurrencyId]        UNIQUEIDENTIFIER NULL,
    [ValueConversionId] UNIQUEIDENTIFIER NULL,
    [UserCreatedById]   NVARCHAR (255)   NULL,
    [ErrorNotes]        NVARCHAR (1050)  NULL,
    CONSTRAINT [PK_StagingCompanySegmentInformation] PRIMARY KEY CLUSTERED ([Id] ASC)
);

