﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyFinancialAnalysisController : AviraBaseController
    {
        private readonly ICompanyFinancialAnalysisService _companyFinancialAnalysisService;
        private readonly ICompanyFinancialAnalysisRepository _companyFinancialAnalysisRepository;
        private ILoggerManager _logger;
        public CompanyFinancialAnalysisController(
            ICompanyFinancialAnalysisService companyFinancialAnalysisService,
            ICompanyFinancialAnalysisRepository companyFinancialAnalysisRepository,
             ILoggerManager logger)
        {
            _companyFinancialAnalysisRepository = companyFinancialAnalysisRepository;
            _companyFinancialAnalysisService = companyFinancialAnalysisService;
            _logger = logger;

        }

        [HttpGet]
        public async Task<ActionResult<MainCompanyFinancialAnalysisInsertUpdateViewModel>> Get(string companyId = "")
        {
            var res = await _companyFinancialAnalysisService.GetAll(0, 0, 0, null, null, false, companyId);
            return res;
        }


        [HttpGet]
        [Route("GetAllConversions")]
        public ActionResult<IEnumerable<ValueConversionViewModel>> GetAllValueConversion()
        {
            List<ValueConversionViewModel> res = null;
            try
            { 
             res = _companyFinancialAnalysisService.GetAllValueConversion().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet]
        [Route("GetAllCurrencies")]
        public ActionResult<IEnumerable<CurrencyViewModel>> GetAll_Currencies()
        {
            List<CurrencyViewModel> res = null;
            try
            {
                 res = _companyFinancialAnalysisService.GetAllCurrencies().ToList();
        }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet]
        [Route("GetAllCategories")]
        public ActionResult<IEnumerable<CategoryViewModel>> GetAll_Categories()
        {
            List<CategoryViewModel> res = null;
            try
            {
                 res = _companyFinancialAnalysisService.GetAllCategories().ToList();
            
}
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);

            } return res;
        }

        [HttpPost]
        public ActionResult Post([FromBody] CompanyFinancialAnalysisInsertUpdateModel viewModel)
        {
            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new Company Financial Analysis, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                var result = _companyFinancialAnalysisService.InsertUpdate(viewModel, CurrentUser);
                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Company Financial Analysis is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }

    }
}