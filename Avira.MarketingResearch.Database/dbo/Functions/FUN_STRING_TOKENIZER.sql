﻿CREATE FUNCTION dbo.FUN_STRING_TOKENIZER
(
@Data NVARCHAR(MAX),
@Delimiter NVARCHAR(MAX) = N','
)
RETURNS TABLE
RETURN	SELECT	CAST(x.[Key] AS INT)+1 AS Position,
JSON_VALUE(x.Value, N'$.p') AS Value
FROM	(
VALUES	(N'[{"p":"' + REPLACE(@Data, @Delimiter, N'"},{"p":"') + N'"}]')
) AS d(Data)
CROSS APPLY	OPENJSON(d.Data, N'$') AS x;