﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels
{
    public class CompanyViewModel
    {
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string Description { get; set; }
        public string Telephone { get; set; }
        public string Telephone2 { get; set; }
        public string Email { get; set; }
        public string Email2 { get; set; }
        public string Fax { get; set; }
        public string Fax2 { get; set; }
        public string ParentCompanyName { get; set; }
        public string ProfiledCompanyName { get; set; }
        public string Website { get; set; }
        public int NoOfEmployees { get; set; }
        public int YearOfEstb { get; set; }
        public int ParentCompanyYearOfEstb { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public Guid Id { get; set; }
        public Guid NatureId { get; set; }
        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "Company";
            }
        }

    }

    public class CompanyInsertDbViewModel
    {
        [Required]
        public string CompanyCode { get; set; }
        [Required]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }
        public string Description { get; set; }
        public string Telephone { get; set; }
        public string Telephone2 { get; set; }
        public string Email { get; set; }
        public string Email2 { get; set; }
        public string Fax { get; set; }
        public string Fax2 { get; set; }
        public string ParentCompanyName { get; set; }
        public string ProfiledCompanyName { get; set; }
        public string Website { get; set; }
        public int NoOfEmployees { get; set; }
        public int YearOfEstb { get; set; }
        public int ParentCompanyYearOfEstb { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public Guid Id { get; set; }
        public Guid NatureId { get; set; }
    }

    public class CompanyUpdateDbViewModel
    {
        [Required]
        public string CompanyCode { get; set; }
        [Required]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }
        public string Description { get; set; }
        public string Telephone { get; set; }
        public string Telephone2 { get; set; }
        public string Email { get; set; }
        public string Email2 { get; set; }
        public string Fax { get; set; }
        public string Fax2 { get; set; }
        public string ParentCompanyName { get; set; }
        public string ProfiledCompanyName { get; set; }
        public string Website { get; set; }
        public int NoOfEmployees { get; set; }
        public int YearOfEstb { get; set; }
        public int ParentCompanyYearOfEstb { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool IsDeleted { get; set; } = false;
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        [Required]
        public Guid Id { get; set; }
        public Guid NatureId { get; set; }
    }
}
