﻿
CREATE PROCEDURE [dbo].[SStaging_InsertImportQualitativeAnalysis]
@TemplateName NVARCHAR(200)='',      
@ImportFileName NVARCHAR(200)='',      
@JsonQualitativeNews NVARCHAR(MAX) ,
@JsonQualitativeAnalysis NVARCHAR(MAX) ,
@JsonQualitativeDescription NVARCHAR(MAX) ,
@JsonQualitativeQuote NVARCHAR(MAX) ,
@JsonQualitativeSegments NVARCHAR(MAX) ,
@JsonQualitativeRegions NVARCHAR(MAX) ,
@JsonESMSegments NVARCHAR(MAX) ,
@JsonESMAnalysis NVARCHAR(MAX) ,
@UserCreatedById UNIQUEIDENTIFIER       
AS      
/*================================================================================  
Procedure Name: SStaging_InsertImportQualitativeAnalysis 
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
08/25/2019 Sharath   Initial Version  
================================================================================*/       
DECLARE @ImportId UNIQUEIDENTIFIER =NEWID()      
declare @ErrorMsg NVARCHAR(2048),@CreatedOn Datetime      
BEGIN      
 SET @CreatedOn = GETDATE()      
 BEGIN TRY 
 INSERT INTO ImportData(Id, TemplateName, ImportFileName, ImportedOn, UserImportedById)      
VALUES(@ImportId, @TemplateName, @ImportFileName,@CreatedOn, @UserCreatedById)  
---------------------------------------------------  
---TO Delete Existing Records FROM Staging
DELETE FROM StagingQualitativeNews;
DELETE FROM StagingQualitativeAnalysis;
DELETE FROM StagingQualitativeDescription;
DELETE FROM StagingQualitativeQuote;
DELETE FROM StagingQualitativeSegmentMapping;
DELETE FROM StagingQualitativeRegionMapping;
DELETE FROM StagingESMSegmentMapping;
DELETE FROM StagingESMAnalysis;


    
--StagingQualitativeNews
INSERT INTO [dbo].[StagingQualitativeNews]
   ([Id]   
   ,[ImportId]
   ,[RowNo]
   ,MarketName
	,SubMarketName
	,NewsDate
	,News
	,NewsCategory
	,Importance
	,Impact)
SELECT	NEWID()
	,@ImportId
	,RowNo
	,MarketName
	,SubMarketName
	,convert(datetime, NewsDate, 126)
	,News
	,NewsCategory
	,Importance
	,Impact
FROM OPENJSON(@JsonQualitativeNews)      
WITH (RowNo [int],
    MarketName [nvarchar](255),
	SubMarketName [nvarchar](255),
	[NewsDate] [datetime],
	[News] [nvarchar](1000),
	[NewsCategory] [nvarchar](255),
	[Importance] [nvarchar](255),
	[Impact] [nvarchar](255)) AS JsonQualitativeNews;
--StagingQualitativeAnalysis
INSERT INTO [dbo].[StagingQualitativeAnalysis]
	([Id]
   ,[ImportId]
   ,RowNo
   ,MarketName
	,SubMarketName
	,AnalysisCategory
	,Analysissubcategory
	,AnalysisText
	,Importance
	,Impact)
SELECT NEWID()
	,@ImportId
	,RowNo
	,MarketName
	,SubMarketName
	,Analysis_Category
	,Analysis_subcategory
	,AnalysisText
	,Importance
	,Impact
FROM OPENJSON(@JsonQualitativeAnalysis)      
     with(RowNo [int],
	 MarketName [nvarchar](255),
	SubMarketName [nvarchar](255),
	Analysis_Category [nvarchar](255),
	Analysis_subcategory [nvarchar](255),
	AnalysisText [nvarchar](4000),
	[Importance] [nvarchar](255),
	[Impact] [nvarchar](255)) as JsonQualitativeAnalysis;
--StagingQualitativeDescription
INSERT INTO [dbo].[StagingQualitativeDescription]
           ([Id]
			,[ImportId]
			,RowNo
			,MarketName
			,SubMarketName
			,DescriptionCategory
			,DescriptionSubcategory
			,DescriptionText)
SELECT NEWID()
		,@ImportId
		,RowNo
		,MarketName
		,SubMarketName
		,Description_Category
		,Description_Subcategory
		,DescriptionText
FROM OPENJSON(@JsonQualitativeDescription)      
     with(RowNo [int],
	    MarketName [nvarchar](255),
		SubMarketName [nvarchar](255),
		Description_Category [nvarchar](255),
		Description_Subcategory [nvarchar](255),
		DescriptionText [nvarchar](4000)) as JsonQualitativeDescription;
--StagingQualitativeQuote
INSERT INTO [dbo].[StagingQualitativeQuote]
           ([Id]
			,[ImportId]
			,RowNo
			,MarketName
			,SubMarketName
			,ResourceName
			,ResourceCompany
			,ResourceDesignation
			,DateOfQuote
			,Quote
			,OtherRemarks)
SELECT NEWID()
		,@ImportId
		,RowNo
		,MarketName
		,SubMarketName
		,ResourceName
		,ResourceCompany
		,ResourceDesignation
		,DateOfQuote
		,Quote
		,OtherRemarks
FROM OPENJSON(@JsonQualitativeQuote)      
     with(RowNo [int],
	    MarketName [nvarchar](255),
		SubMarketName [nvarchar](255),
		ResourceName [nvarchar](255),
		ResourceCompany [nvarchar](255),
		ResourceDesignation [nvarchar](255),
		[DateOfQuote] [datetime],
		Quote [nvarchar](1000),
		OtherRemarks [nvarchar](1000)) as JsonQualitativeQuote;
--StagingQualitativeSegmentMapping
INSERT INTO [dbo].[StagingQualitativeSegmentMapping]
           ([Id]
           ,[ImportId]
		   ,RowNo
           ,MarketName
			,SegmentName
			,SubSegmentName)
SELECT NEWID(),
		@ImportId
		,RowNo
		,MarketName
		,SegmentName
		,SubSegmentName
FROM OPENJSON(@JsonQualitativeSegments)      
     with(RowNo [int],
	    MarketName [nvarchar](255),
		SegmentName [nvarchar](255),
		SubSegmentName [nvarchar](255)) as JsonQualitativeSegments;
--StagingQualitativeRegionMapping
INSERT INTO [dbo].[StagingQualitativeRegionMapping]
           ([Id]
           ,[ImportId]
		   ,RowNo
           ,MarketName
			,RegionName
			,CountryName)
SELECT NEWID(),
		@ImportId
		,RowNo
		,MarketName
		,RegionName
		,CountryName
FROM OPENJSON(@JsonQualitativeRegions)      
     with(RowNo [int],
	    MarketName [nvarchar](255),
		RegionName [nvarchar](255),
		CountryName [nvarchar](255)) as JsonQualitativeRegions;

----StagingESMSegmentMapping
INSERT INTO [dbo].[StagingESMSegmentMapping]
           ([Id]
           ,[ImportId]
		   ,RowNo
		   ,MarketName
		   ,SegmentName
		   ,SubSegmentName)
SELECT NEWID(),
		@ImportId
		,RowNo
		,MarketName
		,SegmentName
		,SubSegmentName
FROM OPENJSON(@JsonESMSegments)      
     with(RowNo [int],
	    MarketName [nvarchar](255),
	    SegmentName [nvarchar](255),
		SubSegmentName [nvarchar](260)) as JsonESMSegments;

--StagingESMAnalysis
INSERT INTO [dbo].[StagingESMAnalysis]
	([Id]
    ,[ImportId]
	,RowNo
	,MarketName
    ,SegmentName
	,SubSegmentName
	,AnalysisCategory
	,AnalysisText
	,Importance
	,Impact)
SELECT NEWID()
	,@ImportId
	,RowNo
	,MarketName
	,SegmentName
	,SubSegmentName
	,Analysis_Category
	,AnalysisText
	,Importance
	,Impact
FROM OPENJSON(@JsonESMAnalysis)      
     with(RowNo [int],
	 MarketName [nvarchar](255),
	 SegmentName [nvarchar](255),
	SubSegmentName [nvarchar](255),
	Analysis_Category [nvarchar](255),
	AnalysisText [nvarchar](MAX),
	[Importance] [nvarchar](255),
	[Impact] [nvarchar](255)) as JsonESMAnalysis;



SELECT StoredProcedureName ='SStaging_InsertImportQualitativeAnalysis',Message =@ErrorMsg,Success = 1; 
END TRY  
BEGIN CATCH  
    -- Execute the error retrieval routine.  
 DECLARE @ErrorNumber int;  
 DECLARE @ErrorSeverity int;  
 DECLARE @ErrorProcedure varchar(100);  
 DECLARE @ErrorLine int;  
 DECLARE @ErrorMessage varchar(500);  
  
  SELECT @ErrorNumber = ERROR_NUMBER(),  
@ErrorSeverity = ERROR_SEVERITY(),  
@ErrorProcedure = ERROR_PROCEDURE(),  
@ErrorLine = ERROR_LINE(),  
@ErrorMessage = ERROR_MESSAGE()  
  
 insert into dbo.Errorlog(ErrorNumber,  
       ErrorSeverity,  
       ErrorProcedure,  
       ErrorLine,  
       ErrorMessage,  
       ErrorDate)  
values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,@CreatedOn);  

 set @ErrorMsg = 'Error while importing Qualitative Analysis, please contact Admin for more details.';  
--THROW 50000,@ErrorMsg,1;    
--return(1)
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine
    ,Message =@ErrorMsg,Success = 0;   
   
END CATCH   
      
      
END