﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.Helpers;
using System.Collections.Generic;

namespace Avira.MarketingResearch.Repository.CoreInterfaces
{
    public interface IMenuRepository
    {
        IEnumerable<MenuViewModel> GetAll();
        Result<SpTransactionMessage> Create(MenuInsertDbViewModel viewModel);
        Result<SpTransactionMessage> Update(MenuUpdateDbViewModel viewModel);
    }
}
