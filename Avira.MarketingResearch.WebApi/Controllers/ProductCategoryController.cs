﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductCategoryController : AviraBaseController
    {
        public readonly IProductCategoryService _productCategoryService;
        private ILoggerManager _logger;
        public ProductCategoryController(
            IProductCategoryService productCategoryService, ILoggerManager logger)
        {
            _productCategoryService = productCategoryService;
            _logger = logger;
        }

        [HttpGet]       
        public ActionResult<IEnumerable<ProductCategoryViewModel>> Get()
        {
            List<ProductCategoryViewModel> res = null;
            try
            {
                res = _productCategoryService.GetAll().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }
    }
}