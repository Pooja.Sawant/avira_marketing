﻿CREATE TABLE [dbo].[ProductStatus] (
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [ProductStatusName] NVARCHAR (256)   CONSTRAINT [DF_ProductStatus_ProductStatusName] DEFAULT ('') NOT NULL,
    [Description]       NVARCHAR (256)   NULL,
    [CreatedOn]         DATETIME2 (7)    NULL,
    [UserCreatedById]   UNIQUEIDENTIFIER NULL,
    [ModifiedOn]        DATETIME2 (7)    NULL,
    [UserModifiedById]  UNIQUEIDENTIFIER NULL,
    [IsDeleted]         BIT              CONSTRAINT [DF_ProductStatus_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]         DATETIME2 (7)    NULL,
    [UserDeletedById]   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_ProductStatus] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ProductStatus_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_ProductStatus_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id])
);


GO

GO

GO

GO

GO

GO

GO