﻿CREATE TABLE [dbo].[StagingCompanyClients] (
    [Id]             UNIQUEIDENTIFIER NOT NULL,
    [ImportId]       UNIQUEIDENTIFIER NULL,
    [ClientName]     NVARCHAR (255)   NULL,
    [ClientIndustry] NVARCHAR (255)   NULL,
    [Remarks]        NVARCHAR (512)   NULL,
    [CompanyId]      UNIQUEIDENTIFIER NULL,
    [ErrorNotes]     NVARCHAR (1000)  NULL,
    [RowNo]          INT              NULL,
    CONSTRAINT [PK_StagingCompanyClients] PRIMARY KEY CLUSTERED ([Id] ASC)
);



