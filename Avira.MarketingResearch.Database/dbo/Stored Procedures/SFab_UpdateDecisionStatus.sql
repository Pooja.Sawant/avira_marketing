﻿CREATE PROCEDURE [dbo].[SFab_UpdateDecisionStatus]
(
@BaseDecisionId uniqueidentifier,
@DecisionId uniqueidentifier,
@StatusId	uniqueidentifier,
@UserId	uniqueidentifier
)
As   

/*================================================================================    
Procedure Name: SFab_UpdateDecisionStatus   
Author: Praveen    
Create date: 07-Jan-2020
Description: Update decision status by user
Change History    
Date  Developer  Reason  
__________ ____________ ______________________________________________________    
07-Jan-2020  Praveen   Initial Version 
08-Jan-2020  Harshal   Added Statement in SP
30-Jan-2020 Nitin Issue fixed
================================================================================*/    
BEGIN  
Declare @ErrorMsg NVARCHAR(2048);

BEGIN TRY
	Begin    
		------------------
		--Required Tables: Decision
		--Notes: N/A
		--Case 1: N/A
		--Expected Result :SELECT StoredProcedureName ='SFab_UpdateDecisionStatus',Message = @ErrorMsg,Success = 1/0;

		If Not Exists(Select Id From Decision Where ID = @DecisionId and UserId = @UserId)
		Begin
		    if(@BaseDecisionId='00000000-0000-0000-0000-000000000000')
			   set @BaseDecisionId=@DecisionId
			   
			Execute SFab_CopyDecisionFromBase @InDecisionId = @BaseDecisionId, @UserId = @UserId, 
				@CopyDecisionParametersFromBase = 0, @OutDecisionId = @DecisionId OUTPUT		
		End
		UPDATE Decision SET StatusId = @StatusId, ModifiedOn = GetDate(), UserModifiedById = @UserId 
		WHERE Id = @DecisionId and UserId = @UserId
	END
	SELECT StoredProcedureName ='SFab_UpdateDecisionStatus',Message =@ErrorMsg,Success = 1;  
END TRY
BEGIN CATCH
    -- Execute the error retrieval routine.
EXEC SApp_LogErrorInfo

    set @ErrorMsg = 'Error while Update DecisionStatus, please contact Admin for more details.';
 	SELECT StoredProcedureName ='SFab_UpdateDecisionStatus',Message =@ErrorMsg,Success = 0;    
 
  
END CATCH 
END