﻿
  
CREATE PROCEDURE [dbo].[SApp_GetAllImportance]    
 AS  
/*================================================================================  
Procedure Name: [SApp_GetAllImportance]  
Author: Swami  
Create date: 04/26/2019  
Description: Get list of conversion values from table  
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
04/26/2019 Swami   Initial Version  
================================================================================*/  
  
BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
	
	SELECT Id as ImportanceId, ImportanceName, [Description] AS ImportanceDescription FROM Importance
	WHERE IsDeleted = 0 
	order by sequence asc

END