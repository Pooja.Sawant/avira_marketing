﻿using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Model.ViewModels;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Linq;

namespace Avira.MarketingResearch.Repository.Implementations.CompanyProfile
{
    public class EcoSystemPresenceRepositary : IEcoSystemPresenceRepository
    {
        protected IDbHelper _dbHelper;
        public EcoSystemPresenceRepositary(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }
        public IEnumerable<EcoSystemPresenceMapViewModel> GetEcoSystemPresenceMapByCompanyId(Guid CompanyId)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<EcoSystemPresenceMapViewModel>(EcoSystemPresenceQueries.StoreProcSelectEcoSystemPresenceByCompanyId, 
                new {@CompanyID = CompanyId }, null, true, null, CommandType.StoredProcedure);
        }

        public IEnumerable<SectorPresenceMapViewModel> GetSectorPresenceMapByCompanyId(Guid CompanyId)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<SectorPresenceMapViewModel>(EcoSystemPresenceQueries.StoreProcSelectEcoSystemPresenceSectorByCompanyId,
                new { @CompanyID = CompanyId }, null, true, null, CommandType.StoredProcedure);
        }

        public IEnumerable<EcoSystemPresenceKeyCompViewModel> GetEcoSystemPresenceKeyCompByCompanyId(Guid CompanyId)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<EcoSystemPresenceKeyCompViewModel>(EcoSystemPresenceQueries.StoreProcSelectEcoSystemPresenceCompetitersByCompanyId,
                new { @CompanyID = CompanyId }, null, true, null, CommandType.StoredProcedure);
        }
       
        public Result<SpTransactionMessage> InsertUpdate(EcoSystemPresenceDbViewModel ecoSystemPresenceUpdateDbViewModel)
        {
            //Data table for Company ESM Details.
            DataTable ecoSystemPresenceTable = new DataTable();
            ecoSystemPresenceTable.Columns.Add("ID", typeof(Guid));
            ecoSystemPresenceTable.Columns.Add("bitValue", typeof(bool));
            if (ecoSystemPresenceUpdateDbViewModel.EcoSystemPresences != null & ecoSystemPresenceUpdateDbViewModel.EcoSystemPresences.Count > 0)
            {
                foreach (var item in ecoSystemPresenceUpdateDbViewModel.EcoSystemPresences.Where(x => x.Ischecked == true))
                {
                    ecoSystemPresenceTable.Rows.Add(new Object[] { item.EcoSystemId, item.Ischecked });
                }
            }

            var spTransactionMessage = _dbHelper.ExecuteQuery(EcoSystemPresenceQueries.StoreProcInsertEcoSystemPresenceESMMap,
               new
               {
                   @CompanyId = ecoSystemPresenceUpdateDbViewModel.CompanyId,
                   @EsmIdList = ecoSystemPresenceTable.AsTableValuedParameter("dbo.udtGUIDBitValue"),
                   @UserCreatedById = ecoSystemPresenceUpdateDbViewModel.UserCreatedById,
                   @CreatedOn = DateTime.UtcNow
               }, CommandType.StoredProcedure);

            if (spTransactionMessage.IsSuccess == false)
            {
                return spTransactionMessage;
            }
            //Data table for Company Sector Details.
            DataTable ecoSystemPresenceSectorTable = new DataTable();
            ecoSystemPresenceSectorTable.Columns.Add("ID", typeof(Guid));
            ecoSystemPresenceSectorTable.Columns.Add("bitValue", typeof(bool));
            if (ecoSystemPresenceUpdateDbViewModel.SectorPresences != null & ecoSystemPresenceUpdateDbViewModel.SectorPresences.Count > 0)
            {
                foreach (var item in ecoSystemPresenceUpdateDbViewModel.SectorPresences.Where(x=>x.Ischecked==true))
                {
                    ecoSystemPresenceSectorTable.Rows.Add(new Object[] { item.IndustryId, item.Ischecked });
                }
            }

            var spTransactionMessage1 = _dbHelper.ExecuteQuery(EcoSystemPresenceQueries.StoreProcInsertForEcoSystemPresenceSectorMap,
               new
               {
                   @CompanyId = ecoSystemPresenceUpdateDbViewModel.CompanyId,
                   @SectorIdList = ecoSystemPresenceSectorTable.AsTableValuedParameter("dbo.udtGUIDBitValue"),
                   @UserCreatedById = ecoSystemPresenceUpdateDbViewModel.UserCreatedById,
                   @CreatedOn = DateTime.UtcNow
               }, CommandType.StoredProcedure);

            if (spTransactionMessage1.IsSuccess == false)
            {
                return spTransactionMessage1;
            }

            //Data table for Company KeyCompetiters.
            DataTable ecoSystemPresenceKeyCompetitersTable = new DataTable();
            ecoSystemPresenceKeyCompetitersTable.Columns.Add("Value", typeof(string));
            if (ecoSystemPresenceUpdateDbViewModel.KeyCompetiters != null & ecoSystemPresenceUpdateDbViewModel.KeyCompetiters.Count > 0)
            {
                foreach (var Value in ecoSystemPresenceUpdateDbViewModel.KeyCompetiters)
                {
                    if (!string.IsNullOrEmpty(Value.KeyCompetiters))
                    {
                        ecoSystemPresenceKeyCompetitersTable.Rows.Add(new Object[] { Value.KeyCompetiters });
                    }
                }
            }
            var spTransactionMessage2 = _dbHelper.ExecuteQuery(EcoSystemPresenceQueries.StoreProcInsertEcoSystemPresenceCompetitersMap,
               new
               {
                   @CompanyId = ecoSystemPresenceUpdateDbViewModel.CompanyId,
                   @KeyCompetitersList = ecoSystemPresenceKeyCompetitersTable.AsTableValuedParameter("dbo.udtVarcharValue"),
                   @UserCreatedById = ecoSystemPresenceUpdateDbViewModel.UserCreatedById
               }, CommandType.StoredProcedure);

            if (spTransactionMessage2.IsSuccess == false)
            {
                return spTransactionMessage2;
            }

            return spTransactionMessage;
        }
    }
}
