﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels
{
    public class KeyValueViewModel
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
