﻿CREATE TYPE [dbo].[udtGUIDShareHolidingMultiValue] AS TABLE (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [Name]         NVARCHAR (256)   NOT NULL,
    [EntityTypeId] UNIQUEIDENTIFIER NOT NULL,
    [DecimalValue] DECIMAL (19, 4)  NULL);

