﻿
CREATE PROCEDURE [dbo].[SFab_GetDecisionAlternativeTaskDetails]    
(  
    @DecisionAlternativeTaskId UNIQUEIDENTIFIER,
	@DecisionId UNIQUEIDENTIFIER,
	@DecisionAlternativeId UNIQUEIDENTIFIER,
	@AviraUserId UNIQUEIDENTIFIER
)
As    
/*================================================================================    
Procedure Name: SFab_GetDecisionAlternativeTaskDetails   
Author: Harshal Ghotkar    
Create date: 30-Jan-2020
Description: To return Decision,Alternative And Task details by DecisionAlternativeTaskId of the user
Change History    
Date  Developer  Reason  
__________ ____________ ______________________________________________________    
30-Jan-2020  Harshal   Initial Version
================================================================================*/    
BEGIN    
------------------

Declare @DecisionStatusDecisionMadeId uniqueidentifier

	Select @DecisionStatusDecisionMadeId = ID From LookupStatus
	Where StatusName = 'Decision made' And StatusType = 'DMDecisionStatus'

SELECT DecisionAlternativeTask.Id as TaskId,Decision.Id as DecisionId,Decision.Description as DecisionDescription, 
       DecisionAlternative.Id AS AlternativeId, DecisionAlternative.Description AS AlternativDescription,
	   DecisionAlternativeTask.Description as TaskDescription,DecisionAlternativeTask.ResourceAssigned as ResourceAssigned,      
	   case when DecisionAlternativeTask.DueDate != null then REPLACE(CONVERT(CHAR(11), DecisionAlternativeTask.DueDate, 106),' ','/') else REPLACE(CONVERT(CHAR(11), GETDATE(), 106),' ','/') end as Deadline, 
	   DecisionAlternativeTask.StatusId as StatusId,LookupStatus.StatusName as StatusName,
	   DecisionAlternativeTask.CreatedOn as TaksCreatedOn,DecisionAlternativeTask.ModifiedOn as TaskModifiedOn,
       CASE WHEN (Decision.StatusId = @DecisionStatusDecisionMadeId) THEN 'true' 
	   ELSE 'false' 
	   END AS IsReadonly
FROM DecisionAlternativeTask  
INNER JOIN Decision ON DecisionAlternativeTask.DecisionId=Decision.Id
INNER JOIN DecisionAlternative ON DecisionAlternativeTask.DecisionAlternativeId = DecisionAlternative.Id 
INNER JOIN LookupStatus ON DecisionAlternativeTask.StatusId = LookupStatus.Id and LookupStatus.StatusType = 'DMDecisionAlternativeTaskStatus'
WHERE DecisionAlternativeTask.Id = @DecisionAlternativeTaskId
   AND DecisionAlternativeTask.DecisionAlternativeId = @DecisionAlternativeId 
   AND DecisionAlternativeTask.DecisionId = @DecisionId 
   and Decision.UserId=@AviraUserId
END