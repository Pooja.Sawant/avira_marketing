﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces.Masters;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Avira.MarketingResearch.Repository.Implementations.Masters
{
    public class EcoSystemRepository : IEcoSystemRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "EcoSystem";
        public EcoSystemRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }


        public EcoSystemViewModel GetById(Guid guid)
        {
            EcoSystemViewModel ecoSystemViewModel = new EcoSystemViewModel();
            ecoSystemViewModel = GetAllEcoSystem(guid).FirstOrDefault();
            return ecoSystemViewModel;
        }

        public IEnumerable<EcoSystemViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted)
        {
            return GetAllEcoSystem(null, PageStart, pageSize, SortDirection, OrdbyByColumnName,Search, includeDeleted);
        }

        private IEnumerable<EcoSystemViewModel> GetAllEcoSystem(Guid? guid, int PageStart=0, int pageSize=0, int SortDirection=-1, string OrdbyByColumnName="", string Search = "", bool includeDeleted = false)
        {           

            DataTable ecoSystemIdsTable = new DataTable();
            ecoSystemIdsTable.Columns.Add("ID", typeof(Guid));

            if (guid != null)
            {
                ecoSystemIdsTable.Rows.Add(guid);
            }            
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<EcoSystemViewModel>(EcoSystemQueries.StoreProcSelectAll
                            , new { @EcoSystemIDList = ecoSystemIdsTable.AsTableValuedParameter("dbo.udtUniqueIdentifier"), @includeDeleted = includeDeleted, @PageStart= PageStart, @pageSize= pageSize, @SortDirection= SortDirection, @OrdbyByColumnName= OrdbyByColumnName, @EcoSystemName=Search }
                            ,null, true, null, CommandType.StoredProcedure);
        }

        public Result<SpTransactionMessage> Create(EcoSystemInsertDbViewModel ecoSystemInsertDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(EcoSystemQueries.StoreProcInsert, ecoSystemInsertDbViewModel, CommandType.StoredProcedure);
            return spTransactionMessage;

        }

        public Result<SpTransactionMessage> Update(EcoSystemUpdateDbViewModel ecoSystemUpdateDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(EcoSystemQueries.StoreProcUpdate, ecoSystemUpdateDbViewModel, CommandType.StoredProcedure);
            return spTransactionMessage;
        }



    }
}
