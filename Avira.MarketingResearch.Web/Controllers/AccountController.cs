﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using static Avira.MarketingResearch.Model.ViewModels.PermissionsByUserRoleIdViewModel;

namespace Avira.MarketingResearch.Web.Controllers
{
    public class AccountController : Controller
    {
        public readonly IServiceRepository _serviceRepository;
        private ILoggerManager _logger;
        public AccountController(IServiceRepository serviceRepository, ILoggerManager logger)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
        }

        //Get : Login
        public ActionResult Login()
        {           
            if (HttpContext.Session.GetString("Username") != null)
                return RedirectToAction("Index", "Project");
            else
                return View();
        }

        [HttpPost]
        public ActionResult Login(AviraUserLogin application)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                    HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, application);
                    response.EnsureSuccessStatusCode();
                    ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                    if (viewModel != null)
                    {
                        ViewBag.Message = viewModel.Message;
                        if (ViewBag.Message == "Password is incorrect. You have 2 chances left." || ViewBag.Message == "Password is incorrect. You have 1 chance left." || ViewBag.Message == "Password is incorrect. Your Email Address is Locked. Please contact Admin." || ViewBag.Message == "Your Email Address is Locked. Please contact Admin." || ViewBag.Message == "Kindly enter correct email Address and Password Or Email address is not registred")
                        {
                            return View(application);
                        }
                    }
                    //
                    HttpResponseMessage userResponse = _serviceRepository.GetByIdResponse(contollerName, application.EmailAddress);
                    userResponse.EnsureSuccessStatusCode();
                    AviraUser userDetails = userResponse.Content.ReadAsAsync<AviraUser>().Result;
                    HttpContext.Session.SetString("UserId", userDetails.Id.ToString());
                    HttpContext.Session.SetString("Username", userDetails.Username);
                    HttpContext.Session.SetString("EmailAddress", userDetails.EmailAddress);
                    HttpContext.Session.SetString("DisplayName", userDetails.DisplayName);
                    // HttpContext.Session.SetString("UserRoleId", userDetails.UserRoleId.ToString());
                    HttpContext.Session.SetString("Id", userDetails.Id.ToString());

                    GetUserRoles(userDetails.Id);

                    //-- Get permissions, forms, menus based on userRoleId
                    var roles = HttpContext.Session.GetString("userRoles");
                    var userRoles = JsonConvert.DeserializeObject<List<RoleViewModel>>(roles);

                    HttpContext.Session.SetString("UserRoleName",userRoles[0].UserRoleName);
                    //ViewBag.UserRoleName = userRoles[0].UserRoleName;
                    //
                    string userRoleId = "";
                    //
                    List<SelectListItem> listItems = new List<SelectListItem>();
                    foreach (var item in userRoles)
                    {
                        userRoleId = item.Id.ToString();
                        listItems.Add(new SelectListItem
                        {
                            Text = item.UserRoleName,
                            Value = item.Id.ToString()
                        });
                    }


                    //userRoles.Contains(value).ToString();
                    //if (listItems.Count == 1)
                    //    var userRoleId = userRoles.IndexOf()
                    //HttpResponseMessage userPermissionResponse = _serviceRepository.GetByIdResponse(contollerName, application.EmailAddress);
                    HttpResponseMessage userPermissionResponse = _serviceRepository.GetPermissionByGuIdResponse(contollerName, userRoleId);
                    userResponse.EnsureSuccessStatusCode();
                    PermissionsByUserRoleIdViewModel userPermissions = userPermissionResponse.Content.ReadAsAsync<PermissionsByUserRoleIdViewModel>().Result;
                    ViewBag.userPermissions = userPermissions;
                    ViewData["userPermissions"] = userPermissions;

                    if (userPermissions.Forms != null && userPermissions.Menus != null && userPermissions.Permissions != null)
                    {
                        //HttpContext.Session.SetString("FormId", userPermissions.Forms.FormId.ToString());
                        //HttpContext.Session.SetString("FormName", userPermissions.Forms.FormName);

                        List<PermissionByForm> FormsList = new List<PermissionByForm>();
                        List<MenuList> menuList = new List<MenuList>();
                        List<PermissionByRoleId> permissionByRoleIdList = new List<PermissionByRoleId>();
                        foreach (var item in userPermissions.Forms)
                            FormsList.Add(item);
                        foreach (var item in userPermissions.Menus)
                            menuList.Add(item);
                        foreach (var item in userPermissions.Permissions)
                            permissionByRoleIdList.Add(item);
                        HttpContext.Session.SetString("FormsList", JsonConvert.SerializeObject(FormsList));
                        HttpContext.Session.SetString("MenuList", JsonConvert.SerializeObject(menuList));
                        HttpContext.Session.SetString("PermissionList", JsonConvert.SerializeObject(permissionByRoleIdList));
                    }

                    //
                    ClaimsIdentity identity = null;
                    bool isAuthenticated = false;

                    identity = new ClaimsIdentity(new[]
                    {
                        new Claim(ClaimTypes.Name, application.EmailAddress)
                    }, CookieAuthenticationDefaults.AuthenticationScheme);

                    isAuthenticated = true;

                    if (isAuthenticated)
                    {
                        var principal = new ClaimsPrincipal(identity);

                        var login = HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);

                        return RedirectToAction("Index", "Project");
                        //return RedirectToAction("Index", "RawMaterial");

                    }
                    return View(application);
                    //

                    //return RedirectToAction("Index", "Project");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                }
            }

            return View(application);
        }

        public void GetUserRoles(Guid Id)
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage UserRolesResponse = _serviceRepository.GetByGuIdResponse(contollerName, Id);
                UserRolesResponse.EnsureSuccessStatusCode();
                IEnumerable<RoleViewModel> userRoles = UserRolesResponse.Content.ReadAsAsync<IEnumerable<RoleViewModel>>().Result;
                List<SelectListItem> listItems = new List<SelectListItem>();
                foreach (var item in userRoles)
                {
                    listItems.Add(new SelectListItem
                    {
                        Text = item.UserRoleName,
                        Value = item.Id.ToString()
                    });
                }
                //
                HttpContext.Session.SetString("userRoles", JsonConvert.SerializeObject(userRoles));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        public IActionResult Logout()
        {
            var login = HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            HttpContext.Session.Clear();
            return RedirectToAction("Login");
        }

        // GET: Account
        public ActionResult Index()
        {
            if (HttpContext.Session.GetString("Username") != null)
                return View();
            else
                return RedirectToAction("Index", "Project");
        }

        // GET: Account/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Account/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Account/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Account/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Account/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Account/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Account/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public ActionResult ChangePassword()
        {
            AviraUserChangePassword model = new AviraUserChangePassword();
            try
            {
                model.EmailAddress = HttpContext.Session.GetString("EmailAddress");
                model.AviraUserID = new Guid(HttpContext.Session.GetString("UserId"));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult ChangePassword(AviraUserChangePassword viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(viewModel);
                }
                else
                {
                    var userId = viewModel.AviraUserID;
                    var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                    HttpResponseMessage userResponse = _serviceRepository.GetDynamicURL(contollerName, userId.ToString());
                    userResponse.EnsureSuccessStatusCode();
                    AviraUser userDetails = userResponse.Content.ReadAsAsync<AviraUser>().Result;
                    //var user = 
                    //var user = mUserService.Get(Security.Security.UserID);
                    //if (user == null)
                    //{
                    //    var error = mUserService.GetLastError();
                    //    if (error == null)
                    //        ViewBag.LoginError = "Either userid is invalid";
                    //    else
                    //        ViewBag.LoginError = error.Message;
                    //    ModelState.AddModelError(string.Empty, ViewBag.LoginError);
                    //    return View(viewModel);
                    //}
                    //else
                    //{
                    //    if (user.Password == viewModel.OldPassword)
                    //    {
                    //        user.Password = viewModel.NewPassword;
                    //        mUserService.UpdateUserPassword(user);
                    //        TempData["PasswordUpdate"] = "Your password updated successfully.";

                    //        //return RedirectToAction("Index", "Home");
                    //    }
                    //    else
                    //    {
                    //        //ViewBag.errorMessage = "Old password is wrong";
                    //        // ViewBag.LoginError = "Old password is wrong";
                    //        //  ModelState.AddModelError(string.Empty, ViewBag.LoginError);
                    //        TempData["WrongPassword"] = "Old password is wrong.";

                    //        //ModelState.AddModelError(string.Empty, "Old password is wrong");
                    //    }
                    //}
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View();
        }
    }
}