﻿CREATE PROCEDURE [dbo].[SFab_GetMarketSizingData_BarChart]
(
 @SegmentID uniqueidentifier=null,
 @ProjectID uniqueidentifier=null,
 @SegmentIdList dbo.[udtUniqueIdentifier] readonly,
 @RegionIdList dbo.[udtUniqueIdentifier] readonly,
 @CountryIdList dbo.[udtUniqueIdentifier] readonly,
 @TimeTagId uniqueidentifier = null,
 @CurrencyId uniqueidentifier = null,
 @MatricType nvarchar(100)=null,
 @ValueName	nvarchar(100)= null out,
 @CaptureJson bit = 0
)  
AS  
/*================================================================================  
Procedure Name: [SFab_GetMarketSizingData_BarChart]  
Author: Swami Aluri  
Create date: 06/04/2019  
Description: Get list of Marketing sizing from table   
Change History  
Date        Developer     Reason  
__________  ____________  ______________________________________________________  
06/04/2019   Swami Aluri   Initial Version  
================================================================================*/  

BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
	if @CaptureJson is null set @CaptureJson = 0
	Declare @SegmentType NVARCHAR(100);
	SELECT @SegmentType = SegmentName FROM Segment
	WHERE Segment.Id = @SegmentID
	declare @USDCurrencyId uniqueidentifier	
	select @USDCurrencyId = Id from Currency
	where CurrencyName = 'United States dollar';
	If @CurrencyId is null 
	set @CurrencyId = @USDCurrencyId;
	declare @Today datetime = getdate();
	IF (@SegmentType is null or @SegmentType = '')
	SET @SegmentType = 'End User'
	declare @RegionLevel tinyint;
	set @RegionLevel = 1;
	DECLARE @RegionList dbo.[udtUniqueIdentifier];

	IF not exists (select 1 from @RegionIdList)
	begin
	insert into @RegionList
	SELECT id FROM Region WHERE RegionName = 'Global'
	end
	else
	begin
	insert into @RegionList 
	select id from @RegionIdList;
	end

	while (@RegionLevel < = 3)
	begin
	set @RegionLevel = @RegionLevel +1;

	insert into @RegionList 
	select ID
	from Region
	where RegionLevel = @RegionLevel
	and exists (select 1 from @RegionList ParentRegion 
				where Region.ParentRegionId = ParentRegion.ID)

	end


	declare @CountryIdFilterList dbo.[udtUniqueIdentifier];

	if exists (select 1 from @CountryIdList)
	begin
	insert into @CountryIdFilterList
	select Id
	from country
	where exists(select 1 from @CountryIdList list
				inner join CountryRegionMap CRM
				on list.ID = CRM.CountryId
				inner join @RegionList RList
				on CRM.RegionId = RList.ID
				where country.id = CRM.CountryId
				);
	end
	else
	begin
	insert into @CountryIdFilterList
	select Id
	from country
	where exists(select 1 from CountryRegionMap CRM
				inner join @RegionList RList
				on CRM.RegionId = RList.ID
				where country.id = CRM.CountryId
				);
	end
    DECLARE @Years int;
    DECLARE @YearMin int;
    DECLARE @YearMax int;
    if @TimeTagId is not null
	begin
    SELECT @Years = DurationYears FROM TimeTag WHERE Id =@TimeTagId;
	end
	else
	begin
	SELECT @Years = DurationYears FROM TimeTag WHERE TimeTagName = 'LongTerm';
	end
    select @YearMin = BaseYear  from BaseYearMaster
    select @YearMax = @YearMin + @Years 

		CREATE table #tmp_Market(SegmentName nvarchar(256),
						[Value] DECIMAL(20,2),
						GrowthRatePercentage DECIMAL(20,2),
						[Year] int);
	CREATE table #tmp_Segment (SegmentId uniqueidentifier,
						SegmentName nvarchar(256));
	insert into #tmp_Segment
	select id as SegmentId,
		SubSegmentName as SegmentName
		from SubSegment
		where SegmentId = @SegmentID
		and (id IN (SELECT id FROM @SegmentIdList) OR not exists(select 1 from @SegmentIdList ))
		and IsDeleted = 0
		
declare @UnitsId uniqueidentifier;

select top 1 @UnitsId  = ID,
@ValueName = ValueName from ValueConversion VC
where exists (select 1 FROM MarketValueSummary as MV
   WHERE mv.Year between @YearMin and @YearMax 
   AND mv.ProjectId =  @ProjectID  --06/17/2019 comment for testing by Pooja
   --Either map country from list or map regions and sub-regions with no country mentioned
   and (MV.CountryId in (select ID from @CountryIdFilterList)
		OR (MV.CountryId is null and MV.RegionId in( select ID from @RegionList)))
and (((@MatricType is null or @MatricType = '' or @MatricType = 'Value') and vc.Id = mv.ValueConversionId)
or (@MatricType = 'Volume' and vc.Id = mv.VolumeConversionId)
))
order by vc.Conversion desc;

CREATE TABLE #tmp_MarketValue(
	[Value] [decimal](20, 4),
	[Year] [int],
	[SegmentId] [uniqueidentifier]);


insert into #tmp_MarketValue
SELECT	 sum(case when @MatricType = 'Volume' Then [dbo].[fun_UnitDeConversion](@UnitsId,MV.Volume)
			  else  [dbo].[fun_CurrencyConversion]([dbo].[fun_UnitDeConversion](@UnitsId,MV.[Value]), @USDCurrencyId, @CurrencyId,@Today) end)   AS [Value], 
		   MV.[Year] AS Year,
		   mv.SubSegmentId as SegmentId

   FROM MarketValueSummary mv
   
   
   WHERE mv.Year between @YearMin and @YearMax 
   AND mv.ProjectId =  @ProjectID  
   and mv.SegmentId = @SegmentID
   and ((MV.CountryId is null and MV.RegionId in( select ID from @RegionList))
		OR MV.CountryId in (select ID from @CountryIdFilterList))
		--and MVSM.SubSegmentId in (SELECT id FROM @SegmentIdList)
	group by MV.[Year], mv.SubSegmentId ;

insert into #tmp_Market(SegmentName, [Value], [Year])
SELECT segCTE.SegmentName,
SUM(CTE_MV.[Value]) AS [Value],
CTE_MV.[Year]
FROM #tmp_MarketValue CTE_MV 
inner JOIN #tmp_Segment segCTE 
ON CTE_MV.SegmentId = segCTE.SegmentId
GROUP BY  segCTE.SegmentName,
CTE_MV.[Year];

Declare @ColumnList nvarchar(4000);
Declare @TotalCol nvarchar(4000);
declare @SelectColList nvarchar(4000);
declare @sql nvarchar(4000);

select @ColumnList = STRING_AGG(QUOTENAME(SegmentName), ','),
@SelectColList = STRING_AGG('CAST('+QUOTENAME(SegmentName) + 'AS VARCHAR(20)) AS '+ QUOTENAME(SegmentName), ','),
@TotalCol = STRING_AGG('ISNULL('+QUOTENAME(SegmentName)+',0)', '+')
from #tmp_Segment
where SegmentName in (select top 12 SegmentName 
					from #tmp_Market 
					where [Value] != 0 
					order by [Value] desc);

create table #Tmp_json
(JsonString nvarchar(max));

if @CaptureJson = 1
begin
set @sql = 'insert into #Tmp_json 
select (SELECT cast([Year]as varchar(10)) as Name, '+ @SelectColList + ', CAST('+ @TotalCol+ ' AS VARCHAR(20)) AS Average FROM 
(SELECT SegmentName, [Value], [Year]
FROM #tmp_Market
where [Value] != 0
) p  
PIVOT  
(  
SUM([Value])  
FOR SegmentName IN  
( ' + @ColumnList + ')  
) AS pvt
ORDER BY pvt.[Year]
FOR JSON PATH)'
exec sp_executesql @sql;
select JsonString from #Tmp_json 
end
else
begin
set @sql = 'SELECT cast([Year]as varchar(10)) as Name, '+ @SelectColList + ', CAST('+ @TotalCol+ ' AS VARCHAR(20)) AS Average FROM 
(SELECT SegmentName, [Value], [Year]
FROM #tmp_Market
where [Value] != 0
) p  
PIVOT  
(  
SUM([Value])  
FOR SegmentName IN  
( ' + @ColumnList + ')  
) AS pvt
ORDER BY pvt.[Year]
FOR JSON PATH'
exec sp_executesql @sql;
end
drop table #tmp_Market;
drop table #tmp_MarketValue;
drop table #tmp_Segment;
drop table #Tmp_json;

END