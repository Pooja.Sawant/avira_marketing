﻿using System;
using System.Collections.Generic;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using System.Text;
using System.Threading.Tasks;

namespace Avira.MarketingResearch.Common.IService.CompanyProfile
{
    public interface IEcoSystemPresenceService
    {
        IEnumerable<EcoSystemPresenceMapViewModel> GetEcoSystemPresenceMapByCompanyId(Guid CompanyId);
        IEnumerable<SectorPresenceMapViewModel> GetSectorPresenceMapByCompanyId(Guid CompanyId);
        IEnumerable<EcoSystemPresenceKeyCompViewModel> GetEcoSystemPresenceKeyCompByCompanyId(Guid CompanyId);
        Result<SpTransactionMessage> InsertUpdate(EcoSystemPresenceDbViewModel tfc, AviraUser currenUser);
    }
}
