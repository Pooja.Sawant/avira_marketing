﻿


CREATE procedure [dbo].[SApp_DeleteTrendKeyCompanyMap]      
(
@TrendId uniqueidentifier,   
@TrendKeyCompanymap dbo.[udtUniqueIdentifier] readonly,
@AviraUserId uniqueidentifier
)      
as      
/*================================================================================      
Procedure Name: SApp_DeleteTrendKeyCompanyMap      
Author: Praveen	      
Create date: 04/23/2019      
Description: Remove a record into TrendKeyCompanyMap table      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
04/23/2019 Praveen   Initial Version    
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)  
    
--Validations      
--IF exists(select 1 from [dbo].[TrendKeyCompanyMap]       
--   where TrendId = @TrendId AND CompanyId = @CompanyId AND IsDeleted = 0)      
--begin       
--set @ErrorMsg = 'KeyCompany with Id "'+ CONVERT (nvarchar(30), @CompanyId) + '" already exists.';    
--SELECT StoredProcedureName ='SApp_InsertTrendKeyCompanyMap',Message =@ErrorMsg,Success = 0;    
--RETURN;    
--end      
--End of Validations      
BEGIN TRY         
--mark records as delete which are not in list
--update [dbo].[TrendKeyCompanyMap]
--set IsDeleted = 1,
--UserDeletedById = @AviraUserId,
--DeletedOn = GETDATE()
--from [dbo].[TrendKeyCompanyMap]
--where [TrendKeyCompanyMap].TrendId = @TrendId
--and not exists (select 1 from @TrendKeyCompanymap map where map.id = [TrendKeyCompanyMap].CompanyId)

 --Update records for change
update [dbo].[TrendKeyCompanyMap]
set IsDeleted = 1,
ModifiedOn = GETDATE(),
UserModifiedById = @AviraUserId
from [dbo].[TrendKeyCompanyMap]
inner join @TrendKeyCompanymap map 
on map.id = [TrendKeyCompanyMap].CompanyId
where [TrendKeyCompanyMap].TrendId = @TrendId;
 
     
SELECT StoredProcedureName ='SApp_DeleteTrendKeyCompanyMap',Message =@ErrorMsg,Success = 1;       
    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber	int;
	DECLARE @ErrorSeverity	int;
	DECLARE @ErrorProcedure	varchar(100);
	DECLARE @ErrorLine	int;
	DECLARE @ErrorMessage	varchar(500);     
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());      
    
 set @ErrorMsg = 'Error while insert a new TrendKeyCompanyMap, please contact Admin for more details.';    
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
       
END CATCH      
      
      
end