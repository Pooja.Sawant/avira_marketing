﻿
CREATE PROCEDURE [dbo].[SApp_GetRegionByTrendID]      
 (@TrendID uniqueidentifier = null,  
 @includeDeleted bit = 0,  
 @OrdbyByColumnName NVARCHAR(50) ='RegionName',  
 @SortDirection INT = -1,  
 @PageStart INT = 0,  
 @PageSize INT = null)      
 AS      
/*================================================================================      
Procedure Name: SApp_GetRegionbyTrendID  
Author: Swami      
Create date: 04/12/2019      
Description: Get list from Region table by TrendID      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
04/12/2019 Swami   Initial Version      
================================================================================*/     
      
BEGIN      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED   
if @PageSize is null or @PageSize =0  
set @PageSize = 10  
   
begin  
;WITH CTE_TBL AS (  
 SELECT   
 --@TrendID as TrendID,    
 [Region].[Id] AS [RegionId],      
  [Region].[RegionName],     
   [Region].[RegionLevel],  
   [Region].IsDeleted,  
   [Region].ParentRegionId,  
   TrendRegionMap.Impact as ImpactId,
   TrendRegionMap.EndUser,
   TrendRegionMap.TrendId,
   TrendRegionMap.Id as TrendRegionMapId,
     cast (case when exists(select 1 from dbo.TrendRegionMap         
     where TrendRegionMap.TrendId = @TrendID        
     and TrendRegionMap.IsDeleted = 0        
     and TrendRegionMap.RegionId = Region.Id) then 1 else 0 end as bit) as IsMapped, 
 CASE  
  WHEN @OrdbyByColumnName = 'RegionName' THEN [Region].RegionName  
  WHEN @OrdbyByColumnName = 'RegionLevel' THEN cast([Region].[RegionLevel] as nvarchar(10))
  ELSE [Region].RegionName  
 END AS SortColumn  
  
FROM [dbo].Region
LEFT JOIN dbo.TrendRegionMap 
ON Region.Id = TrendRegionMap.RegionId  AND  TrendRegionMap.TrendId = @TrendID   
where (@includeDeleted = 1 or Region.IsDeleted = 0)  
)  
  
 SELECT   
    CTE_TBL.*,  
    tCountResult.TotalItems  
  FROM CTE_TBL  
  CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult  
  ORDER BY   
    CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,  
    CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC   
  OFFSET @PageStart ROWS  
  FETCH NEXT @PageSize ROWS ONLY;  
  END  
  
END