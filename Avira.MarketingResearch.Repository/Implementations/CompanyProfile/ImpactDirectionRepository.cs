﻿using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.CompanyProfile
{
    public class ImpactDirectionRepository : IImpactDirectionRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "ImpactDirection";
        public ImpactDirectionRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }
        public IEnumerable<ImpactDirectionViewModel> GetAll()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<ImpactDirectionViewModel>(ImpactDirectionQueries.StoreProcSelectAll
                            , null
                            , null, true, null, CommandType.StoredProcedure);
        }
    }
}
