﻿CREATE TABLE [dbo].[CompanyProduct] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [ProductName]      NVARCHAR (256)   NOT NULL,
    [LaunchDate]       DATETIME2 (7)    NULL,
    [Description]      NVARCHAR (512)   NULL,
    [Patents]          NVARCHAR (512)   NULL,
    [StatusId]         UNIQUEIDENTIFIER NULL,
    [ImageURL]         NVARCHAR (256)   NULL,
    [ImageDisplayName] NVARCHAR (256)   NULL,
    [ActualImageName]  NVARCHAR (256)   NULL,
    [CompanyId]        UNIQUEIDENTIFIER NULL,
    [ProjectId]        UNIQUEIDENTIFIER NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_CompanyProduct_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CompanyProduct] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CompanyProduct_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyProduct_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyProduct_AviraUser2] FOREIGN KEY ([UserDeletedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyProduct_ProductStatus] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[ProductStatus] ([Id]),
    CONSTRAINT [IX_CompanyProduct] UNIQUE NONCLUSTERED ([CompanyId] ASC, [ProjectId] ASC, [ProductName] ASC)
);



