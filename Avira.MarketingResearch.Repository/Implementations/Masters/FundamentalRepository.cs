﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces.Masters;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Avira.MarketingResearch.Repository.Implementations.Masters
{
    public class FundamentalRepository:IFundamentalRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "Fundamental";
        public FundamentalRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public FundamentalViewModel GetById(Guid guid)
        {
            FundamentalViewModel fundamentalViewModel = new FundamentalViewModel();
            fundamentalViewModel = GetAllFundamental(guid).FirstOrDefault();
            return fundamentalViewModel;
        }

        public IEnumerable<FundamentalViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search , bool includeDeleted)
        {
            return GetAllFundamental(null, PageStart, pageSize, SortDirection, OrdbyByColumnName,Search, includeDeleted);
        }

        private IEnumerable<FundamentalViewModel> GetAllFundamental(Guid? guid, int PageStart = 0, int pageSize = 0, int SortDirection = -1, string OrdbyByColumnName = "", string Search = "", bool includeDeleted = false)
        {

            DataTable fundamentalIdsTable = new DataTable();
            fundamentalIdsTable.Columns.Add("ID", typeof(Guid));

            if (guid != null)
            {
                fundamentalIdsTable.Rows.Add(guid);
            }
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<FundamentalViewModel>(FundamentalQueries.StoreProcSelectAll
                            , new { @FundamentalIDList = fundamentalIdsTable.AsTableValuedParameter("dbo.udtUniqueIdentifier"), @includeDeleted = includeDeleted, @PageStart = PageStart, @pageSize = pageSize, @SortDirection = SortDirection, @OrdbyByColumnName = OrdbyByColumnName, @FundamentalName=Search }
                            , null, true, null, CommandType.StoredProcedure);
        }
        
        public Result<SpTransactionMessage> Create(FundamentalInsertDbViewModel fundamentalInsertDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(FundamentalQueries.StoreProcInsert, fundamentalInsertDbViewModel, CommandType.StoredProcedure);
            return spTransactionMessage;

        }

        public Result<SpTransactionMessage> Update(FundamentalUpdateDbViewModel fundamentalUpdateDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(FundamentalQueries.StoreProcUpdate, fundamentalUpdateDbViewModel, CommandType.StoredProcedure);
            return spTransactionMessage;
        }
    }
}
