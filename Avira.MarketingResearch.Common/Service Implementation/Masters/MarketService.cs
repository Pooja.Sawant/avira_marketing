﻿using Avira.MarketingResearch.Common.IService.Masters;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces.Masters;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;

namespace Avira.MarketingResearch.Common.Service_Implementation.Masters
{
    public class MarketService : IMarketService
    {
        private readonly IMarketRepository _marketRepository;

        public MarketService(IMarketRepository buildLevelRepository)
        {
            _marketRepository = buildLevelRepository;
        }

        public Result<SpTransactionMessage> Create(MarketInsertDbViewModel tfc, AviraUser currentUser)
        {
            return _marketRepository.Create(tfc);
        }


        public IEnumerable<MarketViewModel> GetAll(Guid Id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<MarketViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false)
        {
            return _marketRepository.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName,Search,isDeletedInclude);
        }

        public IEnumerable<MarketViewModel> GetAllParentMarket()
        {
            return _marketRepository.GetAllParentMarket();        
        }

        public MarketViewModel GetById(Guid guid)
        {
            return _marketRepository.GetById(guid);
        }


        public Result<SpTransactionMessage> Update(MarketUpdateDbViewModel tfc, AviraUser currenUser)
        {
            return _marketRepository.Update(tfc);
        }
    }
}
