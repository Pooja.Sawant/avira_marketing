﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.Trend
{
    public class TrendRegionMapViewModel
    {
        public Guid TrendId { get; set; }
        public Guid RegionId { get; set; }
        public Guid ImpactId { get; set; }
        public string RegionName { get; set; }
        public bool IsMapped { get; set; }
        public int TotalItems { get; set; }
        public List<CountryViewModel> CountryList { get; set; }
        //public List<RegionCountryMapModel> RegionCountryMapModel { get; set; }
        //public List<CountryViewModel> CountryList { get; set; }
        public string RowType
        {
            get
            {
                return "RegionMap";
            }
        }
    }

    public class MainTrendRegionMapInsertUpdateDbViewModel
    {
        public Guid TrendId { get; set; }
        public string RegionName { get; set; }
        public List<TrendRegionMapViewModel> TrendRegionMapInsertUpdate { get; set; }

        public List<List<TrendRegionMapViewModel>> TrendRegionMapInsertUpdateTile { get; set; }

        public Guid AviraUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public TrendNoteInsertUpdateDbViewModel TrendNote { get; set; }
        public TrendViewModel TrendModel { get; set; }
        public TrendNoteViewModel TrendNoteModel { get; set; }
    }

    public class RegionCountryMapModel
    {
        public Guid RegionId { get; set; }
        public string RegionName { get; set; }
        public int RegionLevel { get; set; }
        public bool? IsDeleted { get; set; }
        public Guid ParentRegionId { get; set; }
        public string RegionImpact { get; set; }
        public bool RegionEndUser { get; set; }
        public Guid TrendId { get; set; }
        public Guid TrendRegionMapId { get; set; }
        public int IsAssigned { get; set; }
        public List<CountryViewModel> CountryList { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "Region";
            }
        }

    }
}
