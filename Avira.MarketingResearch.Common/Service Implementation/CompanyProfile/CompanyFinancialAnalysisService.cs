﻿using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Avira.MarketingResearch.Common.Service_Implementation.CompanyProfile
{
    public class CompanyFinancialAnalysisService: ICompanyFinancialAnalysisService
    {
        private readonly ICompanyFinancialAnalysisRepository _companyFinancialAnalysisRepository;
     

        public CompanyFinancialAnalysisService(ICompanyFinancialAnalysisRepository companyFinancialAnalysisRepository)
        {
            _companyFinancialAnalysisRepository = companyFinancialAnalysisRepository;
           
        }
        
        public Task<MainCompanyFinancialAnalysisInsertUpdateViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false, string id = "")
        {
            return _companyFinancialAnalysisRepository.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude = false, id);
        }

        public IEnumerable<ValueConversionViewModel> GetAllValueConversion()
        {
            return _companyFinancialAnalysisRepository.GetAllValueConversion();
        }

        public IEnumerable<CurrencyViewModel> GetAllCurrencies()
        {
            return _companyFinancialAnalysisRepository.GetAllCurrencies();
        }
        public IEnumerable<CategoryViewModel> GetAllCategories()
        {
            return _companyFinancialAnalysisRepository.GetAllCategories();
        }

        public Result<SpTransactionMessage> InsertUpdate(CompanyFinancialAnalysisInsertUpdateModel tfc, AviraUser currenUser)
        {
            var compId = Guid.NewGuid();
            //tfc.Id = compId;
            //tfc.CompanyFinancialId = compId;
            tfc.companyTransactionsInsertUpdateViewModel.ForEach(i => i.UserCreatedById = currenUser.Id);
            //tfc.companyTransactionsInsertUpdateViewModel.ForEach(i => i.CompanyFinancialID = compId);
            //tfc.assetsLiabilitiesViewModels.ForEach(i => i.CompanyFinancialID = compId);
            //tfc.cashFlowsViewModel.ForEach(i => i.CompanyFinancialID = compId);
            //tfc.expensesBreakdownViewModel.ForEach(i => i.CompanyFinancialID = compId);
            tfc.UserCreatedById = currenUser.Id;
            //tfc.companyTransactionsInsertUpdateViewModel.ForEach(i => i.CompanyFinancialID = compId);
            //tfc.companyTransactionsInsertUpdateViewModel.ForEach(i => i.UserCreatedById = currenUser.Id);

            tfc.jsonAssets = JsonConvert.SerializeObject(tfc.assetsLiabilitiesViewModels);
            tfc.jsonCash = JsonConvert.SerializeObject(tfc.cashFlowsViewModel);
            tfc.jsonExpence = JsonConvert.SerializeObject(tfc.expensesBreakdownViewModel);
            tfc.jsonTrans = JsonConvert.SerializeObject(tfc.companyTransactionsInsertUpdateViewModel);

            var res = _companyFinancialAnalysisRepository.InsertUpdate(tfc);  
            return res;
        }
    }
}
