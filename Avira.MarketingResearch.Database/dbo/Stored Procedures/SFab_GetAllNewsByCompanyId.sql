﻿CREATE procedure [dbo].[SFab_GetAllNewsByCompanyId]  
(  
@ProjectId uniqueidentifier = null,  
@CompanyID uniqueidentifier = null,  
@OrdbyByColumnName NVARCHAR(50) ='Date',  
@SortDirection INT = -1,  
@AviraUserID uniqueidentifier = null,  
@NoOfRow INT = -1,  
@NoDefaultFilter bit = 0  
)  
as  
/*================================================================================  
Procedure Name: [dbo].[SFab_GetAllNewsByCompanyId]  
Author: Praveen  
Create date: 07/08/2019  
Description: Get list from News table by filter  
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
07/08/2019 Praveen   Initial Version  
================================================================================*/  
BEGIN  
SET NOCOUNT ON;  
declare @ProjectApprovedStatusId uniqueidentifier
select @ProjectApprovedStatusId = ID from ProjectStatus
where ProjectStatusName = 'Completed';

IF(@NoDefaultFilter = 0)  
BEGIN  
  SELECT    
News.Id,   
  News.Date,      
  News.News,   
  News.NewsCategoryId,   
  News.CompanyID,    
  News.ImpactDirectionId,  
  NewsCategory.NewsCategoryName,  
  News.ImportanceId,  
  Importance.GroupName as ImportanceName  
  
FROM News   
INNER JOIN NewsCategory ON News.NewsCategoryId = NewsCategory.Id  
LEFT JOIN Importance ON News.ImportanceId = Importance.Id  
WHERE News.CompanyID = @CompanyID --AND News.ProjectID = @ProjectId  
--and exists (select 1 from project 
--			where project.ID = News.ProjectID
--			and project.ProjectStatusID = @ProjectApprovedStatusId)
ORDER BY   
CASE WHEN @SortDirection = 1 AND @OrdbyByColumnName = 'Date' THEN   
   News.Date END ASC,  
CASE WHEN @SortDirection = 1 AND @OrdbyByColumnName = 'Importance' THEN   
   Importance.Sequence END ASC,  
  
CASE WHEN @SortDirection < 0 AND @OrdbyByColumnName = 'Date' THEN   
   News.Date END DESC,  
CASE WHEN @SortDirection < 0 AND @OrdbyByColumnName = 'Importance' THEN   
   Importance.Sequence END DESC  
  
 END  
 BEGIN  
   
 SELECT * FROM (  
SELECT    
ROW_NUMBER() OVER (ORDER BY News.Date DESC, News.News) AS rownumber,  
News.Id,   
  News.Date,    
  News.News,   
  News.NewsCategoryId,   
  News.CompanyID,    
  News.ImpactDirectionId,  
  NewsCategory.NewsCategoryName,  
  News.ImportanceId,  
  Importance.ImportanceName  
  
FROM News   
INNER JOIN NewsCategory ON News.NewsCategoryId = NewsCategory.Id  
LEFT JOIN Importance ON News.ImportanceId = Importance.Id  
WHERE News.CompanyID = @CompanyID --AND News.ProjectID = @ProjectId  
--and exists (select 1 from project 
--			where project.ID = News.ProjectID
--			and project.ProjectStatusID = @ProjectApprovedStatusId)
 ) AS NEWS  
  
WHERE rownumber <= CASE WHEN @NoOfRow < 1 THEN rownumber ELSE @NoOfRow END ORDER BY rownumber  
  
 END  
END