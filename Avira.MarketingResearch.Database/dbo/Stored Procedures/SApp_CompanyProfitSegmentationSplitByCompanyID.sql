﻿-- =============================================          
-- Author:  Laxmikant          
-- Create date: 05/29/2019          
-- Description: Get Profit Company total revenue by Company Id    
-- =============================================          
CREATE PROCEDURE [dbo].[SApp_CompanyProfitSegmentationSplitByCompanyID]               
 @ComapanyID uniqueidentifier = null, 
 @includeDeleted bit = 0               
AS          
BEGIN           
 SET NOCOUNT ON;            
 SELECT   
 [CompanyProfitSegmentationSplitMap].[Id]    
 ,[CompanyProfitSegmentationSplitMap].[CompanyProfitId]    
 ,[CompanyProfitSegmentationSplitMap].[Split]    
 ,[CompanyProfitSegmentationSplitMap].[Segmentation]    
 ,[CompanyProfitSegmentationSplitMap].[SegmentValue]    
 ,[CompanyProfitSegmentationSplitMap].[Year]    
 ,[CompanyProfitSegmentationSplitMap].[CreatedOn]    
 ,[CompanyProfitSegmentationSplitMap].[UserCreatedById]    
 ,[CompanyProfitSegmentationSplitMap].[ModifiedOn]    
 ,[CompanyProfitSegmentationSplitMap].[UserModifiedById]    
 ,[CompanyProfitSegmentationSplitMap].[IsDeleted]    
 ,[CompanyProfitSegmentationSplitMap].[DeletedOn]    
 ,[CompanyProfitSegmentationSplitMap].[UserDeletedById]    
  FROM [dbo].[CompanyProfitSegmentationSplitMap]          
       
  WHERE ([CompanyProfitSegmentationSplitMap].CompanyId = @ComapanyID) --and [CompanyRevenue].[projectId] = @ProjectID)                
 AND (@includeDeleted = 0 or [CompanyProfitSegmentationSplitMap].IsDeleted = 1)            
END