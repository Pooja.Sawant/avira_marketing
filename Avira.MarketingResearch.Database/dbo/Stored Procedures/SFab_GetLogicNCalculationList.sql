﻿
  
CREATE PROCEDURE [dbo].[SFab_GetLogicNCalculationList]
(
@SegmentID uniqueidentifier=null,
@ProjectID uniqueidentifier=null
)  
AS  
/*================================================================================  
Procedure Name: [SFab_GetLogicNCalculationList]  
Author: Pooja Sawant
Create date: 06/19/2019  
Description: Get list of Logic n calculation from table   
Change History  
Date        Developer     Reason  
__________  ____________  ______________________________________________________  
06/19/2019   Pooja   Initial Version  
================================================================================*/  
BEGIN            
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED    
    
SELECT [ProjectID]
      ,[CalculationLogicDescription] from ProjectCalculationLogic 
INNER JOIN Project
ON ProjectCalculationLogic.ProjectID = Project.Id  

	WHERE ProjectCalculationLogic.ProjectID = @ProjectID and ProjectCalculationLogic.IsDeleted=0
	
        
END