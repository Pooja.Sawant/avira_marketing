﻿
CREATE procedure [dbo].[SApp_UpdateComponent]
(@Id uniqueidentifier,
@Description nvarchar(256),
@ComponentName nvarchar(256),
@IsDeleted bit=0,  
@UserModifiedById  uniqueidentifier,  
@DeletedOn datetime = null,  
@UserDeletedById uniqueidentifier = null,  
@ModifiedOn Datetime )
as
/*================================================================================
Procedure Name: SApp_UpdateComponent
Author: Sharath
Create date: 02/19/2019
Description: update a record in Component table by ID. Also used for delete
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
03/16/2019	Adarsh Dubey			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

IF exists(select 1 from 
--[dbo].[Component] 
[dbo].SubSegment [Components]
INNER JOIN Segment ON [Components].SegmentId = Segment.Id AND Segment.SegmentName = 'Components'

		 where [Components].[Id] !=  @Id
		 and  SubSegmentName = @ComponentName AND [Components].IsDeleted = 0)
begin
declare @ErrorMsg NVARCHAR(2048);
set @ErrorMsg = 'Component with Name "'+ @ComponentName + '" already exists.';

SELECT StoredProcedureName ='SApp_UpdateComponent','Message' =@ErrorMsg,'Success' = 0;  
RETURN;
end    
  
BEGIN TRY 

--update record
update [dbo].SubSegment
set SubSegmentName = @ComponentName,
	[Description] = @Description,
	[ModifiedOn] = getdate(),
	DeletedOn = case when IsDeleted = 0 and @IsDeleted = 1 then @DeletedOn else DeletedOn end,
	UserDeletedById = case when IsDeleted = 0 and @IsDeleted = 1 then @UserDeletedById else UserDeletedById end,
	IsDeleted = isnull(@IsDeleted, IsDeleted),
	[UserModifiedById] = @UserModifiedById
    where ID = @Id

SELECT StoredProcedureName ='SApp_UpdateComponent',Message =@ErrorMsg,Success = 1;     
  
END TRY    
BEGIN CATCH    
    -- Execute the error retrieval routine.    
 DECLARE @ErrorNumber int;    
 DECLARE @ErrorSeverity int;    
 DECLARE @ErrorProcedure varchar(100);    
 DECLARE @ErrorLine int;    
 DECLARE @ErrorMessage varchar(500);    
    
  SELECT @ErrorNumber = ERROR_NUMBER(),    
        @ErrorSeverity = ERROR_SEVERITY(),    
        @ErrorProcedure = ERROR_PROCEDURE(),    
        @ErrorLine = ERROR_LINE(),    
        @ErrorMessage = ERROR_MESSAGE()    
    
 insert into dbo.Errorlog(ErrorNumber,    
       ErrorSeverity,    
       ErrorProcedure,    
       ErrorLine,    
       ErrorMessage,    
       ErrorDate)    
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE()); 

set @ErrorMsg = 'Error while updating Component, please contact Admin for more details.' 		   
 --set @ErrorMsg = 'Unspecified Error';    
--THROW 50000,@ErrorMsg,1;    
 --return(1)  
 SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;     
END CATCH    
end