﻿CREATE TABLE [dbo].[BaseDecision] (
    [Id]               UNIQUEIDENTIFIER CONSTRAINT [DF__BaseDecision__Id__5344D5FE] DEFAULT (newid()) NOT NULL,
    [Description]      NVARCHAR (1024)  NOT NULL,
    [TimeTagOutlookId] UNIQUEIDENTIFIER NULL,
    [DecisionTypeId]   UNIQUEIDENTIFIER NULL,
    [StatusId]         UNIQUEIDENTIFIER NULL,
    [Score]            DECIMAL (19, 2)  NULL,
    [CreatedOn]        DATETIME2 (7)    CONSTRAINT [DF__BaseDecis__Creat__562142A9] DEFAULT (getdate()) NOT NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK__BaseDeci__3214EC07BCFEFD42] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__BaseDecis__Decis__552D1E70] FOREIGN KEY ([DecisionTypeId]) REFERENCES [dbo].[GrowthType] ([Id]),
    CONSTRAINT [FK__BaseDecis__TimeT__5438FA37] FOREIGN KEY ([TimeTagOutlookId]) REFERENCES [dbo].[TimeTag] ([Id]),
    CONSTRAINT [FK__BaseDecis__UserC__571566E2] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK__BaseDecis__UserD__58FDAF54] FOREIGN KEY ([UserDeletedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK__BaseDecis__UserM__58098B1B] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_BaseDecision_LookupStatus] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[LookupStatus] ([Id])
);

