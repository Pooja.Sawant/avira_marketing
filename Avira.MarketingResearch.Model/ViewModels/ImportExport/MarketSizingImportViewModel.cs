﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.ImportExport
{
    public class MarketSizingImportViewModel
    {
        public int RowNo { get; set; }
        public string Project { get; set; }
        public string Market { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }
        public string Currency { get; set; }
        public string Value { get; set; }
        public string ASPUnit { get; set; }
        public string VolumeUnit { get; set; }
        //public string RawMaterial { get; set; }
        //public string Component { get; set; }
        //public string Processes { get; set; }
        //public string CompanyProduct { get; set; }
        //public string DeviceAndSolution { get; set; }
        //public string Size { get; set; }
        //public string Financing { get; set; }
        //public string Logistics { get; set; }
        //public string Distribution { get; set; }
        //public string Units { get; set; }
        //public string Dimensions { get; set; }
        //public string CommunicationProtocol { get; set; }
        //public string CommunicationTechnology { get; set; }
        //public string PriceRange { get; set; }
        //public string MountingType { get; set; }
        //public string PowerSource { get; set; }
        //public string Equiptment { get; set; }
        //public string PartsandAccessories { get; set; }
        //public string Rating { get; set; }
        //public string Class { get; set; }
        //public string From1 { get; set; }
        //public string SalesChannels { get; set; }
        //public string Technology { get; set; }
        //public string Sensors { get; set; }
        //public string Application { get; set; }
        //public string EndUser { get; set; }

        public string SegmentMapJson { get; set; }

        public string ASP_2018 { get; set; }
        public string Value_2018 { get; set; }
        public string Volume_2018 { get; set; }
        public string OranicGrowth_2018 { get; set; }
        public string TrendsGrowth_2018 { get; set; }
        public string Source1_2018 { get; set; }
        public string Source2_2018 { get; set; }
        public string Source3_2018 { get; set; }
        public string Source4_2018 { get; set; }
        public string Source5_2018 { get; set; }
        public string Rationale_2018 { get; set; }

        public string ASP_2019 { get; set; }
        public string Value_2019 { get; set; }
        public string Volume_2019 { get; set; }
        public string OranicGrowth_2019 { get; set; }
        public string TrendsGrowth_2019 { get; set; }
        public string Source1_2019 { get; set; }
        public string Source2_2019 { get; set; }
        public string Source3_2019 { get; set; }
        public string Source4_2019 { get; set; }
        public string Source5_2019 { get; set; }
        public string Rationale_2019 { get; set; }

        public string ASP_2020 { get; set; }
        public string Value_2020 { get; set; }
        public string Volume_2020 { get; set; }
        public string OranicGrowth_2020 { get; set; }
        public string TrendsGrowth_2020 { get; set; }
        public string Source1_2020 { get; set; }
        public string Source2_2020 { get; set; }
        public string Source3_2020 { get; set; }
        public string Source4_2020 { get; set; }
        public string Source5_2020 { get; set; }
        public string Rationale_2020 { get; set; }

        public string ASP_2021 { get; set; }
        public string Value_2021 { get; set; }
        public string Volume_2021 { get; set; }
        public string OranicGrowth_2021 { get; set; }
        public string TrendsGrowth_2021 { get; set; }
        public string Source1_2021 { get; set; }
        public string Source2_2021 { get; set; }
        public string Source3_2021 { get; set; }
        public string Source4_2021 { get; set; }
        public string Source5_2021 { get; set; }
        public string Rationale_2021 { get; set; }

        public string ASP_2022 { get; set; }
        public string Value_2022 { get; set; }
        public string Volume_2022 { get; set; }
        public string OranicGrowth_2022 { get; set; }
        public string TrendsGrowth_2022 { get; set; }
        public string Source1_2022 { get; set; }
        public string Source2_2022 { get; set; }
        public string Source3_2022 { get; set; }
        public string Source4_2022 { get; set; }
        public string Source5_2022 { get; set; }
        public string Rationale_2022 { get; set; }

        public string ASP_2023 { get; set; }
        public string Value_2023 { get; set; }
        public string Volume_2023 { get; set; }
        public string OranicGrowth_2023 { get; set; }
        public string TrendsGrowth_2023 { get; set; }
        public string Source1_2023 { get; set; }
        public string Source2_2023 { get; set; }
        public string Source3_2023 { get; set; }
        public string Source4_2023 { get; set; }
        public string Source5_2023 { get; set; }
        public string Rationale_2023 { get; set; }

        public string ASP_2024 { get; set; }
        public string Value_2024 { get; set; }
        public string Volume_2024 { get; set; }
        public string OranicGrowth_2024 { get; set; }
        public string TrendsGrowth_2024 { get; set; }
        public string Source1_2024 { get; set; }
        public string Source2_2024 { get; set; }
        public string Source3_2024 { get; set; }
        public string Source4_2024 { get; set; }
        public string Source5_2024 { get; set; }
        public string Rationale_2024 { get; set; }

        public string ASP_2025 { get; set; }
        public string Value_2025 { get; set; }
        public string Volume_2025 { get; set; }
        public string OranicGrowth_2025 { get; set; }
        public string TrendsGrowth_2025 { get; set; }
        public string Source1_2025 { get; set; }
        public string Source2_2025 { get; set; }
        public string Source3_2025 { get; set; }
        public string Source4_2025 { get; set; }
        public string Source5_2025 { get; set; }
        public string Rationale_2025 { get; set; }

        public string ASP_2026 { get; set; }
        public string Value_2026 { get; set; }
        public string Volume_2026 { get; set; }
        public string OranicGrowth_2026 { get; set; }
        public string TrendsGrowth_2026 { get; set; }
        public string Source1_2026 { get; set; }
        public string Source2_2026 { get; set; }
        public string Source3_2026 { get; set; }
        public string Source4_2026 { get; set; }
        public string Source5_2026 { get; set; }
        public string Rationale_2026 { get; set; }

        public string ASP_2027 { get; set; }
        public string Value_2027 { get; set; }
        public string Volume_2027 { get; set; }
        public string OranicGrowth_2027 { get; set; }
        public string TrendsGrowth_2027 { get; set; }
        public string Source1_2027 { get; set; }
        public string Source2_2027 { get; set; }
        public string Source3_2027 { get; set; }
        public string Source4_2027 { get; set; }
        public string Source5_2027 { get; set; }
        public string Rationale_2027 { get; set; }

        public string ASP_2028 { get; set; }
        public string Value_2028 { get; set; }
        public string Volume_2028 { get; set; }
        public string OranicGrowth_2028 { get; set; }
        public string TrendsGrowth_2028 { get; set; }
        public string Source1_2028 { get; set; }
        public string Source2_2028 { get; set; }
        public string Source3_2028 { get; set; }
        public string Source4_2028 { get; set; }
        public string Source5_2028 { get; set; }
        public string Rationale_2028 { get; set; }
        public string CAGR_3Years { get; set; }
        public string CAGR_5Years { get; set; }
        public string Total_CAGR { get; set; }
        public string ErrorNotes { get; set; }
        public string Message { get; set; }
        public bool Success { get; set; }
    }
    public class MarketSizingImportInsertViewModel
    {
        public string TemplateName { get; set; }
        public string ImportFileName { get; set; }
        public string TrendJson { get; set; }
        public Guid ImportId { get; set; }
        public Guid UserCreatedById { get; set; }
        public IEnumerable<MarketSizingImportViewModel> MarketSizingsImportViewModel { get; set; }
    }


    public class MarketSizingImportSegmentModel
    {
        public string SegmentName { get; set; }
        public string SubSegment { get; set; }
    }
}
