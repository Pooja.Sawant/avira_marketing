﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.ImportExport;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyBasicInfoImportExportController : AviraBaseController
    {
        private ILoggerManager _logger;
        public readonly ICompanyBasicInfoImportExportService _companyBasicInfoImportExportService;

        public CompanyBasicInfoImportExportController(
            ICompanyBasicInfoImportExportService companyBasicInfoImportExportService,
            ILoggerManager logger)
        {
            _companyBasicInfoImportExportService = companyBasicInfoImportExportService;
            _logger = logger;
        }

        [HttpPost]
        public ActionResult Post([FromBody] CompanyBasicInfoImportInsertModel viewModel)
        {
            viewModel.UserCreatedById = CurrentUser.Id;
            ImportCommonResponseMainModel viewData = new ImportCommonResponseMainModel()
            {
                IsSuccess = false,
                Message = "Error while creating a new Qualitative Analysis, please contact Admin for more details."
            };
            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                IEnumerable<ImportCommonResponseModel> result = _companyBasicInfoImportExportService.Create(viewModel, CurrentUser);

                var res = result.Where(s => s.ErrorNotes != null).ToList();

                if (res.Count <= 0)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Company Basic Info imported successfully.";
                }
                else
                {
                    viewData.Message = "Error occured. Company Basic Info import.";
                    viewData.ResponseModel = result;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);

                viewData = new ImportCommonResponseMainModel()
                {
                    IsSuccess = false,
                    Message = "Error while importing company basic info. Error Details: " + ex.Message + ". please contact Admin for more details."
                };
            }
            return Ok(viewData);
        }
    }
}