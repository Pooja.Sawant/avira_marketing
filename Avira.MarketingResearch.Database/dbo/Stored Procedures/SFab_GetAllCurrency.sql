﻿
  
CREATE PROCEDURE [dbo].[SFab_GetAllCurrency]    
(@UserId uniqueidentifier =  null)
 AS  
/*================================================================================  
Procedure Name: [SFab_GetAllCurrency]  
Author: Pooja Sawant  
Create date: 05/28/2019  
Description: Get list of Currency from table  
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
05/28/2019 Pooja   Initial Version  
================================================================================*/  
  
BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
	
	select * from Currency 
	WHERE IsDeleted = 0 
	order by CurrencyName asc

END