﻿CREATE TABLE [dbo].[StagingCompanyEcosystemSegmentMap] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [ImportId]         UNIQUEIDENTIFIER NOT NULL,
    [CompanyId]        UNIQUEIDENTIFIER NULL,
    [SegmentName]      NVARCHAR (MAX)   NULL,
    [SubSegmentName]   NVARCHAR (MAX)   NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [RowNo]            INT              NULL,
    [ErrorNotes]       NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_StagingCompanyEcosystemSegmentMap] PRIMARY KEY CLUSTERED ([Id] ASC)
);

