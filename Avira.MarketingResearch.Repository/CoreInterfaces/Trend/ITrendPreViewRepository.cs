﻿using Avira.MarketingResearch.Model.ViewModels.Trend;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.Trend
{
    public interface ITrendPreviewRepository
    {
        TrendPreviewViewModel GetById(Guid guid);
    }
}
