﻿CREATE TABLE [dbo].[NLogExceptionTest] (
    [id]                INT            IDENTITY (1, 1) NOT NULL,
    [timestamp]         DATETIME       NOT NULL,
    [level]             VARCHAR (100)  NOT NULL,
    [logger]            VARCHAR (1000) NOT NULL,
    [message]           VARCHAR (3600) NOT NULL,
    [userid]            NVARCHAR (100) NULL,
    [exception]         VARCHAR (3600) NULL,
    [stacktrace]        VARCHAR (3600) NULL,
    [AppEnviromentType] VARCHAR (250)  NULL,
    [SiteId]            SMALLINT       NULL,
    CONSTRAINT [PK_ExceptionLog] PRIMARY KEY CLUSTERED ([id] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'1: Authoring Tool API, 2: Authoring Tool Web, 3: Fabric API, 4: Fabric Web', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'NLogExceptionTest', @level2type = N'COLUMN', @level2name = N'SiteId';

