﻿CREATE procedure [dbo].[SApp_GetMenusByUserRoleId]            
(@UserRoleId uniqueidentifier)            
as            
/*================================================================================            
Procedure Name: [SApp_GetMenusByUserRolesId]            
Author: Gopi            
Create date: 04/10/2019            
Description: Get Menus Based on logged in User Role        
Change History            
Date  Developer  Reason            
__________ ____________ ______________________________________________________            
04/10/2019 Gopi   Initial Version            
================================================================================*/            
BEGIN            
SET NOCOUNT ON;            
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;            
        
--Menus        
        
;with menu_src as (        
select Id as MenuID,        
MenuName,        
ParentMenuID        
from Menu        
where exists (select 1 from MenuFormMap        
   inner join FormPermissionMap        
   on MenuFormMap.FormId = FormPermissionMap.FormId         
   inner join UserRolePermissionMap        
   on FormPermissionMap.PermissionId = UserRolePermissionMap.PermissionId        
   where UserRolePermissionMap.UserRoleId = @UserRoleId      
   and menu.Id = MenuFormMap.MenuId)        
      
union all        
select ParentMenu.Id as MenuID,        
ParentMenu.MenuName,        
ParentMenu.ParentMenuID        
from menu ParentMenu        
inner join menu_src        
on ParentMenu.Id = menu_src.ParentMenuID        
)        
select distinct         
MenuID,        
MenuName,        
ParentMenuID        
 from menu_src        
        
END