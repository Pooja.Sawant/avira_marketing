﻿CREATE procedure [dbo].[SFab_GetCompanyComparisonDetailsbyProjectID]
(
@SegmentList [dbo].[udtUniqueIdentifier] READONLY,
@LocationList [dbo].[udtUniqueIdentifier] READONLY,
@CompanystageId UniqueIdentifier,
@StratergyList [dbo].[udtUniqueIdentifier] READONLY,
@Years nvarchar(100),
@ProjectId uniqueidentifier,
@AviraUserID uniqueidentifier = null
)
as
/*================================================================================
Procedure Name: [SFab_GetCompanyComparisonDetailsbyProjectID]
Author: Sai
Create date: 07/29/2019
Description: Get list from company table by project ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
07/29/2019	Sai Krishna	    Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ProjectApprovedStatusId uniqueidentifier
select @ProjectApprovedStatusId = ID from ProjectStatus
where ProjectStatusName = 'Completed';

declare @RegionList dbo.[udtUniqueIdentifier];
declare @YearList table (Year int);
insert into @YearList
select value
from string_split(@Years,',')

insert into @RegionList
select Id
from Region
where id in (select id from @LocationList)
or exists (select 1
			from CountryRegionMap CRM
			inner join @LocationList Loc
			on CRM.CountryId = Loc.ID
			and Region.id = CRM.RegionId)
			;

--Fundamentals
	 select comp.Id as CompanyId,
	 comp.CompanyName,
	 comp.CompanyCode,
	 comp.DisplayName,
	 comp.NatureId,
	 Nature.NatureTypeName as NatureTypeName,
	 (select STRING_AGG(sub.CompanyName, ',')
	 from Company sub
	 where sub.ParentId = comp.Id) as CompanySubsidaryNames,
	 comp.ParentId,
	 comp.ParentCompanyName,
	 comp.CompanyLocation,
	 comp.IsHeadQuarters,
	 comp.YearOfEstb,
	 comp.CompanyStageId,
	 CompanyStage.CompanyStageName,
	 comp.ActualImageName as ImageActualName
	from Company comp
	inner join Nature
	on comp.NatureId = Nature.Id
	inner join CompanyStage
	on comp.CompanyStageId = CompanyStage.Id
	where comp.IsDeleted = 0 
	and exists (select 1 from CompanyProjectMap cpmap
				inner join project proj
				on cpmap.ProjectId = proj.ID
				where comp.Id = cpmap.CompanyId 
				and proj.ProjectStatusID = @ProjectApprovedStatusId
	and cpmap.ProjectId = @ProjectId)
	and (not exists (select * from @LocationList) 
		or exists(select 1 from CompanyRegionMap CRM
				  inner join @RegionList Rlist
				  on CRM.RegionId = Rlist.ID))
	and (@CompanystageId is null or comp.CompanyStageId = @CompanystageId)
	and (not exists (select * from @StratergyList) 
		or exists(select 1 from [CompanyStrategyMapping] CSM
				inner join  @StratergyList CSlist
				on CSM.Category = CSlist.ID
				where comp.Id = CSM.CompanyId))
	and (not exists(select 1 from @SegmentList)
		or exists(select 1 from CompanyProduct CP
					inner join CompanyProductSegmentMap CPSM
					on cp.Id=cpsm.CompanyProductId 
					inner join @SegmentList list
					on list.ID = CPSM.SegmentId
					where cp.CompanyId = comp.Id));

--Segment
;With Clist as (select comp.Id as CompanyId,
	 comp.CompanyName
	from Company comp
	where comp.IsDeleted = 0 
	and exists (select 1 from CompanyProjectMap cpmap
				inner join project proj
				on cpmap.ProjectId = proj.ID
				where comp.Id = cpmap.CompanyId 
				and proj.ProjectStatusID = @ProjectApprovedStatusId
				and cpmap.ProjectId = @ProjectId 
				and comp.IsDeleted = 0 )
	and (not exists (select * from @LocationList) 
		or exists(select 1 from CompanyRegionMap CRM
				  inner join @RegionList Rlist
				  on CRM.RegionId = Rlist.ID
				  where CRM.CompanyId = comp.Id))
	and (@CompanystageId is null or  comp.CompanyStageId = @CompanystageId)
	and (not exists (select * from @StratergyList) 
		or exists(select 1 from [CompanyStrategyMapping] CSM
				inner join  @StratergyList CSlist
				on CSM.Category = CSlist.ID
				where comp.Id = CSM.CompanyId))
	and (not exists(select 1 from @SegmentList)
		or exists(select 1 from CompanyProduct CP
					inner join CompanyProductSegmentMap CPSM
					on cp.Id=cpsm.CompanyProductId 
					inner join @SegmentList list
					on list.ID = CPSM.SegmentId
					where cp.CompanyId = comp.Id))
	and (@Years is null or @Years = '' 
		or exists(select 1 from CompanyRevenue
					inner join @YearList Ylist
					on Ylist.Year = CompanyRevenue.Year
					and CompanyRevenue.CompanyId = comp.Id)))
select Clist.CompanyId,
Clist.CompanyName,
[TableType],
[TableName],
[Rows],
[Columns],
[TableMetadata],
[TableData]
from Clist
inner join CompanyProfileRevenueTableTab Revenue
on Clist.CompanyId = Revenue.CompanyId
where [TableType] = 'IncomeStatement'
--ProfitAndLossAnalysis
--IncomeStatement
--BalancedSheet
--CashFlow

--Products
;With Clist as (select comp.Id as CompanyId,
	 comp.CompanyName
	from Company comp
	where comp.IsDeleted = 0 
	and exists (select 1 from CompanyProjectMap cpmap
				inner join project proj
				on cpmap.ProjectId = proj.ID
				where comp.Id = cpmap.CompanyId 
				and proj.ProjectStatusID = @ProjectApprovedStatusId
				and cpmap.ProjectId = @ProjectId 
				and comp.IsDeleted = 0 )
	and (not exists (select * from @LocationList) 
		or exists(select 1 from CompanyRegionMap CRM
				  inner join @RegionList Rlist
				  on CRM.RegionId = Rlist.ID
				  where CRM.CompanyId = comp.Id))
	and (@CompanystageId is null or  comp.CompanyStageId = @CompanystageId)
	and (not exists (select * from @StratergyList) 
		or exists(select 1 from [CompanyStrategyMapping] CSM
				inner join  @StratergyList CSlist
				on CSM.Category = CSlist.ID
				where comp.Id = CSM.CompanyId))
	and (not exists(select 1 from @SegmentList)
		or exists(select 1 from CompanyProduct CP
					inner join CompanyProductSegmentMap CPSM
					on cp.Id=cpsm.CompanyProductId 
					inner join @SegmentList list
					on list.ID = CPSM.SegmentId
					where cp.CompanyId = comp.Id))
	and (@Years is null or @Years = '' 
		or exists(select 1 from CompanyRevenue
					inner join @YearList Ylist
					on Ylist.Year = CompanyRevenue.Year
					and CompanyRevenue.CompanyId = comp.Id)))
select cp.CompanyId,
Clist.CompanyName,
cp.Id as CompanyProductId,
cp.ProductName,
cp.ImageURL,
cp.ImageDisplayName,
cp.ActualImageName,
cp.StatusId,
ps.ProductStatusName,
cp.Description,
cp.LaunchDate,
cp.Patents,
(select string_agg(Country.CountryName, ',') from [dbo].[CompanyProductCountryMap] CPCM
inner join Country
on CPCM.CountryId = Country.Id
where CPCM.CompanyProductId = CP.Id) as TargetGeography

from Clist
inner join  [dbo].[CompanyProduct] CP
on Clist.CompanyId = CP.CompanyId
inner join dbo.ProductStatus PS
on cp.StatusId = ps.Id
where CP.IsDeleted =0
and (not exists(select 1 from @SegmentList)
		or exists(select 1 from CompanyProductSegmentMap CPSM
					inner join @SegmentList list
					on list.ID = CPSM.SegmentId
					where cp.Id=cpsm.CompanyProductId 
					));

--Financils
--<TBD>
end