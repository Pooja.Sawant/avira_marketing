﻿CREATE PROCEDURE [dbo].[SFab_GetCompanySWOT]                 
 @CompanyID uniqueidentifier,
 @ProjectID uniqueidentifier, 
 @AviraUserID uniqueidentifier = null                
AS  
/*================================================================================
Procedure Name: SFab_GetCompanySWOT
Author: Praveen
Create date: 07/10/2019
Description: Get list from CompanySWOTMap table by companyid and projectid
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
07/10/2019	Praveen			Initial Version
================================================================================*/          
BEGIN             
 SET NOCOUNT ON;              
 SELECT[CompanySWOTMap].[Id] 
	,[CompanySWOTMap].[CategoryId]   
      ,[CompanySWOTMap].[SWOTDescription]
	  ,[CompanySWOTMap].[SWOTTypeId]
	  ,[SWOTType].SWOTTypeName                
  FROM [dbo].[CompanySWOTMap]        
  LEFT JOIN [dbo].[SWOTType] SWOTType ON [CompanySWOTMap].SWOTTypeId = SWOTType.Id          
  WHERE ([CompanySWOTMap].CompanyId = @CompanyId and [CompanySWOTMap].ProjectId = @ProjectID )                  
 AND [CompanySWOTMap].IsDeleted = 0        
END