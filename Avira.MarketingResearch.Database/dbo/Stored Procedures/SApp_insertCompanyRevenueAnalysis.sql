﻿CREATE procedure [dbo].[SApp_insertCompanyRevenueAnalysis]      
(      
@ID uniqueidentifier,      
@CompanyId uniqueidentifier,       
@CurrencyId uniqueidentifier,      
@UnitId uniqueidentifier,      
@Year int,      
@TotalRevenue decimal,      
@UserCreatedById uniqueidentifier    
--@CreatedOn Datetime,    
--@out_ClientId uniqueidentifier OUTPUT        
)      
as      
/*================================================================================      
Procedure Name: SApp_insertCompanyRevenueAnalysis      
Author: Gopi      
Create date: 05/10/2019      
Description: Insert a record into CompanyRevenue table      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
05/10/2019 Gopi   Initial Version      
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048);      
      
Begin try      
--DELETE FROM [DBO].[CompanyRevenue]         
--WHERE [COMPANYID] = @COMPANYID      
--AND [YEAR] = @Year     
  
update [DBO].[CompanyRevenue]     
set IsDeleted=1      
WHERE [COMPANYID] = @COMPANYID  
AND [YEAR] = @Year      
   
 --if(@ID is null or @ID = '00000000-0000-0000-0000-000000000000')     
set @ID = NEWID();  
--SET @out_ClientId = NEWID();    
      
    
INSERT INTO [dbo].[CompanyRevenue]([Id],        
        [CompanyId],        
  [CurrencyId],         
  [UnitId],                
  [TotalRevenue],        
  [Year],     
        [IsDeleted],                
        [UserCreatedById],                
        [CreatedOn])                
 Values (@ID,         
 @CompanyId,          
 @CurrencyId,          
    @UnitId,        
  dbo.fun_UnitConversion(@UnitId, @TotalRevenue),      
   @Year,       
   0,                
   @UserCreatedById,                 
   GETDATE());      
         
  DECLARE @uuid VARCHAR(50)    
SET @uuid = @ID    
--SELECT StoredProcedureName ='SApp_insertCompanyRevenueAnalysis',Message =@ErrorMsg,Success = 1,@out_ClientId;             
      SELECT @ID, StoredProcedureName ='SApp_insertCompanyRevenueAnalysis',Message =@uuid, Success = 1;             
   --SELECT @ID, Success = 1;             
    
END TRY            
BEGIN CATCH            
    -- Execute the error retrieval routine.            
 DECLARE @ErrorNumber int;            
 DECLARE @ErrorSeverity int;            
 DECLARE @ErrorProcedure varchar(100);            
 DECLARE @ErrorLine int;            
 DECLARE @ErrorMessage varchar(500);            
            
  SELECT @ErrorNumber = ERROR_NUMBER(),            
        @ErrorSeverity = ERROR_SEVERITY(),            
        @ErrorProcedure = ERROR_PROCEDURE(),            
        @ErrorLine = ERROR_LINE(),            
        @ErrorMessage = ERROR_MESSAGE()            
            
 insert into dbo.Errorlog(ErrorNumber,            
       ErrorSeverity,            
       ErrorProcedure,            
       ErrorLine,            
       ErrorMessage,            
       ErrorDate)            
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());            
          
 set @ErrorMsg = 'Error while insert a new Company Revenue, please contact Admin for more details.';          
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine          
    ,Message =@ErrorMsg,Success = 0;             
             
END CATCH            
            
            
end