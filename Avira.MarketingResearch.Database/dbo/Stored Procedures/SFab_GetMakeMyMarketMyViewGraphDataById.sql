﻿      
      
      
Create   procedure [dbo].[SFab_GetMakeMyMarketMyViewGraphDataById]      
(
	@ProjectId uniqueidentifier,
	@MarketId uniqueidentifier    
)      
as      
/*================================================================================      
Procedure Name: [SFab_GetMakeMyMarketMyViewGraphDataById]      
Author: Nitin      
Create date: 13-Sep-2019      
Description: Get My View Data From MakeMyMarketMyView Table      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
13-Sep-2019 Nitin   Initial Version      
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
    
	SELECT MMV.Year,
		   MMV.MyViewValue As TrendValue
	FROM MakeMyMarketMyView MMV
	WHERE MMV.ProjectId = @ProjectId   
		AND MMV.MarketId = @MarketId		
		AND MMV.IsDeleted = 0
	ORDER BY MMV.Year 
    
END