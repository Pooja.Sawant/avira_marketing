﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.QualitativeInfo;
using Avira.MarketingResearch.Model.ViewModels.QualitativeInfo;
using Avira.MarketingResearch.Repository.CoreInterfaces.QualitativeInfo;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QualitativeInfoTrackerController : AviraBaseController
    {
        private ILoggerManager _logger;
        public readonly IQualitativeInfoTrackerRepository _qualitativeInfoTrackerRepository;
        public readonly IQualitativeInfoTrackerService _qualitativeInfoTrackerService;

        public QualitativeInfoTrackerController(
            IQualitativeInfoTrackerService qualitativeInfoTrackerService,
            IQualitativeInfoTrackerRepository qualitativeInfoTrackerRepository,
            ILoggerManager logger)
        {
            _qualitativeInfoTrackerRepository = qualitativeInfoTrackerRepository;
            _qualitativeInfoTrackerService = qualitativeInfoTrackerService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<QualitativeInfoTrackerViewModel>> Get(Guid? Id, int PageStart = 0, int pageSize = 0, int SortDirection = 1, string OrdbyByColumnName = "CreatedDate", string Search = "", bool isDeletedInclude = false)
        {
            List<QualitativeInfoTrackerViewModel> res = null;
            try
            {
                res = _qualitativeInfoTrackerService.GetAll(Id, PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }
    }
}