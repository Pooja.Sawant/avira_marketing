﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public static class ComponentQueries
    {
        public static readonly string StoreProcInsert = "SApp_insertComponent";

        public static readonly string StoreProcUpdate = "SApp_UpdateComponent";

        public static readonly string StoreProcSelectById = "[SApp_GetComponentbyID]";

        public static string StoreProcGetAll = "SApp_ComponentGridWithFilters";
    }
}
