﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.Trend
{
    public class TrendNoteViewModel
    {
        public Guid Id { get; set; }
        public Guid TrendId { get; set; }    
        public string TrendType { get; set; }
        public string TrendStatusName { get; set; }
        public string AuthorRemark { get; set; }
        public string ApproverRemark { get; set; }       
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "TrendNote";
            }
        }
    }

    public class TrendNoteInsertUpdateDbViewModel
    {
        public Guid Id { get; set; }
        public Guid TrendId { get; set; }
        public string TrendType { get; set; }
        public string TrendStatusName { get; set; }
        public string Description { get; set; }
        public string ApproverRemark { get; set; }
        public string AuthorRemark { get; set; }        
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
    }
    
}
