﻿  
  
CREATE PROCEDURE [dbo].[SFab_GetMarketingSizingKeyCompanyData]
(
 @SegmentID uniqueidentifier=null,
 @ProjectId uniqueidentifier=null,
 @RegionIdList dbo.[udtUniqueIdentifier] readonly
)  
AS  
/*================================================================================  
Procedure Name: [SFab_GetMarketingSizingRegionData]  
Author: Swami Aluri  
Create date: 06/07/2019  
Description: Get list of Marketing sizing from table   
Change History  
Date        Developer     Reason  
__________  ____________  ______________________________________________________  
06/07/2019   Swami Aluri   Initial Version  
================================================================================*/  

BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	Declare @SegmentType NVARCHAR(100);
	SELECT @SegmentType = SegmentName FROM Segment
	WHERE Segment.Id = @SegmentID

	IF (@SegmentType is null or @SegmentType = '')
	SET @SegmentType = 'Raw materials'

	declare @RegionLevel tinyint;
	set @RegionLevel = 1;
	DECLARE @RegionList dbo.[udtUniqueIdentifier];

	if(@ProjectId = '00000000-0000-0000-0000-000000000000')
begin
set @ProjectId=null;
end
	IF not exists (select 1 from @RegionIdList)
	begin
	insert into @RegionList
	SELECT id FROM Region WHERE RegionName = 'Global'
	end
	else
	begin
	insert into @RegionList 
	select id from @RegionIdList;
	end

	while (@RegionLevel < = 3)

	begin
	set @RegionLevel = @RegionLevel +1;

	insert into @RegionList 
	select ID
	from Region
	where RegionLevel = @RegionLevel
	and exists (select 1 from @RegionList ParentRegion 
				where Region.ParentRegionId = ParentRegion.ID)
	end

	
	declare @CountryIdFilterList dbo.[udtUniqueIdentifier];
	insert into @CountryIdFilterList
	select Id
	from country
	where exists(select 1 from CountryRegionMap CRM
				inner join @RegionList RList
				on CRM.RegionId = RList.ID
				where country.id = CRM.CountryId
				);



	;with cte as (
	Select Distinct cpmap.CompanyId 
	from CompanyProjectMap cpmap
	where (@ProjectId is null or cpmap.ProjectId = @ProjectId)
	)
	Select TOP 10 comp.Id as CompanyId,
		   comp.CompanyName,
		   comp.RankNumber,
		   RANK () OVER( ORDER BY comp.RankNumber desc) AS ChartRank,
		   ROW_NUMBER() OVER(ORDER BY comp.Id desc) AS ChartColorRank,
		   cr.TotalRevenue,
		   cr.Year,
		   cr.CurrencyId,
		   cr.UnitId
	from Company comp
	inner Join cte On comp.Id = cte.CompanyId
	LEFT Join CompanyRevenue cr On comp.Id = cr.CompanyId and cr.IsDeleted = 0
	Where comp.IsDeleted = 0 and 
	exists (select 1 from MarketValue MV
			inner join MarketValueSegmentMapping MVSM 
			on MVSM.MarketValueId = MV.Id
			inner join CompanyProduct cp
			on MVSM.subsegmentid = cp.Id 
			--inner join  @CountryIdFilterList cfl 
			--on MV.CountryId = cfl.ID  
			inner join @RegionList RList
			on MV.RegionId = RList.ID
			where cp.CompanyId = comp.Id 
			and mv.ProjectId = @ProjectId) 
										 
										 
	Order By cr.TotalRevenue Desc
End