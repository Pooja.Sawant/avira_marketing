﻿using Avira.MarketingResearch.Common.IService.Masters;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces.Masters;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MarketController : AviraBaseController
    {
        public readonly IMarketRepository _marketRepository;
        public readonly IMarketService _marketService;
        private ILoggerManager _logger;
        public MarketController(
            IMarketService marketService,
            IMarketRepository marketRepository,
             ILoggerManager logger)
        {
            _marketRepository = marketRepository;
            _marketService = marketService;
            _logger = logger;
            
        }

        [HttpGet]
        public ActionResult<IEnumerable<MarketViewModel>> Get(int PageStart = 0, int pageSize = 0, int SortDirection = 1, string OrdbyByColumnName = "CreatedDate", string Search = "", bool isDeletedInclude = false)
        {
            List<MarketViewModel> res = null;
            try
            {
                res = _marketService.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet]
        [Route("GetAllParent")]
        public ActionResult<IEnumerable<MarketViewModel>> GetAllParent()
        {
            List<MarketViewModel> res = null;
            try
            {
                res = _marketService.GetAllParentMarket().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet("{id}")]
        public ActionResult<MarketViewModel> Get(Guid id)
        {
            MarketViewModel res = null;
            try
            {
                res = _marketService.GetById(id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpPost]
        public ActionResult Post([FromBody] MarketInsertDbViewModel viewModel)
        {
            viewModel.CreatedOn = System.DateTime.UtcNow;
            viewModel.UserCreatedById = CurrentUser.Id;
            viewModel.Id = Guid.NewGuid();

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new Market, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                var result = _marketService.Create(viewModel, CurrentUser);
                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Market name " + viewModel.MarketName + " is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }

        [HttpPut]
        public ActionResult Put([FromBody] MarketUpdateDbViewModel viewModel)
        {
            viewModel.ModifiedOn = System.DateTime.UtcNow;
            viewModel.UserModifiedById = CurrentUser.Id;

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while updating a new Market, please contact Admin for more details."
            };
            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                Result<SpTransactionMessage> result = _marketService.Update(viewModel, CurrentUser);
                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Market name " + viewModel.MarketName + " is updated successfully.";
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Ok(viewData);
        }

        [Route("DeleteSegment")]
        [HttpPut]
        public ActionResult Delete([FromBody] MarketUpdateDbViewModel viewModel)
        {
            viewModel.DeletedOn = System.DateTime.UtcNow;
            viewModel.UserDeletedById = CurrentUser.Id;
            viewModel.IsDeleted = true;

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while deleting a new Market, please contact Admin for more details."
            };
            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                Result<SpTransactionMessage> result = _marketService.Update(viewModel, CurrentUser);
                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Market name " + viewModel.MarketName + " is deleted.";
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Ok(viewData);
        }

    }
}