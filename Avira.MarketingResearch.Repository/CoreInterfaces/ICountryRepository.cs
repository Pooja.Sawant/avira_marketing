﻿using Avira.MarketingResearch.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces
{
    public interface ICountryRepository
    {
        CountryViewModel GetById(Guid guid);
        IEnumerable<CountryViewModel> GetAll(bool includeDeleted);
    }
}
