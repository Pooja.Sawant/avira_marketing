﻿-- =============================================    
-- Author:  <Praveen>    
-- Create date: <05-07-2019 ,>    
-- Description: <function for currency conversion>    
-- =============================================    
CREATE FUNCTION [dbo].[fun_UnitConversion]    
(    
 -- Add the parameters for the function here    
 @UnitId UNIQUEIDENTIFIER, @Value DECIMAL(18,4)    
)    
RETURNS DECIMAL(18,4)    
AS    
BEGIN    
DECLARE @ConvertedValue DECIMAL(18,4)    
    
 SET @ConvertedValue = CASE WHEN @UnitId is null then @Value
							ELSE @Value * (SELECT TOP 1 CASE WHEN (ValueName = 'Percentage' OR ValueName = 'Fraction' OR ValueName='Kilo Tons') THEN 1  
												 ELSE Conversion END  
												 FROM ValueConversion 
												 WHERE Id = @UnitId)
					END
    
 -- Return the result of the function    
 RETURN @ConvertedValue    
    
END