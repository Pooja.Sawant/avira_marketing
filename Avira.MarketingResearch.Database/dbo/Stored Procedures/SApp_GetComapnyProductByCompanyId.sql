﻿
  
CREATE PROCEDURE [dbo].[SApp_GetComapnyProductByCompanyId]      
 (
 @LaunchDate datetime=null,  
 @CompanyID UniqueIdentifier =null,  
 @includeDeleted bit = 0      
 )      
 AS      
/*================================================================================      
Procedure Name: SApp_GetComapnyProductByCompanyID      
Author: Harshal      
Create date: 05/02/2019      
Description: Get Details from ComapnyProduct table by ID      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
05/02/2019 Harshal   Initial Version  
================================================================================*/      
      
BEGIN      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED      
if @includeDeleted is null set @includeDeleted = 0;  
       
if @LaunchDate is null set @LaunchDate = '12/31/2999'  
     
     
 SELECT [CompanyProduct].[Id],       
  [CompanyProduct].[ProductName],      
  [CompanyProduct].[LaunchDate] ,  
  [CompanyProduct].[Description], 
  [CompanyProduct].[Patents],  
  [CompanyProduct].[StatusId],  
  [ProductStatus].[ProductStatusName],
  [CompanyProduct].[ImageURL],
  [CompanyProduct].[ImageDisplayName], 
  [Currency].[CurrencyName],
  [ValueConversion].[ValueName],
  [CompanyProductValue].[Amount],
  [CompanyProduct].[IsDeleted]
FROM [dbo].[CompanyProduct]  

INNER JOIN [dbo].[ProductStatus] ProductStatus      
ON [CompanyProduct].StatusId = [ProductStatus].Id
LEFT JOIN [dbo].CompanyProductValue CompanyProductValue
ON [CompanyProduct].Id=[CompanyProductValue].CompanyProductId
INNER JOIN [dbo].Currency Currency
ON [CompanyProductValue].CurrencyId= [Currency].Id
INNER JOIN [dbo].[ValueConversion] ValueConversion
ON [CompanyProductValue].ValueConversionId= [ValueConversion].Id
WHERE ([CompanyProduct].CompanyId =@CompanyID)      
 AND (@includeDeleted = 0 or [CompanyProduct].IsDeleted = 1)      
     
 END