﻿

CREATE PROCEDURE [dbo].[SApp_GetCurrencybyID]
	(@CurrencyIDList dbo.udtUniqueIdentifier READONLY,
	@CurrencyName nvarchar(256)='',
	@includeDeleted bit = 0,
	@OrdbyByColumnName NVARCHAR(50) ='CreatedDate',
	@SortDirection INT = -1,
	@PageStart INT = 0,
	@PageSize INT = null)
	AS
/*================================================================================
Procedure Name: [SApp_GetCurrencybyID]
Author: Adarsh
Create date: 04/16/2019
Description: Get list from Currency table by Name or ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
04/16/2019	Adarsh			Initial Version
__________	____________	______________________________________________________

================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
if @includeDeleted is null set @includeDeleted = 0;
if len(@CurrencyName)>2 set @CurrencyName = '%'+ @CurrencyName + '%'
else set @CurrencyName = ''

	
	if @PageSize is null or @PageSize =0
		set @PageSize = 10
begin
;WITH CTE_TBL AS (

	SELECT [Currency].[Id]
      ,[Currency].[CurrencyCode]
      ,[Currency].[CurrencyName]
      ,[Currency].[CurrencySymbol]
      ,[Currency].[Fractionalunit]
      ,[Currency].[IsDeleted],
  
		CASE
		WHEN @OrdbyByColumnName = 'CurrencyName' THEN [Currency].CurrencyName
		
		ELSE [Currency].CurrencyName
	END AS SortColumn

  FROM [dbo].[Currency]
where (NOT EXISTS (SELECT * FROM @CurrencyIDList) or  [Currency].Id  in (select * from @CurrencyIDList))
	AND (@CurrencyName = '' or [Currency].CurrencyName like @CurrencyName)
	AND (@includeDeleted = 1 or [Currency].IsDeleted = 0)
	)

	SELECT 
		  CTE_TBL.*,
		  tCountResult.TotalItems
		FROM CTE_TBL
		CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult
		ORDER BY 
		  CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,
		  CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC 
		OFFSET @PageStart ROWS
		--FETCH NEXT @PageSize ROWS ONLY;
end
	END