﻿
CREATE PROCEDURE [dbo].[SApp_GetImportRequestByUserId]
(
	@UserId uniqueidentifier,
	@ImportStatusName nvarchar(100), 
	@TemplateName	nvarchar(200)
)
	AS
/*================================================================================
Procedure Name: [dbo].[SApp_GetImportRequestByUserId]
Author: Harshal
Create date: 10/16/2019
Description: Get list Import Data table by UserId
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
10/16/2019	Harshal			Initial Version
================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

declare  @StatusId uniqueidentifier;
       
SELECT @StatusId=Id FROM LookupStatus where StatusName=@ImportStatusName;


	SELECT 
	   [ImportData].[Id]
	  ,[TemplateName]
      ,[ImportFileName]
	  ,[FileName]
      ,[ImportFilePath]
      ,[ImportStatusId]
	  ,[StatusName]
      ,[ImportException]
      ,[ImportExceptionFilePath]
      ,[ImportedOn]
      ,[UserImportedById]
      ,[ModifiedOn]
      ,[UserModifiedById]
FROM [dbo].[ImportData]
INNER JOIN LookupStatus ON LookupStatus.Id=ImportData.ImportStatusId
where  ([UserImportedById] = @UserId and TemplateName=@TemplateName and ImportData.ImportStatusId=@StatusId )
Order By Case When ModifiedOn is null then ImportedOn  else ModifiedOn  end desc;
END