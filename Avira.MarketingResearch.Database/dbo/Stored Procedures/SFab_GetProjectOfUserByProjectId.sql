﻿CREATE procedure [dbo].[SFab_GetProjectOfUserByProjectId]
(@UserId uniqueidentifier=null,
@ProjectId uniqueidentifier=null,
@includeDeleted bit = 0)
as
/*================================================================================
Procedure Name: SFab_GetProjectOfUserByProjectId 
Author: Pooja
Create date: 12/10/2019
Description: Get list from project table of user by project id
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
12/10/2019	Pooja			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ProjectApprovedStatusId uniqueidentifier
select @ProjectApprovedStatusId = ID from ProjectStatus
where ProjectStatusName = 'Completed';

if @includeDeleted is null set @includeDeleted = 0

declare @CustomerId    uniqueidentifier ;
declare @usertype    int ;
	SELECT @CustomerId = CustomerId FROM AviraUser
	WHERE Id = @UserId 
SELECT @usertype = UserTypeId FROM AviraUser
	WHERE Id = @UserId 
 if(@usertype = 2)
 begin
SELECT         Project.ID, Project.ProjectName
FROM            SubscriptionSegmentMap AS SSM 
INNER JOIN Project ON SSM.SegmentId = Project.ID and ssm.IsDeleted=0
WHERE 
project.ProjectStatusID = @ProjectApprovedStatusId
and Project.ID=@ProjectId
AND 
(SSM.SegmentType = 'Project' and Project.IsDeleted=0) 
AND EXISTS (SELECT        1 AS Expr1
            FROM            CustomerSubscriptionMap AS CSP
            WHERE        (CustomerId = @CustomerId) 
			AND (SSM.SubscriptionId = SubscriptionId))

ORDER BY SSM.CreatedOn
end
else
begin
	select * from Project 
	where project.ProjectStatusID = @ProjectApprovedStatusId
	 and Project.ID=@ProjectId
	AND IsDeleted=0
end
end