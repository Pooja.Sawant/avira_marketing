﻿CREATE TABLE [dbo].[Component](
    [Id] [uniqueidentifier] NOT NULL,
    [ComponentName] [nvarchar](256) NOT NULL,
    [Description] [nvarchar](256) NULL,
    [CreatedOn] [datetime2](7) NULL,
    [UserCreatedById] [uniqueidentifier] NULL,
    [ModifiedOn] [datetime2](7) NULL,
    [UserModifiedById] [uniqueidentifier] NULL,
    [IsDeleted] [bit] NOT NULL,
    [DeletedOn] [datetime2](7) NULL,
    [UserDeletedById] [uniqueidentifier] NULL,
 CONSTRAINT [PK_Component] PRIMARY KEY CLUSTERED 
(
    [Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Component] ADD  CONSTRAINT [DF_Component_ComponentName]  DEFAULT ('') FOR [ComponentName]
GO
ALTER TABLE [dbo].[Component] ADD  CONSTRAINT [DF_Component_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Component]  WITH CHECK ADD  CONSTRAINT [FK_Component_AviraUser] FOREIGN KEY([UserCreatedById])
REFERENCES [dbo].[AviraUser] ([Id])
GO
ALTER TABLE [dbo].[Component] CHECK CONSTRAINT [FK_Component_AviraUser]
GO
ALTER TABLE [dbo].[Component]  WITH CHECK ADD  CONSTRAINT [FK_Component_AviraUser1] FOREIGN KEY([UserModifiedById])
REFERENCES [dbo].[AviraUser] ([Id])
GO
ALTER TABLE [dbo].[Component] CHECK CONSTRAINT [FK_Component_AviraUser1]
GO