﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.Utils;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Avira.MarketingResearch.Web.Controllers
{
    [Authorize]
    [NoCache]
    public class NewsController : BaseController
    {
        public readonly IServiceRepository _serviceRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;
        string contentRootPath = string.Empty;
        private ILoggerManager _logger;

        public NewsController(IServiceRepository serviceRepository, ILoggerManager logger, IHostingEnvironment hostingEnvironment, IConfiguration configuration)
        {
            _hostingEnvironment = hostingEnvironment;
            _serviceRepository = serviceRepository;
            _logger = logger;
            _configuration = configuration;
            contentRootPath = _hostingEnvironment.ContentRootPath;
        }

        public IActionResult Index(string companyId,string projectId)
        {
      
            SetRoleList();
            BindDropdowns();
            ViewBag.Message = string.Empty;
            MainNewsInsertUpdateDbViewModel mainNewsInsertUpdateDbViewModel = new MainNewsInsertUpdateDbViewModel();
            mainNewsInsertUpdateDbViewModel.CompanyId = new Guid(companyId);
            mainNewsInsertUpdateDbViewModel.ProjectId = new Guid(projectId);
            try
            {
                var ContollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage NewsResponse = _serviceRepository.GetCompanyDetailsByGuIdsResponse(ContollerName, companyId, projectId);
                List<NewsViewModel> NewsViewModel = NewsResponse.Content.ReadAsAsync<List<NewsViewModel>>().Result;
                if (NewsViewModel.Count == 0)
                {
                    NewsViewModel objempty = new NewsViewModel();
                    objempty.Date = DateTime.Now;
                    NewsViewModel.Add(objempty);
                }
                mainNewsInsertUpdateDbViewModel.NewsInsertUpdate = NewsViewModel;
                HttpResponseMessage CompanyNoteResponse = _serviceRepository.GetCompanyNoteByIdResponse("CompanyNote", companyId, "CompanyNews", projectId);
                CompanyNoteViewModel companyNoteViewModel = CompanyNoteResponse.Content.ReadAsAsync<CompanyNoteViewModel>().Result;
                mainNewsInsertUpdateDbViewModel.CompanyNoteModel = companyNoteViewModel ?? new CompanyNoteViewModel();

                HttpResponseMessage CompanyResponse = _serviceRepository.GetByIdResponse("Company", companyId);
                CompanyViewModel companyViewModel = CompanyResponse.Content.ReadAsAsync<CompanyViewModel>().Result;

                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, mainNewsInsertUpdateDbViewModel.ProjectId);
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                    ViewBag.StatusName = mainNewsInsertUpdateDbViewModel.CompanyNoteModel.StatusName;
                }

                ViewBag.CompanyId = mainNewsInsertUpdateDbViewModel.CompanyId;
                ViewBag.CompanyName = companyViewModel.CompanyName;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(mainNewsInsertUpdateDbViewModel);
        }

        private void BindDropdowns()
        {
            try
            {
                HttpResponseMessage responseImpactType = _serviceRepository.GetResponse("ImpactDirection");
                responseImpactType.EnsureSuccessStatusCode();
                ViewBag.ImpactList = responseImpactType.Content.ReadAsAsync<List<ImpactDirectionViewModel>>().Result;

                HttpResponseMessage responseImportance = _serviceRepository.GetResponse("Importance");
                responseImportance.EnsureSuccessStatusCode();
                ViewBag.ImportanceList = responseImportance.Content.ReadAsAsync<List<ImportanceViewModel>>().Result;

                HttpResponseMessage responseNewsCat = _serviceRepository.GetAllParentResponse("News");
                responseNewsCat.EnsureSuccessStatusCode();
                ViewBag.NewsCategoryList = responseNewsCat.Content.ReadAsAsync<List<NewsCategoryViewModel>>().Result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }


        private void SetRoleList()
        {
            var roles = HttpContext.Session.GetString("userRoles");
            var userRoles = JsonConvert.DeserializeObject<List<RoleViewModel>>(roles);
            List<string> RoleListItems = new List<string>();
            foreach (var item in userRoles)
                RoleListItems.Add(item.UserRoleName);
            ViewBag.UserRoleList = RoleListItems;
        }

        [HttpPost]
        public IActionResult Index(MainNewsInsertUpdateDbViewModel model, string actionName)
        {
            SetRoleList();
            ViewBag.Message = string.Empty;
            model.CompanyNoteModel.CompanyType = "CompanyNews";
            bool bResult = false;
            try
            { 
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                if (actionName.Equals("Save"))
                {

                    bResult = validate(model);
                    if (bResult)
                    {
                        model.CompanyNoteModel.CompanyStatusName = "Draft";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            return RedirectToAction("Index", new { companyId = model.CompanyId.ToString(), projectId = model.ProjectId.ToString() });
                        }
                    }
                }
                else if (actionName.Equals("Submitted"))
                {

                    bResult = validate(model);
                    if (bResult)
                    {
                        model.CompanyNoteModel.CompanyStatusName = "Submitted";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            return RedirectToAction("Index", new { companyId = model.CompanyId.ToString(), projectId = model.ProjectId.ToString() });
                        }
                    }

                }
                else if (actionName.Equals("Reject"))
                {
                    bResult = validate(model);
                    if (bResult)
                    {
                        model.CompanyNoteModel.CompanyStatusName = "Rejected";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            return RedirectToAction("Index", new { companyId = model.CompanyId.ToString(), projectId = model.ProjectId.ToString() });
                        }
                    }
                }
                else if (actionName.Equals("Approve"))
                {
                    bResult = validate(model);
                    if (bResult)
                    {
                        model.CompanyNoteModel.CompanyStatusName = "Approved";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            return RedirectToAction("Index", new { companyId = model.CompanyId.ToString(), projectId = model.ProjectId.ToString() });
                        }
                    }
                }

                else if (actionName.Equals("Next"))
                    return RedirectToAction("Index", "EcoSystemPresence", new { companyId = model.CompanyId.ToString(), projectId = model.ProjectId.ToString() });
                else if (actionName.Equals("Previous"))
                    return RedirectToAction("Index", "CompanyClientAndStrategy", new { companyId = model.CompanyId.ToString(), projectId = model.ProjectId.ToString() });
                else if (actionName.Equals("Cancel"))
                    return RedirectToAction("Index", "#", new { companyId = model.CompanyId.ToString(), projectId = model.ProjectId.ToString() });
                else if (actionName.Equals("AddRow"))
                {
                    NewsViewModel objempty = new NewsViewModel();
                    objempty.Date = DateTime.Now;
                    model.NewsInsertUpdate.Add(objempty);
                    model.CompanyNoteModel = model.CompanyNoteModel ?? new CompanyNoteViewModel();
                }

                BindDropdowns();
                HttpResponseMessage CompanyResponse = _serviceRepository.GetByIdResponse("Company", model.CompanyId.ToString());
                CompanyViewModel companyViewModel = CompanyResponse.Content.ReadAsAsync<CompanyViewModel>().Result;
                HttpResponseMessage CompanyNoteResponse = _serviceRepository.GetCompanyNoteByIdResponse("CompanyNote", model.CompanyId.ToString(), "CompanyNews", model.ProjectId.ToString());
                CompanyNoteViewModel companyNoteViewModel = CompanyNoteResponse.Content.ReadAsAsync<CompanyNoteViewModel>().Result;
                companyNoteViewModel = companyNoteViewModel ?? new CompanyNoteViewModel();
                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, model.ProjectId);
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                    ViewBag.StatusName = companyNoteViewModel.StatusName??string.Empty;
                }

                ViewBag.CompanyName = companyViewModel.CompanyName;
                ViewBag.CompanyId = model.CompanyId;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(model);
        }


        /// <summary>
        /// Method to validate Form
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private bool validate(MainNewsInsertUpdateDbViewModel model)
        {
            if (model.NewsInsertUpdate.Count < 5)
            {
                ViewBag.Message = "Please fill atleast five set of Company news mapping";
                return false;
            } 
            if (model.CompanyNoteModel.AuthorRemark == null)
            {
                ViewBag.Message = "Please fill Author Notes.";
                return false;
            }
            else
            {
                ViewBag.Message = string.Empty;
                return true;
            }
        }

    }
}