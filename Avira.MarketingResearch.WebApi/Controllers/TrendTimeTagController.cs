﻿using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Utility;
using Avira.MarketingResearch.WebApi;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.Repository.CoreInterfaces.Masters;
using Avira.MarketingResearch.Common.IService.Masters;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels.Trend;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrendTimeTagController : AviraBaseController
    {
        private ILoggerManager _logger;
        public readonly ITrendTimeTagService _trendTimeTagService;
        public readonly ITrendTimeTagRepository _trendTimeTagRepository;
        
        public TrendTimeTagController(
            ITrendTimeTagService trendTimeTagService,
            ITrendTimeTagRepository trendTimeTagRepository,
            ILoggerManager logger)
        {
            _trendTimeTagRepository = trendTimeTagRepository;
            _trendTimeTagService = trendTimeTagService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TrendTimeTagViewModel>> Get(int PageStart = 0, int pageSize = 0, int SortDirection = 1, string OrdbyByColumnName = "Seq", string Search = "", bool isDeletedInclude = false, string id = "")
        {
            List<TrendTimeTagViewModel> res = null;
            try
            {
                res = _trendTimeTagService.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude, id).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        

        [HttpPost]
        public ActionResult Post([FromBody] MainTrendTimeTagInsertUpdateDbViewModel viewModel)
        {
            viewModel.UserCreatedById = CurrentUser.Id;
            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new Trend Tag, please contact Admin for more details."
            };
            try
            {

                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                var result = _trendTimeTagService.InsertUpdate(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Time Tag name " + viewModel.TimeTagName + " is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }

            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);

        }
    }
}