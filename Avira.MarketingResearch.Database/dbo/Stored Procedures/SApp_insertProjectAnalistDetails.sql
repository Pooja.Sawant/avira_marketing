﻿

CREATE procedure [dbo].[SApp_insertProjectAnalistDetails]
(--@Id uniqueidentifier,
@ProjectID uniqueidentifier, 
@AnalysisType nvarchar(100),
@AnalystType nvarchar(100),
@AnalystUserIDList [dbo].[udtUniqueIdentifier] readonly, 
@UserCreatedById uniqueidentifier
)
as
/*================================================================================
Procedure Name: SApp_insertProjectAnalistDetails
Author: Swami
Create date: 10/19/2019
Description: Insert a record into Industry table
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
03/19/2019	Swami			Initial Version
04/16/2019	Pooja			Initial declaration to null
04/17/2-19	Sharath			Changed Table structure to accomodate multiple CoApprovers and CoAuthors
04/17/2019	Pooja			create id in table itself
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ErrorMsg NVARCHAR(2048);

--Validations
IF exists(select 1 from [dbo].ProjectAnalistDetails 
		 where ProjectID = @ProjectID 
		 AND AnalysisType = @AnalysisType 
		 and AnalystType  = @AnalystType
		 and AnalystUserID in(select id from  @AnalystUserIDList)
		 AND IsDeleted = 0)
begin
--declare @ErrorMsg NVARCHAR(2048);
set @ErrorMsg = 'Analyst User already exists.';
--THROW 50000,@ErrorMsg,1;
SELECT StoredProcedureName ='SApp_insertProjectAnalistDetails',Message =@ErrorMsg,Success = 0;  
RETURN;  

--return(1)
end

--End of Validations
BEGIN TRY

insert into [dbo].ProjectAnalistDetails([Id],
								ProjectID,
								AnalysisType,
								AnalystType,
								AnalystUserID,
								[IsDeleted],
								[UserCreatedById],
								[CreatedOn])
select NEWID(), 
		@ProjectID,
		@AnalysisType,
		@AnalystType,
		ID,
		0,
		@UserCreatedById, 
		GETDATE()
from @AnalystUserIDList;

SELECT StoredProcedureName ='SApp_insertProjectAnalistDetails',Message =@ErrorMsg,Success = 1;     

END TRY
BEGIN CATCH
    -- Execute the error retrieval routine.
	DECLARE @ErrorNumber	int;
	DECLARE @ErrorSeverity	int;
	DECLARE @ErrorProcedure	varchar(100);
	DECLARE @ErrorLine	int;
	DECLARE @ErrorMessage	varchar(500);

	 SELECT @ErrorNumber = ERROR_NUMBER(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorProcedure = ERROR_PROCEDURE(),
        @ErrorLine = ERROR_LINE(),
        @ErrorMessage = ERROR_MESSAGE()

	insert into dbo.Errorlog(ErrorNumber,
							ErrorSeverity,
							ErrorProcedure,
							ErrorLine,
							ErrorMessage,
							ErrorDate)
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());

 set @ErrorMsg = 'Error while creating a new ProjectAnalistDetails, please contact Admin for more details.';    
--THROW 50000,@ErrorMsg,1;      
--return(1)  
SELECT Number = @ErrorNumber, 
		Severity =@ErrorSeverity,
		StoredProcedureName =@ErrorProcedure,
		LineNumber= @ErrorLine,
		Message =@ErrorMsg,
		Success = 0;     
END CATCH
end