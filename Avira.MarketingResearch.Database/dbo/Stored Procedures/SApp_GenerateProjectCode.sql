﻿CREATE procedure [dbo].[SApp_GenerateProjectCode]  
(@ProjectID uniqueidentifier)  
as  
/*================================================================================  
Procedure Name: SApp_GenerateProjectCode  
Author: Praveen  
Create date: 04/23/2019  
Description: Generate project code 
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
04/23/2019 Praveen   Initial Version  
================================================================================*/  
BEGIN  
SET NOCOUNT ON;  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
declare @ErrorMsg NVARCHAR(2048);  
DECLARE @MarketName NVARCHAR(512), @SubMarketName NVARCHAR(512), @IndustryName NVARCHAR(512), @SubIndustryName NVARCHAR(512), @ProjectCode NVARCHAR(100),@MaxSerial INT
DECLARE @today DATETIME = CONVERT(VARCHAR, GETDATE(), 101) 
BEGIN TRY  
-------------Markets-----------------------------
SELECT TOP 1 @MarketName = Market.MarketName
FROM ProjectSegmentMapping 
JOIN Market ON ProjectSegmentMapping.SegmentID = Market.Id
WHERE ProjectID = @ProjectID AND ProjectSegmentMapping.SegmentType = 'Markets'
-------------Sub-Markets------------ as discuss with BA hide submarket from Projectcode
--SELECT top 1 @SubMarketName = sub.MarketName
--FROM ProjectSegmentMapping 
--JOIN Market sub ON ProjectSegmentMapping.SegmentID = sub.Id
--WHERE ProjectID = @ProjectID AND ProjectSegmentMapping.SegmentType = 'Sub-Markets'
-------------Industries---------------------
SELECT top 1 @IndustryName =  Industry.SubSegmentName
FROM ProjectSegmentMapping 
JOIN SubSegment Industry 
ON ProjectSegmentMapping.SegmentID = Industry.Id
WHERE ProjectID = @ProjectID AND ProjectSegmentMapping.SegmentType = 'Industries'
------------------Broad Industries------------------
SELECT top 1 @SubIndustryName = broad.SubSegmentName 
FROM ProjectSegmentMapping 
JOIN SubSegment broad ON ProjectSegmentMapping.SegmentID = broad.Id
WHERE ProjectID = @ProjectID AND ProjectSegmentMapping.SegmentType = 'Broad Industries'
-----------------END---------------------
  
SET @ProjectCode = CONVERT(NVARCHAR,LEFT(REPLACE(@MarketName,' ',''), 2)) --+ CONVERT(NVARCHAR,LEFT(REPLACE(@SubMarketName,' ',''), 2))
 + CONVERT(NVARCHAR,LEFT(REPLACE(@IndustryName,' ',''), 2)) + CONVERT(NVARCHAR,LEFT(REPLACE(@SubIndustryName,' ',''), 2))
+ '-' + REPLACE(CONVERT(VARCHAR, GETDATE(),11),'/','') 
--IOCDASRT-190423-2
IF EXISTS(SELECT 0 FROM Project WHERE SUBSTRING(ProjectCode, CHARINDEX('-', ProjectCode) + 1, 6) = REPLACE(CONVERT(VARCHAR, GETDATE(),11),'/',''))
BEGIN



--select @today

;with cte as (
select ROW_NUMBER() over(order by CreatedOn ) as seq,ID from project where ISNULL(ProjectCode,'') != '' AND CreatedOn > @today)
select top 1 @MaxSerial = seq--,ID
from cte order by seq desc

	--SELECT @MaxSerial = MAX(Convert(int,RIGHT(ProjectCode,2))) FROM Project WHERE ISNULL(ProjectCode,'') != '' AND SUBSTRING(ProjectCode, CHARINDEX('-', ProjectCode) + 1, 6) = REPLACE(CONVERT(VARCHAR, GETDATE(),11),'/','')
	SET @ProjectCode = @ProjectCode + '-' + RIGHT('0' + CONVERT(NVARCHAR,(@MaxSerial + 1)),2)
	--select @ProjectCode
END
ELSE
BEGIN
	SELECT @ProjectCode = @ProjectCode + '-01' FROM Project WHERE ID = @ProjectID
END

UPDATE Project SET ProjectCode = UPPER(@ProjectCode) WHERE ID = @ProjectID AND ISNULL(ProjectCode,'') = '' AND CONVERT(DATE, CreatedOn, 101) = CONVERT(DATE, GETDATE(), 101)

SELECT StoredProcedureName ='SApp_GenerateProjectCode',Message =@ErrorMsg,Success = 1; 
END TRY  
BEGIN CATCH  
    -- Execute the error retrieval routine.  
 DECLARE @ErrorNumber int;  
 DECLARE @ErrorSeverity int;  
 DECLARE @ErrorProcedure varchar(100);  
 DECLARE @ErrorLine int;  
 DECLARE @ErrorMessage varchar(500);  
  
  SELECT @ErrorNumber = ERROR_NUMBER(),  
        @ErrorSeverity = ERROR_SEVERITY(),  
        @ErrorProcedure = ERROR_PROCEDURE(),  
        @ErrorLine = ERROR_LINE(),  
        @ErrorMessage = ERROR_MESSAGE()  
  
 insert into dbo.Errorlog(ErrorNumber,  
       ErrorSeverity,  
       ErrorProcedure,  
       ErrorLine,  
       ErrorMessage,  
       ErrorDate)  
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());  
  
 set @ErrorMsg = 'Error while generating project code, please contact Admin for more details.';      
--THROW 50000,@ErrorMsg,1;        
--return(1)    
SELECT Number = @ErrorNumber,   
  Severity =@ErrorSeverity,  
  StoredProcedureName =@ErrorProcedure,  
  LineNumber= @ErrorLine,  
  Message =@ErrorMsg,  
  Success = 0;       
END CATCH  
end