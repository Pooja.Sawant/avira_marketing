﻿using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation
{
    public class ServiceService : IServiceService
    {
        private readonly IServiceRepository _iServiceRepository;

        public ServiceService(IServiceRepository iServiceRepository)
        {
            _iServiceRepository = iServiceRepository;
        }

        public Result<SpTransactionMessage> Create(ServiceInsertDbViewModel tfc, AviraUser currentUser)
        {
            return _iServiceRepository.Create(tfc);
        }


        public IEnumerable<ServiceViewModel> GetAll(Guid Id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ServiceViewModel> GetAllParentService()
        {
            return _iServiceRepository.GetAllParentService();
        }

        public IEnumerable<ServiceViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false)
        {
            return _iServiceRepository.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName,Search, isDeletedInclude);
        }

        public ServiceViewModel GetById(Guid guid)
        {
            return _iServiceRepository.GetById(guid);
        }


        public Result<SpTransactionMessage> Update(ServiceUpdateDbViewModel tfc, AviraUser currenUser)
        {
            return _iServiceRepository.Update(tfc);
        }
    }
}
