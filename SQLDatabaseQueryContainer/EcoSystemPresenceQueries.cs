﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public class EcoSystemPresenceQueries
    {
        public static string StoreProcSelectEcoSystemPresenceSectorByCompanyId = "SApp_GetEcoSystemPresenceSectorByCompanyID";
        public static string StoreProcSelectEcoSystemPresenceByCompanyId = "SApp_GetEcoSystemPresenceByCompanyID";
        public static string StoreProcSelectEcoSystemPresenceCompetitersByCompanyId = "SApp_EcoSystemPresenceCompetitersByCompanyID";
        public static readonly string StoreProcInsertForEcoSystemPresenceSectorMap = "SApp_insertUpdateCompanyEcosystemPresenceSectorMap";
        public static readonly string StoreProcInsertEcoSystemPresenceESMMap = "SApp_insertUpdateCompanyEcosystemPresenceESMMap";
        public static readonly string StoreProcInsertEcoSystemPresenceCompetitersMap = "SApp_insertUpdateCompanyEcosystemPresenceKeyCompetitersMap";
    }
}
