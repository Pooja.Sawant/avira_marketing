﻿
CREATE procedure [dbo].[SFab_GetCIAnalysisPageTableDataById]
(
@Id uniqueidentifier=null,
@ProjectId uniqueidentifier=null,
@includeDeleted bit = 0,
@AviraUserID uniqueidentifier = null)
as
/*================================================================================
Procedure Name: SFab_GetCIAnalysisPageTableDataById
Author: Harshal
Create date: 07/16/2019
Description: Get Record from QualitativeTableTab by Id and ProjectId
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
07/16/2019	Harshal			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ProjectApprovedStatusId uniqueidentifier
select @ProjectApprovedStatusId = ID from ProjectStatus
where ProjectStatusName = 'Completed';

if @includeDeleted is null set @includeDeleted = 0

SELECT        QualitativeTableTab.Id,QualitativeTableTab.ProjectId, QualitativeTableTab.TableName, QualitativeTableTab.TableData, QualitativeTableTab.TableMetaData 
FROM            QualitativeTableTab 
WHERE        (QualitativeTableTab.Id=@Id AND QualitativeTableTab.ProjectId = @ProjectId)
and exists (select 1 from project 
			where project.ID = QualitativeTableTab.ProjectID
			and project.ProjectStatusID = @ProjectApprovedStatusId)
and (QualitativeTableTab.IsDeleted=0 or QualitativeTableTab.IsDeleted is null)
order by QualitativeTableTab.TableName asc
end