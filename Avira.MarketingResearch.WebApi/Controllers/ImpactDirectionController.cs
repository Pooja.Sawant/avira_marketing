﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImpactDirectionController : AviraBaseController
    {
        public readonly IImpactDirectionService _iImpactDirectionService;
        private ILoggerManager _logger;
        public ImpactDirectionController(
            IImpactDirectionService iImpactDirectionService, ILoggerManager logger)
        {
            _iImpactDirectionService = iImpactDirectionService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ImpactDirectionViewModel>> Get()
        {
            List<ImpactDirectionViewModel> res = null;
            try
            {
                res = _iImpactDirectionService.GetAll().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }
    }
}