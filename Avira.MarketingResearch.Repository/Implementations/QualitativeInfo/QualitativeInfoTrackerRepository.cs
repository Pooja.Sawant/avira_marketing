﻿using Avira.MarketingResearch.Model.ViewModels.QualitativeInfo;
using Avira.MarketingResearch.Repository.CoreInterfaces.QualitativeInfo;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.QualitativeInfo
{
    public class QualitativeInfoTrackerRepository : IQualitativeInfoTrackerRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "QualitativeInfo";
        public QualitativeInfoTrackerRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public IEnumerable<QualitativeInfoTrackerViewModel> GetAll(Guid? guid, int PageStart = 0, int pageSize = 0, int SortDirection = -1, string OrdbyByColumnName = "", string Search = "", bool includeDeleted = false)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<QualitativeInfoTrackerViewModel>(QualitativeInfoTrackerQueries.StoreProcSelectAll
                            , new { @ProjectID = guid, @includeDeleted = includeDeleted, @PageStart = PageStart, @pageSize = pageSize, @SortDirection = SortDirection, @OrdbyByColumnName = OrdbyByColumnName, @Search = Search }
                            , null, true, null, CommandType.StoredProcedure);
        }
    }
}
