﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.Trend
{
    public interface ITrendKeywordMapService
    {
        IEnumerable<TrendKeyWordMapViewModel> GetAll(Guid? TrendId, int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false);
        IEnumerable<TrendKeyWordMapViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false);
        Result<SpTransactionMessage> Create(TrendKeyWordMapInsertDbViewModel1 tfc, AviraUser currentUser);
        Result<SpTransactionMessage> Update(TrendKeyWordMapUpdateDbViewModel tfc, AviraUser currenUser);

        IEnumerable<TrendKeywordNames> GetAllTrendNames();
        IEnumerable<TrendKeyWordMapViewModel> GetById(Guid guid);
        TrendKeywordNames GetTrendNameByID(Guid guid);
    }
}
