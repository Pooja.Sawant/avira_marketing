﻿CREATE procedure [dbo].[SApp_GetRegionbyID]
(@RegionID uniqueidentifier,
@includeDeleted bit = 0)
as
/*================================================================================
Procedure Name: SApp_GetCompanybyID
Author: Laxmikant
Create date: 02/11/2019
Description: Get list from Company table by Name or ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
03/04/2019	Laxmikant			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if @includeDeleted is null set @includeDeleted = 0

SELECT [Id], 
		[RegionName],
		[RegionLevel],
		[ParentRegionId]
FROM [dbo].[Region]
where (@RegionID is null or ID = @RegionID)
and (@includeDeleted = 1 or IsDeleted = 0)  
end