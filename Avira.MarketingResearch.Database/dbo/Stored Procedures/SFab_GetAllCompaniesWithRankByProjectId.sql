﻿
Create PROCEDURE [dbo].[SFab_GetAllCompaniesWithRankByProjectId]  
(  
	@ProjectId UniqueIdentifier=null,  
	@Revenue bit = 1,  
	@Rank bit = 0,  
	@SegmentList [dbo].[udtUniqueIdentifier] READONLY,
	@RegionList [dbo].[udtUniqueIdentifier] READONLY,
	@CompanyStageList [dbo].[udtUniqueIdentifier] READONLY,
	@AvirauserId UniqueIdentifier = null  
)  
as  

--DECLARE 
--@ProjectId UniqueIdentifier=null,  
--	@Revenue bit = 1,  
--	@Rank bit = 0,  
--	@SegmentList [dbo].[udtUniqueIdentifier],
--	@AvirauserId UniqueIdentifier = null  


--Select @ProjectId='41EB144B-51EC-4264-8680-05957E5413A4', @Revenue=1,@Rank=0,@AvirauserId='00000000-0000-0000-0000-000000000000'

--insert into @SegmentList values('57D02A9D-1EF9-4A0B-BBC6-040AE0CA3E25')

--Select c.CompanyName, * FROM CompanyEcosystemPresenceESMMap S Inner Join Company C On S.CompanyId = C.Id
--Where s.ESMId = '57D02A9D-1EF9-4A0B-BBC6-040AE0CA3E25'
/*================================================================================  
Procedure Name: [SFab_GetAllCompaniesWithRankByProjectId]  
Author: Harshal  
Create date: 12/09/2019  
Description: Get list from Companies by project Id  
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
15/05/2019 Harshal  Initial Version  
16-Oct-2019 Nitin Changes for CI Rank Segment - Filter by segment
================================================================================*/  
BEGIN  
	SET NOCOUNT ON;  
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  

	DECLARE @ProjectApprovedStatusId uniqueidentifier;
	DECLARE @BaseYear INT;
	DECLARE @FromYear INT;
	DECLARE @USDCurrencyId uniqueidentifier	
	Declare @SegmentFilterApplied bit = 0, @RegionFilterApplied bit = 0, @CompanyStageFilterApplied bit = 0
	DECLARE @tempTable TABLE(    
		CompanyRank smallint,  
		YEAR INT ,
		Revenue decimal(18, 2),
		CompanyName nvarchar(50),
		CompanyId uniqueidentifier)

	SELECT @BaseYear = BaseYear FROM BaseYearMaster
	SET @FromYear = @BaseYear - 5
	SELECT @ProjectApprovedStatusId = ID from ProjectStatus Where ProjectStatusName = 'Completed'	
	Set @USDCurrencyId = dbo.fun_GetCurrencyIDForUSD()	

	If (exists(select 1 from @SegmentList))
	Begin
		
		Set @SegmentFilterApplied = 1
	End

	If (exists(select 1 from @RegionList))
	Begin
		
		Set @RegionFilterApplied = 1
	End

	If (exists(select 1 from @CompanyStageList))
	Begin
		
		Set @CompanyStageFilterApplied = 1
	End

	IF(@Revenue = 1)  
	Begin  

		WHILE(@FromYear <= @BaseYear)
		BEGIN

			INSERT INTO @tempTable(CompanyRank, YEAR, Revenue, CompanyId, CompanyName)
				SELECT TOP 10 RANK() over(order by Revenue desc) CompanyRank, YEAR, Revenue, CompanyProjectMap.CompanyId, Company.CompanyName --,CompanyProjectMap.ProjectId
				FROM CompanyPLStatement 
					Inner JOIN CompanyProjectMap ON CompanyPLStatement.CompanyId = CompanyProjectMap.CompanyId And CompanyProjectMap.IsDeleted = 0
					Inner JOIN Company ON Company.Id=CompanyProjectMap.CompanyId
				WHERE YEAR = @FromYear AND ProjectId = @ProjectId 
					And (@SegmentFilterApplied = 0 or 
							 exists(SELECT 1 
									FROM CompanyEcosystemPresenceESMMap CPSM 
										inner join @SegmentList SegmentList On 
											CPSM.ESMId =SegmentList.ID And 
											CPSM.CompanyId = CompanyPLStatement.CompanyId)
						 )
					And (@RegionFilterApplied = 0 Or
							exists(Select 1 From CompanyRegionMap CRM Inner Join @RegionList R On CRM.RegionId = R.ID And CRM.CompanyId = CompanyPLStatement.CompanyId))
					And (@CompanyStageFilterApplied = 0 Or
							--exists(Select 1 From @CompanyStageList Where Id = Company.CompanyStageId))
							Company.CompanyStageId In (Select Id From @CompanyStageList))
    
				SET @FromYear = @FromYear + 1
		END		
	End  
	Else If(@Rank = 1)  
	Begin 
 
		WHILE(@FromYear <= @BaseYear)
		begin 

			;with cteRank as (  
					Select Distinct CPMAP.CompanyId   
					FROM CompanyProjectMap CPMAP  
						INNER JOIN [dbo].Project ON Project.Id=cpmap.ProjectId
						Inner Join Company On CPMAP.CompanyId = Company.Id
					WHERE cpmap.ProjectId = @ProjectId AND Project.ProjectStatusID = @ProjectApprovedStatusId AND cpmap.IsDeleted = 0
						And (not exists(select 1 from @SegmentList) or 
								 exists(SELECT 1 
										FROM CompanyEcosystemPresenceESMMap CPSM 
											inner join @SegmentList SegmentList On 
												CPSM.ESMId =SegmentList.ID And 
												CPSM.CompanyId = CPMAP.CompanyId)
							)
						And (@RegionFilterApplied = 0 Or
							exists(Select 1 From CompanyRegionMap CRM Inner Join @RegionList R On CRM.RegionId = R.ID And CRM.CompanyId = CPMAP.CompanyId))
						And (@CompanyStageFilterApplied = 0 Or							
							Company.CompanyStageId In (Select Id From @CompanyStageList))
						
				)

			, ctr_Growth as (
			Select comp.Id as CompanyId,  
				comp.CompanyName,  
				--RANK () OVER( ORDER BY comp.RankNumber desc) AS ChartRank,  
				RANK () OVER( ORDER BY cr.GROWTH desc) AS CompanyRank,
				cr.GROWTH As Revenue,
				cr.Year
	 
			from Company comp inner Join cteRank On comp.Id = cteRank.CompanyId  
			Join (
			SELECT t1.CompanyId, t1.YEAR, t1.Revenues,    CASE WHEN t2.YEAR IS NOT NULL THEN
					--FORMAT(
					CONVERT(DECIMAL(19, 4), 
						CONVERT(DECIMAL(19, 4), (t1.Revenues - t2.Revenues)) /
					   nullif( CONVERT(DECIMAL(19, 4), t2.Revenues),0)--, 'p')
					   )
				ELSE 0 END AS GROWTH
			FROM
			(
				SELECT CompanyId, YEAR, SUM(Revenue) AS Revenues
				FROM CompanyPLStatement
				WHERE Year = @FromYear AND 
				IsDeleted = 0 GROUP BY CompanyId, YEAR
			) t1
			LEFT JOIN
			(
				SELECT CompanyId, YEAR AS YEAR, SUM(Revenue) AS Revenues
				FROM CompanyPLStatement
				WHERE Year = @FromYear -1 AND 
				IsDeleted = 0 GROUP BY CompanyId, YEAR
			) t2
			ON t1.CompanyId = t2.CompanyId AND t2.YEAR = t1.YEAR - 1
			)cr  
			On comp.Id = cr.CompanyId
			Where comp.IsDeleted = 0  
			--Order By comp.RankNumber 
			)

			INSERT INTO @tempTable(CompanyRank, YEAR, Revenue, CompanyId,CompanyName)
			SELECT top 10  ctr_Growth.CompanyRank,ctr_Growth.Year,ctr_Growth.Revenue,ctr_Growth.CompanyId,ctr_Growth.CompanyName FROM ctr_Growth --,ChartRank 
 
			SET @FromYear = @FromYear + 1
		End		
	End
	SELECT * 
	FROM @tempTable 
	ORDER BY YEAR DESC  
END  
