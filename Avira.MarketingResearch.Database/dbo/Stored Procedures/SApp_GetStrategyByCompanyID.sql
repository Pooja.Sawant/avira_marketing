﻿CREATE PROCEDURE [dbo].[SApp_GetStrategyByCompanyID]             
 @ComapanyID uniqueidentifier = null,         
 @includeDeleted bit = 0             
AS        
BEGIN         
 SET NOCOUNT ON;          
 SELECT [CompanyStrategyMapping].[Id]        
      ,[CompanyId]        
      ,[Date]        
      ,[Approach] as ApproachId        
   ,Approach.ApproachName as ApproachName        
      ,[Strategy]        
      ,[Category] as CategoryId        
   ,ClientStrategyCategory.CategoryName as CategoryName        
      ,[Importance] as ImportanceId        
   ,Importance.ImportanceName as ImportanceName        
      ,[Impact] as ImpactId        
   ,ImpactType.ImpactTypeName as ImpactName        
      ,[Remark]     
      ,[ClientStrategyCategory].[CreatedOn]        
      ,[ClientStrategyCategory].[UserCreatedById]        
      ,[ClientStrategyCategory].[ModifiedOn]        
      ,[ClientStrategyCategory].[UserModifiedById]        
      ,[ClientStrategyCategory].[IsDeleted]        
      ,[ClientStrategyCategory].[DeletedOn]        
      ,[ClientStrategyCategory].[UserDeletedById]        
  FROM [dbo].[CompanyStrategyMapping]        
  LEFT JOIN [dbo].[ClientStrategyCategory] ClientStrategyCategory  ON [CompanyStrategyMapping].Category = [ClientStrategyCategory].Id        
  LEFT JOIN [dbo].[Importance] Importance ON [CompanyStrategyMapping].Importance = Importance.Id        
  LEFT JOIN [dbo].[ImpactType] ImpactType ON [CompanyStrategyMapping].Impact = ImpactType.Id        
  LEFT JOIN [dbo].[Approach] Approach ON [CompanyStrategyMapping].Approach = Approach.Id   
  WHERE ([CompanyStrategyMapping].CompanyId = @ComapanyID)              
 AND (@includeDeleted = 0 or [CompanyStrategyMapping].IsDeleted = 0)          
END