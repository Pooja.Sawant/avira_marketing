﻿CREATE PROCEDURE [dbo].[SFab_InsertDecisionParameter]
(
	@Id uniqueidentifier = null,
	@BaseDecisionParameterId uniqueidentifier = null,
	@DecisionId	uniqueidentifier,
	@Description nvarchar(1024),
	@CategoryId	uniqueidentifier,
	@RatingId	uniqueidentifier,
	@Weightage	decimal(18, 2),
	@UserId	uniqueidentifier,
	@NewDecisionParameterId uniqueidentifier OutPut
)
As
/*================================================================================    
Procedure Name: SFab_InsertDecisionParameter   
Author: Praveen    
Create date: 07-Jan-2020
Description: Add new decision parameter by user
Change History    
Date  Developer  Reason  
__________ ____________ ______________________________________________________    
07-Jan-2020  Praveen   Initial Version    
15-Jan-2020  Nitin
================================================================================*/    
BEGIN    
------------------
	--Required Tables: Decision, DecisionParameter and LookupCategory
	--Notes: N/A
	--Case 1: N/A
	--Expected Result :SELECT StoredProcedureName ='SFab_InsertDecisionParamete',Message = @ErrorMsg,Success = 1/0;

Declare @ErrorMsg NVARCHAR(2048)  

	BEGIN TRY
		EXEC [SFab_CopyDecisionFromBase]
			@InDecisionId = @DecisionId,
			@UserId = @UserId,
			@OutDecisionId = @DecisionID OUTPUT		
	
		If (@BaseDecisionParameterId Is Null Or @BaseDecisionParameterId = '00000000-0000-0000-0000-000000000000')
		Begin
			
			Set @NewDecisionParameterId = NewID()
			
			Insert Into DecisionParameter (Id, DecisionId, Description, CategoryId, RatingId, Weightage)
			Values (@NewDecisionParameterId, @DecisionId, @Description, @CategoryId, @RatingId, @Weightage)
		End
		Else
		Begin
	
			Update DecisionParameter
			Set CategoryId = @CategoryId,
				RatingId = @RatingId,
				Weightage = @Weightage
			Where BaseDecisionParameterId = @BaseDecisionParameterId And DecisionId = @DecisionId
		End
		SELECT StoredProcedureName ='SFab_InsertDecisionParameter', Message = @ErrorMsg, Success = 1;   
	END TRY
	BEGIN CATCH      
		-- Execute the error retrieval routine.      
			EXEC SApp_LogErrorInfo
    
			set @ErrorMsg = 'Error while inserting Decision parameter, please contact Admin for more details.';    
			
		SELECT StoredProcedureName ='SFab_InsertDecisionParameter', Message = @ErrorMsg, Success = 0;     
       
	END CATCH 
END