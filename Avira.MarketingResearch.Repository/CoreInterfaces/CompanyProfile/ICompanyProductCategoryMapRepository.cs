﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile
{
    public interface ICompanyProductCategoryMapRepository
    {
        IEnumerable<CompanyProductCategoryMapViewModel> GetAll(Guid? Id);
        Result<SpTransactionMessage> InsertUpdate(CompanyProductViewMoel companyProductViewMoel);
    }
}
