﻿

CREATE PROCEDURE [dbo].[SApp_UserGridWithFilters]
	(@UserIDList dbo.udtUniqueIdentifier READONLY,
	@UserName nvarchar(256)='',
	@includeDeleted bit = 0,
	@OrdbyByColumnName NVARCHAR(50) ='CreatedDate',
	@SortDirection INT = -1,
	@PageStart INT = 0,
	@PageSize INT = 25)
	AS
/*================================================================================
Procedure Name: SApp_UserGridWithFilters
Author: Adarsh
Create date: 03/16/2019
Description: Get list from User table by Name or ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
03/16/2019	Adarsh			Initial Version
================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
if @includeDeleted is null set @includeDeleted = 0;
if len(@UserName)>2 set @UserName = '%'+ @UserName + '%'
else set @UserName = ''
	
	;WITH CTE_TBL AS (

	SELECT [AviraUser].[Id],
		[AviraUser].[FirstName],
		[AviraUser].[LastName],
		[AviraUser].[UserName],
		[AviraUser].[DisplayName],
		[AviraUser].[EmailAddress],
		[AviraUser].[PhoneNumber],
		[AviraUser].[IsDeleted],
		(

		SELECT STRING_AGG (UserRole.UserRoleName, ', ')  
		FROM UserRole INNER JOIN AviraUserRoleMap ON AviraUserRoleMap.UserRoleId = UserRole.Id AND AviraUserRoleMap.AviraUserId = [AviraUser].[Id]

) AS UserRoleName,
		CASE
		WHEN @OrdbyByColumnName = 'UserName' THEN [AviraUser].UserName
		WHEN @OrdbyByColumnName = 'FirstName' THEN [AviraUser].[FirstName]
		WHEN @OrdbyByColumnName = 'LastName' THEN [AviraUser].[LastName]
		WHEN @OrdbyByColumnName = 'DisplayName' THEN [AviraUser].[DisplayName]
		WHEN @OrdbyByColumnName = 'EmailAddress' THEN [AviraUser].[EmailAddress]
		WHEN @OrdbyByColumnName = 'PhoneNumber' THEN [AviraUser].[PhoneNumber]
		--WHEN @OrdbyByColumnName = 'CreatedDate' THEN [AviraUser].[CreatedOn]
		ELSE [AviraUser].[UserName]
	END AS SortColumn

FROM [dbo].[AviraUser]
where (NOT EXISTS (SELECT * FROM @UserIDList) or  [AviraUser].Id  in (select * from @UserIDList))
 
	AND (@UserName = '' 
		or [AviraUser].[UserName] like @UserName
		or [AviraUser].[FirstName] like @UserName
		or [AviraUser].[LastName] like @UserName
		or [AviraUser].[UserName] like @UserName
		or [AviraUser].[DisplayName] like @UserName
		or [AviraUser].[EmailAddress] like @UserName)
	AND (@includeDeleted = 1 or [AviraUser].IsDeleted = 0)
	)

	SELECT 
		  CTE_TBL.*,
		  tCountResult.TotalItems
		FROM CTE_TBL
		CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult
		ORDER BY 
		  CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,
		  CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC 
		OFFSET @PageStart ROWS
		FETCH NEXT @PageSize ROWS ONLY;

	END