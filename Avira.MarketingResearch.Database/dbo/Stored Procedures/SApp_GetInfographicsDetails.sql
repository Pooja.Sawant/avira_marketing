﻿CREATE PROCEDURE [dbo].[SApp_GetInfographicsDetails]
(
@Id uniqueidentifier
)
 AS      
/*================================================================================      
Procedure Name: SApp_LookupCategoryGet      
Author: Praveen      
Create date: 10/25/2019      
Description: Get list from infographics BY id
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
10/25/2019 Praveen   Initial Version      
================================================================================*/      
      
BEGIN      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED      

SELECT 	
Id,
ProjectId,
LookupCategoryId,
InfogrTitle,
InfogrDescription,
SequenceNo,
ImageActualName,
ImageName,
ImagePath,
CreatedOn,
UserCreatedById
FROM Infographics
WHERE Id= @Id


      
 END