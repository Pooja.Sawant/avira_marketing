﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public class FundamentalQueries
    {
        public static readonly string StoreProcInsert = "SApp_insertFundamental";

        public static readonly string StoreProcUpdate = "SApp_UpdateFundamental";

        public static string StoreProcSelectAll = "SApp_FundamentalGridWithFilters";
    }
}
