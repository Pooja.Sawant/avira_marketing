﻿using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Avira.MarketingResearch.Web.ImportExportUtility
{
    public class ImportTrend : IExcelImport
    {
        public readonly IServiceRepository _serviceRepository;
        private ILoggerManager _logger;
        public ImportTrend(IServiceRepository serviceRepository, ILoggerManager logger)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
        }
        public ImportResponse Import(IFormFile formFile)
        {
            var res = new ImportResponse();
           
            var inputModel = new TrendImportInsertViewModel();
            using (var stream = new MemoryStream())
            {
                formFile.CopyTo(stream);

                using (var package = new ExcelPackage(stream))
                {
                    if (package.Workbook.Worksheets["Trends"] != null)
                    {
                        inputModel.TrendsImportViewModel = GetTrendImportData(package.Workbook.Worksheets["Trends"]);
                    }
                    else
                    {
                        res.ResponseMessage = "Please select correct template for the file you want to import / Error while reading template.'Trends' worksheet is missing.";
                        res.TotalSuccessRecords = 0;
                        res.TotalFailedRecords = 0;
                        res.ResponseStatus = "Error";
                        return res;
                    }
                    if (package.Workbook.Worksheets["Keywords"] != null)
                    {
                        inputModel.ProjectKeyWordImportViewModel = GetKeyWordsImportData(package.Workbook.Worksheets["Keywords"]);
                    }
                    else
                    {
                        res.ResponseMessage = "Please select correct template for the file you want to import / Error while reading template.'Keywords' worksheet is missing.";
                        res.TotalSuccessRecords = 0;
                        res.TotalFailedRecords = 0;
                        res.ResponseStatus = "Error";
                        return res;
                    }
                }
            }

            //--------Call api to post data
           
            inputModel.TemplateName = "Trend Import Template";
            inputModel.ImportFileName = formFile.FileName;
            try
            {

                HttpResponseMessage response = _serviceRepository.PostResponse("TrendImportExport", inputModel);
                response.EnsureSuccessStatusCode();
                ImportCommonResponseMainModel viewModel = response.Content.ReadAsAsync<ImportCommonResponseMainModel>().Result;

                res.ResponseStatus = viewModel.IsSuccess ? "Success" : "Failed";
                res.ResponseMessage = viewModel.Message;
                res.TotalRecords = inputModel.TrendsImportViewModel.Count;
                if (viewModel.ResponseModel == null)
                {
                    res.TotalSuccessRecords = (inputModel.TrendsImportViewModel == null ? 0: inputModel.TrendsImportViewModel.Count);
                    res.TotalFailedRecords = 0;
                }
                else
                {
                    res.ResponseMessage = "Please check downloaded file for trend error List";
                    res.TotalSuccessRecords = ((inputModel.TrendsImportViewModel == null ? 0 :inputModel.TrendsImportViewModel.Count)) - (viewModel.ResponseModel.Count());
                    res.TotalFailedRecords = viewModel.ResponseModel.Count();
                }
                res.ResponseModel = viewModel.ResponseModel;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            //res.ResponseStatus = "Success";
            //res.ResponseMessage = "Partialy uploaded, please see the list";
            //res.TotalRecords = 100;
            //res.TotalSuccessRecords = 80;
            //res.TotalFailedRecords = 20;

            return res;
        }

        private List<ProjectKeyWordImportViewModel> GetKeyWordsImportData(ExcelWorksheet worksheet)
        {
            List<ProjectKeyWordImportViewModel> projectKeywordList = new List<ProjectKeyWordImportViewModel>();
            var rowCount = worksheet.Dimension.Rows;

            int colIndexProjectName = GetColIndex(worksheet, "ProjectName");
            int colIndexKeyWord = GetColIndex(worksheet, "KeyWord");

            for (int row = 7; row <= rowCount; row++)
            {
                ProjectKeyWordImportViewModel projectKeyWordImportViewModel = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

                if (!string.IsNullOrEmpty(cn))
                {
                    projectKeyWordImportViewModel = new ProjectKeyWordImportViewModel
                    {
                        RowNo = row,
                        Project = worksheet.Cells[row, colIndexProjectName].Value?.ToString().Trim(),
                        KeyWord = worksheet.Cells[row, colIndexKeyWord].Value?.ToString().Trim()
                    };
                    projectKeywordList.Add(projectKeyWordImportViewModel);
                }
            }
            return projectKeywordList;
        }

        private int GetColIndex(ExcelWorksheet worksheet, string colName)
        {
            int colIndex = worksheet.Cells["6:6"].First(c => c.Value.ToString() == colName).Start.Column;

            return colIndex;
        }

        private List<TrendImportViewModel> GetTrendImportData(ExcelWorksheet worksheet)
        {
            var list = new List<TrendImportViewModel>();
            var rowCount = worksheet.Dimension.Rows;

            for (int row = 7; row <= rowCount; row++)
            {
                list.Add(new TrendImportViewModel
                {
                    RowNo = row,
                    Project = worksheet.Cells[row, 1].Value?.ToString().Trim(),
                    TrendName = worksheet.Cells[row, 2].Value?.ToString().Trim(),
                    Description = worksheet.Cells[row, 3].Value?.ToString().Trim(),
                    TrendStatus = worksheet.Cells[row, 4].Value?.ToString().Trim(),
                    Importance = worksheet.Cells[row, 5].Value?.ToString().Trim(),
                    ImpactDirection = worksheet.Cells[row, 6].Value?.ToString().Trim(),
                    TrendValue = worksheet.Cells[row, 7].Value?.ToString().Trim(),
                    Category = worksheet.Cells[row, 8].Value?.ToString().Trim(),
                    CompanyGroup = worksheet.Cells[row, 9].Value?.ToString().Trim(),
                    CompanyGroupImpact = worksheet.Cells[row, 10].Value?.ToString().Trim(),
                    CompanyGroupEndUser = worksheet.Cells[row, 11].Value?.ToString().Trim(),
                    Company = worksheet.Cells[row, 12].Value?.ToString().Trim(),
                    Country = worksheet.Cells[row, 13].Value?.ToString().Trim(),
                    CountryImpact = worksheet.Cells[row, 14].Value?.ToString().Trim(),
                    CountryEndUser = worksheet.Cells[row, 15].Value?.ToString().Trim(),
                    EcoSystem = worksheet.Cells[row, 16].Value?.ToString().Trim(),
                    EcoSystemSubSegment = worksheet.Cells[row, 17].Value?.ToString().Trim(),

                    EcoSystemImpact = worksheet.Cells[row, 18].Value?.ToString().Trim(),
                    EcoSystemEndUser = worksheet.Cells[row, 19].Value?.ToString().Trim(),
                    TrendKeyWord = worksheet.Cells[row, 20].Value?.ToString().Trim(),
                    Industry = worksheet.Cells[row, 21].Value?.ToString().Trim(),
                    IndustryImpact = worksheet.Cells[row, 22].Value?.ToString().Trim(),
                    IndustryEndUser = worksheet.Cells[row, 23].Value?.ToString().Trim(),
                    Market = worksheet.Cells[row, 24].Value?.ToString().Trim(),
                    Region = worksheet.Cells[row, 25].Value?.ToString().Trim(),
                    RegionImpact = worksheet.Cells[row, 26].Value?.ToString().Trim(),
                    RegionEndUser = worksheet.Cells[row, 27].Value?.ToString().Trim(),
                    MarketImpact = worksheet.Cells[row, 28].Value?.ToString().Trim(),
                    MarketEndUser = worksheet.Cells[row, 29].Value?.ToString().Trim(),
                    Project1 = worksheet.Cells[row, 30].Value?.ToString().Trim(),
                    ProjectImpact = worksheet.Cells[row, 31].Value?.ToString().Trim(),
                    ProjectEndUser = worksheet.Cells[row, 32].Value?.ToString().Trim(),
                    TimeTag = worksheet.Cells[row, 33].Value?.ToString().Trim(),
                    TimeTagImpact = worksheet.Cells[row, 34].Value?.ToString().Trim(),
                    TimeTagEndUser = worksheet.Cells[row, 35].Value?.ToString().Trim(),
                    ValueConversion = worksheet.Cells[row, 36].Value?.ToString().Trim(),
                    Rationale_2018 = worksheet.Cells[row, 37].Value?.ToString().Trim(),
                    Value_2018 = worksheet.Cells[row, 38].Value?.ToString().Trim(),
                    Rationale_2019 = worksheet.Cells[row, 39].Value?.ToString().Trim(),
                    Value_2019 = worksheet.Cells[row, 40].Value?.ToString().Trim(),
                    Rationale_2020 = worksheet.Cells[row, 41].Value?.ToString().Trim(),
                    Value_2020 = worksheet.Cells[row, 42].Value?.ToString().Trim(),
                    Rationale_2021 = worksheet.Cells[row, 43].Value?.ToString().Trim(),
                    Value_2021 = worksheet.Cells[row, 44].Value?.ToString().Trim(),
                    Rationale_2022 = worksheet.Cells[row, 45].Value?.ToString().Trim(),
                    Value_2022 = worksheet.Cells[row, 46].Value?.ToString().Trim(),
                    Rationale_2023 = worksheet.Cells[row, 47].Value?.ToString().Trim(),
                    Value_2023 = worksheet.Cells[row, 48].Value?.ToString().Trim(),
                    Rationale_2024 = worksheet.Cells[row, 49].Value?.ToString().Trim(),
                    Value_2024 = worksheet.Cells[row, 50].Value?.ToString().Trim(),
                    Rationale_2025 = worksheet.Cells[row, 51].Value?.ToString().Trim(),
                    Value_2025 = worksheet.Cells[row, 52].Value?.ToString().Trim(),
                    Rationale_2026 = worksheet.Cells[row, 53].Value?.ToString().Trim(),
                    Value_2026 = worksheet.Cells[row, 54].Value?.ToString().Trim(),
                    Rationale_2027 = worksheet.Cells[row, 55].Value?.ToString().Trim(),
                    Value_2027 = worksheet.Cells[row, 56].Value?.ToString().Trim(),
                    Rationale_2028 = worksheet.Cells[row, 57].Value?.ToString().Trim(),
                    Value_2028 = worksheet.Cells[row, 58].Value?.ToString().Trim(),
                    Rationale_2029 = worksheet.Cells[row, 59].Value?.ToString().Trim(),
                    Value_2029 = worksheet.Cells[row, 60].Value?.ToString().Trim(),
                    Rationale_2030 = worksheet.Cells[row, 61].Value?.ToString().Trim(),
                    Value_2030 = worksheet.Cells[row, 62].Value?.ToString().Trim(),
                    Rationale_2031 = worksheet.Cells[row, 63].Value?.ToString().Trim(),
                    Value_2031 = worksheet.Cells[row, 64].Value?.ToString().Trim(),
                    Rationale_2032 = worksheet.Cells[row, 65].Value?.ToString().Trim(),
                    Value_2032 = worksheet.Cells[row, 66].Value?.ToString().Trim(),
                    CompanyGroup_ApproverRemark = worksheet.Cells[row, 67].Value?.ToString().Trim(),
                    CompanyGroup_AuthorRemark = worksheet.Cells[row, 68].Value?.ToString().Trim(),
                    CompanyGroup_TrendStatus = worksheet.Cells[row, 69].Value?.ToString().Trim(),
                    EcoSystem_ApproverRemark = worksheet.Cells[row, 70].Value?.ToString().Trim(),
                    EcoSystem_AuthorRemark = worksheet.Cells[row, 71].Value?.ToString().Trim(),
                    EcoSystem_TrendStatus = worksheet.Cells[row, 72].Value?.ToString().Trim(),
                    Industry_ApproverRemark = worksheet.Cells[row, 73].Value?.ToString().Trim(),
                    Industry_AuthorRemark = worksheet.Cells[row, 74].Value?.ToString().Trim(),
                    Industry_TrendStatus = worksheet.Cells[row, 75].Value?.ToString().Trim(),
                    KeyCompany_ApproverRemark = worksheet.Cells[row, 76].Value?.ToString().Trim(),
                    KeyCompany_AuthorRemark = worksheet.Cells[row, 77].Value?.ToString().Trim(),
                    KeyCompany_TrendStatus = worksheet.Cells[row, 78].Value?.ToString().Trim(),
                    keyword_ApproverRemark = worksheet.Cells[row, 79].Value?.ToString().Trim(),
                    keyword_AuthorRemark = worksheet.Cells[row, 80].Value?.ToString().Trim(),
                    keyword_TrendStatus = worksheet.Cells[row, 81].Value?.ToString().Trim(),
                    Market_ApproverRemark = worksheet.Cells[row, 82].Value?.ToString().Trim(),
                    Market_AuthorRemark = worksheet.Cells[row, 83].Value?.ToString().Trim(),
                    Market_TrendStatus = worksheet.Cells[row, 84].Value?.ToString().Trim(),
                    Project_ApproverRemark = worksheet.Cells[row, 85].Value?.ToString().Trim(),
                    Project_AuthorRemark = worksheet.Cells[row, 86].Value?.ToString().Trim(),
                    Project_TrendStatus = worksheet.Cells[row, 87].Value?.ToString().Trim(),
                    Region_ApproverRemark = worksheet.Cells[row, 88].Value?.ToString().Trim(),
                    Region_AuthorRemark = worksheet.Cells[row, 89].Value?.ToString().Trim(),
                    Region_TrendStatus = worksheet.Cells[row, 90].Value?.ToString().Trim(),
                    TimeTag_ApproverRemark = worksheet.Cells[row, 91].Value?.ToString().Trim(),
                    TimeTag_AuthorRemark = worksheet.Cells[row, 92].Value?.ToString().Trim(),
                    TimeTag_TrendStatus = worksheet.Cells[row, 93].Value?.ToString().Trim(),
                    Value_ApproverRemark = worksheet.Cells[row, 94].Value?.ToString().Trim(),
                    Value_AuthorRemark = worksheet.Cells[row, 95].Value?.ToString().Trim(),
                    Value_TrendStatus = worksheet.Cells[row, 96].Value?.ToString().Trim(),
                    Trend_ApproverRemark = worksheet.Cells[row, 97].Value?.ToString().Trim(),
                    Trend_AuthorRemark = worksheet.Cells[row, 98].Value?.ToString().Trim(),
                });
            }
            return list;
        }

        private static byte[] GetBytes(ExcelPackage file)
        {
            using (var stream = new MemoryStream())
            {
                file.SaveAs(stream);
                return stream.ToArray();
            }
        }
    }
}
