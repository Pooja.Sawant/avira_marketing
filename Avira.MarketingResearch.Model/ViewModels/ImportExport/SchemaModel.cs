﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.ImportExport
{
    public class SchemaModel
    {
        public string SchemaType { get; set;}
        public string TableName { get; set; }
        public string DataBaseName { get; set; }
    }
}
