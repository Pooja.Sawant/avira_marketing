﻿using Avira.MarketingResearch.Model.ViewModels.Masters;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Avira.MarketingResearch.Utility
{
    public class FileUpload
    {
        private ILoggerManager _logger;
        public FileUploadViewModel SaveFile(IFormFile file, string pathToUplaod)
        {
            FileUploadViewModel FileUploadViewModel = new FileUploadViewModel();
            string filename= GetFileName(file, true);
            string pathwithfileName = pathToUplaod + "\\" + filename;
            using (var fileStream = new FileStream(pathwithfileName, FileMode.Create))
            {
                file.CopyTo(fileStream);
            }
            FileUploadViewModel.FileUrl = pathwithfileName;
            FileUploadViewModel.FileName = filename;

            return FileUploadViewModel;
        }
        public string GetFileName(IFormFile file, bool BuildUniqeName)
        {
            string fileName = string.Empty;
            string strFileName = file.FileName.Substring(
                file.FileName.LastIndexOf("\\"))
                .Replace("\\", string.Empty);

            string fileExtension = GetFileExtension(file);
            if (BuildUniqeName)
            {
                string strUniqName = GetUniqueName();
                fileName = strUniqName + fileExtension;
            }
            else
            {
                fileName = strFileName;
            }
            return fileName;
        }
        public static string GetUniqueName()
        {
            string uName = Guid.NewGuid() + DateTime.UtcNow.ToShortDateString()
                .Replace("/", "-")
                .Replace(":", "-")
                .Replace(" ", string.Empty)
                .Replace("PM", string.Empty)
                .Replace("AM", string.Empty);
            return uName;
        }
        public static string GetFileExtension(IFormFile file)
        {
            string fileExtension;
            fileExtension = (file != null) ?
                file.FileName.Substring(file.FileName.LastIndexOf('.')).ToLower()
                : string.Empty;
            return fileExtension;
        }

        public static string ByteArrayToFile(string fileName, byte[] byteArray, string pathToUplaod,string fileExtension)
        {
            try
            {
                string pathwithfileName = string.Empty;
                if (!string.IsNullOrEmpty(fileName))
                {
                    DeleteFile(pathToUplaod, fileName);//Delete existing file
                }
                string strUniqName = GetUniqueName();//Generate new file name
                fileName = strUniqName + fileExtension;
                pathwithfileName = pathToUplaod + "/" + fileName;
                  
               
                using (var fs = new FileStream(pathwithfileName, FileMode.Create, FileAccess.Write))
                {
                    fs.Write(byteArray, 0, byteArray.Length);
                }
               
                return fileName;
            }

            catch (Exception ex)
            {
                return null;
            }

        }
        public static bool ByteArrayToFile(byte[] byteArray, string filePathToUplaod)
        {
            var isSuccess = false;
            try
            {
               
                if (!string.IsNullOrEmpty(filePathToUplaod))
                {
                    DeleteFile(filePathToUplaod);//Delete existing file
                }
                
                using (var fs = new FileStream(filePathToUplaod, FileMode.Create, FileAccess.Write))
                {
                    fs.Write(byteArray, 0, byteArray.Length);
                }

                isSuccess = true;
            }

            catch (Exception ex)
            {
                //return null;
            }
            return isSuccess;

        }

        public static byte[] FileTobyteArray(string importExceptionFilePath)
        {
            byte[] filebyte = null;
            try
            {
                // Convert file to byte array  
                if (System.IO.File.Exists(importExceptionFilePath))
                {
                    filebyte=File.ReadAllBytes(importExceptionFilePath);
                }
            }
            catch (Exception ex)
            {
                filebyte = null;
            }
            return filebyte;
        }

        public static string ByteArrayToExceFile(byte[] byteArray, string pathToUplaod, string fileExtension)
        {
            try
            {
                string pathwithfileName = string.Empty;
                string fileName = string.Empty;

                string strUniqName = @"MarketsizingImport" + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss");
                fileName = strUniqName + fileExtension;
                pathwithfileName = pathToUplaod + "/" + fileName;


                using (var fs = new FileStream(pathwithfileName, FileMode.Create, FileAccess.Write))
                {
                    fs.Write(byteArray, 0, byteArray.Length);
                }

                return fileName;
            }

            catch (Exception ex)
            {
                return null;
            }

        }

        public static void DeleteFile(string filePath, string fileName)
        {
            try
            {
                // Check if file exists with its full path    
                if (File.Exists(Path.Combine(filePath, fileName)))
                {
                    // If file found, delete it    
                    File.Delete(Path.Combine(filePath, fileName));
                }
            }
            catch (IOException ioExp)
            {
               // Console.WriteLine(ioExp.Message);
            }
        }

        public static bool DeleteFile(string filePath)
        {
            var isSuccess = false;
            try
            {
                // Check if file exists with its full path    
                if (File.Exists(filePath))
                {
                    // If file found, delete it    
                    File.Delete(filePath);
                }
                isSuccess = true;
            }
            catch (IOException ioExp)
            {
                // Console.WriteLine(ioExp.Message);
            }
            return isSuccess;
        }
    }
}
