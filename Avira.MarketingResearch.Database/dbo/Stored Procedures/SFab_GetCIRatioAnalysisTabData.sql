﻿
--drop Procedure SFab_GetCIRatioAnalysisTabData

--execute [SFab_GetCIRatioAnalysisTabData]  @CompanyID = '2e51f475-663b-4fbf-9b75-1b9418a292e6'

CREATE   Procedure [dbo].[SFab_GetCIRatioAnalysisTabData] 
(
	@CompanyID uniqueidentifier = null
)
As
/*================================================================================  
Procedure Name: [SFab_GetCIRatioAnalysisTabData]  
Author: Nitin 
Create date: 12-Nov-2019  
Description: Get CI Company Ratio Analysis Tab Data
Change History  
Date         Developer     Reason  
____________ __________  ______________________________________________________  
12-Nov-2019  Nitin       Initial Version  
================================================================================*/  
BEGIN

	--Declare @CompanyID uniqueidentifier = '97ad6d5a-56a8-4af7-961a-e220725c52f1'

	Declare @UnitId uniqueidentifier

	Declare @Years Table (CompanyID uniqueidentifier, [Year] int)
	Declare @MaxYear int, @MinYear int

	Select @MaxYear = Max([Year]), @MinYear = Min([Year])
	From CompanyPLStatement
	Where CompanyId = @CompanyID;	

	If (@MaxYear Is Not Null And @MinYear Is Not Null)
	Begin
		WITH CTE
			 AS (SELECT @CompanyID As CompanyID, @MinYear As Year1
				 UNION ALL
				 SELECT @CompanyID As CompanyID, Year1 + 1 
				 FROM   CTE
				 WHERE  Year1 < @MaxYear)
		Insert Into @Years
		SELECT *
		FROM   CTE 
	End
	
	--Select * From @Years Order by 1 desc;

	Declare @CICompanyRatioAnalysisTabData Table(
		CompanyId uniqueidentifier,
		[Year] int,
		RevenueGrowthPercentage	decimal (19, 4),
		OperatingProfitGrowthPercentage	decimal (19, 4),
		NetProfitGrowthPercentage	decimal (19, 4),
		GrossProfitMarginPercentage	decimal (19, 4),
		OperatingProfitMarginPercentage	decimal (19, 4),
		NetProfitMarginPercentage	decimal (19, 4),
		ReturnOnTangibleNetworthPercentage	decimal (19, 4),
		ReturnOnAverageTangibleNetworthPercentage	decimal (19, 4),
		ReturnOnCapitalEmployedPercentage	decimal (19, 4),
		ReturnOnAverageCapitalEmployedPercentage	decimal (19, 4),
		ReturnOnFixedAssetsPercentage	decimal (19, 4),
		ReturnOnTotalAssetsPercentage	decimal (19, 4),
		QuickRatioTimes	decimal (19, 4),
		CurrentRatioTimes	decimal (19, 4),
		InventoryTurnoverRatioTimes	decimal (19, 4),
		FixedAssetsTurnoverRatioTimes	decimal (19, 4),
		LongTermDebtEquityRatioTimes	decimal (19, 4),
		TotalDebtEquityRatioTimes	decimal (19, 4),
		InterestCoverageRatioTimes	decimal (19, 4),
		CurrentLiabilitiesToTangibleNetworthPercentage	decimal (19, 4),
		WorkingCapitalTurnoverRatioTimes	decimal (19, 4),
		TotalDebtEBITDATimes	decimal (19, 4),
		InterestCoverageRatioTimes1	decimal (19, 4),
		DebtServiceCoverageRatioTimes	decimal (19, 4)
	)

	Select @UnitId = Id 
	From ValueConversion
	Where ValueName = 'Millions';

	Insert Into @CICompanyRatioAnalysisTabData (CompanyId, [Year], 		
		RevenueGrowthPercentage,
		OperatingProfitGrowthPercentage,
		NetProfitGrowthPercentage,
		GrossProfitMarginPercentage,
		OperatingProfitMarginPercentage,
		NetProfitMarginPercentage,
		ReturnOnTangibleNetworthPercentage,
		ReturnOnAverageTangibleNetworthPercentage,
		ReturnOnCapitalEmployedPercentage,
		ReturnOnAverageCapitalEmployedPercentage,
		ReturnOnFixedAssetsPercentage,
		ReturnOnTotalAssetsPercentage,
		QuickRatioTimes,
		CurrentRatioTimes,
		InventoryTurnoverRatioTimes,
		FixedAssetsTurnoverRatioTimes,
		LongTermDebtEquityRatioTimes,
		TotalDebtEquityRatioTimes,
		InterestCoverageRatioTimes,
		CurrentLiabilitiesToTangibleNetworthPercentage,
		WorkingCapitalTurnoverRatioTimes,
		TotalDebtEBITDATimes,
		InterestCoverageRatioTimes1,
		DebtServiceCoverageRatioTimes
	)
	Select Y.CompanyId, Y.[Year],			
			((CPLS.Revenue * 1.00) / NullIf(LAG(CPLS.Revenue) Over (Order By Y.[Year]), 0) -1) * 100 As GrowthPercentage,
			((CPLS.EBIT * 1.00) / NullIf(LAG(CPLS.EBIT) Over (Order By Y.[Year]), 0) -1) * 100 As OperatingProfitGrowthPercentage,
			((CPLS.NetIncome * 1.00) / NullIf(LAG(CPLS.NetIncome) Over (Order By CPLS.[Year]), 0) -1) * 100 As NetProfitGrowthPercentage,
			((CPLS.GrossProfit * 1.00) / NullIf(CPLS.Revenue, 0)) * 100 As GrossProfitMarginPercentage,
			((CPLS.EBIT * 1.00) / NullIf(CPLS.Revenue, 0)) * 100 As OperatingProfitMarginPercentage,
			((CPLS.NetIncome * 1.00) / NullIf(CPLS.Revenue, 0)) * 100 As NetProfitMarginPercentage,
			((CPLS.NetIncome * 1.00) / 
				NullIf(Cast(IsNull(CBS.TotalAssets, 0) As decimal(19, 4)) - 
					   Cast(IsNull(CBS.TotalLiabilities, 0) As decimal(19, 4)) - 
					   Cast(IsNull(CBS.IntangibleAssets, 0) As decimal(19, 4)) -
					   Cast(IsNull(CBS.Goodwill, 0) As decimal(19, 4))
					   , 0)
			) * 100 As ReturnOnTangibleNetworthPercentage,
			(CPLS.NetIncome * 1.00) / 
				NullIf(((Cast(IsNull(CBS.TotalAssets, 0) As decimal(19, 4)) - 
					   Cast(IsNull(CBS.TotalLiabilities, 0) As decimal(19, 4)) - 
					   Cast(IsNull(CBS.IntangibleAssets, 0) As decimal(19, 4)) -
					   Cast(IsNull(CBS.Goodwill, 0) As decimal(19, 4))) +
					  (Cast(IsNull(LAG(CBS.TotalAssets) Over (Order By Y.[Year]), 0) As decimal(19, 4)) - 
						Cast(IsNull(LAG(CBS.TotalLiabilities) Over (Order By Y.[Year]), 0) As decimal(19, 4)) - 
						Cast(IsNull(LAG(CBS.IntangibleAssets) Over (Order By Y.[Year]), 0) As decimal(19, 4)) - 
						Cast(IsNull(LAG(CBS.Goodwill) Over (Order By Y.[Year]), 0) As decimal(19, 4)))
				) / 2, 0) * 100 As ReturnOnAverageTangibleNetworthPercentage,
			((CPLS.EBIT * 1.00) / 
				NullIf(Cast(IsNull(CBS.TotalAssets, 0) As decimal(19, 4)) - 
					   Cast(IsNull(CBS.TotalCurrentLiabilities, 0) As decimal(19, 4)), 0)) * 100 As ReturnOnCapitalEmployedPercentage,
			((CPLS.EBIT * 1.00) / 
				NullIf(
						((Cast(IsNull(CBS.TotalAssets, 0) As decimal(19, 4)) - 
						  Cast(IsNull(CBS.TotalCurrentLiabilities, 0) As decimal(19, 4))
						 ) + 
						 (Cast(IsNull(LAG(CBS.TotalAssets) Over (Order By Y.[Year]), 0) As decimal(19, 4)) - 
						  Cast(IsNull(LAG(CBS.TotalCurrentLiabilities) Over (Order By Y.[Year]), 0) As decimal(19, 4))
						 )
						) / 2, 0)) * 100 As ReturnOnAverageCapitalEmployedPercentage,
			(CPLS.Revenue * 1.00) /  NullIf(Cast(IsNull(CBS.PropertyPlantAndEquipment, 0) As decimal(19, 4)), 0) * 100 As ReturnOnFixedAssetsPercentage,
			(CPLS.Revenue * 1.00) /  NullIf(Cast(IsNull(CBS.TotalAssets, 0) As decimal(19, 4)), 0) * 100 As ReturnOnTotalAssetsPercentage,
			(Cast(IsNull(CBS.CashAndCashEquivalents, 0) As decimal(19, 4)) + 
				Cast(IsNull(CBS.ShortTermInvestments, 0) As decimal(19, 4)) ) /  NullIf(Cast(IsNull(CBS.TotalCurrentLiabilities, 0) As decimal(19, 4)), 0) 
				 As QuickRatioTimes,
			(Cast(IsNull(CBS.TotalCurrentAssets, 0) As decimal(19, 4)) /  
				NullIf(Cast(IsNull(CBS.TotalCurrentLiabilities, 0) As decimal(19, 4)),0)) As CurrentRatioTimes, 
			Cast(IsNull(CBS.Inventories, 0) As decimal(19, 4)) / 
				(NullIf((IsNull(CPLS.CostOfProduction, 0) + IsNull(CPLS.ResourceCost, 0)) +
				  IsNull(LAG(CPLS.CostOfProduction) Over (Order By CPLS.[Year]), 0) + 
				  IsNull(LAG(CPLS.ResourceCost) Over (Order By CPLS.[Year]), 0), 0) / 2) As InventoryTurnoverRatioTimes,
			(CPLS.Revenue * 1.00) / 
				(NullIf(Cast(IsNull(CBS.PropertyPlantAndEquipment, 0) As decimal(19, 4)) + 
				     Cast(IsNull(LAG(CBS.PropertyPlantAndEquipment) Over (Order By CPLS.[Year]), 0) As decimal(19, 4)), 0) / 2)  As FixedAssetsTurnoverRatioTimes,
			Cast(IsNull(CBS.LongTermDebt, 0) As decimal(19, 4)) / 
				NullIf(Cast(IsNull(CBS.TotalShareholdersEquity, 0) As decimal(19, 4)), 0) As LongTermDebtEquityRatioTimes,
			(Cast(IsNull(CBS.ShortTermDebt, 0) As decimal(19, 4)) + Cast(IsNull(CBS.LongTermDebt, 0) As decimal(19, 4))) / 
				NullIf(Cast(IsNull(CBS.TotalShareholdersEquity, 0) As decimal(19, 4)), 0) As TotalDebtEquityRatioTimes,
			(CPLS.EBIT * 1.00) / NullIf(CPLS.InterestExpense, 0)  As InterestCoverageRatioTimes,
			(Cast(IsNull(CBS.TotalCurrentLiabilities, 0) As decimal(19, 4)) / 
				NullIf(Cast(IsNull(CBS.TotalAssets, 0) As decimal(19, 4)) - 
					   Cast(IsNull(CBS.TotalLiabilities, 0) As decimal(19, 4)) - 
					   Cast(IsNull(CBS.IntangibleAssets, 0) As decimal(19, 4)) -
					   Cast(IsNull(CBS.Goodwill, 0) As decimal(19, 4))
					   , 0)
			) * 100 As CurrentLiabilitiesToTangibleNetworthPercentage,
			(CPLS.Revenue * 1.00) / 
				NullIf(Cast(IsNull(CBS.TotalCurrentAssets, 0) As decimal(19, 4)) -
				 Cast(IsNull(CBS.TotalCurrentLiabilities, 0) As decimal(19, 4)), 0) As WorkingCapitalTurnoverRatioTimes,
			(Cast(IsNull(CBS.ShortTermDebt, 0) As decimal(19, 4)) + Cast(IsNull(CBS.LongTermDebt, 0) As decimal(19, 4))) /
				NullIf(CPLS.EBITDA, 0) As TotalDebtEBITDATimes,
			(CPLS.EBIT * 1.00) / NullIf(CPLS.InterestExpense, 0) As InterestCoverageRatioTimes1,
			(CPLS.EBIT * 1.00) / 
				NullIf(Cast(IsNull(CBS.ShortTermDebt, 0) As decimal(19, 4)) + Cast(IsNull(CBS.LongTermDebt, 0) As decimal(19, 4)), 0) 
					As DebtServiceCoverageRatioTimes
	From @Years Y 
		Left Join CompanyPLStatement CPLS 
			On Y.[Year] = CPLS.[Year] And CPLS.CompanyId = @CompanyID
		Left Join CompanyBalanceSheet CBS 
			On Y.[Year] = CBS.[Year] And CPLS.CompanyId = CBS.CompanyId 
		--Left Join CompanyCashFlowStatement CCFS 
		--	On Y.[Year] = CCFS.[Year] And CPLS.CompanyId = CCFS.CompanyId 
	Order By Y.[Year] 

	Select top 7 CompanyId, [Year], 		
		RevenueGrowthPercentage,
		OperatingProfitGrowthPercentage,
		NetProfitGrowthPercentage,
		GrossProfitMarginPercentage,
		OperatingProfitMarginPercentage,
		NetProfitMarginPercentage,
		ReturnOnTangibleNetworthPercentage,
		ReturnOnAverageTangibleNetworthPercentage,
		ReturnOnCapitalEmployedPercentage,
		ReturnOnAverageCapitalEmployedPercentage,
		ReturnOnFixedAssetsPercentage,
		ReturnOnTotalAssetsPercentage,
		QuickRatioTimes,
		CurrentRatioTimes,
		InventoryTurnoverRatioTimes,
		FixedAssetsTurnoverRatioTimes,
		LongTermDebtEquityRatioTimes,
		TotalDebtEquityRatioTimes,
		InterestCoverageRatioTimes,
		CurrentLiabilitiesToTangibleNetworthPercentage,
		WorkingCapitalTurnoverRatioTimes,
		TotalDebtEBITDATimes,
		InterestCoverageRatioTimes1,
		DebtServiceCoverageRatioTimes
	From @CICompanyRatioAnalysisTabData
	Order By [Year] Desc 
END