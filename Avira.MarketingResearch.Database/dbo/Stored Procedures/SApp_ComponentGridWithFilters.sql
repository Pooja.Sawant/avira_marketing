﻿CREATE PROCEDURE [dbo].[SApp_ComponentGridWithFilters]
	(@ComponentIDList dbo.udtUniqueIdentifier READONLY,
	@ComponentName nvarchar(256)='',
	@Description nvarchar(256)='',
	@includeDeleted bit = 0,
	@OrdbyByColumnName NVARCHAR(50) ='CreatedDate',
	@SortDirection INT = -1,
	@PageStart INT = 0,
	@PageSize INT = Null)
	AS
/*================================================================================
Procedure Name: SApp_GetComponentbyID
Author: Adarsh
Create date: 03/16/2019
Description: Get list from Component table by Name or ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
03/16/2019	Adarsh			Initial Version
__________	____________	______________________________________________________
04/22/2019	Pooja			For Client side paging comment pagesize and null init
================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
declare @SegmentId uniqueidentifier;
select @SegmentId = Id from Segment
where SegmentName = 'Components';

if @includeDeleted is null set @includeDeleted = 0;
if len(@ComponentName)>2 set @ComponentName = '%'+ @ComponentName + '%'
else set @ComponentName = ''
if len(@Description)>2 set @Description = '%'+ @Description + '%';
else set @Description = '';
	if @PageSize is null or @PageSize =0
	set @PageSize = 10
	begin
	;WITH CTE_TBL AS (

	SELECT [SubSegment].[Id],
		[SubSegment].[SubSegmentName] as [ComponentName],
		[SubSegment].[Description],
		[SubSegment].[IsDeleted],
		CASE
		WHEN @OrdbyByColumnName = 'ComponentName' THEN [SubSegment].[SubSegmentName]
		WHEN @OrdbyByColumnName = 'Description' THEN [SubSegment].[Description]
		ELSE [SubSegment].[SubSegmentName]
	END AS SortColumn

FROM [dbo].[SubSegment]
where (NOT EXISTS (SELECT * FROM @ComponentIDList) or  [SubSegment].Id  in (select * from @ComponentIDList))
	AND SegmentId = @SegmentId
	AND (@ComponentName = '' or [SubSegment].[SubSegmentName] like @ComponentName)
	AND (@Description = '' or [SubSegment].[Description] like @Description)
	AND (@includeDeleted = 1 or [SubSegment].IsDeleted = 0)
	)

	SELECT 
		  CTE_TBL.*,
		  tCountResult.TotalItems
		FROM CTE_TBL
		CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult
		ORDER BY 
		  CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,
		  CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC 
		OFFSET @PageStart ROWS
		FETCH NEXT @PageSize ROWS ONLY;
		END
	END