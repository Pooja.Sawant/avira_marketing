﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.Masters
{
    public class FileUploadViewModel
    {
        public string FileUrl { get; set; }
        public string FileName { get; set; }
    }
}
