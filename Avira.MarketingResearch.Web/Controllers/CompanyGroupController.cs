﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.Models;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.Web.Controllers
{
    [NoCache]
    [Authorize]
    public class CompanyGroupController : BaseController
    {
        private ILoggerManager _logger;
        public readonly IServiceRepository _serviceRepository;
        public CompanyGroupController(IServiceRepository serviceRepository, ILoggerManager logger)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
        }

        public IActionResult Index()
        { 
            return View();
        }
     
        public ActionResult LoadData(CompanyGroupViewModel companyGroup)
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            int totalRecords = 0;
            List<CompanyGroupViewModel> data = null;
            try
            {
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                string search = Request.Form["search[value]"];
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDir = Request.Form["order[0][dir]"].FirstOrDefault();
                var SortDirection = -1;
                if (sortColumnDir == "asc")
                {
                    SortDirection = 1;
                }
                else
                {
                    SortDirection = -1;
                }

                int skip = start != null ? Convert.ToInt32(start) : 0;
                int pageSize = length.ToString() != null ? Convert.ToInt32(length) : 0;
                int PageStart = start.ToString() != null ? Convert.ToInt32(start) : 0;
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.GetResponse(contollerName, PageStart, pageSize, SortDirection, sortColumn, search);
                response.EnsureSuccessStatusCode();
                List<CompanyGroupViewModel> companyGroups = response.Content.ReadAsAsync<List<CompanyGroupViewModel>>().Result;
                ViewBag.Title = "All CompanyGroup";
                data = companyGroups;
              
                foreach (var item in companyGroups)
                {
                    totalRecords = item.TotalItems;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data });
                        
        }
       
        [HttpGet]
        public IActionResult Edit(string id)
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.GetByIdResponse(contollerName, id);
                response.EnsureSuccessStatusCode();
                CompanyGroupUpdateDbViewModel companyGroup = response.Content.ReadAsAsync<CompanyGroupUpdateDbViewModel>().Result;
                return View(companyGroup);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                ErrorViewModel err = new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier };
                return View("Error", err);
            }
        }

        [HttpGet]
        public IActionResult Delete(string id)
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.GetByIdResponse(contollerName, id);
                response.EnsureSuccessStatusCode();
                CompanyGroupUpdateDbViewModel companyGroup = response.Content.ReadAsAsync<CompanyGroupUpdateDbViewModel>().Result;
                return View(companyGroup);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                ErrorViewModel err = new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier };
                return View("Error", err);
            }
        }

        [HttpPost]
        public IActionResult Edit(CompanyGroupUpdateDbViewModel companyGroup)
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.PutResponse(contollerName, companyGroup);
                ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                if (viewModel != null)
                {
                    ViewBag.Message = viewModel.Message;
                }
                response.EnsureSuccessStatusCode();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(companyGroup);
        }

        public IActionResult Create()
        {

            return View();
        }

        [HttpPost]
        public IActionResult Create(CompanyGroupInsertDbViewModel companyGroup)
        {

            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, companyGroup);
                response.EnsureSuccessStatusCode();
                ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                if (viewModel != null)
                {
                    ViewBag.Message = viewModel.Message;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(companyGroup);
        }

        [HttpPost]
        public IActionResult Delete(CompanyGroupUpdateDbViewModel companyGroup)
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.DeleteResponse(contollerName, companyGroup);
                response.EnsureSuccessStatusCode();
                ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                if (viewModel != null)
                {
                    ViewBag.Message = viewModel.Message;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return RedirectToAction("Index");
        }
    }
}