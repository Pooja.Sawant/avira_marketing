﻿--sp_helptext SApp_UpdateRegion

     
CREATE PROCEDURE [dbo].[SApp_UpdateRegion]            
(@Id uniqueidentifier,            
@RegionName nvarchar(256),        
@RegionLevel int = 0,        
@ParentRegionId uniqueidentifier = null,        
@IsDeleted bit=0,         
@CountryIDList dbo.udtUniqueIdentifier READONLY,        
@UserModifiedById  uniqueidentifier  = null,          
@UserDeletedById uniqueidentifier = null,          
@ModifiedOn Datetime,        
@DeletedOn datetime = null          
)        
as            
/*================================================================================            
Procedure Name: SApp_UpdateRegion            
Author: Sharath            
Create date: 03/19/2019            
Description: update a record in Region table by ID. Also used for delete.         
   This is used to assign or unassign countries to region        
Change History            
Date  Developer  Reason  
__________ ____________ ______________________________________________________            
03/19/2019 Sharath   Initial Version     
04/09/2019 Praveen    Update proc to mark all countries which are in list as not deleted   
================================================================================*/            
BEGIN            
SET NOCOUNT ON;            
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;        
          
IF exists(select 1 from [dbo].[Region]             
   where [Id] !=  @Id            
   and  RegionName = @RegionName      
   AND IsDeleted = 0    
   and  (ParentRegionId = @ParentRegionId or (@ParentRegionId is null and ParentRegionId is null))
   )            
begin            
declare @ErrorMsg NVARCHAR(2048);            
set @ErrorMsg = 'Region with Name "'+ @RegionName + '" already exists in the parent region group.';            
--SELECT 50000,@ErrorMsg,1;           
--return(1);           
SELECT StoredProcedureName ='SApp_UpdateRegion','Message' =@ErrorMsg,'Success' = 0;          
RETURN;        
END            
          
BEGIN TRY             
--update record            
UPDATE [dbo].[Region]            
SET [RegionName] = @RegionName,            
 [RegionLevel] = @RegionLevel,            
 [ParentRegionId] = @ParentRegionId,              
 [ModifiedOn] = @ModifiedOn,            
 DeletedOn = CASE WHEN IsDeleted = 0 AND @IsDeleted = 1 THEN @ModifiedOn ELSE DeletedOn END,            
 UserDeletedById = CASE WHEN IsDeleted = 0 AND @IsDeleted = 1 THEN @UserModifiedById ELSE UserDeletedById END,            
 IsDeleted = @IsDeleted,        
 [UserModifiedById] = @UserModifiedById            
    WHERE ID = @Id           
        
--Refresh country list        
--Mark all the countries which are not in list as deleted        
UPDATE lkup        
SET IsDeleted = 1,        
 DeletedOn = CASE WHEN IsDeleted = 0 THEN @ModifiedOn ELSE DeletedOn END,            
 UserDeletedById = CASE WHEN IsDeleted = 0 THEN @UserModifiedById ELSE UserDeletedById END           
FROM [dbo].[CountryRegionMap] lkup        
WHERE lkup.RegionId = @Id        
AND NOT EXISTS(SELECT 1 FROM @CountryIDList list        
    WHERE list.id = lkup.CountryId)    
   
   
------Added on 09/04/2019 Mark all countries which are in list as not deleted---  
  
UPDATE lkup        
SET IsDeleted = 0,        
 DeletedOn = CASE WHEN IsDeleted = 1 THEN @ModifiedOn ELSE DeletedOn END,            
 UserDeletedById = CASE WHEN IsDeleted = 1 THEN @UserModifiedById ELSE UserDeletedById END           
FROM [dbo].[CountryRegionMap] lkup        
WHERE IsDeleted = 1 AND lkup.RegionId = @Id        
AND EXISTS(SELECT 1 FROM @CountryIDList list        
    WHERE list.id = lkup.CountryId)    
  
---------------------------------------------------------  
        
--Add newly added list if not exists in mapping        
 INSERT INTO [dbo].[CountryRegionMap]        
  ([Id],        
  CountryId,        
  RegionId,         
  [IsDeleted],        
  [UserCreatedById],         
  [CreatedOn])         
   SELECT NEWID() as ID,        
   list.ID as CountryId,         
   @Id as RegionId,        
   0 as [IsDeleted],         
   @UserModifiedById as [UserCreatedById],         
   @ModifiedOn as [CreatedOn]        
   FROM @CountryIDList list        
   WHERE NOT EXISTS (SELECT 1 FROM [dbo].[CountryRegionMap] lkup(NOLOCK)        
      WHERE list.id = lkup.CountryId        
      AND lkup.RegionId = @Id);        
        
SELECT StoredProcedureName ='SApp_UpdateRegion',Message =@ErrorMsg,Success = 1;             
          
END TRY            
BEGIN CATCH            
    -- Execute the error retrieval routine.            
 DECLARE @ErrorNumber int;            
 DECLARE @ErrorSeverity int;            
 DECLARE @ErrorProcedure varchar;            
 DECLARE @ErrorLine int;            
 DECLARE @ErrorMessage varchar;            
            
  SELECT @ErrorNumber = ERROR_NUMBER(),            
        @ErrorSeverity = ERROR_SEVERITY(),     
        @ErrorProcedure = ERROR_PROCEDURE(),            
        @ErrorLine = ERROR_LINE(),            
        @ErrorMessage = ERROR_MESSAGE()            
            
 insert into dbo.Errorlog(ErrorNumber,            
       ErrorSeverity,            
       ErrorProcedure,            
       ErrorLine,            
       ErrorMessage,            
       ErrorDate)            
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());         
        
set @ErrorMsg = 'Error while updating Region, please contact Admin for more details.'              
 --set @ErrorMsg = 'Unspecified Error';            
--THROW 50000,@ErrorMsg,1;            
 --return(1)          
 SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine          
    ,Message =@ErrorMsg,Success = 0;             
END CATCH            
            
          
          
            
end