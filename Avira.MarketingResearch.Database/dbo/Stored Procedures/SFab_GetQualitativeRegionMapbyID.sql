﻿

CREATE procedure [dbo].[SFab_GetQualitativeRegionMapbyID]
(@ProjectID uniqueidentifier=null,
@includeDeleted bit = 0,
@AviraUserID uniqueidentifier = null)
as
/*================================================================================
Procedure Name: SFab_GetQualitativeRegionMapbyID
Author: Harshal 
Create date: 09/04/2019
Description: Get list from QualitativeRegionMapping table by Filters
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
09/04/2019	Harshal			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if @includeDeleted is null set @includeDeleted = 0

SELECT distinct
       [RegionName]
      ,MRP.RegionId
	  
FROM [dbo].[QualitativeRegionMapping] MRP
INNER JOIN Region Region ON Region.Id=MRP.RegionId
where (@ProjectId is null or MRP.ProjectId = @ProjectID)
and (@includeDeleted = 1 or MRP.IsDeleted = 0) order by RegionName

end