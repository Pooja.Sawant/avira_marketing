﻿CREATE procedure [dbo].[SApp_GetServicebyID]
(@ServiceID uniqueidentifier,
@includeDeleted bit = 0)
as
/*================================================================================
Procedure Name: SApp_GetServicebyID
Author: Sharath
Create date: 02/19/2019
Description: Get list from Service table by Name or ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
02/19/2019	Sharath			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @SegmentId uniqueidentifier;
select @SegmentId = Id from Segment
where SegmentName = 'End User';

if @includeDeleted is null set @includeDeleted = 0

SELECT [Id],
		SubSegmentName as [ServiceName],
		[Description],
		ParentId as ParentServiceId
FROM [dbo].SubSegment
where (@ServiceID is null or ID = @ServiceID)
and SegmentId = @SegmentId
and (@includeDeleted = 1 or IsDeleted = 0)



end