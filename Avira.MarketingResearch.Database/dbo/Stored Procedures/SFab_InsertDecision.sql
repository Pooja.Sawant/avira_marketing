﻿CREATE PROCEDURE [dbo].[SFab_InsertDecision]
(
@Description	nvarchar(1024),
@BusinessObjectivesJsonList nvarchar(max) = null,
@TargetedOutcomeJsonList nvarchar(max) = null,
@ResourceAssigned	nvarchar(512),
@DecisionDeadLine datetime2(7),
@TimeTagOutlookId	uniqueidentifier	,
@DecisionTypeId	uniqueidentifier,
@BusinessFunctionId uniqueidentifier,
@BusinessStageId uniqueidentifier,
@BusinessRoleId uniqueidentifier,
@UserId	uniqueidentifier,
@DecisionId uniqueidentifier out
)
As   

/*================================================================================    
Procedure Name: SFab_InsertDecision   
Author: Praveen    
Create date: 07-Jan-2020
Description: Create new decision by user
Change History    
Date  Developer  Reason  
__________ ____________ ______________________________________________________    
07-Jan-2020  Praveen   Initial Version    
22-Jan-2020  Praveen   Implemented insertion 
================================================================================*/    
BEGIN  

SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)     
 BEGIN TRY 
--Declare @BaseDecisionId uniqueidentifier  
-- select @BaseDecisionId = id  from BaseDecision 
-- where Description = @Description;
 Declare @StatusId uniqueidentifier 
 Select @StatusId = Id From [LookupStatus] 
	Where [StatusType] = 'DMDecisionStatus' And StatusName = 'To be initiated';
	
Declare @newDecisionId uniqueidentifier
set  @NewDecisionId = newid()

------------------
	--Required Tables: Decision, DecisionObjective, DecisionOutcome, BaseDecisionFunction,DecisionBusinessStage and DecisionBusinessRole
	--Notes: N/A
	--Case 1: N/A
	--Expected Result :SELECT StoredProcedureName ='SFab_InsertDecision',Message = @ErrorMsg,Success = 1/0;
	IF EXISTS (SELECT 1 FROM decision
				WHERE Description = @Description and UserId=@UserId)
	BEGIN      
     
		Set @ErrorMsg = 'Decision Name is already exists.';      
  
		SELECT StoredProcedureName ='SFab_InsertDecision','Message' =@ErrorMsg,'Success' = 0;    
		--RETURN;  
	End   
	Else
	Begin
		If exists (select 1 from BaseDecision 
					where Description=@Description --and id=@DecisionId
					)
		Begin
			
			set @ErrorMsg = 'Decision Name is already exists.';      
  
			SELECT StoredProcedureName ='SFab_InsertDecision','Message' =@ErrorMsg,'Success' = 0;    
			--RETURN;  
		END 
		ELSE
		BEGIN
		
									INSERT INTO [dbo].[Decision]
										   ([Id],
										   [Description],
										   [ResourceAssigned],
										   [UserId],           
										   [TimeTagOutlookId],
										   [DecisionTypeId],
										   [Deadline],
										   [StatusId],
										   [Score],
										   [CreatedOn],
										   [UserCreatedById]
										)
									 VALUES(
									@newdecisionId,
									@Description,
									@ResourceAssigned,
									@UserId,	
									@TimeTagOutlookId,
									@DecisionTypeId,
									@DecisionDeadLine, 	
									@StatusId,
									1,
									GETDATE(),
									@UserId)
 

									IF(ISNULL(@BusinessFunctionId,'00000000-0000-0000-0000-000000000000') != '00000000-0000-0000-0000-000000000000')
									BEGIN
										INSERT INTO DecisionFunction(DecisionId, BusinessFunctionId)
										VALUES(@NewDecisionId, @BusinessFunctionId)
									END

										IF(ISNULL(@BusinessStageId,'00000000-0000-0000-0000-000000000000') != '00000000-0000-0000-0000-000000000000')
									BEGIN
										INSERT INTO DecisionBusinessStage(DecisionId, BusinessStageId)
										VALUES(@NewDecisionId, @BusinessStageId)
									END

										IF(ISNULL(@BusinessRoleId,'00000000-0000-0000-0000-000000000000') != '00000000-0000-0000-0000-000000000000')
									BEGIN
										INSERT INTO DecisionBusinessRole(DecisionId, BusinessRoleId)
										VALUES(@NewDecisionId, @BusinessRoleId)
									END

									IF(ISNULL(@BusinessObjectivesJsonList, '') != '' AND LEN(ISNULL(@BusinessObjectivesJsonList, '')) > 1)
									BEGIN
										INSERT INTO DecisionObjective (Id,DecisionId, BusinessObjective, CreatedOn, UserCreatedById, SeqNumber)  
										SELECT NEWID(), @NewDecisionId ,businessobjective, GETDATE(), @UserId, ROW_NUMBER() OVER (order by (select 1))
										FROM OPENJSON(@BusinessObjectivesJsonList)  
										WITH (      
											BusinessObjective NVARCHAR(1024) '$.BusinessObjective'  
										);  
								  END
  									IF(ISNULL(@TargetedOutcomeJsonList, '') != '' AND LEN(ISNULL(@TargetedOutcomeJsonList, '')) > 1)
									BEGIN
										INSERT INTO DecisionOutcome (Id, DecisionId, TargetedOutcome, CreatedOn, UserCreatedById, SeqNumber) 
										SELECT NEWID(), @NewDecisionId ,TargetedOutcome, GETDATE(), @UserId, ROW_NUMBER() OVER (order by (select 1))
										FROM OPENJSON(@TargetedOutcomeJsonList)  
										WITH (      
											TargetedOutcome NVARCHAR(1024) '$.TargetedOutcome'  
										);  

									 END

									SET @DecisionId = @NewDecisionId

									Execute SFab_CopyDecisionFromBase @InDecisionId = @DecisionId, @UserId = @UserId, @CopyDecisionParametersFromBase = 1, @OutDecisionId = @DecisionId OUTPUT

									SELECT StoredProcedureName ='SFab_InsertDecision',Message =@ErrorMsg,Success = 1; 
				
		END
  end  
	

END TRY      
BEGIN CATCH      
  exec SApp_LogErrorInfo
 set @ErrorMsg = 'Error while insert a new Decision, please contact Admin for more details.';    
SELECT StoredProcedureName ='SFab_InsertDecision',Message =@ErrorMsg,Success = 0;  
END CATCH   
		
END