﻿        
        
        
CREATE procedure [dbo].[SApp_GetTrendbyID]        
(@TrendId uniqueidentifier,        
@IsDeleted bit=0)        
as        
/*================================================================================        
Procedure Name: SApp_GetTrendbyID        
Author: Gopi        
Create date: 05/02/2019        
Description: Get TrendDetails by ID        
Change History        
Date  Developer  Reason        
__________ ____________ ______________________________________________________        
05/02/2019 Gopi   Initial Version        
================================================================================*/        
BEGIN        
SET NOCOUNT ON;        
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;          

SELECT ID,TRENDNAME
FROM TREND  
WHERE (ID=@TRENDID)  
AND (ISDELETED = 0)    
      
END