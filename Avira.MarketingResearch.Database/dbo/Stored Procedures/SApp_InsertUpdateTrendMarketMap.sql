﻿


CREATE procedure [dbo].[SApp_InsertUpdateTrendMarketMap]      
(
@TrendId uniqueidentifier,   
@TrendMarketmap dbo.[udtGUIDGUIDMapValue] readonly,
@AviraUserId uniqueidentifier
)      
as      
/*================================================================================      
Procedure Name: SApp_InsertTrendMarketMap      
Author: Swami      
Create date: 04/12/2019      
Description: Insert a record into TrendMarketMap table      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
04/12/2019 Swami   Initial Version    
05/02/2019 Praveen Commented update code for existing records    
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)  
    
--Validations      
--IF exists(select 1 from [dbo].[TrendMarketMap]       
--   where TrendId = @TrendId AND MarketId = @MarketId AND IsDeleted = 0)      
--begin       
--set @ErrorMsg = 'Market with Id "'+ CONVERT (nvarchar(30), @MarketId) + '" already exists.';    
--SELECT StoredProcedureName ='SApp_InsertTrendMarketMap',Message =@ErrorMsg,Success = 0;    
--RETURN;    
--end      
--End of Validations      
BEGIN TRY         
--mark records as delete which are not in list
--update [dbo].[TrendMarketMap]
--set IsDeleted = 1,
--UserDeletedById = @AviraUserId,
--DeletedOn = GETDATE()
--from [dbo].[TrendMarketMap]
--where [TrendMarketMap].TrendId = @TrendId
--and not exists (select 1 from @TrendMarketmap map where map.id = [TrendMarketMap].MarketId)

 --Update records for change
update [dbo].[TrendMarketMap]
set Impact = map.mapid,
EndUser = map.bitvalue,
IsDeleted = 0,
ModifiedOn = GETDATE(),
UserModifiedById = @AviraUserId
from [dbo].[TrendMarketMap]
inner join @TrendMarketmap map 
on map.id = [TrendMarketMap].MarketId
where [TrendMarketMap].TrendId = @TrendId;

--Insert new records
insert into [dbo].[TrendMarketMap]([Id],      
        [TrendId],      
        [MarketId], 
		[Impact],
		[EndUser],
        [IsDeleted],      
        [UserCreatedById],      
        [CreatedOn])      
 select NEWID(),
   @TrendId,       
   map.ID as [MarketId],
   map.mapid as [Impact],     
   map.bitvalue,     
   0,      
   @AviraUserId,       
   GETDATE()
 from @TrendMarketmap map
 where not exists (select 1 from [dbo].[TrendMarketMap] map1
					where map1.TrendId = @TrendId
					and map.ID = map1.MarketId);       
     
SELECT StoredProcedureName ='SApp_InsertTrendMarketMap',Message =@ErrorMsg,Success = 1;       
    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);      
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());      
    
 set @ErrorMsg = 'Error while insert a new TrendMarketMap, please contact Admin for more details.';    
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
       
END CATCH      
      
      
end