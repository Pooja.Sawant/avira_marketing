﻿

CREATE PROCEDURE [dbo].[SFab_GetCustomerSubscrition]
(
	@CustomerId uniqueidentifier
)
	AS
/*================================================================================
Procedure Name: [SFab_GetCustomerSubscrition]
Author: Sai Krishna
Create date: 23/05/2019
Description: Get list from Subscription table by CustomerId
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
23/05/2019	Sai Krishna		Initial Version
================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

;WITH cteSegId as(
	SELECT csm.SubscriptionId 
	FROM CustomerSubscriptionMap csm
	WHERE csm.CustomerId = @CustomerId
	AND csm.IsDeleted = 0
)
SELECT ssm.Id,
	   ssm.SegmentId,
	   (
			CASE
				WHEN ssm.SegmentType = 'BoardIndustries' then 'Industries'
				WHEN ssm.SegmentType = 'Industries' then 'Industries'
				WHEN ssm.SegmentType = 'Market' then 'Market'
				ELSE 'Project'
			END
	   ) AS SegmentType
FROM SubscriptionSegmentMap ssm
Inner Join cteSegId
On ssm.SubscriptionId = cteSegId.SubscriptionId
WHERE IsDeleted = 0
ORDER BY ssm.SegmentType

END