﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.Helpers;
using System.Collections.Generic;
using System;
using System.Data;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.Masters
{
    public interface IEcoSystemRepository
    {
        EcoSystemViewModel GetById(Guid guid);
        IEnumerable<EcoSystemViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted);
        Result<SpTransactionMessage> Create(EcoSystemInsertDbViewModel viewModel);
        Result<SpTransactionMessage> Update(EcoSystemUpdateDbViewModel viewModel);
    }
}
