﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.Masters;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces.Masters;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CurrencyController : AviraBaseController
    {
        private ILoggerManager _logger;
        public readonly ICurrencyRepository _currencyRepository;
        public readonly ICurrencyService _currencyService;
        public CurrencyController(
            ICurrencyService currencyService,
            ICurrencyRepository currencyRepository,
            ILoggerManager logger)
        {
            _currencyRepository = currencyRepository;
            _currencyService = currencyService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<CurrencyViewModel>> Get(int PageStart = 0, int pageSize = 0, int SortDirection = 1, string OrdbyByColumnName = "CreatedDate", string Search = "", bool isDeletedInclude = false)
        {
            List<CurrencyViewModel> res = null;
            try
            {
                res = _currencyService.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }


        [HttpGet("{id}")]
        public ActionResult<CurrencyViewModel> Get(Guid id)
        {
            CurrencyViewModel res = null;
            try
            {
                res = _currencyService.GetById(id);
                if (res == null)
                {
                    return NotFound();
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }
    }
}