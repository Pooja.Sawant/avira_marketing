﻿
CREATE PROCEDURE [dbo].[SFab_GetDownloadHistory]      
(    
 @ModuleId UNIQUEIDENTIFIER= NULL, 
 @ProjectId UNIQUEIDENTIFIER= NULL,
 @fileName NVARCHAR(256),
 @StatusName NVARCHAR(256)= NULL,
 @AviraUserId UNIQUEIDENTIFIER  
)  
As      
  
/*================================================================================      
Procedure Name: SFab_GetDownloadHistory     
Author: Harshal Ghotkar      
Create date: 27-April-2020  
Description: To return DownloadHistory  
Change History      
Date  Developer  Reason    
__________ ____________ ______________________________________________________      
27-April-2020  Harshal G   Initial Version      
================================================================================*/      
BEGIN      
Declare @StatusId uniqueidentifier
select @StatusId = ID from LookupStatus
where StatusName = @StatusName and StatusType='ImportStatus';

SELECT DownloadHistory.Id,DownloadHistory.FileName,DownloadHistory.DownloadDate,DownloadHistory.UserId,DownloadHistory.DownloadCount,
      DownloadHistory.ProjectId 
FROM DownloadHistory DownloadHistory
INNER JOIN LookupStatus ON DownloadHistory.StatusId = LookupStatus.Id
WHERE (DownloadHistory.UserId = @AviraUserId) 
AND (@ProjectId is null OR DownloadHistory.ProjectId = @ProjectId)
AND (DownloadHistory.FileName=@fileName)
AND  (@ModuleId is null OR DownloadHistory.ModuleId = @ModuleId)
and DownloadHistory.StatusId=@StatusId
order by DownloadHistory.DownloadDate desc
END