﻿using Avira.MarketingResearch.Model;
using Avira.MarketingResearch.Repository.Helpers;
using System.Collections.Generic;
using System;
using System.Data;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.Trend
{
    public interface ITrendKeywordMapRepository
    {
        IEnumerable<TrendKeyWordMapViewModel> GetAll(Guid? guid, int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted);
        IEnumerable<TrendKeyWordMapViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted);
        Result<SpTransactionMessage> Create(TrendKeyWordMapInsertDbViewModel1 viewModel);
        Result<SpTransactionMessage> Update(TrendKeyWordMapUpdateDbViewModel viewModel);

        //IEnumerable<TrendViewModel> GetAllTrendNames();
        IEnumerable<TrendKeywordNames> GetAllTrendNames();
        IEnumerable<TrendKeyWordMapViewModel> GetById(Guid guid);
        TrendKeywordNames GetTrendNameByID(Guid guid);
    }
}
