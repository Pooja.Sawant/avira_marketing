﻿CREATE procedure [dbo].[SApp_insertUpdateApplication]
(@ApplicationID uniqueidentifier,
@Description nvarchar(256),
@ApplicationName nvarchar(256),
@IsDeleted bit,
@AviraUserId uniqueidentifier)
as
/*================================================================================
Procedure Name: SApp_DeleteApplicationbyID
Author: Sharath
Create date: 02/11/2019
Description: soft delete a record from Application table by ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
02/11/2019	Sharath			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @SegmentId uniqueidentifier;
select @SegmentId = Id from Segment
where SegmentName = 'Applications';

IF exists(select 1 from [dbo].SubSegment 
		 where ((@ApplicationID is null or [Id] !=  @ApplicationID)
		 and SegmentId = @SegmentId
		 and  SubSegmentName = @ApplicationName AND IsDeleted = 0))
begin
declare @ErrorMsg NVARCHAR(2048);
set @ErrorMsg = 'Application with Name "'+ @ApplicationName + '" already exists.';
THROW 50000,@ErrorMsg,1;
end

IF (@ApplicationID is null)
begin
--Insert new record
	set @ApplicationID = NEWID();

	insert into [dbo].SubSegment([Id],
								[SegmentId],
								[SubSegmentName],
								[Description],
								[IsDeleted],
								[UserCreatedById],
								[CreatedOn])
	values(@ApplicationID, 
			@SegmentId,
			@ApplicationName, 
			@Description, 
			0,
			@AviraUserId, 
			getdate());
end
else
begin
--update record
	update [dbo].SubSegment
	set [SubSegmentName] = @ApplicationName,
		[Description] = @Description,
		[ModifiedOn] = getdate(),
		DeletedOn = case when IsDeleted = 0 and @IsDeleted = 1 then getdate() else DeletedOn end,
		UserDeletedById = case when IsDeleted = 0 and @IsDeleted = 1 then @AviraUserId else UserDeletedById end,
		IsDeleted = isnull(@IsDeleted, IsDeleted),
		[UserModifiedById] = @AviraUserId
     where ID = @ApplicationID
	 and SegmentId = @SegmentId 
end

select @ApplicationID;
end