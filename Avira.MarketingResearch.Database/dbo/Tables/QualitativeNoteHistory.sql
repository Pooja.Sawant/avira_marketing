﻿CREATE TABLE [dbo].[QualitativeNoteHistory] (
    [Id]                  UNIQUEIDENTIFIER NOT NULL,
    [CompanyId]           UNIQUEIDENTIFIER NOT NULL,
    [AuthorRemark]        NVARCHAR (512)   NULL,
    [ApproverRemark]      NVARCHAR (512)   NULL,
    [QualitativeStatusID] UNIQUEIDENTIFIER NULL,
    [CreatedOn]           DATETIME2 (7)    NULL,
    [UserCreatedById]     UNIQUEIDENTIFIER NULL,
    [ModifiedOn]          DATETIME2 (7)    NULL,
    [UserModifiedById]    UNIQUEIDENTIFIER NULL,
    [IsDeleted]           BIT              NOT NULL,
    [DeletedOn]           DATETIME2 (7)    NULL,
    [UserDeletedById]     UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_QualitativeNoteHistory] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_QualitativeNoteHistory_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_QualitativeNoteHistory_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_QualitativeNoteHistory_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([Id])
);

