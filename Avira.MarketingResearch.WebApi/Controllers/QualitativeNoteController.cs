﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.QualitativeInfo;
using Avira.MarketingResearch.Model.ViewModels.QualitativeInfo;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QualitativeNoteController : AviraBaseController
    {
        private ILoggerManager _logger;
        public readonly IQualitativeNoteService _qualitativeService;

        public QualitativeNoteController(IQualitativeNoteService qualitativeService, ILoggerManager logger)
        {
            _qualitativeService = qualitativeService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<QualitativeNoteViewModel> Get(Guid Id,Guid ProjectId)
        {
            QualitativeNoteViewModel note = null;
            try
            {
                note = _qualitativeService.GetById(Id, ProjectId);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return note;
        }
    }
}