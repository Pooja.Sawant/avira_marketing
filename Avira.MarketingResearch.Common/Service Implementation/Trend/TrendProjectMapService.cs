﻿using Avira.MarketingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.Trend
{
    public class TrendProjectMapService : ITrendProjectMapService
    {
        private readonly ITrendProjectMapRepository _trendProjectRepository;
        private readonly ITrendNoteRepository _iTrendNoteRepository;

        public TrendProjectMapService(ITrendProjectMapRepository trendProjectMapRepository, ITrendNoteRepository iTrendNoteRepository)
        {
            _trendProjectRepository = trendProjectMapRepository;
            _iTrendNoteRepository = iTrendNoteRepository;
        }

        public IEnumerable<TrendProjectMapViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false, string id = "")
        {
            return _trendProjectRepository.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude = false, id);
        }

        public Result<SpTransactionMessage> InsertUpdate(MainTrendProjectMapInsertUpdateDbViewModel tfc, AviraUser currenUser)
        {

            var res = _trendProjectRepository.InsertUpdate(tfc);
            if (res.IsSuccess == true)
            {
                tfc.TrendNoteModel.Id = Guid.NewGuid();
                tfc.TrendNoteModel.UserCreatedById = tfc.UserCreatedById;
                tfc.TrendNoteModel.TrendId = tfc.TrendId;
                _iTrendNoteRepository.Insert(tfc.TrendNoteModel);
            }
            return res;
        }
    }
}
