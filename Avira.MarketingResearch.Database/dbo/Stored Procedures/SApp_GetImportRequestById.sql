﻿

CREATE PROCEDURE [dbo].[SApp_GetImportRequestById]
(
	@Id uniqueidentifier
)
	AS
/*================================================================================
Procedure Name: [dbo].[SApp_GetImportRequestById]
Author: Harshal
Create date: 10/16/2019
Description: Get list Import Data table by Id
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
10/16/2019	Harshal			Initial Version
================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT 
	   [ImportData].[Id]
	  ,[TemplateName]
      ,[ImportFileName]
      ,[ImportFilePath]
      ,[ImportStatusId]
	  ,[StatusName]
      ,[ImportException]
      ,[ImportExceptionFilePath]
      ,[ImportedOn]
      ,[UserImportedById]
      ,[ModifiedOn]
      ,[UserModifiedById]
FROM [dbo].[ImportData]
INNER JOIN LookupStatus ON LookupStatus.Id=ImportData.ImportStatusId
where  ([ImportData].[Id] = @Id )
--Order By Case When ModifiedOn is null then ImportedOn  else ModifiedOn  end desc;
END