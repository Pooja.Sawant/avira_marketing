﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public static class UserQueries
    {
        public static readonly string StoreProcInsert = "SApp_insertAviraUser";

        public static readonly string StoreProcGetByEmail = "SApp_GetUserDetailsByEmailID";

        public static readonly string StoreProcGetByUserId = "SApp_GetUserDetailsByUserID";

        public static readonly string StoreProcGetUserLogin = "SSec_GetUserLogin";

        public static string StoreProcSelectAllParent = "SApp_GetUserRoles";

        public static string StoreProcSelectAllLocation = "SApp_GetLocation";

     

        public static readonly string StoreProcUpdateUserLogin = "SSec_UpdateUserLogin";

        public static readonly string StoreProcInsertUserRole = "SApp_insertAviraUserRoleMap";

        public static readonly string StoreProcGetUserRoles = "SApp_GetUserRolesByAviraUserId";

        //public static readonly string StoreProcGetPermissionsByUserRoleId = "SApp_GetPermissionsByUserRoleId1";
        public static readonly string StoreProcGetPermissionsByUserRoleId = "SApp_GetPermissionsByUserRoleId";

        public static readonly string StoreProcGetFormsByUserRoleId = "SApp_GetFormsByUserRoleId";

        public static readonly string StoreProcGetMenusByUserRoleId = "SApp_GetMenusByUserRoleId";

        public static readonly string StoreProcGetAllUsersByRole = "SApp_GetAllUsersByRole";

        //public static readonly string StoreProcGetPermissionsByUserRoleId = "SApp_GetPermissionsByUserRoleId";

        public static readonly string StoreProcGetAllUsers = "SApp_UserGridWithFilters";
    }
}
