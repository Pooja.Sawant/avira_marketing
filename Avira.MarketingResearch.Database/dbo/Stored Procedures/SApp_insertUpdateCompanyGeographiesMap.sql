﻿

CREATE procedure [dbo].[SApp_insertUpdateCompanyGeographiesMap]      
(
	@GeographiesListmap [dbo].[udtUniqueIdentifier] readonly,
	@CompanyId uniqueidentifier,
	@UserCreatedById uniqueidentifier
)      
as      
/*================================================================================      
Procedure Name: [SApp_insertUpdateCompanyGeographiesMap]      
Author: Sai Krishna      
Create date: 08/05/2019      
Description: Insert a record into CompanyRegionMap table      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
08/05/2019 Sai Krishna   Initial Version    
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)  
    
      
BEGIN TRY         

Delete from [dbo].[CompanyRegionMap]
Where [CompanyRegionMap].[CompanyId] = @CompanyId

--Insert new records
insert into [dbo].[CompanyRegionMap](
		[Id],      
        [RegionId],
		[CompanyId],
        [IsDeleted],      
        [UserCreatedById],      
        [CreatedOn])      
 select NEWID(),
   map.ID,
   @CompanyId,    
   0,      
   @UserCreatedById,       
   GETDATE()
 from @GeographiesListmap map
 where not exists (select 1 from [dbo].[CompanyRegionMap] map1
					where map1.CompanyId = @CompanyId
					and map.Id = map1.Id);       
     
SELECT StoredProcedureName ='SApp_insertUpdateCompanyShareHolidingMap',Message =@ErrorMsg,Success = 1;       
    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);      
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());      
    
 set @ErrorMsg = 'Error while insert a new TrendMarketMap, please contact Admin for more details.';    
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
       
END CATCH      
      
      
end