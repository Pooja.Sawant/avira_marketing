﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.Utils;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Avira.MarketingResearch.Web.Controllers
{
    [Authorize]
    [NoCache]
    public class CompanyAnalystViewController : BaseController
    {
        public readonly IServiceRepository _serviceRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;
        string contentRootPath = string.Empty;
        private ILoggerManager _logger;

        public CompanyAnalystViewController(IServiceRepository serviceRepository, ILoggerManager logger, IHostingEnvironment hostingEnvironment, IConfiguration configuration)
        {
            _hostingEnvironment = hostingEnvironment;
            _serviceRepository = serviceRepository;
            _logger = logger;
            _configuration = configuration;
            contentRootPath = _hostingEnvironment.ContentRootPath;
        }

        public IActionResult Index(string companyId, string projectId)
        { 
            SetRoleList();
            MainCompanyAnalystViewModel mainCompanyAnalystViewModel = new MainCompanyAnalystViewModel();
            try
            {
                mainCompanyAnalystViewModel.CompanyId = new Guid(companyId);
                mainCompanyAnalystViewModel.ProjectId = new Guid(projectId);
                var ContollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage CompanyTrendsMapResponse = _serviceRepository.GetCompanyDetailsByGuIdsResponse(ContollerName, companyId, projectId);
                CompanyAnalystViewModel companyAnalystViewModel = CompanyTrendsMapResponse.Content.ReadAsAsync<CompanyAnalystViewModel>().Result;

                mainCompanyAnalystViewModel.CompanyAnalystViewModel = companyAnalystViewModel;
                HttpResponseMessage CompanyNoteResponse = _serviceRepository.GetCompanyNoteByIdResponse("CompanyNote", companyId, "CompanyAnalystView", projectId);
                CompanyNoteViewModel companyNoteViewModel = CompanyNoteResponse.Content.ReadAsAsync<CompanyNoteViewModel>().Result;
                mainCompanyAnalystViewModel.CompanyNoteViewModel = companyNoteViewModel ?? new CompanyNoteViewModel();
                HttpResponseMessage CompanyResponse = _serviceRepository.GetByIdResponse("Company", companyId);
                CompanyViewModel companyViewModel = CompanyResponse.Content.ReadAsAsync<CompanyViewModel>().Result;

                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, mainCompanyAnalystViewModel.ProjectId);
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                    ViewBag.StatusName = mainCompanyAnalystViewModel.CompanyNoteViewModel.StatusName;
                }
                ViewBag.CompanyName = companyViewModel.CompanyName;
                ViewBag.CompanyId = mainCompanyAnalystViewModel.CompanyId;
                ViewBag.Message = string.Empty;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(mainCompanyAnalystViewModel);
        }

        private void SetRoleList()
        {
            try
            {
                var roles = HttpContext.Session.GetString("userRoles");
                var userRoles = JsonConvert.DeserializeObject<List<RoleViewModel>>(roles);
                List<string> RoleListItems = new List<string>();
                foreach (var item in userRoles)
                    RoleListItems.Add(item.UserRoleName);
                ViewBag.UserRoleList = RoleListItems;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        /// <summary>
        /// Method to validate Form
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private bool validate(MainCompanyAnalystViewModel model)
        {
            if (model.CompanyAnalystViewModel.AnalystView == null)
            {
                ViewBag.Message = "Please Fill Analyst View for the Company mapping.";
                return false;
            }
            
            if (model.CompanyNoteViewModel.AuthorRemark == null)
            {
                ViewBag.Message = "Please Fill Author Remark.";
                return false;
            }
            else
            {
                ViewBag.Message = string.Empty;
                return true;
            } 
        }

        [HttpPost]
        public IActionResult Index(MainCompanyAnalystViewModel model, string actionName)
        {
            SetRoleList();
            ViewBag.Message = string.Empty;
            bool bResult = false;
            try
            {
                model.CompanyNoteViewModel.CompanyType = "CompanyAnalystView";
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                if (actionName.Equals("Save"))
                {
                    bResult = validate(model);
                    if (bResult)
                    {
                        model.CompanyNoteViewModel.CompanyStatusName = "Draft";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            return RedirectToAction("Index", new { companyId = model.CompanyId.ToString(), projectId = model.ProjectId.ToString() });
                        }
                    } 
                }
                else if (actionName.Equals("Submitted"))
                {
                    bResult = validate(model);
                    if (bResult)
                    {
                        model.CompanyNoteViewModel.CompanyStatusName = "Submitted";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            return RedirectToAction("Index", new { companyId = model.CompanyId.ToString(), projectId = model.ProjectId.ToString() });
                        }
                    }
                }
                else if (actionName.Equals("Approve"))
                {
                    bResult = validate(model);
                    if (bResult)
                    {
                        model.CompanyNoteViewModel.CompanyStatusName = "Approved";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            return RedirectToAction("Index", new { companyId = model.CompanyId.ToString(), projectId = model.ProjectId.ToString() });
                        }
                    }
                }
                else if (actionName.Equals("Reject"))
                {
                    bResult = validate(model);
                    if (bResult)
                    {
                        model.CompanyNoteViewModel.CompanyStatusName = "Rejected";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            return RedirectToAction("Index", new { companyId = model.CompanyId.ToString(), projectId = model.ProjectId.ToString() });
                        }
                    }
                }
                else if (actionName.Equals("Next"))
                    return RedirectToAction("Index", "CompanyTracker", new { Id = model.ProjectId.ToString() });
                else if (actionName.Equals("Previous"))
                    return RedirectToAction("Index", "CompanyOtherInfo", new { companyId = model.CompanyId.ToString(), projectId = model.ProjectId.ToString() });
                else if (actionName.Equals("Cancel"))
                    return RedirectToAction("Index", "CompanyTracker");

                HttpResponseMessage CompanyNoteResponse = _serviceRepository.GetCompanyNoteByIdResponse("CompanyNote", model.CompanyId.ToString(), "CompanyAnalystView", model.ProjectId.ToString());
                CompanyNoteViewModel companyNoteViewModel = CompanyNoteResponse.Content.ReadAsAsync<CompanyNoteViewModel>().Result;
                companyNoteViewModel = companyNoteViewModel ?? new CompanyNoteViewModel();
                HttpResponseMessage CompanyResponse = _serviceRepository.GetByIdResponse("Company", model.CompanyId.ToString());
                CompanyViewModel companyViewModel = CompanyResponse.Content.ReadAsAsync<CompanyViewModel>().Result;
                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, model.ProjectId);
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                    ViewBag.StatusName = companyNoteViewModel.StatusName??string.Empty;
                }
                ViewBag.CompanyName = companyViewModel.CompanyName;
                ViewBag.CompanyId = model.CompanyId;
                ViewBag.ProjectId = model.ProjectId;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(model);
        }
    }
}