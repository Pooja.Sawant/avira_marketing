﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.CompanyProfile
{
    public class RevenueProfitAnalysisRepository : IRevenueProfitAnalysisRepository
    {
        protected IDbHelper _dbHelper;
        public RevenueProfitAnalysisRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }
        public IEnumerable<CurrencyMapViewModel> GetAllCurrencies()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<CurrencyMapViewModel>(RevenueProfitAnalysisQueries.StoreProcSelectAllCurrencies
                            , null
                            , null, true, null, CommandType.StoredProcedure);
        }

        public IEnumerable<RegionMapViewModel> GetAllRegions()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<RegionMapViewModel>(RevenueProfitAnalysisQueries.StoreProcSelectAllRegions
                            , null
                            , null, true, null, CommandType.StoredProcedure);
        }

        public IEnumerable<ValueConversionViewModel> GetAllValueConversion()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<ValueConversionViewModel>(RevenueProfitAnalysisQueries.StoreProcSelectAllUnits
                            , null
                            , null, true, null, CommandType.StoredProcedure);
        }

        public Result<SpTransactionMessage> InsertUpdate(MainRevenueProfitAnalysisInsertUpdateDbViewModel revenueProfitAnalysisInsertUpdateDbViewModel)
        {
            //--CompanyRevenue Table--//
            var spTransactionMessage = _dbHelper.ExecuteQuery(RevenueProfitAnalysisQueries.StoreProcInsertUpdateCompanyRevenue,
                new
                {
                    @CompanyId = revenueProfitAnalysisInsertUpdateDbViewModel.CompanyRevenueInsertUpdate.CompanyId,
                    @CurrencyId = revenueProfitAnalysisInsertUpdateDbViewModel.CompanyRevenueInsertUpdate.CurrencyId,
                    @UnitId = revenueProfitAnalysisInsertUpdateDbViewModel.CompanyRevenueInsertUpdate.UnitId,
                    @Year = revenueProfitAnalysisInsertUpdateDbViewModel.CompanyRevenueInsertUpdate.Year,
                    @TotalRevenue = revenueProfitAnalysisInsertUpdateDbViewModel.CompanyRevenueInsertUpdate.TotalRevenue,
                    @UserCreatedById = revenueProfitAnalysisInsertUpdateDbViewModel.UserCreatedById,
                    @ID = revenueProfitAnalysisInsertUpdateDbViewModel.CompanyRevenueInsertUpdate.Id
                }
                , CommandType.StoredProcedure);

            var obj = spTransactionMessage;
            Guid companyRevenueId = new Guid(obj.Value.Message);

            if (spTransactionMessage.IsSuccess == false)
            {
                return spTransactionMessage;
            }

            //--CompanyRevenueGeographicSplitMap Table--//
            DataTable geographicSplitTable = new DataTable();
            geographicSplitTable.Columns.Add("Id", typeof(Guid));
            geographicSplitTable.Columns.Add("RegionId", typeof(Guid));
            geographicSplitTable.Columns.Add("RegionValue", typeof(decimal));

            if (revenueProfitAnalysisInsertUpdateDbViewModel.CompanyGeographicSplitInsertUpdate != null & revenueProfitAnalysisInsertUpdateDbViewModel.CompanyGeographicSplitInsertUpdate.Count > 0)
            {
                foreach (var item in revenueProfitAnalysisInsertUpdateDbViewModel.CompanyGeographicSplitInsertUpdate)
                {
                    geographicSplitTable.Rows.Add(new Object[] { item.Id, item.RegionId, item.RegionValue });
                }
            }


            var spTransactionMessageGeograhic = _dbHelper.ExecuteQuery(RevenueProfitAnalysisQueries.StoreProcInsertUpdateCompanyRevenueGeographic,
               new
               {
                   @CompanyId = revenueProfitAnalysisInsertUpdateDbViewModel.CompanyRevenueInsertUpdate.CompanyId,
                   @CompanyRevenueId = companyRevenueId,
                   @Year = revenueProfitAnalysisInsertUpdateDbViewModel.CompanyGeographicSplitInsertUpdate.First().Year,
                   @RegionList = geographicSplitTable.AsTableValuedParameter("dbo.udtGUIDGeographicSplitMultiValue"),
                   @UserCreatedById = revenueProfitAnalysisInsertUpdateDbViewModel.UserCreatedById
               }
                    , CommandType.StoredProcedure);

            if (spTransactionMessageGeograhic.IsSuccess == false)
            {
                return spTransactionMessageGeograhic;
            }

            //--CompanyRevenueSegmentationSplitMap Table--//
            foreach (var itemSegmentation in revenueProfitAnalysisInsertUpdateDbViewModel.CompanySegmentationSplitInsertUpdate)
            {
                DataTable segmentationSplitTable = new DataTable();
                segmentationSplitTable.Columns.Add("Id", typeof(Guid));
                segmentationSplitTable.Columns.Add("Segmentation", typeof(string));
                segmentationSplitTable.Columns.Add("SegmentValue", typeof(decimal));

                if (revenueProfitAnalysisInsertUpdateDbViewModel.CompanySegmentationSplitInsertUpdate != null & revenueProfitAnalysisInsertUpdateDbViewModel.CompanySegmentationSplitInsertUpdate.Count > 0)
                {
                    //foreach (var item in revenueProfitAnalysisInsertUpdateDbViewModel.CompanySegmentationSplitInsertUpdate.First().Segment)
                    foreach (var item in itemSegmentation.Segment)
                    {
                        segmentationSplitTable.Rows.Add(new Object[] { item.Id, item.Segmentation, item.SegmentValue });
                    }
                }

                var spTransactionMessageSegmentation = _dbHelper.ExecuteQuery(RevenueProfitAnalysisQueries.StoreProcInsertUpdateCompanyRevenueSegmentation,
              new
              {
                  @CompanyId = revenueProfitAnalysisInsertUpdateDbViewModel.CompanyRevenueInsertUpdate.CompanyId,
                  @CompanyRevenueId = companyRevenueId,
                  @Split = itemSegmentation.Split,
                  @Year = revenueProfitAnalysisInsertUpdateDbViewModel.CompanySegmentationSplitInsertUpdate.First().Year,
                  @SegmentationList = segmentationSplitTable.AsTableValuedParameter("dbo.udtGUIDSegmentationSplitMultiValue"),
                  @UserCreatedById = revenueProfitAnalysisInsertUpdateDbViewModel.UserCreatedById
              }
                   , CommandType.StoredProcedure);

                if (spTransactionMessageSegmentation.IsSuccess == false)
                {
                    return spTransactionMessageSegmentation;
                }

            }
            

            //--CompanyProfit Table--//
            var spProfitTransactionMessage = _dbHelper.ExecuteQuery(RevenueProfitAnalysisQueries.StoreProcInsertUpdateCompanyProfit,
                new
                {
                    @CompanyId = revenueProfitAnalysisInsertUpdateDbViewModel.CompanyProfitInsertUpdate.CompanyId,
                    @CurrencyId = revenueProfitAnalysisInsertUpdateDbViewModel.CompanyProfitInsertUpdate.CurrencyId,
                    @UnitId = revenueProfitAnalysisInsertUpdateDbViewModel.CompanyProfitInsertUpdate.UnitId,
                    @Year = revenueProfitAnalysisInsertUpdateDbViewModel.CompanyProfitInsertUpdate.Year,
                    @TotalProfit = revenueProfitAnalysisInsertUpdateDbViewModel.CompanyProfitInsertUpdate.TotalProfit,
                    @UserCreatedById = revenueProfitAnalysisInsertUpdateDbViewModel.UserCreatedById,
                    @ID = revenueProfitAnalysisInsertUpdateDbViewModel.CompanyProfitInsertUpdate.Id
                }
                , CommandType.StoredProcedure);

            var objCompany = spProfitTransactionMessage;
            Guid companyProfitId = new Guid(objCompany.Value.Message);

            if (spProfitTransactionMessage.IsSuccess == false)
            {
                return spProfitTransactionMessage;
            }

            //--CompanyProfitGeographicSplitMap Table--//
            DataTable profitgeographicSplitTable = new DataTable();
            profitgeographicSplitTable.Columns.Add("Id", typeof(Guid));
            profitgeographicSplitTable.Columns.Add("RegionId", typeof(Guid));
            profitgeographicSplitTable.Columns.Add("RegionValue", typeof(decimal));

            if (revenueProfitAnalysisInsertUpdateDbViewModel.CompanyProfitGeographicSplitInsertUpdate != null & revenueProfitAnalysisInsertUpdateDbViewModel.CompanyProfitGeographicSplitInsertUpdate.Count > 0)
            {
                foreach (var item in revenueProfitAnalysisInsertUpdateDbViewModel.CompanyProfitGeographicSplitInsertUpdate)
                {
                    profitgeographicSplitTable.Rows.Add(new Object[] { item.Id, item.RegionId, item.RegionValue });
                }
            }


            var spTransactionMessageProfitGeograhic = _dbHelper.ExecuteQuery(RevenueProfitAnalysisQueries.StoreProcInsertUpdateCompanyProfitGeographic,
               new
               {
                   @CompanyId = revenueProfitAnalysisInsertUpdateDbViewModel.CompanyProfitInsertUpdate.CompanyId,
                   @CompanyProfitId = companyProfitId,
                   @Year = revenueProfitAnalysisInsertUpdateDbViewModel.CompanyProfitGeographicSplitInsertUpdate.First().Year,
                   @RegionList = profitgeographicSplitTable.AsTableValuedParameter("dbo.udtGUIDGeographicSplitMultiValue"),
                   @UserCreatedById = revenueProfitAnalysisInsertUpdateDbViewModel.UserCreatedById
               }
                    , CommandType.StoredProcedure);

            if (spTransactionMessageProfitGeograhic.IsSuccess == false)
            {
                return spTransactionMessageProfitGeograhic;
            }


            //--CompanyProjectSegmentationSplitMap Table--//
            foreach (var itemSegmentation in revenueProfitAnalysisInsertUpdateDbViewModel.CompanyProfitSegmentationSplitInsertUpdate)
            {
                DataTable profitsegmentationSplitTable = new DataTable();
                profitsegmentationSplitTable.Columns.Add("Id", typeof(Guid));
                profitsegmentationSplitTable.Columns.Add("Segmentation", typeof(string));
                profitsegmentationSplitTable.Columns.Add("SegmentValue", typeof(decimal));

                if (revenueProfitAnalysisInsertUpdateDbViewModel.CompanyProfitSegmentationSplitInsertUpdate != null & revenueProfitAnalysisInsertUpdateDbViewModel.CompanyProfitSegmentationSplitInsertUpdate.Count > 0)
                {
                    //foreach (var item in revenueProfitAnalysisInsertUpdateDbViewModel.CompanySegmentationSplitInsertUpdate.First().Segment)
                    foreach (var item in itemSegmentation.Segment)
                    {
                        profitsegmentationSplitTable.Rows.Add(new Object[] { item.Id, item.Segmentation, item.SegmentValue });
                    }
                }

                var spTransactionMessageProfitSegmentation = _dbHelper.ExecuteQuery(RevenueProfitAnalysisQueries.StoreProcInsertUpdateCompanyProfitSegmentation,
              new
              {
                  @CompanyId = revenueProfitAnalysisInsertUpdateDbViewModel.CompanyProfitInsertUpdate.CompanyId,
                  @CompanyProfitId = companyProfitId,
                  @Split = itemSegmentation.Split,
                  @Year = revenueProfitAnalysisInsertUpdateDbViewModel.CompanyProfitSegmentationSplitInsertUpdate.First().Year,
                  @SegmentationList = profitsegmentationSplitTable.AsTableValuedParameter("dbo.udtGUIDSegmentationSplitMultiValue"),
                  @UserCreatedById = revenueProfitAnalysisInsertUpdateDbViewModel.UserCreatedById
              }
                   , CommandType.StoredProcedure);

                if (spTransactionMessageProfitSegmentation.IsSuccess == false)
                {
                    return spTransactionMessageProfitSegmentation;
                }

            }

            return spTransactionMessage;
        }
    }
}
