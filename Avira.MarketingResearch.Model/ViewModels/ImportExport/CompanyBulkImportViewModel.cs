﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.ImportExport
{
    public class CompanyBulkImportViewModel
    {
        public string TemplateName { get; set; }
        public string ImportFileName { get; set; }
        public string JsonCompanyBasic { get; set; }
        public string JsonCompanyFinancial { get; set; }
        public Guid UserCreatedById { get; set; }
        public List<CompanyBulkCompanyDetailsViewModel> CompanyBasicInfoList { get; set; }
        public List<CompanyBulkFinancialImportModel> CompanyFinancialList { get; set; }
    }

    public class CompanyBulkCompanyDetailsViewModel
    {
        public int RowNo { get; set; }
        public string CompanyName { get; set; }
        public string Headquarter { get; set; }
        public string Nature { get; set; }
        public string KeyExecutives { get; set; }
        public string Shareholders { get; set; }
        public string Website { get; set; }
        public string FoundedYear { get; set; }
        public string NumberofEmployees { get; set; }
        public string AsonDate { get; set; }
        public string PhoneNumber1 { get; set; }
        public string Decription { get; set; }
        public string ErrorNotes { get; set; }

    }


    public class CompanyBulkFinancialImportModel
    {
        public int RowNo { get; set; }
        public string CompanyName { get; set; }
        public string Currency { get; set; }
        public string Units { get; set; }
        public string FinanicialYear { get; set; }
        public string TotalAssets { get; set; }
        public string TotalCurrentAssets { get; set; }
        public string CashAndCashEquivalents { get; set; }
        public string ShortTermInvestments { get; set; }
        public string TotalEquity { get; set; }
        public string TotalLiabilities { get; set; }
        public string TotalCurrentLiabilities { get; set; }
        public string NetDebt { get; set; }
        public string Revenue { get; set; }
        public string EBITDA { get; set; }
        public string EBIT { get; set; }
        public string NetIncome { get; set; }
        public string CompanyListedOn { get; set; }
        public string CompanyId { get; set; }
        public string CurrencyId { get; set; }
        public string ValueConversionId { get; set; }
        public string ErrorNotes { get; set; }

    }
}
