﻿using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.ImportExport
{
    public class StrategyInstanceImportExportRepository : IStrategyInstanceImportExportRepository
    {
        protected IDbHelper _dbHelper;

        public StrategyInstanceImportExportRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public IEnumerable<ImportCommonResponseModel> Create(StrategyIntanceImportModel viewModel)
        {

            var spTransactionMessage = _dbHelper.ExecuteQuery(ImportExportQueries.storeProcInsertStrategyInstance, new
            {
                @TemplateName = viewModel.TemplateName,
                @ImportFileName = viewModel.ImportFileName,

                @JsonStrategy = viewModel.JsonStrategyList,
                @UserCreatedById = viewModel.UserCreatedById
            }, CommandType.StoredProcedure);

            IEnumerable<ImportCommonResponseModel> spTransactionMessage1 = new List<ImportCommonResponseModel>();
            if (spTransactionMessage.IsSuccess == true)
            {
                //var spTransactionMessage1 = _dbHelper.ExecuteQuery(ImportExportQueries.StoreProcUpdateMarketSizing1, null, CommandType.StoredProcedure);
                var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
                var spTransactionMain = dapper.Query<ImportCommonResponseModel>(ImportExportQueries.storeProcUpdateStrategyInstance
                            , null
                            , null, true, null, CommandType.StoredProcedure);

                if (spTransactionMessage1 != null)
                {
                    spTransactionMessage1 = spTransactionMain;
                }
            }
            return spTransactionMessage1;
        }
    }
}
