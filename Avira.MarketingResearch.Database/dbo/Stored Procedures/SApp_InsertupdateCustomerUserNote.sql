﻿
CREATE PROCEDURE [dbo].[SApp_InsertupdateCustomerUserNote]   
(	@Id UNIQUEIDENTIFIER,
	@UserId UNIQUEIDENTIFIER,
	@ProjectId UNIQUEIDENTIFIER,
	@Module NVARCHAR(512),
	@CompanyId UNIQUEIDENTIFIER = NULL,
	@MarketID UNIQUEIDENTIFIER = NULL,
	@TrendID UNIQUEIDENTIFIER = NULL,
	@Notes NVARCHAR(1000),
	@LoginUserId UNIQUEIDENTIFIER,
	@IsDeleted BIT = 0
)
AS   
 /*================================================================================    
Procedure Name: SApp_InsertupdateCustomerUserNote    
Author: Sharath    
Create date: 07/12/2019    
Description: Get Customer note for a project and module by User id    
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
07/12/2019 Sharath   Initial Version    
================================================================================*/   
 SET NOCOUNT ON;  
 declare @ErrorMsg varchar(500);
BEGIN TRY       
	if (@Id is null) or not exists(select 1 from [CustomerUserNotes] where Id = @Id)
	begin
	--if @Id is null set @Id = NEWID();

	INSERT INTO [dbo].[CustomerUserNotes]
           ([Id],
           [UserId],
           [ProjectId],
           [Module],
           [CompanyId],
           [MarketID],
           [TrendID],
           [Notes],
           [CreatedOn],
           [UserCreatedById],
           [IsDeleted]
           )
     VALUES
           (@Id,
           @UserId,
           @ProjectId,
           @Module,
           @CompanyId,
           @MarketID,
           @TrendID,
           @Notes,
           GETDATE(),
           @LoginUserId,
           0)

	end
	else
	begin
	if (@IsDeleted = 0 or @IsDeleted  is null)
	begin
	UPDATE [dbo].[CustomerUserNotes]
	SET [UserId] = @UserId,
      [ProjectId] = @ProjectId,
      [Module] = @Module,
      [CompanyId] = @CompanyId,
      [MarketID] = @MarketID,
      [TrendID] = @TrendID,
      [Notes] = @Notes,
      [ModifiedOn] = GETDATE(),
      [UserModifiedById] = @LoginUserId,
      [IsDeleted] = 0
	 WHERE id = @Id
	 end
	 else
	 begin
	 UPDATE [dbo].[CustomerUserNotes]
	SET [IsDeleted] = 1,
	[ModifiedOn] = GETDATE(),
    [UserModifiedById] = @LoginUserId,
	[DeletedOn] = GETDATE(),
	[UserDeletedById] = @LoginUserId
	 WHERE id = @Id
	 end
	end

  SELECT StoredProcedureName ='SApp_InsertupdateCustomerUserNote',Message =@ErrorMsg,Success = 1;    
END TRY   
BEGIN CATCH        
    -- Execute the error retrieval routine.        
 DECLARE @ErrorNumber int;        
 DECLARE @ErrorSeverity int;        
 DECLARE @ErrorProcedure varchar(50);        
 DECLARE @ErrorLine int;        
 DECLARE @ErrorMessage varchar(500);        
        
  SELECT @ErrorNumber = ERROR_NUMBER(),        
        @ErrorSeverity = ERROR_SEVERITY(),        
        @ErrorProcedure = ERROR_PROCEDURE(),        
        @ErrorLine = ERROR_LINE(),        
        @ErrorMessage = ERROR_MESSAGE()        
        
 insert into dbo.Errorlog(ErrorNumber,        
       ErrorSeverity,        
       ErrorProcedure,        
       ErrorLine,        
       ErrorMessage,        
       ErrorDate)        
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());        
      
set @ErrorMessage = 'Error while creating customer notes, please contact Admin for more details.';      
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine      
    ,Message =@ErrorMessage,Success = 0;         
         
END CATCH