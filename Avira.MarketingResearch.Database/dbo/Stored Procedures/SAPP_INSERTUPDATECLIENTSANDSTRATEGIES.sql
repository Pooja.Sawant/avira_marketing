﻿
CREATE PROCEDURE [DBO].[SAPP_INSERTUPDATECLIENTSANDSTRATEGIES]        
(  
@ID UNIQUEIDENTIFIER,  
@CompanyId uniqueidentifier,
@Date datetime2(7),
@Approach uniqueidentifier,
@Strategy nvarchar(256),
@Category uniqueidentifier,
@Importance uniqueidentifier,
@Impact uniqueidentifier,
@Remark nvarchar(512),
@ClientName nvarchar(256),
@ClientIndustry uniqueidentifier,
@ClientRemark nvarchar(512),
@CreatedOn datetime2(7),
@UserCreatedById uniqueidentifier,
@ModifiedOn datetime2(7),
@UserModifiedById uniqueidentifier,
@IsDeleted bit,
@DeletedOn datetime2(7),
@UserDeletedById uniqueidentifier 
)        
AS 
/*=============================================================================        
PROCEDURE NAME: SAPP_INSERTUPDATECLIENTSANDSTRATEGIES        
AUTHOR: LAXMIKANT M.      
CREATE DATE: 05/06/2019        
DESCRIPTION: INSERT A RECORD IN CLIENTSTRATEGY TABLE      
CHANGE HISTORY        
DATE  DEVELOPER  REASON        
__________ ____________ ______________________________________________________        
05/06/2019   LAXMIKANT   INITIAL VERSION      
================================================================================*/      
BEGIN        
SET NOCOUNT ON;        
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;        
DECLARE @ERRORMSG NVARCHAR(2048)
BEGIN   
	BEGIN TRY  
		IF exists(select 1 from  [dbo].[ClientsAndStrategies]   where ID = @ID AND CompanyId = @CompanyId AND IsDeleted = 0)   
			BEGIN
				UPDATE [dbo].[ClientsAndStrategies]
			   SET  
				  CompanyId  = @CompanyId,
				  Date  =  @Date,
				  Approach  = @Approach,
				  Strategy  = @Strategy,
				  Category  = @Category,
				  Importance  = @Importance,
				  Impact  = @Impact, 
				  Remark  = @Remark,
				  ClientName  = @ClientName,
				  ClientIndustry  = @ClientIndustry,
				  ClientRemark  = @ClientRemark,
				  CreatedOn  = @CreatedOn,
				  UserCreatedById  = @UserCreatedById,
				  ModifiedOn  = @ModifiedOn,
				  UserModifiedById  = @UserModifiedById,
				  IsDeleted  = @IsDeleted,
				  DeletedOn  = @DeletedOn,
				  UserDeletedById  = @UserDeletedById
				WHERE Id  = @Id
			END
		ELSE
			BEGIN
				INSERT INTO [DBO].[CLIENTSANDSTRATEGIES]
				   ([ID]
				   ,[COMPANYID]
				   ,[DATE]
				   ,[APPROACH]
				   ,[STRATEGY]
				   ,[CATEGORY]
				   ,[IMPORTANCE]
				   ,[IMPACT]
				   ,[REMARK]
				   ,[CLIENTNAME]
				   ,[CLIENTINDUSTRY]
				   ,[CLIENTREMARK]
				   ,[CREATEDON]
				   ,[USERCREATEDBYID]
				   ,[MODIFIEDON]
				   ,[USERMODIFIEDBYID]
				   ,[ISDELETED]
				   ,[DELETEDON]
				   ,[USERDELETEDBYID])
			 VALUES
				   (
					@ID,
					@CompanyId,
					@DATE,
					@APPROACH, 
				   @STRATEGY, 
				   @CATEGORY,  
				   @IMPORTANCE, 
				   @IMPACT,  
				   @REMARK, 
				   @CLIENTNAME,  
				   @CLIENTINDUSTRY, 
				   @CLIENTREMARK, 
				   @CREATEDON, 
				   @USERCREATEDBYID,  
				   @MODIFIEDON,  
				   @USERMODIFIEDBYID, 
				   @ISDELETED,  
				   @DELETEDON,  
				   @USERDELETEDBYID
				   )
			END 
		SELECT STOREDPROCEDURENAME ='SAPP_INSERTUPDATECLIENTSANDSTRATEGIES',MESSAGE =@ERRORMSG,SUCCESS = 1;      
	END TRY
	BEGIN CATCH
		-- EXECUTE THE ERROR RETRIEVAL ROUTINE.        
	 DECLARE @ERRORNUMBER INT;        
	 DECLARE @ERRORSEVERITY INT;        
	 DECLARE @ERRORPROCEDURE VARCHAR;        
	 DECLARE @ERRORLINE INT;        
	 DECLARE @ERRORMESSAGE VARCHAR;        
        
	  SELECT @ERRORNUMBER = ERROR_NUMBER(),        
			@ERRORSEVERITY = ERROR_SEVERITY(),        
			@ERRORPROCEDURE = ERROR_PROCEDURE(),        
			@ERRORLINE = ERROR_LINE(),        
			@ERRORMESSAGE = ERROR_MESSAGE()        
        
	 INSERT INTO DBO.ERRORLOG(ERRORNUMBER,        
		   ERRORSEVERITY,        
		   ERRORPROCEDURE,        
		   ERRORLINE,        
		   ERRORMESSAGE,        
		   ERRORDATE)        
		   VALUES(@ERRORNUMBER,@ERRORSEVERITY,@ERRORPROCEDURE,@ERRORLINE,@ERRORMESSAGE,GETDATE());        
      
	 SET @ERRORMSG = 'ERROR WHILE INSERT A NEW TRENDINDUSTRYMAP, PLEASE CONTACT ADMIN FOR MORE DETAILS.';      
	 SELECT NUMBER = @ERRORNUMBER, SEVERITY =@ERRORSEVERITY,STOREDPROCEDURENAME =@ERRORPROCEDURE,LINENUMBER= @ERRORLINE      
	 ,MESSAGE =@ERRORMSG,SUCCESS = 0;  
	END CATCH   
END  
END