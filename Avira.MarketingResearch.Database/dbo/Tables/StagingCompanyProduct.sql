﻿CREATE TABLE [dbo].[StagingCompanyProduct] (
    [Id]                 UNIQUEIDENTIFIER NOT NULL,
    [ImportId]           UNIQUEIDENTIFIER NULL,
    [ProductServiceName] NVARCHAR (255)   NULL,
    [LaunchDate]         DATETIME         NULL,
    [Market]             NVARCHAR (255)   NULL,
    [ProductCategory]    NVARCHAR (255)   NULL,
    [Segment]            NVARCHAR (255)   NULL,
    [Revenue]            DECIMAL (19, 4)  NULL,
    [Units]              NVARCHAR (255)   NULL,
    [Currency]           NVARCHAR (3)     NULL,
    [TargetedGeogrpahy]  NVARCHAR (512)   NULL,
    [Patents]            NVARCHAR (512)   NULL,
    [DecriptionRemarks]  NVARCHAR (512)   NULL,
    [Status]             NVARCHAR (255)   NULL,
    [SegmentId]          UNIQUEIDENTIFIER NULL,
    [ValueConversionId]  UNIQUEIDENTIFIER NULL,
    [CurrencyId]         UNIQUEIDENTIFIER NULL,
    [ProductCategoryId]  UNIQUEIDENTIFIER NULL,
    [CompanyId]          UNIQUEIDENTIFIER NULL,
    [ProjectId]          UNIQUEIDENTIFIER NULL,
    [ErrorNotes]         NVARCHAR (1000)  NULL,
    [RowNo]              INT              NULL,
    CONSTRAINT [PK_StagingCompanyProduct] PRIMARY KEY CLUSTERED ([Id] ASC)
);





