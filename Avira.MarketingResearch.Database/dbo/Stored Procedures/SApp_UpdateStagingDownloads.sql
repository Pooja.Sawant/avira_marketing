﻿CREATE procedure [dbo].[SApp_UpdateStagingDownloads]      
as      
/*================================================================================      
Procedure Name: SApp_UpdateStagingDownloads     
Author: Raju    
Create date: 16/06/2019      
Description: to update StagingDownloads          
Date       Developer    Reason      
__________ ____________ ______________________________________________________      
12/03/2020   Raju      Initial Version      
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)
       
Begin try
UPDATE [dbo].[StagingDownloadsExportData]
      SET 
      [ProjectName] = [dbo].[udfTrim](ProjectName),
      [ModuleName] = [dbo].[udfTrim](ModuleName), 
      [Header] = [dbo].[udfTrim](Header), 
      [Footer] = [dbo].[udfTrim](Footer), 
      [ReportTitle] = [dbo].[udfTrim](ReportTitle),
      [CoverPageImageLink] = [dbo].[udfTrim](CoverPageImageLink),
      [Overview] = [dbo].[udfTrim](Overview), 
      [KeyPoints] = [dbo].[udfTrim](KeyPoints), 
      [Methodology] =[dbo].[udfTrim](Methodology),
      [MethodologyImageLink] = [dbo].[udfTrim](MethodologyImageLink), 
      [ContactUsAnalystName] = [dbo].[udfTrim](ContactUsAnalystName), 
      [ContactUsOfficeContactNo] = ContactUsOfficeContactNo,
      [ContactUsAnalystEmailId] = [dbo].[udfTrim](ContactUsAnalystEmailId),
	  ProjectId = (select top 1 id from Project where Project.ProjectName = StagingDownloadsExportData.ProjectName), 
	  ModuleTypeId = (select top 1 id from ModuleType where ModuleType.DisplayName = StagingDownloadsExportData.ModuleName) ;

;With cteEcoSystemBV as (           
 select SD.ProjectName,     
    CASE WHEN SD.ProjectName IS NULL 
	   THEN 'Project Name is Mandatory' 
	ELSE Null  
	END As ProjectNameError, 
	CASE WHEN SD.ProjectId IS NULL 
	   THEN ' Invaild Project Name' 
	ELSE Null  
	END As ProjectIdError,
	CASE WHEN SD.ModuleName IS NULL 
	   THEN ' Module Name is Mandatory' 
	ELSE Null  
	END As ModuleNameError, 
	CASE WHEN SD.Id IS NULL 
	   THEN ' Invaild Module Name' 
	ELSE Null  
	END As ModuleIdError,
	CASE WHEN  SD.header IS NULL 
       THEN ' Header is Mandatory '  
    ELSE NULL 
    END AS HeaderNameError,
	CASE WHEN  LEN(SD.header) >100 
       THEN ' Maximum 100 Characters are allowed for header'  
    ELSE NULL 
    END AS HeaderNameLengthError,
	CASE  WHEN  SD.Footer IS NULL 
        THEN ' Footer is Mandatory'  
    ELSE NULL END AS FooterNameError,
	CASE WHEN  LEN(SD.Footer) >100 
       THEN ' Maximum 100 Characters are allowed for Footer'  
    ELSE NULL 
    END AS FooterNameLengthError,
	CASE  WHEN  SD.ReportTitle IS NULL 
        THEN ' Report Title is Mandatory'  
    ELSE NULL END AS ReportTitleError,
	CASE WHEN  LEN(SD.ReportTitle) >200 
       THEN ' Maximum 200 Characters are allowed for Report Title'  
    ELSE NULL 
    END AS ReportTitleLengthError,
	CASE WHEN SD.CoverPageImageLink IS NULL 
        THEN ' CoverPageImageLink is Mandatory' 
    ELSE NULL
    END AS CoverPageImageLinkError,
	CASE WHEN  LEN(SD.CoverPageImageLink) >200 
       THEN ' Maximum 200 Characters are allowed for CoverPage Image Link'  
    ELSE NULL 
    END AS CoverPageImageLinkLengthError,
	CASE WHEN SD.Methodology IS NULL 
        THEN ' Methodology is Mandatory' 
    ELSE NULL
    END AS MethodologyNameError,
	CASE WHEN  LEN(SD.Methodology) >3000 
       THEN ' Maximum 3000 Characters are allowed for Methodology'  
    ELSE NULL 
    END AS MethodologyLengthError,
    CASE WHEN (SD.MethodologyImageLink != null AND LEN(SD.MethodologyImageLink) >200)
       THEN ' Maximum 200 Characters are allowed for Methodology Image Link'  
    ELSE NULL 
    END AS MethodologyImageLinkLengthError,
	CASE WHEN SD.Overview IS NULL 
        THEN ' Overview is Mandatory' 
    ELSE NULL
    END AS OverViewNameError,
	CASE WHEN LEN(SD.Overview)>3000
        THEN ' Maximum 3000 Characters are allowed for Overview' 
    ELSE NULL
    END AS OverViewLengthError,
	CASE WHEN SD.KeyPoints IS NULL 
        THEN ' Keypoints is Mandatory' 
    ELSE NULL
    END AS KeyPointsError,
	CASE WHEN LEN(SD.KeyPoints)>3000
        THEN ' Maximum 3000 Characters are allowed for KeyPoints' 
    ELSE NULL
    END AS KeyPointsLengthError,
	CASE WHEN SD.ContactUsAnalystName IS NULL 
	     THEN ' Analystname is Mandatory' 
    ELSE NULL
    END AS AnalystNameError,
    CASE WHEN LEN(SD.ContactUsAnalystName)>100 
	     THEN ' Maximum 100 Characters are allowed for AnalystName'  
    ELSE NULL
    END AS AnalystNameLengthError,  
	CASE WHEN SD.ContactUsOfficeContactNo IS NULL 
	     THEN ' ContactUsOfficeContactNo is Mandatory' 
    ELSE NULL
    END AS AnalystContactNoError,
    CASE WHEN LEN(SD.ContactUsOfficeContactNo)>15 
	     THEN ' Maximum 15 digit are allowed for ContactUsOfficeContactNo'  
    ELSE NULL
    END AS ContactUsOfficeContactNoLengthError,
	CASE WHEN SD.ContactUsAnalystEmailId IS NULL 
         THEN ' Please enter analystemailid' 
    ELSE NULL
    END AS AnalystEmailIdError,
	CASE WHEN LEN(SD.ContactUsAnalystEmailId)>254
	     THEN ' Invalid Mail-Id'  
    ELSE NULL
	End As AnalystEmailIdInvalidError,
	Sd.id as stagingid     
 from  StagingDownloadsExportData SD 
 Where ModuleName='Ecosystem - Broad Level'
 )
Update StagingDownloadsExportData
SET ErrorNotes = ISNULL(cteEcoSystemBV.ProjectNameError,'') + ISNULL(cteEcoSystemBV.ProjectIdError,'')
               + ISNULL(cteEcoSystemBV.ModuleNameError,'') + ISNULL(cteEcoSystemBV.ModuleIdError,'')
               + IsNull(cteEcoSystemBV.HeaderNameError, '') + IsNull(cteEcoSystemBV.HeaderNameLengthError, '')
			   + IsNull(cteEcoSystemBV.FooterNameError, '') + IsNull(cteEcoSystemBV.FooterNameLengthError, '')
			   + IsNull(cteEcoSystemBV.ReportTitleError, '') + IsNull(cteEcoSystemBV.ReportTitleLengthError, '')
               + IsNull(cteEcoSystemBV.CoverPageImageLinkError, '') + IsNull(cteEcoSystemBV.CoverPageImageLinkLengthError, '')
			   + IsNull(cteEcoSystemBV.OverViewNameError, '') + IsNull(cteEcoSystemBV.OverViewLengthError, '')
			   + IsNull(cteEcoSystemBV.KeyPointsError, '') + IsNull(cteEcoSystemBV.KeyPointsLengthError, '')
			   + IsNull(cteEcoSystemBV.MethodologyNameError, '') + IsNull(cteEcoSystemBV.MethodologyLengthError, '')
			   + IsNull(cteEcoSystemBV.AnalystNameError, '')  + IsNull(cteEcoSystemBV.AnalystNameLengthError, '')
			   + IsNull(cteEcoSystemBV.AnalystContactNoError, '')  + IsNull(cteEcoSystemBV.ContactUsOfficeContactNoLengthError, '')
               + IsNull(cteEcoSystemBV.AnalystEmailIdError, '') + IsNull(cteEcoSystemBV.AnalystEmailIdInvalidError, '')
               + IsNull(cteEcoSystemBV.MethodologyImageLinkLengthError, '') 
From cteEcoSystemBV
Inner Join StagingDownloadsExportData sd
On cteEcoSystemBV.stagingid = sd.Id;

;With cteEcoSystemSV as (           
 select SD.ProjectName,     
    CASE WHEN SD.ProjectName IS NULL 
	   THEN 'Project Name is Mandatory' 
	ELSE Null  
	END As ProjectNameError, 
	CASE WHEN SD.ProjectId IS NULL 
	   THEN ' Invaild Project Name' 
	ELSE Null  
	END As ProjectIdError,
	CASE WHEN SD.ModuleName IS NULL 
	   THEN ' Module Name is Mandatory' 
	ELSE Null  
	END As ModuleNameError, 
	CASE WHEN SD.Id IS NULL 
	   THEN ' Invaild Module Name' 
	ELSE Null  
	END As ModuleIdError,
	CASE WHEN  SD.header IS NULL 
       THEN ' Header is Mandatory '  
    ELSE NULL 
    END AS HeaderNameError,
	CASE WHEN  LEN(SD.header) >100 
       THEN ' Maximum 100 Characters are allowed for header'  
    ELSE NULL 
    END AS HeaderNameLengthError,
	CASE  WHEN  SD.Footer IS NULL 
        THEN ' Footer is Mandatory'  
    ELSE NULL END AS FooterNameError,
	CASE WHEN  LEN(SD.Footer) >100 
       THEN ' Maximum 100 Characters are allowed for Footer'  
    ELSE NULL 
    END AS FooterNameLengthError,
	CASE  WHEN  SD.ReportTitle IS NULL 
        THEN ' Report Title is Mandatory'  
    ELSE NULL END AS ReportTitleError,
	CASE WHEN  LEN(SD.ReportTitle) >200 
       THEN ' Maximum 200 Characters are allowed for Report Title'  
    ELSE NULL 
    END AS ReportTitleLengthError,
	CASE WHEN SD.CoverPageImageLink IS NULL 
        THEN ' CoverPageImageLink is Mandatory' 
    ELSE NULL
    END AS CoverPageImageLinkError,
	CASE WHEN  LEN(SD.CoverPageImageLink) >200 
       THEN ' Maximum 200 Characters are allowed for CoverPage Image Link'  
    ELSE NULL 
    END AS CoverPageImageLinkLengthError,
	CASE WHEN SD.Methodology IS NULL 
        THEN ' Methodology is Mandatory' 
    ELSE NULL
    END AS MethodologyNameError,
	CASE WHEN  LEN(SD.Methodology) >3000 
       THEN ' Maximum 3000 Characters are allowed for Methodology'  
    ELSE NULL 
    END AS MethodologyLengthError,
    CASE WHEN (SD.MethodologyImageLink != null AND LEN(SD.MethodologyImageLink) >200)
       THEN ' Maximum 200 Characters are allowed for Methodology Image Link'  
    ELSE NULL 
    END AS MethodologyImageLinkLengthError,
	CASE WHEN SD.Overview IS NULL 
        THEN ' Overview is Mandatory' 
    ELSE NULL
    END AS OverViewNameError,
	CASE WHEN LEN(SD.Overview)>3000
        THEN ' Maximum 3000 Characters are allowed for Overview' 
    ELSE NULL
    END AS OverViewLengthError,
	CASE WHEN SD.KeyPoints IS NULL 
        THEN ' Keypoints is Mandatory' 
    ELSE NULL
    END AS KeyPointsError,
	CASE WHEN LEN(SD.KeyPoints)>3000
        THEN ' Maximum 3000 Characters are allowed for KeyPoints' 
    ELSE NULL
    END AS KeyPointsLengthError,
	CASE WHEN SD.ContactUsAnalystName IS NULL 
	     THEN ' Analystname is Mandatory' 
    ELSE NULL
    END AS AnalystNameError,
    CASE WHEN LEN(SD.ContactUsAnalystName)>100 
	     THEN ' Maximum 100 Characters are allowed for AnalystName'  
    ELSE NULL
    END AS AnalystNameLengthError,  
	CASE WHEN SD.ContactUsOfficeContactNo IS NULL 
	     THEN ' ContactUsOfficeContactNo is Mandatory' 
    ELSE NULL
    END AS AnalystContactNoError,
    CASE WHEN LEN(SD.ContactUsOfficeContactNo)>15 
	     THEN ' Maximum 15 digit are allowed for ContactUsOfficeContactNo'  
    ELSE NULL
    END AS ContactUsOfficeContactNoLengthError,
	CASE WHEN SD.ContactUsAnalystEmailId IS NULL 
         THEN ' Please enter analystemailid' 
    ELSE NULL
    END AS AnalystEmailIdError,
	CASE WHEN LEN(SD.ContactUsAnalystEmailId)>254
	     THEN ' Invalid Mail-Id'  
    ELSE NULL
	End As AnalystEmailIdInvalidError,
	Sd.id as stagingid     
 from  StagingDownloadsExportData SD 
 Where ModuleName='Ecosystem - Segment Level'
 )
Update StagingDownloadsExportData
SET ErrorNotes = ISNULL(cteEcoSystemSV.ProjectNameError,'') + ISNULL(cteEcoSystemSV.ProjectIdError,'')
               + ISNULL(cteEcoSystemSV.ModuleNameError,'') + ISNULL(cteEcoSystemSV.ModuleIdError,'')
               + IsNull(cteEcoSystemSV.HeaderNameError, '') + IsNull(cteEcoSystemSV.HeaderNameLengthError, '')
			   + IsNull(cteEcoSystemSV.FooterNameError, '') + IsNull(cteEcoSystemSV.FooterNameLengthError, '')
			   + IsNull(cteEcoSystemSV.ReportTitleError, '') + IsNull(cteEcoSystemSV.ReportTitleLengthError, '')
               + IsNull(cteEcoSystemSV.CoverPageImageLinkError, '') + IsNull(cteEcoSystemSV.CoverPageImageLinkLengthError, '')
			   + IsNull(cteEcoSystemSV.OverViewNameError, '') + IsNull(cteEcoSystemSV.OverViewLengthError, '')
			   + IsNull(cteEcoSystemSV.KeyPointsError, '') + IsNull(cteEcoSystemSV.KeyPointsLengthError, '')
			   + IsNull(cteEcoSystemSV.MethodologyNameError, '') + IsNull(cteEcoSystemSV.MethodologyLengthError, '')
			   + IsNull(cteEcoSystemSV.AnalystNameError, '')  + IsNull(cteEcoSystemSV.AnalystNameLengthError, '')
			   + IsNull(cteEcoSystemSV.AnalystContactNoError, '')  + IsNull(cteEcoSystemSV.ContactUsOfficeContactNoLengthError, '')
               + IsNull(cteEcoSystemSV.AnalystEmailIdError, '') + IsNull(cteEcoSystemSV.AnalystEmailIdInvalidError, '')
               + IsNull(cteEcoSystemSV.MethodologyImageLinkLengthError, '') 
From cteEcoSystemSV
Inner Join StagingDownloadsExportData sd
On cteEcoSystemSV.stagingid = sd.Id;

;With cteCP as (           
 select SD.ProjectName,     
    CASE WHEN SD.ProjectName IS NULL 
	   THEN 'Project Name is Mandatory' 
	ELSE Null  
	END As ProjectNameError, 
	CASE WHEN SD.ProjectId IS NULL 
	   THEN ' Invaild Project Name' 
	ELSE Null  
	END As ProjectIdError,
	CASE WHEN SD.ModuleName IS NULL 
	   THEN ' Module Name is Mandatory' 
	ELSE Null  
	END As ModuleNameError, 
	CASE WHEN SD.Id IS NULL 
	   THEN ' Invaild Module Name' 
	ELSE Null  
	END As ModuleIdError,
	CASE WHEN  SD.header IS NULL 
       THEN ' Header is Mandatory '  
    ELSE NULL 
    END AS HeaderNameError,
	CASE WHEN  LEN(SD.header) >100 
       THEN ' Maximum 100 Characters are allowed for header'  
    ELSE NULL 
    END AS HeaderNameLengthError,
	CASE  WHEN  SD.Footer IS NULL 
        THEN ' Footer is Mandatory'  
    ELSE NULL END AS FooterNameError,
	CASE WHEN  LEN(SD.Footer) >100 
       THEN ' Maximum 100 Characters are allowed for Footer'  
    ELSE NULL 
    END AS FooterNameLengthError,
	CASE  WHEN  SD.ReportTitle IS NULL 
        THEN ' Report Title is Mandatory'  
    ELSE NULL END AS ReportTitleError,
	CASE WHEN  LEN(SD.ReportTitle) >200 
       THEN ' Maximum 200 Characters are allowed for Report Title'  
    ELSE NULL 
    END AS ReportTitleLengthError,
	CASE WHEN SD.CoverPageImageLink IS NULL 
        THEN ' CoverPageImageLink is Mandatory' 
    ELSE NULL
    END AS CoverPageImageLinkError,
	CASE WHEN  LEN(SD.CoverPageImageLink) >200 
       THEN ' Maximum 200 Characters are allowed for CoverPage Image Link'  
    ELSE NULL 
    END AS CoverPageImageLinkLengthError,
	CASE WHEN SD.Methodology IS NULL 
        THEN ' Methodology is Mandatory' 
    ELSE NULL
    END AS MethodologyNameError,
	CASE WHEN  LEN(SD.Methodology) >3000 
       THEN ' Maximum 3000 Characters are allowed for Methodology'  
    ELSE NULL 
    END AS MethodologyLengthError,
    CASE WHEN (SD.MethodologyImageLink != null AND LEN(SD.MethodologyImageLink) >200)
       THEN ' Maximum 200 Characters are allowed for Methodology Image Link'  
    ELSE NULL 
    END AS MethodologyImageLinkLengthError,
	CASE WHEN SD.ContactUsAnalystName IS NULL 
	     THEN ' Analystname is Mandatory' 
    ELSE NULL
    END AS AnalystNameError,
    CASE WHEN LEN(SD.ContactUsAnalystName)>100 
	     THEN ' Maximum 100 Characters are allowed for AnalystName'  
    ELSE NULL
    END AS AnalystNameLengthError,  
	CASE WHEN SD.ContactUsOfficeContactNo IS NULL 
	     THEN ' ContactUsOfficeContactNo is Mandatory' 
    ELSE NULL
    END AS AnalystContactNoError,
    CASE WHEN LEN(SD.ContactUsOfficeContactNo)>15 
	     THEN ' Maximum 15 digit are allowed for ContactUsOfficeContactNo'  
    ELSE NULL
    END AS ContactUsOfficeContactNoLengthError,
	CASE WHEN SD.ContactUsAnalystEmailId IS NULL 
         THEN ' Please enter analystemailid' 
    ELSE NULL
    END AS AnalystEmailIdError,
	CASE WHEN LEN(SD.ContactUsAnalystEmailId)>254 
	     THEN ' Invalid Mail-Id'  
    ELSE NULL
	End As AnalystEmailIdInvalidError,
	Sd.id as stagingid
 from  StagingDownloadsExportData SD 
 Where ModuleName='Company Profile'
 )
Update StagingDownloadsExportData
SET ErrorNotes = ISNULL(cteCP.ProjectNameError,'') + ISNULL(cteCP.ProjectIdError,'')
               + ISNULL(cteCP.ModuleNameError,'') + ISNULL(cteCP.ModuleIdError,'')
               + IsNull(cteCP.HeaderNameError, '') + IsNull(cteCP.HeaderNameLengthError, '')
			   + IsNull(cteCP.FooterNameError, '') + IsNull(cteCP.FooterNameLengthError, '')
			   + IsNull(cteCP.ReportTitleError, '') + IsNull(cteCP.ReportTitleLengthError, '')
               + IsNull(cteCP.CoverPageImageLinkError, '') + IsNull(cteCP.CoverPageImageLinkLengthError, '')
			   + IsNull(cteCP.MethodologyNameError, '') + IsNull(cteCP.MethodologyLengthError, '')
			   + IsNull(cteCP.AnalystNameError, '')  + IsNull(cteCP.AnalystNameLengthError, '')
			   + IsNull(cteCP.AnalystContactNoError, '')  + IsNull(cteCP.ContactUsOfficeContactNoLengthError, '')
               + IsNull(cteCP.AnalystEmailIdError, '') + IsNull(cteCP.AnalystEmailIdInvalidError, '')
               + IsNull(cteCP.MethodologyImageLinkLengthError, '') 
From cteCP
Inner Join StagingDownloadsExportData sd
On cteCP.stagingid = sd.Id;

;With cteTrends as (           
 select SD.ProjectName,     
    CASE WHEN SD.ProjectName IS NULL 
	   THEN 'Project Name is Mandatory' 
	ELSE Null  
	END As ProjectNameError, 
	CASE WHEN SD.ProjectId IS NULL 
	   THEN ' Invaild Project Name' 
	ELSE Null  
	END As ProjectIdError,
	CASE WHEN SD.ModuleName IS NULL 
	   THEN ' Module Name is Mandatory' 
	ELSE Null  
	END As ModuleNameError, 
	CASE WHEN SD.Id IS NULL 
	   THEN ' Invaild Module Name' 
	ELSE Null  
	END As ModuleIdError,
	CASE WHEN  SD.header IS NULL 
       THEN ' Header is Mandatory '  
    ELSE NULL 
    END AS HeaderNameError,
	CASE WHEN  LEN(SD.header) >100 
       THEN ' Maximum 100 Characters are allowed for header'  
    ELSE NULL 
    END AS HeaderNameLengthError,
	CASE  WHEN  SD.Footer IS NULL 
        THEN ' Footer is Mandatory'  
    ELSE NULL END AS FooterNameError,
	CASE WHEN  LEN(SD.Footer) >100 
       THEN ' Maximum 100 Characters are allowed for Footer'  
    ELSE NULL 
    END AS FooterNameLengthError,
	CASE  WHEN  SD.ReportTitle IS NULL 
        THEN ' Report Title is Mandatory'  
    ELSE NULL END AS ReportTitleError,
	CASE WHEN  LEN(SD.ReportTitle) >200 
       THEN ' Maximum 200 Characters are allowed for Report Title'  
    ELSE NULL 
    END AS ReportTitleLengthError,
	CASE WHEN SD.CoverPageImageLink IS NULL 
        THEN ' CoverPageImageLink is Mandatory' 
    ELSE NULL
    END AS CoverPageImageLinkError,
	CASE WHEN  LEN(SD.CoverPageImageLink) >200 
       THEN ' Maximum 200 Characters are allowed for CoverPage Image Link'  
    ELSE NULL 
    END AS CoverPageImageLinkLengthError,
	CASE WHEN SD.Methodology IS NULL 
        THEN ' Methodology is Mandatory' 
    ELSE NULL
    END AS MethodologyNameError,
	CASE WHEN  LEN(SD.Methodology) >3000 
       THEN ' Maximum 3000 Characters are allowed for Methodology'  
    ELSE NULL 
    END AS MethodologyLengthError,
    CASE WHEN (SD.MethodologyImageLink != null AND LEN(SD.MethodologyImageLink) >200)
       THEN ' Maximum 200 Characters are allowed for Methodology Image Link'  
    ELSE NULL 
    END AS MethodologyImageLinkLengthError,
	CASE WHEN SD.ContactUsAnalystName IS NULL 
	     THEN ' Analystname is Mandatory' 
    ELSE NULL
    END AS AnalystNameError,
    CASE WHEN LEN(SD.ContactUsAnalystName)>100 
	     THEN ' Maximum 100 Characters are allowed for AnalystName'  
    ELSE NULL
    END AS AnalystNameLengthError,  
	CASE WHEN SD.ContactUsOfficeContactNo IS NULL 
	     THEN 'ContactUsOfficeContactNo is Mandatory' 
    ELSE NULL
    END AS AnalystContactNoError,
    CASE WHEN LEN(SD.ContactUsOfficeContactNo)>15 
	     THEN ' Maximum 15 digit are allowed for ContactUsOfficeContactNo'  
    ELSE NULL
    END AS ContactUsOfficeContactNoLengthError,
	CASE WHEN SD.ContactUsAnalystEmailId IS NULL 
         THEN ' Please enter analystemailid' 
    ELSE NULL
    END AS AnalystEmailIdError,
	CASE WHEN LEN(SD.ContactUsAnalystEmailId)>254
	     THEN ' Invalid Mail-Id'  
    ELSE NULL
	End As AnalystEmailIdInvalidError,
	Sd.id as stagingid
 from  StagingDownloadsExportData SD 
 Where ModuleName='Trends'
 )
Update StagingDownloadsExportData
SET ErrorNotes = ISNULL(cteTrends.ProjectNameError,'') + ISNULL(cteTrends.ProjectIdError,'')
               + ISNULL(cteTrends.ModuleNameError,'') + ISNULL(cteTrends.ModuleIdError,'')
               + IsNull(cteTrends.HeaderNameError, '') + IsNull(cteTrends.HeaderNameLengthError, '')
			   + IsNull(cteTrends.FooterNameError, '') + IsNull(cteTrends.FooterNameLengthError, '')
			   + IsNull(cteTrends.ReportTitleError, '') + IsNull(cteTrends.ReportTitleLengthError, '')
               + IsNull(cteTrends.CoverPageImageLinkError, '') + IsNull(cteTrends.CoverPageImageLinkLengthError, '')
			   + IsNull(cteTrends.MethodologyNameError, '') + IsNull(cteTrends.MethodologyLengthError, '')
			   + IsNull(cteTrends.AnalystNameError, '')  + IsNull(cteTrends.AnalystNameLengthError, '')
			   + IsNull(cteTrends.AnalystContactNoError, '')  + IsNull(cteTrends.ContactUsOfficeContactNoLengthError, '')
               + IsNull(cteTrends.AnalystEmailIdError, '') + IsNull(cteTrends.AnalystEmailIdInvalidError, '')
               + IsNull(cteTrends.MethodologyImageLinkLengthError, '')

From cteTrends
Inner Join StagingDownloadsExportData sd
On cteTrends.stagingid = sd.Id;

;With cteInfographics as (           
select SD.ProjectName,     
    CASE WHEN SD.ProjectName IS NULL 
	   THEN 'Project Name is Mandatory' 
	ELSE Null  
	END As ProjectNameError, 
	CASE WHEN SD.ProjectId IS NULL 
	   THEN ' Invaild Project Name' 
	ELSE Null  
	END As ProjectIdError,
	CASE WHEN SD.ModuleName IS NULL 
	   THEN ' Module Name is Mandatory' 
	ELSE Null  
	END As ModuleNameError, 
	CASE WHEN SD.Id IS NULL 
	   THEN ' Invaild Module Name' 
	ELSE Null  
	END As ModuleIdError,
	CASE WHEN  SD.header IS NULL 
       THEN ' Header is Mandatory '  
    ELSE NULL 
    END AS HeaderNameError,
	CASE WHEN  LEN(SD.header) >100 
       THEN ' Maximum 100 Characters are allowed for header'  
    ELSE NULL 
    END AS HeaderNameLengthError,
	CASE  WHEN  SD.Footer IS NULL 
        THEN ' Footer is Mandatory'  
    ELSE NULL END AS FooterNameError,
	CASE WHEN  LEN(SD.Footer) >100 
       THEN ' Maximum 100 Characters are allowed for Footer'  
    ELSE NULL 
    END AS FooterNameLengthError,
	CASE  WHEN  SD.ReportTitle IS NULL 
        THEN ' Report Title is Mandatory'  
    ELSE NULL END AS ReportTitleError,
	CASE WHEN  LEN(SD.ReportTitle) >200 
       THEN ' Maximum 200 Characters are allowed for Report Title'  
    ELSE NULL 
    END AS ReportTitleLengthError,
	CASE WHEN SD.CoverPageImageLink IS NULL 
        THEN ' CoverPageImageLink is Mandatory' 
    ELSE NULL
    END AS CoverPageImageLinkError,
	CASE WHEN  LEN(SD.CoverPageImageLink) >200 
       THEN ' Maximum 200 Characters are allowed for CoverPage Image Link'  
    ELSE NULL 
    END AS CoverPageImageLinkLengthError,
	CASE WHEN SD.Methodology IS NULL 
        THEN ' Methodology is Mandatory' 
    ELSE NULL
    END AS MethodologyNameError,
	CASE WHEN  LEN(SD.Methodology) >3000 
       THEN ' Maximum 3000 Characters are allowed for Methodology'  
    ELSE NULL 
    END AS MethodologyLengthError,
    CASE WHEN (SD.MethodologyImageLink != null AND LEN(SD.MethodologyImageLink) >200)
       THEN ' Maximum 200 Characters are allowed for Methodology Image Link'  
    ELSE NULL 
    END AS MethodologyImageLinkLengthError,
	CASE WHEN SD.ContactUsAnalystName IS NULL 
	     THEN ' Analystname is Mandatory' 
    ELSE NULL
    END AS AnalystNameError,
    CASE WHEN LEN(SD.ContactUsAnalystName)>100 
	     THEN ' Maximum 100 Characters are allowed for AnalystName'  
    ELSE NULL
    END AS AnalystNameLengthError,  
	CASE WHEN SD.ContactUsOfficeContactNo IS NULL 
	     THEN ' ContactUsOfficeContactNo is Mandatory' 
    ELSE NULL
    END AS AnalystContactNoError,
    CASE WHEN LEN(SD.ContactUsOfficeContactNo)>15 
	     THEN ' Maximum 15 digit are allowed for ContactUsOfficeContactNo'  
    ELSE NULL
    END AS ContactUsOfficeContactNoLengthError,
	CASE WHEN SD.ContactUsAnalystEmailId IS NULL 
         THEN ' Please enter analystemailid' 
    ELSE NULL
    END AS AnalystEmailIdError,
	CASE WHEN LEN(SD.ContactUsAnalystEmailId)>254 
	     THEN ' Invalid Mail-Id'  
    ELSE NULL
	End As AnalystEmailIdInvalidError,
	Sd.id as stagingid
 from  StagingDownloadsExportData SD 
 Where ModuleName='Infographics'
 )
Update StagingDownloadsExportData
SET ErrorNotes = ISNULL(cteInfographics.ProjectNameError,'') + ISNULL(cteInfographics.ProjectIdError,'')
               + ISNULL(cteInfographics.ModuleNameError,'') + ISNULL(cteInfographics.ModuleIdError,'')
               + IsNull(cteInfographics.HeaderNameError, '') + IsNull(cteInfographics.HeaderNameLengthError, '')
			   + IsNull(cteInfographics.FooterNameError, '') + IsNull(cteInfographics.FooterNameLengthError, '')
			   + IsNull(cteInfographics.ReportTitleError, '') + IsNull(cteInfographics.ReportTitleLengthError, '')
               + IsNull(cteInfographics.CoverPageImageLinkError, '') + IsNull(cteInfographics.CoverPageImageLinkLengthError, '')
			   + IsNull(cteInfographics.MethodologyNameError, '') + IsNull(cteInfographics.MethodologyLengthError, '')
			   + IsNull(cteInfographics.AnalystNameError, '')  + IsNull(cteInfographics.AnalystNameLengthError, '')
			   + IsNull(cteInfographics.AnalystContactNoError, '')  + IsNull(cteInfographics.ContactUsOfficeContactNoLengthError, '')
               + IsNull(cteInfographics.AnalystEmailIdError, '') + IsNull(cteInfographics.AnalystEmailIdInvalidError, '')
               + IsNull(cteInfographics.MethodologyImageLinkLengthError, '')
From cteInfographics
Inner Join StagingDownloadsExportData sd
On cteInfographics.stagingid = sd.Id;

;With cteOverviews as (           
select SD.ProjectName,     
    CASE WHEN SD.ProjectName IS NULL 
	   THEN 'Project Name is Mandatory' 
	ELSE Null  
	END As ProjectNameError, 
	CASE WHEN SD.ProjectId IS NULL 
	   THEN ' Invaild Project Name' 
	ELSE Null  
	END As ProjectIdError,
	CASE WHEN SD.ModuleName IS NULL 
	   THEN ' Module Name is Mandatory' 
	ELSE Null  
	END As ModuleNameError, 
	CASE WHEN SD.Id IS NULL 
	   THEN ' Invaild Module Name' 
	ELSE Null  
	END As ModuleIdError,
	CASE WHEN  SD.header IS NULL 
       THEN ' Header is Mandatory '  
    ELSE NULL 
    END AS HeaderNameError,
	CASE WHEN  LEN(SD.header) >100 
       THEN ' Maximum 100 Characters are allowed for header'  
    ELSE NULL 
    END AS HeaderNameLengthError,
	CASE  WHEN  SD.Footer IS NULL 
        THEN ' Footer is Mandatory'  
    ELSE NULL END AS FooterNameError,
	CASE WHEN  LEN(SD.Footer) >100 
       THEN ' Maximum 100 Characters are allowed for Footer'  
    ELSE NULL 
    END AS FooterNameLengthError,
	CASE  WHEN  SD.ReportTitle IS NULL 
        THEN ' Report Title is Mandatory'  
    ELSE NULL END AS ReportTitleError,
	CASE WHEN  LEN(SD.ReportTitle) >200 
       THEN ' Maximum 200 Characters are allowed for Report Title'  
    ELSE NULL 
    END AS ReportTitleLengthError,
	CASE WHEN SD.CoverPageImageLink IS NULL 
        THEN ' CoverPageImageLink is Mandatory' 
    ELSE NULL
    END AS CoverPageImageLinkError,
	CASE WHEN  LEN(SD.CoverPageImageLink) >200 
       THEN ' Maximum 200 Characters are allowed for CoverPage Image Link'  
    ELSE NULL 
    END AS CoverPageImageLinkLengthError,
	CASE WHEN SD.Methodology IS NULL 
        THEN ' Methodology is Mandatory' 
    ELSE NULL
    END AS MethodologyNameError,
	CASE WHEN  LEN(SD.Methodology) >3000 
       THEN ' Maximum 3000 Characters are allowed for Methodology'  
    ELSE NULL 
    END AS MethodologyLengthError,
    CASE WHEN (SD.MethodologyImageLink != null AND LEN(SD.MethodologyImageLink) >200)
       THEN ' Maximum 200 Characters are allowed for Methodology Image Link'  
    ELSE NULL 
    END AS MethodologyImageLinkLengthError,
	CASE WHEN SD.ContactUsAnalystName IS NULL 
	     THEN ' Analystname is Mandatory' 
    ELSE NULL
    END AS AnalystNameError,
    CASE WHEN LEN(SD.ContactUsAnalystName)>100 
	     THEN ' Maximum 100 Characters are allowed for AnalystName'  
    ELSE NULL
    END AS AnalystNameLengthError,  
	CASE WHEN SD.ContactUsOfficeContactNo IS NULL 
	     THEN ' ContactUsOfficeContactNo is Mandatory' 
    ELSE NULL
    END AS AnalystContactNoError,
    CASE WHEN LEN(SD.ContactUsOfficeContactNo)>15 
	     THEN ' Maximum 15 digit are allowed for ContactUsOfficeContactNo'  
    ELSE NULL
    END AS ContactUsOfficeContactNoLengthError,
	CASE WHEN SD.ContactUsAnalystEmailId IS NULL 
         THEN ' Please enter analystemailid' 
    ELSE NULL
    END AS AnalystEmailIdError,
	CASE WHEN LEN(SD.ContactUsAnalystEmailId)>254 
	     THEN ' Invalid Mail-Id'  
    ELSE NULL
	End As AnalystEmailIdInvalidError,
	Sd.id as stagingid
 from  StagingDownloadsExportData SD 
 Where ModuleName='Overviews'
 )
Update StagingDownloadsExportData
SET ErrorNotes = ISNULL(cteOverviews.ProjectNameError,'') + ISNULL(cteOverviews.ProjectIdError,'')
               + ISNULL(cteOverviews.ModuleNameError,'') + ISNULL(cteOverviews.ModuleIdError,'')
               + IsNull(cteOverviews.HeaderNameError, '') + IsNull(cteOverviews.HeaderNameLengthError, '')
			   + IsNull(cteOverviews.FooterNameError, '') + IsNull(cteOverviews.FooterNameLengthError, '')
			   + IsNull(cteOverviews.ReportTitleError, '') + IsNull(cteOverviews.ReportTitleLengthError, '')
               + IsNull(cteOverviews.CoverPageImageLinkError, '') + IsNull(cteOverviews.CoverPageImageLinkLengthError, '')
			   + IsNull(cteOverviews.MethodologyNameError, '') + IsNull(cteOverviews.MethodologyLengthError, '')
			   + IsNull(cteOverviews.AnalystNameError, '')  + IsNull(cteOverviews.AnalystNameLengthError, '')
			   + IsNull(cteOverviews.AnalystContactNoError, '')  + IsNull(cteOverviews.ContactUsOfficeContactNoLengthError, '')
               + IsNull(cteOverviews.AnalystEmailIdError, '') + IsNull(cteOverviews.AnalystEmailIdInvalidError, '')
               + IsNull(cteOverviews.MethodologyImageLinkLengthError, '')
From cteOverviews
Inner Join StagingDownloadsExportData sd
On cteOverviews.stagingid = sd.Id;

;With cteMarketTree as (           
 select SD.ProjectName,     
    CASE WHEN SD.ProjectName IS NULL 
	   THEN 'Project Name is Mandatory' 
	ELSE Null  
	END As ProjectNameError, 
	CASE WHEN SD.ProjectId IS NULL 
	   THEN ' Invaild Project Name' 
	ELSE Null  
	END As ProjectIdError,
	CASE WHEN SD.ModuleName IS NULL 
	   THEN ' Module Name is Mandatory' 
	ELSE Null  
	END As ModuleNameError, 
	CASE WHEN SD.Id IS NULL 
	   THEN ' Invaild Module Name' 
	ELSE Null  
	END As ModuleIdError,
	CASE WHEN  SD.header IS NULL 
       THEN ' Header is Mandatory '  
    ELSE NULL 
    END AS HeaderNameError,
	CASE WHEN  LEN(SD.header) >100 
       THEN ' Maximum 100 Characters are allowed for header'  
    ELSE NULL 
    END AS HeaderNameLengthError,
	CASE  WHEN  SD.Footer IS NULL 
        THEN ' Footer is Mandatory'  
    ELSE NULL END AS FooterNameError,
	CASE WHEN  LEN(SD.Footer) >100 
       THEN ' Maximum 100 Characters are allowed for Footer'  
    ELSE NULL 
    END AS FooterNameLengthError,
	CASE  WHEN  SD.ReportTitle IS NULL 
        THEN ' Report Title is Mandatory'  
    ELSE NULL END AS ReportTitleError,
	CASE WHEN  LEN(SD.ReportTitle) >200 
       THEN ' Maximum 200 Characters are allowed for Report Title'  
    ELSE NULL 
    END AS ReportTitleLengthError,
	CASE WHEN SD.CoverPageImageLink IS NULL 
        THEN ' CoverPageImageLink is Mandatory' 
    ELSE NULL
    END AS CoverPageImageLinkError,
	CASE WHEN  LEN(SD.CoverPageImageLink) >200 
       THEN ' Maximum 200 Characters are allowed for CoverPage Image Link'  
    ELSE NULL 
    END AS CoverPageImageLinkLengthError,
	CASE WHEN SD.Methodology IS NULL 
        THEN 'Methodology is Mandatory' 
    ELSE NULL
    END AS MethodologyNameError,
	CASE WHEN  LEN(SD.Methodology) >3000 
       THEN ' Maximum 3000 Characters are allowed for Methodology'  
    ELSE NULL 
    END AS MethodologyLengthError,
    CASE WHEN (SD.MethodologyImageLink != null AND LEN(SD.MethodologyImageLink) >200)
       THEN ' Maximum 200 Characters are allowed for Methodology Image Link'  
    ELSE NULL 
    END AS MethodologyImageLinkLengthError,
	CASE WHEN SD.Overview IS NULL 
        THEN ' Overview is Mandatory' 
    ELSE NULL
    END AS OverViewNameError,
	CASE WHEN LEN(SD.Overview)>3000
        THEN ' Maximum 3000 Characters are allowed for Overview' 
    ELSE NULL
    END AS OverViewLengthError,
	CASE WHEN SD.KeyPoints IS NULL 
        THEN ' Keypoints is Mandatory' 
    ELSE NULL
    END AS KeyPointsError,
	CASE WHEN LEN(SD.KeyPoints)>3000
        THEN ' Maximum 3000 Characters are allowed for KeyPoints' 
    ELSE NULL
    END AS KeyPointsLengthError,
	CASE WHEN SD.ContactUsAnalystName IS NULL 
	     THEN ' Analystname is Mandatory' 
    ELSE NULL
    END AS AnalystNameError,
    CASE WHEN LEN(SD.ContactUsAnalystName)>100 
	     THEN ' Maximum 100 Characters are allowed for AnalystName'  
    ELSE NULL
    END AS AnalystNameLengthError,  
	CASE WHEN SD.ContactUsOfficeContactNo IS NULL 
	     THEN ' ContactUsOfficeContactNo is Mandatory' 
    ELSE NULL
    END AS AnalystContactNoError,
    CASE WHEN LEN(SD.ContactUsOfficeContactNo)>15 
	     THEN ' Maximum 15 digit are allowed for ContactUsOfficeContactNo'  
    ELSE NULL
    END AS ContactUsOfficeContactNoLengthError,
	CASE WHEN SD.ContactUsAnalystEmailId IS NULL 
         THEN ' Please enter analystemailid' 
    ELSE NULL
    END AS AnalystEmailIdError,
	CASE WHEN LEN(SD.ContactUsAnalystEmailId)>254 
	     THEN ' Invalid Mail-Id'  
    ELSE NULL
	End As AnalystEmailIdInvalidError,
	Sd.id as stagingid     
 from  StagingDownloadsExportData SD 
 Where ModuleName='Market Tree'
 )
Update StagingDownloadsExportData
SET ErrorNotes = ISNULL(cteMarketTree.ProjectNameError,'') + ISNULL(cteMarketTree.ProjectIdError,'')
               + ISNULL(cteMarketTree.ModuleNameError,'') + ISNULL(cteMarketTree.ModuleIdError,'')
               + IsNull(cteMarketTree.HeaderNameError, '') + IsNull(cteMarketTree.HeaderNameLengthError, '')
			   + IsNull(cteMarketTree.FooterNameError, '') + IsNull(cteMarketTree.FooterNameLengthError, '')
			   + IsNull(cteMarketTree.ReportTitleError, '') + IsNull(cteMarketTree.ReportTitleLengthError, '')
               + IsNull(cteMarketTree.CoverPageImageLinkError, '') + IsNull(cteMarketTree.CoverPageImageLinkLengthError, '')
			   + IsNull(cteMarketTree.OverViewNameError, '') + IsNull(cteMarketTree.OverViewLengthError, '')
			   + IsNull(cteMarketTree.KeyPointsError, '') + IsNull(cteMarketTree.KeyPointsLengthError, '')
			   + IsNull(cteMarketTree.MethodologyNameError, '') + IsNull(cteMarketTree.MethodologyLengthError, '')
			   + IsNull(cteMarketTree.AnalystNameError, '')  + IsNull(cteMarketTree.AnalystNameLengthError, '')
			   + IsNull(cteMarketTree.AnalystContactNoError, '')  + IsNull(cteMarketTree.ContactUsOfficeContactNoLengthError, '')
               + IsNull(cteMarketTree.AnalystEmailIdError, '') + IsNull(cteMarketTree.AnalystEmailIdInvalidError, '')
               + IsNull(cteMarketTree.MethodologyImageLinkLengthError, '') 
From cteMarketTree
Inner Join StagingDownloadsExportData sd
On cteMarketTree.stagingid = sd.Id;

;With cteMarketSize as (           
 select SD.ProjectName,     
    CASE WHEN SD.ProjectName IS NULL 
	   THEN 'Project Name is Mandatory' 
	ELSE Null  
	END As ProjectNameError, 
	CASE WHEN SD.ProjectId IS NULL 
	   THEN ' Invaild Project Name' 
	ELSE Null  
	END As ProjectIdError,
	CASE WHEN SD.ModuleName IS NULL 
	   THEN ' Module Name is Mandatory' 
	ELSE Null  
	END As ModuleNameError, 
	CASE WHEN SD.Id IS NULL 
	   THEN ' Invaild Module Name' 
	ELSE Null  
	END As ModuleIdError,
	CASE WHEN  SD.header IS NULL 
       THEN ' Header is Mandatory '  
    ELSE NULL 
    END AS HeaderNameError,
	CASE WHEN  LEN(SD.header) >100 
       THEN ' Maximum 100 Characters are allowed for header'  
    ELSE NULL 
    END AS HeaderNameLengthError,
	CASE  WHEN  SD.Footer IS NULL 
        THEN ' Footer is Mandatory'  
    ELSE NULL END AS FooterNameError,
	CASE WHEN  LEN(SD.Footer) >100 
       THEN ' Maximum 100 Characters are allowed for Footer'  
    ELSE NULL 
    END AS FooterNameLengthError,
	CASE  WHEN  SD.ReportTitle IS NULL 
        THEN ' Report Title is Mandatory'  
    ELSE NULL END AS ReportTitleError,
	CASE WHEN  LEN(SD.ReportTitle) >200 
       THEN ' Maximum 200 Characters are allowed for Report Title'  
    ELSE NULL 
    END AS ReportTitleLengthError,
	CASE WHEN SD.CoverPageImageLink IS NULL 
        THEN ' CoverPageImageLink is Mandatory' 
    ELSE NULL
    END AS CoverPageImageLinkError,
	CASE WHEN  LEN(SD.CoverPageImageLink) >200 
       THEN ' Maximum 200 Characters are allowed for CoverPage Image Link'  
    ELSE NULL 
    END AS CoverPageImageLinkLengthError,
	CASE WHEN SD.Methodology IS NULL 
        THEN ' Methodology is Mandatory' 
    ELSE NULL
    END AS MethodologyNameError,
	CASE WHEN  LEN(SD.Methodology) >3000 
       THEN ' Maximum 3000 Characters are allowed for Methodology'  
    ELSE NULL 
    END AS MethodologyLengthError,
    CASE WHEN (SD.MethodologyImageLink != null AND LEN(SD.MethodologyImageLink) >200)
       THEN ' Maximum 200 Characters are allowed for Methodology Image Link'  
    ELSE NULL 
    END AS MethodologyImageLinkLengthError,
	CASE WHEN SD.Overview IS NULL 
        THEN ' Overview is Mandatory' 
    ELSE NULL
    END AS OverViewNameError,
	CASE WHEN LEN(SD.Overview)>3000
        THEN ' Maximum 3000 Characters are allowed for Overview' 
    ELSE NULL
    END AS OverViewLengthError,
	CASE WHEN SD.KeyPoints IS NULL 
        THEN ' Keypoints is Mandatory' 
    ELSE NULL
    END AS KeyPointsError,
	CASE WHEN LEN(SD.KeyPoints)>3000
        THEN ' Maximum 3000 Characters are allowed for KeyPoints' 
    ELSE NULL
    END AS KeyPointsLengthError,
	CASE WHEN SD.ContactUsAnalystName IS NULL 
	     THEN ' Analystname is Mandatory' 
    ELSE NULL
    END AS AnalystNameError,
    CASE WHEN LEN(SD.ContactUsAnalystName)>100 
	     THEN ' Maximum 100 Characters are allowed for AnalystName'  
    ELSE NULL
    END AS AnalystNameLengthError,  
	CASE WHEN SD.ContactUsOfficeContactNo IS NULL 
	     THEN ' ContactUsOfficeContactNo is Mandatory' 
    ELSE NULL
    END AS AnalystContactNoError,
    CASE WHEN LEN(SD.ContactUsOfficeContactNo)>15 
	     THEN ' Maximum 15 digit are allowed for ContactUsOfficeContactNo'  
    ELSE NULL
    END AS ContactUsOfficeContactNoLengthError,
	CASE WHEN SD.ContactUsAnalystEmailId IS NULL 
         THEN ' Please enter analystemailid' 
    ELSE NULL
    END AS AnalystEmailIdError,
	CASE WHEN LEN(SD.ContactUsAnalystEmailId)>254
	     THEN ' Invalid Mail-Id'  
    ELSE NULL
	End As AnalystEmailIdInvalidError,
	Sd.id as stagingid     
 from  StagingDownloadsExportData SD 
 Where ModuleName='Market Size'
 )
Update StagingDownloadsExportData
SET ErrorNotes = ISNULL(cteMarketSize.ProjectNameError,'') + ISNULL(cteMarketSize.ProjectIdError,'')
               + ISNULL(cteMarketSize.ModuleNameError,'') + ISNULL(cteMarketSize.ModuleIdError,'')
               + IsNull(cteMarketSize.HeaderNameError, '') + IsNull(cteMarketSize.HeaderNameLengthError, '')
			   + IsNull(cteMarketSize.FooterNameError, '') + IsNull(cteMarketSize.FooterNameLengthError, '')
			   + IsNull(cteMarketSize.ReportTitleError, '') + IsNull(cteMarketSize.ReportTitleLengthError, '')
               + IsNull(cteMarketSize.CoverPageImageLinkError, '') + IsNull(cteMarketSize.CoverPageImageLinkLengthError, '')
			   + IsNull(cteMarketSize.OverViewNameError, '') + IsNull(cteMarketSize.OverViewLengthError, '')
			   + IsNull(cteMarketSize.KeyPointsError, '') + IsNull(cteMarketSize.KeyPointsLengthError, '')
			   + IsNull(cteMarketSize.MethodologyNameError, '') + IsNull(cteMarketSize.MethodologyLengthError, '')
			   + IsNull(cteMarketSize.AnalystNameError, '')  + IsNull(cteMarketSize.AnalystNameLengthError, '')
			   + IsNull(cteMarketSize.AnalystContactNoError, '')  + IsNull(cteMarketSize.ContactUsOfficeContactNoLengthError, '')
               + IsNull(cteMarketSize.AnalystEmailIdError, '') + IsNull(cteMarketSize.AnalystEmailIdInvalidError, '')
               + IsNull(cteMarketSize.MethodologyImageLinkLengthError, '') 
From cteMarketSize
Inner Join StagingDownloadsExportData sd
On cteMarketSize.stagingid = sd.Id;

IF(EXISTS(SELECT 1 FROM StagingDownloadsExportData WHERE ErrorNotes != ''))   
BEGIN   
Select StagingDownloadsExportData.ModuleName as Module,RowNo, ErrorNotes,CAST(0 AS BIT) AS Success FROM StagingDownloadsExportData WHERE ErrorNotes != '' 
ORDER BY RowNo

DELETE FROM ImportData where Id in (select ImportId from StagingDownloadsExportData sd);    
DELETE FROM StagingDownloadsExportData;

END  
ELSE  
Begin
	Exec SApp_InsertDownloadsExportDataFromStagging;
	DELETE FROM StagingDownloadsExportData;	
	
End 
End try
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 exec SApp_LogErrorInfo
 set @ErrorMsg = 'Error while update Downloads, please contact Admin for more details.';    
SELECT StoredProcedureName ='SApp_UpdateStagingDownloads',Message =@ErrorMsg,Success = 0;
END CATCH              
end