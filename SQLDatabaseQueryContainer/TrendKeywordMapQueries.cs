﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public class TrendKeywordMapQueries
    {
        public static readonly string StoreProcInsert = "SApp_insertupdateTrendkeywords";

        public static readonly string StoreProcUpdate = "SApp_UpdateTrendkeywords";

        public static string StoreProcSelectAllTrendNames = "SApp_GetAllTrendNames";

        public static string StoreProcSelectAll = "SApp_TrendKeywordsGridWithFilters";

        public static string StoreProcSelectByTrendID = "SApp_GetTrendKeywordbyID";

        public static string StoreProcSelectTrendByTrendId = "SApp_GetTrendbyID";
    }
}
