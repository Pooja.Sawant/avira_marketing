﻿CREATE TABLE [dbo].[TrendNote] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [TrendId]          UNIQUEIDENTIFIER NOT NULL,
    [TrendType]        NVARCHAR (512)   NULL,
    [AuthorRemark]     NVARCHAR (512)   NULL,
    [ApproverRemark]   NVARCHAR (512)   NULL,
    [TrendStatusID]    UNIQUEIDENTIFIER NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_TrendNote] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TrendNote_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_TrendNote_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id])
);




 
 
 
 
 
 
 
 
GO
-- =============================================
-- Author:		Praveen
-- Create date: 26-04-2019
-- Description:	Insert History of the TrendNote
--Change History    
--Date  Developer  Reason    
--__________ ____________ ______________________________________________________    
--26/04/2019 Praveen   Initial Version    
-- =============================================
CREATE TRIGGER [dbo].[trTrendNote]
   ON  [dbo].[TrendNote]
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	INSERT INTO [dbo].[TrendNoteHistory]  
     (  
       Id,TrendNoteID,TrendId,TrendType,AuthorRemark,ApproverRemark,TrendStatusID,CreatedOn,UserCreatedById,ModifiedOn,UserModifiedById,IsDeleted,DeletedOn,UserDeletedById
     )
	 SELECT NEWID(), inserted.Id,inserted.TrendId,inserted.TrendType,inserted.AuthorRemark,inserted.ApproverRemark,inserted.TrendStatusID,inserted.CreatedOn,inserted.UserCreatedById,inserted.ModifiedOn,inserted.UserModifiedById,inserted.IsDeleted,inserted.DeletedOn,inserted.UserDeletedById
	 FROM inserted 
	 JOIN deleted ON deleted.ID = inserted.id
	 WHERE deleted.[TrendStatusID] != inserted.[TrendStatusID] 

END
GO
-- =============================================
-- Author:		Praveen
-- Create date: 26-04-2019
-- Description:	Trend Status change history of the TrendNote
--Change History    
--Date  Developer  Reason    
--__________ ____________ ______________________________________________________    
--26/04/2019 Praveen   Initial Version    
-- =============================================
CREATE TRIGGER [dbo].[trTrendNoteUpdateStatusTracking]
   ON  [dbo].[TrendNote]
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	INSERT INTO [dbo].[TrendStatusTracking](Id,TrendNoteID,TrendId,TrendType,TrendStatusID,CreatedOn,UserCreatedById)
	SELECT NEWID(),
	inserted.Id,
	inserted.TrendId,
	inserted.TrendType,
	inserted.TrendStatusID,
	isnull(inserted.ModifiedOn, inserted.CreatedOn),
	isnull(inserted.UserModifiedById, inserted.UserCreatedById)
	FROM inserted 
	JOIN deleted ON deleted.ID = inserted.id
	WHERE deleted.[TrendStatusID] != inserted.[TrendStatusID] 

END