﻿CREATE PROCEDURE [dbo].[SFabChart_GetMarketImpactByTrend]  
(  
	 @ProjectId UniqueIdentifier = null,  
	 @TrendId UniqueIdentifier,   
	 @AvirauserId UniqueIdentifier = null
)  
as  
/*================================================================================  
Procedure Name: [SFabChart_GetMarketImpactByTrend]  
Author: Praveen  
Create date: 22/07/2019  
Description: Get data for market impact chart based on the trend
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
22/07/2019 Praveen      Initial Version  
05/02/2020 Pooja		add new filter for top segment id
================================================================================*/  
BEGIN  
	SET NOCOUNT ON;  
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	declare @TrendApprovedStatusId uniqueidentifier
	declare @TopSegmentID uniqueidentifier
	declare @ValueConversiton varchar(200)

	select @TrendApprovedStatusId = Id
		from TrendStatus
		where TrendStatusName = 'Approved';
	declare @ProjectApprovedStatusId uniqueidentifier
	select @ProjectApprovedStatusId = ID from ProjectStatus
	where ProjectStatusName = 'Completed'

	SELECT DISTINCT TOP (1) @TopSegmentID=MarketValuesummary.SegmentId, @ValueConversiton=ValueConversion.ValueName FROM 
	 MarketValuesummary LEFT  JOIN  ValueConversion ON MarketValuesummary.ValueConversionId = ValueConversion.Id 
	 where Projectid = @ProjectId;


	declare @CustomerId uniqueidentifier
	select @CustomerId = CustomerId from AviraUser where id=@AvirauserId;
	;with MV as (
	select Year,
	sum(dbo.fun_UnitDeConversion(MarketValueSummary.ValueConversionId,value)) as val 
	
	from MarketValueSummary
	where MarketValueSummary.ProjectId = @ProjectID
	and MarketValuesummary.SegmentId=@TopSegmentID
	and exists (select 1 from Project 
				where Project.ProjectStatusID = @ProjectApprovedStatusId
				and project.ID = MarketValueSummary.ProjectId)
	AND
	
	(
		EXISTS(
				SELECT 1 FROM TrendValueMap  WHERE TrendId = @TrendId AND MarketValueSummary.Year = TrendValueMap.Year	
			)
	--OR
	or MarketValueSummary.Year = (SELECT TOP 1 BaseYear FROM BaseYearMaster))
	--
	
	
	-- (MarketValueSummary.Year IN (SELECT Year FROM TrendValueMap  WHERE TrendId = @TrendId) 



	--or MarketValueSummary.Year = (SELECT TOP 1 BaseYear FROM BaseYearMaster))
	group by Year)
--select * from mv

	Select MV.Year, 
	MV.Val As IndustryViewpointMid, 1.3 * MV.Val As  IndustryViewpointBest, 
	0.7 * MV.Val As IndustryViewpointBase,
	IsNull((1 + (TVP.TrendValue / 100)) * MV.Val, MV.Val) as MyViewPoint,
	@ValueConversiton as ValueConversionName
	From MV 
	Left Join TrendBuildMyViewPoint TVP 
	On MV.Year = TVP.Year
	And TrendId = @TrendId
	And TVP.CustomerId=@CustomerId
	And Exists(Select 1 
				From Trend 
				Where Trend.Id = TVP.TrendId 
				And Trend.TrendStatusID = @TrendApprovedStatusId)
	Order By MV.Year
End