﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations
{
    public class ImpactTypeRepository : IImpactTypeRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "ImpactType";
        public ImpactTypeRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }
        public IEnumerable<ImpactTypeViewModel> GetAll()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<ImpactTypeViewModel>(ImpactTypeQueries.StoreProcSelectAll
                            , null
                            , null, true, null, CommandType.StoredProcedure);
        }
        
    }
}
