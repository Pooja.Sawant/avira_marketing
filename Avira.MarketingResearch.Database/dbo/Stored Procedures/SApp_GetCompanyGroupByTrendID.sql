﻿




      
 CREATE PROCEDURE [dbo].[SApp_GetCompanyGroupByTrendID]            
 (@TrendID uniqueidentifier = null,  
 @CompanyGroupName nvarchar(256)='',      
 @includeDeleted bit = 0,        
 @OrdbyByColumnName NVARCHAR(50) ='TrendCompanyGroupID',        
 @SortDirection INT = -1,        
 @PageStart INT = 0,        
 @PageSize INT = null)            
 AS            
/*================================================================================            
Procedure Name: SApp_GetCompanyGroupByTrendID      
Author: Swami            
Create date: 04/16/2019            
Description: Get list from Company Group table by TrendID            
Change History            
Date  Developer  Reason            
__________ ____________ ______________________________________________________            
04/16/2019  Sai Krishna   Initial Version  
================================================================================*/           
            
BEGIN            
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED         
if @PageSize is null or @PageSize =0        
set @PageSize = 10        
 
if isnull(@CompanyGroupName,'') = '' set @CompanyGroupName = '' 
else set @CompanyGroupName = '%'+ @CompanyGroupName +'%';         

begin        
;WITH CTE_TBL AS (        
 SELECT         
 --@TrendID as TrendID,          
   [CompanyGroup].[Id] AS [CompanyGroupId],            
   [CompanyGroup].[CompanyGroupName],           
   [CompanyGroup].[Description],        
   [CompanyGroup].IsDeleted,        
   TrendCompanyGroupMap.Impact as ImpactId,      
   TrendCompanyGroupMap.EndUser,      
   TrendCompanyGroupMap.TrendId,      
   TrendCompanyGroupMap.Id as TrendCompanyGroupMapId,      
  cast (case when exists(select 1 from dbo.TrendCompanyGroupMap         
     where TrendCompanyGroupMap.TrendId = @TrendID        
     and TrendCompanyGroupMap.IsDeleted = 0        
     and TrendCompanyGroupMap.CompanyGroupId = CompanyGroup.Id) then 1 else 0 end as bit) as IsMapped,        
 CASE        
  WHEN @OrdbyByColumnName = 'CompanyGroupName' THEN CompanyGroup.CompanyGroupName        
  WHEN @OrdbyByColumnName = 'Description' THEN [CompanyGroup].[Description] 
  when @OrdbyByColumnName = 'IsMapped' THEN cast(case when TrendCompanyGroupMap.TrendId is null then 1 else 0 end as varchar(2))  
  ELSE [CompanyGroup].CompanyGroupName        
 END AS SortColumn        
        
FROM [dbo].CompanyGroup     
LEFT JOIN dbo.TrendCompanyGroupMap       
ON CompanyGroup.Id = TrendCompanyGroupMap.CompanyGroupId    
and TrendCompanyGroupMap.TrendId = @TrendID
and TrendCompanyGroupMap.IsDeleted = 0        
where (@CompanyGroupName = '' or [CompanyGroup].[CompanyGroupName] LIKE @CompanyGroupName)
AND (@includeDeleted = 1 or CompanyGroup.IsDeleted = 0)        
)               
        
 SELECT         
    CTE_TBL.*,        
    tCountResult.TotalItems        
  FROM CTE_TBL        
  CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult        
  ORDER BY         
    CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,        
    CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC         
  OFFSET @PageStart ROWS        
  FETCH NEXT @PageSize ROWS ONLY;        
  END        
        
END