﻿CREATE TABLE [dbo].[CompanyEcosystemPresenceESMMap] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [CompanyId]        UNIQUEIDENTIFIER NOT NULL,
    [IsChecked]        BIT              NULL,
    [ESMId]            UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CompanyEcosystemPresenceMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CompanyEcosystemPresenceMap_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyEcosystemPresenceMap_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyEcosystemPresenceMap_AviraUser2] FOREIGN KEY ([UserDeletedById]) REFERENCES [dbo].[AviraUser] ([Id])
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the placeholder for SegmentID from Segment Table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyEcosystemPresenceESMMap', @level2type = N'COLUMN', @level2name = N'ESMId';

