﻿
  
CREATE PROCEDURE [dbo].[SApp_GenerateMarketGraphData]   
@ProjectID uniqueidentifier
 AS  
/*================================================================================  
Procedure Name: [SApp_GenerateMarketGraphData]  
Author: Sharath  
Create date: 11/07/2019  
Description: Get list of conversion values from table  
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
11/08/2019 Sharath   Initial Version  
================================================================================*/  
  
BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
declare @p2 dbo.udtUniqueIdentifier
declare @p3 dbo.udtUniqueIdentifier
declare @p4 dbo.udtUniqueIdentifier
declare @p9 nvarchar(100)
declare @MetricType nvarchar(100) 
declare @JsonString table(JsonOutput nvarchar(max))
declare @MetricTypeList table(MetricType nvarchar(50));

declare @SegmentId uniqueidentifier;
insert into @MetricTypeList
select 'Volume' union all
--select 'Growth%' union all
--select 'ASP' union all
select 'Value';

delete [MarketGraphData] 
where ProjectID = @ProjectID;
delete MarketValueSummary
where ProjectID = @ProjectID;

insert into MarketValueSummary(Projectid,
Regionid,
Countryid,
SegmentId,
SubSegmentId,
Year,
Volume,
value)
select mv.Projectid, 
Regionid, 
Countryid,
mvsm.SegmentId,
mvsm.SubSegmentId,
mv.Year,
sum(MV.Volume) as Volume,
sum(MV.value) as value
from MarketValue mv
inner join MarketValueSegmentMapping mvsm
on mv.id= mvsm.MarketValueId
where mv.ProjectId = @ProjectID
group by mv.Projectid, Regionid, Countryid, mvsm.SegmentId,
mvsm.SubSegmentId, mv.Year;

update MarketValueSummary
set ValueConversionId = (select top 1 ID
from ValueConversion vc
where exists (select 1 FROM MarketValue as MV
   WHERE mv.ProjectId =  @ProjectID
and vc.Id = mv.ValueConversionId)
order by vc.Conversion desc),
VolumeConversionId = (select top 1 ID
from ValueConversion vc
where exists (select 1 FROM MarketValue as MV
   WHERE mv.ProjectId =  @ProjectID
and vc.Id = mv.VolumeUnitId)
order by vc.Conversion desc)
where MarketValueSummary.ProjectID  = @ProjectID;


;with cte_marketseg as (SELECT DISTINCT SegmentId 
FROM MarketValueSegmentMapping
WHERE ProjectID=@ProjectID)
insert into [MarketGraphData](Id, ProjectID,SegmentID,MetricType)
select newid(),
@ProjectID, 
SegmentId,
MetricType
from cte_marketseg
cross apply @MetricTypeList list;


DECLARE Segment_cursor CURSOR FOR   
SELECT DISTINCT SegmentId 
FROM MarketValueSegmentMapping
WHERE ProjectID=@ProjectID

OPEN Segment_cursor  
FETCH NEXT FROM Segment_cursor INTO @SegmentId  
  
    WHILE @@FETCH_STATUS = 0  
    BEGIN  
	DECLARE Metric_cursor CURSOR FOR   
	SELECT MetricType
	from @MetricTypeList;
	OPEN Metric_cursor
	FETCH NEXT FROM Metric_cursor INTO @MetricType  
  
    WHILE @@FETCH_STATUS = 0  
    BEGIN  

	delete @JsonString;
	insert into @JsonString
	exec SFab_GetMarketSizingData_BarChart 
	@SegmentID=@SegmentId,
	@SegmentIdList=@p2,
	@RegionIdList=@p3,
	@CountryIdList=@p4,
	@TimeTagId=NULL,
	@CurrencyId=NULL,
	@MatricType=@MetricType,
	@ProjectID=@ProjectID,
	@ValueName=@p9 output,
	@CaptureJson= 1;

	update [MarketGraphData]
	set BarChartJson = (select top 1 JsonOutput from @JsonString)
	from [MarketGraphData]
	where ProjectID = @ProjectID
	and SegmentID = @SegmentId
	and MetricType = @MetricType;

	delete @JsonString;
	insert into @JsonString
	exec [SFab_GetMarketSizingData_Table] 
	@SegmentID=@SegmentId,
	@ProjectID=@ProjectID,
	@SegmentIdList=@p2,
	@RegionIdList=@p3,
	@CountryIdList=@p4,
	@TimeTagId=NULL,
	@CurrencyId=NULL,
	@MatricType=@MetricType,
	@ValueName=@p9 output,
	@CaptureJson= 1;

	update [MarketGraphData]
	set TableJson = (select top 1 JsonOutput from @JsonString)
	from [MarketGraphData]
	where ProjectID = @ProjectID
	and SegmentID = @SegmentId
	and MetricType = @MetricType;
	
	FETCH NEXT FROM Metric_cursor INTO @MetricType  
    END  
  
    CLOSE Metric_cursor  
    DEALLOCATE Metric_cursor
	delete @JsonString;
	insert into @JsonString
	exec [dbo].[SFab_GetMarketSizingData_MapChart]
	 @SegmentID =@SegmentId,
	 @ProjectID =@ProjectID,
 	 @SegmentIdList =@p2,
 	 @RegionIdList =@p3,
 	 @CountryIdList =@p4,
 	 @TimeTagId = null,
 	 @CurrencyId = null,
	@CaptureJson = 1;
	 update [MarketGraphData]
	set MapJson = (select top 1 JsonOutput from @JsonString)
	from [MarketGraphData]
	where ProjectID = @ProjectID
	and SegmentID = @SegmentId;

    FETCH NEXT FROM Segment_cursor INTO @SegmentId  
    END  
  
    CLOSE Segment_cursor  
    DEALLOCATE Segment_cursor  
	
END