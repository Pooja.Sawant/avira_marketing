﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.Trend
{
    public interface ITrendEcoSystemMapRepository
    {
        IEnumerable<TrendEcoSystemMapViewModel> GetAll(Guid? Id,int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false);
        Result<SpTransactionMessage> InsertUpdate(MainTrendEcoSystemMapInsertUpdateDbViewModel trendEcoSystemMapUpdateDbViewModel);
    }
}
