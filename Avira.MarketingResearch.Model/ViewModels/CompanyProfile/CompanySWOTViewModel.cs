﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.CompanyProfile
{
    public class CompanySWOTOpportunitiesViewModel
    {
        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
        public Guid ProjectId { get; set; }
        public Guid CategoryId { get; set; }
        public Guid SWOTTypeId { get; set; }
        public string SWOTTypeName { get; set; }
        public string SWOTDescription { get; set; } 
        public string RowType
        {
            get
            {
                return "CompanySWOTOpportunities";
            }
        }
    }
    public class CompanySWOTStrengthViewModel
    {
        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
        public Guid ProjectId { get; set; }
        public Guid CategoryId { get; set; }
        public Guid SWOTTypeId { get; set; }
        public string SWOTTypeName { get; set; }
        public string SWOTDescription { get; set; }
        public string RowType
        {
            get
            {
                return "CompanySWOTStrength";
            }
        }
    }
    public class CompanySWOTThreatsViewModel
    {
        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
        public Guid ProjectId { get; set; }
        public Guid CategoryId { get; set; }
        public Guid SWOTTypeId { get; set; }
        public string SWOTTypeName { get; set; }
        public string SWOTDescription { get; set; }
        public string RowType
        {
            get
            {
                return "CompanySWOTThreats";
            }
        }
    }
    public class CompanySWOTWeeknessesViewModel
    {
        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
        public Guid ProjectId { get; set; }
        public Guid CategoryId { get; set; }
        public Guid SWOTTypeId { get; set; }
        public string SWOTTypeName { get; set; }
        public string SWOTDescription { get; set; }
        public string RowType
        {
            get
            {
                return "CompanySWOTWeeknesses";
            }
        }
    }

    public class CompanySWOTViewModel
    {
        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
        public Guid ProjectId { get; set; }
        public Guid CategoryId { get; set; }
        public Guid SWOTTypeId { get; set; }
        public string SWOTTypeName { get; set; }
        public string SWOTDescription { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public string RowType
        {
            get
            {
                return "CompanySWOTWeeknesses";
            }
        }
    }

    public class MainCompanySWOTViewModel
    {
        public Guid CompanyId { get; set; }
        public Guid ProjectId { get; set; }
        public List<CompanySWOTOpportunitiesViewModel> companySWOTOpportunitiesViewModel { get; set; }
        public List<CompanySWOTStrengthViewModel> companySWOTStrengthViewModel { get; set; }
        public List<CompanySWOTThreatsViewModel> companySWOTThreatsViewModel { get; set; }
        public List<CompanySWOTWeeknessesViewModel> companySWOTWeeknessesViewModel { get; set; }
        public List<CompanySWOTViewModel> companySWOTViewModel { get; set; }
        public CompanyNoteViewModel CompanyNoteViewModel { get; set; }
    }
}
