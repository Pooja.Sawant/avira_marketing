﻿

CREATE PROCEDURE [dbo].[SFab_GetAllNewsCategory]
(
	@includeDeleted bit = 0,
	@AviraUserID uniqueidentifier = null
)
	AS
/*================================================================================
Procedure Name: [dbo].[SFab_GetAllNewsCategory]
Author: Pooja Sawant
Create date: 05/21/2019
Description: Get all list NewsCategory table
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
03/21/2019	Pooja			Initial Version
================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SELECT 	*	FROM [dbo].[NewsCategory]
where  ([NewsCategory].IsDeleted = 0)
order by NewsCategoryName asc
END