﻿
  
CREATE PROCEDURE [dbo].[SApp_GetMakeMyMarketImpactDetailsByIds]
(
	@CustomerId UNIQUEIDENTIFIER,
	@ProjectId UNIQUEIDENTIFIER,
	@TrendId UNIQUEIDENTIFIER
)  
AS  
/*================================================================================  
Procedure Name: [SApp_GetMakeMyMarketImpactDetailsByIds]  
Author: Swami  
Create date: 27/07/2019  
Description: Get list of Make My Market Impact from table   
Change History  
Date        Developer     Reason  
__________  ____________  ______________________________________________________  
27/07/2019  Swami   Initial Version  
================================================================================*/  

BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

;WITH MarketIdList as (

  SELECT MM.Id AS ID  FROM MakeMyMarket AS MM
  WHERE MM.CustomerId = @CustomerId 
		AND MM.ProjectId = @ProjectId
		AND MM.TrendId = @TrendId
		AND MM.IsDeleted = 0
)


SELECT MI.[Id]
      ,MI.[MakeMyMarketId]
      ,MI.[ProjectId]
      ,MI.[CustomerId]
      ,MI.[MarketMoverType]
      ,MI.[EcoSystemType]
      ,MI.[MetricType]
      ,MI.[Year]
      ,MI.[Value]
      ,MI.[CAGR]

FROM MakeMyMarketImpact MI
--INNER JOIN MakeMyMarket MM ON MI.MakeMyMarketId = MM.Id
INNER JOIN MarketIdList ML ON MI.MakeMyMarketId = ML.ID
WHERE MI.ProjectId = @ProjectId 
AND MI.CustomerId = @CustomerId
ORDER BY YEAR

END