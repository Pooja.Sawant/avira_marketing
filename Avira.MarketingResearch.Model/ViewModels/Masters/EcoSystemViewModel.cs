﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Avira.MarketingResearch.Model.ViewModels.Masters
{
    public class EcoSystemViewModel
    {
      
        public string EcoSystemName { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public Guid Id { get; set; }
        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "EcoSystem";
            }
        }

    }

    public class EcoSystemInsertDbViewModel
    {
        [Required]
        [Display(Name = "EcoSystem Name")]
        public string EcoSystemName { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public Guid Id { get; set; }
    }

    public class EcoSystemUpdateDbViewModel
    {
        [Required]
        [Display(Name = "EcoSystem Name")]
        public string EcoSystemName { get; set; }
        public string Description { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; } = false;
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        [Required]
        public Guid Id { get; set; }
    }


}
