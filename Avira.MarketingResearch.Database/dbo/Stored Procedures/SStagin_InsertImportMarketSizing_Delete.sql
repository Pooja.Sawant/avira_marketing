﻿CREATE PROCEDURE [dbo].[SStagin_InsertImportMarketSizing_Delete] 
@ImportId UNIQUEIDENTIFIER,         
@UserCreatedById UNIQUEIDENTIFIER
AS            
/*================================================================================                
Procedure Name: SStagin_InsertImportMarketSizing_Delete               
Change History                
Date  Developer  Reason                
__________ ____________ ______________________________________________________                
10/21/2019 Praveen   Initial Version                
================================================================================*/
declare @ErrorMsg NVARCHAR(2048),@CreatedOn Datetime            
BEGIN            
 SET @CreatedOn = GETDATE()            
 BEGIN TRY               
-- INSERT INTO ImportData            
-- (Id               ,             
--TemplateName    ,            
--ImportFileName    ,            
----ImportStatusId    ,            
----ImportException    ,            
--ImportedOn     ,            
--UserImportedById)            
--VALUES(@ImportId, @TemplateName, @ImportFileName,@CreatedOn, @UserCreatedById)            
---------------------------------------------------            

DELETE FROM StagingMarketSizing WHERE ImportId = @ImportId

            
 SELECT StoredProcedureName ='SStagin_InsertImportMarketSizing',Message =@ErrorMsg,Success = 1;               
END TRY                
BEGIN CATCH                
    -- Execute the error retrieval routine.                
 DECLARE @ErrorNumber int;                
 DECLARE @ErrorSeverity int;                
 DECLARE @ErrorProcedure varchar(100);                
 DECLARE @ErrorLine int;                
 DECLARE @ErrorMessage varchar(500);                
                
  SELECT @ErrorNumber = ERROR_NUMBER(),                
        @ErrorSeverity = ERROR_SEVERITY(),                
        @ErrorProcedure = ERROR_PROCEDURE(),                
        @ErrorLine = ERROR_LINE(),                
        @ErrorMessage = ERROR_MESSAGE()                
                
 insert into dbo.Errorlog(ErrorNumber,                
       ErrorSeverity,                
       ErrorProcedure,                
       ErrorLine,                
       ErrorMessage,                
       ErrorDate)                
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,@CreatedOn);                
              
 set @ErrorMsg = 'Error while delete marketSizing staging, please contact Admin for more details.';                
--THROW 50000,@ErrorMsg,1;                  
--return(1)              
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine              
    ,Message =@ErrorMsg,Success = 0;                 
                 
END CATCH                 
            
            
END