﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService
{
    public interface ICompanyService
    {
        IEnumerable<CompanyViewModel> GetAll(Guid Id);
        IEnumerable<CompanyViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false);
        CompanyViewModel GetById(Guid guid);

        Result<SpTransactionMessage> Create(CompanyInsertDbViewModel tfc, AviraUser currentUser);
        Result<SpTransactionMessage> Update(CompanyUpdateDbViewModel tfc, AviraUser currenUser);

    }
}
