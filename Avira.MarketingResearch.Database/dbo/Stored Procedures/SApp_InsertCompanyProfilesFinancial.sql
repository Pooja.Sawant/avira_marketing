﻿                  
CREATE PROCEDURE [dbo].[SApp_InsertCompanyProfilesFinancial]                
AS                
/*================================================================================                    
Procedure Name: SApp_InsertCompanyProfilesFinancial                   
Change History                    
Date  Developer  Reason                    
__________ ____________ ______________________________________________________                    
07/25/2019 Gopi   Initial Version                    
================================================================================*/                 
            
DECLARE @Id uniqueidentifier,  
	@CompanyId uniqueidentifier,
	@ProjectId uniqueidentifier=null,
	@CompanyType NCHAR(512),  
	@AuthorRemark NVARCHAR(512) = null,  
	@ApproverRemark NVARCHAR(512) = null,  
	@CompanyStatusName VARCHAR(512),    
	@AviraUserId uniqueidentifier,           
	@ErrorMsg NVARCHAR(2048),
	@CreatedOn Datetime,
	@Today datetime = getdate()
	
	Declare @USDCurrencyId uniqueidentifier	
	Set @USDCurrencyId = dbo.[fun_GetCurrencyIDForUSD]()
                
BEGIN
	BEGIN TRY    
	SELECT TOP 1 @CompanyId = CompanyId FROM StagingCompanyProfilesFinancial
	SET @ApproverRemark = 'NA'
	SET @AuthorRemark = 'NA'
	SET @CompanyStatusName = 'Approved'  

	--SELECT FinanicialYear,FinanicialYear,
	--* FROM StagingCompanyProfilesFinancial

	--Delete OldMapping Records.  
	--Delete from [dbo].[CompanyProfilesFinancial]    
	--Where [CompanyProfilesFinancial].[CompanyId] in     
	--(select SCPF.CompanyId  
	--from StagingCompanyProfilesFinancial SCPF)  
	--------------------PLStatement------------------------------------------  

	UPDATE StagingCompanyProfilesFinancial SET FinanicialYear = (SELECT TOP 1 value FROM STRING_SPLIT (FinanicialYear, '-'))
	UPDATE StagingCompanySegmentInformation SET FinanicialYear = (SELECT TOP 1 value FROM STRING_SPLIT (FinanicialYear, '-'))
	DELETE CPLS
	FROM CompanyPLStatement CPLS
	INNER JOIN StagingCompanyProfilesFinancial StagCPLS
	ON --CPLS.Year = CAST(StagCPLS.FinanicialYear AS int)  AND 
	CPLS.CompanyId = StagCPLS.CompanyId
	------------------BalanceSheet------------------------------------------
	DELETE CPBS
	FROM CompanyBalanceSheet CPBS
	INNER JOIN StagingCompanyProfilesFinancial StagCPLS
	ON --CPBS.Year = CAST(StagCPLS.FinanicialYear AS int) AND 
	CPBS.CompanyId = StagCPLS.CompanyId
	------------------CompanyCashFlowStatement------------------------------------------
	DELETE CPCFS
	FROM CompanyCashFlowStatement CPCFS
	INNER JOIN StagingCompanyProfilesFinancial StagCPLS
	ON --CPCFS.Year = CAST(StagCPLS.FinanicialYear AS int) AND 
	CPCFS.CompanyId = StagCPLS.CompanyId
	------------------CompanySegmentInformation------------------------------------------
	DELETE CPSI
	FROM CompanySegmentInformation CPSI
	INNER JOIN StagingCompanyProfilesFinancial StagCPLS
	ON --CPSI.Year =CAST(StagCPLS.FinanicialYear AS int) AND 
	CPSI.CompanyId = StagCPLS.CompanyId
	--------------------------------------------------------------
     
;with cte_CompanyPLStatement as(              
Select  --CAST(Revenues_2018 AS decimal(19,4)) as Revenues, 
 CompanyId,  
 CurrencyId,  
 ValueConversionId,              
 CompanyName,               
 Currency,              
 Units,
CAST(FinanicialYear AS INT) FinanicialYear,
CAST(CASE WHEN ISNUMERIC(Revenue) = '0' THEN '0' ELSE Revenue END AS decimal(19,4)) AS Revenue,
CAST(CASE WHEN ISNUMERIC(CostsAndExpenses) = '0' THEN '0' ELSE CostsAndExpenses END AS decimal(19,4)) AS CostsAndExpenses,
CAST(CASE WHEN ISNUMERIC(CostOfProduction) = '0' THEN '0' ELSE CostOfProduction END AS decimal(19,4)) AS CostOfProduction,
CAST(CASE WHEN ISNUMERIC(ResourceCost) = '0' THEN '0' ELSE ResourceCost END AS decimal(19,4)) AS ResourceCost,
CAST(CASE WHEN ISNUMERIC(SalaryCost) = '0' THEN '0' ELSE SalaryCost END AS decimal(19,4)) AS SalaryCost,
CAST(CASE WHEN ISNUMERIC(CostOfSales) = '0' THEN '0' ELSE CostOfSales END AS decimal(19,4)) AS CostOfSales,
CAST(CASE WHEN ISNUMERIC(SellingExpenses) = '0' THEN '0' ELSE SellingExpenses END AS decimal(19,4)) AS SellingExpenses,
CAST(CASE WHEN ISNUMERIC(MarketingExpenses) = '0' THEN '0' ELSE MarketingExpenses END AS decimal(19,4)) AS MarketingExpenses,
CAST(CASE WHEN ISNUMERIC(AdministrativeExpenses) = '0' THEN '0' ELSE AdministrativeExpenses END AS decimal(19,4)) AS AdministrativeExpenses,
CAST(CASE WHEN ISNUMERIC(GeneralExpenses) = '0' THEN '0' ELSE GeneralExpenses END AS decimal(19,4)) AS GeneralExpenses,
CAST(CASE WHEN ISNUMERIC(SellingGeneralAndAdminExpenses) = '0' THEN '0' ELSE SellingGeneralAndAdminExpenses END AS decimal(19,4)) AS SellingGeneralAndAdminExpenses,
CAST(CASE WHEN ISNUMERIC(ResearchAndDevelopmentExpenses) = '0' THEN '0' ELSE ResearchAndDevelopmentExpenses END AS decimal(19,4)) AS ResearchAndDevelopmentExpenses,
CAST(CASE WHEN ISNUMERIC(Depreciation) = '0' THEN '0' ELSE Depreciation END AS decimal(19,4)) AS Depreciation,
CAST(CASE WHEN ISNUMERIC(Amortization) = '0' THEN '0' ELSE Amortization END AS decimal(19,4)) AS Amortization,
CAST(CASE WHEN ISNUMERIC(Restructuring) = '0' THEN '0' ELSE Restructuring END AS decimal(19,4)) AS Restructuring,
CAST(CASE WHEN ISNUMERIC(OtherIncomeOrDeductions) = '0' THEN '0' ELSE OtherIncomeOrDeductions END AS decimal(19,4)) AS OtherIncomeOrDeductions,
CAST(CASE WHEN ISNUMERIC(EBIT) = '0' THEN '0' ELSE EBIT END AS decimal(19,4)) AS EBIT,
CAST(CASE WHEN ISNUMERIC(InterestIncome) = '0' THEN '0' ELSE InterestIncome END AS decimal(19,4)) AS InterestIncome,
CAST(CASE WHEN ISNUMERIC(InterestExpense) = '0' THEN '0' ELSE InterestExpense END AS decimal(19,4)) AS InterestExpense,
CAST(CASE WHEN ISNUMERIC(EBT) = '0' THEN '0' ELSE EBT END AS decimal(19,4)) AS EBT,
CAST(CASE WHEN ISNUMERIC(Tax) = '0' THEN '0' ELSE Tax END AS decimal(19,4)) AS Tax,
CAST(CASE WHEN ISNUMERIC(NetIncomeFromContinuingOperations) = '0' THEN '0' ELSE NetIncomeFromContinuingOperations END AS decimal(19,4)) AS NetIncomeFromContinuingOperations,
CAST(CASE WHEN ISNUMERIC(DiscontinuedOperations) = '0' THEN '0' ELSE DiscontinuedOperations END AS decimal(19,4)) AS DiscontinuedOperations,
CAST(CASE WHEN ISNUMERIC(IncomeLossFromDiscontinuedOperations) = '0' THEN '0' ELSE IncomeLossFromDiscontinuedOperations END AS decimal(19,4)) AS IncomeLossFromDiscontinuedOperations,
CAST(CASE WHEN ISNUMERIC(GainOnDisposalOfDiscontinuedOperations) = '0' THEN '0' ELSE GainOnDisposalOfDiscontinuedOperations END AS decimal(19,4)) AS GainOnDisposalOfDiscontinuedOperations,
CAST(CASE WHEN ISNUMERIC(DiscontinuedOperationsNetOfTax) = '0' THEN '0' ELSE DiscontinuedOperationsNetOfTax END AS decimal(19,4)) AS DiscontinuedOperationsNetOfTax,
CAST(CASE WHEN ISNUMERIC(NetIncomeBeforeAllocationToNoncontrollingInterests) = '0' THEN '0' ELSE NetIncomeBeforeAllocationToNoncontrollingInterests END AS decimal(19,4)) AS NetIncomeBeforeAllocationToNoncontrollingInterests,
CAST(CASE WHEN ISNUMERIC(NonControllingInterests) = '0' THEN '0' ELSE NonControllingInterests END AS decimal(19,4)) AS NonControllingInterests,
CAST(CASE WHEN ISNUMERIC(NetIncome) = '0' THEN '0' ELSE NetIncome END AS decimal(19,4)) AS NetIncome,
CAST(CASE WHEN ISNUMERIC(BasicEPS_) = '0' THEN '0' ELSE BasicEPS_ END AS decimal(19,4)) AS BasicEPS_,
CAST(CASE WHEN ISNUMERIC(BasicEPSFromContinuingOperations) = '0' THEN '0' ELSE BasicEPSFromContinuingOperations END AS decimal(19,4)) AS BasicEPSFromContinuingOperations,
CAST(CASE WHEN ISNUMERIC(BasicEPSFromDiscontinuedOperations) = '0' THEN '0' ELSE BasicEPSFromDiscontinuedOperations END AS decimal(19,4)) AS BasicEPSFromDiscontinuedOperations,
CAST(CASE WHEN ISNUMERIC(BasicEPS) = '0' THEN '0' ELSE BasicEPS END AS decimal(19,4)) AS BasicEPS,
CAST(CASE WHEN ISNUMERIC(DilutedEPS_) = '0' THEN '0' ELSE DilutedEPS_ END AS decimal(19,4)) AS DilutedEPS_,
CAST(CASE WHEN ISNUMERIC(DilutedEPSFromContinuingOperations) = '0' THEN '0' ELSE DilutedEPSFromContinuingOperations END AS decimal(19,4)) AS DilutedEPSFromContinuingOperations,
CAST(CASE WHEN ISNUMERIC(DilutedEPSFromDiscontinuedOperations) = '0' THEN '0' ELSE DilutedEPSFromDiscontinuedOperations END AS decimal(19,4)) AS DilutedEPSFromDiscontinuedOperations,
CAST(CASE WHEN ISNUMERIC(DilutedEPS) = '0' THEN '0' ELSE DilutedEPS END AS decimal(19,4)) AS DilutedEPS,
CAST(CASE WHEN ISNUMERIC(WeightedAverageSharesOutstandingBasic) = '0' THEN '0' ELSE WeightedAverageSharesOutstandingBasic END AS decimal(19,4)) AS WeightedAverageSharesOutstandingBasic,
CAST(CASE WHEN ISNUMERIC(WeightedAverageSharesOutstandingDiluted) = '0' THEN '0' ELSE WeightedAverageSharesOutstandingDiluted END AS decimal(19,4)) AS WeightedAverageSharesOutstandingDiluted,

CAST(CASE WHEN ISNUMERIC(GrossProfit) = '0' THEN '0' ELSE GrossProfit END AS decimal(19,4)) AS GrossProfit,
CAST(CASE WHEN ISNUMERIC(TotalOperatingExpenses) = '0' THEN '0' ELSE TotalOperatingExpenses END AS decimal(19,4)) AS TotalOperatingExpenses,
CAST(CASE WHEN ISNUMERIC(EBITDA) = '0' THEN '0' ELSE EBITDA END AS decimal(19,4)) AS EBITDA
   ,UserCreatedById
 from StagingCompanyProfilesFinancial              
)        
--SELECT * FROM cte_CompanyPLStatement

insert into CompanyPLStatement              
(              
 Id,
CompanyId,
CurrencyId,
ValueConversionId,
CompanyName,
Currency,
Units,
Year,
Revenue,
CostsAndExpenses,
CostOfProduction,
ResourceCost,
SalaryCost,
CostOfSales,
SellingExpenses,
MarketingExpenses,
AdministrativeExpenses,
GeneralExpenses,
SellingGeneralAndAdminExpenses,
ResearchAndDevelopmentExpenses,
Depreciation,
Amortization,
Restructuring,
OtherIncomeOrDeductions,
EBIT,
InterestIncome,
InterestExpense,
EBT,
Tax,
NetIncomeFromContinuingOperations,
DiscontinuedOperations,
IncomeLossFromDiscontinuedOperations,
GainOnDisposalOfDiscontinuedOperations,
DiscontinuedOperationsNetOfTax,
NetIncomeBeforeAllocationToNoncontrollingInterests,
NonControllingInterests,
NetIncome,
BasicEPS_,
BasicEPSFromContinuingOperations,
BasicEPSFromDiscontinuedOperations,
BasicEPS,
DilutedEPS_,
DilutedEPSFromContinuingOperations,
DilutedEPSFromDiscontinuedOperations,
DilutedEPS,
WeightedAverageSharesOutstandingBasic,
WeightedAverageSharesOutstandingDiluted,  
GrossProfit,
TotalOperatingExpenses,
EBITDA,          
 CreatedOn,              
 UserCreatedById,              
 IsDeleted              
 )              
              
 Select NEWID(),           
	CompanyId,           
	CurrencyId,              
	ValueConversionId,            
	CompanyName,             
	Currency,              
	Units,              
	FinanicialYear,
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, Revenue), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, CostsAndExpenses), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, CostOfProduction), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, ResourceCost), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, SalaryCost), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, CostOfSales), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, SellingExpenses), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, MarketingExpenses), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, AdministrativeExpenses), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, GeneralExpenses), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, SellingGeneralAndAdminExpenses), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, ResearchAndDevelopmentExpenses), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, Depreciation), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, Amortization), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, Restructuring), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, OtherIncomeOrDeductions), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, EBIT), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, InterestIncome), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, InterestExpense), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, EBT), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, Tax), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, NetIncomeFromContinuingOperations), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, DiscontinuedOperations), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, IncomeLossFromDiscontinuedOperations), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, GainOnDisposalOfDiscontinuedOperations), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, DiscontinuedOperationsNetOfTax), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, NetIncomeBeforeAllocationToNoncontrollingInterests), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, NonControllingInterests), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, NetIncome), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, BasicEPS_), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, BasicEPSFromContinuingOperations), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, BasicEPSFromDiscontinuedOperations), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, BasicEPS), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, DilutedEPS_), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, DilutedEPSFromContinuingOperations), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, DilutedEPSFromDiscontinuedOperations), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, DilutedEPS), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, WeightedAverageSharesOutstandingBasic), CurrencyId, @USDCurrencyId, @Today),
	WeightedAverageSharesOutstandingDiluted, 
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, GrossProfit), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, TotalOperatingExpenses), CurrencyId, @USDCurrencyId, @Today),
	[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, EBITDA), CurrencyId, @USDCurrencyId, @Today),
 GETDATE(),              
 UserCreatedById,      
 0               
 from cte_CompanyPLStatement;
    
;with cte_CompanyBalanceSheet as(              
 Select  --CAST(Revenues_2018 AS decimal(19,4)) as Revenues, 
 CompanyId,  
 CurrencyId,  
 ValueConversionId,              
 CompanyName,               
 Currency,              
 Units,
CAST(FinanicialYear AS INT) FinanicialYear,
CAST(CASE WHEN ISNUMERIC(Assets_) = '0' THEN '0' ELSE Assets_ END AS decimal(19,4)) AS Assets_,
CAST(CASE WHEN ISNUMERIC(CashAndCashEquivalents) = '0' THEN '0' ELSE CashAndCashEquivalents END AS decimal(19,4)) AS CashAndCashEquivalents,
CAST(CASE WHEN ISNUMERIC(ShortTermInvestments) = '0' THEN '0' ELSE ShortTermInvestments END AS decimal(19,4)) AS ShortTermInvestments,
CAST(CASE WHEN ISNUMERIC(AccountsReceivables) = '0' THEN '0' ELSE AccountsReceivables END AS decimal(19,4)) AS AccountsReceivables,
CAST(CASE WHEN ISNUMERIC(Inventories) = '0' THEN '0' ELSE Inventories END AS decimal(19,4)) AS Inventories,
CAST(CASE WHEN ISNUMERIC(CurrentTaxAssets) = '0' THEN '0' ELSE CurrentTaxAssets END AS decimal(19,4)) AS CurrentTaxAssets,
CAST(CASE WHEN ISNUMERIC(OtherCurrentAssets) = '0' THEN '0' ELSE OtherCurrentAssets END AS decimal(19,4)) AS OtherCurrentAssets,
CAST(CASE WHEN ISNUMERIC(AssetsHeldForSale) = '0' THEN '0' ELSE AssetsHeldForSale END AS decimal(19,4)) AS AssetsHeldForSale,
CAST(CASE WHEN ISNUMERIC(TotalCurrentAssets) = '0' THEN '0' ELSE TotalCurrentAssets END AS decimal(19,4)) AS TotalCurrentAssets,
CAST(CASE WHEN ISNUMERIC(LongTermInvestments) = '0' THEN '0' ELSE LongTermInvestments END AS decimal(19,4)) AS LongTermInvestments,
CAST(CASE WHEN ISNUMERIC(PropertyPlantAndEquipment) = '0' THEN '0' ELSE PropertyPlantAndEquipment END AS decimal(19,4)) AS PropertyPlantAndEquipment,
CAST(CASE WHEN ISNUMERIC(IntangibleAssets) = '0' THEN '0' ELSE IntangibleAssets END AS decimal(19,4)) AS IntangibleAssets,
CAST(CASE WHEN ISNUMERIC(Goodwill) = '0' THEN '0' ELSE Goodwill END AS decimal(19,4)) AS Goodwill,
CAST(CASE WHEN ISNUMERIC(DeferredTaxAssets) = '0' THEN '0' ELSE DeferredTaxAssets END AS decimal(19,4)) AS DeferredTaxAssets,
CAST(CASE WHEN ISNUMERIC(OtherNoncurrentAssets) = '0' THEN '0' ELSE OtherNoncurrentAssets END AS decimal(19,4)) AS OtherNoncurrentAssets,
CAST(CASE WHEN ISNUMERIC(TotalAssets) = '0' THEN '0' ELSE TotalAssets END AS decimal(19,4)) AS TotalAssets,
CAST(CASE WHEN ISNUMERIC(LiabilitiesAndEquity_) = '0' THEN '0' ELSE LiabilitiesAndEquity_ END AS decimal(19,4)) AS LiabilitiesAndEquity_,
CAST(CASE WHEN ISNUMERIC(ShortTermDebt) = '0' THEN '0' ELSE ShortTermDebt END AS decimal(19,4)) AS ShortTermDebt,
CAST(CASE WHEN ISNUMERIC(TradeAccountsPayable) = '0' THEN '0' ELSE TradeAccountsPayable END AS decimal(19,4)) AS TradeAccountsPayable,
CAST(CASE WHEN ISNUMERIC(DividendsPayable) = '0' THEN '0' ELSE DividendsPayable END AS decimal(19,4)) AS DividendsPayable,
CAST(CASE WHEN ISNUMERIC(IncomeTaxesPayable) = '0' THEN '0' ELSE IncomeTaxesPayable END AS decimal(19,4)) AS IncomeTaxesPayable,
CAST(CASE WHEN ISNUMERIC(AccruedExpenses) = '0' THEN '0' ELSE AccruedExpenses END AS decimal(19,4)) AS AccruedExpenses,
CAST(CASE WHEN ISNUMERIC(OtherCurrentLiabilities) = '0' THEN '0' ELSE OtherCurrentLiabilities END AS decimal(19,4)) AS OtherCurrentLiabilities,
CAST(CASE WHEN ISNUMERIC(LiabilitiesHeldForSale) = '0' THEN '0' ELSE LiabilitiesHeldForSale END AS decimal(19,4)) AS LiabilitiesHeldForSale,
CAST(CASE WHEN ISNUMERIC(TotalCurrentLiabilities) = '0' THEN '0' ELSE TotalCurrentLiabilities END AS decimal(19,4)) AS TotalCurrentLiabilities,
CAST(CASE WHEN ISNUMERIC(LongTermDebt) = '0' THEN '0' ELSE LongTermDebt END AS decimal(19,4)) AS LongTermDebt,
CAST(CASE WHEN ISNUMERIC(PensionBenefitObligations) = '0' THEN '0' ELSE PensionBenefitObligations END AS decimal(19,4)) AS PensionBenefitObligations,
CAST(CASE WHEN ISNUMERIC(PostretirementBenefitObligations) = '0' THEN '0' ELSE PostretirementBenefitObligations END AS decimal(19,4)) AS PostretirementBenefitObligations,
CAST(CASE WHEN ISNUMERIC(DeferredTaxLiabilities) = '0' THEN '0' ELSE DeferredTaxLiabilities END AS decimal(19,4)) AS DeferredTaxLiabilities,
CAST(CASE WHEN ISNUMERIC(OtherTaxesPayable) = '0' THEN '0' ELSE OtherTaxesPayable END AS decimal(19,4)) AS OtherTaxesPayable,
CAST(CASE WHEN ISNUMERIC(OtherNoncurrentLliabilities) = '0' THEN '0' ELSE OtherNoncurrentLliabilities END AS decimal(19,4)) AS OtherNoncurrentLliabilities,
CAST(CASE WHEN ISNUMERIC(TotalLiabilities) = '0' THEN '0' ELSE TotalLiabilities END AS decimal(19,4)) AS TotalLiabilities,
CAST(CASE WHEN ISNUMERIC(CommitmentsAndContingencies_) = '0' THEN '0' ELSE CommitmentsAndContingencies_ END AS decimal(19,4)) AS CommitmentsAndContingencies_,
CAST(CASE WHEN ISNUMERIC(PreferredStock) = '0' THEN '0' ELSE PreferredStock END AS decimal(19,4)) AS PreferredStock,
CAST(CASE WHEN ISNUMERIC(CommonStock) = '0' THEN '0' ELSE CommonStock END AS decimal(19,4)) AS CommonStock,
CAST(CASE WHEN ISNUMERIC(Debentures) = '0' THEN '0' ELSE Debentures END AS decimal(19,4)) AS Debentures,
CAST(CASE WHEN ISNUMERIC(AdditionalPaidInCapital) = '0' THEN '0' ELSE AdditionalPaidInCapital END AS decimal(19,4)) AS AdditionalPaidInCapital,
CAST(CASE WHEN ISNUMERIC(TresuryStock) = '0' THEN '0' ELSE TresuryStock END AS decimal(19,4)) AS TresuryStock,
CAST(CASE WHEN ISNUMERIC(RetainedEarnings) = '0' THEN '0' ELSE RetainedEarnings END AS decimal(19,4)) AS RetainedEarnings,
CAST(CASE WHEN ISNUMERIC(AccumulatedOtherComprehensiveLoss) = '0' THEN '0' ELSE AccumulatedOtherComprehensiveLoss END AS decimal(19,4)) AS AccumulatedOtherComprehensiveLoss,
CAST(CASE WHEN ISNUMERIC(TotalShareholdersEquity) = '0' THEN '0' ELSE TotalShareholdersEquity END AS decimal(19,4)) AS TotalShareholdersEquity,
CAST(CASE WHEN ISNUMERIC(MinorityInterest) = '0' THEN '0' ELSE MinorityInterest END AS decimal(19,4)) AS MinorityInterest,
CAST(CASE WHEN ISNUMERIC(TotalEquity) = '0' THEN '0' ELSE TotalEquity END AS decimal(19,4)) AS TotalEquity,
CAST(CASE WHEN ISNUMERIC(TotalLiabilitiesAndEquity) = '0' THEN '0' ELSE TotalLiabilitiesAndEquity END AS decimal(19,4)) AS TotalLiabilitiesAndEquity,
CAST(CASE WHEN ISNUMERIC(DeferredRevenuesShortTerm) = '0' THEN '0' ELSE DeferredRevenuesShortTerm END AS decimal(19,4)) AS DeferredRevenuesShortTerm,
CAST(CASE WHEN ISNUMERIC(DeferredRevenuesLongTerm) = '0' THEN '0' ELSE DeferredRevenuesLongTerm END AS decimal(19,4)) AS DeferredRevenuesLongTerm,
UserCreatedById
 from StagingCompanyProfilesFinancial              
)        
--SELECT * FROM cte_CompanyPLStatement

insert into CompanyBalanceSheet              
(              
 Id,
CompanyId,
CurrencyId,
ValueConversionId,
CompanyName,
Currency,
Units,
Year,
Assets_,
CashAndCashEquivalents,
ShortTermInvestments,
AccountsReceivables,
Inventories,
CurrentTaxAssets,
OtherCurrentAssets,
AssetsHeldForSale,
TotalCurrentAssets,
LongTermInvestments,
PropertyPlantAndEquipment,
IntangibleAssets,
Goodwill,
DeferredTaxAssets,
OtherNoncurrentAssets,
TotalAssets,
LiabilitiesAndEquity_,
ShortTermDebt,
TradeAccountsPayable,
DividendsPayable,
IncomeTaxesPayable,
AccruedExpenses,
OtherCurrentLiabilities,
LiabilitiesHeldForSale,
TotalCurrentLiabilities,
LongTermDebt,
PensionBenefitObligations,
PostretirementBenefitObligations,
DeferredTaxLiabilities,
OtherTaxesPayable,
OtherNoncurrentLliabilities,
TotalLiabilities,
CommitmentsAndContingencies_,
PreferredStock,
CommonStock,
Debentures,
AdditionalPaidInCapital,
TresuryStock,
RetainedEarnings,
AccumulatedOtherComprehensiveLoss,
TotalShareholdersEquity,
MinorityInterest,
TotalEquity,
TotalLiabilitiesAndEquity,   
DeferredRevenuesShortTerm,
DeferredRevenuesLongTerm  ,       
 CreatedOn,              
 UserCreatedById,              
 IsDeleted              
 )              
              
	Select NEWID(),           
		CompanyId,           
		CurrencyId,              
		ValueConversionId,            
		CompanyName,             
		Currency,              
		Units,              
		FinanicialYear,              
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, Assets_), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, CashAndCashEquivalents), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, ShortTermInvestments), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, AccountsReceivables), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, Inventories), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, CurrentTaxAssets), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, OtherCurrentAssets), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, AssetsHeldForSale), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, TotalCurrentAssets), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, LongTermInvestments), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, PropertyPlantAndEquipment), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, IntangibleAssets), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, Goodwill), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, DeferredTaxAssets), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, OtherNoncurrentAssets), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, TotalAssets), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, LiabilitiesAndEquity_), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, ShortTermDebt), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, TradeAccountsPayable), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, DividendsPayable), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, IncomeTaxesPayable), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, AccruedExpenses), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, OtherCurrentLiabilities), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, LiabilitiesHeldForSale), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, TotalCurrentLiabilities), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, LongTermDebt), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, PensionBenefitObligations), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, PostretirementBenefitObligations), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, DeferredTaxLiabilities), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, OtherTaxesPayable), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, OtherNoncurrentLliabilities), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, TotalLiabilities), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, CommitmentsAndContingencies_), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, PreferredStock), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, CommonStock), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, Debentures), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, AdditionalPaidInCapital), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, TresuryStock), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, RetainedEarnings), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, AccumulatedOtherComprehensiveLoss), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, TotalShareholdersEquity), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, MinorityInterest), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, TotalEquity), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, TotalLiabilitiesAndEquity), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, DeferredRevenuesShortTerm), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, DeferredRevenuesLongTerm), CurrencyId, @USDCurrencyId, @Today),
		GETDATE(),
		UserCreatedById,
		0
 from cte_CompanyBalanceSheet;    
      
	  	  
;with cte_CompanyCashFlowStatement as(              
 Select  --CAST(Revenues_2018 AS decimal(19,4)) as Revenues, 
 CompanyId,  
 CurrencyId,  
 ValueConversionId,              
 CompanyName,               
 Currency,              
 Units,
CAST(FinanicialYear AS INT) FinanicialYear,
CAST(CASE WHEN ISNUMERIC(OperatingActivities_) = '0' THEN '0' ELSE OperatingActivities_ END AS decimal(19,4)) AS OperatingActivities_,
CAST(CASE WHEN ISNUMERIC(AdjustmentsToOperatingActivities_) = '0' THEN '0' ELSE AdjustmentsToOperatingActivities_ END AS decimal(19,4)) AS AdjustmentsToOperatingActivities_,
CAST(CASE WHEN ISNUMERIC(DepreciationAndAmortization) = '0' THEN '0' ELSE DepreciationAndAmortization END AS decimal(19,4)) AS DepreciationAndAmortization,
CAST(CASE WHEN ISNUMERIC(Impairments) = '0' THEN '0' ELSE Impairments END AS decimal(19,4)) AS Impairments,
CAST(CASE WHEN ISNUMERIC(LossOnSaleOfAssets) = '0' THEN '0' ELSE LossOnSaleOfAssets END AS decimal(19,4)) AS LossOnSaleOfAssets,
CAST(CASE WHEN ISNUMERIC(OtherLossesOrGains) = '0' THEN '0' ELSE OtherLossesOrGains END AS decimal(19,4)) AS OtherLossesOrGains,
CAST(CASE WHEN ISNUMERIC(DeferredTaxes) = '0' THEN '0' ELSE DeferredTaxes END AS decimal(19,4)) AS DeferredTaxes,
CAST(CASE WHEN ISNUMERIC(ShareBasedCompensationExpense) = '0' THEN '0' ELSE ShareBasedCompensationExpense END AS decimal(19,4)) AS ShareBasedCompensationExpense,
CAST(CASE WHEN ISNUMERIC(BenefitPlanContributions) = '0' THEN '0' ELSE BenefitPlanContributions END AS decimal(19,4)) AS BenefitPlanContributions,
CAST(CASE WHEN ISNUMERIC(OtherAdjustmentsNet) = '0' THEN '0' ELSE OtherAdjustmentsNet END AS decimal(19,4)) AS OtherAdjustmentsNet,
CAST(CASE WHEN ISNUMERIC(OtherChangesInAssetsAndLiabilitiesNetOfAcquisitionsAndDistitures_) = '0' THEN '0' ELSE OtherChangesInAssetsAndLiabilitiesNetOfAcquisitionsAndDistitures_ END AS decimal(19,4)) AS OtherChangesInAssetsAndLiabilitiesNetOfAcquisitionsAndDistitures_,
CAST(CASE WHEN ISNUMERIC(AccountsReceivable) = '0' THEN '0' ELSE AccountsReceivable END AS decimal(19,4)) AS AccountsReceivable,
CAST(CASE WHEN ISNUMERIC(OtherAssets) = '0' THEN '0' ELSE OtherAssets END AS decimal(19,4)) AS OtherAssets,
CAST(CASE WHEN ISNUMERIC(AccountsPayable) = '0' THEN '0' ELSE AccountsPayable END AS decimal(19,4)) AS AccountsPayable,
CAST(CASE WHEN ISNUMERIC(OtherLiabilities) = '0' THEN '0' ELSE OtherLiabilities END AS decimal(19,4)) AS OtherLiabilities,
CAST(CASE WHEN ISNUMERIC(OtherTaxAccountsNet) = '0' THEN '0' ELSE OtherTaxAccountsNet END AS decimal(19,4)) AS OtherTaxAccountsNet,
CAST(CASE WHEN ISNUMERIC(NetCashProvidedByOrUsedInOperatingActivities) = '0' THEN '0' ELSE NetCashProvidedByOrUsedInOperatingActivities END AS decimal(19,4)) AS NetCashProvidedByOrUsedInOperatingActivities,
CAST(CASE WHEN ISNUMERIC(InvestingActivities_) = '0' THEN '0' ELSE InvestingActivities_ END AS decimal(19,4)) AS InvestingActivities_,
CAST(CASE WHEN ISNUMERIC(PurchasesOfPropertyPlantAndEquipment) = '0' THEN '0' ELSE PurchasesOfPropertyPlantAndEquipment END AS decimal(19,4)) AS PurchasesOfPropertyPlantAndEquipment,
CAST(CASE WHEN ISNUMERIC(PurchasesOfShortTermInvestments) = '0' THEN '0' ELSE PurchasesOfShortTermInvestments END AS decimal(19,4)) AS PurchasesOfShortTermInvestments,
CAST(CASE WHEN ISNUMERIC(SaleOfShortTermInvestments) = '0' THEN '0' ELSE SaleOfShortTermInvestments END AS decimal(19,4)) AS SaleOfShortTermInvestments,
CAST(CASE WHEN ISNUMERIC(RedemptionsOfShortTermInvestments) = '0' THEN '0' ELSE RedemptionsOfShortTermInvestments END AS decimal(19,4)) AS RedemptionsOfShortTermInvestments,
CAST(CASE WHEN ISNUMERIC(PurchasesOfLongTermInvestments) = '0' THEN '0' ELSE PurchasesOfLongTermInvestments END AS decimal(19,4)) AS PurchasesOfLongTermInvestments,
CAST(CASE WHEN ISNUMERIC(SaleOfLongTermInvestments) = '0' THEN '0' ELSE SaleOfLongTermInvestments END AS decimal(19,4)) AS SaleOfLongTermInvestments,
CAST(CASE WHEN ISNUMERIC(BusinessAcquisitions) = '0' THEN '0' ELSE BusinessAcquisitions END AS decimal(19,4)) AS BusinessAcquisitions,
CAST(CASE WHEN ISNUMERIC(AcquisitionsOfIntangibleAssets) = '0' THEN '0' ELSE AcquisitionsOfIntangibleAssets END AS decimal(19,4)) AS AcquisitionsOfIntangibleAssets,
CAST(CASE WHEN ISNUMERIC(OtherInvestingActivitiesNet) = '0' THEN '0' ELSE OtherInvestingActivitiesNet END AS decimal(19,4)) AS OtherInvestingActivitiesNet,
CAST(CASE WHEN ISNUMERIC(NetCashProvidedByOrUsedInInvestingActivities) = '0' THEN '0' ELSE NetCashProvidedByOrUsedInInvestingActivities END AS decimal(19,4)) AS NetCashProvidedByOrUsedInInvestingActivities,
CAST(CASE WHEN ISNUMERIC(FinancingActivities_) = '0' THEN '0' ELSE FinancingActivities_ END AS decimal(19,4)) AS FinancingActivities_,
CAST(CASE WHEN ISNUMERIC(ProceedsFromShortTermDebt) = '0' THEN '0' ELSE ProceedsFromShortTermDebt END AS decimal(19,4)) AS ProceedsFromShortTermDebt,
CAST(CASE WHEN ISNUMERIC(PrincipalPaymentsOnShortTermDebt) = '0' THEN '0' ELSE PrincipalPaymentsOnShortTermDebt END AS decimal(19,4)) AS PrincipalPaymentsOnShortTermDebt,
CAST(CASE WHEN ISNUMERIC(NetPaymentsonOrProceedsFromShortTermBorrowingsWithOrigilMaturitiesOfThreeMonthsOrLess) = '0' THEN '0' ELSE NetPaymentsonOrProceedsFromShortTermBorrowingsWithOrigilMaturitiesOfThreeMonthsOrLess END AS decimal(19,4)) AS NetPaymentsonOrProceedsFromShortTermBorrowingsWithOrigilMaturitiesOfThreeMonthsOrLess,
CAST(CASE WHEN ISNUMERIC(IssuanceOfLongTermDebt) = '0' THEN '0' ELSE IssuanceOfLongTermDebt END AS decimal(19,4)) AS IssuanceOfLongTermDebt,
CAST(CASE WHEN ISNUMERIC(PrincipalPaymentsOnLongTermDebt) = '0' THEN '0' ELSE PrincipalPaymentsOnLongTermDebt END AS decimal(19,4)) AS PrincipalPaymentsOnLongTermDebt,
CAST(CASE WHEN ISNUMERIC(PurchasesOfCommonStock) = '0' THEN '0' ELSE PurchasesOfCommonStock END AS decimal(19,4)) AS PurchasesOfCommonStock,
CAST(CASE WHEN ISNUMERIC(CashDividendsPaid) = '0' THEN '0' ELSE CashDividendsPaid END AS decimal(19,4)) AS CashDividendsPaid,
CAST(CASE WHEN ISNUMERIC(ProceedsFromExerciseOfStockOptions) = '0' THEN '0' ELSE ProceedsFromExerciseOfStockOptions END AS decimal(19,4)) AS ProceedsFromExerciseOfStockOptions,
CAST(CASE WHEN ISNUMERIC(OtherFinancingActivitiesNet) = '0' THEN '0' ELSE OtherFinancingActivitiesNet END AS decimal(19,4)) AS OtherFinancingActivitiesNet,
CAST(CASE WHEN ISNUMERIC(NetCashProvidedByOrUsedInFinancingActivities) = '0' THEN '0' ELSE NetCashProvidedByOrUsedInFinancingActivities END AS decimal(19,4)) AS NetCashProvidedByOrUsedInFinancingActivities,
CAST(CASE WHEN ISNUMERIC(ForeignExchangeImpact) = '0' THEN '0' ELSE ForeignExchangeImpact END AS decimal(19,4)) AS ForeignExchangeImpact,
CAST(CASE WHEN ISNUMERIC(DecreaseInCashAndCashEquivalents) = '0' THEN '0' ELSE DecreaseInCashAndCashEquivalents END AS decimal(19,4)) AS DecreaseInCashAndCashEquivalents,
CAST(CASE WHEN ISNUMERIC(OpeningBalanceOfCashAndCashEquivalents) = '0' THEN '0' ELSE OpeningBalanceOfCashAndCashEquivalents END AS decimal(19,4)) AS OpeningBalanceOfCashAndCashEquivalents,
CAST(CASE WHEN ISNUMERIC(ClosingBalanceOfCashAndCashEquivalents) = '0' THEN '0' ELSE ClosingBalanceOfCashAndCashEquivalents END AS decimal(19,4)) AS ClosingBalanceOfCashAndCashEquivalents,
CAST(CASE WHEN ISNUMERIC(CashPaidReceivedDuringThePeriodFor_) = '0' THEN '0' ELSE CashPaidReceivedDuringThePeriodFor_ END AS decimal(19,4)) AS CashPaidReceivedDuringThePeriodFor_,
CAST(CASE WHEN ISNUMERIC(IncomeTaxes) = '0' THEN '0' ELSE IncomeTaxes END AS decimal(19,4)) AS IncomeTaxes,
CAST(CASE WHEN ISNUMERIC(InterestRateHedges) = '0' THEN '0' ELSE InterestRateHedges END AS decimal(19,4)) AS InterestRateHedges,
CAST(CASE WHEN ISNUMERIC(CommonStockRepurchased) = '0' THEN '0' ELSE CommonStockRepurchased END AS decimal(19,4)) AS CommonStockRepurchased,
CAST(CASE WHEN ISNUMERIC(CapitalExpenditure) = '0' THEN '0' ELSE CapitalExpenditure END AS decimal(19,4)) AS CapitalExpenditure,
UserCreatedById
 from StagingCompanyProfilesFinancial              
)        
--SELECT * FROM cte_CompanyPLStatement

insert into CompanyCashFlowStatement              
(              
 Id,
CompanyId,
CurrencyId,
ValueConversionId,
CompanyName,
Currency,
Units,
Year,
OperatingActivities_,
AdjustmentsToOperatingActivities_,
DepreciationAndAmortization,
Impairments,
LossOnSaleOfAssets,
OtherLossesOrGains,
DeferredTaxes,
ShareBasedCompensationExpense,
BenefitPlanContributions,
OtherAdjustmentsNet,
OtherChangesInAssetsAndLiabilitiesNetOfAcquisitionsAndDistitures_,
AccountsReceivable,
OtherAssets,
AccountsPayable,
OtherLiabilities,
OtherTaxAccountsNet,
NetCashProvidedByOrUsedInOperatingActivities,
InvestingActivities_,
PurchasesOfPropertyPlantAndEquipment,
PurchasesOfShortTermInvestments,
SaleOfShortTermInvestments,
RedemptionsOfShortTermInvestments,
PurchasesOfLongTermInvestments,
SaleOfLongTermInvestments,
BusinessAcquisitions,
AcquisitionsOfIntangibleAssets,
OtherInvestingActivitiesNet,
NetCashProvidedByOrUsedInInvestingActivities,
FinancingActivities_,
ProceedsFromShortTermDebt,
PrincipalPaymentsOnShortTermDebt,
NetPaymentsonOrProceedsFromShortTermBorrowingsWithOrigilMaturitiesOfThreeMonthsOrLess,
IssuanceOfLongTermDebt,
PrincipalPaymentsOnLongTermDebt,
PurchasesOfCommonStock,
CashDividendsPaid,
ProceedsFromExerciseOfStockOptions,
OtherFinancingActivitiesNet,
NetCashProvidedByOrUsedInFinancingActivities,
ForeignExchangeImpact,
DecreaseInCashAndCashEquivalents,
OpeningBalanceOfCashAndCashEquivalents,
ClosingBalanceOfCashAndCashEquivalents,
CashPaidReceivedDuringThePeriodFor_,
IncomeTaxes,
InterestRateHedges,
CommonStockRepurchased, 
CapitalExpenditure,          
 CreatedOn,              
 UserCreatedById,              
 IsDeleted              
 )              
              
	select NEWID(),           
		CompanyId,           
		CurrencyId,              
		ValueConversionId,            
		CompanyName,             
		Currency,              
		Units,              
		FinanicialYear,              
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, OperatingActivities_), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, AdjustmentsToOperatingActivities_), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, DepreciationAndAmortization), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, Impairments), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, LossOnSaleOfAssets), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, OtherLossesOrGains), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, DeferredTaxes), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, ShareBasedCompensationExpense), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, BenefitPlanContributions), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, OtherAdjustmentsNet), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, OtherChangesInAssetsAndLiabilitiesNetOfAcquisitionsAndDistitures_), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, AccountsReceivable), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, OtherAssets), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, AccountsPayable), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, OtherLiabilities), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, OtherTaxAccountsNet), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, NetCashProvidedByOrUsedInOperatingActivities), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, InvestingActivities_), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, PurchasesOfPropertyPlantAndEquipment), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, PurchasesOfShortTermInvestments), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, SaleOfShortTermInvestments), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, RedemptionsOfShortTermInvestments), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, PurchasesOfLongTermInvestments), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, SaleOfLongTermInvestments), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, BusinessAcquisitions), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, AcquisitionsOfIntangibleAssets), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, OtherInvestingActivitiesNet), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, NetCashProvidedByOrUsedInInvestingActivities), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, FinancingActivities_), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, ProceedsFromShortTermDebt), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, PrincipalPaymentsOnShortTermDebt), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, NetPaymentsonOrProceedsFromShortTermBorrowingsWithOrigilMaturitiesOfThreeMonthsOrLess), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, IssuanceOfLongTermDebt), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, PrincipalPaymentsOnLongTermDebt), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, PurchasesOfCommonStock), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, CashDividendsPaid), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, ProceedsFromExerciseOfStockOptions), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, OtherFinancingActivitiesNet), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, NetCashProvidedByOrUsedInFinancingActivities), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, ForeignExchangeImpact), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, DecreaseInCashAndCashEquivalents), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, OpeningBalanceOfCashAndCashEquivalents), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, ClosingBalanceOfCashAndCashEquivalents), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, CashPaidReceivedDuringThePeriodFor_), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, IncomeTaxes), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, InterestRateHedges), CurrencyId, @USDCurrencyId, @Today),
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, CommonStockRepurchased), CurrencyId, @USDCurrencyId, @Today), 
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, CapitalExpenditure), CurrencyId, @USDCurrencyId, @Today),
		GETDATE(),              
		UserCreatedById,      
		0               
	FROM cte_CompanyCashFlowStatement; 
 	  	  
;with cte_CompanySegmentInformation as(              
 Select  --CAST(Revenues_2018 AS decimal(19,4)) as Revenues, 
 CompanyId,  
 CurrencyId,  
 ValueConversionId,              
CAST(FinanicialYear AS INT) FinanicialYear,
Type,
Name,
CAST(CASE WHEN ISNUMERIC(Value) = '0' THEN '0' ELSE Value END AS decimal(19,4)) AS Value,
UserCreatedById
 from StagingCompanySegmentInformation              
)        
--SELECT * FROM cte_CompanyPLStatement

insert into CompanySegmentInformation              
(              
 Id,
CompanyId,
CurrencyId,
ValueConversionId,
Year,
Type,
Name,
Value,
CreatedOn,              
UserCreatedById,              
IsDeleted              
 )              
 select NEWID(),           
 CompanyId,           
 CurrencyId,              
 ValueConversionId,            
 FinanicialYear,
 Type,
 Name,  
 [dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](ValueConversionId, Value), CurrencyId, @USDCurrencyId, @Today),           
 GETDATE(),              
 UserCreatedById,      
 0               
 from cte_CompanySegmentInformation;    
   
---------------Update approval status -----------------
SET @CompanyType = 'CompanyPLStatement'
SET @Id = NEWID()
exec SApp_InsertCompanyNotes @Id, @CompanyId, @ProjectId, @CompanyType, @AuthorRemark, @ApproverRemark, @CompanyStatusName, @AviraUserId 
-----
SET @CompanyType = 'CompanyBalanceSheet'
SET @Id = NEWID()
exec SApp_InsertCompanyNotes @Id, @CompanyId, @ProjectId, @CompanyType, @AuthorRemark, @ApproverRemark, @CompanyStatusName, @AviraUserId 
-----
SET @CompanyType = 'CompanyCashFlowStatement'
SET @Id = NEWID()
exec SApp_InsertCompanyNotes @Id, @CompanyId, @ProjectId, @CompanyType, @AuthorRemark, @ApproverRemark, @CompanyStatusName, @AviraUserId 
-----
SET @CompanyType = 'CompanySegmentInformation'
SET @Id = NEWID()
exec SApp_InsertCompanyNotes @Id, @CompanyId, @ProjectId, @CompanyType, @AuthorRemark, @ApproverRemark, @CompanyStatusName, @AviraUserId 
-----
 ---Update Statics--
 UPDATE STATISTICS [CompanyPLStatement] WITH FULLSCAN;
 UPDATE STATISTICS [CompanyBalanceSheet] WITH FULLSCAN;
 UPDATE STATISTICS [CompanyCashFlowStatement] WITH FULLSCAN;
 UPDATE STATISTICS [CompanySegmentInformation] WITH FULLSCAN;
 UPDATE STATISTICS [CompanyNote] WITH FULLSCAN;

 ---End Statics----
            
SELECT StoredProcedureName ='SApp_InsertCompanyProfilesFinancial',Message =@ErrorMsg,Success = 1;                   
END TRY                    
BEGIN CATCH                    
    -- Execute the error retrieval routine.                    
 DECLARE @ErrorNumber int;                    
 DECLARE @ErrorSeverity int;                    
 DECLARE @ErrorProcedure varchar(100);                    
 DECLARE @ErrorLine int;                    
 DECLARE @ErrorMessage varchar(500);                    
                    
  SELECT @ErrorNumber = ERROR_NUMBER(),                    
        @ErrorSeverity = ERROR_SEVERITY(),                    
        @ErrorProcedure = ERROR_PROCEDURE(),                    
        @ErrorLine = ERROR_LINE(),                    
        @ErrorMessage = ERROR_MESSAGE(),   
		@CreatedOn = GETDATE()                 
                    
 insert into dbo.Errorlog(ErrorNumber,                    
       ErrorSeverity,                    
       ErrorProcedure,                    
       ErrorLine,                    
       ErrorMessage,                    
       ErrorDate)                    
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,@CreatedOn);                    
                  
 set @ErrorMsg = 'Error while inserting CompanyProfilesFinancial, please contact Admin for more details.';                    
--THROW 50000,@ErrorMsg,1;                      
--return(1)                  
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine                  
    ,Message =@ErrorMsg,Success = 0;                     
                     
END CATCH                     
                
                
END