﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyBalanceSheetController : AviraBaseController
    {
        private readonly ICompanyBalanceSheetService _iCompanyBalanceSheetService;
        private ILoggerManager _logger;

        public CompanyBalanceSheetController(
           ICompanyBalanceSheetService iCompanyBalanceSheetService,
            ILoggerManager logger)
        {
            _iCompanyBalanceSheetService = iCompanyBalanceSheetService;
            _logger = logger;

        }

        [HttpGet("{id}")]
        public ActionResult<IEnumerable<CompanyBalanceSheetViewModel>> GetCompanyBalanceSheet(Guid Id)
        {
            List<CompanyBalanceSheetViewModel> res = null;
            AviraUser user = new AviraUser();
            try
            {
                res = _iCompanyBalanceSheetService.GetCompanyBalanceSheetData(Id, user).ToList();

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);

            }
            return res;
        }
    }
}
