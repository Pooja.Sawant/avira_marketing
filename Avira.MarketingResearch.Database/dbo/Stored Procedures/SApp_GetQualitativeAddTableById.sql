﻿  
CREATE procedure [dbo].[SApp_GetQualitativeAddTableById]  
(@QualitativeID uniqueidentifier,  
@includeDeleted bit = 0)  
as  
/*================================================================================  
Procedure Name: SApp_GetQualitativeAddTableById  
Author: Laxmikant  
Create date: 02/19/2019  
Description: Get Qualitative Table by ID  
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
06/21/2019 Laxmikant   Initial Version  
================================================================================*/  
BEGIN  
SET NOCOUNT ON;  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
  
if @includeDeleted is null set @includeDeleted = 0  
  
SELECT [Id]  
      ,[ProjectId]  
      ,[TableName]  
      ,[TableMetadata]  
      ,[TableData]  
      ,[CreatedOn]  
      ,[UserCreatedById]  
      ,[ModifiedOn]  
      ,[UserModifiedById]  
      ,[IsDeleted]  
      ,[DeletedOn]  
      ,[UserDeletedById]  
  FROM [dbo].[QualitativeTableTab]  
where (@QualitativeID is null or ID = @QualitativeID)  
and (IsDeleted = @includeDeleted Or IsDeleted is null)   
end