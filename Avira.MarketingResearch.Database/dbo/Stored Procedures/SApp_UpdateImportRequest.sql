﻿
CREATE procedure [dbo].[SApp_UpdateImportRequest]
(@Id uniqueidentifier,
 @ImportStatusName nvarchar(100),  
 @ImportException nvarchar(MAX)=null,
 @ImportExceptionFilePath nvarchar(1000)=null,
 --@ModifiedOn datetime =null,
 @UserModifiedById uniqueidentifier=null)
as
/*================================================================================
Procedure Name: SApp_UpdateImportRequest
Author: Harshal
Create date: 10/15/2019
Description: update a record in Import Request table by ID.
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
10/15/2019	Harshal			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

declare @ErrorMsg NVARCHAR(2048),
        @StatusId uniqueidentifier

SELECT @StatusId=Id FROM LookupStatus where StatusName=@ImportStatusName;


BEGIN TRY 
--update record
update [dbo].[ImportData]
set ImportStatusId=@StatusId,
    ImportException=@ImportException,
    ImportExceptionFilePath=@ImportExceptionFilePath,
	ModifiedOn=GETDATE(),
	UserModifiedById=@UserModifiedById
    WHERE ID = @Id

SELECT StoredProcedureName ='SApp_UpdateImportRequest',Message =@ErrorMsg,Success = 1;     
  
END TRY    
BEGIN CATCH    
    -- Execute the error retrieval routine.    
 DECLARE @ErrorNumber int;    
 DECLARE @ErrorSeverity int;    
 DECLARE @ErrorProcedure varchar(100);    
 DECLARE @ErrorLine int;    
 DECLARE @ErrorMessage varchar(2048);    
    
  SELECT @ErrorNumber = ERROR_NUMBER(),    
        @ErrorSeverity = ERROR_SEVERITY(),    
        @ErrorProcedure = ERROR_PROCEDURE(),    
        @ErrorLine = ERROR_LINE(),    
        @ErrorMessage = ERROR_MESSAGE()    
    
 insert into dbo.Errorlog(ErrorNumber,    
       ErrorSeverity,    
       ErrorProcedure,    
       ErrorLine,    
       ErrorMessage,    
       ErrorDate)    
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE()); 

set @ErrorMsg = 'Error while updating Import Data, please contact Admin for more details.' 		   

 SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;     
END CATCH    
    
 END