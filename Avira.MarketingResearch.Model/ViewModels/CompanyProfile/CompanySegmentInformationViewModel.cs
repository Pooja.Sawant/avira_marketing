﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.CompanyProfile
{
    public class CompanySegmentInformationViewModel
    {
        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
        public Guid CurrencyId { get; set; }
        public Guid ValueConversionId { get; set; }
        public string CompanyName { get; set; }
        public string Currency { get; set; }
        public string Units { get; set; }
        public int Year { get; set; }
        public decimal Segment1 { get; set; }
        public decimal Segment2 { get; set; }
        public decimal Segment3 { get; set; }
        public decimal Segment4 { get; set; }
        public decimal Segment5 { get; set; }
        public decimal TotalReportableSegments { get; set; }
        public decimal Geography1 { get; set; }
        public decimal Geography2 { get; set; }
        public decimal Geography3 { get; set; }
        public decimal Geography4 { get; set; }
        public decimal Geography5 { get; set; }
        public decimal TotalGeographicSegments { get; set; }
        public DateTime? CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid UserModifiedById { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid UserDeletedById { get; set; }
    }

    public class MainCompanySegmentInformationViewModel
    {
        public Guid ProjectId { get; set; }
        public Guid CompanyId { get; set; }
        public List<CompanySegmentInformationViewModel> CompanySegmentInformationViewModelList { get; set; }
        public CompanyNoteViewModel CompanyNoteViewModel { get; set; }
        public List<int> YearList { get; set; }
    }
}
