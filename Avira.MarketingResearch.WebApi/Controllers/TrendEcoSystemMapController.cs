﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrendEcoSystemMapController : AviraBaseController
    {
        private readonly ITrendEcoSystemMapService _trendEcoSystemMapService;
        private readonly ITrendEcoSystemMapRepository _trendEcoSystemMapRepository;
        private ILoggerManager _logger;

        public TrendEcoSystemMapController(
            ITrendEcoSystemMapService trendEcoSystemMapService,
            ITrendEcoSystemMapRepository trendEcoSystemMapRepository,
             ILoggerManager logger)
        {
            _trendEcoSystemMapRepository = trendEcoSystemMapRepository;
            _trendEcoSystemMapService = trendEcoSystemMapService;
            _logger = logger;

        }

        [HttpGet]
        public ActionResult<IEnumerable<TrendEcoSystemMapViewModel>> Get(Guid? Id ,int PageStart = 0, int pageSize = 0, int SortDirection = 1, string OrdbyByColumnName = "CreatedDate", string Search = "", bool isDeletedInclude = false)
        {
            List<TrendEcoSystemMapViewModel> res = null;
            try
            {
                res = _trendEcoSystemMapService.GetAll(Id,PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }


        [HttpPost]
        public ActionResult Post([FromBody] MainTrendEcoSystemMapInsertUpdateDbViewModel viewModel)
        {
            viewModel.CreatedOn = System.DateTime.UtcNow;
            viewModel.AviraUserId = CurrentUser.Id;

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new TrendEcoSystemMap, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                var result = _trendEcoSystemMapService.InsertUpdate(viewModel, CurrentUser);
                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "TrendEcoSystemMap is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }

        

        
    }
}