﻿using Avira.MarketingResearch.Model;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using static Avira.MarketingResearch.Model.ViewModels.ApplicationViewModel;

namespace Avira.MarketingResearch.Common.IService
{
    public interface IApplicationService
    {
        IEnumerable<ApplicationViewModel> GetAll(Guid Id);
        IEnumerable<ApplicationViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false);
        ApplicationViewModel GetById(Guid guid);

        Result<SpTransactionMessage> Create(ApplicationInsertDbViewModel tfc, AviraUser currentUser);
        Result<SpTransactionMessage> Update(ApplicationUpdateDbViewModel tfc, AviraUser currenUser);
    }
}
