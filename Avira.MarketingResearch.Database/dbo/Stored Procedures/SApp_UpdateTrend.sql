﻿


CREATE procedure [dbo].[SApp_UpdateTrend]
(@Id uniqueidentifier,
@TrendName varchar(899),    
@Description nvarchar(256),  
@ProjectID uniqueidentifier, 
@AuthorRemark nvarchar(512),
@ApproverRemark nvarchar(512), 
@TrendStatusID uniqueidentifier,
@TrendStatusName nvarchar(20),
@IsDeleted bit=0,  
@UserModifiedById  uniqueidentifier,  
@DeletedOn datetime = null,  
@UserDeletedById uniqueidentifier = null,  
@ModifiedOn Datetime)
as
/*================================================================================
Procedure Name: SApp_UpdateTrend
Author: Harshal
Create date: 04/12/2019
Description: update a record in Trend table by ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
04/12/2019	Harshal			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET @TrendStatusID =(SELECT Id FROM [dbo].[TrendStatus] WHERE TrendStatusName=@TrendStatusName);
IF exists(select 1 from [dbo].[Trend] 
		 where [Id] !=  @Id
		 AND  TrendName = @TrendName 
		 AND ProjectID = @ProjectID or (ProjectID is null and @ProjectID is null) 
		 AND TrendStatusID=@TrendStatusID
		 AND IsDeleted = 0)
begin
declare @ErrorMsg NVARCHAR(2048);
set @ErrorMsg = 'Trend with Name "'+ @TrendName + '" already exists.aaaa';
--THROW 50000,@ErrorMsg,1;
--end
SELECT StoredProcedureName ='SApp_UpdateTrend','Message' =@ErrorMsg,'Success' = 0;  
RETURN;
end 

BEGIN TRY 
--update record
update [dbo].[Trend]
set [TrendName] = @TrendName,
	[Description] = @Description,
	[ProjectID]=@ProjectID,
	[ApproverRemark]=@ApproverRemark,
	[AuthorRemark]=@AuthorRemark,
	[TrendStatusID]=@TrendStatusID,
	[ModifiedOn] = @ModifiedOn,
	DeletedOn = case when IsDeleted = 0 and @IsDeleted = 1 then @DeletedOn else DeletedOn end,
	UserDeletedById = case when IsDeleted = 0 and @IsDeleted = 1 then @UserDeletedById else UserDeletedById end,
	IsDeleted = isnull(@IsDeleted, IsDeleted),
	[UserModifiedById] = @UserModifiedById
    where ID = @Id

SELECT StoredProcedureName ='SApp_UpdateTrend',Message =@ErrorMsg,Success = 1;     
  
END TRY    
BEGIN CATCH    
    -- Execute the error retrieval routine.    
 DECLARE @ErrorNumber int;    
 DECLARE @ErrorSeverity int;    
 DECLARE @ErrorProcedure varchar(100);    
 DECLARE @ErrorLine int;    
 DECLARE @ErrorMessage varchar(500);    
    
  SELECT @ErrorNumber = ERROR_NUMBER(),    
        @ErrorSeverity = ERROR_SEVERITY(),    
        @ErrorProcedure = ERROR_PROCEDURE(),    
        @ErrorLine = ERROR_LINE(),    
        @ErrorMessage = ERROR_MESSAGE()    
    
 insert into dbo.Errorlog(ErrorNumber,    
       ErrorSeverity,    
       ErrorProcedure,    
       ErrorLine,    
       ErrorMessage,    
       ErrorDate)    
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE()); 

set @ErrorMsg = 'Error while updating Trend, please contact Admin for more details.' 		   
 --set @ErrorMsg = 'Unspecified Error';    
--THROW 50000,@ErrorMsg,1;    
 --return(1)  
 SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;     
END CATCH   
end