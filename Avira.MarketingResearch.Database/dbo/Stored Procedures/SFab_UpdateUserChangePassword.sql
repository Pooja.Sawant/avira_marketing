﻿
create procedure [dbo].[SFab_UpdateUserChangePassword]    
( 
@UserName nvarchar(100),
@PassWord nvarchar(1000) 
)    
as    
/*================================================================================    
Procedure Name: SFab_UpdateUserChangePassword   
Author: Pooja Sawant
Create date: 09/09/2019  
Description: update user table password for change password
Change History  
Date        Developer     Reason  
__________  ____________  ______________________________________________________  
09/09/2019  Pooja         Initial Version   
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
declare @ErrorMsg NVARCHAR(2048)
  
BEGIN TRY       

UPDATE [dbo].[AviraUser] SET
       PassWord=@PassWord,
       ModifiedOn=GETDATE() 
	   WHERE UserName = @UserName AND UserTypeId = 2
  
SELECT StoredProcedureName ='SFab_UpdateUserChangePassword',Message =@ErrorMsg,Success = 1; 
   
END TRY    
BEGIN CATCH    
    -- Execute the error retrieval routine.    
 DECLARE @ErrorNumber int;    
 DECLARE @ErrorSeverity int;    
 DECLARE @ErrorProcedure varchar(100);    
 DECLARE @ErrorLine int;    
 DECLARE @ErrorMessage varchar(500);    
    
  SELECT @ErrorNumber = ERROR_NUMBER(),    
        @ErrorSeverity = ERROR_SEVERITY(),    
        @ErrorProcedure = ERROR_PROCEDURE(),    
        @ErrorLine = ERROR_LINE(),    
        @ErrorMessage = ERROR_MESSAGE()    
    
 insert into dbo.Errorlog(ErrorNumber,    
       ErrorSeverity,    
       ErrorProcedure,    
       ErrorLine,    
       ErrorMessage,    
       ErrorDate)    
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());    
  
 set @ErrorMsg = 'Error while Update a User chnage password, please contact Admin for more details.';    
--THROW 50000,@ErrorMsg,1;      
--return(1)  
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;     
     
END CATCH    
    
    
end