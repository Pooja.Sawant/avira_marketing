﻿using Avira.KeyCompanyingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.Trend
{
    public class TrendKeyCompanyMapService : ITrendKeyCompanyMapService
    {
        private readonly ITrendKeyCompanyMapRepository _trendKeyCompanyRepository;
        private readonly ITrendNoteRepository _iTrendNoteRepository;

        public TrendKeyCompanyMapService(ITrendKeyCompanyMapRepository trendEcoSystemMapRepository, ITrendNoteRepository iTrendNoteRepository)
        {
            _trendKeyCompanyRepository = trendEcoSystemMapRepository;
            _iTrendNoteRepository = iTrendNoteRepository;
        }

        public IEnumerable<TrendKeyCompanyMapViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false, string id = "")
        {
            return _trendKeyCompanyRepository.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude = false, id);
        }

        public Result<SpTransactionMessage> InsertUpdate(MainTrendKeyCompanyMapInsertUpdateDbViewModel tfc, AviraUser currenUser)
        {
            var res = _trendKeyCompanyRepository.InsertUpdate(tfc);
            tfc.TrendNoteModel.Id = Guid.NewGuid();
            tfc.TrendNoteModel.UserCreatedById = tfc.AviraUserId;
            tfc.TrendNoteModel.TrendId = tfc.TrendId;
            _iTrendNoteRepository.Insert(tfc.TrendNoteModel);

            return res;
        }

        public Result<SpTransactionMessage> Insert(MainTrendKeyCompanyMapInsertUpdateDbViewModel tfc, AviraUser currenUser)
        {
            var res = _trendKeyCompanyRepository.Insert(tfc);
            tfc.TrendNoteModel.Id = Guid.NewGuid();
            tfc.TrendNoteModel.UserCreatedById = tfc.AviraUserId;
            tfc.TrendNoteModel.TrendId = tfc.TrendId;
            _iTrendNoteRepository.Insert(tfc.TrendNoteModel);

            return res;
        }

        public Result<SpTransactionMessage> Delete(MainTrendKeyCompanyMapInsertUpdateDbViewModel tfc, AviraUser currenUser)
        {
            var res = _trendKeyCompanyRepository.InsertUpdate(tfc);
            tfc.TrendNoteModel.Id = Guid.NewGuid();
            tfc.TrendNoteModel.UserCreatedById = tfc.AviraUserId;
            tfc.TrendNoteModel.TrendId = tfc.TrendId;
            _iTrendNoteRepository.Insert(tfc.TrendNoteModel);

            return res;
        }
    }
}
