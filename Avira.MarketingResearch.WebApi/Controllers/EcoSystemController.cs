﻿using Avira.MarketingResearch.Common.IService.Masters;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces.Masters;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Utility;
using Avira.MarketingResearch.WebApi;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Avira.MarketingResearch.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EcoSystemController : AviraBaseController
    {
        private ILoggerManager _logger;
        public readonly IEcoSystemRepository _ecoSystemRepository;
        public readonly IEcoSystemService _ecoSystemService;
        public EcoSystemController(
            IEcoSystemService ecoSystemService,
            IEcoSystemRepository ecoSystemRepository,
            ILoggerManager logger)
        {
            _ecoSystemRepository = ecoSystemRepository;
            _ecoSystemService = ecoSystemService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<EcoSystemViewModel>> Get( int PageStart=0, int pageSize=0, int SortDirection=1, string OrdbyByColumnName= "CreatedDate", string Search = "", bool isDeletedInclude = false)
        {
            List<EcoSystemViewModel> res = null;
            try
            {
                res= _ecoSystemService.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }


        [HttpGet("{id}")]
        public ActionResult<EcoSystemViewModel> Get(Guid id)
        {
            EcoSystemViewModel ecoSystem = null;
            try
            {

                ecoSystem = _ecoSystemService.GetById(id);
                if (ecoSystem == null)
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return ecoSystem;
        }

        [HttpPost]
        public ActionResult Post([FromBody] EcoSystemInsertDbViewModel viewModel)
        {
            viewModel.CreatedOn = System.DateTime.UtcNow;
            viewModel.UserCreatedById = CurrentUser.Id;
            viewModel.Id = Guid.NewGuid();

            
            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new EcoSystem, please contact Admin for more details."
            };

            try
            {

                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                var result = _ecoSystemService.Create(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "EcoSystem name " + viewModel.EcoSystemName + " is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }

            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);

        }


        [HttpPut]
        public ActionResult Put([FromBody] EcoSystemUpdateDbViewModel viewModel)
        {

            viewModel.ModifiedOn = System.DateTime.UtcNow;
            viewModel.UserModifiedById = CurrentUser.Id;


            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while updating a  EcoSystem, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                Result<SpTransactionMessage> result = _ecoSystemService.Update(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "EcoSystem name " + viewModel.EcoSystemName + " is updated successfully.";

                }
                else
                {
                    viewData.Message = result.Error;
                }
            }

            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }

        [Route("DeleteSegment")]
        [HttpPut]
        public ActionResult DeleteSegment([FromBody] EcoSystemUpdateDbViewModel viewModel)
        {
           
                viewModel.DeletedOn = System.DateTime.UtcNow;
                viewModel.UserDeletedById = CurrentUser.Id;
                viewModel.IsDeleted = true;


                IViewModel viewData = new ViewData()
                {
                    IsSuccess = false,
                    Message = "Error while deleting a new EcoSystem, please contact Admin for more details."
                };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                Result<SpTransactionMessage> result = _ecoSystemService.Update(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;

                    viewData.Message = "EcoSystem name " + viewModel.EcoSystemName + " is deleted.";

                }
                else
                {
                    viewData.Message = result.Error;
                }
            }

            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }

    }
}
