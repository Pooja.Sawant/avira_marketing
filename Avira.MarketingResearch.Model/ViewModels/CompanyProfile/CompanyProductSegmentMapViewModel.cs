﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.CompanyProfile
{
    public class CompanyProductSegmentMapViewModel
    {
        public Guid Id { get; set; }
        public Guid CompanyProductId { get; set; }
        public Guid ProductSegmentId { get; set; }
        public string ProductSegmentName { get; set; }
        public bool IsMapped { get; set; }
    }

    public class MainCompanyProductSegmentMapInsertUpdateDbViewModel
    {
        public Guid CompanyProductId { get; set; }
        public string ProductSegmentName { get; set; }
        public List<CompanyProductSegmentMapViewModel> CompanyProductSegmentMapInsertUpdate { get; set; }
        public Guid AviraUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
    }
}
