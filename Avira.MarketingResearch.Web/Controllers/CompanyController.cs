﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.Models;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.Web.Controllers
{
    [NoCache]
    [Authorization]
    public class CompanyController : BaseController
    {
        private ILoggerManager _logger;
        public readonly IServiceRepository _serviceRepository;
        public CompanyController(IServiceRepository serviceRepository, ILoggerManager logger)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public ActionResult LoadData(CompanyViewModel company)
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            int totalRecords = 0;
            List<CompanyViewModel> data = null;
            try
            {
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                string search = Request.Form["search[value]"];

                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDir = Request.Form["order[0][dir]"].FirstOrDefault();
                var SortDirection = -1;
                if (sortColumnDir == "asc")
                {
                    SortDirection = 1;
                }
                else
                {
                    SortDirection = -1;
                }

                int skip = start != null ? Convert.ToInt32(start) : 0;
                int pageSize = length.ToString() != null ? Convert.ToInt32(length) : 0;
                int PageStart = start.ToString() != null ? Convert.ToInt32(start) : 0;
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();

                HttpResponseMessage response = _serviceRepository.GetResponse(contollerName, PageStart, pageSize, SortDirection, sortColumn, search);
                response.EnsureSuccessStatusCode();
                List<CompanyViewModel> companies = response.Content.ReadAsAsync<List<CompanyViewModel>>().Result;
                ViewBag.Title = "All Companies";
                data = companies;
            
                foreach (var item in companies)
                {
                    totalRecords = item.TotalItems;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data });

        }
        [HttpGet]
        public IActionResult Edit(string id)
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.GetByIdResponse(contollerName, id);
                response.EnsureSuccessStatusCode();
                CompanyUpdateDbViewModel company = response.Content.ReadAsAsync<CompanyUpdateDbViewModel>().Result;
                return View(company);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                ErrorViewModel err = new ErrorViewModel { RequestId = "1234" };
                //Log the exception 
                return View("Error", err);
            }
        }

        [HttpGet]
        public IActionResult Delete(string id)
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.GetByIdResponse(contollerName, id);
                response.EnsureSuccessStatusCode();
                CompanyUpdateDbViewModel company = response.Content.ReadAsAsync<CompanyUpdateDbViewModel>().Result;
                return View(company);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                ErrorViewModel err = new ErrorViewModel { RequestId = "1234" };
                //Log the exception 
                return View("Error", err);
            }
        }

        [HttpPost]
        public IActionResult Edit(CompanyUpdateDbViewModel company)
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.PutResponse(contollerName, company);
                ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                if (viewModel != null)
                {
                    ViewBag.Message = viewModel.Message;
                }
                response.EnsureSuccessStatusCode();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(company);
        }

        public IActionResult Create()
        {

            return View();
        }

        [HttpPost]
        public IActionResult Create(CompanyInsertDbViewModel company)
        {

            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, company);
                response.EnsureSuccessStatusCode();
                ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                if (viewModel != null)
                {
                    ViewBag.Message = viewModel.Message;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(company);
        }

        [HttpPost]
        public IActionResult Delete(CompanyUpdateDbViewModel company)
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.DeleteResponse(contollerName, company);
                response.EnsureSuccessStatusCode();
                ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                if (viewModel != null)
                {
                    ViewBag.Message = viewModel.Message;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return RedirectToAction("Index");
        }
       

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}