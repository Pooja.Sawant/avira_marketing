﻿CREATE procedure [dbo].[SApp_insertUpdateDeviceAndSolution]
(@DeviceAndSolutionID uniqueidentifier,
@Description nvarchar(256),
@Name nvarchar(256),
@IsProduct	bit,
@IsSoftware	bit,
@IsDeleted bit,
@AviraUserId uniqueidentifier)
as
/*================================================================================
Procedure Name: SApp_DeleteDeviceAndSolutionbyID
Author: Sharath
Create date: 02/19/2019
Description: soft delete a record from DeviceAndSolution table by ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
02/19/2019	Sharath			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

IF exists(select 1 from [dbo].[DeviceAndSolution] 
		 where ((@DeviceAndSolutionID is null or [Id] !=  @DeviceAndSolutionID)
		 and  Name = @Name AND IsDeleted = 0))
begin
declare @ErrorMsg NVARCHAR(2048);
set @ErrorMsg = 'Device or Solution with Name "'+ @Name + '" already exists.';
THROW 50000,@ErrorMsg,1;
end

IF (@DeviceAndSolutionID is null)
begin
--Insert new record
	set @DeviceAndSolutionID = NEWID();

	insert into [dbo].[DeviceAndSolution]([Id],
								[Name],
								[Description],
								IsProduct,
								IsSoftware,
								[IsDeleted],
								[UserCreatedById],
								[CreatedOn])
	values(@DeviceAndSolutionID, 
			@Name, 
			@Description, 
			@IsProduct,
			@IsSoftware,
			0,
			@AviraUserId, 
			getdate());
end
else
begin
--update record
	update [dbo].[DeviceAndSolution]
	set [Name] = @Name,
		[Description] = @Description,
		IsProduct = @IsProduct,
		IsSoftware = @IsSoftware,
		[ModifiedOn] = getdate(),
		DeletedOn = case when IsDeleted = 0 and @IsDeleted = 1 then getdate() else DeletedOn end,
		UserDeletedById = case when IsDeleted = 0 and @IsDeleted = 1 then @AviraUserId else UserDeletedById end,
		IsDeleted = isnull(@IsDeleted, IsDeleted),
		[UserModifiedById] = @AviraUserId
     where ID = @DeviceAndSolutionID
end

select @DeviceAndSolutionID;
end