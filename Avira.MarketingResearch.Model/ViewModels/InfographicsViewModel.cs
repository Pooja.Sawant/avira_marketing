﻿using Avira.MarketingResearch.Model.CustomValidation;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels
{
    public class InfographicsViewModel
    {
        public Guid Id { get; set; }

        [Required]
        [Display(Name = "Project")]
        public Guid? ProjectId { get; set; }
        public string ProjectName { get; set; }
        [Required]
        [Display(Name = "Category")]
        public Guid? LookupCategoryId { get; set; }
        public string Category { get; set; }
        [Required]
        [Display(Name = "Title")]
        public string InfogrTitle { get; set; }
        //[Required]
        [Display(Name = "Description")]
        public string InfogrDescription { get; set; }
        //[Required]
        [Display(Name = "Sequence No")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Invalid Sequence No")]
        public Int16 SequenceNo { get; set; }

        [Display(Name = "ImageActualName")]
        public string ImageActualName { get; set; }
        [Display(Name = "ImageName")]
        public string ImageName { get; set; }
        [Display(Name = "ImagePath")]
        public string ImagePath { get; set; }
        //[Required]
        [ValidateImageCustom(maxContentLength:524288, allowedFileExtensions:new string[] { ".jpg", ".jpeg", ".gif", ".png", ".bmp" }, imageWidthMax:1280, imageHeightMax:1280, minContentLength:10250, imageWidthMin:500, imageHeightMin:300)]
        //[Display(Name = "Image")]
        public IFormFile ImageFile { get; set; }
        public byte[] FileByte { get; set; }
        public int TotalItems { get; set; }
        [Display(Name = "CreatedOn")]
        public DateTime? CreatedOn { get; set; }
        public Guid? UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public List<Guid> SelectIdList { get; set; }
    }

    

}
