﻿Create procedure [dbo].[SApp_GetCompanyDetailsbyProjectID]
(
	@CompanyId uniqueidentifier,
	@ProjectId uniqueidentifier
)
as
/*================================================================================
Procedure Name: [SApp_GetCompanyDetailsbyProjectID]
Author: Pooja
Create date: 05/06/2019
Description: Get list from company table by project ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
05/06/2019	Sai Krishna	    Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	 select comp.Id as CompanyId,
	 comp.CompanyName,
	 comp.CompanyCode,
	 comp.NatureId,
	 comp.ParentCompanyName,
	 comp.CompanyLocation,
	 comp.Telephone,
	 comp.Telephone2,
	 comp.Telephone3,
	 comp.Email,
	 comp.Email2,
	 comp.Email3,
	 comp.Website,
	 comp.YearOfEstb,
	 comp.CompanyStageId,
	 crm.RegionId,
	 comp.NoOfEmployees,
	 ckem.Id as KeyEmpId,
	 ckem.KeyEmployeeName,
	 ckem.KeyEmployeeDesignation,
	 ckem.KeyEmployeeEmailId,
	 ckem.KeyEmployeeComments,
	 cbdm.Id as BoardOfDirectorsId,
	 cbdm.CompanyBoardOfDirecorsName,
	 cbdm.CompanyBoardOfDirecorsOtherCompanyName,
	 cbdm.CompanyBoardOfDirecorsOtherCompanyDesignation,
	 comp.RankNumber,
	 comp.RankRationale,
	 cshpm.ShareHoldingName,
	 cshpm.EntityTypeId,
	 cshpm.ShareHoldingPercentage
	from Company comp
	inner Join CompanyProjectMap cpmap
	on comp.Id = cpmap.CompanyId 
	and cpmap.ProjectId = @ProjectId 
	and comp.IsDeleted = 0 
	right join CompanyBoardOfDirecorsMap cbdm
	on comp.Id = cbdm.CompanyId
	and cbdm.IsDeleted = 0
	left join CompanyKeyEmployeesMap ckem
	on comp.Id = ckem.CompanyId
	left join CompanyRegionMap crm
	on comp.Id = crm.CompanyId
	left join ShareHoldingPatternMap cshpm
	on comp.Id = cshpm.CompanyId
	where comp.IsDeleted = 0 and comp.Id = @CompanyId 
end