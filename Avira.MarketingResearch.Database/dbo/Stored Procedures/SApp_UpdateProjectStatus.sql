﻿CREATE procedure [dbo].[SApp_UpdateProjectStatus]      
(@Id uniqueidentifier,
@ProjectStatusID UNIQUEIDENTIFIER=null 
    
)  
as      
/*================================================================================      
Procedure Name: SApp_UpdateProjectStatus     
Author: Pooja      
Create date: 04/25/2019      
Description: update a record in Project table by ID to update project status 
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
04/25/2019 Pooja   Initial Version     
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
         
declare @ErrorMsg NVARCHAR(2048);  
BEGIN TRY       
--update record  
if @ProjectStatusID is null
set @ProjectStatusID=(select Id from ProjectStatus
where ProjectStatusName = 'Ongoing')
    
update [dbo].[Project]      
set [ProjectStatusID] = @ProjectStatusID   
  where ID = @Id     
     
SELECT StoredProcedureName ='SApp_UpdateProjectStatus',Message =@ErrorMsg,Success = 1;       
    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);      
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());   
  
set @ErrorMsg = 'Error while updating project, please contact Admin for more details.'        
   
 SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
END CATCH      
      
    
    
      
end