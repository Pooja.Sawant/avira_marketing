﻿
var popup, dataTable; var pp;

$(document).ready(function () {

    Load();

});

function Load() {
    $.ajax({
        url: "RawMaterial/Load/",
        method: "GET",
        dataType: "JSON",
        success: function (data) {
            pp = $("#data-table1").DataTable({
                destroy: true,
                data: data.rawmaterial,
                "columns": [
                    { data: "rawMaterialName" },
                    { data: "description" },
                    { data: "createdOn" },
                    { data: "userCreatedById" },
                    { data: "modifiedOn" },
                    { data: "userModifiedById" },
                    { data: "isDeleted" },
                    { data: "deletedOn" },
                    { data: "userDeletedById" },
                    {
                        data: "id",
                        render: function (data) {
                            return "<a class='btn btn-default btn-xs btn-rounded p-l-10 p-r-10 editor_edit' href='@Url.Action("Edit","Edit")?id='" + data + "')><i class='fa fa-cog'></i>  Edit</a><a class='btn btn-default btn-xs btn-rounded p-l-10 p-r-10 editor_remove' style='margin-left:5px' href='@Url.Action("Delete","Delete")?id='" + data + "')><i class='fa fa-trash'></i> Delete</a>";
                        }
                    }
                ],

                "columnDefs": [
                    {
                        "targets": [2],
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": [3],
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": [4],
                        "visible": false
                    },
                    {
                        "targets": [5],
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": [6],
                        "visible": false
                    },
                    {
                        "targets": [7],
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": [8],
                        "visible": false
                    },
                ],
                language: {
                    "emptyTable": "no data found."
                }
            });
        }
    });


}
