﻿CREATE TABLE [dbo].[TrendValueMap] (
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [TrendId]           UNIQUEIDENTIFIER NOT NULL,
    [ValueConversionId] UNIQUEIDENTIFIER NOT NULL,
    [TimeTagId]         UNIQUEIDENTIFIER NOT NULL,
    [Rationale]         NVARCHAR (512)   NOT NULL,
    [Amount]            DECIMAL (19, 4)  NOT NULL,
    [Year]              INT              NOT NULL,
    [CreatedOn]         DATETIME2 (7)    NULL,
    [UserCreatedById]   UNIQUEIDENTIFIER NULL,
    [ModifiedOn]        DATETIME2 (7)    NULL,
    [UserModifiedById]  UNIQUEIDENTIFIER NULL,
    [IsDeleted]         BIT              NOT NULL,
    [DeletedOn]         DATETIME2 (7)    NULL,
    [UserDeletedById]   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_TrendValueMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TrendValueMap_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_TrendValueMap_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_TrendValueMap_TimeTag] FOREIGN KEY ([TimeTagId]) REFERENCES [dbo].[TimeTag] ([Id]),
    CONSTRAINT [FK_TrendValueMap_ValueConversion] FOREIGN KEY ([ValueConversionId]) REFERENCES [dbo].[ValueConversion] ([Id])
);




GO
CREATE NONCLUSTERED INDEX [IX_TrendValueMap]
    ON [dbo].[TrendValueMap]([TrendId] ASC);

