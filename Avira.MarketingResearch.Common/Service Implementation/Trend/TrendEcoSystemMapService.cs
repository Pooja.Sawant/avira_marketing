﻿using Avira.MarketingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.Trend
{
    public class TrendEcoSystemMapService : ITrendEcoSystemMapService
    {
        private readonly ITrendEcoSystemMapRepository _trendEcoSystemMapRepository;
        private readonly ITrendNoteRepository _iTrendNoteRepository;

        public TrendEcoSystemMapService(ITrendEcoSystemMapRepository trendEcoSystemMapRepository, ITrendNoteRepository iTrendNoteRepository)
        {
            _trendEcoSystemMapRepository = trendEcoSystemMapRepository;
            _iTrendNoteRepository = iTrendNoteRepository;
        }

        public IEnumerable<TrendEcoSystemMapViewModel> GetAll(Guid? Id,int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false)
        {
            return _trendEcoSystemMapRepository.GetAll(Id,PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude = false);
        }

        public Result<SpTransactionMessage> InsertUpdate(MainTrendEcoSystemMapInsertUpdateDbViewModel tfc, AviraUser currenUser)
        {

            var res = _trendEcoSystemMapRepository.InsertUpdate(tfc);
            tfc.TrendNoteModel.Id = Guid.NewGuid();
            tfc.TrendNoteModel.UserCreatedById = tfc.AviraUserId;
            tfc.TrendNoteModel.TrendId = tfc.TrendId;
            _iTrendNoteRepository.Insert(tfc.TrendNoteModel);

            return res;
        }
    }
}
