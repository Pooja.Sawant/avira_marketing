﻿
CREATE PROCEDURE [dbo].[SFab_GetModuleType]
	-- Add the parameters for the stored procedure here
	@AviraUserId Uniqueidentifier=null
As    
/*================================================================================    
Procedure Name: [SFab_GetModuleType] 
Get Data from Table
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
28-April-2020 Harshal   Initial Version    
================================================================================*/
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;
SELECT Id,ModuleName,DisplayName 
FROM  ModuleType
WHERE ParentModuleTypeId is null ORDER BY DisplayName
END