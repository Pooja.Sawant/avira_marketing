﻿using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Utility;
using Avira.MarketingResearch.WebApi;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.Repository.CoreInterfaces.Masters;
using Avira.MarketingResearch.Common.IService.Masters;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using System;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Common.IService.CompanyProfile;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EcoSystemPresenceController : AviraBaseController
    {
        private ILoggerManager _logger;

        public readonly IEcoSystemPresenceService _ecoSystemPresenceService;
        public readonly IEcoSystemPresenceRepository _ecoSystemPresenceRepository;

        public EcoSystemPresenceController(
            IEcoSystemPresenceService ecoSystemPresenceService,
            IEcoSystemPresenceRepository ecoSystemPresenceRepository,
            ILoggerManager logger)
        {
            _ecoSystemPresenceRepository = ecoSystemPresenceRepository;
            _ecoSystemPresenceService = ecoSystemPresenceService;
            _logger = logger;
        }

        [HttpGet]
        [Route("GetEcoSystemPresenceMapByCompanyId")]
        public ActionResult<IEnumerable<EcoSystemPresenceMapViewModel>> GetEcoSystemPresenceMapByCompanyId(Guid CompanyId)
        {
            List<EcoSystemPresenceMapViewModel> res = null;
            try
            {
                res = _ecoSystemPresenceService.GetEcoSystemPresenceMapByCompanyId(CompanyId).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);

            }
            return res;
        }

        [HttpGet]
        [Route("GetSectorPresenceMapByCompanyId")]
        public ActionResult<IEnumerable<SectorPresenceMapViewModel>> GetSectorPresenceMapByCompanyId(Guid CompanyId)
        {
            List<SectorPresenceMapViewModel> res = null;
            try
            {
                res = _ecoSystemPresenceService.GetSectorPresenceMapByCompanyId(CompanyId).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);

            }
            return res;
        }

        [HttpGet]
        [Route("GetEcoSystemPresenceKeyCompByCompanyId")]
        public ActionResult<IEnumerable<EcoSystemPresenceKeyCompViewModel>> GetEcoSystemPresenceKeyCompByCompanyId(Guid CompanyId)
        {
            List<EcoSystemPresenceKeyCompViewModel> res = null;
            try
            {
                res = _ecoSystemPresenceService.GetEcoSystemPresenceKeyCompByCompanyId(CompanyId).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);

            }
            return res;
        }

        [HttpPost]
        public ActionResult Post([FromBody] EcoSystemPresenceDbViewModel viewModel)
        {
            viewModel.UserCreatedById = CurrentUser.Id;
            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new EcoSystem Presence Mapping, please contact Admin for more details."
            };
            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                var result = _ecoSystemPresenceService.InsertUpdate(viewModel, CurrentUser);
                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Company EcoSystem Presence Mapping created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Ok(viewData);
        }
    }
}