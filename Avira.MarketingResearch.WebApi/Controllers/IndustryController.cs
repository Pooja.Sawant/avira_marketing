﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IndustryController : AviraBaseController
    {
        public readonly IIndustryRepository _iIndustryRepository;
        public readonly IIndustryService _iIndustryService;
        private ILoggerManager _logger;

        public IndustryController(
            IIndustryService iIndustryService,
            IIndustryRepository iIndustryRepository,
            ILoggerManager logger)
        {
            _iIndustryRepository = iIndustryRepository;
            _iIndustryService = iIndustryService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<IndustryViewModel>> Get(int PageStart = 0, int pageSize = 0, int SortDirection = 1, string OrdbyByColumnName = "CreatedDate", string Search = "", bool includeDeleted = false)
        {
            List<IndustryViewModel> res = null;
            try
            {
                res = _iIndustryService.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, includeDeleted).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;

        }

        [HttpGet]
        [Route("GetAllParent")]
        public ActionResult<IEnumerable<IndustryViewModel>> GetAllParent()
        {
            List<IndustryViewModel> res = null;
            try
            {
                res= _iIndustryService.GetAllParentIndustry().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet("{id}")]
        public ActionResult<IndustryViewModel> Get(Guid id)
        {
            IndustryViewModel industry = null;
            try
            {
                industry = _iIndustryService.GetById(id);
                if (industry == null)
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return _iIndustryService.GetById(id);
        }

        [HttpPost]
        public ActionResult Post([FromBody] IndustryInsertDbViewModel viewModel)
        {
            viewModel.CreatedOn = System.DateTime.UtcNow;
            viewModel.UserCreatedById = CurrentUser.Id;
            viewModel.Id = Guid.NewGuid();

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new Industry, please contact Admin for more details."
            };

            try
            {

                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                var result = _iIndustryService.Create(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Industry name " + viewModel.IndustryName + " is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }


        [HttpPut]
        public ActionResult Put([FromBody] IndustryUpdateDbViewModel viewModel)
        {
            if (viewModel.IsDeleted)
            {
                viewModel.DeletedOn = System.DateTime.UtcNow;
                viewModel.UserDeletedById = CurrentUser.Id;
            }
            else
            {
                viewModel.ModifiedOn = System.DateTime.UtcNow;
                viewModel.UserModifiedById = CurrentUser.Id;
            }
            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while updating a Industry, please contact Admin for more details."
            };
            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                Result<SpTransactionMessage> result = _iIndustryService.Update(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    if (viewModel.IsDeleted)
                    {
                        viewData.Message = "Industry name " + viewModel.IndustryName + " is deleted.";

                    }
                    else
                    {
                        viewData.Message = "Industry name " + viewModel.IndustryName + " is updated successfully.";
                    }
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }

        [Route("DeleteSegment")]
        [HttpPut]
        public ActionResult DeleteSegment([FromBody] IndustryUpdateDbViewModel viewModel)
        {

            viewModel.DeletedOn = System.DateTime.UtcNow;
            viewModel.UserDeletedById = CurrentUser.Id;
            viewModel.IsDeleted = true;

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while deleting a new Industry, please contact Admin for more details."
            };
            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                Result<SpTransactionMessage> result = _iIndustryService.Update(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;

                    viewData.Message = "Industry name " + viewModel.IndustryName + " is deleted.";

                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }
    }
}