﻿CREATE TABLE [dbo].[News] (
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [Date]              DATETIME2 (7)    NOT NULL,
    [News]              NVARCHAR (MAX)   NOT NULL,
    [NewsCategoryId]    UNIQUEIDENTIFIER NOT NULL,
    [ImportanceId]      UNIQUEIDENTIFIER NULL,
    [ImpactDirectionId] UNIQUEIDENTIFIER NULL,
    [OtherParticipants] NVARCHAR (128)   NULL,
    [Remarks]           NVARCHAR (128)   NULL,
    [CreatedOn]         DATETIME2 (7)    NULL,
    [UserCreatedById]   UNIQUEIDENTIFIER NULL,
    [ModifiedOn]        DATETIME2 (7)    NULL,
    [UserModifiedById]  UNIQUEIDENTIFIER NULL,
    [IsDeleted]         BIT              CONSTRAINT [DF_News_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]         DATETIME2 (7)    NULL,
    [UserDeletedById]   UNIQUEIDENTIFIER NULL,
    [CompanyID]         UNIQUEIDENTIFIER NULL,
    [ProjectID]         UNIQUEIDENTIFIER NULL,
    [Marketid]          UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_News] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_News_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_News_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_News_Company] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Company] ([Id]),
    CONSTRAINT [FK_News_ImpactDirection] FOREIGN KEY ([ImpactDirectionId]) REFERENCES [dbo].[ImpactDirection] ([Id]),
    CONSTRAINT [FK_News_Importance] FOREIGN KEY ([ImportanceId]) REFERENCES [dbo].[Importance] ([Id]),
    CONSTRAINT [FK_News_Market] FOREIGN KEY ([Marketid]) REFERENCES [dbo].[Market] ([Id]),
    CONSTRAINT [FK_News_NewsCategory] FOREIGN KEY ([NewsCategoryId]) REFERENCES [dbo].[NewsCategory] ([Id])
);







