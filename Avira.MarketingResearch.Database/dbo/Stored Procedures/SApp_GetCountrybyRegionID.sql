﻿CREATE procedure [dbo].[SApp_GetCountrybyRegionID]  
(@RegionID uniqueidentifier = null,  
@IncludeDeleted bit = 0,  
@IncludeUnassigned bit = 0)  
as  
/*================================================================================  
Procedure Name: SApp_GetCountrybyRegionID  
Author: Sharath  
Create date: 03/19/2019  
Description: Get list from Country Region Map table  
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
03/19/2019 Sharath   Initial Version  
================================================================================*/  
BEGIN  
SET NOCOUNT ON;  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
  
if @IncludeDeleted is null set @IncludeDeleted = 0  
if @IncludeUnassigned is null set @IncludeUnassigned = 0  
  
SELECT [Country].[Id] as [CountryId],  
 [CountryCode],  
    [CountryName],  
 CAST(CASE WHEN [CountryRegionMap].IsDeleted = 0 THEN 1 ELSE 0 END AS BIT)AS IsAssigned  
FROM [dbo].[Country]  
left join [dbo].[CountryRegionMap]  
on [Country].Id = [CountryRegionMap].CountryId  
and [CountryRegionMap].RegionId = @RegionID  
where (@IncludeDeleted = 1 or [Country].IsDeleted = 0)  
and (@IncludeUnassigned = 1 OR [CountryRegionMap].IsDeleted = 0)  
end