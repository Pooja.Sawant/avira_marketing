﻿using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.ImportExport
{
    public class CPFundamentalImportExportRepository: ICPFundamentalImportExportRepository
    {
        protected IDbHelper _dbHelper;
        public CPFundamentalImportExportRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        IEnumerable<ImportCommonResponseModel> ICPFundamentalImportExportRepository.Create(CompanyFundamentalsViewModel viewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(ImportExportQueries.StoreProcInsertCompanyFundamentals, new
            {
                @TemplateName = viewModel.TemplateName,
                @ImportFileName = viewModel.ImportFileName,

                @JsonFundamentals = viewModel.JsonCompanyBasic,
                @JsonRevenue=viewModel.JsonRevenue,
                @JsonCompanySegment = viewModel.JsonCompanySegment,
                @JsonCompanyKeyword = viewModel.JsonCompanyKeyword,
                @JsonCompanyAnalystView = viewModel.JsonCompanyAnalystView,
                @UserCreatedById = viewModel.UserCreatedById
            }, CommandType.StoredProcedure);

            IEnumerable<ImportCommonResponseModel> spTransactionMessage1 = new List<ImportCommonResponseModel>();
            if (spTransactionMessage.IsSuccess == true)
            {
                //var spTransactionMessage1 = _dbHelper.ExecuteQuery(ImportExportQueries.StoreProcUpdateMarketSizing1, null, CommandType.StoredProcedure);
                var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
                var spTransactionMain = dapper.Query<ImportCommonResponseModel>(ImportExportQueries.StoreProcUpdateCompanyFundamentals
                            , null
                            , null, true, 300, CommandType.StoredProcedure);

                if (spTransactionMessage1 != null)
                {
                    spTransactionMessage1 = spTransactionMain;
                }
            }
            return spTransactionMessage1;
        }
    }
}
