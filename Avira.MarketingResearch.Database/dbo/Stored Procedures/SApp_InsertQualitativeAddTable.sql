﻿--sp_helptext SApp_GetQualitativeAddTableById

  
CREATE PROCEDURE [dbo].[SApp_InsertQualitativeAddTable]  
(  
 @Id     uniqueidentifier=null,  
 @ProjectId   uniqueidentifier=null,  
 @TableName   nvarchar(50)=null,  
 @TableMetadata  nvarchar(512)=null,  
 @TableData  nvarchar(512)=null,  
 @CreatedOn   datetime2(7)=null,  
 @UserCreatedById uniqueidentifier=null,  
 @ModifiedOn   datetime2(7)=null,  
 @UserModifiedById uniqueidentifier=null,  
 @IsDeleted   bit=0,  
 @DeletedOn   datetime2(7)=null,  
 @UserDeletedById uniqueidentifier=null  
)  
as  
/*================================================================================  
Procedure Name: SApp_InsertQualitativeAddTable  
Author: Laxmikant  
Create date: 06/20/2019  
Description: Insert or update a record in QualitativeTableTab table  
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
06/20/2019 Laxmikant   Initial Version  
================================================================================*/  
BEGIN  
SET NOCOUNT ON;  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
declare @ErrorMsg NVARCHAR(2048);  
IF exists(select 1 from [dbo].[QualitativeTableTab]   
   where ((@Id is null or [Id] !=  @Id)and  TableName = @TableName AND IsDeleted = 0))  
begin  
set @ErrorMsg = 'Qualitative with Name "'+ @TableName + '" already exists.';  
SELECT StoredProcedureName ='SApp_InsertQualitativeAddTable',Message =@ErrorMsg,Success = 0;    
RETURN;    
end  
BEGIN TRY  
  
INSERT INTO [dbo].[QualitativeTableTab]  
(  
 Id,      
 ProjectId,    
 TableName,    
 TableMetadata,   
 TableData,   
 CreatedOn,    
 UserCreatedById,  
 ModifiedOn,    
 UserModifiedById,  
 IsDeleted,    
 DeletedOn,    
 UserDeletedById  
)  
values  
(  
 @Id,      
 @ProjectId,    
 @TableName,    
 @TableMetadata,   
 @TableData,   
 @CreatedOn,    
 @UserCreatedById,  
 @ModifiedOn,    
 @UserModifiedById,  
 @IsDeleted,    
 @DeletedOn,    
 @UserDeletedById  
)  
  
SELECT StoredProcedureName ='SApp_InsertQualitativeAddTable',Message =@ErrorMsg,Success = 1;     
END TRY  
BEGIN CATCH  
    -- Execute the error retrieval routine.  
 DECLARE @ErrorNumber int;  
 DECLARE @ErrorSeverity int;  
 DECLARE @ErrorProcedure varchar(100);  
 DECLARE @ErrorLine int;  
 DECLARE @ErrorMessage varchar(500);  
  
  SELECT @ErrorNumber = ERROR_NUMBER(),  
        @ErrorSeverity = ERROR_SEVERITY(),  
        @ErrorProcedure = ERROR_PROCEDURE(),  
        @ErrorLine = ERROR_LINE(),  
        @ErrorMessage = ERROR_MESSAGE()  
  
 insert into dbo.Errorlog(ErrorNumber,  
       ErrorSeverity,  
       ErrorProcedure,  
       ErrorLine,  
       ErrorMessage,  
       ErrorDate)  
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());  
  
 set @ErrorMsg = 'Error while creating a new Qualitative Table, please contact Admin for more details.';      
--THROW 50000,@ErrorMsg,1;        
--return(1)    
SELECT Number = @ErrorNumber,   
  Severity =@ErrorSeverity,  
  StoredProcedureName =@ErrorProcedure,  
  LineNumber= @ErrorLine,  
  Message =@ErrorMsg,  
  Success = 0;       
END CATCH  
end