﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.CompanyProfile
{
    public class CompanyTrendsMapViewModel
    {
        public Guid CompanyId { get; set; }
        public Guid ProjectId { get; set; }
        public string TrendName { get; set; }
        public string Department { get; set; }
        public Guid ImportanceId { get; set; }
        public string ImportanceName { get; set; }
        public Guid ImpactId { get; set; }
        public string ImpactName { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public Guid Id { get; set; }
        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "CompanyTrendsMap";
            }
        }
    }

    public class MainCompanyTrendsMapInsertUpdateDbViewModel
    {
        public Guid CompanyId { get; set; }
        public Guid ProjectId { get; set; }
        public List<CompanyTrendsMapViewModel> CompanyTrendsMapInsertUpdate { get; set; }
        public Guid AviraUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public CompanyNoteViewModel CompanyNoteModel { get; set; }
    }

   
}
