﻿using Avira.MarketingResearch.Common.IService.ImportExport;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.ImportExport
{
    public class QualitativeAnalysisImportExportService : IQualitativeAnalysisImportExportService
    {
        private readonly IQualitativeAnalysisImportExportRepository _qualitativeAnalysisImportExportService;
        public QualitativeAnalysisImportExportService(IQualitativeAnalysisImportExportRepository buildLevelRepository)
        {
            _qualitativeAnalysisImportExportService = buildLevelRepository;
        }

        public IEnumerable<ImportCommonResponseModel> Create(QualitativeAnalysisImportModel viewModel)
        {
            viewModel.JsonNewsList = JsonConvert.SerializeObject(viewModel.NewsList);
            viewModel.JsonAnalysisList = JsonConvert.SerializeObject(viewModel.AnalysisList);
            viewModel.JsonDescriptionList = JsonConvert.SerializeObject(viewModel.DescriptionList);
            viewModel.JsonQuoteList = JsonConvert.SerializeObject(viewModel.QuoteList);
            viewModel.JsonSegmentsList = JsonConvert.SerializeObject(viewModel.SegmentsList);
            viewModel.JsonRegionList = JsonConvert.SerializeObject(viewModel.RegionList);
            viewModel.JsonESMSegmentList = JsonConvert.SerializeObject(viewModel.ESMSegmentList);
            viewModel.JsonESMAnalysisList = JsonConvert.SerializeObject(viewModel.ESMAnalysisList);
            var trendImportResponse = _qualitativeAnalysisImportExportService.Create(viewModel);
            return trendImportResponse;
        }
    }
}
