﻿
Create procedure [dbo].[SFab_insertCustomerUserSubscriptionMap]
(
	@CustomerId uniqueidentifier,
	@CustomerUserId uniqueidentifier,
	@SegmentList dbo.udtGUIDVarcharValue READONLY,
	@UserCreatedById uniqueidentifier  
)
as
/*================================================================================
Procedure Name: [SFab_insertCustomerUserSubscriptionMap]
Author: Adarsh
Create date: 23/05/2019
Description: Insert a record into Customer User Subscription Table
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
23/05/2019	Sai Krishna			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ErrorMsg NVARCHAR(2048);

BEGIN TRY

INSERT INTO [dbo].[UserSegmentMap]
           (
			[Id]
			,[CustomerId]
           ,[CustomerUserId]
           ,[SegmentType]
           ,[SegmentId]
		   ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted]
           )
   Select  NEWID(),
		   @CustomerId,
		   @CustomerUserId,
		   [Value],
		   [ID],
		   GETDATE(),
		   @UserCreatedById,
		   0
		 From @SegmentList

--select @Id;
--return(0)
SELECT StoredProcedureName ='SFab_insertCustomerSubscriptionMap',Message =@ErrorMsg,Success = 1;  
END TRY
BEGIN CATCH
    -- Execute the error retrieval routine.
	DECLARE @ErrorNumber	int;
	DECLARE @ErrorSeverity	int;
	DECLARE @ErrorProcedure	varchar;
	DECLARE @ErrorLine	int;
	DECLARE @ErrorMessage	varchar;

	 SELECT @ErrorNumber = ERROR_NUMBER(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorProcedure = ERROR_PROCEDURE(),
        @ErrorLine = ERROR_LINE(),
        @ErrorMessage = ERROR_MESSAGE()

	insert into dbo.Errorlog(ErrorNumber,
							ErrorSeverity,
							ErrorProcedure,
							ErrorLine,
							ErrorMessage,
							ErrorDate)
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());

	set @ErrorMsg = 'Error while permission to role, please contact Admin for more details.';

SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;

END CATCH


end