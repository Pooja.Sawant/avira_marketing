﻿using System;
using System.Collections.Generic;
using System.Text;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;

namespace Avira.MarketingResearch.Model.ViewModels
{
    public class ViewData : IViewModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public Guid Id { get; set; }
        public List<MarketSizingImportViewModel> Data { get; set; }
        public List<TrendImportViewModel> TrendData { get; set; }
        public List<ImportCommonResponseModel> cpData { get; set; }
        public List<CPFinancialImportModel> CPFData { get; set; }
        public List<ImportCommonResponseModel> CBIData { get; set; }
    }
    public class MultiSelectViewData : IMultiViewData
    {
        public string Label { get; set; }
        public Guid? Value { get; set; }
    }
}
