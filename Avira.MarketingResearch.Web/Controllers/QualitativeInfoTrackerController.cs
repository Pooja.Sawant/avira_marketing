﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Model.ViewModels.QualitativeInfo;
using Avira.MarketingResearch.Web.Utils;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.Web.Controllers
{
    public class QualitativeInfoTrackerController : BaseController
    {
        private ILoggerManager _logger;
        public readonly IServiceRepository _serviceRepository;
        public QualitativeInfoTrackerController(IServiceRepository serviceRepository, ILoggerManager logger)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
        }

        public IActionResult Index(string Id)
        {
            try
            {
                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, new Guid(Id));
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View();
        }
        public ActionResult LoadData(QualitativeInfoTrackerViewModel model)
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            int totalRecords = 0;
            List<QualitativeInfoTrackerViewModel> data = null;
            try
            {
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                string search = Request.Form["search[value]"];

                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDir = Request.Form["order[0][dir]"].FirstOrDefault();
                var SortDirection = -1;
                if (sortColumnDir == "asc")
                {
                    SortDirection = 1;
                }
                else
                {
                    SortDirection = -1;
                }

                int skip = start != null ? Convert.ToInt32(start) : 0;
                int pageSize = length.ToString() != null ? Convert.ToInt32(length) : 0;
                int PageStart = start.ToString() != null ? Convert.ToInt32(start) : 0;
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();

                HttpResponseMessage response = _serviceRepository.GetResponse(contollerName, model.ProjectId, PageStart, pageSize, SortDirection, sortColumn, search);
                response.EnsureSuccessStatusCode();
                List<QualitativeInfoTrackerViewModel> QualitativeInfoTrackerViewModelFromDB = response.Content.ReadAsAsync<List<QualitativeInfoTrackerViewModel>>().Result;
                List<QualitativeInfoTrackerViewModel> QualitativeInfoTrackerViewModelLocal = new List<QualitativeInfoTrackerViewModel>();
                var QualitativeInfoTabNameList = new List<KeyValuePair<int, string>>();
                //QualitativeInfoTabNameList.Add(new KeyValuePair<int, string>(1, "Analyst Views"));
                //QualitativeInfoTabNameList.Add(new KeyValuePair<int, string>(2, "Descriptions"));
                //QualitativeInfoTabNameList.Add(new KeyValuePair<int, string>(3, "DROC Analysis"));
                //QualitativeInfoTabNameList.Add(new KeyValuePair<int, string>(4, "Images"));
                //QualitativeInfoTabNameList.Add(new KeyValuePair<int, string>(5, "Porter's Five Force Analysis"));
                foreach (var lst in QualitativeInfoTabNameList)
                {
                    QualitativeInfoTrackerViewModel Newobj = new QualitativeInfoTrackerViewModel();
                    Newobj.TableName = lst.Value;
                    Newobj.ProjectId = model.ProjectId;
                    QualitativeInfoTrackerViewModelLocal.Add(Newobj);
                }
                if (QualitativeInfoTrackerViewModelFromDB != null && QualitativeInfoTrackerViewModelFromDB.Count > 0)
                {
                    foreach (var obj in QualitativeInfoTrackerViewModelFromDB)
                    {
                        QualitativeInfoTrackerViewModelLocal.Add(obj);
                    }
                }

                data = QualitativeInfoTrackerViewModelLocal;
          
                foreach (var item in QualitativeInfoTrackerViewModelLocal)
                {
                    totalRecords = item.TotalItems;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data});

        }
    }
}