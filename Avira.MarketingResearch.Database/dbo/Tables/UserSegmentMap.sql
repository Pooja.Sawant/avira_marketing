﻿CREATE TABLE [dbo].[UserSegmentMap] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [CustomerId]       UNIQUEIDENTIFIER NULL,
    [CustomerUserId]   UNIQUEIDENTIFIER NULL,
    [SegmentType]      NVARCHAR (150)   NULL,
    [SegmentId]        UNIQUEIDENTIFIER NULL,
    [CreatedOn]        DATETIME         NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME         NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NULL,
    [DeletedOn]        DATETIME         NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_UserSegmentMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_UserSegmentMap_AviraUser] FOREIGN KEY ([CustomerUserId]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_UserSegmentMap_AviraUser1] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_UserSegmentMap_AviraUser2] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_UserSegmentMap_AviraUser3] FOREIGN KEY ([UserDeletedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_UserSegmentMap_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([Id])
);

