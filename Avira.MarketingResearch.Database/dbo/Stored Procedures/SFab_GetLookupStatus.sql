﻿
CREATE PROCEDURE [dbo].[SFab_GetLookupStatus]
(
	@StatusType nvarchar(100)
)
 AS      
/*================================================================================      
Procedure Name: SFab_GetLookupStatus      
Author: Harshal      
Create date: 01/03/2020      
Description: Get list from LookupStatus BY Type      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
01/03/2020 Harshal   Initial Version      
================================================================================*/      
      
BEGIN      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED      

	SELECT Id,
		StatusType,
		StatusName,
		Description 
	FROM LookupStatus 
	WHERE StatusType = @StatusType
	ORDER BY SeqNumber, StatusName 
      
 END