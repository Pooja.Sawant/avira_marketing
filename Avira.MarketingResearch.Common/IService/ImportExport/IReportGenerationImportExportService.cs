﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.ImportExport
{
    public interface IReportGenerationImportExportService
    {
        List<ImportCommonResponseModel> Create(ReportGenerationImportModel viewModel, AviraUser currentUser);
    }
}
