﻿
CREATE PROCEDURE [dbo].[SApp_CompanyGridWithFilters]
	(@CompanyIDList dbo.udtUniqueIdentifier READONLY,
	@CompanyCode nvarchar(256)='',
	@CompanyName nvarchar(256)='',
	@Description nvarchar(256)='',
	@Telephone nvarchar(20)='', 
	@Telephone2 nvarchar(20)='',
	@Email nvarchar(20)='',
	@Email2 nvarchar(20)='', 
	@ParentCompanyName nvarchar(256)='',
	@ProfiledCompanyName nvarchar(256)='',
	@Website nvarchar(128)='',
	@YearOfEstb INT=0,
	@ParentCompanyYearOfEstb INT=0,
	@includeDeleted bit = 0,
	@OrdbyByColumnName NVARCHAR(50) ='CreatedDate',
	@SortDirection INT = -1,
	@PageStart INT = 0,
	@PageSize INT = null)
	AS
/*================================================================================
Procedure Name: SApp_CompanyGridWithFilters
Author: Harshal
Create date: 03/26/2019
Description: Get list from comapany table with filters
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
03/26/2019	Harshal			Initial Version
__________	____________	______________________________________________________
04/01/2019	Pooja			For Client side paging comment pagesize and null init 
================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
if @includeDeleted is null set @includeDeleted = 0;
if len(@CompanyCode)>2 set @CompanyCode = '%'+ @CompanyCode+ '%'
else if @CompanyCode=''
if len(@CompanyName)>2 set @CompanyName = '%'+ @CompanyName + '%'
else set @CompanyName = ''
if len(@Description)>2 set @Description = '%'+ @Description + '%';
else set @Description = ''
if len(@Telephone)>2 set @Telephone = '%'+ @Telephone + '%';
else set @Telephone = ''
if len(@Telephone2)>2 set @Telephone2 = '%'+ @Telephone2 + '%';
else set @Telephone2 = ''
if len(@Email)>2 set @Email = '%'+ @Email + '%';
else set @Email = ''
if len(@Email2)>2 set @Email2 = '%'+ @Email2 + '%';
else set @Email2 = ''
if len(@ParentCompanyName)>2 set @ParentCompanyName = '%'+ @ParentCompanyName + '%';
else set @ParentCompanyName = ''
if len(@ProfiledCompanyName)>2 set @ProfiledCompanyName = '%'+ @ProfiledCompanyName + '%'; 
else set @ProfiledCompanyName = ''
if len(@Website)>2 set @Website = '%'+ @Website + '%';
else set @Website = '';
	if @PageSize is null or @PageSize =0
		set @PageSize = 10
	
	begin
	;WITH CTE_TBL AS (

	SELECT [Company].[Id],
	    [Company].[CompanyCode],
		[Company].[CompanyName],
		[Company].[Description],
		[Company].[Telephone],
		[Company].[Telephone2],
		[Company].[Email],
		[Company].[Email2],
		[Company].[Fax],
		[Company].[Fax2],
		[Company].[ParentCompanyName],
		[Company].[ProfiledCompanyName],
		[Company].[Website],
		[Company].[NoOfEmployees],
		[Company].[YearOfEstb],
		[Company].[ParentCompanyYearOfEstb],
		[Company].[IsDeleted],
		CASE
		WHEN @OrdbyByColumnName = 'CompanyName' THEN [Company].CompanyName
		WHEN @OrdbyByColumnName = 'Description' THEN [Company].[Description]
		ELSE [Company].[CompanyName]
	END AS SortColumn

FROM [dbo].[Company]
where (NOT EXISTS (SELECT * FROM @CompanyIDList) or  [Company].Id  in (select * from @CompanyIDList))
    AND (@CompanyCode = '' or [Company].[CompanyCode] like @CompanyCode)
	AND (@CompanyName = '' or [Company].[CompanyName] like @CompanyName)
	AND (@Description = '' or [Company].[Description] like @Description)
	AND (@Telephone = '' or [Company].[Telephone] like @Telephone)
	AND (@Telephone2 = '' or [Company].[Telephone2] like @Telephone2)
	AND (@Email = '' or [Company].[Email] like @Email)
	AND (@Email2 = '' or [Company].[Email2] like @Email2)
	AND (@ParentCompanyName = '' or [Company].[ParentCompanyName] like @ParentCompanyName)
	AND (@ProfiledCompanyName = '' or [Company].[ProfiledCompanyName] like @ProfiledCompanyName)
	AND (@Website = '' or [Company].[Website] like @Website)
	AND (@YearOfEstb = 0 or [Company].[YearOfEstb] = @YearOfEstb)
	AND (@ParentCompanyYearOfEstb = 0 or [Company].[ParentCompanyYearOfEstb] = @ParentCompanyYearOfEstb)
	AND (@includeDeleted = 1 or [Company].IsDeleted = 0)
	)

	SELECT 
		  CTE_TBL.*,
		  tCountResult.TotalItems
		FROM CTE_TBL
		CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult
		ORDER BY 
		  CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,
		  CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC 
		OFFSET @PageStart ROWS
		FETCH NEXT @PageSize ROWS ONLY;
		END
	END