﻿CREATE procedure [dbo].[SApp_GetAllTrendNews]  
  
as  
/*================================================================================  
Procedure Name: [SApp_GetAllTrendNews]  
Author: Gopi Krishna  
Create date: 14/05/2019  
Description: Get list from TrendNews  
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
14/05/2019 Gopi Krishna  Initial Version  
================================================================================*/  
BEGIN  
SET NOCOUNT ON;  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
 
SELECT *
 FROM [dbo].[News]  
where ([News].[IsDeleted] = 0)    
end