﻿CREATE TABLE [dbo].[MakeMyMarketImpact] (
    [Id]              UNIQUEIDENTIFIER NOT NULL,
    [MakeMyMarketId]  UNIQUEIDENTIFIER NULL,
    [ProjectId]       UNIQUEIDENTIFIER NULL,
    [CustomerId]      UNIQUEIDENTIFIER NULL,
    [MarketMoverType] NVARCHAR (256)   NULL,
    [EcoSystemType]   NVARCHAR (256)   NULL,
    [MetricType]      NVARCHAR (256)   NULL,
    [Year]            INT              NULL,
    [Value]           DECIMAL (18, 4)  NULL,
    [CAGR]            DECIMAL (18, 4)  NULL,
    [CreatedOn]       DATETIME         NULL,
    [CreatedBy]       UNIQUEIDENTIFIER NULL,
    [ModifiedOn]      DATETIME         NULL,
    [ModifiedBy]      UNIQUEIDENTIFIER NULL,
    [IsDeleted]       BIT              NULL,
    CONSTRAINT [PK_MakeMyMarketImpact] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_MakeMyMarketImpact_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([Id]),
    CONSTRAINT [FK_MakeMyMarketImpact_Customer1] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[Customer] ([Id]),
    CONSTRAINT [FK_MakeMyMarketImpact_Customer2] FOREIGN KEY ([ModifiedBy]) REFERENCES [dbo].[Customer] ([Id]),
    CONSTRAINT [FK_MakeMyMarketImpact_MakeMyMarket] FOREIGN KEY ([MakeMyMarketId]) REFERENCES [dbo].[MakeMyMarket] ([Id]),
    CONSTRAINT [FK_MakeMyMarketImpact_Project] FOREIGN KEY ([ProjectId]) REFERENCES [dbo].[Project] ([ID])
);

