﻿
CREATE procedure [dbo].[SFab_GetQualitativeQuotesDatabyID]
(@ProjectID uniqueidentifier=null,
 @CompanyID uniqueidentifier=null,
@includeDeleted bit = 0,
@AviraUserID uniqueidentifier = null)
as
/*================================================================================
Procedure Name: SFab_GetQualitativeQuotesDatabyID
Author: Harshal 
Create date: 09/04/2019
Description: Get list from QualitativeQuotes table by Filters
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
09/04/2019	Harshal			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if @includeDeleted is null set @includeDeleted = 0

SELECT [QualitativeQuotes].[Id]
      ,[QualitativeQuotes].[ProjectId]
	  ,[ProjectName]
      ,[QualitativeQuotes].[MarketId]
	  ,[MarketName]
      ,[ResourceName]
      ,[ResourceDesignation]
      ,[ResourceDesignationId]
      ,[ResourceCompanyName]
      ,[ResourceCompanyId]
      ,[DateOfQuote]
      ,[Quote]
      ,[OtherRemarks]
		
FROM [dbo].[QualitativeQuotes]
INNER JOIN Project Project ON Project.ID=QualitativeQuotes.ProjectId
LEFT JOIN Market Market ON Market.Id=QualitativeQuotes.MarketId
where (@ProjectId is null or QualitativeQuotes.ProjectId = @ProjectID)
AND  (@CompanyID is null or QualitativeQuotes.ResourceCompanyId = @CompanyID)
and (@includeDeleted = 1 or QualitativeQuotes.IsDeleted = 0)

end