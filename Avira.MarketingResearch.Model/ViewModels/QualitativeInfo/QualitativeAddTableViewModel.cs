﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.QualitativeInfo
{
    public class QualitativeAddTableViewModel
    {
        public Guid CompanyId { get; set; }
        public Guid ProjectId { get; set; } 
        public Guid Id { get; set; }  
        public string TableName { get; set; }
        public string TableMetaData { get; set; }
        public string TableData { get; set; } 
    }
    public class QualitativeGenerateTableViewModel
    {
        
        public string TableName { get; set; }
        public Guid ProjectId { get; set; }
        public int Rows { get; set; }
        public int Columns  { get; set; } 
        public bool IsHeaderRequired  { get; set; }
        public bool IsImageRequired { get; set; } 
        public string TableHTML { get; set; }
    }
    public class NameValue
    {
        public List<string> name { get; set; }
        public List<string> value { get; set; }
    }
    public class QualitativeGenerateTableCRUDViewModel
    {
        public Guid Id { get; set; }
        public QualitativeAddTableViewModel qualitativeAddTableViewModel { get; set; }
        public QualitativeGenerateTableViewModel qualitativeGenerateTableViewModel { get; set; }
        public QualitativeNoteViewModel QualitativeNoteViewModel { get; set; }
        public NameValue NameValue { get; set; }
        public Guid CompanyId { get; set; }
        public Guid ProjectId { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public Guid AviraUserId { get; set; }

        public Dictionary<string, string> TableData   { get; set; }
        public Dictionary<string, string> HeaderData { get; set; }

    }
}
 