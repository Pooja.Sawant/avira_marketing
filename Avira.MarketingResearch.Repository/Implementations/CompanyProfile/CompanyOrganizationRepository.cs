﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile
{
    public class CompanyOrganizationRepository : ICompanyOrganizationRepository
    {
        protected IDbHelper _dbHelper;
        public CompanyOrganizationRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        } 

        public IEnumerable<CompanyOrganizationViewModel> GetOrganizationList(Guid? Id)
        {
            //throw new NotImplementedException();
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<CompanyOrganizationViewModel>(CompanyOrganizationQueries.StoreProcSelectById
                            , new { @CompanyID = Id}
                            , null, true, null, CommandType.StoredProcedure);
        }
    }
}
