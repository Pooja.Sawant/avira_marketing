﻿CREATE TABLE [dbo].[SubscriptionSegmentMap] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [SubscriptionId]   UNIQUEIDENTIFIER NULL,
    [SegmentType]      NVARCHAR (150)   NULL,
    [SegmentId]        UNIQUEIDENTIFIER NULL,
    [CreatedOn]        DATETIME         NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATE             NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NULL,
    [DeletedOn]        DATETIME         NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_SubscriptionSegmentMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_SubscriptionSegmentMap_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_SubscriptionSegmentMap_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_SubscriptionSegmentMap_AviraUser2] FOREIGN KEY ([UserDeletedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_SubscriptionSegmentMap_Subscription] FOREIGN KEY ([SubscriptionId]) REFERENCES [dbo].[Subscription] ([Id])
);



