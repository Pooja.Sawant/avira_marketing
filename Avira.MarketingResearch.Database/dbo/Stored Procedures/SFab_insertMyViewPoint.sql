﻿
CREATE procedure [dbo].[SFab_insertMyViewPoint]      
(
	@TrendId uniqueidentifier,
	@CustomerId uniqueidentifier,
	@ValueYearList udtIntDecimal readonly
	--@Year int=null,
	--@TrendValue decimal(19,4)=null
)      
as      
/*================================================================================      
Procedure Name: [SFab_insertMyViewPoint]      
Author: Jagan      
Create date: 16/07/2019      
Description: Insert a record into TrendBuildMyViewPoint Table 
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
16/07/2019 Jagan   Initial Version 
11/02/2020 Pooja   Change Sp and datatype because wrong data insert     
================================================================================*/      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)
BEGIN
BEGIN TRY
IF exists(
select 1 
from TrendBuildMyViewPoint
WHERE TrendId = @TrendId and CustomerId = @CustomerId 
)
BEGIN
	Update TrendBuildMyViewPoint 
	set TrendValue=VYL.decimalValue,
	ModifiedOn = GETDATE(),
	UserModifiedById = @CustomerId 
	From @ValueYearList VYL
	where TrendId=@TrendId
	and Year = VYL.intValue and  CustomerId = @CustomerId
	--delete from TrendBuildMyViewPoint
	--where TrendId = @TrendId
SELECT StoredProcedureName ='SFab_insertMyViewPoint',Message =@ErrorMsg,Success = 1;     

END   
ELSE
BEGIN            
insert into [dbo].[TrendBuildMyViewPoint](
	        [Id],      
			[TrendId], 
			[CustomerId],     
			[Year],  
			[TrendValue],
			[CreatedOn],
			[UserCreatedById],      
			[IsDeleted] )     
	 Select NEWID(),
		   @TrendId, 
		   @CustomerId,      
		   VYL.intValue,       
		   VYL.decimalValue,  
		   GETDATE(), 
		   @CustomerId,
		   0  
	 From @ValueYearList VYL	
	        
SELECT StoredProcedureName ='SFab_insertMyViewPoint',Message =@ErrorMsg,Success = 1;     
END  
END TRY  
BEGIN CATCH    
    -- Execute the error retrieval routine.    
 DECLARE @ErrorNumber int;    
 DECLARE @ErrorSeverity int;    
 DECLARE @ErrorProcedure varchar(100);    
 DECLARE @ErrorLine int;    
 DECLARE @ErrorMessage varchar(500);    
    
  SELECT @ErrorNumber = ERROR_NUMBER(),    
        @ErrorSeverity = ERROR_SEVERITY(),    
        @ErrorProcedure = ERROR_PROCEDURE(),    
        @ErrorLine = ERROR_LINE(),    
        @ErrorMessage = ERROR_MESSAGE()    
    
 insert into dbo.Errorlog(ErrorNumber,    
       ErrorSeverity,    
       ErrorProcedure,    
       ErrorLine,    
       ErrorMessage,    
       ErrorDate)    
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());    
  
 set @ErrorMsg = 'Error while Inserting the viewpoint, please contact Admin for more details.';    
--THROW 50000,@ErrorMsg,1;      
--return(1)  
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;     
     
END CATCH    
END