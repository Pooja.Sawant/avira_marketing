﻿CREATE TABLE [dbo].[DecisionAlternativeTask] (
    [Id]                    UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [DecisionId]            UNIQUEIDENTIFIER NOT NULL,
    [DecisionAlternativeId] UNIQUEIDENTIFIER NOT NULL,
    [Description]           NVARCHAR (1024)  NOT NULL,
    [ResourceAssigned]      NVARCHAR (899)   NULL,
    [StatusId]              UNIQUEIDENTIFIER NOT NULL,
    [DueDate]               DATETIME2 (7)    NOT NULL,
    [StandpointId]          UNIQUEIDENTIFIER NULL,
    [CreatedOn]             DATETIME2 (7)    DEFAULT (getdate()) NOT NULL,
    [UserCreatedById]       UNIQUEIDENTIFIER NULL,
    [ModifiedOn]            DATETIME2 (7)    NULL,
    [UserModifiedById]      UNIQUEIDENTIFIER NULL,
    [StatusModifiedOn]      DATETIME2 (7)    NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([DecisionAlternativeId]) REFERENCES [dbo].[DecisionAlternative] ([Id]),
    FOREIGN KEY ([StandpointId]) REFERENCES [dbo].[LookupCategory] ([Id]),
    FOREIGN KEY ([StatusId]) REFERENCES [dbo].[LookupStatus] ([Id]),
    FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK__DecisionA__Decis__20844C07] FOREIGN KEY ([DecisionId]) REFERENCES [dbo].[Decision] ([Id])
);

