﻿
/*================================================================================      
Procedure Name: [SApp_GetCompanyTrendsMapByCompanyID]   
Author: Harshal      
Create date: 05/08/2019      
Description: Get Records froms ComapnyTrendsMap By CompanyId      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
05/08/2019 Harshal   Initial Version     
================================================================================*/  
CREATE PROCEDURE [dbo].[SApp_GetCompanyTrendsMapByCompanyID]     
	@CompanyID uniqueidentifier , 
	@includeDeleted bit = 0     
AS
BEGIN 
	SET NOCOUNT ON;  
	SELECT [CompanyTrendsMap].[Id]
	  ,[TrendName]
	  ,[Department]
      ,[CompanyId]
      ,[ImportanceId] 
	  ,Importance.ImportanceName as ImportanceName
      ,[ImpactId] 
	  ,ImpactType.ImpactDirectionName as ImpactName
      ,[CompanyTrendsMap].[CreatedOn]
      ,[CompanyTrendsMap].[UserCreatedById]
      ,[CompanyTrendsMap].[ModifiedOn]
      ,[CompanyTrendsMap].[UserModifiedById]
      ,[CompanyTrendsMap].[IsDeleted]
      ,[CompanyTrendsMap].[DeletedOn]
      ,[CompanyTrendsMap].[UserDeletedById]
  FROM [dbo].[CompanyTrendsMap]
  INNER JOIN [dbo].[Importance] Importance ON [CompanyTrendsMap].ImportanceId = Importance.Id
  INNER JOIN [dbo].[ImpactDirection] ImpactType ON [CompanyTrendsMap].ImpactId = ImpactType.Id
  WHERE ([CompanyTrendsMap].CompanyId = @CompanyID )      
 AND ([CompanyTrendsMap].IsDeleted = 0)  
END