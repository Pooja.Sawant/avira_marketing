﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces
{
    public interface IInfographicsRepository
    {
        InfographicsViewModel GetById(Guid guid);
        IEnumerable<InfographicsViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, Guid? projectId = null, Guid? lookupCategoryId = null);
        Result<SpTransactionMessage> CreateUpdate(InfographicsViewModel viewModel);
        Result<SpTransactionMessage> Delete(Guid Id);
    }
}
