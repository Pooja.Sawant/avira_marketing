﻿  
  
CREATE PROCEDURE [dbo].[SApp_GetMarketsByIndustryId]
(
	@IndustryIdList dbo.[udtUniqueIdentifier] readonly
)  
AS  
/*================================================================================  
Procedure Name: [SApp_GetMarketsByIndustryId]  
Author: Sai Krishna  
Create date: 10/05/2019  
Description: Get list of Markets by IndustryId from table   
Change History  
Date        Developer     Reason  
__________  ____________  ______________________________________________________  
10/05/2019  Sai krishna   Initial Version  
================================================================================*/  

BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
/*
SELECT Mar.Id,
	Mar.MarketName,
	Mar.ParentId
FROM Market Mar
INNER JOIN @IndustryIdList t1
ON Mar.ParentId = t1.ID
WHERE IsDeleted = 0 
ORDER By Mar.Id
*/

declare @IndustrySegmentId uniqueidentifier;

select @IndustrySegmentId = id
from Segment
where SegmentName = 'End User';

Select M.Id, M.MarketName, M.ParentId
From Market M 
where exists(select 1 from marketvalue MV
			inner join [MarketValueSegmentMapping] MVSM 
			on MV.ID = MVSM.MarketValueId
			 Inner Join @IndustryIdList IL
			 On MVSM.SubSegmentId = IL.ID
			 Where M.Id = MV.MarketId
			 and MVSM.SegmentId = @IndustrySegmentId
			 and MV.IsDeleted = 0);
END