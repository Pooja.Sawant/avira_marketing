﻿/*================================================================================      
Procedure Name:SFab_GetKeyEmployees     
Author: Raju    
Create date: 13/06/2019      
Description: Company KeyEmployees and Designations         
Date       Developer    Reason      
__________ ____________ ______________________________________________________      
13/03/2020   Raju      Initial Version      
================================================================================*/         
CREATE PROCEDURE [dbo].[SFab_GetKeyEmployees]         
 @CompanyID uniqueidentifier ,         
 @includeDeleted bit = 0,
 @AviraUserID uniqueidentifier = null            
AS        
BEGIN         
 SET NOCOUNT ON;          
select distinct KeyEmployeeName AS [Name], Designation   from CompanyKeyEmployeesMap K
Inner Join Designation D on K.DesignationId=D.Id
where CompanyId = @CompanyID  AND (@includeDeleted = 0 or K.IsDeleted = 0) AND IsDirector=0   ORDER BY [Name]    
END