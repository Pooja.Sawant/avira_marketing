﻿ 
  
-- =============================================        
-- Author:  Laxmikant        
-- Create date: 05/29/2019        
-- Description: Get Profit Company total revenue by Company Id  
-- =============================================        
CREATE PROCEDURE [dbo].[SApp_CompanyProfitGeographicSplitByCompanyID]             
 @ComapanyID uniqueidentifier = null,    
 @includeDeleted bit = 0             
AS        
BEGIN         
 SET NOCOUNT ON;          
 SELECT CompanyProfitGeographicSplitMap.[Id]   
   ,CompanyProfitGeographicSplitMap.[CompanyProfitId]  
      ,CompanyProfitGeographicSplitMap.[RegionId]  
      ,CompanyProfitGeographicSplitMap.[RegionValue]  
      ,CompanyProfitGeographicSplitMap.[Year]  
      ,CompanyProfitGeographicSplitMap.[CreatedOn]  
      ,CompanyProfitGeographicSplitMap.[UserCreatedById]  
      ,CompanyProfitGeographicSplitMap.[ModifiedOn]  
      ,CompanyProfitGeographicSplitMap.[UserModifiedById]  
      ,CompanyProfitGeographicSplitMap.[IsDeleted]  
      ,CompanyProfitGeographicSplitMap.[DeletedOn]  
      ,CompanyProfitGeographicSplitMap.[UserDeletedById]  
      ,CompanyProfitGeographicSplitMap.[CompanyId]  
  FROM [dbo].CompanyProfitGeographicSplitMap        
     
  WHERE (CompanyProfitGeographicSplitMap.CompanyId = @ComapanyID) --and [CompanyRevenue].[projectId] = @ProjectID)              
 AND (@includeDeleted = 0 or CompanyProfitGeographicSplitMap.IsDeleted = 1)          
END