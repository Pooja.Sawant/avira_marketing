﻿using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.Web.Utils;
using System.Globalization;

namespace Avira.MarketingResearch.Web.ImportExportUtility
{
    public class ImportCompanyProfile: IExcelImport
    {
        public readonly IServiceRepository _serviceRepository;
        private ILoggerManager _logger;
        public ImportCompanyProfile(IServiceRepository serviceRepository, ILoggerManager logger)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
        }

        private int GetColIndex(ExcelWorksheet worksheet, string colName)
        {
            int colIndex = worksheet.Cells["6:6"].First(c => c.Value.ToString() == colName).Start.Column;

            return colIndex;
        }

        private List<CompanyProfileShareHoldingImportModel> GetCPShareHolding(ExcelWorksheet worksheet)
        {
            List<CompanyProfileShareHoldingImportModel> CPShareholdingList = new List<CompanyProfileShareHoldingImportModel>();
            var rowCount = worksheet.Dimension.Rows;

            int colIndexName = GetColIndex(worksheet, "Name");
            int colIndexEntity = GetColIndex(worksheet, "Entity");
            int colIndexHolding = GetColIndex(worksheet, "Holding");

            for (int row = 7; row <= rowCount; row++)
            {
                CompanyProfileShareHoldingImportModel CompanyProfileShareHoldingImportModel = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

                if (!string.IsNullOrEmpty(cn))
                {
                    CompanyProfileShareHoldingImportModel = new CompanyProfileShareHoldingImportModel
                    {
                        RowNo = row,
                        Name = worksheet.Cells[row, colIndexName].Value?.ToString().Trim(),
                        Entity = worksheet.Cells[row, colIndexEntity].Value?.ToString().Trim(),
                        Holding = worksheet.Cells[row, colIndexHolding].Value?.ToString().Trim()
                    };
                    CPShareholdingList.Add(CompanyProfileShareHoldingImportModel);
                }                
            }
            return CPShareholdingList;
        }

        private List<CompanyProfileFundamentalsImportModel> GetCPFundamentals(ExcelWorksheet worksheet)
        {
            
            List<CompanyProfileFundamentalsImportModel> CompanyProfileFundamentalsImportModelList = new List<CompanyProfileFundamentalsImportModel>();
            var rowCount = worksheet.Dimension.Rows;

            int colIndexCompanyName = GetColIndex(worksheet, "Company Name");
            int colIndexDescription = GetColIndex(worksheet, "Description");
            int colIndexFoundedYear = GetColIndex(worksheet, "Founded year");
            //int colIndexNature = GetColIndex(worksheet, "Nature");
            int colIndexCompanyStage = GetColIndex(worksheet, "Company Stage");
            int colIndexNumberOfEmployee = GetColIndex(worksheet, "Number Of Employee");
            int colIndexAsOnDate = GetColIndex(worksheet, "As on Date");
            int colIndexWebsite = GetColIndex(worksheet, "Website");
            int colIndexCompanyRank_Rank = GetColIndex(worksheet, "CompanyRank_Rank");
            int colIndexCompanyRank_Rationale = GetColIndex(worksheet, "CompanyRank_Rationale");
            int colIndexCurrency1 = GetColIndex(worksheet, "Currency1");
            int colIndexPhoneNumber1 = GetColIndex(worksheet, "Phone Number1");
            int colIndexPhoneNumber2 = GetColIndex(worksheet, "Phone Number2");
            int colIndexPhoneNumber3 = GetColIndex(worksheet, "Phone Number3");
            //int colIndexGeographicPresnece = GetColIndex(worksheet, "Geographic Presnece");

            for (int row = 7; row <= rowCount; row++)
            {
                CompanyProfileFundamentalsImportModel CompanyProfileFundamentalsImportModel = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

                if (!string.IsNullOrEmpty(cn))
                {
                    CompanyProfileFundamentalsImportModel = new CompanyProfileFundamentalsImportModel
                    {
                        RowNo = row,
                        CompanyName = worksheet.Cells[row, colIndexCompanyName].Value?.ToString().Trim(),
                        Description = worksheet.Cells[row, colIndexDescription].Value?.ToString().Trim(),
                        Foundedyear = worksheet.Cells[row, colIndexFoundedYear].Value?.ToString().Trim(),
                        //Nature = worksheet.Cells[row, colIndexNature].Value?.ToString().Trim(),
                        CompanyStage = worksheet.Cells[row, colIndexCompanyStage].Value?.ToString().Trim(),
                        NumberOfEmployee = worksheet.Cells[row, colIndexNumberOfEmployee].Value?.ToString().Trim(),
                        AsonDate = Convert.ToDateTime(worksheet.Cells[row, colIndexAsOnDate].Value?.ToString().Trim()),
                        Website = worksheet.Cells[row, colIndexWebsite].Value?.ToString().Trim(),
                        CompanyRank_Rank = worksheet.Cells[row, colIndexCompanyRank_Rank].Value?.ToString().Trim(),
                        CompanyRank_Rationale = worksheet.Cells[row, colIndexCompanyRank_Rationale].Value?.ToString().Trim(),
                        Currency1 = worksheet.Cells[row, colIndexCurrency1].Value?.ToString().Trim(),
                        PhoneNumber1 = worksheet.Cells[row, colIndexPhoneNumber1].Value?.ToString().Trim(),
                        PhoneNumber2 = worksheet.Cells[row, colIndexPhoneNumber2].Value?.ToString().Trim(),
                        PhoneNumber3 = worksheet.Cells[row, colIndexPhoneNumber3].Value?.ToString().Trim(),
                        //GeographicPresence = worksheet.Cells[row, colIndexGeographicPresnece].Value?.ToString().Trim(),
                    };
                    CompanyProfileFundamentalsImportModelList.Add(CompanyProfileFundamentalsImportModel);
                }                
            }
            return CompanyProfileFundamentalsImportModelList;
        }

        private List<CompanyProfileSubsidiariesImportModel> GetCPSubsidiaries(ExcelWorksheet worksheet)
        {
            List<CompanyProfileSubsidiariesImportModel> CPSubsidiariesList = new List<CompanyProfileSubsidiariesImportModel>();
            var rowCount = worksheet.Dimension.Rows;

            int colIndexCompanySubsdiaryName = GetColIndex(worksheet, "CompanySubsdiaryName");
            int colIndexCompanySubsdiaryLocation = GetColIndex(worksheet, "CompanySubsdiaryLocation");

            for (int row = 7; row <= rowCount; row++)
            {
                CompanyProfileSubsidiariesImportModel CompanyProfileSubsidiariesImportModel = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

                if (!string.IsNullOrEmpty(cn))
                {
                    CompanyProfileSubsidiariesImportModel = new CompanyProfileSubsidiariesImportModel
                    {
                        RowNo = row,
                        CompanySubsdiaryName = worksheet.Cells[row, colIndexCompanySubsdiaryName].Value?.ToString().Trim(),
                        CompanySubsdiaryLocation = worksheet.Cells[row, colIndexCompanySubsdiaryLocation].Value?.ToString().Trim()
                    };
                    CPSubsidiariesList.Add(CompanyProfileSubsidiariesImportModel);
                }                
            }
            return CPSubsidiariesList;
        }

        private List<CompanyKeyEmployeesImportModel> GetKeyEmployees(ExcelWorksheet worksheet)
        {
            List<CompanyKeyEmployeesImportModel> CPKeyEmployeesList = new List<CompanyKeyEmployeesImportModel>();
            var rowCount = worksheet.Dimension.Rows;

            int colIndexKeyEmployeeName1 = GetColIndex(worksheet, "KeyEmployeeName1");
            int colIndexKeyEmplyeeDesignation = GetColIndex(worksheet, "KeyEmplyeeDesignation");
            int colIndexKeyEmployeeEmail = GetColIndex(worksheet, "KeyEmployeeEmail");
            int colIndexReportingManager = GetColIndex(worksheet, "Reporting Manager");

            for (int row = 7; row <= rowCount; row++)
            {
                CompanyKeyEmployeesImportModel CompanyKeyEmployeesImportModel = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

                if (!string.IsNullOrEmpty(cn))
                {
                    CompanyKeyEmployeesImportModel = new CompanyKeyEmployeesImportModel
                    {
                        RowNo = row,
                        KeyEmployeeName = worksheet.Cells[row, colIndexKeyEmployeeName1].Value?.ToString().Trim(),
                        KeyEmployeeDesignation = worksheet.Cells[row, colIndexKeyEmplyeeDesignation].Value?.ToString().Trim(),
                        KeyEmployeeEmail = worksheet.Cells[row, colIndexKeyEmployeeEmail].Value?.ToString().Trim(),
                        ReportingManager = worksheet.Cells[row, colIndexReportingManager].Value?.ToString().Trim()
                    };
                    CPKeyEmployeesList.Add(CompanyKeyEmployeesImportModel);
                }
            }
            return CPKeyEmployeesList;
        }

        private List<CompanyBoardOfDirectorsImportModel> GetBoDs(ExcelWorksheet worksheet)
        {
            List<CompanyBoardOfDirectorsImportModel> CPBoDs = new List<CompanyBoardOfDirectorsImportModel>();
            var rowCount = worksheet.Dimension.Rows;

            int colIndexBoardofDirectorName = GetColIndex(worksheet, "BoardofDirectorName");
            int colIndexBoardofDirectorEmail = GetColIndex(worksheet, "BoardofDirectorEmail ");
            int colIndexBoardofDirectorOtherCompany = GetColIndex(worksheet, "BoardofDirectorOtherCompany");
            int colIndexBoardofDirectorOtherCompanyDesignation = GetColIndex(worksheet, "BoardofDirectorOtherCompanyDesignation");

            for (int row = 7; row <= rowCount; row++)
            {
                CompanyBoardOfDirectorsImportModel CompanyBoardOfDirectorsImportModel = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

                if (!string.IsNullOrEmpty(cn))
                {
                    CompanyBoardOfDirectorsImportModel = new CompanyBoardOfDirectorsImportModel
                    {
                        RowNo = row,
                        BoardofDirectorName = worksheet.Cells[row, colIndexBoardofDirectorName].Value?.ToString().Trim(),
                        BoardofDirectorEmail = worksheet.Cells[row, colIndexBoardofDirectorEmail].Value?.ToString().Trim(),
                        BoardofDirectorOtherCompany = worksheet.Cells[row, colIndexBoardofDirectorOtherCompany].Value?.ToString().Trim(),
                        BoardofDirectorOtherCompanyDesignation = worksheet.Cells[row, colIndexBoardofDirectorOtherCompanyDesignation].Value?.ToString().Trim()
                    };
                    CPBoDs.Add(CompanyBoardOfDirectorsImportModel);
                }
            }
            return CPBoDs;
        }

        private List<CompanyProductImportModel> GetProducts(ExcelWorksheet worksheet)
        {
            List<CompanyProductImportModel> CPProducts = new List<CompanyProductImportModel>();
            var rowCount = worksheet.Dimension.Rows;

            int colIndexMarket = GetColIndex(worksheet, "Market");
            int colIndexProductServiceName = GetColIndex(worksheet, "Product_ServiceName");
            //int colIndexLaunchDate = GetColIndex(worksheet, "LaunchDate");
            int colIndexSpecification = GetColIndex(worksheet, "Specification");
            int colIndexProductCategory = GetColIndex(worksheet, "ProductCategory");
            int colIndexSegment = GetColIndex(worksheet, "Segment");
            int colIndexRevenue = GetColIndex(worksheet, "Revenue");
            int colIndexUnits = GetColIndex(worksheet, "Units");
            int colIndexCurrency = GetColIndex(worksheet, "Currency");
            int colIndexTargetedGeogrpahy = GetColIndex(worksheet, "TargetedGeogrpahy");
            int colIndexEndUserIndustriesTargeted = GetColIndex(worksheet, "EndUser_Industries_Targeted");
            int colIndexDecriptionRemarks = GetColIndex(worksheet, "Decription_Remarks");
            int colIndexStatus = GetColIndex(worksheet, "Status");

            for (int row = 7; row <= rowCount; row++)
            {
                CompanyProductImportModel CompanyProductImportModel = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

                if (!string.IsNullOrEmpty(cn))
                {
                    decimal? revenue = null;
                    DateTime? launchdate = null;

                    if (worksheet.Cells[row, colIndexRevenue].Value != null && worksheet.Cells[row, colIndexRevenue].Value.ToString() != "")
                    {
                        decimal revenue1;
                        Decimal.TryParse(worksheet.Cells[row, colIndexRevenue].Value.ToString(), out revenue1);

                        if (revenue1 >= 0) revenue = revenue1;
                    }

                    //if (worksheet.Cells[row, colIndexLaunchDate].Value != null && worksheet.Cells[row, colIndexLaunchDate].Value.ToString() != "")
                    //{
                    //    DateTime launchdate1;
                    //    DateTime.TryParse(worksheet.Cells[row, colIndexLaunchDate].Value.ToString(), out launchdate1);

                    //    launchdate = launchdate1;
                    //}

                    CompanyProductImportModel = new CompanyProductImportModel
                    {
                        RowNo = row,
                        Market = worksheet.Cells[row, colIndexMarket].Value?.ToString().Trim(),
                        ProductServiceName = worksheet.Cells[row, colIndexProductServiceName].Value?.ToString().Trim(),
                        Specification = worksheet.Cells[row, colIndexSpecification].Value?.ToString().Trim(),
                        ProductCategory = worksheet.Cells[row, colIndexProductCategory].Value?.ToString().Trim(),
                        Segment = worksheet.Cells[row, colIndexSegment].Value?.ToString().Trim(),
                        Revenue = revenue,
                        Units = worksheet.Cells[row, colIndexUnits].Value?.ToString().Trim(),
                        Currency = worksheet.Cells[row, colIndexCurrency].Value?.ToString().Trim(),
                        TargetedGeogrpahy = worksheet.Cells[row, colIndexTargetedGeogrpahy].Value?.ToString().Trim(),
                        EndUserIndustriesTargeted = worksheet.Cells[row, colIndexEndUserIndustriesTargeted].Value?.ToString().Trim(),
                        DecriptionRemarks = worksheet.Cells[row, colIndexDecriptionRemarks].Value?.ToString().Trim(),
                        Status = worksheet.Cells[row, colIndexStatus].Value?.ToString().Trim(),
                    };
                    CPProducts.Add(CompanyProductImportModel);
                }
            }
            return CPProducts;
        }

        private List<CompanyClientsImportModel> GetClient(ExcelWorksheet worksheet)
        {
            List<CompanyClientsImportModel> CPClient = new List<CompanyClientsImportModel>();
            var rowCount = worksheet.Dimension.Rows;


            int colIndexClientName = GetColIndex(worksheet, "ClientName");
            int colIndexClientIndustry = GetColIndex(worksheet, "ClientIndustry");
            int colIndexRemarks = GetColIndex(worksheet, "Remarks1");       


            for (int row = 7; row <= rowCount; row++)
            {
                CompanyClientsImportModel CompanyClient = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

                if (!string.IsNullOrEmpty(cn))
                {
                    CompanyClient = new CompanyClientsImportModel
                    {
                        RowNo = row,
                        ClientName = worksheet.Cells[row, colIndexClientName].Value?.ToString().Trim(),
                        ClientIndustry = worksheet.Cells[row, colIndexClientIndustry].Value?.ToString().Trim(),
                        Remarks = worksheet.Cells[row, colIndexRemarks].Value?.ToString().Trim()
                    };
                    CPClient.Add(CompanyClient);
                }
            }
            return CPClient;
        }

        private List<CompanyStrategyImportModel> GetStrategy(ExcelWorksheet worksheet)
        {
            List<CompanyStrategyImportModel> CPStrategy = new List<CompanyStrategyImportModel>();
            var rowCount = worksheet.Dimension.Rows;

            int colIndexDate = GetColIndex(worksheet, "Date2");
            int colIndexOrganicInorganic = GetColIndex(worksheet, "Organic_Inorganic");
            int colIndexStrategy = GetColIndex(worksheet, "Strategy");
            int colIndexCategory = GetColIndex(worksheet, "Category");
            int colIndexImportance = GetColIndex(worksheet, "ImportanceH_M_L");
            int colIndexImpact = GetColIndex(worksheet, "ImpactDirection");
            int colIndexRemarks = GetColIndex(worksheet, "Remarks");

            for (int row = 7; row <= rowCount; row++)
            {
                CompanyStrategyImportModel CompanyStrategy = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

                if (!string.IsNullOrEmpty(cn))
                {
                    CompanyStrategy = new CompanyStrategyImportModel
                    {
                        RowNo = row,
                        Date = Convert.ToDateTime(worksheet.Cells[row, colIndexDate].Value?.ToString().Trim()),
                        OrganicInorganic = worksheet.Cells[row, colIndexOrganicInorganic].Value?.ToString().Trim(),
                        Strategy = worksheet.Cells[row, colIndexStrategy].Value?.ToString().Trim(),
                        Category = worksheet.Cells[row, colIndexCategory].Value?.ToString().Trim(),
                        Importance = worksheet.Cells[row, colIndexImportance].Value?.ToString().Trim(),
                        ImpactDirection = worksheet.Cells[row, colIndexImpact].Value?.ToString().Trim(),
                        Remarks = worksheet.Cells[row, colIndexRemarks].Value?.ToString().Trim()
                    };
                    CPStrategy.Add(CompanyStrategy);
                }
            }
            return CPStrategy;
        }

        private List<CompanyNewsImportModel> GetNews(ExcelWorksheet worksheet)
        {
            List<CompanyNewsImportModel> CPnews = new List<CompanyNewsImportModel>();

            var rowCount = worksheet.Dimension.Rows;

            int colIndexNewsDate = GetColIndex(worksheet, "News Date");
            int colIndexNews = GetColIndex(worksheet, "News");
            int colIndexNewsCategory = GetColIndex(worksheet, "NewsCategory");
            int colIndexImportance = GetColIndex(worksheet, "ImportanceH_M_L1");
            int colIndexImpact = GetColIndex(worksheet, "Impact_positive_Negative");
            int colIndexOtherParticipants = GetColIndex(worksheet, "OtherParticipants");
            int colIndexRemarks = GetColIndex(worksheet, "Remarks1");

            for (int row = 7; row <= rowCount; row++)
            {
                CompanyNewsImportModel CompanyNews = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

                if (!string.IsNullOrEmpty(cn))
                {
                    CompanyNews = new CompanyNewsImportModel
                    {
                        RowNo = row,
                        NewsDate = Convert.ToDateTime(worksheet.Cells[row, colIndexNewsDate].Value?.ToString().Trim()),
                        News = worksheet.Cells[row, colIndexNews].Value?.ToString().Trim(),
                        NewsCategory = worksheet.Cells[row, colIndexNewsCategory].Value?.ToString().Trim(),
                        Importance = worksheet.Cells[row, colIndexImportance].Value?.ToString().Trim(),
                        Impact = worksheet.Cells[row, colIndexImpact].Value?.ToString().Trim(),
                        OtherParticipants = worksheet.Cells[row, colIndexOtherParticipants].Value?.ToString().Trim(),
                        Remarks = worksheet.Cells[row, colIndexRemarks].Value?.ToString().Trim()
                    };
                    CPnews.Add(CompanyNews);
                }
            }
            return CPnews;
        }

        private List<CompanySWOTImportModel> GetSWOT(ExcelWorksheet worksheet)
        {
            List<CompanySWOTImportModel> CPSWOT = new List<CompanySWOTImportModel>();
            var rowCount = worksheet.Dimension.Rows;

            int colIndexAnalysisPoint = GetColIndex(worksheet, "AnalysisPoint");
            int colIndexMarket = GetColIndex(worksheet, "Market");
            int colIndexCategory = GetColIndex(worksheet, "Category");
           int colIndexSWOTType = GetColIndex(worksheet, "SWOTType");// not in excel

            for (int row = 7; row <= rowCount; row++)
            {
                CompanySWOTImportModel CompanySWOT = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

                if (!string.IsNullOrEmpty(cn))
                {
                    CompanySWOT = new CompanySWOTImportModel
                    {
                        RowNo = row,
                        AnalysisPoint = worksheet.Cells[row, colIndexAnalysisPoint].Value?.ToString().Trim(),
                        Market = worksheet.Cells[row, colIndexMarket].Value?.ToString().Trim(),
                        Category = worksheet.Cells[row, colIndexCategory].Value?.ToString().Trim(),
                        SWOTType = worksheet.Cells[row, colIndexSWOTType].Value?.ToString().Trim(),// not in excel
                    };
                    CPSWOT.Add(CompanySWOT);
                }
            }
            return CPSWOT;
        }
        private List<CompanyProfilesESMSegmentImportModel> GetESMSegmentList(ExcelWorksheet worksheet)
        {
            List<CompanyProfilesESMSegmentImportModel> CPESMSegmentList = new List<CompanyProfilesESMSegmentImportModel>();
            int colIndexESMSegment;
            int colIndexESMSubSegment;

            var rowCount = worksheet.Dimension.Rows;
            try
            {
                colIndexESMSegment = GetColIndex(worksheet, "ESM Segment");
                colIndexESMSubSegment = GetColIndex(worksheet, "ESM Subsegment");
            }
            catch(Exception ex)
            {
                throw new Exception("Column header(s) for ESM Segment worksheet not correct.");
            }

            for (int row = 7; row <= rowCount; row++)
            {
                CompanyProfilesESMSegmentImportModel CompanyProfilesESMSegment = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

                //if (!string.IsNullOrEmpty(cn))
                
                //if (Avira.MarketingResearch.Web.Utils.Common.IsValidExcelRow(worksheet, row, worksheet.Dimension.Columns) == true)
                {
                    CompanyProfilesESMSegment = new CompanyProfilesESMSegmentImportModel
                    {
                        RowNo = row,
                        ESMSegmentName = worksheet.Cells[row, colIndexESMSegment].Value?.ToString().Trim(),
                        ESMSubSegmentName = worksheet.Cells[row, colIndexESMSubSegment].Value?.ToString().Trim()
                    };
                    CPESMSegmentList.Add(CompanyProfilesESMSegment);
                }
            }
            return CPESMSegmentList;
        }

        private List<CompanyEcosystemPresenceImportModel> GetEcosystem(ExcelWorksheet worksheet)
        {
            List<CompanyEcosystemPresenceImportModel> CPEcosystem = new List<CompanyEcosystemPresenceImportModel>();
            var rowCount = worksheet.Dimension.Rows;

            int colIndexESMPresence = GetColIndex(worksheet, "ESMPresence");
            int colIndexSectorPresence = GetColIndex(worksheet, "SectorPresence");
            int colIndexKeyCompetitor = GetColIndex(worksheet, "KeyCompetitor");
           
            for (int row = 7; row <= rowCount; row++)
            {
                CompanyEcosystemPresenceImportModel CompanyEcosystem = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

                if (!string.IsNullOrEmpty(cn))
                {
                    CompanyEcosystem = new CompanyEcosystemPresenceImportModel
                    {
                        RowNo = row,
                        ESMPresence = worksheet.Cells[row, colIndexESMPresence].Value?.ToString().Trim(),
                        SectorPresence = worksheet.Cells[row, colIndexSectorPresence].Value?.ToString().Trim(),
                        KeyCompetitor = worksheet.Cells[row, colIndexKeyCompetitor].Value?.ToString().Trim(),
                    };
                    CPEcosystem.Add(CompanyEcosystem);
                }
            }
            return CPEcosystem;
        }

        private List<CompanyTrendsImportModel> GetTrend(ExcelWorksheet worksheet)
        {
            List<CompanyTrendsImportModel> CPTrend = new List<CompanyTrendsImportModel>();
            var rowCount = worksheet.Dimension.Rows;

            int colIndexTrends = GetColIndex(worksheet, "Trends");
            int colIndexDepartment = GetColIndex(worksheet, "Department");
            int colIndexCategory = GetColIndex(worksheet, "Category");
            int colIndexEcosystem = GetColIndex(worksheet, "Ecosystem");
            int colIndexImportance = GetColIndex(worksheet, "Importance");
            int colIndexImpact = GetColIndex(worksheet, "Impact");
            //int colIndexMarket = GetColIndex(worksheet, "Market");// not in excel

            for (int row = 7; row <= rowCount; row++)
            {
                CompanyTrendsImportModel CompanyTrend = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

                if (!string.IsNullOrEmpty(cn))
                {
                    CompanyTrend = new CompanyTrendsImportModel
                    {
                        RowNo = row,
                        Trends = worksheet.Cells[row, colIndexTrends].Value?.ToString().Trim(),
                        Department = worksheet.Cells[row, colIndexDepartment].Value?.ToString().Trim(),
                        Category = worksheet.Cells[row, colIndexCategory].Value?.ToString().Trim(),
                        Ecosystem = worksheet.Cells[row, colIndexEcosystem].Value?.ToString().Trim(),
                        Importance = worksheet.Cells[row, colIndexImportance].Value?.ToString().Trim(),
                        Impact = worksheet.Cells[row, colIndexImpact].Value?.ToString().Trim()
                        //Market = worksheet.Cells[row, colIndexMarket].Value?.ToString().Trim(),// not in excel
                    };
                    CPTrend.Add(CompanyTrend);
                }
            }
            return CPTrend;
        }

        private List<CompanyAnalystViewImportModel> GetAnalystViews(ExcelWorksheet worksheet)
        {
            List<CompanyAnalystViewImportModel> CPAnalystViews = new List<CompanyAnalystViewImportModel>();
            var rowCount = worksheet.Dimension.Rows;

            int colIndexMarket = GetColIndex(worksheet, "Market/Project Name");
            int colIndexAnalystView = GetColIndex(worksheet, "AnalystView");
            int colIndexDateOfUpdate = GetColIndex(worksheet, "Date of update");
            int colIndexCategory = GetColIndex(worksheet, "Category");
            int colIndexImportance = GetColIndex(worksheet, "Importance");

            for (int row = 7; row <= rowCount; row++)
            {
                CompanyAnalystViewImportModel CompanyAnalystViews = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

                if (!string.IsNullOrEmpty(cn))
                {
                    CompanyAnalystViews = new CompanyAnalystViewImportModel
                    {
                        RowNo = row,
                        Market = worksheet.Cells[row, colIndexMarket].Value?.ToString().Trim(),
                        AnalystView = worksheet.Cells[row, colIndexAnalystView].Value?.ToString().Trim(),
                        DateOfUpdate = Convert.ToDateTime(worksheet.Cells[row, colIndexDateOfUpdate].Value?.ToString().Trim()),
                        Category = worksheet.Cells[row, colIndexCategory].Value?.ToString().Trim(),
                        Importance = worksheet.Cells[row, colIndexImportance].Value?.ToString().Trim()
                    };
                    CPAnalystViews.Add(CompanyAnalystViews);
                }
            }
            return CPAnalystViews;
        }

        private List<PriceVolumeViewImportModel> GetPriceVolume(ExcelWorksheet worksheet)
        {
            List<PriceVolumeViewImportModel> priceVolumnList = new List<PriceVolumeViewImportModel>();
            var rowCount = worksheet.Dimension.Rows;

            int colIndexDate = GetColIndex(worksheet, "Date");
            int colIndexPrice = GetColIndex(worksheet, "Price");
            int colIndexCurrency = GetColIndex(worksheet, "Currency");
            int colIndexCurrencyUnit = GetColIndex(worksheet, "Currency_Unit");
            int colIndexVolume = GetColIndex(worksheet, "Volume");
            int colIndexVolumeUnit = GetColIndex(worksheet, "Volume_Unit");

            for (int row = 7; row <= rowCount; row++)
            {
                PriceVolumeViewImportModel priceVolumeViewImportModel = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

                if (!string.IsNullOrEmpty(cn))
                {
                    decimal? price = null;
                    DateTime? date = null;
                    decimal? volume = null;

                    if (worksheet.Cells[row, colIndexPrice].Value != null && worksheet.Cells[row, colIndexPrice].Value.ToString() != "")
                    {
                        decimal price1;
                        Decimal.TryParse(worksheet.Cells[row, colIndexPrice].Value.ToString(), out price1);

                        if (price1 >= 0) price = price1;
                    }

                    if (worksheet.Cells[row, colIndexDate].Value != null && worksheet.Cells[row, colIndexDate].Value.ToString() != "")
                    {
                        DateTime date1;
                        DateTime.TryParse(worksheet.Cells[row, colIndexDate].Value.ToString(), out date1);

                        date = date1;
                    }

                    if (worksheet.Cells[row, colIndexVolume].Value != null && worksheet.Cells[row, colIndexVolume].Value.ToString() != "")
                    {
                        decimal volume1;
                        Decimal.TryParse(worksheet.Cells[row, colIndexVolume].Value.ToString(), out volume1);

                        if (volume1 >= 0) volume = volume1;
                    }

                    priceVolumeViewImportModel = new PriceVolumeViewImportModel
                    {
                        RowNo = row,
                        Date = date,
                        Price = price,
                        Currency = worksheet.Cells[row, colIndexCurrency].Value?.ToString().Trim(),
                        CurrencyUnit = worksheet.Cells[row, colIndexCurrencyUnit].Value?.ToString().Trim(),
                        Volume = volume,
                        VolumeUnit = worksheet.Cells[row, colIndexVolumeUnit].Value?.ToString().Trim(),
                    };
                    priceVolumnList.Add(priceVolumeViewImportModel);
                }
            }
            return priceVolumnList;
        }

        public ImportResponse Import(IFormFile formFile)
        {
            var res = new ImportResponse();           
            var inputModel = new CompanyProfileImportInsertViewModel();
            using (var stream = new MemoryStream())
            {
                formFile.CopyTo(stream);

                using (var package = new ExcelPackage(stream))
                {
                    try
                    {
                        if (package.Workbook.Worksheets["Fundamentals"] != null)
                        {
                            inputModel.CompanyProfileFundamentalsImportModel = GetCPFundamentals(package.Workbook.Worksheets["Fundamentals"]);
                            //if (inputModel.CompanyProfileFundamentalsImportModel.Count > 0)
                            //{
                            //    for (int i = 0; i < inputModel.CompanyProfileFundamentalsImportModel.Count; i++)
                            //    {
                            //        DateTime yearValue;
                            //        if (inputModel.CompanyProfileFundamentalsImportModel[i].AsonDate != null)
                            //        {
                            //            DateTime asOnDate = (DateTime)inputModel.CompanyProfileFundamentalsImportModel[i].AsonDate;
                            //            if (DateTime.TryParseExact(asOnDate.ToShortDateString(), "dd-MMM-yy", CultureInfo.InvariantCulture, DateTimeStyles.None, out yearValue))
                            //            {

                            //            }
                            //            else
                            //            {
                            //                res.ResponseStatus = "Error";
                            //                res.ResponseMessage = "Error while reading template. 'AsonDate value not in proper Format.'";
                            //                res.TotalFailedRecords = 1;
                            //                _logger.LogError("Error while reading template. 'AsonDate value not in proper Format.'");
                            //                return res;
                            //            }
                            //        }
                            //    }   
                            //}
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'Fundamentals' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'Fundamentals' worksheet is missing.");
                            return res;
                        }
                        if (package.Workbook.Worksheets["Share Holding Pattern"] != null)
                        {
                            inputModel.CompanyProfileShareHoldingImportModel = GetCPShareHolding(package.Workbook.Worksheets["Share Holding Pattern"]);
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'Share Holding Pattern' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'Share Holding Pattern' worksheet is missing.");
                            return res;
                        }
                        if (package.Workbook.Worksheets["Subsidiaries"] != null)
                        {
                            inputModel.CompanyProfileSubsidiariesImportModel = GetCPSubsidiaries(package.Workbook.Worksheets["Subsidiaries"]);
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'Subsidiaries' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'Subsidiaries' worksheet is missing.");
                            return res;
                        }
                        if (package.Workbook.Worksheets["Key Employees"] != null)
                        {
                            inputModel.CompanyKeyEmployeesImportModel = GetKeyEmployees(package.Workbook.Worksheets["Key Employees"]);
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'Key Employees' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'Key Employees' worksheet is missing.");
                            return res;
                        }
                        if (package.Workbook.Worksheets["BoDs"] != null)
                        {
                            inputModel.CompanyBoardOfDirectorsImportModel = GetBoDs(package.Workbook.Worksheets["BoDs"]);
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'BoDs' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'BoDs' worksheet is missing.");
                            return res;
                        }
                        if (package.Workbook.Worksheets["Product"] != null)
                        {
                            inputModel.CompanyProductImportModel = GetProducts(package.Workbook.Worksheets["Product"]);
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'Product' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'Product' worksheet is missing.");
                            return res;
                        }
                        if (package.Workbook.Worksheets["Client & Strategies"] != null)
                        {
                            inputModel.CompanyClientsImportModel = GetClient(package.Workbook.Worksheets["Client & Strategies"]);
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'Client & Strategies' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'Client & Strategies' worksheet is missing.");
                            return res;
                        }
                        if (package.Workbook.Worksheets["Client & Strategies"] != null)
                        {
                            inputModel.CompanyStrategyImportModel = GetStrategy(package.Workbook.Worksheets["Client & Strategies"]);
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'Client & Strategies' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'Client & Strategies' worksheet is missing.");
                            return res;
                        }
                        if (package.Workbook.Worksheets["News"] != null)
                        {
                            inputModel.CompanyNewsImportModel = GetNews(package.Workbook.Worksheets["News"]);
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'News' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'News' worksheet is missing.");
                            return res;
                        }
                        if (package.Workbook.Worksheets["SWOT"] != null)
                        {
                            inputModel.CompanySWOTImportModel = GetSWOT(package.Workbook.Worksheets["SWOT"]);
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'SWOT' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'SWOT' worksheet is missing.");
                            return res;
                        }
                        if (package.Workbook.Worksheets["Ecosystem"] != null)
                        {
                            inputModel.CompanyEcosystemPresenceImportModel = GetEcosystem(package.Workbook.Worksheets["Ecosystem"]);
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'Ecosystem' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'Ecosystem' worksheet is missing.");
                            return res;
                        }
                        //if (package.Workbook.Worksheets["ESM Segment"] != null)
                        //{
                        //    inputModel.CompanyProfilesESMSegmentListImportModel = GetESMSegmentList(package.Workbook.Worksheets["ESM Segment"]);
                        //}
                        //else
                        //{
                        //    res.ResponseStatus = "Error";
                        //    res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'ESM Segment' worksheet is missing.";
                        //    res.TotalFailedRecords = 1;
                        //    _logger.LogError("Error while reading template. 'ESM Segment' worksheet is missing.");
                        //    return res;
                        //}
                        if (package.Workbook.Worksheets["Trends"] != null)
                        {
                            inputModel.CompanyTrendsImportModel = GetTrend(package.Workbook.Worksheets["Trends"]);
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'Trends' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'Trends' worksheet is missing.");
                            return res;
                        }
                        //if (package.Workbook.Worksheets["Analyst Views"] != null)
                        //{
                        //    inputModel.CompanyAnalystViewImportModel = GetAnalystViews(package.Workbook.Worksheets["Analyst Views"]);
                        //}
                        //else
                        //{
                        //    res.ResponseStatus = "Error";
                        //    res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'Analyst Views' worksheet is missing.";
                        //    res.TotalFailedRecords = 1;
                        //    _logger.LogError("Error while reading template. 'Analyst Views' worksheet is missing.");
                        //    return res;
                        //}
                        if (package.Workbook.Worksheets["Price Volume"] != null)
                        {
                            inputModel.PriceVolumeViewImportModel = GetPriceVolume(package.Workbook.Worksheets["Price Volume"]);
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'Price Volume' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'Price Volume' worksheet is missing.");
                            return res;
                        }

                    }
                    catch(Exception ex)
                    {
                        res.ResponseStatus = "Error";
                        res.ResponseMessage = "Error while reading template. Error Message: " + ex.Message + ". Please check input excel. More details logged in error log.";
                        res.TotalFailedRecords = 1;
                        _logger.LogError("Error Message: " + ex.Message + " Stack Trace: " + ex.StackTrace.ToString());
                        return res;
                    }
                }
            }

            //--------Call api to post data            
            inputModel.TemplateName = "CompanyProfile Import Template";
            inputModel.ImportFileName = formFile.FileName;

            try
            {

                HttpResponseMessage response = _serviceRepository.PostResponse("CompanyProfileImportExport", inputModel);
                response.EnsureSuccessStatusCode();
                ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;

                res.ResponseStatus = viewModel.IsSuccess ? "Success" : "Failed";
                res.ResponseMessage = viewModel.Message;
                res.TotalRecords = inputModel.CompanyProfileFundamentalsImportModel.Count;
                if (res.ResponseStatus == "Success")
                {
                    res.TotalSuccessRecords = inputModel.CompanyProfileFundamentalsImportModel.Count;
                    res.TotalFailedRecords = 0;
                }
                else
                {
                    res.TotalSuccessRecords = 0;
                    res.TotalFailedRecords = inputModel.CompanyProfileFundamentalsImportModel.Count;
                }
                res.GetCompanyProfileErrorList = viewModel.cpData;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        public ImportResponse Import_old(IFormFile formFile)
        {
            var res = new ImportResponse();
            var list = new List<CompanyProfileImportViewModel>();
            var inputModel = new CompanyProfileImportInsertViewModel();
            using (var stream = new MemoryStream())
            {
                formFile.CopyTo(stream);

                using (var package = new ExcelPackage(stream))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                    var rowCount = worksheet.Dimension.Rows;

                    for (int row = 2; row <= rowCount; row++)
                    {
                        var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();
                        if (!string.IsNullOrEmpty(cn))
                        {
                            list.Add(new CompanyProfileImportViewModel
                            {
                                CompanyName = worksheet.Cells[row, 1].Value?.ToString().Trim(),
                                Nature = worksheet.Cells[row, 2].Value?.ToString().Trim(),
                                CompanySubsdiaryName = worksheet.Cells[row, 3].Value?.ToString().Trim(),
                                CompanySubsdiaryLocation = worksheet.Cells[row, 4].Value?.ToString().Trim(),
                                CompanyParentName = worksheet.Cells[row, 5].Value?.ToString().Trim(),
                                HQ = worksheet.Cells[row, 6].Value?.ToString().Trim(),
                                PhoneNumber1 = worksheet.Cells[row, 7].Value?.ToString().Trim(),
                                PhoneNumber2 = worksheet.Cells[row, 8].Value?.ToString().Trim(),
                                PhoneNumber3 = worksheet.Cells[row, 9].Value?.ToString().Trim(),
                                Mail1 = worksheet.Cells[row, 10].Value?.ToString().Trim(),
                                Mail2 = worksheet.Cells[row, 11].Value?.ToString().Trim(),
                                Mail3 = worksheet.Cells[row, 12].Value?.ToString().Trim(),
                                Website = worksheet.Cells[row, 13].Value?.ToString().Trim(),
                                FoundedYear = worksheet.Cells[row, 14].Value?.ToString().Trim(),
                                CompanyStage = worksheet.Cells[row, 15].Value?.ToString().Trim(),
                                Geographies = worksheet.Cells[row, 16].Value?.ToString().Trim(),
                                NumberofEmployees = worksheet.Cells[row, 17].Value?.ToString().Trim(),
                                AsonDate = worksheet.Cells[row, 18].Value?.ToString().Trim(),
                                KeyEmployeeName = worksheet.Cells[row, 19].Value?.ToString().Trim(),
                                KeyEmplyeeDesignation = worksheet.Cells[row, 20].Value?.ToString().Trim(),
                                KeyEmployeeEmail = worksheet.Cells[row, 21].Value?.ToString().Trim(),
                                KeyEmployeeComments = worksheet.Cells[row, 22].Value?.ToString().Trim(),
                                BoardofDirectorName = worksheet.Cells[row, 23].Value?.ToString().Trim(),
                                BoardofDirectorEmail = worksheet.Cells[row, 24].Value?.ToString().Trim(),
                                BoardofDirectorOtherCompany = worksheet.Cells[row, 25].Value?.ToString().Trim(),
                                BoardofDirectorOtherCompanyDesignation = worksheet.Cells[row, 26].Value?.ToString().Trim(),
                                CompanyRank_Rank = worksheet.Cells[row, 27].Value?.ToString().Trim(),
                                CompanyRank_Rationale = worksheet.Cells[row, 28].Value?.ToString().Trim(),
                                Name = worksheet.Cells[row, 29].Value?.ToString().Trim(),
                                Entity = worksheet.Cells[row, 30].Value?.ToString().Trim(),
                                Holding = worksheet.Cells[row, 31].Value?.ToString().Trim(),
                                Level = worksheet.Cells[row, 32].Value?.ToString().Trim(),
                                Designation = worksheet.Cells[row, 33].Value?.ToString().Trim(),
                                Product_ServiceName = worksheet.Cells[row, 34].Value?.ToString().Trim(),
                                LaunchDate = worksheet.Cells[row, 35].Value?.ToString().Trim(),
                                ProductCategory = worksheet.Cells[row, 36].Value?.ToString().Trim(),
                                Segment = worksheet.Cells[row, 37].Value?.ToString().Trim(),
                                Revenue = worksheet.Cells[row, 38].Value?.ToString().Trim(),
                                Units = worksheet.Cells[row, 39].Value?.ToString().Trim(),
                                Currency = worksheet.Cells[row, 40].Value?.ToString().Trim(),
                                TargetedGeogrpahy = worksheet.Cells[row, 41].Value?.ToString().Trim(),
                                Patents = worksheet.Cells[row, 42].Value?.ToString().Trim(),
                                Decription_Remarks = worksheet.Cells[row, 43].Value?.ToString().Trim(),
                                Status = worksheet.Cells[row, 44].Value?.ToString().Trim(),
                                Currency1 = worksheet.Cells[row, 45].Value?.ToString().Trim(),
                                Unit = worksheet.Cells[row, 46].Value?.ToString().Trim(),
                                TableType = worksheet.Cells[row, 47].Value?.ToString().Trim(),
                                TotalRevenue = worksheet.Cells[row, 48].Value?.ToString().Trim(),
                                Revenue_2018 = worksheet.Cells[row, 49].Value?.ToString().Trim(),
                                Revenue_2017 = worksheet.Cells[row, 50].Value?.ToString().Trim(),
                                Revenue_2016 = worksheet.Cells[row, 51].Value?.ToString().Trim(),
                                Revenue_2015 = worksheet.Cells[row, 52].Value?.ToString().Trim(),
                                Revenue_2014 = worksheet.Cells[row, 53].Value?.ToString().Trim(),
                                Profit = worksheet.Cells[row, 54].Value?.ToString().Trim(),
                                Profit_2018 = worksheet.Cells[row, 55].Value?.ToString().Trim(),
                                Profit_2017 = worksheet.Cells[row, 56].Value?.ToString().Trim(),
                                Profit_2016 = worksheet.Cells[row, 57].Value?.ToString().Trim(),
                                Profit_2015 = worksheet.Cells[row, 58].Value?.ToString().Trim(),
                                Profit_2014 = worksheet.Cells[row, 59].Value?.ToString().Trim(),
                                ExpenseUnit = worksheet.Cells[row, 60].Value?.ToString().Trim(),
                                ExpenseCurency = worksheet.Cells[row, 61].Value?.ToString().Trim(),
                                OtherFinancialTableType = worksheet.Cells[row, 62].Value?.ToString().Trim(),
                                TotalExpense = worksheet.Cells[row, 63].Value?.ToString().Trim(),
                                Expence_2018 = worksheet.Cells[row, 64].Value?.ToString().Trim(),
                                Expence_2017 = worksheet.Cells[row, 65].Value?.ToString().Trim(),
                                Expence_2016 = worksheet.Cells[row, 66].Value?.ToString().Trim(),
                                Expence_2015 = worksheet.Cells[row, 67].Value?.ToString().Trim(),
                                Expence_2014 = worksheet.Cells[row, 68].Value?.ToString().Trim(),
                                Currency11 = worksheet.Cells[row, 69].Value?.ToString().Trim(),
                                Period = worksheet.Cells[row, 70].Value?.ToString().Trim(),
                                Base_Date = worksheet.Cells[row, 71].Value?.ToString().Trim(),
                                Date1 = worksheet.Cells[row, 72].Value?.ToString().Trim(),
                                Price = worksheet.Cells[row, 73].Value?.ToString().Trim(),
                                Volume = worksheet.Cells[row, 74].Value?.ToString().Trim(),
                                Date2 = worksheet.Cells[row, 75].Value?.ToString().Trim(),
                                Organic_Inorganic = worksheet.Cells[row, 76].Value?.ToString().Trim(),
                                Strategy = worksheet.Cells[row, 77].Value?.ToString().Trim(),
                                Category = worksheet.Cells[row, 78].Value?.ToString().Trim(),
                                ImportanceH_M_L = worksheet.Cells[row, 79].Value?.ToString().Trim(),
                                ClientName = worksheet.Cells[row, 80].Value?.ToString().Trim(),
                                ClientIndustry = worksheet.Cells[row, 81].Value?.ToString().Trim(),
                                Remarks = worksheet.Cells[row, 82].Value?.ToString().Trim(),
                                ImportDate = worksheet.Cells[row, 83].Value?.ToString().Trim(),
                                News = worksheet.Cells[row, 84].Value?.ToString().Trim(),
                                NewsCategory = worksheet.Cells[row, 85].Value?.ToString().Trim(),
                                ImportanceH_M_L1 = worksheet.Cells[row, 86].Value?.ToString().Trim(),
                                Impact_positive_Negative = worksheet.Cells[row, 87].Value?.ToString().Trim(),
                                OtherParticipants = worksheet.Cells[row, 88].Value?.ToString().Trim(),
                                Remarks1 = worksheet.Cells[row, 89].Value?.ToString().Trim(),
                                ESMPresence = worksheet.Cells[row, 90].Value?.ToString().Trim(),
                                SectorPresence = worksheet.Cells[row, 91].Value?.ToString().Trim(),
                                KeyCompetitor = worksheet.Cells[row, 92].Value?.ToString().Trim(),
                                Strenght = worksheet.Cells[row, 93].Value?.ToString().Trim(),
                                Weakness = worksheet.Cells[row, 94].Value?.ToString().Trim(),
                                Opportunity = worksheet.Cells[row, 95].Value?.ToString().Trim(),
                                Threats = worksheet.Cells[row, 96].Value?.ToString().Trim(),
                                Trends = worksheet.Cells[row, 97].Value?.ToString().Trim(),
                                Department = worksheet.Cells[row, 98].Value?.ToString().Trim(),
                                Importance = worksheet.Cells[row, 99].Value?.ToString().Trim(),
                                Impact = worksheet.Cells[row, 100].Value?.ToString().Trim(),
                                AddVendorInfoTable = worksheet.Cells[row, 101].Value?.ToString().Trim(),
                                AddLitigationsAndOtherLegalInfo = worksheet.Cells[row, 102].Value?.ToString().Trim(),
                                AddPatentRelatedInfo = worksheet.Cells[row, 103].Value?.ToString().Trim(),
                                AddAnyotherinfo = worksheet.Cells[row, 104].Value?.ToString().Trim(),
                                AddTable = worksheet.Cells[row, 105].Value?.ToString().Trim(),
                                AnalystViewText = worksheet.Cells[row, 106].Value?.ToString().Trim(),
                                ProjectName = worksheet.Cells[row, 108].Value?.ToString().Trim(),
                            });
                        }
                    }
                }
            }

            //--------Call api to post data
            //list
            inputModel.CompanyProfileImportViewModel = list;
            inputModel.TemplateName = "CompanyProfile Import Template";
            inputModel.ImportFileName = formFile.FileName;

            try
            {

                HttpResponseMessage response = _serviceRepository.PostResponse("CompanyProfileImportExport", inputModel);
                response.EnsureSuccessStatusCode();
                ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;

                res.ResponseStatus = viewModel.IsSuccess ? "Success" : "Failed";
                res.ResponseMessage = viewModel.Message;
                res.TotalRecords = inputModel.CompanyProfileImportViewModel.Count;
                if (viewModel.TrendData == null)
                {
                    res.TotalSuccessRecords = (inputModel.CompanyProfileImportViewModel.Count);
                    res.TotalFailedRecords = 0;
                }
                else
                {
                    res.TotalSuccessRecords = (inputModel.CompanyProfileImportViewModel.Count) - (viewModel.cpData.Count());
                    res.TotalFailedRecords = viewModel.cpData.Count();
                }
                res.GetCompanyProfileErrorList = viewModel.cpData;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            //try
            //{

            //    HttpResponseMessage response = _serviceRepository.PostResponse("CompanyProfileImportExport", inputModel);
            //    response.EnsureSuccessStatusCode();
            //    ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;

            //    res.ResponseStatus = viewModel.IsSuccess ? "Success" : "Failed";
            //    res.ResponseMessage = viewModel.Message;
            //    res.TotalRecords = inputModel.CompanyProfileImportViewModel.Count;
            //    res.GetCompanyProfileErrorList = viewModel.cpData;
            //}
            //catch (Exception ex)
            //{
            //    _logger.LogError(ex.Message);
            //}

            //res.ResponseStatus = "Success";
            //res.ResponseMessage = "Partialy uploaded, please see the list";
            //res.TotalRecords = 100;
            //res.TotalSuccessRecords = 80;
            //res.TotalFailedRecords = 20;

            return res;
        }
        private static byte[] GetBytes(ExcelPackage file)
        {
            using (var stream = new MemoryStream())
            {
                file.SaveAs(stream);
                return stream.ToArray();
            }
        }
    }
    internal class ExcelFile
    {
    }
}
