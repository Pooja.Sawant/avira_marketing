﻿using Avira.MarketingResearch.Model.ViewModels.QualitativeInfo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.QualitativeInfo
{
    public interface IQualitativeInfoTrackerService
    {
         IEnumerable <QualitativeInfoTrackerViewModel> GetAll(Guid? Id, int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false);
    }
}
