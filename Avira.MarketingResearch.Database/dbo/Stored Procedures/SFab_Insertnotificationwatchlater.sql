﻿CREATE PROCEDURE [dbo].[SFab_Insertnotificationwatchlater]
	-- Add the parameters for the stored procedure here
@ActionName	varchar(1000),
@AssignedToEmailID	nvarchar(100),
@Description	nvarchar(1000) ,
@UserId	uniqueidentifier	
AS
BEGIN 
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;     
   declare @ErrorMsg NVARCHAR(2048) 
   DECLARE @AssignedToId	uniqueidentifier 
 BEGIN TRY 


SELECT @AssignedToId = ID from AviraUser WHERE EmailAddress = @AssignedToEmailID
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  INSERT INTO [dbo].[NotificationWatcher]
            
			([NotificationWatcherID]
           ,[ActionName]
           --,[IsBroadCosted]
           ,[AssignedById]
           ,[AssignedToId]
           ,[Description]
           ,[UserCreatedById])
     VALUES
           (
		   newid(),
           @ActionName,
           --@IsBroadCosted,
            @UserId,
           @AssignedToId,
           @Description,
           @UserId
           )

		   SELECT StoredProcedureName ='SFab_Insertnotificationwatchlater',Message =@ErrorMsg,Success = 1; 

END TRY      
BEGIN CATCH      
  exec SApp_LogErrorInfo
 set @ErrorMsg = 'Error while insert a new notification, please contact Admin for more details.';    
SELECT StoredProcedureName ='SFab_Insertnotificationwatchlater',Message =@ErrorMsg,Success = 0;  
END CATCH   
		
END