﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.QualitativeInfo;
using Avira.MarketingResearch.Repository.CoreInterfaces.QualitativeInfo;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.QualitativeInfo
{
    public class QualitativeNoteRepository : IQualitativeNoteRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "QualitativeNote";

        public QualitativeNoteRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public QualitativeNoteViewModel GetById(Guid Id, Guid projectId)
        {
            QualitativeNoteViewModel qualitativeNoteViewModel = new QualitativeNoteViewModel();
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<QualitativeNoteViewModel>(QualitativeNoteQueries.StoreProcSelectAll
                            , 
                            new {
                                  @Id =Id,
                                  @ProjectId = projectId
                                }
                            , null, true, null, CommandType.StoredProcedure).FirstOrDefault();
        }

        public Result<SpTransactionMessage> Insert(QualitativeNoteViewModel viewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(QualitativeNoteQueries.StoreProcInsert, new
            {
                @Id = viewModel.Id,
                @QualitativeId = viewModel.QualitativeId,
                @ProjectId = viewModel.ProjectId,
                @AuthorRemark = viewModel.AuthorRemark,
                @ApproverRemark = viewModel.ApproverRemark,
                @QualitativeStatusName = viewModel.QualitativeStatusName,
                @AviraUserId = viewModel.UserCreatedById
            }, CommandType.StoredProcedure);
            return spTransactionMessage;
        } 
    }
}
