﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SFab_InsertDownloadsExportData] 
	-- Add the parameters for the stored procedure here
   (@ProjectId uniqueidentifier,
	@ModuleTypeId uniqueidentifier,
	@Header nvarchar(100),
	@Footer nvarchar(100),
	@ReportTitle nvarchar(100),
	@CoverPageImageLink nvarchar(200),
    @CoverPageImageName nvarchar(100),
    @Overview nvarchar(3000),
    @KeyPoints nvarchar(3000),
    @Methodology nvarchar(3000),
    @MethodologyImageLink nvarchar(200),
    @ContactUsAnalystName nvarchar(100),
    @ContactUsOfficeContactNo nvarchar(15),
    @ContactUsAnalystEmailId nvarchar(254),
	@UserId uniqueidentifier)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)     
 BEGIN TRY
   
 select P.ProjectName from  project P inner join DownloadsExportData D  on p.id= D.projectid  order by ProjectName 
 select M.ModuleName from ModuleType M inner join DownloadsExportData D on m.Id= D.ModuleTypeId   order by ModuleName


INSERT INTO [dbo].[DownloadsExportData]
             ([Id]
           ,[ProjectId]
           ,[ModuleTypeId]
           ,[Header]
           ,[Footer]
           ,[ReportTitle]
           ,[CoverPageImageLink]
           ,[CoverPageImageName]
           ,[Overview]
           ,[KeyPoints]
           ,[Methodology]
           ,[MethodologyImageLink]
           ,[ContactUsAnalystName]
           ,[ContactUsOfficeContactNo]
           ,[ContactUsAnalystEmailId]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted]
           ,[DeletedOn]
           ,[UserDeletedById])
VALUES (newid(),
          @ProjectId,
		  @ModuleTypeId,
		  @Header,
		  @Footer,
		  @ReportTitle,
		  @CoverPageImageLink,
          @CoverPageImageName,
		  @Overview,
		  @KeyPoints,
		  @Methodology,
		  @MethodologyImageLink,
          @ContactUsAnalystName,
          @ContactUsOfficeContactNo,
          @ContactUsAnalystEmailId,
		  GetDate(),
		  @UserId,
		  0,
		  GetDate(),
		  @UserId
		  )
END TRY
BEGIN CATCH      
  exec SApp_LogErrorInfo
 set @ErrorMsg = 'Error while insert a DownloadExportData, please contact Admin for more details.';    
SELECT StoredProcedureName ='SFab_InsertDecision',Message =@ErrorMsg,Success = 0;  
END CATCH   		 	  
END