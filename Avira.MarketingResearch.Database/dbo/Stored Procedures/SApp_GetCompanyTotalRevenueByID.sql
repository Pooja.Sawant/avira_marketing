﻿--exec SApp_GetCompanyTotalRevenueByID '775dfcf4-32ef-467d-8116-2e0f2f4be11e',0

--sp_helptext SApp_GetCompanyTotalRevenueByID

-- =============================================              
-- Author:  Laxmikant              
-- Create date: 05/29/2019              
-- Description: Get Revenue Company total revenue by Company Id        
-- =============================================              
CREATE PROCEDURE [dbo].[SApp_GetCompanyTotalRevenueByID]                   
 @ComapanyID uniqueidentifier = null,    
 @includeDeleted bit = 0                   
AS              
BEGIN               
 SET NOCOUNT ON;       
     
DECLARE @BaseYear int;    
SELECT  @BaseYear =  BaseYear from BaseYearMaster    
    
 sele    
    
 SELECT [CompanyRevenue].[Id]      
   ,[CompanyRevenue].[CompanyId]      
      ,[CompanyRevenue].[ProjectId]      
      ,[CompanyRevenue].[CurrencyId]      
      ,[CompanyRevenue].[UnitId]      
      ,dbo.fun_UnitDeConversion([CompanyRevenue].[UnitId], [CompanyRevenue].[TotalRevenue]) as [TotalRevenue]
	  ,[CompanyRevenue].[Year]         
      ,[CompanyRevenue].[CreatedOn]        
      ,[CompanyRevenue].[UserCreatedById]        
      ,[CompanyRevenue].[ModifiedOn]        
      ,[CompanyRevenue].[UserModifiedById]        
      ,[CompanyRevenue].[IsDeleted]        
      ,[CompanyRevenue].[DeletedOn]        
      ,[CompanyRevenue].[UserDeletedById]       
   ,@BaseYear as BaseYearRevenue     
          
  FROM [dbo].[CompanyRevenue]              
           
  WHERE ([CompanyRevenue].CompanyId = @ComapanyID) --and [CompanyRevenue].[projectId] = @ProjectID)                    
 AND (@includeDeleted = 0 or [CompanyRevenue].IsDeleted = 1)                
END