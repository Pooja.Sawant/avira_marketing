﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels
{
    public class PermissionsByUserRoleIdViewModel
    {
        public List<PermissionByRoleId> Permissions { get; set; }
        public List<PermissionByForm> Forms { get; set; }
        public List<MenuList> Menus { get; set; }

        public class PermissionByRoleId
        {
            //public Guid PermisssionId { get; set; }
            public Guid Id { get; set; }
            public string PermissionName { get; set; }
            public int PermissionLevel { get; set; }
            public int UserType { get; set; }
        }
        public class PermissionByForm
        {
            public Guid FormId { get; set; }
            public string FormName { get; set; }
            public Guid MenuId { get; set; }
            public string MenuName { get; set; }
        }
        public class MenuList
        {
            public Guid MenuId { get; set; }
            public string MenuName { get; set; }
            public Guid ParentMenuId { get; set; }
        }
    }
}
