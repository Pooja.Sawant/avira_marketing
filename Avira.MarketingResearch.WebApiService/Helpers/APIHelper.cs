﻿using Microsoft.Extensions.Configuration;

namespace Avira.MarketingResearch.Web.Helpers
{
    public class APIHelper : IAPIHelper
    {
        IConfiguration _configuration;
        public APIHelper(IConfiguration iconfiguration)
        {
            _configuration = iconfiguration;
        }

        public string GetBaseURL()
        {
            return _configuration.GetSection("API")["BaseURL"];
        }

        public string GetCreateSegmentURL(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["CreateURL"];
            return relativeURL;
        }

        public string GetReadByIdSegmentURL(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["GetURLById"];
            return relativeURL;
        }

        public string GetReadByTrendIdSegmentURL(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["GetURLByTrendId"];
            return relativeURL;
        }


        public string GetReadSegmentURL(string segmentName)
        {          
            string relativeURL = _configuration.GetSection("API:"+ segmentName)["GetURL"];
            return relativeURL;
        }
        public string GetReadSegmentURL(string segmentName, string methodName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)[methodName];
            return relativeURL;
        }
        public string GetReadProjectSegmentURL(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["GetGetProjectDetailsIdURL"];
            return relativeURL;
        }
        public string GetReadAllParentSegmentURL(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["GetAllParentURL"];
            return relativeURL;
        }

        public string GetUpdateSegmentURL(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["UpdateURL"];
            return relativeURL;
        }

        public string GetDeleteSegmentURL(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["DeleteURL"];
            return relativeURL;
        }

        public string GetSubSegmentURL(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["SubSegmentURL"];
            return relativeURL;
        }

        public string GetReadAllUserByRoleSegmentURL(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["GetAllUserByRoleURL"];
            return relativeURL;
        }

        public string GetIndustrySegmentListIdSegmentURL(string segmentName)
        {
            string relativeURL;
            relativeURL = _configuration.GetSection("API:" + segmentName)["GetAllsegmentIndustryIdURL"];          

            return relativeURL;
        }

        public string GetMarketSegmentListIdSegmentURL(string segmentName)
        {
            string relativeURL;
            relativeURL = _configuration.GetSection("API:" + segmentName)["GetAllsegmentMarketIdURL"];
            return relativeURL;
        }
        public string GetUpdateStatusSegmentURL(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["UpdateStatusURL"];
            return relativeURL;
        }
        public string GetByAllResponseSegmentURL(string segmentName)
        {
            string relativeURL;
            relativeURL = _configuration.GetSection("API:" + segmentName)["GetAllResponseURL"];
            return relativeURL;
        }

        public string GetImportanceResponse(string segmentName)
        {
            string relativeURL;
            relativeURL = _configuration.GetSection("API:" + segmentName)["GetImportance"];
            return relativeURL;
        }
        public string GetImpactDirectionResponse(string segmentName)
        {
            string relativeURL;
            relativeURL = _configuration.GetSection("API:" + segmentName)["GetImpactDirection"];
            return relativeURL;
        }

        public string GetShareVolByCompanyIdSegmentURL(string segmentName)
        {
            string relativeURL;
            relativeURL = _configuration.GetSection("API:" + segmentName)["GetShareVolume"];
            return relativeURL;
        }

        public string SaveImagebyCompanyIdSegmentURL(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["SaveImageInServer"];
            return relativeURL;
        }

        //Company Client Strategy 
        public string GetAllImportanceByCompanyIdSegmentURL(string segmentName)
        {
            string relativeURL;
            relativeURL = _configuration.GetSection("API:" + segmentName)["GetAllImportance"];
            return relativeURL;
        }

        public string GetAllImpactDirectionByCompanyIdSegmentURL(string segmentName)
        {
            string relativeURL;
            relativeURL = _configuration.GetSection("API:" + segmentName)["GetAllImpactDirection"];
            return relativeURL;
        }

        public string GetAllApproachByCompanyIdSegmentURL(string segmentName)
        {
            string relativeURL;
            relativeURL = _configuration.GetSection("API:" + segmentName)["GetAllApproach"];
            return relativeURL;
        }

        public string GetAllCategoriesByCompanyIdSegmentURL(string segmentName)
        {
            string relativeURL;
            relativeURL = _configuration.GetSection("API:" + segmentName)["GetAllCategories"];
            return relativeURL;
        }

        public string GetAllIndustryByCompanyIdSegmentURL(string segmentName)
        {
            string relativeURL;
            relativeURL = _configuration.GetSection("API:" + segmentName)["GetAllIndustries"];
            return relativeURL;
        }

        public string GetCompanyclientAndStrategyByIdURL(string segmentName)
        {
            string relativeURL;
            relativeURL = _configuration.GetSection("API:" + segmentName)["GetURLById"];
            return relativeURL;
        }

        public string GetCompanyStrategyByIdURL(string segmentName)
        {
            string relativeURL;
            relativeURL = _configuration.GetSection("API:" + segmentName)["GetstrategyById"];
            return relativeURL;
        }

        public string GetCompanyClientByIdURL(string segmentName)
        {
            string relativeURL;
            relativeURL = _configuration.GetSection("API:" + segmentName)["GetclientById"];
            return relativeURL;
        }

        public string GetCompanySWOTStrengthURL(string segmentName)
        {
            string relativeURL;
            relativeURL = _configuration.GetSection("API:" + segmentName)["GetStrengthByIdURL"];
            return relativeURL;
        }

        public string GetCompanySWOTWeeknessesURL(string segmentName)
        {
            string relativeURL;
            relativeURL = _configuration.GetSection("API:" + segmentName)["GetWeeknessesByIdURL"];
            return relativeURL;
        }

        public string GetCompanySWOTOpportunitiesURL(string segmentName)
        {
            string relativeURL;
            relativeURL = _configuration.GetSection("API:" + segmentName)["GetOpportunitiesByIdURL"];
            return relativeURL;
        }

        public string GetCompanySWOTThreatsURL(string segmentName)
        {
            string relativeURL;
            relativeURL = _configuration.GetSection("API:" + segmentName)["GetThreatsByIdURL"];
            return relativeURL;
        }

        public string GetAllParentSegmentURL(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["GetAllParentSegURL"];
            return relativeURL;
        }

        public string GetEcoSystemPresenceKeyCompByCompanyIdURL(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["GetEcoSystemPresenceKeyCompByCompanyIdURL"];
            return relativeURL;
        }

        public string GetSectorPresenceMapByCompanyIdURL(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["GetSectorPresenceMapByCompanyIdURL"];
            return relativeURL;
        }

        public string GetEcoSystemPresenceMapByCompanyIdURL(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["GetEcoSystemPresenceMapByCompanyIdURL"];
            return relativeURL;
        }

        public string GetAllCategorySegmentURL(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["GetAllParentSegURL"];
            return relativeURL;
        }
        public string GetAllCompanyDetailsURL(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["GetAllCompanyDetailsURL"];
            return relativeURL;
        }

        public string GetAllCompaniesResponse(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["GetAllCompaniesURL"];
            return relativeURL;
        }

        public string GetAllDesignationsResponse(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["GetAllDesignationURL"];
            return relativeURL;
        }

        public string GetAllCompanyStagesResponse(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["GetAllStageURL"];
            return relativeURL;
        }

        public string GetCompanyFundamentalResponse(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["GetAllFundamentalURL"];
            return relativeURL;
        }
        public string GetCompanyRegionResponse(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["GetAllRegionURL"];
            return relativeURL;
        }
        public string GetCompanyEntityResponse(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["GetAllEntityURL"];
            return relativeURL;
        }
        public string GetCompanySubsidaryResponse(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["GetAllSubsidaryURL"];
            return relativeURL;
        }

        public string GetCompanyOtherInfoByCompanyIdURL(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["GetURLById"];
            return relativeURL;
        }
        public string GetCompanyOrganizationChartURL(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["GetOrganizationChartURL"];
            return relativeURL;
        }

        public string GetCompanyManagerResponse(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["GetAllManagerURL"];
            return relativeURL;
        }
        public string GetAllCurrenciesURL(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["GetAllCurrencies"];
            return relativeURL;
        }
        public string GetRevenueProfitBaseYearURL(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["GetBaseYear"];
            return relativeURL;
        }

        public string GetAllRegionsURL(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["GetAllRegions"];
            return relativeURL;
        }

        public string GetAllValueConversionURL(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["GetAllValueConversion"];
            return relativeURL;
        }

        public string GetAllCategoryURL(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["GetAllCategoryURL"];
            return relativeURL;
        }

        //public string GetDynamicURL(string segmentName, string method)
        //{
        //    string relativeURL = _configuration.GetSection("API:" + segmentName)[method];
        //    return relativeURL;
        //}

        public string GetDynamicURL(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["GetURLByUserId"];
            return relativeURL;
        }

        public string GetCompanyNatureResponse(string segmentName)
        {
            string relativeURL = _configuration.GetSection("API:" + segmentName)["GetAllNatureURL"];
            return relativeURL;
        }

    }
}
