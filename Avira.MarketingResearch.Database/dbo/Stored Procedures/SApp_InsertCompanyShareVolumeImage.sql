﻿
CREATE PROCEDURE [dbo].[SApp_InsertCompanyShareVolumeImage]
(@Id	UNIQUEIDENTIFIER=null,
@ImageURL	NVARCHAR(256)=null,
@ImageDisplayName	NVARCHAR(256)=null,
@ActualImageName	NVARCHAR(256)=null,
@CompanyId	UNIQUEIDENTIFIER=null,
@ParentCompanyId	UNIQUEIDENTIFIER=null,
@ProjectId	UNIQUEIDENTIFIER=null,
@UserCreatedById UNIQUEIDENTIFIER
)
as
/*================================================================================
Procedure Name: SApp_InsertCompanyShareVolumeImage
Author: Harshal
Create date: 05/29/2019
Description: Insert or updaTe a record in CompanyShareVolumeImage table
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
05/29/2019	Swami			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ErrorMsg NVARCHAR(2048);
Begin try
IF exists(select 1 from [dbo].[CompanyShareVolumeImage] where ([Id] =  @Id and  CompanyId = @CompanyId AND IsDeleted = 0))
Begin
UPDATE [CompanyShareVolumeImage]
SET [ImageURL]=@ImageURL,
    [ImageDisplayName]=@ImageDisplayName,
    [ActualImageName] =@ActualImageName,
	[CompanyId]=@CompanyId,
	[UserModifiedById]=@UserCreatedById,
    [ModifiedOn]=GETDATE()
WHERE CompanyId = @CompanyId And Id=@Id
end

Else
Begin
INSERT INTO [dbo].[CompanyShareVolumeImage]
           ([ID]
           ,[ProjectId]
           ,[ParentCompanyId]
           ,[ImageURL]
           ,[ImageDisplayName]
		   ,[ActualImageName]
           ,[CompanyId]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
	values(@Id, 
			@ProjectId,
			@ParentCompanyId, 
			@ImageURL,
			@ImageDisplayName,
			@ActualImageName,
			@CompanyId,
			GETDATE(), --as [CreatedOn]
			@UserCreatedById, 
			0); --as [IsDeleted]
End
SELECT StoredProcedureName ='SApp_InsertCompanyShareVolumeImage',Message =@ErrorMsg,Success = 1;   
END TRY 

BEGIN CATCH
    -- Execute the error retrieval routine.
	DECLARE @ErrorNumber	int;
	DECLARE @ErrorSeverity	int;
	DECLARE @ErrorProcedure	varchar(100);
	DECLARE @ErrorLine	int;
	DECLARE @ErrorMessage	varchar(500);

	 SELECT @ErrorNumber = ERROR_NUMBER(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorProcedure = ERROR_PROCEDURE(),
        @ErrorLine = ERROR_LINE(),
        @ErrorMessage = ERROR_MESSAGE()

	insert into dbo.Errorlog(ErrorNumber,
							ErrorSeverity,
							ErrorProcedure,
							ErrorLine,
							ErrorMessage,
							ErrorDate)
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());

 set @ErrorMsg = 'Error while uploading share volume image, please contact Admin for more details.';    
--THROW 50000,@ErrorMsg,1;      
--return(1)  
SELECT Number = @ErrorNumber, 
		Severity =@ErrorSeverity,
		StoredProcedureName =@ErrorProcedure,
		LineNumber= @ErrorLine,
		Message =@ErrorMsg,
		Success = 0;     
END CATCH
end