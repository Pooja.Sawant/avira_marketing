﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.CompanyProfile
{
    public class CompanyProductCategoryMapRepository: ICompanyProductCategoryMapRepository
    {
        protected IDbHelper _dbHelper;

        public CompanyProductCategoryMapRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public IEnumerable<CompanyProductCategoryMapViewModel> GetAll(Guid? Id)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<CompanyProductCategoryMapViewModel>(CompanyProductCategoryMapQueries.StoreProcSelectAll
                            , new { @ComapanyProductID = Id, @includeDeleted = false }
                            , null, true, null, CommandType.StoredProcedure);
        }

        public Result<SpTransactionMessage> InsertUpdate(CompanyProductViewMoel companyProductViewMoel)
        {
            DataTable companyProductCategoryMapTable = new DataTable();
            companyProductCategoryMapTable.Columns.Add("ID", typeof(Guid));
            if (companyProductViewMoel != null)
            {
                for (int k = 0; k < companyProductViewMoel.ProductCategoryId.Length; k++)
                {
                    companyProductCategoryMapTable.Rows.Add(companyProductViewMoel.ProductCategoryId[k]);
                }
            }

            var spTransactionMessage = _dbHelper.ExecuteQuery(CompanyProductCategoryMapQueries.StoreProcInsertUpdate,
               new
               {
                   @CompanyProductId = companyProductViewMoel.Id,
                   @CompanyProductCategoryMap = companyProductCategoryMapTable,
                   @AviraUserId = companyProductViewMoel.UserCreatedById
               }
                    , CommandType.StoredProcedure);
            return spTransactionMessage;
        }
    }
}
