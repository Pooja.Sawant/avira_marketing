﻿
CREATE PROCEDURE [dbo].[SFabChart_GetStrategyByCompanyID]
(
	@ComapanyID uniqueidentifier = null,
    @includeDeleted bit = 0,
	@AvirauserId UniqueIdentifier = null
)
as
/*================================================================================
Procedure Name: [SFabChart_GetStrategyByCompanyID]
Author: Harshal
Create date: 09/07/2019
Description: Get list from CompanyStrategy by Company Id
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
09/07/2019	Harshal		Initial Version
================================================================================*/
BEGIN         
 SET NOCOUNT ON;
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED       
 SELECT [CompanyStrategyMapping].[CompanyId],
		[CompanyStrategyMapping].[Approach] as ApproachId,
		Approach.ApproachName as ApproachName,
		COUNT(*) as CategoryCount
  FROM [dbo].[CompanyStrategyMapping]        
  inner JOIN [dbo].[Approach] Approach ON [CompanyStrategyMapping].Approach = Approach.Id   
  WHERE ([CompanyStrategyMapping].CompanyId = @ComapanyID)              
 AND (@includeDeleted = 0 or [CompanyStrategyMapping].IsDeleted = 0)
 GROUP BY [CompanyStrategyMapping].[CompanyId],
		[CompanyStrategyMapping].[Approach],
		Approach.ApproachName
END