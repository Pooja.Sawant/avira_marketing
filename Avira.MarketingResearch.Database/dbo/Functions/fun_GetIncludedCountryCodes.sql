﻿CREATE function [dbo].[fun_GetIncludedCountryCodes]
(@ProjectId UNIQUEIDENTIFIER, 
 @CountryId UNIQUEIDENTIFIER) 
RETURNS NVARCHAR(512) 
as 
begin 
declare @CountryCodes NVARCHAR(512);
declare @CountryName NVARCHAR(512);
declare @RegionId UNIQUEIDENTIFIER
select @CountryName = CountryName
from Country
where id = @CountryId;

If (@CountryName like 'All Of%')
begin 
select top 1 @RegionId = Id
from Region(NOLOCK)
where exists (select 1 from CountryRegionMap CRM (NOLOCK)
			 where CRM.RegionId = Region.id
			 and CRM.CountryId = @CountryId)
order by RegionLevel desc;
select @CountryCodes = STRING_AGG(Country.CountryCode, ',')
from Country(NOLOCK)
where exists(select 1 from CountryRegionMap CRM(NOLOCK)
			 where CRM.RegionId = @RegionId
			 and CRM.CountryId = Country.Id)
and Country.CountryName not like 'Rest Of%'
and Country.CountryName not like 'All Of%';

end
else if (@CountryName like 'Rest Of%')
begin
select top 1 @RegionId = Id
from Region(NOLOCK)
where exists (select 1 from CountryRegionMap CRM(NOLOCK)
			 where CRM.RegionId = Region.id
			 and CRM.CountryId = @CountryId)
order by RegionLevel desc;
select @CountryCodes = STRING_AGG(Country.CountryCode, ',')
from Country(NOLOCK)
where exists(select 1 from CountryRegionMap CRM(NOLOCK)
			 where CRM.RegionId = @RegionId
			 and CRM.CountryId = Country.Id)
and Country.CountryName not like 'Rest Of%'
and Country.CountryName not like 'All Of%'
and not exists(select 1 from MarketValue MV(NOLOCK)
				where MV.ProjectId = @ProjectId
				and mv.CountryId = Country.Id)
end
else
begin
select @CountryCodes = Country.CountryCode
from Country(NOLOCK)
where id = @CountryId
end
	return @CountryCodes
end