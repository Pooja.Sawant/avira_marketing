﻿CREATE PROCEDURE [dbo].[SFab_GetGrowthType]
	-- Add the parameters for the stored procedure here
	@AviraUserId Uniqueidentifier=null
As    
/*================================================================================    
Procedure Name: [SFab_GetGrowthType] 
Get Data from Table
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
08-Jan-2020 Raju/Harshal   Initial Version    
================================================================================*/
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;
SELECT Id,GrowthTypeName 
FROM  GrowthType WHERE IsDeleted = 0  ORDER BY GrowthTypeName
END