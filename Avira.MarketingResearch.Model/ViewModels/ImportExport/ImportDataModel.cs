﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.ImportExport
{
    public class ImportDataModel
    {
        public Guid Id { get; set; }
        public string TemplateName { get; set; }
        public string StatusName { get; set; }
        public string ImportFilePath { get; set; }
    }
}
