﻿CREATE procedure [dbo].[SSec_UpdateUserLogin]  
(@UserName nvarchar(100),  
@Success Bit)  
as  
/*================================================================================  
Procedure Name: SSec_GetUserLogin  
Author: Laxmikant  
Create date: 02/11/2019  
Description: Get user login details by Name  
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
03/04/2019 Laxmikant   Initial Version  
================================================================================*/  
BEGIN  
SET NOCOUNT ON;  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   
If @Success = 1  
	begin  
	update [dbo].[AviraUser]  
	set 
		IsEverLoggedIn = 1,  
		LastLoggedOn = getdate(),  
		ConcurrentLoginFailureCount = 0  
	where UserName = @UserName or EmailAddress = @UserName;  
	end  
else  
	begin  
	update [dbo].[AviraUser]  
	set 
			ConcurrentLoginFailureCount = isnull(ConcurrentLoginFailureCount,0)+1, 
			IsUserLocked = case when ConcurrentLoginFailureCount >= 2 then 1 else IsUserLocked end  
	where UserName = @UserName or EmailAddress = @UserName  
	end  
select	[Id], [UserName],[Salt],[PassWord],UserRoleId,IsUserLocked,ConcurrentLoginFailureCount from [dbo].[AviraUser] where UserName = @UserName or EmailAddress = @UserName; 
end