﻿CREATE TABLE [dbo].[CompanyKeyEmployeesMap] (
    [Id]                                            UNIQUEIDENTIFIER NOT NULL,
    [KeyEmployeeName]                               NVARCHAR (256)   NOT NULL,
    [DesignationId]                                 UNIQUEIDENTIFIER NOT NULL,
    [KeyEmployeeEmailId]                            NVARCHAR (512)   NULL,
    [KeyEmployeeComments]                           NVARCHAR (1000)  NULL,
    [IsDirector]                                    BIT              NOT NULL,
    [CompanyBoardOfDirecorsOtherCompanyName]        NVARCHAR (256)   NULL,
    [CompanyBoardOfDirecorsOtherCompanyDesignation] NVARCHAR (256)   NULL,
    [ManagerId]                                     UNIQUEIDENTIFIER NULL,
    [CompanyId]                                     UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]                                     DATETIME         NULL,
    [UserCreatedById]                               UNIQUEIDENTIFIER NULL,
    [ModifiedOn]                                    DATETIME         NULL,
    [UserModifiedById]                              UNIQUEIDENTIFIER NULL,
    [IsDeleted]                                     BIT              CONSTRAINT [DF_CompanyKeyEmployeesMap_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]                                     DATETIME         NULL,
    [UserDeletedById]                               UNIQUEIDENTIFIER NULL,
    CONSTRAINT [Pk_CompanyKeyEmployeesMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CompanyKeyEmployeesMap_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyKeyEmployeesMap_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyKeyEmployeesMap_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([Id]),
    CONSTRAINT [FK_CompanyKeyEmployeesMap_Designation] FOREIGN KEY ([DesignationId]) REFERENCES [dbo].[Designation] ([Id]),
    CONSTRAINT [FK_CompanyKeyEmployeesMap_Manager] FOREIGN KEY ([ManagerId]) REFERENCES [dbo].[CompanyKeyEmployeesMap] ([Id])
);



