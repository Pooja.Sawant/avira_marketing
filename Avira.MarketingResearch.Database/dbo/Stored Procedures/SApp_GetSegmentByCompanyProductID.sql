﻿

      
 CREATE PROCEDURE [dbo].[SApp_GetSegmentByCompanyProductID]            
 (@ComapanyProductID uniqueidentifier = null, 
  @includeDeleted bit = 0     
 )            
 AS            
/*================================================================================            
Procedure Name: SApp_GetSegmentByCompanyProductID      
Author: Harshal            
Create date: 05/02/2019            
Description: Get list from Segment table by CompanyProductID            
Change History            
Date  Developer  Reason            
__________ ____________ ______________________________________________________            
05/02/2019 Harshal   Initial Version 
================================================================================*/           
            
BEGIN            
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED         
begin        
        
 SELECT         
   [Segment].[Id] AS [SegmentId],            
   [Segment].[SegmentName],           
   [Segment].[Description],        
   [Segment].IsDeleted,        
   CompanyProductSegmentMap.Id as CompanySegmentMapId      
   --cast (case when exists(select 1 from dbo.CompanyProductCategoryMap         
   --  where CompanyProductSegmentMap.CompanyProductId = @ComapanyProductID        
   --  and CompanyProductSegmentMap.IsDeleted = 0        
   --  and CompanyProductSegmentMap.SegmentId = Segment.Id) then 1 else 0 end as bit) as IsMapped        
       
FROM [dbo].[Segment]      
LEFT JOIN dbo.CompanyProductSegmentMap       
ON Segment.Id = CompanyProductSegmentMap.SegmentId
and CompanyProductSegmentMap.CompanyProductId = @ComapanyProductID 
and CompanyProductSegmentMap.IsDeleted = 0
where  (@includeDeleted = 1 or Segment.IsDeleted = 0)        
      
  END        
       
END