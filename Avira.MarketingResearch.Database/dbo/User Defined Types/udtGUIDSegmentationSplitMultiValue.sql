﻿CREATE TYPE [dbo].[udtGUIDSegmentationSplitMultiValue] AS TABLE (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [Segmentation] NVARCHAR (512)   NULL,
    [SegmentValue] DECIMAL (19, 4)  NULL);

