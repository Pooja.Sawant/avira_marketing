﻿

 CREATE PROCEDURE [dbo].[SApp_GetQualitativeTableTabbyProjectID]        
 (@ProjectID uniqueidentifier = null,    
 @includeDeleted bit = 0,    
 @OrdbyByColumnName NVARCHAR(50) ='TableName',    
 @SortDirection INT = -1,    
 @PageStart INT = 0,  
 @Search NVARCHAR(512)='',    
 @PageSize INT = null)        
 AS        
/*================================================================================        
Procedure Name: [SApp_GetQualitativeTableTabbyProjectID]  
Author: Harshal        
Create date: 06/13/2019        
Description: Get list from QualitativeTable table by ProjectID        
Change History        
Date  Developer  Reason        
__________ ____________ ______________________________________________________        
06/13/2019 Harshal   Initial Version        
================================================================================*/       
        
BEGIN        
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED     
if @includeDeleted is null set @includeDeleted = 0;  
if len(@Search)>2 set @Search = '%'+ @Search + '%'  
else set @Search = ''  
if @PageSize is null or @PageSize =0    
set @PageSize = 10    
     
begin    
;WITH CTE_TBL AS (    
 SELECT     
  [QualitativeTableTab].Id,
  [QualitativeTableTab].ProjectId,      
  [QualitativeTableTab].IsDeleted,
  [QualitativeTableTab].TableName,    
 CASE    
  WHEN @OrdbyByColumnName = 'TableName' THEN [QualitativeTableTab].[TableName] 
  ELSE [QualitativeTableTab].[TableName]    
 END AS SortColumn    
    
FROM [dbo].[QualitativeTableTab]   
where [QualitativeTableTab].ProjectID=@ProjectID   AND
      (@Search = '' or [QualitativeTableTab].[TableName] like @Search)  
      AND ([QualitativeTableTab].IsDeleted = 0 Or [QualitativeTableTab].IsDeleted is null)    
)    
    
 SELECT     
    CTE_TBL.*,    
    tCountResult.TotalItems    
  FROM CTE_TBL    
  CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult    
  ORDER BY     
    CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,    
    CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC     
  OFFSET @PageStart ROWS    
  FETCH NEXT @PageSize ROWS ONLY;    
  END    
    
END