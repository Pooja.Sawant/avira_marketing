﻿CREATE PROCEDURE [dbo].[SFab_GetMarketTree]
	(@MarketId uniqueidentifier = null,
	 @ProjectId uniqueidentifier)
	AS
/*================================================================================
Procedure Name:[SFab_MarketTree]
Author: Adarsh
Create date: 07/03/2019
Description: Get list from Market table by Name or ID
Change History
Date		Developer		Reason

================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

declare @marketIdList table (SegmentId	uniqueidentifier,
							 SubSegmentId	uniqueidentifier);

insert into @marketIdList
select distinct SegmentId, SubSegmentId
from QualitativeSegmentMapping QSM
where QSM.ProjectID = @ProjectId
and (@MarketId is null or QSM.MarketId = @MarketId)
and QSM.IsDeleted = 0;

declare @SubMarketList table(SegmentId	uniqueidentifier,
						SegmentName	nvarchar(256),
						SubSegmentId	uniqueidentifier,
						SubSegmentName	nvarchar(256),
						SubsegmentLevel int,
						SubSegmentParentId	uniqueidentifier,
						SubSegmentParentName	nvarchar(256)
						);


----End User Industry
--;WITH ITree_CTE AS (       
--SELECT [I].[Id],
--		[I].IndustryName,
--		[I].[ParentId],
--		PI.[IndustryName] as ParentIndustryName,
--		cast(1 as int) as ILevel
--		--[M].[IsDeleted]
--FROM Industry [I] 
--left join Industry [PI]
--on I.ParentId = [PI].Id
--and [PI].IsDeleted = 0
--where I.IsDeleted = 0
--and exists (select 1 from @marketIdList list
--			inner join Segment Seg
--			on Seg.ID = list.SegmentId
--			and I.Id = list.SubSegmentId
--			and Seg.SegmentName = 'End User'
--			)
--UNION ALL     
--SELECT [I].[Id],
--		[I].IndustryName,
--		[I].[ParentId],
--		[PI].[IndustryName] as ParentIndustryName,
--		[PI].ILevel - 1 as ILevel
--		--[M].[IsDeleted]
--FROM Industry [I] 
--inner join ITree_CTE [PI]
--on I.ParentId = [PI].Id
--where I.IsDeleted = 0
--)
--insert into @SubMarketList (SegmentId,
--						SegmentName,
--						SubSegmentId,
--						SubSegmentName,
--						SubsegmentLevel,
--						SubSegmentParentId,
--						SubSegmentParentName)
--SELECT (Select top 1 id from Segment where SegmentName = 'End User') as SegmentId,
-- 'End User' as SegmentName,
--M.Id as SubSegmentId,
--M.IndustryName as SubSegmentName,
--M.ILevel as  SubsegmentLevel,
--M.ParentId as SubSegmentParentId,
--M.ParentIndustryName as SubSegmentParentName
--	FROM ITree_CTE M
--order by ILevel, IndustryName;

----Service list
--insert into @SubMarketList (SegmentId,
--						SegmentName,
--						SubSegmentId,
--						SubSegmentName,
--						SubsegmentLevel)
--select (Select top 1 id from Segment where SegmentName = 'Services') as SegmentId,
-- 'Services' as SegmentName,
-- S.ID as ServiceId, 
--S.ServiceName,
--1 as SubsegmentLevel
--from [Service] S 
--where S.IsDeleted = 0
--and exists (select 1 from @marketIdList list
--			inner join Segment Seg
--			on Seg.ID = list.SegmentId
--			and S.Id = list.SubSegmentId
--			and Seg.SegmentName = 'Services'
--			);

----RawMaterialId
--insert into @SubMarketList (SegmentId,
--						SegmentName,
--						SubSegmentId,
--						SubSegmentName,
--						SubsegmentLevel)

--select (Select top 1 id from Segment where SegmentName = 'Raw materials') as SegmentId,
-- 'Raw materials' as SegmentName,
-- RM.ID as RawMaterialId, 
--RM.RawMaterialName,
--1 as SubsegmentLevel
--from [RawMaterial] RM 
--where RM.IsDeleted = 0
--and exists (select 1 from @marketIdList list
--			inner join Segment Seg
--			on Seg.ID = list.SegmentId
--			and RM.Id = list.SubSegmentId
--			and Seg.SegmentName = 'Raw materials'
--			);

----ComponentId
--insert into @SubMarketList (SegmentId,
--						SegmentName,
--						SubSegmentId,
--						SubSegmentName,
--						SubsegmentLevel)

--select (Select top 1 id from Segment where SegmentName = 'Components') as SegmentId,
-- 'Components' as SegmentName,
-- CM.ID as ComponentId, 
--CM.ComponentName,
--1 as SubsegmentLevel
--from [Component] CM 
--where CM.IsDeleted = 0
--and exists (select 1 from @marketIdList list
--			inner join Segment Seg
--			on Seg.ID = list.SegmentId
--			and CM.Id = list.SubSegmentId
--			and Seg.SegmentName = 'Components'
--			);


----ApplicationId
--insert into @SubMarketList (SegmentId,
--						SegmentName,
--						SubSegmentId,
--						SubSegmentName,
--						SubsegmentLevel)

--select (Select top 1 id from Segment where SegmentName = 'Applications') as SegmentId,
-- 'Applications' as SegmentName,
-- AP.ID as ApplicationId, 
--AP.ApplicationName,
--1 as SubsegmentLevel
--from [Application] AP 
--where AP.IsDeleted = 0
--and exists (select 1 from @marketIdList list
--			inner join Segment Seg
--			on Seg.ID = list.SegmentId
--			and AP.Id = list.SubSegmentId
--			and Seg.SegmentName = 'Applications'
--			);

insert into @SubMarketList (SegmentId,
						SegmentName,
						SubSegmentId,
						SubSegmentName,
						SubsegmentLevel)
select Segment.Id,
Segment.SegmentName,
SubSegment.Id,
SubSegment.SubSegmentName,
1 as SubsegmentLevel
from Segment 
inner join @marketIdList List
on Segment.Id = List.SegmentId
inner join SubSegment
on SubSegment.Id = List.SubSegmentId
--where Segment.SegmentName not in ('Raw materials', 'Applications','Services', 'End User');

select * 
from @SubMarketList
order by SegmentName, SubsegmentLevel, SubSegmentName;

END