﻿using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Utility;
using Avira.MarketingResearch.WebApi;
using CacheManager.Core;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;

namespace Avira.MarketingResearch.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegionController : AviraBaseController
    {
        private ILoggerManager _logger;
        public readonly IRegionRepository _regionRepository;
        public readonly IRegionService _regionService;
        public RegionController(
            IRegionService regionService,
            IRegionRepository regionRepository,         
            ILoggerManager logger)
        {
            _regionRepository = regionRepository;
            _regionService = regionService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<RegionViewModel>> Get(int PageStart = 0, int pageSize = 0, int SortDirection = 1, string OrdbyByColumnName = "CreatedDate", string Search = "", bool isDeletedInclude = false)
        {
            List<RegionViewModel> res = null;
            try
            {
                res = _regionService.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet("{id}")]
        public ActionResult<RegionViewModel> Get(Guid id)
        {
            RegionViewModel res = null;
            try
            {
                res = _regionService.GetById(id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpPost]
        public ActionResult Post([FromBody] RegionInsertDbViewModel viewModel)
        {
            viewModel.CreatedOn = System.DateTime.UtcNow;
            viewModel.UserCreatedById = CurrentUser.Id;
            viewModel.Id = Guid.NewGuid();   
            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new Region, please contact Admin for more details."
            };
            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                var result = _regionService.Create(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Region name " + viewModel.RegionName + " is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Ok(viewData);
        }


        [HttpPut]
        public ActionResult Put([FromBody] RegionUpdateDbViewModel viewModel)
        {
            viewModel.ModifiedOn = System.DateTime.UtcNow;
            viewModel.UserModifiedById = CurrentUser.Id;


            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while updating a Region, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                Result<SpTransactionMessage> result = _regionService.Update(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "RawMaterial name " + viewModel.RegionName + " is updated successfully.";

                }
                else
                {
                    viewData.Message = result.Error;
                }
            }

            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }

        [Route("DeleteSegment")]
        [HttpPut]
        public ActionResult DeleteSegment([FromBody] RegionUpdateDbViewModel viewModel)
        {

            viewModel.DeletedOn = System.DateTime.UtcNow;
            viewModel.UserDeletedById = CurrentUser.Id;
            viewModel.IsDeleted = true;


            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while deleting a new RawMaterial, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                Result<SpTransactionMessage> result = _regionService.Update(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;

                    viewData.Message = "Region name " + viewModel.RegionName + " is deleted.";

                }
                else
                {
                    viewData.Message = result.Error;
                }
            }

            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        } 
    }
}
