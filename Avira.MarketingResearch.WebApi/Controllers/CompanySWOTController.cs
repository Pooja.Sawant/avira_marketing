﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanySWOTController : AviraBaseController
    {
        private readonly ICompanySWOTService _companySWOTService;
        private readonly ICompanySwotRepository _companySWOTRepository;
        private ILoggerManager _logger;

        public CompanySWOTController(
            ICompanySWOTService CompanySWOTService,
            ICompanySwotRepository CompanySWOTRepository,
             ILoggerManager logger)
        {
            _companySWOTRepository = CompanySWOTRepository;
            _companySWOTService = CompanySWOTService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<MainCompanySWOTViewModel> Get(Guid CompanyId,Guid ProjectId)
        {
            MainCompanySWOTViewModel res = null;
            try
            {
                 res = _companySWOTService.GetAll(CompanyId, ProjectId);
        }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpPost]
        public ActionResult Post([FromBody] MainCompanySWOTViewModel viewModel)
        {
            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a adding Company SWOT mapping, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                var result = _companySWOTService.InsertUpdate(viewModel, CurrentUser);
                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Company SWOT is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }
    }
}