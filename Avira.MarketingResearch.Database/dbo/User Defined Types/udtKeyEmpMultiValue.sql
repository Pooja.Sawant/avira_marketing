﻿CREATE TYPE [dbo].[udtKeyEmpMultiValue] AS TABLE (
    [KeyEmployeeName]                               NVARCHAR (256)   NOT NULL,
    [DesignationID]                                 UNIQUEIDENTIFIER NOT NULL,
    [KeyEmployeeEmailId]                            NVARCHAR (256)   NOT NULL,
    [KeyEmployeeComments]                           NVARCHAR (1000)  NULL,
    [ManagerID]                                     UNIQUEIDENTIFIER NULL,
    [CompanyBoardOfDirecorsOtherCompanyName]        NVARCHAR (256)   NULL,
    [CompanyBoardOfDirecorsOtherCompanyDesignation] NVARCHAR (256)   NULL);

