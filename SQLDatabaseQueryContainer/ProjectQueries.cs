﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public static class ProjectQueries
    {
        public static readonly string StoreProcInsert = "SApp_insertProject";

        public static readonly string StoreProcUpdate = "SApp_UpdateProject";

        public static readonly string StoreProcSelectById = "[SApp_GetProjectbyID]";

        public static readonly string StoreProcSelectAll = "SApp_ProjectGridWithFilters";
        public static readonly string StoreProcSelectAllProject = "SApp_GetAllProjects";
        public static readonly string StoreProcSelectAllIndustry = "SApp_GetIndustrybyID";
        public static readonly string StoreProcSelectAllMarket = "SApp_GetMarketbyID";
        public static readonly string StoreProcUpdteStatus = "SApp_UpdateProjectStatus";
        public static readonly string StoreProcGetSegment = "SApp_GetSegmentbyProjectID";
    }
}
