﻿


CREATE PROCEDURE [dbo].[SApp_GetAllParentService]
	(
		@includeDeleted bit = 0
	)
	AS
/*================================================================================
Procedure Name: [SApp_GetAllParentService]
Author: Harshal
Create date: 03/29/2019
Description: Get list from Industry table by ParentId
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
03/29/2019	Harshal			Initial Version
================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
declare @SegmentId uniqueidentifier;
select @SegmentId = Id from Segment
where SegmentName = 'Services';

	SELECT [SubSegment].[Id],
		[SubSegment].SubSegmentName AS [ServiceName],
		[SubSegment].[Description],
		[SubSegment].ParentId AS [ParentServiceId],
		[SubSegment].[IsDeleted]
		
FROM [dbo].[SubSegment]
where ([SubSegment].ParentId is NULL )
	AND [SubSegment].SegmentId = @SegmentId
	AND ([SubSegment].IsDeleted = 0)
	
	END