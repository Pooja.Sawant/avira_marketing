﻿create procedure [dbo].[SApp_DeleteUserbyID]
(@AviraUserId uniqueidentifier,
@LoginUserId uniqueidentifier)
as
/*================================================================================
Procedure Name: SApp_DeleteUserbyID
Author: Sharath
Create date: 02/19/2019
Description: soft delete a record from User table by ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
02/19/2019	Sharath			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


UPDATE [dbo].[AviraUser]
   SET [IsDeleted] = 1,
   [DeletedOn] = getdate(),
   [UserDeletedById] = @LoginUserId
where ID = @AviraUserId
and IsDeleted = 0 -- no need to delete record which is already deleted

end