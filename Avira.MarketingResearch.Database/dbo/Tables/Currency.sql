﻿CREATE TABLE [dbo].[Currency] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [CurrencyCode]     NVARCHAR (10)    NOT NULL,
    [CurrencyName]     NVARCHAR (256)   CONSTRAINT [DF_Currency_CurrencyName] DEFAULT ('') NOT NULL,
    [CurrencySymbol]   NVARCHAR (10)    NOT NULL,
    [Fractionalunit]   NVARCHAR (50)    NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_Currency_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Currency_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_Currency_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id])
);

