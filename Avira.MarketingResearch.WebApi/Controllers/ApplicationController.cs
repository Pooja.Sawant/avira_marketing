﻿using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using static Avira.MarketingResearch.Model.ViewModels.ApplicationViewModel;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApplicationController : AviraBaseController
    {
        public readonly IApplicationRepository _applicationRepository;
        public readonly IApplicationService _applicationService;
        private ILoggerManager _logger;
        public ApplicationController(
            IApplicationService aaplicationService,
            IApplicationRepository applicationRepository,
            ILoggerManager logger)
        {
            _applicationRepository = applicationRepository;
            _applicationService = aaplicationService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ApplicationViewModel>> Get(int PageStart = 0, int pageSize = 0, int SortDirection = 1, string OrdbyByColumnName = "CreatedDate", string Search = "", bool isDeletedInclude = false)
        {
            List<ApplicationViewModel> res = null;
            try
            {
                res = _applicationService.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }


        [HttpGet("{id}")]
        public ActionResult<ApplicationViewModel> Get(Guid id)
        {
            ApplicationViewModel application = null;
            try
            {
                application = _applicationService.GetById(id);
                if (application == null)
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return application;
        }

        [HttpPost]
        public ActionResult Post([FromBody] ApplicationInsertDbViewModel viewModel)
        {
            viewModel.CreatedOn = System.DateTime.UtcNow;
            viewModel.UserCreatedById = CurrentUser.Id;
            viewModel.Id = Guid.NewGuid();

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new Application, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                var result = _applicationService.Create(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Application name " + viewModel.ApplicationName + " is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }


        [HttpPut]
        public ActionResult Put([FromBody] ApplicationUpdateDbViewModel viewModel)
        {

            viewModel.ModifiedOn = System.DateTime.UtcNow;
            viewModel.UserModifiedById = CurrentUser.Id;

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while updating a new Application, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                Result<SpTransactionMessage> result = _applicationService.Update(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Application name " + viewModel.ApplicationName + " is updated successfully.";

                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }

        [Route("DeleteSegment")]
        [HttpPut]
        public ActionResult Delete([FromBody] ApplicationUpdateDbViewModel viewModel)
        {

            viewModel.DeletedOn = System.DateTime.UtcNow;
            viewModel.UserDeletedById = CurrentUser.Id;
            viewModel.IsDeleted = true;

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while deleting a new Application, please contact Admin for more details."
            };
            try
            {

                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                Result<SpTransactionMessage> result = _applicationService.Update(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;

                    viewData.Message = "Application name " + viewModel.ApplicationName + " is deleted.";

                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }
    }
}
