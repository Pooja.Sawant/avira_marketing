﻿
--drop Procedure SFab_GetCIFinancialStatementBalanceSheetTabData

--execute [SFab_GetCIFinancialStatementBalanceSheetTabData]  @CompanyID = '7cfa9e8e-d78d-41d9-be62-4e49fdc46fa6'

Create   Procedure [dbo].[SFab_GetCIFinancialStatementBalanceSheetTabData] 
(
	@CompanyID uniqueidentifier = null
)
As
/*================================================================================  
Procedure Name: [SFab_GetCIFinancialStatementBalanceSheetTabData]  
Author: Nitin 
Create date: 11-Nov-2019  
Description: Get CI Company Financial Statement - Balance Sheet Tab Data
Change History  
Date         Developer     Reason  
____________ __________  ______________________________________________________  
11-Nov-2019  Nitin       Initial Version  
================================================================================*/  
BEGIN

	--Declare @CompanyID uniqueidentifier = '97ad6d5a-56a8-4af7-961a-e220725c52f1'

	Declare @UnitId uniqueidentifier

	Declare @Years Table (CompanyID uniqueidentifier, [Year] int)
	Declare @MaxYear int, @MinYear int

	Select @MaxYear = Max([Year]), @MinYear = Min([Year])
	From CompanyPLStatement
	Where CompanyId = @CompanyID;	

	If (@MaxYear Is Not Null And @MinYear Is Not Null)
	Begin
		WITH CTE
			 AS (SELECT @CompanyID As CompanyID, @MinYear As Year1
				 UNION ALL
				 SELECT @CompanyID As CompanyID, Year1 + 1 
				 FROM   CTE
				 WHERE  Year1 < @MaxYear)
		Insert Into @Years
		SELECT *
		FROM   CTE 
	End
	
	--Select * From @Years Order by 1 desc;

	Declare @CICompanyFinancialStatementBalanceSheetTabData Table(
		CompanyId uniqueidentifier,
		[Year] int,
		Assets_								decimal (19, 4),
		CashAndCashEquivalents				decimal (19, 4),
		ShortTermInvestments				decimal (19, 4),
		AccountsReceivables					decimal (19, 4),
		Inventories							decimal (19, 4),
		CurrentTaxAssets					decimal (19, 4),
		OtherCurrentAssets					decimal (19, 4),
		AssetsHeldForSale					decimal (19, 4),
		TotalCurrentAssets					decimal (19, 4),
		LongTermInvestments					decimal (19, 4),
		PropertyPlantAndEquipment			decimal (19, 4),
		IntangibleAssets					decimal (19, 4),
		Goodwill							decimal (19, 4),
		DeferredTaxAssets					decimal (19, 4),
		OtherNoncurrentAssets				decimal (19, 4),
		TotalAssets							decimal (19, 4),
		LiabilitiesAndEquity_				decimal (19, 4),
		ShortTermDebt						decimal (19, 4),
		TradeAccountsPayable				decimal (19, 4),
		DividendsPayable					decimal (19, 4),
		IncomeTaxesPayable					decimal (19, 4),
		AccruedExpenses						decimal (19, 4),
		DeferredRevenuesShortTerm			decimal (19, 4),
		OtherCurrentLiabilities				decimal (19, 4),
		LiabilitiesHeldForSale				decimal (19, 4),
		TotalCurrentLiabilities				decimal (19, 4),
		LongTermDebt						decimal (19, 4),
		PensionBenefitObligations			decimal (19, 4),
		PostretirementBenefitObligations	decimal (19, 4),
		DeferredTaxLiabilities				decimal (19, 4),
		OtherTaxesPayable					decimal (19, 4),
		DeferredRevenuesLongTerm			decimal (19, 4),
		OtherNoncurrentLliabilities			decimal (19, 4),
		TotalLiabilities					decimal (19, 4),
		CommitmentsAndContingencies_		decimal (19, 4),
		PreferredStock						decimal (19, 4),
		CommonStock							decimal (19, 4),
		Debentures							decimal (19, 4),
		AdditionalPaidInCapital				decimal (19, 4),
		TresuryStock						decimal (19, 4),
		RetainedEarnings					decimal (19, 4),
		AccumulatedOtherComprehensiveLoss	decimal (19, 4),
		TotalShareholdersEquity				decimal (19, 4),
		MinorityInterest					decimal (19, 4),
		TotalEquity							decimal (19, 4),
		TotalLiabilitiesAndEquity			decimal (19, 4)													
	)

	Select @UnitId = Id 
	From ValueConversion
	Where ValueName = 'Millions';

	Insert Into @CICompanyFinancialStatementBalanceSheetTabData (CompanyId, [Year],
		Assets_	,
		CashAndCashEquivalents,
		ShortTermInvestments,
		AccountsReceivables,
		Inventories,
		CurrentTaxAssets,
		OtherCurrentAssets,
		AssetsHeldForSale,
		TotalCurrentAssets,
		LongTermInvestments,
		PropertyPlantAndEquipment ,
		IntangibleAssets,
		Goodwill,
		DeferredTaxAssets,
		OtherNoncurrentAssets,
		TotalAssets,
		LiabilitiesAndEquity_,
		ShortTermDebt,
		TradeAccountsPayable,
		DividendsPayable,
		IncomeTaxesPayable,
		AccruedExpenses,
		DeferredRevenuesShortTerm ,
		OtherCurrentLiabilities,
		LiabilitiesHeldForSale,
		TotalCurrentLiabilities,
		LongTermDebt,
		PensionBenefitObligations ,
		PostretirementBenefitObligations ,
		DeferredTaxLiabilities,
		OtherTaxesPayable,
		DeferredRevenuesLongTerm ,
		OtherNoncurrentLliabilities ,
		TotalLiabilities,
		CommitmentsAndContingencies_ ,
		PreferredStock,
		CommonStock,
		Debentures,
		AdditionalPaidInCapital,
		TresuryStock,
		RetainedEarnings,
		AccumulatedOtherComprehensiveLoss ,
		TotalShareholdersEquity,
		MinorityInterest,
		TotalEquity,
		TotalLiabilitiesAndEquity				
	)
	Select Y.CompanyId, Y.[Year], 
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.Assets_),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.CashAndCashEquivalents),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.ShortTermInvestments),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.AccountsReceivables),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.Inventories),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.CurrentTaxAssets),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.OtherCurrentAssets),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.AssetsHeldForSale),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.TotalCurrentAssets),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.LongTermInvestments),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.PropertyPlantAndEquipment),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.IntangibleAssets),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.Goodwill),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.DeferredTaxAssets),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.OtherNoncurrentAssets),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.TotalAssets),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.LiabilitiesAndEquity_),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.ShortTermDebt),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.TradeAccountsPayable),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.DividendsPayable),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.IncomeTaxesPayable),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.AccruedExpenses),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.DeferredRevenuesShortTerm),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.OtherCurrentLiabilities),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.LiabilitiesHeldForSale),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.TotalCurrentLiabilities),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.LongTermDebt),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.PensionBenefitObligations),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.PostretirementBenefitObligations),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.DeferredTaxLiabilities),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.OtherTaxesPayable),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.DeferredRevenuesLongTerm),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.OtherNoncurrentLliabilities),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.TotalLiabilities),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.CommitmentsAndContingencies_),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.PreferredStock),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.CommonStock),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.Debentures),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.AdditionalPaidInCapital),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.TresuryStock),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.RetainedEarnings),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.AccumulatedOtherComprehensiveLoss),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.TotalShareholdersEquity),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.MinorityInterest),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.TotalEquity),
			[dbo].[fun_UnitDeConversion](@UnitId, CBS.TotalLiabilitiesAndEquity)
	From @Years Y Left Join CompanyBalanceSheet CBS On Y.[Year] = CBS.[Year] And CBS.CompanyId = @CompanyID
	Order By Y.[Year] 

	Select top 7 *
	From @CICompanyFinancialStatementBalanceSheetTabData
	Order By [Year] Desc 
END