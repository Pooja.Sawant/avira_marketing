﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.Utils;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Avira.MarketingResearch.Web.Controllers
{
    [Authorize]
    [NoCache]
    public class CompanyOrganizationController : BaseController
    {

        public readonly IServiceRepository _serviceRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;
        private ILoggerManager _logger;

        public CompanyOrganizationController(IServiceRepository serviceRepository, ILoggerManager logger, IHostingEnvironment hostingEnvironment, IConfiguration configuration)
        {
            _hostingEnvironment = hostingEnvironment;
            _serviceRepository = serviceRepository;
            _logger = logger;
            _configuration = configuration; 
        }
        public IActionResult Index(string companyId, string projectId)
        {
            CompanyOrganizationViewModel companyOrganizationViewModel = new CompanyOrganizationViewModel();
            companyOrganizationViewModel.CompanyId = new Guid(companyId);
            companyOrganizationViewModel.ProjectId = new Guid(projectId);

            try
            {
                HttpResponseMessage CompanyResponse = _serviceRepository.GetByIdResponse("Company", companyId);
                CompanyViewModel companyViewModel = CompanyResponse.Content.ReadAsAsync<CompanyViewModel>().Result;

                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, new Guid(projectId));
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                }

                ViewBag.CompanyName = companyViewModel.CompanyName;
                ViewBag.CompanyId = new Guid(companyId);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(companyOrganizationViewModel);
        }

        public ActionResult LoadData(string Id)
        {
            List<CompanyOrganizationViewModel> resut = null;
            try
            {
                var ContollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.GetCompanyOrganizationChart(ContollerName, Id);
                response.EnsureSuccessStatusCode();
                resut = response.Content.ReadAsAsync<List<CompanyOrganizationViewModel>>().Result;
                ViewBag.CompanyId = new Guid(Id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Json(resut);
        }

        [HttpPost]
        public IActionResult Index(CompanyOrganizationViewModel model,  string actionName)
        {
            if (actionName.Equals("Next"))
                return RedirectToAction("Index", "CompanyProduct", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
            else if (actionName.Equals("Previous"))
                return RedirectToAction("Index", "CompanyFundamental", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
            return View();
        }
    }
}