﻿using System;
using System.Collections.Generic;
using System.Text;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Model;
using Avira.MarketingResearch.Model.ViewModels.Masters;

namespace Avira.MarketingResearch.Common.IService.Masters
{
   public interface IMarketService
    {
        IEnumerable<MarketViewModel> GetAll(Guid Id);
        IEnumerable<MarketViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false);
        MarketViewModel GetById(Guid guid);
        IEnumerable<MarketViewModel> GetAllParentMarket();
        Result<SpTransactionMessage> Create(MarketInsertDbViewModel tfc, AviraUser currentUser);
        Result<SpTransactionMessage> Update(MarketUpdateDbViewModel tfc, AviraUser currenUser);

    }
}
