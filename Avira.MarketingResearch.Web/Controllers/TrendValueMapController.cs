﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.Utils;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Avira.MarketingResearch.Web.Controllers
{
    [NoCache]
    [Authorization]
    public class TrendValueMapController : BaseController
    {
        private readonly IServiceRepository _serviceRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;
        string contentRootPath = string.Empty;

        private ILoggerManager _logger;
        public TrendValueMapController(IServiceRepository serviceRepository, ILoggerManager logger, IHostingEnvironment hostingEnvironment, IConfiguration configuration)
        {
            _hostingEnvironment = hostingEnvironment;
            _serviceRepository = serviceRepository;
            _logger = logger;
            _configuration = configuration;
            contentRootPath = _hostingEnvironment.ContentRootPath;
        }

        public IActionResult Index(string id)
        {
            SetRoleList();
            MainTrendValueInsertUpdateDbViewModel model = new MainTrendValueInsertUpdateDbViewModel();
            try
            {
                model.TrendId = new Guid(id); ;
                int PageStart = 0;
                int pageSize = Common.TrendPagingSize;
                int SortDirection = 1;

                //------Get master data for Impact           
                HttpResponseMessage responseImpactType = _serviceRepository.GetResponse("ImpactType");
                responseImpactType.EnsureSuccessStatusCode();
                ViewBag.ImpactList = responseImpactType.Content.ReadAsAsync<List<ImpactTypeViewModel>>().Result;

                HttpResponseMessage responseTrend = _serviceRepository.GetByIdResponse("Trend", id);
                responseTrend.EnsureSuccessStatusCode();
                TrendViewModel trend = responseTrend.Content.ReadAsAsync<TrendViewModel>().Result;
                model.TrendModel = trend;

                //------Get trend note by trend Id 
                HttpResponseMessage responseTrendNote = _serviceRepository.GetByIdResponse("TrendNote", id, "Value", "id");
                responseTrendNote.EnsureSuccessStatusCode();
                var tnm = responseTrendNote.Content.ReadAsAsync<TrendNoteViewModel>().Result;
                model.TrendNoteModel = tnm ?? new TrendNoteViewModel();
                model.TrendModel.TrendStatusName = model.TrendNoteModel.TrendStatusName;
                var valueContollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage valueResponse = _serviceRepository.GetAllParentResponse(valueContollerName);
                valueResponse.EnsureSuccessStatusCode();
                List<ValueConversionViewModel> valueConversionViewModel = valueResponse.Content.ReadAsAsync<List<ValueConversionViewModel>>().Result;
                ViewBag.ValueConversion = new SelectList(valueConversionViewModel, "ValueConversionId", "ValueName");

                var importanceContollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage ImporResponse = _serviceRepository.GetImportanceResponse(importanceContollerName);
                ImporResponse.EnsureSuccessStatusCode();
                List<ImportanceViewModel> ImportanceModel = ImporResponse.Content.ReadAsAsync<List<ImportanceViewModel>>().Result;
                model.ImportanceViewModel = ImportanceModel;

                var impactDirectContoller = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage impactResponse = _serviceRepository.GetImpactDirectionResponse(impactDirectContoller);
                impactResponse.EnsureSuccessStatusCode();
                List<ImpactDirectionViewModel> impactDirectionModel = impactResponse.Content.ReadAsAsync<List<ImpactDirectionViewModel>>().Result;
                model.ImpactDirectionViewModel = impactDirectionModel;


                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.GetResponse(contollerName, PageStart, pageSize, SortDirection, string.Empty, id);
                response.EnsureSuccessStatusCode();
                List<TrendValueViewModel> regionCountryMap = response.Content.ReadAsAsync<List<TrendValueViewModel>>().Result;
                model.TrendValueInsertUpdate = regionCountryMap;


                model.ImpactDirectionId = (model.TrendValueInsertUpdate != null && model.TrendValueInsertUpdate.Count > 0) ? model.TrendValueInsertUpdate.FirstOrDefault().ImpactDirectionId : Guid.NewGuid();
                model.ImportanceId = (model.TrendValueInsertUpdate != null && model.TrendValueInsertUpdate.Count > 0) ? model.TrendValueInsertUpdate.FirstOrDefault().ImportanceId : Guid.NewGuid();
                model.ValueConversionId = (model.TrendValueInsertUpdate != null && model.TrendValueInsertUpdate.Count > 0) ? model.TrendValueInsertUpdate.FirstOrDefault().ValueConversionId : Guid.NewGuid();


                if (model.TrendValueInsertUpdate != null)
                {
                    ViewBag.TrendValue = model.TrendValueInsertUpdate.Where(x => x.TrendValue != null).Select(x => x.TrendValue).FirstOrDefault();
                }

                List<string> indirectList = new List<string>();
                List<string> directList = new List<string>();
                List<string> endUserList = new List<string>();

                if (model.TrendValueInsertUpdate != null)
                {
                    foreach (var item in model.TrendValueInsertUpdate)
                    {
                        if (!string.IsNullOrEmpty(item.ImpactTypeName))
                        {
                            if (item.ImpactTypeName.Equals("Indirect"))
                                if (!indirectList.Contains(item.TimeTagName))
                                    indirectList.Add(item.TimeTagName);
                            if (item.ImpactTypeName.Equals("Direct"))
                                if (!directList.Contains(item.TimeTagName))
                                    directList.Add(item.TimeTagName);
                        }
                        if (!string.IsNullOrEmpty(item.TimeTagName) && item.EndUser)
                            if (!endUserList.Contains(item.TimeTagName))
                                endUserList.Add(item.TimeTagName);
                    }

                    ViewBag.Indirect = string.Join("/", indirectList);
                    ViewBag.Direct = string.Join("/", directList);
                    ViewBag.EndUser = string.Join("/", endUserList);
                }

                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, model.TrendModel.ProjectID);
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                }

                ViewBag.Id = model.TrendId;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult Index(IFormCollection collection, MainTrendValueInsertUpdateDbViewModel model, string actionName, int PageStart, string Direction, int totalRecords, int currentpage)
        {
            SetRoleList();
            var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();

            var str = collection["ImportanceId"];

            if (collection["ValueConversionId"].ToString() != "" || !string.IsNullOrEmpty(collection["ValueConversionId"].ToString()))
            {
                model.ValueConversionId = Guid.Parse(collection["ValueConversionId"]);
            }
            ViewBag.Message = string.Empty;
            int pagesize = Common.TrendPagingSize;
            int SortDirection = 1;
            bool bResult = false;

            var valueContollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            HttpResponseMessage valueResponse = _serviceRepository.GetAllParentResponse(valueContollerName);
            valueResponse.EnsureSuccessStatusCode();
            List<ValueConversionViewModel> valueConversionViewModel = valueResponse.Content.ReadAsAsync<List<ValueConversionViewModel>>().Result;
            ViewBag.ValueConversion = new SelectList(valueConversionViewModel, "ValueConversionId", "ValueName");

            var importanceContollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            HttpResponseMessage ImporResponse = _serviceRepository.GetImportanceResponse(importanceContollerName);
            ImporResponse.EnsureSuccessStatusCode();
            List<ImportanceViewModel> ImportanceModel = ImporResponse.Content.ReadAsAsync<List<ImportanceViewModel>>().Result;
            model.ImportanceViewModel = ImportanceModel;

            var impactDirectContoller = this.ControllerContext.RouteData.Values["controller"].ToString();
            HttpResponseMessage impactResponse = _serviceRepository.GetImpactDirectionResponse(impactDirectContoller);
            impactResponse.EnsureSuccessStatusCode();
            List<ImpactDirectionViewModel> impactDirectionModel = impactResponse.Content.ReadAsAsync<List<ImpactDirectionViewModel>>().Result;
            model.ImpactDirectionViewModel = impactDirectionModel;


            HttpResponseMessage responseImpactType = _serviceRepository.GetResponse("ImpactType");
            responseImpactType.EnsureSuccessStatusCode();
            ViewBag.ImpactList = responseImpactType.Content.ReadAsAsync<List<ImpactTypeViewModel>>().Result;

            List<TrendValueViewModel> trendValues = new List<TrendValueViewModel>();
            trendValues = model.TrendValueInsertUpdate;

            //var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            //HttpResponseMessage response = _serviceRepository.GetResponse(contollerName, 0, 0, 1, string.Empty, model.TrendId.ToString());
            //response.EnsureSuccessStatusCode();
            //List<TrendValueViewModel> regionCountryMap = response.Content.ReadAsAsync<List<TrendValueViewModel>>().Result;
            //model.TrendValueInsertUpdate = regionCountryMap;


            HttpResponseMessage responseTrend = _serviceRepository.GetByIdResponse("Trend", model.TrendId.ToString());
            responseTrend.EnsureSuccessStatusCode();
            TrendViewModel trend = responseTrend.Content.ReadAsAsync<TrendViewModel>().Result;
            model.TrendModel = trend ?? new TrendViewModel();

            //HttpResponseMessage trendResponse = _serviceRepository.GetResponse(contollerName, model.TrendId, PageStart, pagesize, SortDirection, "IsMapped", string.Empty);
            //trendResponse.EnsureSuccessStatusCode();
            //List<TrendValueViewModel> trendValueMap = trendResponse.Content.ReadAsAsync<List<TrendValueViewModel>>().Result;
            //model.TrendValueInsertUpdate = trendValueMap;

            ViewBag.totalRecords = totalRecords;
            ViewBag.PageStart = PageStart;
            ViewBag.currentpage = currentpage;


            //if (model.TrendValueInsertUpdate.Count == 0 && trendValues.Count > 0)
            //{
            //    foreach (var item in model.TrendValueInsertUpdate.ToList())
            //    {
            //        foreach (var value in trendValues)
            //        {
            //            item.Amount = value.Amount;
            //            item.Rationale = value.Rationale;
            //        }
            //    }
            //}

            if (model.TrendValueInsertUpdate != null)
            {
                ViewBag.TrendValue = model.TrendValueInsertUpdate.Where(x => x.TrendValue != null).Select(x => x.TrendValue).FirstOrDefault();
            }

            List<string> indirectList = new List<string>();
            List<string> directList = new List<string>();
            List<string> endUserList = new List<string>();

            if (model.TrendValueInsertUpdate != null)
            {
                foreach (var item in model.TrendValueInsertUpdate)
                {
                    if (!string.IsNullOrEmpty(item.ImpactTypeName))
                    {
                        if (item.ImpactTypeName.Equals("Indirect"))
                            if (!indirectList.Contains(item.TimeTagName))
                                indirectList.Add(item.TimeTagName);
                        if (item.ImpactTypeName.Equals("Direct"))
                            if (!directList.Contains(item.TimeTagName))
                                directList.Add(item.TimeTagName);
                    }
                    if (!string.IsNullOrEmpty(item.TimeTagName) && item.EndUser)
                        if (!endUserList.Contains(item.TimeTagName))
                            endUserList.Add(item.TimeTagName);
                }

                ViewBag.Indirect = string.Join("/", indirectList);
                ViewBag.Direct = string.Join("/", directList);
                ViewBag.EndUser = string.Join("/", endUserList);
            }

            try
            {
                model.TrendNoteModel.TrendType = "Value";

                if (actionName.Equals("save"))
                {
                    bResult = validate(model);
                    if (bResult)
                    {
                        model.TrendNoteModel.TrendStatusName = "Draft";

                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            //ViewBag.Message = viewModel.Message;
                            return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.ValueName });
                        }
                    }
                }

                else if (actionName.Equals("Submitted"))
                {
                    bResult = validate(model);
                    if (bResult)
                    {
                        model.TrendNoteModel.TrendStatusName = "Submitted";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            //ViewBag.Message = viewModel.Message;
                            return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.ValueName });
                        }
                    }
                }
                else if (actionName.Equals("Rejected"))
                {
                    bResult = validate(model);
                    if (bResult)
                    {
                        model.TrendNoteModel.TrendStatusName = "Rejected";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            //ViewBag.Message = viewModel.Message;
                            return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.ValueName });
                        }
                    }
                }
                else if (actionName.Equals("Approve"))
                {
                    bResult = validate(model);
                    if (bResult)
                    {
                        model.TrendNoteModel.TrendStatusName = "Approved";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            //ViewBag.Message = viewModel.Message;
                            return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.ValueName });
                        }
                    }
                }
                else if (actionName.Equals("search"))
                    return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.ValueName });
                else if (actionName.Equals("Next"))
                    return RedirectToAction("Index", "TrendPreview", new { id = model.TrendId.ToString() });
                else if (actionName.Equals("Previous"))
                    return RedirectToAction("Index", "TrendKeyCompanyMap", new { id = model.TrendId.ToString() });
                else if (actionName.Equals("Cancel"))
                    return RedirectToAction("Index", "TrendValueMap", new { id = model.TrendId.ToString() });

                HttpResponseMessage responseTrendNote = _serviceRepository.GetByIdResponse("TrendNote", model.TrendId.ToString(), "Company Group", "id");
                responseTrendNote.EnsureSuccessStatusCode();
                var tnm = responseTrendNote.Content.ReadAsAsync<TrendNoteViewModel>().Result;
                model.TrendNoteModel = tnm ?? new TrendNoteViewModel();
                model.TrendModel.TrendStatusName = model.TrendNoteModel.TrendStatusName;

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return View(model);
        }

        private void SetRoleList()
        {
            try
            {
                var roles = HttpContext.Session.GetString("userRoles");
                var userRoles = JsonConvert.DeserializeObject<List<RoleViewModel>>(roles);
                List<string> RoleListItems = new List<string>();
                foreach (var item in userRoles)
                    RoleListItems.Add(item.UserRoleName);
                ViewBag.UserRoleList = RoleListItems;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        /// <summary>
        /// Method to validate Form
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns> 
        private bool validate(MainTrendValueInsertUpdateDbViewModel model)
        {
            int i = 0;
            int count = 0;
            if (model.TrendValueInsertUpdate != null)
            {
                count = model.TrendValueInsertUpdate.Count;
            }
            int flag = 0;
            do
            {
                if (count > 0)
                {
                    if ((model.TrendValueInsertUpdate[i].Amount.Equals(0) && model.TrendValueInsertUpdate[i].Rationale == null))
                    {
                        ViewBag.Message = "Please select all values for Amount & Rationale";
                        flag++;
                        break;
                    }
                    else if (model.ImportanceViewModel == null)
                    {
                        ViewBag.Message = "Please select Importance.";
                        flag++;
                        break;
                    }
                    else if (model.ImpactDirectionId == null)
                    {
                        ViewBag.Message = "Please select Impact.";
                        flag++;
                        break;
                    }

                    else if (model.TrendNoteModel.AuthorRemark == null || model.TrendNoteModel.AuthorRemark == "" || model.TrendNoteModel.AuthorRemark == string.Empty)
                    {
                        ViewBag.Message = "Please select Author Notes.";
                        flag++;
                        break;
                    }
                }
                else if (model.TrendNoteModel.AuthorRemark == null || model.TrendNoteModel.AuthorRemark == "" || model.TrendNoteModel.AuthorRemark == string.Empty)
                {
                    ViewBag.Message = "Please select Author Notes.";
                    flag++;
                    break;
                }

                i++;
                if (i > count - 1)
                    break;

            } while (true);
            if (flag > 0)
                return false;
            else
                return true;
        }
    }
}