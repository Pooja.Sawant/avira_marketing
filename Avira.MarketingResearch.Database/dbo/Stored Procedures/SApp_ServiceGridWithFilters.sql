﻿
CREATE PROCEDURE [dbo].[SApp_ServiceGridWithFilters]
	(@ServiceIDList dbo.udtUniqueIdentifier READONLY,
	@ServiceName nvarchar(256)='',
	@Description nvarchar(256)='',
	@ParentServiceId uniqueidentifier=null,
	@includeDeleted bit = 0,
	@OrdbyByColumnName NVARCHAR(50) ='CreatedDate',
	@SortDirection INT = -1,
	@PageStart INT = 0,
	@PageSize INT = Null)
	AS
/*================================================================================
Procedure Name: [SAppServiceGridWithFilters]
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
03/26/2019	Harshal			Initial Version
__________	____________	______________________________________________________
04/22/2019	Pooja			For Client side paging comment pagesize and null init
================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
if @includeDeleted is null set @includeDeleted = 0;
declare @SegmentId uniqueidentifier;
select @SegmentId = Id from Segment
where SegmentName = 'Raw materials';
 
if len(@ServiceName)>2 set @ServiceName = '%'+ @ServiceName + '%'
else set @ServiceName = ''
if len(@Description)>2 set @Description = '%'+ @Description + '%';
else set @Description = '';

if @PageSize is null or @PageSize =0
	set @PageSize = 10
	begin
	;WITH CTE_TBL AS (

	SELECT [SubSegment].[Id],
		[SubSegment].SubSegmentName as [ServiceName],
		[SubSegment].[Description],
		[SubSegment].ParentId as [ParentServiceId],
		[SubSegment].[IsDeleted],
		CASE
		WHEN @OrdbyByColumnName = 'ServiceName' THEN [SubSegment].SubSegmentName
		WHEN @OrdbyByColumnName = 'Description' THEN [SubSegment].[Description]

		ELSE [SubSegment].SubSegmentName
	END AS SortColumn

FROM [dbo].[SubSegment]
where SegmentId = @SegmentId

and (NOT EXISTS (SELECT * FROM @ServiceIDList) or  [SubSegment].Id  in (select * from @ServiceIDList))
	AND (@ServiceName = '' or [SubSegment].SubSegmentName like @ServiceName)
	AND (@Description = '' or [SubSegment].[Description] like @Description)
	AND (@includeDeleted = 1 or [SubSegment].IsDeleted = 0)
	)

	SELECT 
		  CTE_TBL.*,
		  tCountResult.TotalItems
		FROM CTE_TBL
		LEFT JOIN [dbo].[SubSegment] P ON CTE_TBL.ParentServiceId=P.Id
		CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult
		ORDER BY 
		  CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,
		  CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC 
		OFFSET @PageStart ROWS
		FETCH NEXT @PageSize ROWS ONLY;
		end
	END