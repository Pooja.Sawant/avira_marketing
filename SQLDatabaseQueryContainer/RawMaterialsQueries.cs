﻿namespace SQLDatabaseQueryContainer
{
    public static class RawMaterialsQueries
    {
       public static readonly string StoreProcInsert = "SApp_insertRawMaterial";

        public static readonly string StoreProcUpdate = "SApp_UpdateRawMaterial";

        public static string StoreProcSelectAll = "SApp_RawMaterialGridWithFilters";
    }
}



