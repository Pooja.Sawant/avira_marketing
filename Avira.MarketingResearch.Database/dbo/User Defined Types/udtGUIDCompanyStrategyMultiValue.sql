﻿CREATE TYPE [dbo].[udtGUIDCompanyStrategyMultiValue] AS TABLE (
    [Id]         UNIQUEIDENTIFIER NOT NULL,
    [CompanyId]  UNIQUEIDENTIFIER NOT NULL,
    [Date]       DATETIME2 (7)    NULL,
    [Approach]   UNIQUEIDENTIFIER NULL,
    [Strategy]   NVARCHAR (256)   NULL,
    [Category]   UNIQUEIDENTIFIER NULL,
    [Importance] UNIQUEIDENTIFIER NULL,
    [Impact]     UNIQUEIDENTIFIER NULL,
    [Remark]     NVARCHAR (512)   NULL);

