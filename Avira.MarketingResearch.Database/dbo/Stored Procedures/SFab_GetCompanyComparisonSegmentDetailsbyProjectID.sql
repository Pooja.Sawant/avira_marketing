﻿
CREATE procedure [dbo].[SFab_GetCompanyComparisonSegmentDetailsbyProjectID]
(
@SegmentID uniqueidentifier=null,
@SegmentList dbo.[udtUniqueIdentifier] readonly,
--@RegionId uniqueidentifier = null,
@RegionIdList dbo.[udtUniqueIdentifier] readonly,
@CountryIdList dbo.[udtUniqueIdentifier] readonly,
--@CompanystageId UniqueIdentifier=null,
@CompanystageId [dbo].[udtUniqueIdentifier] READONLY ,
@StratergyList [dbo].[udtUniqueIdentifier] READONLY,
@Years nvarchar(100),
@ProjectId uniqueidentifier,
@CompanyIdList dbo.[udtUniqueIdentifier] readonly,
@AviraUserID uniqueidentifier = null
)
as
/*================================================================================
Procedure Name: [SFab_GetCompanyComparisonSegmentDetailsbyProjectID]
Author: Harshal
Create date: 07/29/2019
Description: Get list from company table by project ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
07/29/2019	Harshal	    Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ProjectApprovedStatusId uniqueidentifier
select @ProjectApprovedStatusId = ID from ProjectStatus
where ProjectStatusName = 'Completed';

declare @RegionList dbo.[udtUniqueIdentifier];
declare @YearList table (Year int);
insert into @YearList
select value
from string_split(@Years,',')

insert into @RegionList
select Id
from Region
where id in(select ID from @RegionIdList)
or exists (select 1
			from CountryRegionMap CRM
			inner join @CountryIdList Loc
			on CRM.CountryId = Loc.ID
			and Region.id = CRM.RegionId);

;With Clist as (select comp.Id as CompanyId,
	 comp.CompanyName
	from Company comp
where comp.IsDeleted = 0 
	and exists (select 1 from CompanyProjectMap cpmap
				inner join project proj
				on cpmap.ProjectId = proj.ID
				where comp.Id = cpmap.CompanyId 
				and proj.ProjectStatusID = @ProjectApprovedStatusId
	and cpmap.ProjectId = @ProjectId)
	and (not exists(select 1 from @CompanyIdList) or comp.Id in (select ID from @CompanyIdList))
	and (not exists (select * from @RegionList) 
		or exists(select 1 from CompanyRegionMap CRM
				  inner join @RegionList Rlist
				  on CRM.RegionId = Rlist.ID))
	--and (@CompanystageId is null or comp.CompanyStageId = @CompanystageId)
		and (not exists (select * from @CompanystageId) 
		or exists(select 1 from [CompanyStage] CST
				inner join  @CompanystageId CSTAList
				on CST.Id = CSTAList.ID
				where comp.CompanyStageId = CSTAList.ID))
	and (not exists (select * from @StratergyList) 
		or exists(select 1 from [CompanyStrategyMapping] CSM
				inner join  @StratergyList CSlist
				on CSM.Category = CSlist.ID
				where comp.Id = CSM.CompanyId))
	--and (not exists(select 1 from @SegmentList)
	--	or exists(select 1 from CompanyProduct CP
	--				inner join CompanyProductSegmentMap CPSM
	--				on cp.Id=cpsm.CompanyProductId 
	--				inner join @SegmentList list
	--				on list.ID = CPSM.SegmentId
	--				where cp.CompanyId = comp.Id))
	and (not exists(select 1 from @SegmentList)
        or exists(SELECT 1 FROM CompanyEcosystemPresenceESMMap CPSM                   
                    inner join @SegmentList list
                    on list.ID = CPSM.ESMId
                    where CPSM.CompanyId = comp.Id))
	and (@Years is null or @Years = '' 
		or exists(select 1 from CompanyRevenue
					inner join @YearList Ylist
					on Ylist.Year = CompanyRevenue.Year
					and CompanyRevenue.CompanyId = comp.Id))
)
select Clist.CompanyId,
Clist.CompanyName,
[TableType],
[TableName],
[Rows],
[Columns],
[TableMetadata],
[TableData]
from Clist
inner join CompanyProfileRevenueTableTab Revenue
on Clist.CompanyId = Revenue.CompanyId
where [TableType] = 'IncomeStatement'

end