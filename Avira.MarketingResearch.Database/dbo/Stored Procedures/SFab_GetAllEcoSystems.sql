﻿CREATE procedure [dbo].[SFab_GetAllEcoSystems]

as
/*================================================================================
Procedure Name: [SFab_GetAllEcoSystems]
Author: Pooja Sawant
Create date: 05/27/2019
Description: Get list from EcoSystem (Segment)
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
05/27/2019	Pooja		Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT [Id], 
		[SegmentName] as EcoSystemName
	FROM [dbo].[Segment]
where ([Segment].[IsDeleted] = 0)  order by SegmentName
end