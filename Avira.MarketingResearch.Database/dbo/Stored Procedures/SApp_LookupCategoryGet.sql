﻿CREATE PROCEDURE [dbo].[SApp_LookupCategoryGet]
(
@CategoryType nvarchar(100)
)
 AS      
/*================================================================================      
Procedure Name: SApp_LookupCategoryGet      
Author: Praveen      
Create date: 10/25/2019      
Description: Get list from LookupCategory BY Type      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
10/25/2019 Praveen   Initial Version      
================================================================================*/      
      
BEGIN      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED      

SELECT Id,
CategoryType,
CategoryName,
Description FROM LookupCategory WHERE IsDeleted  = 0 AND CategoryType = @CategoryType ORDER BY CategoryName 
      
 END