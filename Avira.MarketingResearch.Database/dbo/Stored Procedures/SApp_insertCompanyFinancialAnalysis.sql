﻿

CREATE procedure [dbo].[SApp_insertCompanyFinancialAnalysis]
(
@CompanyId uniqueidentifier, 
@CurrencyId uniqueidentifier,
--@CompanyFinancialId uniqueidentifier,
@UnitId uniqueidentifier,
@CompanyStatusId uniqueidentifier=null,
@AuthorNotes nvarchar(2000)=null,
@ApproverNotes nvarchar(2000)=null,
@UserCreatedById uniqueidentifier,
@jsonAssets NVARCHAR(MAX)=null,
@jsonExpence NVARCHAR(MAX)=null,
@jsonCash NVARCHAR(MAX)=null,
@jsonTrans NVARCHAR(MAX)=null
)
as
/*================================================================================
Procedure Name: SApp_insertCompanyFinancialAnalysis
Author: Pooja Sawant
Create date: 05/06/2019
Description: Insert a record into CompanyFinancialAnalysis table
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
05/06/2019	Pooja			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ErrorMsg NVARCHAR(2048);

Begin try

declare @CompanyFinancialId uniqueidentifier = NULL;
--SELECT @CompanyFinancialId = Id FROM [CompanyFinancialAnalysis] where companyId =  @CompanyId

--if @CompanyStatusId is null
--SELECT @CompanyStatusId = Id FROM ComanyProfileStatus WHERE CompanyStatusName = 'Draft';

--For companyFinancialanalysis Parent Table
if exists(select 1 from CompanyFinancialAnalysis where CompanyId=@CompanyId )
BEGIN 
		SELECT @CompanyFinancialId = Id FROM [CompanyFinancialAnalysis] where companyId =  @CompanyId

		update [dbo].[CompanyFinancialAnalysis]
		set AuthorNotes= @AuthorNotes,
		ApproverNotes=@ApproverNotes,
		CurrencyId = @CurrencyId,
		UnitId = @UnitId,
		UserModifiedById = @UserCreatedById
		--@CompanyFinancialId = [CompanyFinancialAnalysis].ID
		where [CompanyFinancialAnalysis].CompanyId = @CompanyId;
END
ELSE
begin
--Insert new records
		set @CompanyFinancialId = NEWID();
		INSERT INTO [dbo].[CompanyFinancialAnalysis]([Id],      
        [CompanyId],      
        [CurrencyId], 
		[UnitId],
		[CompanyStatusId],
        [AuthorNotes],      
        [ApproverNotes],      
        [CreatedOn],
        [UserCreatedById],
        [IsDeleted])      
 values( @CompanyFinancialId,
   @CompanyId,
   @CurrencyId,       
   @UnitId,
   @CompanyStatusId,     
   @AuthorNotes,     
   @ApproverNotes,
   GETDATE(),
   @UserCreatedById,
   0);
 end


declare @CFAData TABLE
(ExpensesType NVARCHAR(50),
BaseYearMinus_1 DECIMAL(19,4),
BaseYearMinus_2 DECIMAL(19,4),
BaseYearMinus_3 DECIMAL(19,4),
BaseYearMinus_4 DECIMAL(19,4),
BaseYearMinus_5 DECIMAL(19,4));

declare @CFADataUnpivot TABLE
(CompanyFinancialID uniqueidentifier,
ExpensesType NVARCHAR(50),
Expense NVARCHAR(50),
Amount DECIMAL(19,4),
[Year] int);

insert into @CFAData(ExpensesType,
        BaseYearMinus_1,
		BaseYearMinus_2,
		BaseYearMinus_3,
		BaseYearMinus_4,
		BaseYearMinus_5)
select  ExpensesType,
        BaseYearMinus_1,
		BaseYearMinus_2,
		BaseYearMinus_3,
		BaseYearMinus_4,
		BaseYearMinus_5
    FROM OPENJSON(@jsonExpence)
    WITH (ExpensesType NVARCHAR(50),
	BaseYearMinus_1 DECIMAL(19,4),
	BaseYearMinus_2 DECIMAL(19,4),
	BaseYearMinus_3 DECIMAL(19,4),
	BaseYearMinus_4 DECIMAL(19,4),
	BaseYearMinus_5 DECIMAL(19,4))as jsonExpence

delete @CFAData
where (BaseYearMinus_1 is null or BaseYearMinus_1 = 0)
and (BaseYearMinus_2 is null or BaseYearMinus_2 = 0)
and (BaseYearMinus_3 is null or BaseYearMinus_3 = 0)
and (BaseYearMinus_4 is null or BaseYearMinus_4 = 0)
and (BaseYearMinus_5 is null or BaseYearMinus_5 = 0)

declare @BaseYear int
select @BaseYear = BaseYear from BaseYearMaster
--2018

insert into @CFADataUnpivot(CompanyFinancialID,ExpensesType, Expense, Amount, [Year])
SELECT @CompanyFinancialId,ExpensesType, Expense, Amount, @BaseYear - ROW_NUMBER() over (Partition by ExpensesType order by Expense) + 1 as [Year]
FROM @CFAData 
UNPIVOT
(
	Amount
	FOR Expense in (BaseYearMinus_1, BaseYearMinus_2, BaseYearMinus_3, BaseYearMinus_4, BaseYearMinus_5)
) AS CFA_Unpivot;


--For start CompanyExpensesBreakdown Table   
update [dbo].[CompanyExpensesBreakdown]
set ActualAmount = dbo.fun_UnitConversion(@UnitId,cfad.Amount),
ModifiedOn = GETDATE(),
UserModifiedById = @UserCreatedById
from [dbo].[CompanyExpensesBreakdown]
inner join @CFADataUnpivot cfad
on  cfad.ExpensesType =  CompanyExpensesBreakdown.ExpensesType
and cfad.[Year] = CompanyExpensesBreakdown.[Year]
where [CompanyExpensesBreakdown].CompanyFinancialID = @CompanyFinancialId;
--Insert new records
insert into [dbo].[CompanyExpensesBreakdown]([Id],      
        [CompanyFinancialID],      
        [ExpensesType], 
		[Year],
		[ActualAmount],
		[CreatedOn],
		[UserCreatedById],
        [IsDeleted])
              
 select NEWID(),
		@CompanyFinancialId,      
        ExpensesType, 
		[Year],
		dbo.fun_UnitConversion(@UnitId,Amount),
		getdate(),
		@UserCreatedById,
        0
	FROM @CFADataUnpivot as cfa
	where not exists (select 1 from [dbo].[CompanyExpensesBreakdown] compexp
					where compexp.CompanyFinancialID = @CompanyFinancialId
					and cfa.[Year] = compexp.[Year]
					and cfa.ExpensesType = compexp.ExpensesType);    					
--For end CompanyExpensesBreakdown Table   

	
--For start CompanyAssetsLiabilities Table 
declare @AssetsLiabilitiesData TABLE
(AssetsType NVARCHAR(50),
BaseYearMinus_1 DECIMAL(19,4),
BaseYearMinus_2 DECIMAL(19,4),
BaseYearMinus_3 DECIMAL(19,4),
BaseYearMinus_4 DECIMAL(19,4),
BaseYearMinus_5 DECIMAL(19,4));

declare @AssetsLiabilitiesDataUnpivot TABLE
(CompanyFinancialID uniqueidentifier,
AssetsType NVARCHAR(50),
Expense NVARCHAR(50),
Amount DECIMAL(19,4),
[Year] int);

insert into @AssetsLiabilitiesData(AssetsType,
        BaseYearMinus_1,
		BaseYearMinus_2,
		BaseYearMinus_3,
		BaseYearMinus_4,
		BaseYearMinus_5)
select  AssetsType,
        BaseYearMinus_1,
		BaseYearMinus_2,
		BaseYearMinus_3,
		BaseYearMinus_4,
		BaseYearMinus_5
    FROM OPENJSON(@jsonAssets)
    WITH (AssetsType NVARCHAR(50),
	BaseYearMinus_1 DECIMAL(19,4),
	BaseYearMinus_2 DECIMAL(19,4),
	BaseYearMinus_3 DECIMAL(19,4),
	BaseYearMinus_4 DECIMAL(19,4),
	BaseYearMinus_5 DECIMAL(19,4))as jsonAssets

delete @AssetsLiabilitiesData
where (BaseYearMinus_1 is null or BaseYearMinus_1 = 0)
and (BaseYearMinus_2 is null or BaseYearMinus_2 = 0)
and (BaseYearMinus_3 is null or BaseYearMinus_3 = 0)
and (BaseYearMinus_4 is null or BaseYearMinus_4 = 0)
and (BaseYearMinus_5 is null or BaseYearMinus_5 = 0)


insert into @AssetsLiabilitiesDataUnpivot(AssetsType, Expense, Amount, [Year])
SELECT AssetsType, Expense, Amount, @BaseYear - ROW_NUMBER() over (Partition by AssetsType order by Expense) + 1 as [Year]
FROM @AssetsLiabilitiesData 
UNPIVOT
(
	Amount
	FOR Expense in (BaseYearMinus_1, BaseYearMinus_2, BaseYearMinus_3, BaseYearMinus_4, BaseYearMinus_5)
) AS CFA_Unpivot;
 

update [dbo].[CompanyAssetsLiabilities]
set ActualAmount = dbo.fun_UnitConversion(@UnitId,cfad.Amount),
ModifiedOn = GETDATE(),
UserModifiedById = @UserCreatedById
from [dbo].[CompanyAssetsLiabilities]
inner join @AssetsLiabilitiesDataUnpivot cfad
on cfad.AssetsType =  [CompanyAssetsLiabilities].[AssetsType]
and cfad.[Year] = [CompanyAssetsLiabilities].[Year]
where [CompanyAssetsLiabilities].CompanyFinancialID = @CompanyFinancialId;

--Insert new records
insert into [dbo].[CompanyAssetsLiabilities]([Id],      
        [CompanyFinancialID],      
        [AssetsType], 
		[Year],
		[ActualAmount],
		--[ConversionAmount],
		[CreatedOn],
		[UserCreatedById],
        [IsDeleted])
              
 select NEWID(),
		@CompanyFinancialId,      
        AssetsType, 
		[Year],
		dbo.fun_UnitConversion(@UnitId,Amount),
		--ConversionAmount,
		getdate(),
		@UserCreatedById,
        0
	FROM @AssetsLiabilitiesDataUnpivot as cfa
	where not exists (select 1 from [dbo].[CompanyAssetsLiabilities] compexp
					where compexp.CompanyFinancialID = @CompanyFinancialId
					and cfa.[Year] = compexp.[Year]
					and cfa.AssetsType = compexp.AssetsType);  
--For end CompanyAssetsLiabilities Table 

--For start CompanyCashFlows Table 
declare @CashFlowData TABLE
(CashFlowType NVARCHAR(50),
BaseYearMinus_1 DECIMAL(19,4),
BaseYearMinus_2 DECIMAL(19,4),
BaseYearMinus_3 DECIMAL(19,4),
BaseYearMinus_4 DECIMAL(19,4),
BaseYearMinus_5 DECIMAL(19,4));

declare @CashFlowDataUnpivot TABLE
(CompanyFinancialID uniqueidentifier,
CashFlowType NVARCHAR(50),
Expense NVARCHAR(50),
Amount DECIMAL(19,4),
[Year] int);

insert into @CashFlowData(CashFlowType,
        BaseYearMinus_1,
		BaseYearMinus_2,
		BaseYearMinus_3,
		BaseYearMinus_4,
		BaseYearMinus_5)
select  CashFlowType,
        BaseYearMinus_1,
		BaseYearMinus_2,
		BaseYearMinus_3,
		BaseYearMinus_4,
		BaseYearMinus_5
    FROM OPENJSON(@jsonCash)
    WITH (CashFlowType NVARCHAR(50),
	BaseYearMinus_1 DECIMAL(19,4),
	BaseYearMinus_2 DECIMAL(19,4),
	BaseYearMinus_3 DECIMAL(19,4),
	BaseYearMinus_4 DECIMAL(19,4),
	BaseYearMinus_5 DECIMAL(19,4))as jsonCash

delete @CashFlowData
where (BaseYearMinus_1 is null or BaseYearMinus_1 = 0)
and (BaseYearMinus_2 is null or BaseYearMinus_2 = 0)
and (BaseYearMinus_3 is null or BaseYearMinus_3 = 0)
and (BaseYearMinus_4 is null or BaseYearMinus_4 = 0)
and (BaseYearMinus_5 is null or BaseYearMinus_5 = 0)


insert into @CashFlowDataUnpivot(CompanyFinancialID,CashFlowType, Expense, Amount, [Year])
SELECT @CompanyFinancialId,CashFlowType, Expense, Amount, @BaseYear - ROW_NUMBER() over (Partition by CashFlowType order by Expense) + 1 as [Year]
FROM @CashFlowData 
UNPIVOT
(
	Amount
	FOR Expense in (BaseYearMinus_1, BaseYearMinus_2, BaseYearMinus_3, BaseYearMinus_4, BaseYearMinus_5)
) AS CFA_Unpivot;
 

update [dbo].[CompanyCashFlows]
set ActualAmount = dbo.fun_UnitConversion(@UnitId,cfad.Amount),
ModifiedOn = GETDATE(),
UserModifiedById = @UserCreatedById
from [dbo].[CompanyCashFlows]
inner join @CashFlowDataUnpivot cfad
on  cfad.CashFlowType =  [CompanyCashFlows].[CashFlowType]
and cfad.[Year] = [CompanyCashFlows].[Year]
where [CompanyCashFlows].CompanyFinancialID = @CompanyFinancialId;

--Insert new records
insert into [dbo].[CompanyCashFlows]([Id],      
        [CompanyFinancialID],      
        [CashFlowType], 
		[Year],
		[ActualAmount],
		[CreatedOn],
		[UserCreatedById],
        [IsDeleted])
              
 select NEWID(),
		@CompanyFinancialId,      
        [CashFlowType], 
		[Year],
		dbo.fun_UnitConversion(@UnitId,Amount),
		getdate(),
		@UserCreatedById,
        0
	FROM @CashFlowDataUnpivot as cfa
	where not exists (select 1 from [dbo].[CompanyCashFlows] compexp
					where compexp.CompanyFinancialID = @CompanyFinancialId
					and cfa.[Year] = compexp.[Year]
					and cfa.CashFlowType = compexp.CashFlowType);  
--For end CompanyCashFlows Table

--For start CompanyTransactions Table 
update [dbo].[CompanyTransactions]
set CategoryId =jsonTrans.CategoryId,
Value=dbo.fun_UnitConversion(@UnitId,jsonTrans.Value),
OtherParty=jsonTrans.OtherParty,
Date=jsonTrans.Date,
Rationale=jsonTrans.Rationale, 
ModifiedOn = GETDATE(),
UserModifiedById = @UserCreatedById
from [dbo].[CompanyTransactions]
inner join OPENJSON(@jsonTrans)
    WITH ([Id] [uniqueidentifier] ,
		[CategoryId] [uniqueidentifier] ,
		[Value] [decimal](18, 4)  ,
		[OtherParty] [nvarchar](200) ,
		[Date] [datetime],
		[Rationale] [nvarchar](max)
		
		 ) as jsonTrans on jsonTrans.Id=CompanyTransactions.ID 
where [CompanyTransactions].CompanyFinancialID = @CompanyFinancialId;

--Insert new records
insert into [dbo].[CompanyTransactions]([Id],      
        [CompanyFinancialID],      
        [CategoryId], 
		[Value],
		[OtherParty],
		[Date],
		[Rationale],
		[CreatedOn],
		[UserCreatedById],
        [IsDeleted])
              
 select NEWID(),
		@CompanyFinancialId,      
		CategoryId, 
		dbo.fun_UnitConversion(@UnitId,Value) as Value,
		OtherParty,
		Date,
		Rationale,
		getdate(),
		@UserCreatedById,
        0
	FROM OPENJSON(@jsonTrans)
    WITH ([Id] [uniqueidentifier] ,
		[CategoryId] [uniqueidentifier] ,
		[Value] [decimal](18, 4)  ,
		[OtherParty] [nvarchar](200) ,
		[Date] [datetime],
		[Rationale] [nvarchar](max)
		
		 ) as jsonTrans
		  where (jsonTrans.Id is null or jsonTrans.Id ='00000000-0000-0000-0000-000000000000' or  not exists (select 1 from [dbo].[CompanyTransactions] compcash
					where compcash.CompanyFinancialID = @CompanyFinancialId
					and jsonTrans.Id = compcash.ID
					))
		and jsonTrans.[CategoryId]!='00000000-0000-0000-0000-000000000000';   
--For end CompanyCashFlows Table   		  				  
SELECT StoredProcedureName ='SApp_insertCompanyFinancialAnalysis',Message =@ErrorMsg,Success = 1;       
    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);      
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());      
    
 set @ErrorMsg = 'Error while insert a new Company Financial Analysis, please contact Admin for more details.';    
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
       
END CATCH      
      
      
end