﻿--sp_helptext SApp_GetIndustrybyTrendID

 CREATE PROCEDURE [dbo].[SApp_GetIndustrybyTrendID]                  
 (@TrendID uniqueidentifier = null,       
  @IndustryName nvarchar(256)='',              
 @includeDeleted bit = 0,              
 @OrdbyByColumnName NVARCHAR(50) ='IndustryName',              
 @SortDirection INT = -1,              
 @PageStart INT = 0,              
 @PageSize INT = null)                  
 AS                  
/*================================================================================                  
Procedure Name: SApp_GetIndustrybyTrendID              
Author: Sharath                  
Create date: 10/19/2019                  
Description: Get list from Industry table by TrendID                  
Change History                  
Date  Developer  Reason                  
__________ ____________ ______________________________________________________                  
10/19/2019 Sharath   Initial Version                  
================================================================================*/                 
                  
BEGIN                  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED               
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED        
DECLARE @ProjectIdID AS uniqueidentifier    
    
SELECT @ProjectIdID = ProjectID  FROM [Trend]  WHERE id = @TrendID    
declare @SegmentId uniqueidentifier;
select @SegmentId = Id from Segment
where SegmentName = 'End User';
             
if @PageSize is null or @PageSize =0                
set @PageSize = 10                
if isnull(@IndustryName,'') = '' set @IndustryName = ''         
else set @IndustryName = '%'+ @IndustryName +'%';   

begin              
;WITH CTE_TBL AS (              
 SELECT                
 --@TrendID as TrendID,                
 [Industry].[Id] AS [IndustryId],                  
  [Industry].SubSegmentName [IndustryName],                 
   [Industry].[Description],              
   [Industry].IsDeleted,              
   TrendIndustryMap.Impact as ImpactId,            
   TrendIndustryMap.EndUser,            
   TrendIndustryMap.TrendId,            
   TrendIndustryMap.Id as TrendIndustryMapId,            
  cast (case when exists(select 1 from dbo.TrendIndustryMap               
     where TrendIndustryMap.TrendId = @TrendID              
     and TrendIndustryMap.IsDeleted = 0              
     and TrendIndustryMap.IndustryId = Industry.Id) then 1 else 0 end as bit) as IsMapped,              
 CASE              
  WHEN @OrdbyByColumnName = 'IndustryName' THEN [Industry].SubSegmentName              
  WHEN @OrdbyByColumnName = 'Description' THEN [Industry].[Description]       
  when @OrdbyByColumnName = 'IsMapped' THEN cast(case when TrendIndustryMap.TrendId is null then 1 else 0 end as varchar(2))                
  ELSE [Industry].SubSegmentName              
 END AS SortColumn              
              
FROM [dbo].SubSegment [Industry]              
LEFT JOIN dbo.TrendIndustryMap             
ON Industry.Id = TrendIndustryMap.IndustryId      
--left join ProjectSegmentMapping psm on psm.SegmentID = Industry.id    
and TrendIndustryMap.TrendId = @TrendID      
and TrendIndustryMap.IsDeleted = 0                
where (@IndustryName = '' or [Industry].SubSegmentName LIKE @IndustryName)  
and [Industry].SegmentId = @SegmentId
AND (@includeDeleted = 1 or Industry.IsDeleted = 0)     
--AND psm.ProjectID = @ProjectIdID    
--AND psm.SegmentType in ('Broad Industries','Industries')             
)              
              
 SELECT               
    CTE_TBL.*,              
    tCountResult.TotalItems              
  FROM CTE_TBL              
  CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult              
  ORDER BY               
    CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,              
    CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC               
  OFFSET @PageStart ROWS              
  FETCH NEXT @PageSize ROWS ONLY;              
  END              
              
END