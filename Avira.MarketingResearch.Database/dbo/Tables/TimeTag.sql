﻿CREATE TABLE [dbo].[TimeTag] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [TimeTagName]      NVARCHAR (256)   CONSTRAINT [DF_TimeTag_TimeTagName] DEFAULT ('') NOT NULL,
    [Description]      NVARCHAR (256)   NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_TimeTag_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    [Seq]              INT              NULL,
    [DurationYears]    TINYINT          NULL,
    CONSTRAINT [PK_TimeTag] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TimeTag_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_TimeTag_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id])
);


GO

GO

GO

GO

GO

GO

GO