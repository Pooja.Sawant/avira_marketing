﻿CREATE PROCEDURE [dbo].[SFab_GetDecision]      
(    
 @DecisionId UNIQUEIDENTIFIER,
 @AviraUserID UNIQUEIDENTIFIER  
)  
As      
  

/*================================================================================      
Procedure Name: SFab_GetDecision     
Author: Praveen      
Create date: 08-Jan-2020  
Description: To return single Decision of the perticular decision  
Change History      
Date  Developer  Reason    
__________ ____________ ______________________________________________________      
08-Jan-2020  Praveen   Initial Version  
21-JAN-2020  Raju      Implementation
24-Jan-2020  Pooja S   Implementation
31-Jan-2020  Nitin	   Issue fixed
================================================================================*/      
BEGIN      
------------------  
  
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
	declare @DecisionStatusId uniqueidentifier
	declare @ErrorMsg NVARCHAR(2048)

	select @DecisionStatusId = ID from LookupStatus
	where StatusName = 'Decision made' and StatusType='DMDecisionStatus';

	If Not Exists(Select Id From Decision Where ID = @DecisionId and UserId = @AviraUserID)
	Begin

		If Exists(Select Id From Decision Where BaseDecisionID = @DecisionId and UserId = @AviraUserID)
		Begin

			Select @DecisionId = Id From Decision Where BaseDecisionID = @DecisionId and UserId = @AviraUserID
		End
	End

	If Not Exists (Select ID from Decision Where Id = @DecisionId and UserId= @AviraUserID)
	Begin

		SELECT BaseDecision.Id AS DecisionId, BaseDecision.Description, BaseDecision.Score, '' AS TargetedOutcome, @AviraUserID AS UserId, 
		'false' AS IsReadonly, '' AS AssignTo, '' AS Deadline, '' AS DecisionOutlook, GrowthType.GrowthTypeName AS DecisionType, 
        LookupStatus.StatusName,
		 STUFF((SELECT ', ' + LookupCategory.CategoryName 
          FROM BaseDecisionFunction v2		  
		  LEFT JOIN LookupCategory ON v2.BusinessFunctionId = LookupCategory.Id AND CategoryType = 'DMBusinessFunction'	
          WHERE v2.BaseDecisionId = BaseDecision.Id
          ORDER BY CategoryName
          FOR XML PATH('')), 1, 1, '') DecisionFunction,

		  STUFF((SELECT ', ' + LookupCategory.CategoryName 
          FROM BaseDecisionBusinessRole v2		  
		  LEFT JOIN LookupCategory ON v2.BusinessRoleId = LookupCategory.Id AND CategoryType = 'DMBusinessRole'	
          WHERE v2.BaseDecisionId = BaseDecision.Id
          ORDER BY CategoryName
          FOR XML PATH('')), 1, 1, '') DecisionBusinessRole,

		 STUFF((SELECT ', ' + LookupCategory.CategoryName 
          FROM BaseDecisionBusinessStage v2		  
		  LEFT JOIN LookupCategory ON v2.BusinessStageId = LookupCategory.Id AND CategoryType = 'DMBusinessStage'	
          WHERE v2.BaseDecisionId = BaseDecision.Id
          ORDER BY CategoryName
          FOR XML PATH('')), 1, 1, '') DecisionBusinessStage
FROM  BaseDecision INNER JOIN
   LookupStatus ON BaseDecision.StatusId = LookupStatus.Id LEFT  JOIN
   GrowthType ON BaseDecision.DecisionTypeId = GrowthType.Id  and LookupStatus.StatusType='DMDecisionStatus'
	
		Where BaseDecision.Id = @DecisionId
		--return;
	End
	ELSE
	BEGIN

	SELECT        Decision.Id AS DecisionId, Decision.Description, Decision.Score, '' AS TargetedOutcome, Decision.UserId, CASE WHEN (Decision.StatusId = @DecisionStatusId) THEN 'true' ELSE 'false' END AS IsReadonly, 
                         Decision.ResourceAssigned AS AssignTo, CASE WHEN ISNULL(Decision.Deadline, '') = '' THEN '' ELSE FORMAT(Decision.Deadline, 'dd/MMM/yyyy ') END AS Deadline, TimeTag.TimeTagName AS DecisionOutlook, 
                         GrowthType.GrowthTypeName AS DecisionType, LookupStatus.StatusName
						  , 
						   STUFF((SELECT ', ' + LookupCategory.CategoryName 
          FROM DecisionFunction v2		  
		  LEFT JOIN LookupCategory ON v2.BusinessFunctionId = LookupCategory.Id AND CategoryType = 'DMBusinessFunction'	
          WHERE v2.DecisionId = Decision.Id
          ORDER BY CategoryName
          FOR XML PATH('')), 1, 1, '') DecisionFunction,

		  STUFF((SELECT ', ' + LookupCategory.CategoryName 
          FROM DecisionBusinessRole v2		  
		  LEFT JOIN LookupCategory ON v2.BusinessRoleId = LookupCategory.Id AND CategoryType = 'DMBusinessRole'	
          WHERE v2.DecisionId = Decision.Id
          ORDER BY CategoryName
          FOR XML PATH('')), 1, 1, '') DecisionBusinessRole,

		 STUFF((SELECT ', ' + LookupCategory.CategoryName 
          FROM DecisionBusinessStage v2		  
		  LEFT JOIN LookupCategory ON v2.BusinessStageId = LookupCategory.Id AND CategoryType = 'DMBusinessStage'	
          WHERE v2.DecisionId = Decision.Id
          ORDER BY CategoryName
          FOR XML PATH('')), 1, 1, '') DecisionBusinessStage
FROM            Decision INNER JOIN
                         LookupStatus ON Decision.StatusId = LookupStatus.Id LEFT  JOIN
                         GrowthType ON Decision.DecisionTypeId = GrowthType.Id LEFT  JOIN
                         TimeTag ON Decision.TimeTagOutlookId = TimeTag.Id LEFT  JOIN
                         DecisionOutcome ON Decision.Id = DecisionOutcome.DecisionId

		where Decision.Id=@DecisionId and Decision.UserId=@AviraUserID  and LookupStatus.StatusType='DMDecisionStatus'     
		order by Decision.Description
	
	END


	

    
END