﻿CREATE TABLE [dbo].[ESMAnalysis] (
    [Id]                        UNIQUEIDENTIFIER NOT NULL,
    [ProjectId]                 UNIQUEIDENTIFIER NOT NULL,
    [MarketId]                  UNIQUEIDENTIFIER NULL,
    [SegmentId]                 UNIQUEIDENTIFIER NOT NULL,
    [SubSegmentId]              UNIQUEIDENTIFIER NULL,
    [QualitativeAnalysisTypeId] UNIQUEIDENTIFIER NOT NULL,
    [ImportanceId]              UNIQUEIDENTIFIER NULL,
    [ImpactId]                  UNIQUEIDENTIFIER NULL,
    [AnalysisText]              NVARCHAR (4000)  NULL,
    [CreatedOn]                 DATETIME2 (7)    NULL,
    [UserCreatedById]           UNIQUEIDENTIFIER NULL,
    [ModifiedOn]                DATETIME2 (7)    NULL,
    [UserModifiedById]          UNIQUEIDENTIFIER NULL,
    [IsDeleted]                 BIT              NOT NULL,
    [DeletedOn]                 DATETIME2 (7)    NULL,
    [UserDeletedById]           UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_ESMAnalysis] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ESMAnalysis_Project] FOREIGN KEY ([ProjectId]) REFERENCES [dbo].[Project] ([ID]),
    CONSTRAINT [FK_ESMAnalysis_QualitativeAnalysisType] FOREIGN KEY ([QualitativeAnalysisTypeId]) REFERENCES [dbo].[QualitativeAnalysisType] ([Id]),
    CONSTRAINT [FK_ESMAnalysis_Segment] FOREIGN KEY ([SegmentId]) REFERENCES [dbo].[Segment] ([Id]),
    CONSTRAINT [FK_ESMAnalysis_SubSegment] FOREIGN KEY ([SubSegmentId]) REFERENCES [dbo].[SubSegment] ([Id])
);

