﻿CREATE procedure [dbo].[SApp_insertUpdateMarket]
(@MarketID uniqueidentifier,
@Description nvarchar(256),
@MarketName nvarchar(256),
@IsDeleted bit,
@AviraUserId uniqueidentifier)
as
/*================================================================================
Procedure Name: SApp_DeleteMarketbyID
Author: Sharath
Create date: 02/19/2019
Description: soft delete a record from Market table by ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
02/19/2019	Sharath			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

IF exists(select 1 from [dbo].[Market] 
		 where ((@MarketID is null or [Id] !=  @MarketID)
		 and  MarketName = @MarketName AND IsDeleted = 0))
begin
declare @ErrorMsg NVARCHAR(2048);
set @ErrorMsg = 'Market with Name "'+ @MarketName + '" already exists.';
THROW 50000,@ErrorMsg,1;
end

IF (@MarketID is null)
begin
--Insert new record
	set @MarketID = NEWID();

	insert into [dbo].[Market]([Id],
								[MarketName],
								[Description],
								[IsDeleted],
								[UserCreatedById],
								[CreatedOn])
	values(@MarketID, 
			@MarketName, 
			@Description, 
			0,
			@AviraUserId, 
			getdate());
end
else
begin
--update record
	update [dbo].[Market]
	set [MarketName] = @MarketName,
		[Description] = @Description,
		[ModifiedOn] = getdate(),
		DeletedOn = case when IsDeleted = 0 and @IsDeleted = 1 then getdate() else DeletedOn end,
		UserDeletedById = case when IsDeleted = 0 and @IsDeleted = 1 then @AviraUserId else UserDeletedById end,
		IsDeleted = isnull(@IsDeleted, IsDeleted),
		[UserModifiedById] = @AviraUserId
     where ID = @MarketID
end

select @MarketID;
end