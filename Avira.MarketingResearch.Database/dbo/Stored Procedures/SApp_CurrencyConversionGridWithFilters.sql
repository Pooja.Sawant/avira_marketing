﻿CREATE PROCEDURE [dbo].[SApp_CurrencyConversionGridWithFilters]
	(@CurrencyID uniqueidentifier,
	@ConversionRangeType TinyInt=1,
	@StartDate datetime,
	@EndDate	datetime,
	@Year	int,
	@Quarter nvarchar(25),
	@includeDeleted bit = 0,
	@OrdbyByColumnName NVARCHAR(50) ='CreatedOn',
	@SortDirection INT = -1,
	@PageStart INT = 0,
	@PageSize INT = Null)
	AS
/*================================================================================
Procedure Name: SApp_CurrencyConversionGridWithFilters
Author: Sharath
Create date: 04/16/2019
Description: Get list from Currency conversions table by currency ID. 
for range search, start date and end date are indicators. 
for yearly average Year is indicator. 
for quarterly average Year and quarter is indicator
Conversion type values: 1: By Range, 2: By yearly average, 3: by quarterly average
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
03/16/2019	Adarsh			Initial Version
================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
if @includeDeleted is null set @includeDeleted = 0;
if @StartDate is null set @StartDate = '01/01/1900';

if @EndDate is null set @EndDate = '12/31/2999'
else set @EndDate = convert(varchar, @EndDate, 101) + ' 23:59:59.997';

	if @PageSize is null or @PageSize =0
	set @PageSize = 10

	;WITH CTE_TBL AS (

	SELECT [Currency].[Id] as CurrencyId,
		[Currency].[CurrencyName],
		CurrencyConversion.ConversionRangeType,
		CurrencyConversion.ConversionRate,
		CurrencyConversion.StartDate,
		CurrencyConversion.EndDate,
		CurrencyConversion.[Year],
		CurrencyConversion.[Quarter],
		[Currency].IsDeleted,
		CurrencyConversion.Id as CurrencyConversionId,
		CASE
		WHEN @OrdbyByColumnName = 'CurrencyName' THEN [Currency].CurrencyName
		WHEN @OrdbyByColumnName = 'ConversionRangeType' THEN Cast(CurrencyConversion.ConversionRangeType as varchar(max))
		WHEN @OrdbyByColumnName = 'StartDate' THEN FORMAT(CurrencyConversion.StartDate, 'yymmdd')
		WHEN @OrdbyByColumnName = 'EndDate' THEN format(CurrencyConversion.EndDate, 'yymmdd')
		WHEN @OrdbyByColumnName = 'Year' THEN cast(CurrencyConversion.[Year] as varchar(4))
		WHEN @OrdbyByColumnName = 'Quarter' THEN right(CurrencyConversion.[Quarter],4) + left(CurrencyConversion.[Quarter],2)
		ELSE [Currency].[CurrencyName]
	END AS SortColumn

FROM [dbo].[Currency]
inner join dbo.CurrencyConversion
on Currency.Id = CurrencyConversion.CurrencyId
where ([Currency].Id = @CurrencyID)
	AND (CurrencyConversion.ConversionRangeType = @ConversionRangeType)
	AND ((@ConversionRangeType = 1 and 
		(CurrencyConversion.StartDate between @StartDate and @EndDate )
		or (CurrencyConversion.EndDate between @StartDate and @EndDate )) -- for range search
		or(@ConversionRangeType = 2 and (@year is null or currencyconversion.[Year] = @year)) -- yearly average
		or(@ConversionRangeType = 2 and (@year = 0 or currencyconversion.[Year] = @year)
			or (@ConversionRangeType = 3 and (@Quarter is null or currencyconversion.[Quarter] = @Quarter))) --Quarterly average
		)
	AND (@includeDeleted = 1 or [Currency].IsDeleted = 0)
	)

	SELECT 
		  CTE_TBL.*,
		  tCountResult.TotalItems
		FROM CTE_TBL
		CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult
		ORDER BY 
		  CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,
		  CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC 
		OFFSET @PageStart ROWS
		FETCH NEXT @PageSize ROWS ONLY;
end