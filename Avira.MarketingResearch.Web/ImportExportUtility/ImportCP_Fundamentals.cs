﻿using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Avira.MarketingResearch.Web.ImportExportUtility
{
    public class ImportCP_Fundamentals: IExcelImport
    {
        public readonly IServiceRepository _serviceRepository;
        private ILoggerManager _logger;
        public ImportCP_Fundamentals(IServiceRepository serviceRepository, ILoggerManager logger)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
        }

        public ImportResponse Import(IFormFile formFile)
        {
            var res = new ImportResponse();
            var inputModel = new CompanyFundamentalsViewModel();
            using (var stream = new MemoryStream())
            {
                formFile.CopyTo(stream);

                using (var package = new ExcelPackage(stream))
                {
                    try
                    {
                        if (package.Workbook.Worksheets["Fundamentals"] != null)
                        {
                            inputModel.CompanyBasicInfoList = GetCPCompanyBasic(package.Workbook.Worksheets["Fundamentals"]);
                            inputModel.CompanyKeywordList = GetCPKeyWords(package.Workbook.Worksheets["Fundamentals"]);
                            inputModel.CompanySegmentList = GetCPSegments(package.Workbook.Worksheets["Fundamentals"]);

                            List<CompanyFundamentalsRevenueImportModel> YearWiseRevenueList = new List<CompanyFundamentalsRevenueImportModel>();
                            if (inputModel.CompanyBasicInfoList.Count > 0)
                            {
                                for (int i = 0; i < inputModel.CompanyBasicInfoList.Count; i++)
                                {
                                    DateTime yearValue;
                                    decimal value;
                                    if (DateTime.TryParseExact(inputModel.CompanyBasicInfoList[i].Year1, "yyyy-MM", CultureInfo.InvariantCulture, DateTimeStyles.None, out yearValue))
                                    {
                                        CompanyFundamentalsRevenueImportModel model = new CompanyFundamentalsRevenueImportModel();
                                        model.Year = inputModel.CompanyBasicInfoList[i].Year1;
                                        if (Decimal.TryParse(inputModel.CompanyBasicInfoList[i].RevenueYear1, out value))
                                        {
                                            if (value < 0)
                                            {
                                                res.ResponseStatus = "Error";
                                                res.ResponseMessage = "Error while reading template. 'RevenueYear1 value not less than zero.'";
                                                res.TotalFailedRecords = 1;
                                                _logger.LogError("Error while reading template. 'RevenueYear1 value not less than zero.'");
                                                return res;
                                            }
                                            model.Revenue = inputModel.CompanyBasicInfoList[i].RevenueYear1;
                                        }
                                        else
                                        {
                                            if (!string.IsNullOrEmpty(inputModel.CompanyBasicInfoList[i].RevenueYear1))
                                            {
                                                res.ResponseStatus = "Error";
                                                res.ResponseMessage = "Error while reading template. 'RevenueYear1 value not in proper Format.'";
                                                res.TotalFailedRecords = 1;
                                                _logger.LogError("Error while reading template. 'RevenueYear1 value not in proper Format.'");
                                                return res;
                                            }
                                        }

                                        model.Currency = inputModel.CompanyBasicInfoList[i].Currency;
                                        model.Units = inputModel.CompanyBasicInfoList[i].Units;
                                        model.CompanyName = inputModel.CompanyBasicInfoList[i].CompanyName;
                                        model.RowNo = inputModel.CompanyBasicInfoList[i].RowNo;
                                        YearWiseRevenueList.Add(model);

                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(inputModel.CompanyBasicInfoList[i].Year1))
                                        {
                                            res.ResponseStatus = "Error";
                                            res.ResponseMessage = "Error while reading template. 'Year1 value not in proper Format.'";
                                            res.TotalFailedRecords = 1;
                                            _logger.LogError("Error while reading template. 'Year1 value not in proper Format.'");
                                            return res;
                                        }
                                    }
                                    if (DateTime.TryParseExact(inputModel.CompanyBasicInfoList[i].Year2, "yyyy-MM", CultureInfo.InvariantCulture, DateTimeStyles.None, out yearValue))
                                    {
                                        CompanyFundamentalsRevenueImportModel model = new CompanyFundamentalsRevenueImportModel();
                                        model.Year = inputModel.CompanyBasicInfoList[i].Year2;
                                        if (Decimal.TryParse(inputModel.CompanyBasicInfoList[i].RevenueYear2, out value))
                                        {
                                            if (value < 0)
                                            {
                                                res.ResponseStatus = "Error";
                                                res.ResponseMessage = "Error while reading template. 'RevenueYear2 value not less than zero.'";
                                                res.TotalFailedRecords = 1;
                                                _logger.LogError("Error while reading template. 'RevenueYear2 value not less than zero.'");
                                                return res;
                                            }
                                            model.Revenue = inputModel.CompanyBasicInfoList[i].RevenueYear2;
                                        }
                                        else
                                        {
                                            if (!string.IsNullOrEmpty(inputModel.CompanyBasicInfoList[i].RevenueYear2))
                                            {
                                                res.ResponseStatus = "Error";
                                                res.ResponseMessage = "Error while reading template. 'RevenueYear2 value not in proper Format.'";
                                                res.TotalFailedRecords = 1;
                                                _logger.LogError("Error while reading template. 'RevenueYear2 value not in proper Format.'");
                                                return res;
                                            }
                                        }
                                        model.Currency = inputModel.CompanyBasicInfoList[i].Currency;
                                        model.Units = inputModel.CompanyBasicInfoList[i].Units;
                                        model.CompanyName = inputModel.CompanyBasicInfoList[i].CompanyName;
                                        model.RowNo = inputModel.CompanyBasicInfoList[i].RowNo;
                                        YearWiseRevenueList.Add(model);
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(inputModel.CompanyBasicInfoList[i].Year2))
                                        {
                                            res.ResponseStatus = "Error";
                                            res.ResponseMessage = "Error while reading template. 'Year2 value not in proper Format.'";
                                            res.TotalFailedRecords = 1;
                                            _logger.LogError("Error while reading template. 'Year2 value not in proper Format.'");
                                            return res;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                res.ResponseStatus = "Error";
                                res.ResponseMessage = "Company Basic sheet is mandatory.";
                                res.TotalFailedRecords = 1;
                                _logger.LogError("Company Basic sheet is mandatory.");
                                return res;
                            }
                            inputModel.CompanyRevenueList = YearWiseRevenueList;
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'Fundamentals' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'Fundamentals' worksheet is missing.");
                            return res;
                        }                     
                        
                        if (package.Workbook.Worksheets["Fundamentals_AnalystViews"] != null)
                        {
                            inputModel.CompanyAnalystViewList = GetAnalystViews(package.Workbook.Worksheets["Fundamentals_AnalystViews"]);
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'Fundamentals_AnalystViews' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'Analyst Views' worksheet is missing.");
                            return res;
                        }

                    }
                    catch (Exception ex)
                    {
                        res.ResponseStatus = "Error";
                        res.ResponseMessage = "Error while reading template. Error Message: " + ex.Message + ". Please check input excel. More details logged in error log.";
                        res.TotalFailedRecords = 1;
                        _logger.LogError("Error Message: " + ex.Message + " Stack Trace: " + ex.StackTrace.ToString());
                        return res;
                    }
                }
            }

            //--------Call api to post data            
            inputModel.TemplateName = "CP_Fundamentals Import Template";
            inputModel.ImportFileName = formFile.FileName;

            try
            {

                HttpResponseMessage response = _serviceRepository.PostResponse("CPFundamentalsImportExport", inputModel);
                response.EnsureSuccessStatusCode();
                ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;

                res.ResponseStatus = viewModel.IsSuccess ? "Success" : "Failed";
                res.ResponseMessage = viewModel.Message;
                res.TotalRecords = inputModel.CompanyBasicInfoList.Count;
                if (res.ResponseStatus == "Success")
                {
                    res.TotalSuccessRecords = inputModel.CompanyBasicInfoList.Count;
                    res.TotalFailedRecords = 0;
                }
                else
                {
                    res.TotalSuccessRecords = 0;
                    res.TotalFailedRecords = inputModel.CompanyBasicInfoList.Count;
                }
                res.GetCompanyProfileErrorList = viewModel.cpData;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        private List<CompanyFundamentalsBasicInfoImportModel> GetCPCompanyBasic(ExcelWorksheet worksheet)
        {
            List<CompanyFundamentalsBasicInfoImportModel> companyBasicInfoList = new List<CompanyFundamentalsBasicInfoImportModel>();

            var rowCount = worksheet.Dimension.Rows;

            int colIndexProjectName = GetColIndex(worksheet, "Project Name");
            int colIndexCompanyName = GetColIndex(worksheet, "Company Name");
            int colIndexCompanyStage = GetColIndex(worksheet, "Company Stage");
            int colIndexHeadquarter = GetColIndex(worksheet, "Headquarter");
            int colIndexNature = GetColIndex(worksheet, "Nature");
            int colIndexGeographicPresnece = GetColIndex(worksheet, "Geographic Presence");
            int colIndexCurrency = GetColIndex(worksheet, "Currency");
            int colIndexUnits = GetColIndex(worksheet, "Units");
            int colIndexYear1 = GetColIndex(worksheet, "Year1");
            int colIndexRevenueYear1 = GetColIndex(worksheet, "RevenueYear1");
            int colIndexYear2 = GetColIndex(worksheet, "Year2");
            int colIndexRevenueYear2 = GetColIndex(worksheet, "RevenueYear2");


            for (int row = 7; row <= rowCount; row++)
            {
                CompanyFundamentalsBasicInfoImportModel companyBasicInfo = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

                //if (!string.IsNullOrEmpty(cn))
                //{
                companyBasicInfo = new CompanyFundamentalsBasicInfoImportModel
                {
                    RowNo = row,
                    ProjectName = worksheet.Cells[row, colIndexProjectName].Value?.ToString().Trim(),
                    CompanyName = worksheet.Cells[row, colIndexCompanyName].Value?.ToString().Trim(),
                    CompanyStage = worksheet.Cells[row, colIndexCompanyStage].Value?.ToString().Trim(),
                    Headquarter = worksheet.Cells[row, colIndexHeadquarter].Value?.ToString().Trim(),
                    Nature = worksheet.Cells[row, colIndexNature].Value?.ToString().Trim(),
                    GeographicPresence = worksheet.Cells[row, colIndexGeographicPresnece].Value?.ToString().Trim(),
                    Currency = worksheet.Cells[row, colIndexCurrency].Value?.ToString().Trim(),
                    Units = worksheet.Cells[row, colIndexUnits].Value?.ToString().Trim(),
                    Year1 = worksheet.Cells[row, colIndexYear1].Value?.ToString().Trim(),
                    RevenueYear1 = worksheet.Cells[row, colIndexRevenueYear1].Value?.ToString().Trim(),
                    Year2 = worksheet.Cells[row, colIndexYear2].Value?.ToString().Trim(),
                    RevenueYear2 = worksheet.Cells[row, colIndexRevenueYear2].Value?.ToString().Trim()
                };
                companyBasicInfoList.Add(companyBasicInfo);
                //}
            }
            return companyBasicInfoList;
        }


        private List<CompanyFundamentalsSegmentImportModel> GetCPSegments(ExcelWorksheet worksheet)
        {
            List<CompanyFundamentalsSegmentImportModel> companySegmentsList = new List<CompanyFundamentalsSegmentImportModel>();

            var rowCount = worksheet.Dimension.Rows;

            int colIndexProjectName = GetColIndex(worksheet, "Project Name");
            int colIndexCompanyName = GetColIndex(worksheet, "Company Name");
            int colIndexCompanySegment = GetColIndex(worksheet, "Company Segment");
            int colIndexCompanySubsegment = GetColIndex(worksheet, "Company Subsegment");

            for (int row = 7; row <= rowCount; row++)
            {
                CompanyFundamentalsSegmentImportModel companySegment = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

                //if (!string.IsNullOrEmpty(cn))
                //{
                companySegment = new CompanyFundamentalsSegmentImportModel
                {
                    RowNo = row,
                    ProjectName = worksheet.Cells[row, colIndexProjectName].Value?.ToString().Trim(),
                    CompanyName = worksheet.Cells[row, colIndexCompanyName].Value?.ToString().Trim(),
                    CompanySegment = worksheet.Cells[row, colIndexCompanySegment].Value?.ToString().Trim(),
                    CompanySubsegment = worksheet.Cells[row, colIndexCompanySubsegment].Value?.ToString().Trim()
                   
                };
                companySegmentsList.Add(companySegment);
                //}
            }
            return companySegmentsList;
        }

        private List<CompanyFundamentalsKeywordsImportModel> GetCPKeyWords(ExcelWorksheet worksheet)
        {
            List<CompanyFundamentalsKeywordsImportModel> companyKeywordsList = new List<CompanyFundamentalsKeywordsImportModel>();

            var rowCount = worksheet.Dimension.Rows;

            int colIndexProjectName = GetColIndex(worksheet, "Project Name");
            int colIndexCompanyName = GetColIndex(worksheet, "Company Name");
            int colIndexCompanyKeywords = GetColIndex(worksheet, "CompanyKeyWords");
            

            for (int row = 7; row <= rowCount; row++)
            {
                CompanyFundamentalsKeywordsImportModel companyKeywords = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

                //if (!string.IsNullOrEmpty(cn))
                //{
                companyKeywords = new CompanyFundamentalsKeywordsImportModel
                {
                    RowNo = row,
                    ProjectName = worksheet.Cells[row, colIndexProjectName].Value?.ToString().Trim(),
                    CompanyName = worksheet.Cells[row, colIndexCompanyName].Value?.ToString().Trim(),
                    Keywords = worksheet.Cells[row, colIndexCompanyKeywords].Value?.ToString().Trim()
                };
                companyKeywordsList.Add(companyKeywords);
                //}
            }
            return companyKeywordsList;
        }
                
        private List<CompanyFundamentalsAnalystViewImportModel> GetAnalystViews(ExcelWorksheet worksheet)
        {
            List<CompanyFundamentalsAnalystViewImportModel> CPAnalystViews = new List<CompanyFundamentalsAnalystViewImportModel>();
            var rowCount = worksheet.Dimension.Rows;

            int colIndexCompany = GetColIndex(worksheet, "Company Name");
            int colIndexAnalystView = GetColIndex(worksheet, "AnalystView");
            int colIndexDateOfUpdate = GetColIndex(worksheet, "Date of update");
            int colIndexCategory = GetColIndex(worksheet, "Category");
            int colIndexImportance = GetColIndex(worksheet, "Importance");

            for (int row = 7; row <= rowCount; row++)
            {
                CompanyFundamentalsAnalystViewImportModel CompanyAnalystViews = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

                if (!string.IsNullOrEmpty(cn))
                {
                    CompanyAnalystViews = new CompanyFundamentalsAnalystViewImportModel
                    {
                        RowNo = row,
                        Company = worksheet.Cells[row, colIndexCompany].Value?.ToString().Trim(),
                        AnalystView = worksheet.Cells[row, colIndexAnalystView].Value?.ToString().Trim(),
                        DateOfUpdate = Convert.ToDateTime(worksheet.Cells[row, colIndexDateOfUpdate].Value?.ToString().Trim()),
                        Category = worksheet.Cells[row, colIndexCategory].Value?.ToString().Trim(),
                        Importance = worksheet.Cells[row, colIndexImportance].Value?.ToString().Trim()
                    };
                    CPAnalystViews.Add(CompanyAnalystViews);
                }
            }
            return CPAnalystViews;
        }

        private int GetColIndex(ExcelWorksheet worksheet, string colName)
        {
            int colIndex = worksheet.Cells["6:6"].First(c => c.Value.ToString() == colName).Start.Column;

            return colIndex;
        }

    }
}
