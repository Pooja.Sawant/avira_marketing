﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;

namespace Avira.MarketingResearch.Common.IService.Masters
{
    public interface IFundamentalService
    {
        IEnumerable<FundamentalViewModel> GetAll(Guid Id);
        IEnumerable<FundamentalViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false);
        FundamentalViewModel GetById(Guid guid);


        Result<SpTransactionMessage> Create(FundamentalInsertDbViewModel tfc, AviraUser currentUser);
        Result<SpTransactionMessage> Update(FundamentalUpdateDbViewModel tfc, AviraUser currenUser);
    }
}
