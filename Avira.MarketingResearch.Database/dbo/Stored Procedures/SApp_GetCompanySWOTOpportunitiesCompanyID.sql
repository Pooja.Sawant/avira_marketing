﻿ 
-- =============================================            
-- Author:  Laxmikant            
-- Create date: 05/15/2019            
-- Description: Get Company swot Opportunities by CompanyId             
-- =============================================            
CREATE PROCEDURE [dbo].[SApp_GetCompanySWOTOpportunitiesCompanyID]                 
 @ComapanyID uniqueidentifier = null,             
 @includeDeleted bit = 0                 
AS            
BEGIN             
 SET NOCOUNT ON;              
 SELECT[CompanySwotOpportunitiesMap].[Id]            
      ,[CompanySwotOpportunitiesMap].[CompanyId]              
      ,[CompanySwotOpportunitiesMap].[CategoryName]   
      ,[CompanySwotOpportunitiesMap].[Description]            
      ,[CompanySwotOpportunitiesMap].[CreatedOn]            
      ,[CompanySwotOpportunitiesMap].[UserCreatedById]            
      ,[CompanySwotOpportunitiesMap].[ModifiedOn]            
      ,[CompanySwotOpportunitiesMap].[UserModifiedById]            
      ,[CompanySwotOpportunitiesMap].[IsDeleted]            
      ,[CompanySwotOpportunitiesMap].[DeletedOn]            
      ,[CompanySwotOpportunitiesMap].[UserDeletedById]            
  FROM [dbo].[CompanySwotOpportunitiesMap]        
  LEFT JOIN [dbo].[Category] Category ON [CompanySwotOpportunitiesMap].CategoryName = Category.Id          
  WHERE ([CompanySwotOpportunitiesMap].CompanyId = @ComapanyID)                  
 AND (@includeDeleted = 0 or [CompanySwotOpportunitiesMap].IsDeleted = 0)              
END