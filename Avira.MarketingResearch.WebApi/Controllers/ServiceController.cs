﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServiceController : AviraBaseController
    {
        public readonly IServiceRepository _iServiceRepository;
        public readonly IServiceService _iServiceService;
        private ILoggerManager _logger;

        public ServiceController(
            IServiceService iServiceService,
            IServiceRepository iServiceRepository,
            ILoggerManager logger)
        {
            _iServiceRepository = iServiceRepository;
            _iServiceService = iServiceService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ServiceViewModel>> Get(int PageStart = 0, int pageSize = 0, int SortDirection = 1, string OrdbyByColumnName = "CreatedDate", string Search = "", bool isDeletedInclude = false)
        {
            List<ServiceViewModel> res = null;
            try
            {
                res = _iServiceService.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName,Search, isDeletedInclude).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet]
        [Route("GetAllParent")]
        public ActionResult<IEnumerable<ServiceViewModel>> GetAllParent()
        {
            List<ServiceViewModel> res = null;
            try
            {
                res = _iServiceService.GetAllParentService().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet("{id}")]
        public ActionResult<ServiceViewModel> Get(Guid id)
        {
            ServiceViewModel service = null;
            try
            {
                service = _iServiceService.GetById(id);
                if (service == null)
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return service;
        }

        [HttpPost]
        public ActionResult Post([FromBody] ServiceInsertDbViewModel viewModel)
        {
            viewModel.CreatedOn = System.DateTime.UtcNow;
            viewModel.UserCreatedById = CurrentUser.Id;
            viewModel.Id = Guid.NewGuid();

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new Service, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                var result = _iServiceService.Create(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Service name " + viewModel.ServiceName + " is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }


        [HttpPut]
        public ActionResult Put([FromBody] ServiceUpdateDbViewModel viewModel)
        {

            viewModel.ModifiedOn = System.DateTime.UtcNow;
            viewModel.UserModifiedById = CurrentUser.Id;

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while updating a new Service, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                Result<SpTransactionMessage> result = _iServiceService.Update(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Service name " + viewModel.ServiceName + " is updated successfully.";

                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }

        [Route("DeleteSegment")]
        [HttpPut]
        public ActionResult DeleteSegment([FromBody] ServiceUpdateDbViewModel viewModel)
        {

            viewModel.DeletedOn = System.DateTime.UtcNow;
            viewModel.UserDeletedById = CurrentUser.Id;
            viewModel.IsDeleted = true;

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while deleting a new Service, please contact Admin for more details."
            };
            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                Result<SpTransactionMessage> result = _iServiceService.Update(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;

                    viewData.Message = "Service name " + viewModel.ServiceName + " is deleted.";

                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }

    }
}
