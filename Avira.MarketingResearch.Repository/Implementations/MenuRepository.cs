﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Avira.MarketingResearch.Repository.Implementations
{
    public class MenuRepository : IMenuRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "Menu";
        public MenuRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public IEnumerable<MenuViewModel> GetAll()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<MenuViewModel>("SELECT * FROM Menu", null,
                null, true, null, CommandType.Text).Where(x => x.MenuName != null);

        }

        public Result<SpTransactionMessage> Create(MenuInsertDbViewModel menuInsertDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(MenuQueries.StoreProcInsert,
                menuInsertDbViewModel,
                CommandType.StoredProcedure);
            return spTransactionMessage;

        }

        public Result<SpTransactionMessage> Update(MenuUpdateDbViewModel menuUpdateDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(MenuQueries.StoreProcUpdate,
                menuUpdateDbViewModel,
                CommandType.StoredProcedure);
            return spTransactionMessage;
        }
    }
}
