﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public static class NewsQueries
    {
        public static string StoreProcSelectById = "SApp_GetNewsByCompanyID";
        public static string StoreProcSelectAllNewsCategory = "SApp_GetNewsCategory"; 
        public static string StoreProcSelectAllImportance = "SApp_GetImportance";
        public static string StoreProcSelectAllImpactDirection = "SApp_GetImpactDirection";
        public static readonly string StoreProcInsertUpdate = "SApp_insertupdateNews";
    }
}
