﻿CREATE TABLE [dbo].[DecisionBusinessStage] (
    [DecisionId]      UNIQUEIDENTIFIER NOT NULL,
    [BusinessStageId] UNIQUEIDENTIFIER NULL,
    FOREIGN KEY ([BusinessStageId]) REFERENCES [dbo].[LookupCategory] ([Id]),
    CONSTRAINT [FK__DecisionB__Decis__71C95D1E] FOREIGN KEY ([DecisionId]) REFERENCES [dbo].[Decision] ([Id]),
    CONSTRAINT [Decision_BusinessStage] UNIQUE NONCLUSTERED ([DecisionId] ASC, [BusinessStageId] ASC)
);

