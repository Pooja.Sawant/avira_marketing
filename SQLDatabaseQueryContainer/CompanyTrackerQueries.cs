﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public class CompanyTrackerQueries
    {
        public static readonly string StoreProcSelectById = "SApp_GetCompanyProjectMapbyProjectID";
        public static readonly string StoreProcGetProjectInfoById = "";
        public static string StoreProcSelectMappingByMapId = "SApp_GetCompanyProjectMapbyID";
    }
}
