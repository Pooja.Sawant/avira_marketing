﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;

namespace Avira.MarketingResearch.Web.ImportExportUtility
{
    public class ImportCPFinancial : IExcelImport
    {
        public readonly IServiceRepository _serviceRepository;
        private ILoggerManager _logger;        

        public ImportCPFinancial(IServiceRepository serviceRepository, ILoggerManager logger)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
        }

        public List<string> GetYearList(ExcelWorksheet worksheet, int rowIndex, int columnCount)
        {
            List<string> YearList = new List<string>();

            for(int colIndex = 4; colIndex <= columnCount; colIndex++)
            {
                if (worksheet.Cells[rowIndex, colIndex].Value != null)
                {
                    YearList.Add(worksheet.Cells[rowIndex, colIndex].Value.ToString());
                }
            }
            return YearList;
        }

        public ImportResponse Import(IFormFile formFile)
        {
            var res = new ImportResponse();
            var list = new List<CPFinancialImportModel>();
            var inputModel = new CPFinancialImportInsertModel();
            using (var stream = new MemoryStream())
            {
                formFile.CopyTo(stream);

                using (var package = new ExcelPackage(stream))
                {
                    if (package.Workbook.Worksheets["CP_Financial"] == null)
                    {
                        res.ResponseMessage = "Please select correct template for the file you want to import / Error while reading template.'CP_Financial' worksheet is missing.";
                        res.TotalSuccessRecords = 0;
                        res.TotalFailedRecords = 0;
                        res.ResponseStatus = "Error";
                        return res;
                    }
                    ExcelWorksheet worksheet = package.Workbook.Worksheets["CP_Financial"];

                    var rowCount = worksheet.Dimension.Rows;
                    List<string> yearList = GetYearList(worksheet, 7, worksheet.Dimension.Columns);
                    if (yearList != null && yearList.Count > 0)
                    {
                        List<CPFinancialSegmentInformationModel> segmentInformationList;
                        list = ExcelToModel(worksheet, rowCount, yearList, out segmentInformationList);
                        inputModel.CPFinancialImportModel = list;
                        inputModel.CPFinancialSegmentInformationModel = segmentInformationList;
                        inputModel.TemplateName = "CPFinancial Import Template";
                        inputModel.ImportFileName = formFile.FileName;
                        try
                        {
                            HttpResponseMessage response = _serviceRepository.PostResponse("CPFinancialImportExport", inputModel);
                            response.EnsureSuccessStatusCode();
                            ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;

                            res.ResponseStatus = viewModel.IsSuccess ? "Success" : "Failed";
                            res.ResponseMessage = viewModel.Message;

                            res.TotalRecords = inputModel.CPFinancialImportModel.Count;
                            if (viewModel.Data == null)
                            {
                                res.TotalSuccessRecords = (inputModel.CPFinancialImportModel.Count);
                                res.TotalFailedRecords = 0;
                            }
                            else
                            {
                                res.TotalSuccessRecords = (inputModel.CPFinancialImportModel.Count) - (viewModel.Data.Count());
                                res.TotalFailedRecords = viewModel.Data.Count();
                            }
                            res.ResponseModel = viewModel.CBIData;
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError(ex.Message);
                        }
                    }
                    else
                    {
                        res.ResponseStatus = "Error";
                        res.ResponseMessage = "Please add aleast one year data.";
                    }
            }
            }            
            
            return res;
        }

        private static List<CPFinancialImportModel> ExcelToModel(ExcelWorksheet worksheet, int rowCount, List<string> yearList, out List<CPFinancialSegmentInformationModel> segmentInformationList)
        {
            List<CPFinancialImportModel> list = new List<CPFinancialImportModel>();
            segmentInformationList = new List<CPFinancialSegmentInformationModel>();

            for (int yearIndex = 0; yearIndex < yearList.Count; yearIndex++)
            {
                Dictionary<string, string> keyValues = new Dictionary<string, string>();

                var colIndex = 4 + yearIndex;
                //int rowIndex = 8;

                for (int rowIndex1 = 8; rowIndex1 <= rowCount; rowIndex1++)
                {

                    if (worksheet.Cells[rowIndex1, 1].Value == null ||
                        worksheet.Cells[rowIndex1, 1].Value.ToString() == string.Empty )
                    {
                        continue;
                    }

                    if (worksheet.Cells[rowIndex1, 1].Value.ToString() == Avira.MarketingResearch.Web.Utils.Constants.CPFinancialSegmentRow)
                    {

                        if (worksheet.Cells[rowIndex1, 2].Value != null && worksheet.Cells[rowIndex1, 3].Value != null)
                        {
                            CPFinancialSegmentInformationModel segmentInformation = new CPFinancialSegmentInformationModel
                            {
                                RowNo = rowIndex1,
                                CompanyName = worksheet.Cells[1, 4].Value?.ToString().Trim(),
                                Currency = worksheet.Cells[4, 4].Value?.ToString().Trim(),
                                FinanicialYear = yearList[yearIndex],
                                Value = worksheet.Cells[rowIndex1, colIndex].Value == null ? string.Empty : worksheet.Cells[rowIndex1, colIndex].Value.ToString(),
                                Type = worksheet.Cells[rowIndex1, 2].Value.ToString().Trim(),
                                Name = worksheet.Cells[rowIndex1, 3].Value.ToString().Trim(),
                                Units = worksheet.Cells[5, 4].Value?.ToString().Trim()
                            };
                            segmentInformationList.Add(segmentInformation);
                        }
                        continue;
                    }

                    if (worksheet.Cells[rowIndex1, 2].Value != null && worksheet.Cells[rowIndex1, 2].Value.ToString() != string.Empty)
                    {
                        var value = worksheet.Cells[rowIndex1, colIndex].Value == null ? "" : worksheet.Cells[rowIndex1, colIndex].Value.ToString();

                        keyValues.Add(worksheet.Cells[rowIndex1, 2].Value?.ToString().Trim(), value);
                    }
                }
                CPFinancialImportModel model = new CPFinancialImportModel();
                model.CompanyName = worksheet.Cells[1, 4].Value?.ToString().Trim();
                model.Currency = worksheet.Cells[4, 4].Value?.ToString().Trim();
                model.Units = worksheet.Cells[5, 4].Value?.ToString().Trim();

                model.FinanicialYear = yearList[yearIndex];

                model.Revenue = keyValues["Revenue"].ToString();
                model.CostsAndExpenses = keyValues["Costs and expenses:"].ToString();
                model.CostOfProduction = keyValues["Cost of Production"].ToString();
                model.ResourceCost = keyValues["Resource cost"].ToString();
                model.SalaryCost = keyValues["Salary cost"].ToString();
                model.CostOfSales = keyValues["Cost of sales"].ToString();
                model.SellingExpenses = keyValues["Selling expenses"].ToString();
                model.MarketingExpenses = keyValues["Marketing expenses"].ToString();
                model.AdministrativeExpenses = keyValues["Administrative expenses"].ToString();
                model.GeneralExpenses = keyValues["General expenses"].ToString();
                model.SellingGeneralAndAdminExpenses = keyValues["Selling, General and Admin expenses"].ToString();
                model.ResearchAndDevelopmentExpenses = keyValues["Research and development expenses"].ToString();
                model.Depreciation = keyValues["Depreciation"].ToString();
                model.Amortization = keyValues["Amortization"].ToString();
                model.Restructuring = keyValues["Restructuring"].ToString();
                model.OtherIncomeOrDeductions = keyValues["Other (income)/deductions"].ToString();
                model.EBIT = keyValues["EBIT"].ToString();
                model.InterestIncome = keyValues["Interest income"].ToString();
                model.InterestExpense = keyValues["Interest expense"].ToString();
                model.EBT = keyValues["EBT"].ToString();
                model.Tax = keyValues["Tax"].ToString();
                model.NetIncomeFromContinuingOperations = keyValues["Net income from continuing operations"].ToString();
                model.DiscontinuedOperations = keyValues["Discontinued operations:"].ToString();
                model.IncomeLossFromDiscontinuedOperations = keyValues["Income/Loss from discontinued operations"].ToString();
                model.GainOnDisposalOfDiscontinuedOperations = keyValues["Gain on disposal of discontinued operations"].ToString();
                model.DiscontinuedOperationsNetOfTax = keyValues["Discontinued operations––net of tax"].ToString();
                model.NetIncomeBeforeAllocationToNoncontrollingInterests = keyValues["Net income before allocation to noncontrolling interests"].ToString();
                model.NonControllingInterests = keyValues["Non-controlling interests"].ToString();
                model.NetIncome = keyValues["Net income"].ToString();
                model.BasicEPS_ = keyValues["Basic EPS:"].ToString();
                model.BasicEPSFromContinuingOperations = keyValues["Basic EPS from continuing operations"].ToString();
                model.BasicEPSFromDiscontinuedOperations = keyValues["Basic EPS from discontinued operations"].ToString();
                model.BasicEPS = keyValues["Basic EPS"].ToString();
                model.DilutedEPS_ = keyValues["Diluted EPS:"].ToString();
                model.DilutedEPSFromContinuingOperations = keyValues["Diluted EPS from continuing operations"].ToString();
                model.DilutedEPSFromDiscontinuedOperations = keyValues["Diluted EPS from discontinued operations"].ToString();
                model.DilutedEPS = keyValues["Diluted EPS"].ToString();
                model.WeightedAverageSharesOutstandingBasic = keyValues["Weighted Average Shares Outstanding - Basic"].ToString();
                model.WeightedAverageSharesOutstandingDiluted = keyValues["Weighted Average Shares Outstanding - Diluted"].ToString();
                model.Assets_ = keyValues["Assets:"].ToString();
                model.CashAndCashEquivalents = keyValues["Cash and cash equivalents"].ToString();
                model.ShortTermInvestments = keyValues["Short-term investments"].ToString();
                model.AccountsReceivables = keyValues["Accounts Receivables"].ToString();
                model.Inventories = keyValues["Inventories"].ToString();
                model.CurrentTaxAssets = keyValues["Current tax assets"].ToString();
                model.OtherCurrentAssets = keyValues["Other current assets"].ToString();
                model.AssetsHeldForSale = keyValues["Assets held for sale"].ToString();
                model.TotalCurrentAssets = keyValues["Total current assets"].ToString();
                model.LongTermInvestments = keyValues["Long-term investments"].ToString();
                model.PropertyPlantAndEquipment = keyValues["Property, plant and equipment"].ToString();
                model.IntangibleAssets = keyValues["Intangible assets"].ToString();
                model.Goodwill = keyValues["Goodwill"].ToString();
                model.DeferredTaxAssets = keyValues["Deferred tax assets"].ToString();
                model.OtherNoncurrentAssets = keyValues["Other noncurrent assets"].ToString();
                model.TotalAssets = keyValues["Total assets"].ToString();
                model.LiabilitiesAndEquity_ = keyValues["Liabilities and Equity:"].ToString();
                model.ShortTermDebt = keyValues["Short-term Debt"].ToString();
                model.TradeAccountsPayable = keyValues["Trade accounts payable"].ToString();
                model.DividendsPayable = keyValues["Dividends payable"].ToString();
                model.IncomeTaxesPayable = keyValues["Income taxes payable"].ToString();
                model.AccruedExpenses = keyValues["Accrued expenses"].ToString();
                model.OtherCurrentLiabilities = keyValues["Other current liabilities"].ToString();
                model.LiabilitiesHeldForSale = keyValues["Liabilities held for sale"].ToString();
                model.TotalCurrentLiabilities = keyValues["Total current liabilities"].ToString();
                model.LongTermDebt = keyValues["Long-term debt"].ToString();
                model.PensionBenefitObligations = keyValues["Pension benefit obligations"].ToString();
                model.PostretirementBenefitObligations = keyValues["Postretirement benefit obligations"].ToString();
                model.DeferredTaxLiabilities = keyValues["Deferred tax liabilities"].ToString();
                model.OtherTaxesPayable = keyValues["Other taxes payable"].ToString();
                model.OtherNoncurrentLliabilities = keyValues["Other noncurrent liabilities"].ToString();
                model.TotalLiabilities = keyValues["Total liabilities"].ToString();
                model.CommitmentsAndContingencies_ = keyValues["Commitments and Contingencies:"].ToString();
                model.PreferredStock = keyValues["Preferred stock"].ToString();
                model.CommonStock = keyValues["Common stock"].ToString();
                model.Debentures = keyValues["Debentures"].ToString();
                model.AdditionalPaidInCapital = keyValues["Additional paid-in capital"].ToString();
                model.TresuryStock = keyValues["Tresury stock"].ToString();
                model.RetainedEarnings = keyValues["Retained earnings"].ToString();
                model.AccumulatedOtherComprehensiveLoss = keyValues["Accumulated other comprehensive loss"].ToString();
                model.TotalShareholdersEquity = keyValues["Total shareholders equity"].ToString();
                model.MinorityInterest = keyValues["Minority Interest"].ToString();
                model.TotalEquity = keyValues["Total equity"].ToString();
                model.TotalLiabilitiesAndEquity = keyValues["Total liabilities and equity"].ToString();
                model.OperatingActivities_ = keyValues["Operating Activities:"].ToString();
                model.AdjustmentsToOperatingActivities_ = keyValues["Adjustments to operating activities:"].ToString();
                model.DepreciationAndAmortization = keyValues["Depreciation and amortization"].ToString();
                model.Impairments = keyValues["Impairments"].ToString();
                model.LossOnSaleOfAssets = keyValues["Loss on sale of assets"].ToString();
                model.OtherLossesOrGains = keyValues["Other losses/(gains)"].ToString();
                model.DeferredTaxes = keyValues["Deferred taxes"].ToString();
                model.ShareBasedCompensationExpense = keyValues["Share-based compensation expense"].ToString();
                model.BenefitPlanContributions = keyValues["Benefit plan contributions"].ToString();
                model.OtherAdjustmentsNet = keyValues["Other adjustments, net"].ToString();
                model.OtherChangesInAssetsAndLiabilitiesNetOfAcquisitionsAndDistitures_ = keyValues["Other changes in assets and liabilities, net of acquisitions and divestitures:"].ToString();
                model.AccountsReceivable = keyValues["Accounts receivable"].ToString();
                model.OtherAssets = keyValues["Other assets"].ToString();
                model.AccountsPayable = keyValues["Accounts Payable"].ToString();
                model.OtherLiabilities = keyValues["Other liabilities"].ToString();
                model.OtherTaxAccountsNet = keyValues["Other tax accounts, net"].ToString();
                model.NetCashProvidedByOrUsedInOperatingActivities = keyValues["Net cash provided by/(used in) Operating activities"].ToString();
                model.InvestingActivities_ = keyValues["Investing Activities:"].ToString();
                model.PurchasesOfPropertyPlantAndEquipment = keyValues["Purchases of property, plant and equipment"].ToString();
                model.PurchasesOfShortTermInvestments = keyValues["Purchases of short-term investments"].ToString();
                model.SaleOfShortTermInvestments = keyValues["Sale of short-term investments"].ToString();
                model.RedemptionsOfShortTermInvestments = keyValues["Redemptions of short-term investments"].ToString();
                model.PurchasesOfLongTermInvestments = keyValues["Purchases of long-term investments"].ToString();
                model.SaleOfLongTermInvestments = keyValues["Sale of long-term investments"].ToString();
                model.BusinessAcquisitions = keyValues["Business acquisitions"].ToString();
                model.AcquisitionsOfIntangibleAssets = keyValues["Acquisitions of intangible assets"].ToString();
                model.OtherInvestingActivitiesNet = keyValues["Other investing activities, net"].ToString();
                model.NetCashProvidedByOrUsedInInvestingActivities = keyValues["Net cash provided by/(used in) investing activities"].ToString();
                model.FinancingActivities_ = keyValues["Financing Activities:"].ToString();
                model.ProceedsFromShortTermDebt = keyValues["Proceeds from short-term debt"].ToString();
                model.PrincipalPaymentsOnShortTermDebt = keyValues["Principal payments on short-term debt"].ToString();
                model.NetPaymentsonOrProceedsFromShortTermBorrowingsWithOrigilMaturitiesOfThreeMonthsOrLess = keyValues["Net (payments on)/proceeds from short-term borrowings with original maturities of three months or less"].ToString();
                model.IssuanceOfLongTermDebt = keyValues["Issuance of long-term debt"].ToString();
                model.PrincipalPaymentsOnLongTermDebt = keyValues["Principal payments on long-term debt"].ToString();
                model.PurchasesOfCommonStock = keyValues["Purchases of common stock"].ToString();
                model.CashDividendsPaid = keyValues["Cash dividends paid"].ToString();
                model.ProceedsFromExerciseOfStockOptions = keyValues["Proceeds from exercise of stock options"].ToString();
                model.OtherFinancingActivitiesNet = keyValues["Other financing activities, net"].ToString();
                model.NetCashProvidedByOrUsedInFinancingActivities = keyValues["Net cash provided by/(used in) financing activities"].ToString();
                model.ForeignExchangeImpact = keyValues["Foreign exchange impact"].ToString();
                model.DecreaseInCashAndCashEquivalents = keyValues["Decrease in cash and cash equivalents"].ToString();
                model.OpeningBalanceOfCashAndCashEquivalents = keyValues["Opening balance of Cash and cash equivalents"].ToString();
                model.ClosingBalanceOfCashAndCashEquivalents = keyValues["Closing balance of Cash and cash equivalents"].ToString();
                model.CashPaidReceivedDuringThePeriodFor_ = keyValues["Cash paid (received) during the period for:"].ToString();
                model.IncomeTaxes = keyValues["Income taxes"].ToString();
                model.InterestRateHedges = keyValues["Interest rate hedges"].ToString();
                //model.Segment1 = keyValues["Segment - 1"].ToString();
                //model.Segment2 = keyValues["Segment - 2"].ToString();
                //model.Segment3 = keyValues["Segment - 3"].ToString();
                //model.Segment4 = keyValues["Segment - 4"].ToString();
                //model.Segment5 = keyValues["Segment - 5"].ToString();
                //model.TotalReportableSegments = keyValues["Total Reportable Segments:"].ToString();
                //model.Geography1 = keyValues["Geography - 1"].ToString();
                //model.Geography2 = keyValues["Geography - 2"].ToString();
                //model.Geography3 = keyValues["Geography - 3"].ToString();
                //model.Geography4 = keyValues["Geography - 4"].ToString();
                //model.Geography5 = keyValues["Geography - 5"].ToString();
                //model.TotalGeographicSegments = keyValues["Total Geographic Segments:"].ToString();

                model.GrossProfit = keyValues["Gross Profit"].ToString();
                model.TotalOperatingExpenses = keyValues["Total operating expenses"].ToString();
                model.EBITDA = keyValues["EBITDA"].ToString();
                model.DeferredRevenuesShortTerm = keyValues["Deferred revenues Short term"].ToString();
                model.DeferredRevenuesLongTerm = keyValues["Deferred revenues Long term"].ToString();
                model.CommonStockRepurchased = keyValues["Common stock repurchased"].ToString();
                model.CapitalExpenditure = keyValues["Capital expenditure"].ToString();

                list.Add(model);
            }            
            return list;
        }
    }
}
