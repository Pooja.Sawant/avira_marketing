﻿  
  
CREATE PROCEDURE [dbo].[SApp_GetProjectsByIndustryandMarket]
(
	@IdList dbo.[udtUniqueIdentifier] readonly
)  
AS  
/*================================================================================  
Procedure Name: [SApp_GetProjectsByIndustryandMarket]  
Author: Sai Krishna  
Create date: 13/05/2019  
Description: Get list of Projects by IndustryId and Market from table   
Change History  
Date        Developer     Reason  
__________  ____________  ______________________________________________________  
13/05/2019  Sai krishna   Initial Version  
================================================================================*/  
  
BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

SELECT prj.Id as ProjectId,
	   prj.ProjectName	
FROM Project prj

WHERE prj.IsDeleted = 0
AND EXISTS (SELECT 1 FROM ProjectSegmentMapping psm
			INNER JOIN @IdList lst
			ON psm.SegmentID = lst.ID 
			WHERE psm.IsDeleted = 0 
			and prj.ID = psm.ProjectID)
ORDER BY ProjectName 

END