﻿  
CREATE PROCEDURE [dbo].[SFab_GetCompanyFundamentalsByCompanyId]        
 (  
 @CompanyID UniqueIdentifier =null,    
 @ProjectId UniqueIdentifier =null,    
 @includeDeleted bit = 0,  
 @AvirauserId UniqueIdentifier = null       
 )        
 AS        
/*================================================================================        
Procedure Name: SFab_GetCompanyFundamentalsByCompanyId        
Author: Harshal        
Create date: 06/24/2019        
Description: Get Details from Comapny by ID        
Change History        
Date  Developer  Reason        
__________ ____________ ______________________________________________________        
06/24/2019 Harshal   Initial Version    
07/08/2019 Praveen   Added ProjectId and AvirauserId parameters  
================================================================================*/        
        
BEGIN        
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED        
if @includeDeleted is null set @includeDeleted = 0;    
declare @ProjectApprovedStatusId uniqueidentifier
select @ProjectApprovedStatusId = ID from ProjectStatus
where ProjectStatusName = 'Completed';
    
SELECT Company.Id As CompanyId,
RankNumber,
CompanyName,  
--STUFF((SELECT ','+ CompanyH.CompanyLocation  FROM Company CompanyH  
--      WHERE CompanyH.Id=Company.Id and Company.IsHeadQuarters=1  
--   FOR XML PATH('')), 1, 1, '') AS CompanyHeadQuarters, 
case when  Company.IsHeadQuarters=1 then Company.CompanyLocation else '' end AS CompanyHeadQuarters, 
Nature.NatureTypeName,
Website,
YearOfEstb,
CompanyStage.CompanyStageName,
NoOfEmployees,  
 STUFF(
      COALESCE(', ' + RTRIM(Email), '') 
    + COALESCE(', ' + RTRIM(Email2), '') 
    + COALESCE(', ' + RTRIM(Email3),  '')
    , 1, 2, '') as MailId,
STUFF(
      COALESCE(', ' + RTRIM(Telephone),  '') 
    + COALESCE(', ' + RTRIM(Telephone2), '') 
    + COALESCE(', ' + RTRIM(Telephone3),  '')
    , 1, 2, '') as PhoneNo,
STUFF((SELECT ', '+ CompanyP.CompanyName  FROM Company CompanyP  
      WHERE CompanyP.ParentId=Company.Id  ORDER BY CompanyP.CompanyName   
   FOR XML PATH('')), 1, 1, '') AS CompanySubsidiaryName,  
STUFF((SELECT ', '+ Region.RegionName  FROM CompanyRegionMap  
   INNER JOIN Region ON Region.Id=CompanyRegionMap.RegionId  
      WHERE CompanyRegionMap.CompanyId=Company.Id  
   FOR XML PATH('')), 1, 1, '') AS GeographicPresence,  
STUFF(( SELECT ', '+ CompanyEcosystemPresenceKeyCompetitersMap.KeyCompetiters 
		FROM CompanyEcosystemPresenceKeyCompetitersMap  
		WHERE CompanyEcosystemPresenceKeyCompetitersMap.CompanyId=Company.Id  
		FOR XML PATH('')), 1, 1, '') AS KeyCompetiters  
FROM Company  
--LEFT JOIN CompanyProjectMap ON Company.Id = CompanyProjectMap.CompanyId  
INNER JOIN Nature ON Nature.id=Company.NatureId  
INNER JOIN CompanyStage ON CompanyStage.id= Company.CompanyStageId  
Where Company.Id=@CompanyID   
and (@includeDeleted = 1 or Company.IsDeleted = 0)
and exists (select 1 from CompanyProjectMap cpmap
			inner join project proj
			on cpmap.ProjectId = proj.ID
			where cpmap.CompanyId = Company.Id
			and proj.ProjectStatusID = @ProjectApprovedStatusId
			and cpmap.ProjectId = @ProjectId)
 END