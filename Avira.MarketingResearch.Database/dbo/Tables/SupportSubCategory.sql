﻿CREATE TABLE [dbo].[SupportSubCategory] (
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [SubCategoryName]   NVARCHAR (256)   CONSTRAINT [DF_SupportSubCategory_CategoryName] DEFAULT ('') NOT NULL,
    [SupportCategoryId] UNIQUEIDENTIFIER NULL,
    [Description]       NVARCHAR (256)   NULL,
    [CreatedOn]         DATETIME2 (7)    NULL,
    [UserCreatedById]   UNIQUEIDENTIFIER NULL,
    [ModifiedOn]        DATETIME2 (7)    NULL,
    [UserModifiedById]  UNIQUEIDENTIFIER NULL,
    [IsDeleted]         BIT              CONSTRAINT [DF_SupportSubCategory_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]         DATETIME2 (7)    NULL,
    [UserDeletedById]   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_SupportSubCategory] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_SupportSubCategory_SupportCategory] FOREIGN KEY ([SupportCategoryId]) REFERENCES [dbo].[SupportCategory] ([Id])
);

