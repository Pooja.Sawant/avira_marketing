﻿CREATE TYPE [dbo].[udtGUIDMultiValue] AS TABLE (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [IntValue]     INT              NULL,
    [DecimalValue] DECIMAL (19, 4)  NULL,
    [VarcharValue] NVARCHAR (512)   NULL);

