﻿
CREATE procedure [dbo].[SApp_insertCompany]    
( 
@CompanyCode nvarchar(256),
@CompanyName  nvarchar(256),    
@Description nvarchar(256),
@Telephone nvarchar(20),
@Telephone2 nvarchar(20),
@Email nvarchar(100),
@Email2 nvarchar(100),
@Fax nvarchar(20),
@Fax2 nvarchar(20),
@ParentCompanyName nvarchar(256), 
@ProfiledCompanyName nvarchar(256), 
@Website  nvarchar(128), 
@NoOfEmployees int,
@YearOfEstb int,
@ParentCompanyYearOfEstb int,
@UserCreatedById uniqueidentifier,  
@Id uniqueidentifier,  
@CreatedOn Datetime,
@NatureId uniqueidentifier
)    
as    
/*================================================================================    
Procedure Name: SApp_insertCompany    
Author: Harshal    
Create date: 03/26/2019    
Description: Insert a record into Company table    
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
02/26/2019 Harshal   Initial Version    
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    
declare @ErrorMsg NVARCHAR(2048)
  
--Validations    
IF exists(select 1 from [dbo].[Company]     
   where CompanyName = @CompanyName AND IsDeleted = 0)    
begin    
--declare @ErrorMsg NVARCHAR(2048);    
set @ErrorMsg = 'Company with Name "'+ @CompanyName + '" already exists.';    
--THROW 50000,@ErrorMsg,1;    
--return(1)    
SELECT StoredProcedureName ='SApp_insertCompany',Message =@ErrorMsg,Success = 0;  
RETURN;  
end    
--End of Validations    
BEGIN TRY       
       
insert into [dbo].[Company]([Id],  
       -- [CompanyCode],
        [CompanyName],    
        [Description],
		[Telephone],
		[Telephone2],
		[Email],
		[Email2],
		[Fax],
		[Fax2],
		[ParentCompanyName],
		[ProfiledCompanyName],
		[Website],
		[NoOfEmployees],
		[YearOfEstb],
		[ParentCompanyYearOfEstb],    
        [IsDeleted],    
        [UserCreatedById],    
        [CreatedOn],
		[NatureId])    
 values(@Id, 
   --@CompanyCode,    
   @CompanyName,     
   @Description, 
   @Telephone,
   @Telephone2,
   @Email,
   @Email2, 
   @Fax,
   @Fax2,
   @ParentCompanyName,
   @ProfiledCompanyName,
   @Website,
   @NoOfEmployees,
   @YearOfEstb,
   @ParentCompanyYearOfEstb,   
   0,    
   @UserCreatedById,     
   @CreatedOn,
   @NatureId);    
    
  
SELECT StoredProcedureName ='SApp_insertCompany',Message =@ErrorMsg,Success = 1;     
  
END TRY    
BEGIN CATCH    
    -- Execute the error retrieval routine.    
 DECLARE @ErrorNumber int;    
 DECLARE @ErrorSeverity int;    
 DECLARE @ErrorProcedure varchar(100);    
 DECLARE @ErrorLine int;    
 DECLARE @ErrorMessage varchar(500);    
    
  SELECT @ErrorNumber = ERROR_NUMBER(),    
        @ErrorSeverity = ERROR_SEVERITY(),    
        @ErrorProcedure = ERROR_PROCEDURE(),    
        @ErrorLine = ERROR_LINE(),    
        @ErrorMessage = ERROR_MESSAGE()    
    
 insert into dbo.Errorlog(ErrorNumber,    
       ErrorSeverity,    
       ErrorProcedure,    
       ErrorLine,    
       ErrorMessage,    
       ErrorDate)    
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,@CreatedOn);    
  
 set @ErrorMsg = 'Error while creating a new Company, please contact Admin for more details.';    
--THROW 50000,@ErrorMsg,1;      
--return(1)  
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;     
     
END CATCH    
    
    
end