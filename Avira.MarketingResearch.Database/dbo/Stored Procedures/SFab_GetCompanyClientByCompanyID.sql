﻿
CREATE PROCEDURE [dbo].[SFab_GetCompanyClientByCompanyID]             
 @ComapanyID uniqueidentifier = null,      
 @includeDeleted bit = 0,
 @AviraUserID uniqueidentifier = null           
AS      
/*================================================================================  
Procedure Name: [SFab_GetCompanyClientByCompanyID]  
Author: Pooja Sawant
Create date: 07/12/2019  
Description: Get list of clients from table   
Change History  
Date        Developer     Reason  
__________  ____________  ______________________________________________________  
07/12/2019  Pooja Sawant   Initial Version  
================================================================================*/    
BEGIN         
 SET NOCOUNT ON;          
	
	SELECT *
	FROM [dbo].[CompanyClientsMapping]   
		INNER JOIN [Company] [Company] ON [Company].id=[CompanyClientsMapping].[CompanyId]
	WHERE ([CompanyClientsMapping].CompanyId = @ComapanyID)
		AND (@includeDeleted = 0 or [CompanyClientsMapping].IsDeleted = 0) 
		And ClientName <> 'NA'      
END