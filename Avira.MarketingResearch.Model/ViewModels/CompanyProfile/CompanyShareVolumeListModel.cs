﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.CompanyProfile
{
    public class CompanyShareVolumeListModel
    {
        [FileExtensions(Extensions = "jpg,jpeg")]
        public IFormFile File { get; set; }
        public List<CompanyShareVolumeModel> companyShareVolumeModels { get; set; }
        public CompanyNoteViewModel CompanyNoteViewModel { get; set; }
        public Guid CompanyId { get; set; }
        public Guid ProjectId { get; set; }
        public Guid CurrencyId { get; set; }
        public int Period { get; set; }
        public string BaseDate { get; set; }
        public DateTime BaseDateFromString { get; set; }
        public string ImageActualName { get; set; }
        public string ImageDisplayName { get; set; }
        public string ImageURL { get; set; }
        public string ApproverRemark { get; set; }
        public string AuthorRemark { get; set; }
        public Guid ImageId { get; set; }
    }

    public class TimePeriod
    {
        public int Period { get; set; }
        public string Text { get; set; }
    }
}
