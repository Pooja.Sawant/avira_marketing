﻿CREATE TABLE [dbo].[Errorlog](
	[Errorlog] [int] IDENTITY(1,1) NOT NULL,
	[ErrorNumber] [int] NULL,
	[ErrorSeverity] [int] NULL,
	[ErrorProcedure] [varchar](100) NULL,
	[ErrorLine] [int] NULL,
	[ErrorMessage] [varchar](500) NULL,
	[ErrorDate] [datetime] NULL,
 CONSTRAINT [PK_Errorlog] PRIMARY KEY CLUSTERED 
(
	[Errorlog] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO