﻿
CREATE procedure [dbo].[SFab_GetMessageTemplateElement]
(@Type nvarchar(100),
@AviraUserID uniqueidentifier = null,
@TemplateName nvarchar(200))
as
/*================================================================================
Procedure Name: SFab_GetMessageTemplateElement
Author: Pooja Sawant
Create date: 05/13/2019
Description: Get message TemplateElement by template name

Change History
Date		Developer		Reason
__________	____________	______________________________________________________
05/13/2019	Pooja			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT        MessageTemplateElement.ElementType, MessageTemplateElement.Element, MessageTemplateElement.MessageTemplateId, MessageTemplateElement.Id
FROM            MessageTemplate INNER JOIN
                         MessageTemplateElement ON MessageTemplate.Id = MessageTemplateElement.MessageTemplateId
WHERE        (MessageTemplate.Type = @Type) AND (MessageTemplate.TemplateName = @TemplateName) AND (MessageTemplate.IsDeleted = 0)
end