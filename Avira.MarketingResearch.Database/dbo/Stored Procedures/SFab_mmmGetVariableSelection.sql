﻿CREATE PROCEDURE [dbo].[SFab_mmmGetVariableSelection]  
(     
   @ProjectId uniqueidentifier,  
   @UserId uniqueidentifier,  
   @MarketID uniqueidentifier 
  
)  
As     
  
/*================================================================================      
Procedure Name: SFab_mmmGetVariableSelection     
Author: Nagi     
Create date: 10-06-2020  
Description: gets the variable customozations  
__________ ____________ ______________________________________________________      
08-June-20  Praveen     Intial draft version      
================================================================================*/      
BEGIN    
  
  
 SET nocount ON;   
 SET TRANSACTION isolation level READ uncommitted;    
  
 --DECLARE @ProjectId uniqueidentifier ='02586219-6608-4301-B34E-CC776B4FF3F8'
 SELECT VV.Id SelectionId, VV.VariableName, VV.PageLevel, VV.SectionName, VV.IsSelectedVariable FROM (

 SELECT V.Id, V.VariableName, V.PageLevel, V.SectionName, CASE WHEN VS.Id IS NULL THEN 0 ELSE 1 END IsSelectedVariable, SortOrder FROM MMM_Variables V
 LEFT JOIN MMM_VariableSelection VS ON V.Id = VS.SelectionId AND VS.ProjectId = @ProjectId AND VS.MarketId = @MarketID   
 UNION 
 SELECT DISTINCT S.Id SelectionId, S.SegmentName VariableName, 'SegmentCustomSelection' PageLevel, 'BroadVariable' SectionName, CASE WHEN VS.Id IS NULL THEN 0 ELSE 1 END IsSelectedVariable, 0 SortOrder FROM 
 QualitativeSegmentMapping Q 
 INNER JOIN Segment S ON Q.SegmentId = S.Id
 LEFT JOIN MMM_VariableSelection VS ON Q.SegmentId = VS.SelectionId AND VS.MarketId = @MarketID AND VS.SelectionType = 'SegmentCustomSelection'
 WHERE Q.ProjectId = @ProjectId 
 UNION 
 SELECT DISTINCT S.Id SelectionId, S.SegmentName VariableName, 'SubSegmentCustomSelection' PageLevel, 'SegmentVariable' SectionName, CASE WHEN VS.Id IS NULL THEN 0 ELSE 1 END IsSelectedVariable, 0 SortOrder FROM 
 QualitativeSegmentMapping Q 
 INNER JOIN Segment S ON Q.SegmentId = S.Id
 LEFT JOIN MMM_VariableSelection VS ON Q.SegmentId = VS.SelectionId AND VS.MarketId = @MarketID AND VS.SelectionType = 'SubSegmentCustomSelection'
 WHERE Q.ProjectId = @ProjectId 
 UNION 
 SELECT Sub.Id SelectionId, Sub.SubSegmentName VariableName, 'SubSegmentCustomSelection' PageLevel, S.SegmentName SectionName, CASE WHEN VS.Id IS NULL THEN 0 ELSE 1 END IsSelectedVariable, 0 SortOrder FROM 
 QualitativeSegmentMapping Q 
 INNER JOIN SubSegment Sub ON Q.SubSegmentId = Sub.Id
 INNER JOIN Segment S ON Sub.SegmentId = S.Id
 LEFT JOIN MMM_VariableSelection VS ON Q.SubSegmentId = VS.SelectionId AND VS.MarketId = @MarketID
 WHERE Q.ProjectId = @ProjectId  

 ) VV ORDER BY VV.SortOrder
           
    
END