﻿CREATE procedure [dbo].[SApp_GetCompanyNoteCompanyID]    
(@CompanyId uniqueidentifier,    
@CompanyType  NVARCHAR(512))    
as    
/*================================================================================    
Procedure Name: SApp_GetCompanyNoteCompanyID    
Author: Praveen    
Create date: 04/15/2019    
Description: Get Company note for perticular Company type by Company id    
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
03/04/2019 Praveen   Initial Version    
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    
    
    
SELECT TOP 1 Id,    
CompanyId,    
CompanyType,    
AuthorRemark,    
ApproverRemark,    
CompanyStatusID    
FROM [dbo].[CompanyNote]    
WHERE CompanyId = @CompanyId AND CompanyType = @CompanyType    
ORDER BY CASE WHEN ModifiedOn IS NULL THEN CreatedOn ELSE ModifiedOn END  
END