﻿
  
CREATE PROCEDURE [dbo].[SFab_GetAllRegion]  
(@AviraUserID uniqueidentifier = null)  
 AS  
/*================================================================================  
Procedure Name: [SFab_GetAllRegion]  
Author: Pooja Sawant  
Create date: 05/28/2019  
Description: Get list of region from table  
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
05/28/2019 Pooja   Initial Version  
================================================================================*/  
  
BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
	
	select * from Region WHERE IsDeleted = 0 
	order by RegionName asc

END