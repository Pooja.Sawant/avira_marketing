﻿CREATE procedure [dbo].[SApp_GetTrendNoteTrendID]    
(@TrendId uniqueidentifier,    
@TrendType  NVARCHAR(512))    
as    
/*================================================================================    
Procedure Name: SApp_GetTrendNoteTrendID    
Author: Praveen    
Create date: 04/15/2019    
Description: Get trend note for perticular trend type by trend id    
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
03/04/2019 Praveen   Initial Version    
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    
    
    
SELECT TOP 1 TrendNote.Id,    
TrendId,    
TrendType,    
AuthorRemark,    
ApproverRemark,    
TrendStatusID,
TrendStatus.TrendStatusName    
FROM [dbo].[TrendNote] 
INNER JOIN [dbo].[TrendStatus] ON [TrendStatus].Id=TrendNote.TrendStatusID    
WHERE TrendId = @TrendId AND TrendType = @TrendType    
ORDER BY CASE WHEN TrendNote.ModifiedOn IS NULL THEN TrendNote.CreatedOn ELSE TrendNote.ModifiedOn END  
END