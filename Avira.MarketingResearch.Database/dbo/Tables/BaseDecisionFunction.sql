﻿CREATE TABLE [dbo].[BaseDecisionFunction] (
    [BaseDecisionId]     UNIQUEIDENTIFIER NOT NULL,
    [BusinessFunctionId] UNIQUEIDENTIFIER NOT NULL,
    FOREIGN KEY ([BusinessFunctionId]) REFERENCES [dbo].[LookupCategory] ([Id]),
    CONSTRAINT [FK__BaseDecis__BaseD__5AE5F7C6] FOREIGN KEY ([BaseDecisionId]) REFERENCES [dbo].[BaseDecision] ([Id])
);

