﻿CREATE TABLE [dbo].[ImpactDirection] (
    [Id]                  UNIQUEIDENTIFIER NOT NULL,
    [ImpactDirectionName] NVARCHAR (256)   CONSTRAINT [DF_ImpactDirection_ImpactDirectionName] DEFAULT ('') NOT NULL,
    [Description]         NVARCHAR (256)   NULL,
    [CreatedOn]           DATETIME2 (7)    NULL,
    [UserCreatedById]     UNIQUEIDENTIFIER NULL,
    [ModifiedOn]          DATETIME2 (7)    NULL,
    [UserModifiedById]    UNIQUEIDENTIFIER NULL,
    [IsDeleted]           BIT              CONSTRAINT [DF_ImpactDirection_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]           DATETIME2 (7)    NULL,
    [UserDeletedById]     UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_ImpactDirection] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ImpactDirection_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_ImpactDirection_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id])
);


GO

GO

GO

GO

GO

GO

GO