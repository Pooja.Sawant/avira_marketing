﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SegmentController : AviraBaseController
    {
        public readonly ISegmentRepository _iSegmentRepository;
        public readonly ISegmentService _iSegmentService;
        private ILoggerManager _logger;

        public SegmentController(
            ISegmentService iSegmentService,
            ISegmentRepository iSegmentRepository,
            ILoggerManager logger)
        {
            _iSegmentRepository = iSegmentRepository;
            _iSegmentService = iSegmentService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<SegmentViewModel>> Get()
        {
            List<SegmentViewModel> res = null;
            try
            {
                res = _iSegmentService.GetAll().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }
    }
}