﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyProductSegmentMapController : AviraBaseController
    {
        private readonly ICompanyProductSegmentMapService _companyProductSegmentMapService;
        private readonly ICompanyProductSegmentMapRepository _companyProductSegmentMapRepository;
        private ILoggerManager _logger;

        public CompanyProductSegmentMapController(
            ICompanyProductSegmentMapService companyProductSegmentMapService,
            ICompanyProductSegmentMapRepository companyProductSegmentMapRepository,
             ILoggerManager logger)
        {
            _companyProductSegmentMapRepository = companyProductSegmentMapRepository;
            _companyProductSegmentMapService = companyProductSegmentMapService;
            _logger = logger;

        }

        [HttpGet]
        public ActionResult<IEnumerable<CompanyProductSegmentMapViewModel>> Get(Guid? Id)
        {
            List<CompanyProductSegmentMapViewModel> res = null;
            try
            {
                res = _companyProductSegmentMapService.GetAll(Id).ToList();
        }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }


        [HttpPost]
        public ActionResult Post([FromBody] CompanyProductViewMoel viewModel)
        {
            viewModel.CreatedOn = System.DateTime.UtcNow;
            viewModel.UserCreatedById = CurrentUser.Id;

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a adding CompanyProductSegmentMap  , please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                var result = _companyProductSegmentMapService.InsertUpdate(viewModel, CurrentUser);
                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "CompanyProductSegmentMap is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }
    }
}