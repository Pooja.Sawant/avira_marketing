﻿CREATE PROCEDURE [dbo].[SFab_LookupCategoryGet]
(
	@CategoryType nvarchar(100)
)
 AS      
/*================================================================================      
Procedure Name: SFab_LookupCategoryGet      
Author: Nitin      
Create date: 10/25/2019      
Description: Get list from LookupCategory BY Type      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
10/25/2019 Nitin   Initial Version  
03-Feb-2020 Nitin Order by - SeqNo added    
================================================================================*/      
      
BEGIN      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED      

	SELECT Id,
		CategoryType,
		CategoryName,
		Description 
	FROM LookupCategory 
	WHERE IsDeleted  = 0 AND 
		CategoryType = @CategoryType 
	ORDER BY SeqNumber, CategoryName 
      
 END