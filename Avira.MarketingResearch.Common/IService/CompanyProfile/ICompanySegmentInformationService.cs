﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.CompanyProfile
{
    public interface ICompanySegmentInformationService
    {
        IEnumerable<CompanySegmentInformationViewModel> GetCompanySegmentInformationData(Guid CompanyId, AviraUser user);
    }
}
