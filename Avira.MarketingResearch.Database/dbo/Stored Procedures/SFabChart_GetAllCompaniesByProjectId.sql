﻿CREATE PROCEDURE [dbo].[SFabChart_GetAllCompaniesByProjectId]  
(  
 @ProjectId UniqueIdentifier,  
 @Revenue bit = 1,  
 @Rank bit = 0,  
 @AvirauserId UniqueIdentifier = null  
)  
as  
/*================================================================================  
Procedure Name: [SFabChart_GetAllCompaniesByProjectId]  
Author: Sai Krishna  
Create date: 15/05/2019  
Description: Get list from Companies by project Id  
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
15/05/2019 Sai Krishna  Initial Version  
29/05/2019 Praveen      Commented where condition for testing purose  
================================================================================*/  
BEGIN  
SET NOCOUNT ON;  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  

	Declare @USDCurrencyId uniqueidentifier	
	Set @USDCurrencyId = dbo.[fun_GetCurrencyIDForUSD]()

	declare @ProjectApprovedStatusId uniqueidentifier;
	DECLARE @BaseYear INT = 0;
	 select @BaseYear = BaseYear from BaseYearMaster
	select @ProjectApprovedStatusId = ID from ProjectStatus Where ProjectStatusName = 'Completed'
	declare @USDCurrencyCode nvarchar(50)=null	
		select @USDCurrencyCode = CurrencyCode from Currency
		where CurrencyName = 'United States dollar';

	IF(@Revenue = 1)  
	Begin  
		;with cte as (  
		Select Distinct cpmap.CompanyId   
		from CompanyProjectMap cpmap  
		INNER JOIN [dbo].Project ON Project.Id=cpmap.ProjectId
		where cpmap.ProjectId = @ProjectId AND Project.ProjectStatusID=@ProjectApprovedStatusId AND cpmap.IsDeleted=0
		)
  
		, ctr_revenue as (
		Select comp.Id as CompanyId,  
			comp.CompanyName,  
			comp.RankNumber,  
			RANK () OVER( ORDER BY cr.Revenue desc) AS ChartRankActual,  
			ROW_NUMBER() OVER(ORDER BY comp.Id desc) AS ChartColorRank,      
			[dbo].[fun_UnitDeConversion](cr.ValueConversionId,cr.Revenue) as Revenue,
			Format([dbo].[fun_UnitDeConversion](cr.ValueConversionId,cr.Revenue), '##,##0') ChartValue,
			--Format([dbo].[fun_CurrencyConversion]([dbo].[fun_UnitDeConversion](cr.ValueConversionId,cr.Revenue), @USDCurrencyId, CR.CurrencyId,  GetDate()), '##,##0') ChartValue,
			cr.Year,  
			cr.CurrencyId
			,', ' + VC.ValueName AS UnitName
			,@USDCurrencyCode as CurrencyCode
			--,Currency.CurrencyCode
		from Company comp  
		Join cte  
		On comp.Id = cte.CompanyId  
		--INNER Join CompanyProfilesFinancial cr  
		inner join CompanyPLStatement cr On comp.Id = cr.CompanyId 
		LEFT JOIN ValueConversion VC ON cr.ValueConversionId = VC.Id
		LEFT JOIN Currency ON cr.CurrencyId = Currency.Id
 
		Where comp.IsDeleted = 0 and cr.IsDeleted = 0  AND cr.Year = @BaseYear
		--Order By cr.ChartRank Desc 
		)
		SELECT top 10  RANK () OVER( ORDER BY ChartRankActual desc) AS ChartRank,  * FROM ctr_revenue WHERE ChartRankActual <=10 ORDER BY Revenue desc--,ChartRank  
	End  
	Else If(@Rank = 1)  
	Begin  
		;with cteRank as (  
		Select Distinct cpmap.CompanyId   
		from CompanyProjectMap cpmap  
		INNER JOIN [dbo].Project ON Project.Id=cpmap.ProjectId
		where cpmap.ProjectId = @ProjectId AND Project.ProjectStatusID=@ProjectApprovedStatusId AND cpmap.IsDeleted=0
		)  

		, ctr_Growth as (
		Select comp.Id as CompanyId,  
			comp.CompanyName,  
			comp.RankNumber,  
			--RANK () OVER( ORDER BY comp.RankNumber desc) AS ChartRank,  
			RANK () OVER( ORDER BY cr.GROWTH desc) AS ChartRankActual,
			ROW_NUMBER() OVER(ORDER BY comp.Id desc) AS ChartColorRank,  
			(cr.GROWTH*100) as ChartValue,  
			cr.Year
			--,cr.CurrencyId,  
		   --cr.UnitId  
		from Company comp  
		inner Join cteRank  
		On comp.Id = cteRank.CompanyId  
		Join (
		SELECT t1.CompanyId, t1.YEAR, t1.Revenues,    CASE WHEN t2.YEAR IS NOT NULL THEN
				--FORMAT(
				CONVERT(DECIMAL(19, 4), 
					CONVERT(DECIMAL(19, 4), (t1.Revenues - t2.Revenues)) /
				   nullif( CONVERT(DECIMAL(19, 4), t2.Revenues),0)--, 'p')
				   )
			ELSE 0 END AS GROWTH
		FROM
		(
			SELECT CompanyId, YEAR, SUM(Revenue) AS Revenues
			FROM CompanyPLStatement
			WHERE Year = @BaseYear AND IsDeleted = 0 GROUP BY CompanyId, YEAR
		) t1
		LEFT JOIN
		(
			SELECT CompanyId, YEAR AS YEAR, SUM(Revenue) AS Revenues
			FROM CompanyPLStatement
			WHERE Year = @BaseYear -1 AND IsDeleted = 0 GROUP BY CompanyId, YEAR
		) t2
		ON t1.CompanyId = t2.CompanyId AND t2.YEAR = t1.YEAR - 1
		)cr  
		On comp.Id = cr.CompanyId
		Where comp.IsDeleted = 0  
		--Order By comp.RankNumber 
		)
		SELECT top 10  RANK () OVER( ORDER BY ChartRankActual desc) AS ChartRank,  * FROM ctr_Growth WHERE ChartRankActual <=10 ORDER BY ChartValue desc--,ChartRank 
 
	End  
END