﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard
{
    public class ProjectAnalistDetailsViewModel
    {
        public Guid Id { get; set; }
        public Guid ProjectID { get; set; }
        public string AnalysisType { get; set; }
        public Guid AuthorUserID { get; set; }
        public string AuthorUserName { get; set; }
        public List<Guid> CoAuthorUserID { get; set; }
        public Guid ApproverUserId { get; set; }
        public string ApproverUserName { get; set; }
        public List<Guid> CoApproverUserID { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "ProjectAnalystDetails";
            }
        }
    }
    public class ProjectAnalistDetailsInsertDbViewModel
    {
        [Required]
        [Display(Name = "Analysis Name")]
        public string AnalysisType { get; set; }
        public Guid AuthorUserID { get; set; }
        public Guid ProjectID { get; set; }
        public List<Guid> CoAuthorUserID { get; set; }
        public Guid ApproverUserId { get; set; }
        public List<Guid> CoApproverUserID { get; set; }     
        public Guid UserCreatedById { get; set; }  
        public Guid Id { get; set; }
        public string AnalystType { get; set; }
        public Guid[] UserIDs { get; set; }

    }

    public class ProjectAnalistDetailsUpdateDbViewModel
    {
        [Required]
        [Display(Name = "Analysis Name")]
        public string AnalysisType { get; set; }      
        public Guid ProjectID { get; set; }       
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; } = false;
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        [Required]
        public Guid Id { get; set; }
        public Guid[] UserIDs { get; set; }
        public string AnalystType { get; set; }
        public Guid[] AuthorUserIDs { get; set; }
        public Guid[] CoAuthorUserIDs { get; set; }
        public Guid[] ApproverUserIDs { get; set; }
        public Guid[] CoApproverUserIDs { get; set; }
        public IEnumerable<SelectListItem> Values { get; set; }
        public List<string> CoApproverUserID { get; set; }
    }
}
