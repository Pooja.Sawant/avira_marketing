﻿using Avira.MarketingResearch.Model;
using Avira.MarketingResearch.Repository.Helpers;
using System.Collections.Generic;
using System;
using System.Data;
using Avira.MarketingResearch.Model.ViewModels;
using static Avira.MarketingResearch.Model.ViewModels.ComponentViewModel;

namespace Avira.MarketingResearch.Repository.CoreInterfaces
{
    public interface IComponentRepository
    {
        ComponentViewModel GetById(Guid guid);
        IEnumerable<ComponentViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted);
        Result<SpTransactionMessage> Create(ComponentInsertDbViewModel viewModel);
        Result<SpTransactionMessage> Update(ComponentUpdateDbViewModel viewModel);
    }
}
