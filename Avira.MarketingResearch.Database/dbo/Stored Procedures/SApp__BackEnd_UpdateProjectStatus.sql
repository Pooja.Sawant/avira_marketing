﻿CREATE procedure [dbo].[SApp__BackEnd_UpdateProjectStatus]      
(
@ProjectName nvarchar(512),
@ProjectStatusName nvarchar(512)    
)  
as      
/*================================================================================      
Procedure Name: [SApp__BackEnd_UpdateProjectStatus]     
Author: Praveen      
Create date: 09/21/2019      
Description: update a record in Project table by ID to update project status 
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
09/21/2019 Praveen   Initial Version     
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
         
declare @ErrorMsg NVARCHAR(2048), @ProjectStatusID uniqueidentifier, @ProjectID uniqueidentifier;  
BEGIN TRY       
--update record  

IF NOT EXISTS(SELECT 1 FROM ProjectStatus WHERE ProjectStatusName = @ProjectStatusName)
BEGIN	
	SELECT 'Invalid project status name' as ErrorMessage
END
ELSE IF NOT EXISTS(SELECT 1 FROM project WHERE ProjectName = @ProjectName AND IsDeleted = 0)
BEGIN	
	SELECT @ProjectName + ' is invalid project.' AS ErrorMessage	
END
ELSE	
BEGIN


set @ProjectStatusID=(select Id from ProjectStatus
where ProjectStatusName = @ProjectStatusName)
select @ProjectID = ID from project where ProjectName = @ProjectName
update [dbo].[Project]      
set [ProjectStatusID] = @ProjectStatusID   
  where ID = @ProjectID 
     
  SELECT 'The project status successfully updated.' AS ErrorMessage	
END

    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);      
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());   
  
set @ErrorMsg = 'Error while updating project, please contact Admin for more details.'        
   
 SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
END CATCH      
      
    
    
      
end