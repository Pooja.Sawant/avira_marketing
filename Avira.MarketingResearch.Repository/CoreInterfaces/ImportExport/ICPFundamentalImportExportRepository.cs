﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport
{
    public interface ICPFundamentalImportExportRepository
    {
        IEnumerable<ImportCommonResponseModel> Create(CompanyFundamentalsViewModel viewModel);
    }
}
