﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public class QualitativeAddTableQueries
    {
        public static string StoreProcInsert = "SApp_insertQualitativeAddTable";
        public static string StoreProcUpdate = "SApp_UpdateQualitativeAddTable";
        public static string StoreProcGetQualitativeTable = "SApp_GetQualitativeAddTableById"; 
    }
}
