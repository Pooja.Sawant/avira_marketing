﻿
  
CREATE procedure [dbo].[SFab_ValidateCustomerUserEmail]  
(

@Email nvarchar(100)=null,
@AviraUserID uniqueidentifier = null
 

)  
as  
/*================================================================================  
Procedure Name: SFab_ValidateCustomerUserEmail  
Author: Pooja Sawant  
Create date: 05/09/2019  
Description: check email from user table  
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
05/09/2019 Pooja   Initial Version  
================================================================================*/  
BEGIN  
SET NOCOUNT ON;  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
declare @ErrorMsg NVARCHAR(2048);  

--Validations  
IF exists(select 1 from [dbo].AviraUser   
   where (EmailAddress = @Email or UserName=@Email) AND  IsDeleted = 0)  
begin  
set @ErrorMsg = 'User with Email "'+ @Email + '" already exists.';  
SELECT StoredProcedureName ='SFab_ValidateCustomerUserEmail',Message =@ErrorMsg,Success = 0;    
RETURN;    
end    
  
SELECT StoredProcedureName ='SFab_ValidateCustomerUserEmail',Message = @ErrorMsg,Success = 1; 
 
end