﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.Helpers;
using Avira.MarketingResearch.Web.Utils;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;

namespace Avira.MarketingResearch.Web.Controllers
{
    [Authorization]
    public class CompanyShareVolumeController : BaseController
    {
        public readonly IServiceRepository _serviceRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;
        string contentRootPath = string.Empty;
        private IAPIHelper _iAPIHelper;

        private ILoggerManager _logger;

        public CompanyShareVolumeController(IAPIHelper iAPIHelper, IServiceRepository serviceRepository, ILoggerManager logger, IHostingEnvironment hostingEnvironment, IConfiguration configuration)
        {
            _hostingEnvironment = hostingEnvironment;
            _serviceRepository = serviceRepository;
            _logger = logger;
            _configuration = configuration;
            contentRootPath = _hostingEnvironment.ContentRootPath;
            _iAPIHelper = iAPIHelper;
        }

        public IActionResult Index(string CompanyId, string message, string ProjectId)
        {
            CompanyShareVolumeListModel model = new CompanyShareVolumeListModel();
            try
            {
                var shareVolContoller = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage currenciesResponse = _serviceRepository.GetAllParentResponse(shareVolContoller);
                currenciesResponse.EnsureSuccessStatusCode();
                List<CurrencyViewModel> currencyModel = currenciesResponse.Content.ReadAsAsync<List<CurrencyViewModel>>().Result;
                ViewBag.CurrencyViewModel = new SelectList(currencyModel.OrderBy(e => e.CurrencyNameWithCode), "Id", "CurrencyNameWithCode");

                HttpResponseMessage CompanyNoteResponse = _serviceRepository.GetByIdResponse("CompanyNote", CompanyId, "CompanyShareVolume");
                CompanyNoteViewModel companyNoteViewModel = CompanyNoteResponse.Content.ReadAsAsync<CompanyNoteViewModel>().Result;
                model.CompanyNoteViewModel = companyNoteViewModel ?? new CompanyNoteViewModel();

                model.ApproverRemark = model.CompanyNoteViewModel.ApproverRemark;
                model.AuthorRemark = model.CompanyNoteViewModel.AuthorRemark;

                List<TimePeriod> Period = new List<TimePeriod>();
                for (int i = 1; i <= 5; i++)
                {
                    TimePeriod p = new TimePeriod();
                    p.Text = i.ToString();
                    p.Period = i;
                    Period.Add(p);
                }
                ViewBag.PeriodList = new SelectList(Period, "Period", "Text");

                if (!string.IsNullOrEmpty(CompanyId) && CompanyId != "null")
                {
                    HttpResponseMessage shareVolResponse = _serviceRepository.GetShareVolByIdResponse(shareVolContoller, CompanyId);
                    shareVolResponse.EnsureSuccessStatusCode();
                    MainCompanyShareVolModel csvm = shareVolResponse.Content.ReadAsAsync<MainCompanyShareVolModel>().Result;

                    if (csvm != null)
                    {
                        model.companyShareVolumeModels = csvm.CompanyShareVolumeDataModel.OrderByDescending(e => e.Date).ToList();
                        model.CompanyId = new Guid(CompanyId);
                        if (csvm.CompanyShareVolumeDataModel != null && csvm.CompanyShareVolumeDataModel.Count > 0)
                        {
                            model.CurrencyId = csvm.CompanyShareVolumeDataModel.FirstOrDefault().CurrencyId;
                            model.Period = csvm.CompanyShareVolumeDataModel.FirstOrDefault().Period;
                            model.BaseDate = csvm.CompanyShareVolumeDataModel.FirstOrDefault().BaseDate.ToString();
                            model.BaseDateFromString = Convert.ToDateTime(model.BaseDate);
                        }
                        if (csvm.CompanyShareImageModel != null)
                        {
                            model.ImageActualName = csvm.CompanyShareImageModel.ImageActualName;
                            model.ImageDisplayName = csvm.CompanyShareImageModel.ImageDisplayName;
                            model.ImageURL = csvm.CompanyShareImageModel.ImageURL;
                            model.ImageId = csvm.CompanyShareImageModel.Id;
                        }
                        var baseURl = _iAPIHelper.GetBaseURL();
                        if (baseURl != null)
                            ViewBag.ApiBaseURL = baseURl;

                    }
                    else
                    {
                        model.CompanyId = new Guid(CompanyId);
                    }
                }

                HttpResponseMessage CompanyResponse = _serviceRepository.GetByIdResponse("Company", CompanyId);
                CompanyViewModel companyViewModel = CompanyResponse.Content.ReadAsAsync<CompanyViewModel>().Result;
                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, new Guid(ProjectId));
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    model.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.CompanyName = companyViewModel.CompanyName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                    ViewBag.StatusName = model.CompanyNoteViewModel.StatusName;
                }

                ViewBag.CompanyId = model.CompanyId;
                ViewBag.Message = message;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult PostData(CompanyShareVolumeListModel mainModel, IFormCollection collection, string companyId, string actionName)
        {
            if (!string.IsNullOrEmpty(actionName))
            {
                try
                {
                    if (mainModel.companyShareVolumeModels != null)
                        mainModel.companyShareVolumeModels.Clear();

                    string date = collection["DateYear"];
                    string[] dateArray = date.Split(',');
                    List<string> dateList = dateArray.OfType<string>().ToList();

                    string price = collection["Price"];
                    string[] priceArray = price.Split(',');

                    List<string> priceList = priceArray.OfType<string>().ToList();

                    string volume = collection["Volume"];
                    string[] volumeArray = volume.Split(',');
                    List<string> volumeList = volumeArray.OfType<string>().ToList();

                    List<string[]> arrays = new List<string[]>();
                    for (int i = 0; i < dateArray.Length; i++)
                    {
                        for (int j = i; j <= i; j++)
                        {
                            for (int k = j; k <= j; k++)
                            {
                                var dates = dateArray[i];
                                var prices = priceArray[j];
                                var volumes = volumeArray[k];
                                arrays.Add(new string[] { collection["companyShareVolumeModels[" + i + "].Id"], dates, prices, volumes });
                            }
                        }
                    }

                    var result = arrays.ToList();
                    List<CompanyShareVolumeModel> modelList = new List<CompanyShareVolumeModel>();

                    for (int i = 0; i < result.Count; i++)
                    {
                        var data = result[i];
                        CompanyShareVolumeModel csvModel = new CompanyShareVolumeModel();
                        if (data[2] != "" && data[3] != "" && data[2] != null && data[3] != null)
                        {
                            var guid = data[0];
                            if (guid != null)
                                csvModel.Id = new Guid(data[0]);
                            //else
                            //    csvModel.Id = Guid.NewGuid();
                            csvModel.Date = Convert.ToInt32(data[1]);
                            csvModel.Price = Convert.ToDecimal(data[2]);
                            csvModel.Volume = Convert.ToDecimal(data[3]);
                            modelList.Add(csvModel);
                        }
                        else
                        {
                            ViewBag.Message = "Volume & Price should not be empty";
                            return RedirectToAction("Index", new { CompanyId = mainModel.CompanyId.ToString(), message = ViewBag.Message, ProjectId = mainModel.ProjectId });
                        }
                    }
                    mainModel.companyShareVolumeModels = modelList;
                    mainModel.CompanyNoteViewModel.CompanyType = "CompanyShareVolume";
                    mainModel.File = null;

                    var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();

                    if (actionName.Equals("Save"))
                    {
                        if (mainModel.CompanyNoteViewModel.AuthorRemark == null)
                        {
                            ViewBag.Message = "Please select Author Notes!!";
                            return RedirectToAction("Index", new { CompanyId = mainModel.CompanyId.ToString(), message = ViewBag.Message, ProjectId = mainModel.ProjectId });
                        }
                        else
                        {
                            mainModel.CompanyNoteViewModel.CompanyStatusName = "Draft";
                            HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, mainModel);
                            response.EnsureSuccessStatusCode();
                            ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                            if (viewModel != null)
                            {
                                ViewBag.Message = viewModel.Message;
                                return RedirectToAction("Index", new { CompanyId = mainModel.CompanyId.ToString(), message = ViewBag.Message, ProjectId = mainModel.ProjectId });
                            }
                        }
                    }
                    else if (actionName.Equals("Submitted"))
                    {
                        if (mainModel.CompanyNoteViewModel.AuthorRemark == null)
                        {
                            ViewBag.Message = "Please select Author Notes!!";
                            return RedirectToAction("Index", new { CompanyId = mainModel.CompanyId.ToString(), message = ViewBag.Message, ProjectId = mainModel.ProjectId });
                        }
                        else
                        {
                            mainModel.CompanyNoteViewModel.CompanyStatusName = "Submitted";
                            HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, mainModel);
                            response.EnsureSuccessStatusCode();
                            ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                            if (viewModel != null)
                            {
                                ViewBag.Message = viewModel.Message;
                                return RedirectToAction("Index", new { CompanyId = mainModel.CompanyId.ToString(), message = ViewBag.Message, ProjectId = mainModel.ProjectId });
                            }
                        }
                    }
                    else if (actionName.Equals("Reject"))
                    {
                        if (mainModel.CompanyNoteViewModel.AuthorRemark == null)
                        {
                            ViewBag.Message = "Please select Author Notes!!";
                            return RedirectToAction("Index", new { CompanyId = mainModel.CompanyId.ToString(), message = ViewBag.Message, ProjectId = mainModel.ProjectId });
                        }
                        else
                        {
                            mainModel.CompanyNoteViewModel.CompanyStatusName = "Rejected";
                            HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, mainModel);
                            response.EnsureSuccessStatusCode();
                            ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                            if (viewModel != null)
                            {
                                //ViewBag.Message = viewModel.Message;
                                return RedirectToAction("Index", new { CompanyId = mainModel.CompanyId.ToString(), ProjectId = mainModel.ProjectId });
                            }
                        }
                    }
                    else if (actionName.Equals("Approve"))
                    {
                        if (mainModel.CompanyNoteViewModel.AuthorRemark == null)
                        {
                            ViewBag.Message = "Please select Author Notes!!";
                            return RedirectToAction("Index", new { CompanyId = mainModel.CompanyId.ToString(), message = ViewBag.Message, ProjectId = mainModel.ProjectId });
                        }
                        else
                        {
                            mainModel.CompanyNoteViewModel.CompanyStatusName = "Approved";
                            HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, mainModel);
                            response.EnsureSuccessStatusCode();
                            ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                            if (viewModel != null)
                            {
                                //ViewBag.Message = viewModel.Message;
                                return RedirectToAction("Index", new { CompanyId = mainModel.CompanyId.ToString(), ProjectId = mainModel.ProjectId });
                            }
                        }
                    }

                    else if (actionName.Equals("Next"))
                        return RedirectToAction("Index", "CompanyClientAndStrategy", new { CompanyId = mainModel.CompanyId.ToString(), ProjectId = mainModel.ProjectId });
                    else if (actionName.Equals("Previous"))
                        return RedirectToAction("Index", "RevenueProfitAnalysis", new { CompanyId = mainModel.CompanyId.ToString(), ProjectId = mainModel.ProjectId });
                    else if (actionName.Equals("Cancel"))
                        return RedirectToAction("Index", "#", new { CompanyId = mainModel.CompanyId.ToString(), ProjectId = mainModel.ProjectId });


                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                }
            }
            HttpResponseMessage CompanyResponse = _serviceRepository.GetByIdResponse("Company", mainModel.CompanyId.ToString());
            CompanyViewModel companyViewModel = CompanyResponse.Content.ReadAsAsync<CompanyViewModel>().Result;
            ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, mainModel.ProjectId);
            if (project != null)
            {
                ViewBag.ProjectId = project.Id;
                ViewBag.ProjectName = project.ProjectName;
                ViewBag.CompanyName = companyViewModel.CompanyName;
                ViewBag.ProjectCode = project.ProjectCode;
                ViewBag.Approver = project.Approver;
                ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
            }
            return View(mainModel);
        }

        [HttpPost]
        public IActionResult PostImage(CompanyShareVolumeListModel mainModel, IFormCollection collection, string companyId, string projectId, string actionName)
        {
            MainCompanyShareVolModel model = new MainCompanyShareVolModel();
           
            try
            {
                CompanyShareImageModel imageModel = new CompanyShareImageModel();
                imageModel.Id = mainModel.ImageId;
                if (mainModel.File != null)
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        mainModel.File.CopyToAsync(memoryStream);
                        imageModel.fileByte = memoryStream.ToArray();
                    }
                    imageModel.ImageDisplayName = mainModel.File.FileName;
                    imageModel.fileExtension = mainModel.File.FileName.Substring(mainModel.File.FileName.LastIndexOf('.')).ToLower();
                    imageModel.ImageFile = null;

                    imageModel.CompanyId = mainModel.CompanyId;
                    
                }
                else
                {
                    if (string.IsNullOrEmpty(mainModel.ImageDisplayName) && (actionName.Equals("Save") || actionName.Equals("Submitted") || actionName.Equals("Approve") || actionName.Equals("Reject")))
                    {
                        ViewBag.Message = "Please select file.";
                        return RedirectToAction("Index", new { CompanyId = mainModel.CompanyId.ToString(), message = ViewBag.Message, ProjectId = mainModel.ProjectId });
                    }
                    else
                    {
                        imageModel.ImageDisplayName = mainModel.ImageDisplayName;
                        imageModel.ImageActualName = mainModel.ImageActualName;
                        imageModel.ImageURL = mainModel.ImageURL;
                        imageModel.CompanyId = mainModel.CompanyId;
                    }
                }

                model.CompanyShareImageModel = imageModel??new CompanyShareImageModel();
               
                
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();

                    CompanyNoteViewModel noteViewModel = new CompanyNoteViewModel();
                    noteViewModel.ApproverRemark = mainModel.ApproverRemark;
                    noteViewModel.AuthorRemark = mainModel.AuthorRemark;
                    model.CompanyNoteViewModel = noteViewModel;
                    mainModel.CompanyNoteViewModel = noteViewModel ?? new CompanyNoteViewModel();
                    model.CompanyNoteViewModel.CompanyType = "CompanyShareVolume";
                    if (actionName.Equals("Save"))
                    {
                        if (mainModel.AuthorRemark == null)
                        {
                            ViewBag.Message = "Please select Author Notes!!";
                            return RedirectToAction("Index", new { CompanyId = mainModel.CompanyId.ToString(), message = ViewBag.Message, ProjectId = mainModel.ProjectId });
                        }
                        else
                        {
                            model.CompanyNoteViewModel.CompanyStatusName = "Draft";
                            HttpResponseMessage response = _serviceRepository.SaveImageByIdResponse(contollerName, model);
                            response.EnsureSuccessStatusCode();
                            ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                            if (viewModel != null)
                            {
                                ViewBag.Message = viewModel.Message.ToString();
                                return RedirectToAction("Index", new { CompanyId = model.CompanyShareImageModel.CompanyId.ToString(), message = ViewBag.Message, ProjectId = mainModel.ProjectId });
                            }
                        }
                    }
                    else if (actionName.Equals("Submitted"))
                    {
                        if (mainModel.CompanyNoteViewModel.AuthorRemark == null)
                        {
                            ViewBag.Message = "Please select Author Notes!!";
                            return RedirectToAction("Index", new { CompanyId = mainModel.CompanyId.ToString(), message = ViewBag.Message, ProjectId = mainModel.ProjectId });
                        }
                        else
                        {
                            mainModel.CompanyNoteViewModel.CompanyStatusName = "Submitted";
                            HttpResponseMessage response = _serviceRepository.SaveImageByIdResponse(contollerName, model);
                            response.EnsureSuccessStatusCode();
                            ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                            if (viewModel != null)
                            {
                                ViewBag.Message = viewModel.Message.ToString();
                                return RedirectToAction("Index", new { CompanyId = model.CompanyShareImageModel.CompanyId.ToString(), message = ViewBag.Message, ProjectId = mainModel.ProjectId });
                            }
                        }
                    }
                    else if (actionName.Equals("Reject"))
                    {
                        if (mainModel.CompanyNoteViewModel.AuthorRemark == null)
                        {
                            ViewBag.Message = "Please select Author Notes!!";
                            return RedirectToAction("Index", new { CompanyId = mainModel.CompanyId.ToString(), message = ViewBag.Message, ProjectId = mainModel.ProjectId });
                        }
                        else
                        {
                            model.CompanyNoteViewModel.CompanyStatusName = "Rejected";
                            HttpResponseMessage response = _serviceRepository.SaveImageByIdResponse(contollerName, model);
                            response.EnsureSuccessStatusCode();
                            ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                            if (viewModel != null)
                            {
                                ViewBag.Message = viewModel.Message.ToString();
                                return RedirectToAction("Index", new { CompanyId = model.CompanyShareImageModel.CompanyId.ToString(), message = ViewBag.Message, ProjectId = mainModel.ProjectId });
                            }
                        }
                    }
                    else if (actionName.Equals("Approve"))
                    {
                        if (mainModel.CompanyNoteViewModel.AuthorRemark == null)
                        {
                            ViewBag.Message = "Please select Author Notes!!";
                            return RedirectToAction("Index", new { CompanyId = mainModel.CompanyId.ToString(), message = ViewBag.Message, ProjectId = mainModel.ProjectId });
                        }
                        else
                        {
                            model.CompanyNoteViewModel.CompanyStatusName = "Approved";
                            HttpResponseMessage response = _serviceRepository.SaveImageByIdResponse(contollerName, model);
                            response.EnsureSuccessStatusCode();
                            ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                            if (viewModel != null)
                            {
                                ViewBag.Message = viewModel.Message.ToString();
                                return RedirectToAction("Index", new { CompanyId = model.CompanyShareImageModel.CompanyId.ToString(), message = ViewBag.Message, ProjectId = mainModel.ProjectId });
                            }
                        }
                    }


               
                else if (actionName.Equals("Next"))
                    return RedirectToAction("Index", "CompanyClientAndStrategy", new { CompanyId = companyId, ProjectId = projectId });
                else if (actionName.Equals("Previous"))
                    return RedirectToAction("Index", "CompanySegmentInformation", new { CompanyId = companyId, ProjectId = projectId });
                else if (actionName.Equals("Cancel"))
                    return RedirectToAction("Index", "#", new { CompanyId = model.CompanyShareImageModel.CompanyId.ToString(), ProjectId = mainModel.ProjectId });
                //else if (mainModel.File == null)
                //{
                   
                //}

                HttpResponseMessage CompanyNoteResponse = _serviceRepository.GetByIdResponse("CompanyNote", mainModel.CompanyId.ToString(), "CompanyShareVolume");
                CompanyNoteViewModel companyNoteViewModel = CompanyNoteResponse.Content.ReadAsAsync<CompanyNoteViewModel>().Result;
                companyNoteViewModel = companyNoteViewModel ?? new CompanyNoteViewModel();
                HttpResponseMessage CompanyResponse = _serviceRepository.GetByIdResponse("Company", mainModel.CompanyId.ToString());
                CompanyViewModel companyViewModel = CompanyResponse.Content.ReadAsAsync<CompanyViewModel>().Result;
                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, mainModel.ProjectId);
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.CompanyName = companyViewModel.CompanyName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                    ViewBag.StatusName = companyNoteViewModel.StatusName??string.Empty;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(mainModel);
        }

        private bool validate(CompanyShareVolumeListModel model, string actionName)
        {
            int i = 0;
            int count = model.companyShareVolumeModels.Count;
            int flag = 0;
            do
            {
                if (model.companyShareVolumeModels[i].Volume == 0)
                {
                    ViewBag.Message = "Please enter Volume.";
                    flag++;
                    break;
                }
                if (model.companyShareVolumeModels[i].Price == 0)
                {
                    ViewBag.Message = "Please enter Price.";
                    flag++;
                    break;
                }
                i++;
                if (i > count - 1)
                    break;

            } while (true);
            if (flag > 0)
                return false;
            else
                return true;
        }

    }
}