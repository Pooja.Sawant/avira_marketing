﻿CREATE procedure [dbo].[SApp_GetSWOTbyTrendId]
(@TrendId uniqueidentifier,
@includeDeleted bit = 0,
@OrdbyByColumnName NVARCHAR(50) ='StrengthName',
@SortDirection INT = -1,
@PageStart INT = 0,
@PageSize INT = null)
as
/*================================================================================
Procedure Name: SApp_GetSWOTbyTrendId
Author: Sharath
Create date: 02/19/2019
Description: Get list from Customer table by Name or ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
02/19/2019	Sharath			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @TrendName	nvarchar(256);
SELECT @TrendName = TrendName
FROM dbo.Trend
WHERE Trend.Id = @TrendID;

IF @includeDeleted is null SET @includeDeleted = 0

if @PageSize is null or @PageSize =0
set @PageSize = 10
	
begin
;WITH CTE_TBL AS (

SELECT T.Id, T.TrendName,
		StrengthType.Id AS StrengthId,
		StrengthType.SWOTTypeName AS StrengthName, 
		TS.StrengthDescription AS StrengthDescription,
		WeaknessType.Id AS WeaknessId, 
		WeaknessType.SWOTTypeName AS WeaknessName,
		TS.WeaknessDescription AS WeaknessDescription,
		OpportunityType.Id AS OpportunityId, 
		OpportunityType.SWOTTypeName AS OpportunityName, 
		TS.OpportunityDescription AS OpportunityDescription,
		ThreatType.Id AS ThreatId, 
		ThreatType.SWOTTypeName AS ThreatName,
		TS.ThreatDescription AS ThreatDescription,
		CASE
		WHEN @OrdbyByColumnName = 'StrengthName' THEN StrengthType.SWOTTypeName
		WHEN @OrdbyByColumnName = 'WeaknessName' THEN WeaknessType.SWOTTypeName
		WHEN @OrdbyByColumnName = 'OpportunityName' THEN OpportunityType.SWOTTypeName
		WHEN @OrdbyByColumnName = 'ThreatName' THEN ThreatType.SWOTTypeName
		ELSE StrengthType.SWOTTypeName
	END AS SortColumn
		FROM [TrendSWOT] AS TS

INNER JOIN Trend AS T ON T.Id = TS.TrendId
INNER JOIN SWOTType AS StrengthType ON TS.StrengthtTypeId = StrengthType.Id
INNER JOIN SWOTType AS WeaknessType ON TS.WeaknessTypeId = WeaknessType.Id
INNER JOIN SWOTType AS OpportunityType ON TS.OpportunityTypeId = OpportunityType.Id
INNER JOIN SWOTType AS ThreatType ON TS.ThreatTypeId = ThreatType.Id
WHERE (TS.TrendId = @TrendId)
and (@includeDeleted = 1 or TS.IsDeleted = 0)
)

	SELECT 
		  CTE_TBL.*,
		  tCountResult.TotalItems
		FROM CTE_TBL
		CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult
		ORDER BY 
		  CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,
		  CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC 
		OFFSET @PageStart ROWS
		FETCH NEXT @PageSize ROWS ONLY;
		END

END