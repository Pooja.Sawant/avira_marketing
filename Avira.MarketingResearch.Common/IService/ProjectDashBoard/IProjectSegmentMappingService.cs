﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.ProjectDashBoard
{
    public interface IProjectSegmentMappingService
    {
        IEnumerable<ProjectSegmentMappingViewModel> GetAll(Guid Id);
        IEnumerable<ProjectSegmentMappingViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false);
        ProjectSegmentMappingViewModel GetById(Guid guid);


        Result<SpTransactionMessage> Create(ProjectSegmentMappingInsertDbViewModel tfc, AviraUser currentUser);
        Result<SpTransactionMessage> CreateProjectCode(ProjectSegmentMappingInsertDbViewModel tfc, AviraUser currentUser);
        Result<SpTransactionMessage> Update(ProjectSegmentMappingUpdateDbViewModel tfc, AviraUser currenUser); 
    }
}
