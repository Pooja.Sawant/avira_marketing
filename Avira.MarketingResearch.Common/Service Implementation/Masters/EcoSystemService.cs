﻿using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Common.IService.Masters;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.CoreInterfaces.Masters;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;

namespace Avira.MarketingResearch.Common.Service_Implementation.Masters
{
    public class EcoSystemService : IEcoSystemService
    {
        private readonly IEcoSystemRepository _ecoSystemRepository;

        public EcoSystemService(IEcoSystemRepository ecoSystemRepository)
        {
            _ecoSystemRepository = ecoSystemRepository;
        }

        public Result<SpTransactionMessage> Create(EcoSystemInsertDbViewModel tfc, AviraUser currentUser)
        {
            return _ecoSystemRepository.Create(tfc);
        }


        public IEnumerable<EcoSystemViewModel> GetAll(Guid Id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<EcoSystemViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false)
        {
            return _ecoSystemRepository.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName,Search, isDeletedInclude);
        }

        public EcoSystemViewModel GetById(Guid guid)
        {
            return _ecoSystemRepository.GetById(guid);
        }


        public Result<SpTransactionMessage> Update(EcoSystemUpdateDbViewModel tfc, AviraUser currenUser)
        {
            return _ecoSystemRepository.Update(tfc);
        }
    }
}
