﻿
CREATE PROCEDURE [dbo].[SApp_IndustryGridWithFilters]
	(@IndustryIDList dbo.udtUniqueIdentifier READONLY,
	@IndustryName nvarchar(256)='',
	@Description nvarchar(256)='',
	@ParentId uniqueidentifier= null,
	@includeDeleted bit = 0,
	@OrdbyByColumnName NVARCHAR(50) ='CreatedDate',
	@SortDirection INT = -1,
	@PageStart INT = 0,
	@PageSize INT = Null)
	AS
/*================================================================================
Procedure Name: SAppIndustryGridWithFilters
Author: Harshal
Create date: 03/19/2019
Description: Get list from Industry table by Name or ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
03/19/2019	Harshal			Initial Version
__________	____________	______________________________________________________
04/22/2019	Pooja			For Client side paging comment pagesize and null init
================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
if @includeDeleted is null set @includeDeleted = 0;
if len(@IndustryName)>2 set @IndustryName = '%'+ @IndustryName + '%'
else set @IndustryName = ''
declare @SegmentId uniqueidentifier;
select @SegmentId = Id from Segment
where SegmentName = 'End User';


	if @PageSize is null or @PageSize =0
	set @PageSize = 10
	begin
	;WITH CTE_TBL AS (

	SELECT [SubSegment].[Id],
		[SubSegment].SubSegmentName [IndustryName],
		[SubSegment].[Description],
		[SubSegment].[ParentId],
		ParentIndustry.SubSegmentName as ParentIndustryName,
		[SubSegment].[IsDeleted],
		CASE
		WHEN @OrdbyByColumnName = 'IndustryName' THEN [SubSegment].SubSegmentName
		WHEN @OrdbyByColumnName = 'Description' THEN [SubSegment].[Description]
		WHEN @OrdbyByColumnName = 'ParentIndustryName' THEN ParentIndustry.SubSegmentName
		ELSE [SubSegment].SubSegmentName
	END AS SortColumn

FROM [dbo].[SubSegment]
LEFT JOIN [dbo].[SubSegment] ParentIndustry
on  [SubSegment].ParentId =ParentIndustry.Id
and ParentIndustry.SegmentId = @SegmentId
where (NOT EXISTS (SELECT * FROM @IndustryIDList) or  [SubSegment].Id  in (select * from @IndustryIDList))
	AND (@IndustryName = '' or [SubSegment].SubSegmentName like @IndustryName)
	AND (@Description = '' or [SubSegment].[Description] like @Description)
	AND (@ParentId is null or [SubSegment].[ParentId]=@ParentId)
	and SubSegment.SegmentId = @SegmentId
	AND (@includeDeleted = 1 or [SubSegment].IsDeleted = 0)
	)

	SELECT 
		  CTE_TBL.*,
		  tCountResult.TotalItems
		FROM CTE_TBL
		LEFT JOIN [dbo].[SubSegment] P ON CTE_TBL.ParentId=P.Id
		CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult
		ORDER BY 
		  CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,
		  CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC 
		OFFSET @PageStart ROWS
		FETCH NEXT @PageSize ROWS ONLY;
		end
	END