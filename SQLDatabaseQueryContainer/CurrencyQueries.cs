﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public static class CurrencyQueries
    {
        public static readonly string StoreProcSelectGetById = "SApp_GetCurrencybyID";
        public static readonly string StoreProcGetById = "SApp_GetCurrencyConversionbyID";
        public static readonly string StoreProcInsertUpdate = "SApp_insertUpdateCurrencyConversion";
        public static readonly string StoreProcSelectall = "SApp_GetAllCurrencies";
        public static readonly string StoreProcSelectGrid = "SApp_CurrencyConversionGridWithFilters";
    }
}
