﻿
CREATE PROCEDURE [dbo].[SStaging_InsertImportCompanyProfiles_BasicInfo]      
@TemplateName NVARCHAR(200)='',      
@ImportFileName NVARCHAR(200)='',      
@JsonFundamentals	NVARCHAR(MAX) ,
@JsonRevenue NVARCHAR(MAX) ,
@UserCreatedById UNIQUEIDENTIFIER       
AS      
/*================================================================================  
Procedure Name: SStaging_InsertImportCompanyProfiles_Multitab 
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
08/25/2019 Praveen   Initial Version  
================================================================================*/       
DECLARE @ImportId UNIQUEIDENTIFIER =NEWID()      
declare @ErrorMsg NVARCHAR(2048),@CreatedOn Datetime      
BEGIN      
 SET @CreatedOn = GETDATE()      
 BEGIN TRY 
------Clear for existing data----
DELETE FROM ImportData where Id in (select ImportId from StagingCompanyProfileFundamentals sm);    
DELETE FROM StagingCompanyProfileFundamentals;
DELETE FROM StagingCompanyBasicRevenue;



 INSERT INTO ImportData(Id, TemplateName, ImportFileName, ImportedOn, UserImportedById)      
VALUES(@ImportId, @TemplateName, @ImportFileName,@CreatedOn, @UserCreatedById)  
---------------------------------------------------      

INSERT INTO [dbo].[StagingCompanyProfileFundamentals]
   ([Id]   
   ,[ImportId]
   ,RowNo
   ,[CompanyName]
   ,[Headquarter]
   ,[Nature]
   ,[GeographicPresence])
SELECT	NEWID()
,@ImportId
,RowNo
	   ,[dbo].[udfTrim]([CompanyName])
	   ,[dbo].[udfTrim]([Headquarter])
	   ,[dbo].[udfTrim]([Nature])
	   ,[dbo].[udfTrim]([GeographicPresence])
FROM OPENJSON(@JsonFundamentals)      
WITH (
	RowNo INT,
	CompanyName	nvarchar(257),
	Headquarter	nvarchar(256),	
	Nature	nvarchar(50),	
	GeographicPresence	nvarchar(511)) AS jsonFundamentals;

INSERT INTO [dbo].[StagingCompanyBasicRevenue]
   ([Id]   
   ,[ImportId]
   ,RowNo
   ,[CompanyName]
   ,[Currency]	
   ,[Units]	
   ,[FinanicialYear]
   ,[Revenue])	
SELECT	NEWID()
,@ImportId
,RowNo
,[dbo].[udfTrim]([CompanyName])
,Currency 
,Units 
,Year 
,Revenue 
   FROM OPENJSON(@JsonRevenue)      
WITH (
	RowNo INT,
	CompanyName	nvarchar(257),  
	Currency nvarchar(255), 
    Units nvarchar(255),
    Year nvarchar(255),
    Revenue nvarchar(255)) AS jsonRevenue; 

SELECT StoredProcedureName ='SStaging_InsertImportCompanyProfiles_BasicInfo',Message =@ErrorMsg,Success = 1; 
END TRY  
BEGIN CATCH  
    -- Execute the error retrieval routine.  
 DECLARE @ErrorNumber int;  
 DECLARE @ErrorSeverity int;  
 DECLARE @ErrorProcedure varchar(100);  
 DECLARE @ErrorLine int;  
 DECLARE @ErrorMessage varchar(500);  
  
  SELECT @ErrorNumber = ERROR_NUMBER(),  
@ErrorSeverity = ERROR_SEVERITY(),  
@ErrorProcedure = ERROR_PROCEDURE(),  
@ErrorLine = ERROR_LINE(),  
@ErrorMessage = ERROR_MESSAGE()  
  
 insert into dbo.Errorlog(ErrorNumber,  
       ErrorSeverity,  
       ErrorProcedure,  
       ErrorLine,  
       ErrorMessage,  
       ErrorDate)  
values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,@CreatedOn);  

 set @ErrorMsg = 'Error while importing CompanyProfiles, please contact Admin for more details.';  
--THROW 50000,@ErrorMsg,1;    
--return(1)
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine
    ,Message =@ErrorMsg,Success = 0;   
   
END CATCH   
      
      
END