﻿CREATE TABLE [dbo].[Errorlog] (
    [Errorlog]       INT           IDENTITY (1, 1) NOT NULL,
    [ErrorNumber]    INT           NULL,
    [ErrorSeverity]  INT           NULL,
    [ErrorProcedure] VARCHAR (100) NULL,
    [ErrorLine]      INT           NULL,
    [ErrorMessage]   VARCHAR (MAX) NULL,
    [ErrorDate]      DATETIME      NULL,
    CONSTRAINT [PK_Errorlog] PRIMARY KEY CLUSTERED ([Errorlog] ASC)
);



