﻿CREATE PROCEDURE [dbo].[SFab_GetNotificationCount]      
( 
 @AssignedToId UNIQUEIDENTIFIER  
)  
As      
  
/*================================================================================      
Procedure Name: SFab_GetNotificationCount     
Author: Pooja Sawant      
Create date: 25-Feb-2020  
Description: To return count of NotificationWatcher by assigntoid
Change History      
Date  Developer  Reason    
__________ ____________ ______________________________________________________      
25-Feb-2020  Pooja S   Initial Version      
================================================================================*/      
BEGIN   

select count(0) from NotificationWatcher where IsBroadCosted=0 and ModifiedOn is null and AssignedToId=@AssignedToId 
    
END