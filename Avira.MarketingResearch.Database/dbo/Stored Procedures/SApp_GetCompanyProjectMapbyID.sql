﻿
CREATE procedure [dbo].[SApp_GetCompanyProjectMapbyID]
(@MapId uniqueidentifier,
@includeDeleted bit = 0)
as
/*================================================================================
Procedure Name:[SApp_GetCompanyProjectMapbyID]
Author: Harshal
Create date: 05/31/2019
Description: Get list from Company Project Map table by ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
05/31/2019	Harshal			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if @includeDeleted is null set @includeDeleted = 0

SELECT [CompanyprojectMap].[Id],
	   [CompanyprojectMap].[CompanyId],
	   [Company].[CompanyName],
	   [CompanyprojectMap].[ProjectId]
FROM [dbo].[CompanyprojectMap]
INNER JOIN [Company] ON [CompanyprojectMap].CompanyId=[Company].Id
where ( [CompanyprojectMap].Id = @MapId)
and (@includeDeleted = 1 or  [CompanyprojectMap].IsDeleted = 0)

end