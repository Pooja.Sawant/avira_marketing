﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyProductController : AviraBaseController
    {
        private readonly ICompanyProductService _companyProductService;
        private readonly ICompanyProductRepository _companyProductRepository;
        private ILoggerManager _logger;

        public CompanyProductController(IHostingEnvironment hostingEnvironment,
            ICompanyProductService companyProductService,
            ICompanyProductRepository companyProductRepository,
             ILoggerManager logger)
        {
            _companyProductRepository = companyProductRepository;
            _companyProductService = companyProductService;
            _logger = logger;

        }

        [HttpGet("{id}")]
        public ActionResult<MainCompanyProductViewMoel> Get(Guid Id)
        {
            MainCompanyProductViewMoel res = null;
            try
            {
                res = _companyProductService.GetAll(Id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }


        [HttpPost]
        public IActionResult Post([FromBody]MainCompanyProductViewMoel viewModel)
        {

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a adding Company Product  , please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                var result = _companyProductService.InsertUpdate(viewModel, CurrentUser);
                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Company Product is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }
    
    }
}