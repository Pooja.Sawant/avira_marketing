﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.Utils;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Avira.MarketingResearch.Web.Controllers
{
    [Authorize]
    [NoCache]
    public class CompanySWOTController : BaseController
    {
        public readonly IServiceRepository _serviceRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;
        string contentRootPath = string.Empty;
        private ILoggerManager _logger;

        public CompanySWOTController(IServiceRepository serviceRepository, ILoggerManager logger, IHostingEnvironment hostingEnvironment, IConfiguration configuration)
        {
            _hostingEnvironment = hostingEnvironment;
            _serviceRepository = serviceRepository;
            _logger = logger;
            _configuration = configuration;
            contentRootPath = _hostingEnvironment.ContentRootPath;
        }

        public IActionResult Index(string companyId, string projectId)
        {
            
                SetRoleList();
                BindDropdowns();
                ViewBag.Message = string.Empty;
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage responseSWOT = _serviceRepository.GetCompanyDetailsByGuIdsResponse(contollerName, companyId, projectId);
                responseSWOT.EnsureSuccessStatusCode();
                MainCompanySWOTViewModel maincompanySWOTViewModel = responseSWOT.Content.ReadAsAsync<MainCompanySWOTViewModel>().Result;
            try
            {
                maincompanySWOTViewModel.CompanyId = new Guid(companyId);
                maincompanySWOTViewModel.ProjectId = new Guid(projectId);
                if (maincompanySWOTViewModel.companySWOTStrengthViewModel == null || maincompanySWOTViewModel.companySWOTStrengthViewModel.Count == 0)
                {
                    CompanySWOTStrengthViewModel objempty = new CompanySWOTStrengthViewModel();
                    objempty.SWOTTypeName = "Strength";
                    maincompanySWOTViewModel.companySWOTStrengthViewModel.Add(objempty);
                }
                if (maincompanySWOTViewModel.companySWOTOpportunitiesViewModel == null || maincompanySWOTViewModel.companySWOTOpportunitiesViewModel.Count == 0)
                {
                    CompanySWOTOpportunitiesViewModel objempty = new CompanySWOTOpportunitiesViewModel();
                    objempty.SWOTTypeName = "Opportunities";
                    maincompanySWOTViewModel.companySWOTOpportunitiesViewModel.Add(objempty);
                }
                if (maincompanySWOTViewModel.companySWOTThreatsViewModel == null || maincompanySWOTViewModel.companySWOTThreatsViewModel.Count == 0)
                {
                    CompanySWOTThreatsViewModel objempty = new CompanySWOTThreatsViewModel();
                    objempty.SWOTTypeName = "Threats";
                    maincompanySWOTViewModel.companySWOTThreatsViewModel.Add(objempty);
                }
                if (maincompanySWOTViewModel.companySWOTWeeknessesViewModel == null || maincompanySWOTViewModel.companySWOTWeeknessesViewModel.Count == 0)
                {
                    CompanySWOTWeeknessesViewModel objempty = new CompanySWOTWeeknessesViewModel();
                    objempty.SWOTTypeName = "Weaknesses";
                    maincompanySWOTViewModel.companySWOTWeeknessesViewModel.Add(objempty);
                }

                HttpResponseMessage CompanyNoteResponse = _serviceRepository.GetCompanyNoteByIdResponse("CompanyNote", companyId, "CompanySWOT", projectId);
                CompanyNoteViewModel companyNoteViewModel = CompanyNoteResponse.Content.ReadAsAsync<CompanyNoteViewModel>().Result;
                maincompanySWOTViewModel.CompanyNoteViewModel = companyNoteViewModel ?? new CompanyNoteViewModel();

                HttpResponseMessage CompanyResponse = _serviceRepository.GetByIdResponse("Company", companyId);
                CompanyViewModel companyViewModel = CompanyResponse.Content.ReadAsAsync<CompanyViewModel>().Result;
                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, new Guid(projectId));
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                    ViewBag.StatusName = companyNoteViewModel.StatusName;
                }
                ViewBag.CompanyName = companyViewModel.CompanyName;
                ViewBag.CompanyId = maincompanySWOTViewModel.CompanyId;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(maincompanySWOTViewModel); 
        }

        [HttpPost]
        public IActionResult Index(MainCompanySWOTViewModel maincompanySWOTViewModel, string actionName)
        {
            SetRoleList();
            ViewBag.Message = string.Empty;
            bool bResult = false;
            try
            {
                maincompanySWOTViewModel.CompanyNoteViewModel.CompanyType = "CompanySWOT";
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString(); 
                if (actionName.Equals("Save"))
                {
                    bResult = validate(maincompanySWOTViewModel);
                    if (bResult)
                    {
                        maincompanySWOTViewModel.CompanyNoteViewModel.CompanyStatusName = "Draft";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, maincompanySWOTViewModel);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            BindDropdowns();
                            return RedirectToAction("Index", new { CompanyId = maincompanySWOTViewModel.CompanyId.ToString(), ProjectId = maincompanySWOTViewModel.ProjectId.ToString() });
                        }
                    }
                }
                else if (actionName.Equals("Submitted"))
                { 
                    bResult = validate(maincompanySWOTViewModel);
                    if (bResult)
                    {
                        maincompanySWOTViewModel.CompanyNoteViewModel.CompanyStatusName = "Submitted";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, maincompanySWOTViewModel);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            BindDropdowns();
                            return RedirectToAction("Index", new { CompanyId = maincompanySWOTViewModel.CompanyId.ToString(), ProjectId = maincompanySWOTViewModel.ProjectId.ToString() });
                        }
                    }

                }
                else if (actionName.Equals("Reject"))
                {
                    bResult = validate(maincompanySWOTViewModel);
                    if (bResult)
                    {
                        maincompanySWOTViewModel.CompanyNoteViewModel.CompanyStatusName = "Rejected";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, maincompanySWOTViewModel);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            BindDropdowns();
                            return RedirectToAction("Index", new { CompanyId = maincompanySWOTViewModel.CompanyId.ToString(), ProjectId = maincompanySWOTViewModel.ProjectId.ToString() });
                        }
                    }
                }
                else if (actionName.Equals("Approve"))
                {
                    bResult = validate(maincompanySWOTViewModel);
                    if (bResult)
                    {
                        maincompanySWOTViewModel.CompanyNoteViewModel.CompanyStatusName = "Approved";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, maincompanySWOTViewModel);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            BindDropdowns();
                            return RedirectToAction("Index", new { CompanyId = maincompanySWOTViewModel.CompanyId.ToString(), ProjectId = maincompanySWOTViewModel.ProjectId.ToString() });
                        }
                    }
                }

                else if (actionName.Equals("Next"))
                    return RedirectToAction("Index", "CompanyTrendsMap", new { CompanyId = maincompanySWOTViewModel.CompanyId.ToString(), ProjectId = maincompanySWOTViewModel.ProjectId.ToString() });
                else if (actionName.Equals("Previous"))
                    return RedirectToAction("Index", "EcoSystemPresence", new { CompanyId = maincompanySWOTViewModel.CompanyId.ToString(), ProjectId = maincompanySWOTViewModel.ProjectId.ToString() });
                else if (actionName.Equals("Cancel"))
                    return RedirectToAction("Index", "#", new { CompanyId = maincompanySWOTViewModel.CompanyId.ToString(), ProjectId = maincompanySWOTViewModel.ProjectId.ToString() });
                else if (actionName.Equals("AddStrength"))
                {
                    CompanySWOTStrengthViewModel objempty = new CompanySWOTStrengthViewModel();
                    objempty.SWOTTypeName = "Strength";
                    maincompanySWOTViewModel.companySWOTStrengthViewModel.Add(objempty);
                    maincompanySWOTViewModel.CompanyNoteViewModel = maincompanySWOTViewModel.CompanyNoteViewModel ?? new CompanyNoteViewModel();
                }
                else if (actionName.Equals("AddWeeknesses"))
                {
                    CompanySWOTWeeknessesViewModel objempty = new CompanySWOTWeeknessesViewModel();
                    objempty.SWOTTypeName = "Weakness";
                    maincompanySWOTViewModel.companySWOTWeeknessesViewModel.Add(objempty);
                    maincompanySWOTViewModel.CompanyNoteViewModel = maincompanySWOTViewModel.CompanyNoteViewModel ?? new CompanyNoteViewModel();
                }
                else if (actionName.Equals("AddOpportunities"))
                {
                    CompanySWOTOpportunitiesViewModel objempty = new CompanySWOTOpportunitiesViewModel();
                    objempty.SWOTTypeName = "Opportunities";
                    maincompanySWOTViewModel.companySWOTOpportunitiesViewModel.Add(objempty);
                    maincompanySWOTViewModel.CompanyNoteViewModel = maincompanySWOTViewModel.CompanyNoteViewModel ?? new CompanyNoteViewModel();
                }
                else if (actionName.Equals("AddThreats"))
                {
                    CompanySWOTThreatsViewModel objempty = new CompanySWOTThreatsViewModel();
                    objempty.SWOTTypeName = "Threats";
                    maincompanySWOTViewModel.companySWOTThreatsViewModel.Add(objempty);
                    maincompanySWOTViewModel.CompanyNoteViewModel = maincompanySWOTViewModel.CompanyNoteViewModel ?? new CompanyNoteViewModel();
                }

                BindDropdowns();
                HttpResponseMessage CompanyNoteResponse = _serviceRepository.GetCompanyNoteByIdResponse("CompanyNote", maincompanySWOTViewModel.CompanyId.ToString(), "CompanySWOT", maincompanySWOTViewModel.ProjectId.ToString());
                CompanyNoteViewModel companyNoteViewModel = CompanyNoteResponse.Content.ReadAsAsync<CompanyNoteViewModel>().Result;
                companyNoteViewModel = companyNoteViewModel ?? new CompanyNoteViewModel();
                HttpResponseMessage CompanyResponse = _serviceRepository.GetByIdResponse("Company", maincompanySWOTViewModel.CompanyId.ToString());
                CompanyViewModel companyViewModel = CompanyResponse.Content.ReadAsAsync<CompanyViewModel>().Result;
                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, maincompanySWOTViewModel.ProjectId);
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                    ViewBag.StatusName = companyNoteViewModel.StatusName??string.Empty;
                }
                ViewBag.CompanyId = maincompanySWOTViewModel.CompanyId;
                ViewBag.CompanyName = companyViewModel.CompanyName;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
           
            return View(maincompanySWOTViewModel);
        }

        private void BindDropdowns()
        {
            try
            {
                string sCategory = "CompanyClientAndStrategy";
                HttpResponseMessage responseCategories = _serviceRepository.GetAllCategoriesByCompanyIdSegmentResponse(sCategory);
                responseCategories.EnsureSuccessStatusCode();
                ViewBag.CategoryList = responseCategories.Content.ReadAsAsync<List<CompanyClientStrategyCategoryViewModel>>().Result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        private void SetRoleList()
        {
            try
            {
                var roles = HttpContext.Session.GetString("userRoles");
                var userRoles = JsonConvert.DeserializeObject<List<RoleViewModel>>(roles);
                List<string> RoleListItems = new List<string>();
                foreach (var item in userRoles)
                    RoleListItems.Add(item.UserRoleName);
                ViewBag.UserRoleList = RoleListItems;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        /// <summary>
        /// Method to validate Form
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private bool validate(MainCompanySWOTViewModel model)
        { 
            if (model.companySWOTStrengthViewModel.Count >= 1 && (model.companySWOTStrengthViewModel[0].SWOTDescription == string.Empty || model.companySWOTStrengthViewModel[0].CategoryId == Guid.Empty))
            {
                ViewBag.Message = "Please enter atleast one Strength detail for the Company mapping.";
                return false;
            }
            else if (model.companySWOTWeeknessesViewModel.Count >= 1 && (model.companySWOTWeeknessesViewModel[0].SWOTDescription == string.Empty || model.companySWOTWeeknessesViewModel[0].CategoryId == Guid.Empty))
            {
                ViewBag.Message = "Please enter atleast one Weakness detail for the Company mapping.";
                return false;
            }
            else if (model.companySWOTOpportunitiesViewModel.Count >= 1 && (model.companySWOTOpportunitiesViewModel[0].SWOTDescription == string.Empty || model.companySWOTOpportunitiesViewModel[0].CategoryId == Guid.Empty))
            {
                ViewBag.Message = "Please enter atleast one Opportunity detail for the Company mapping.";
                return false;
            }
            else if (model.companySWOTThreatsViewModel.Count >= 1 && (model.companySWOTThreatsViewModel[0].SWOTDescription == string.Empty || model.companySWOTThreatsViewModel[0].CategoryId == Guid.Empty))
            {
                ViewBag.Message = "Please enter atleast one Threats detail for the Company mapping.";
                return false;
            }
            else if (model.CompanyNoteViewModel.AuthorRemark == string.Empty || model.CompanyNoteViewModel.AuthorRemark == null)
            {
                ViewBag.Message = "Please enter Author notes.";
                return false;
            }
            else
            {
                ViewBag.Message = string.Empty;
                return true;
            }
        }
    }
}