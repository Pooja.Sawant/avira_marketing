﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.ImportExport
{
    public class CompanyBasicInfoImportModel
    {
        public int RowNo { get; set; }
        public string CompanyName { get; set; }
        public string Headquarter { get; set; }
        public string Nature { get; set; }
        public string GeographicPresence { get; set; }
        public string Currency { get; set; }
        public string Units { get; set; }
        public string Year1 { get; set; }
        public string RevenueYear1 { get; set; }
        public string Year2 { get; set; }
        public string RevenueYear2 { get; set; }
    }

    public class CompanyBasicInfoRevenueImportModel
    {
        public int RowNo { get; set; }
        public string CompanyName { get; set; }
        public string Currency { get; set; }
        public string Units { get; set; }
        public string Year { get; set; }
        public string Revenue { get; set; }
    }

    //public class CompanyBasicInfoImportResponseModel
    //{
    //    public string Module { get; set; }
    //    public string ErrorNotes { get; set; }
    //    public string Success { get; set; }
    //}

    public class CompanyBasicInfoImportInsertModel
    {
        public string TemplateName { get; set; }
        public string ImportFileName { get; set; }
        public string JsonFundamentals { get; set; }
        public string JsonRevenue { get; set; }
        public Guid UserCreatedById { get; set; }
        public List<CompanyBasicInfoImportModel> CompanyBasicInfoList { get; set; }
        public List<CompanyBasicInfoRevenueImportModel> YearWiseRevenueList { get; set; }
    }
}