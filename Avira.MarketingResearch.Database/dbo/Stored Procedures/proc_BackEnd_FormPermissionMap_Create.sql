﻿CREATE PROCEDURE [dbo].[proc_BackEnd_FormPermissionMap_Create]
(
@FormName NVARCHAR(MAX),
@PermissionName NVARCHAR(MAX)
)
AS
/*================================================================================
Procedure Name: proc_BackEnd_Form_Create
Author: Praveen
Create date: 07/02/2019
Description: To Map Form with Permission from backend
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
07/02/2019	Praveen			Initial Version
================================================================================*/
BEGIN

declare @FormId uniqueidentifier,  @PermissionId uniqueidentifier
SELECT @FormId = Id FROM Form WHERE FormName = @FormName
SELECT @PermissionId = Id FROM Permission WHERE PermissionName = @PermissionName

	INSERT INTO FormPermissionMap(Id,FormId,PermissionId)
	VALUES(NEWID(),@FormId,@PermissionId)

END