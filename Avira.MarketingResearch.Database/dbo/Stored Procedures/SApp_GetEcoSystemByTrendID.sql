﻿
      
 CREATE PROCEDURE [dbo].[SApp_GetEcoSystemByTrendID]            
 (@TrendID uniqueidentifier = null,    
 @EcoSystemName nvarchar(256)='',    
 @includeDeleted bit = 0,        
 @OrdbyByColumnName NVARCHAR(50) ='TrendEcoSystemMapID',        
 @SortDirection INT = -1,        
 @PageStart INT = 0,        
 @PageSize INT = null)            
 AS            
/*================================================================================            
Procedure Name: SApp_GetEcoSystemByTrendID       
Author: Harshal            
Create date: 04/22/2019            
Description: Get list from EcoSystem table by TrendID            
Change History            
Date  Developer  Reason            
__________ ____________ ______________________________________________________            
04/22/2019 Harshal   Initial Version 
================================================================================*/           
            
BEGIN            
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED         
if @PageSize is null or @PageSize =0        
set @PageSize = 10        
if isnull(@EcoSystemName,'') = '' set @EcoSystemName = '' 
else set @EcoSystemName = '%'+ @EcoSystemName +'%';
begin        
;WITH CTE_TBL AS (        
 SELECT         
   Segment.[Id] AS [EcoSystemId],            
   Segment.SegmentName as [EcoSystemName],           
   Segment.[Description],        
   Segment.IsDeleted,        
   TrendEcoSystemMap.Impact as ImpactId,      
   TrendEcoSystemMap.EndUser,      
   TrendEcoSystemMap.TrendId,      
   TrendEcoSystemMap.Id as TrendEcoSytemMapId,      
  cast (case when exists(select 1 from dbo.TrendEcoSystemMap         
     where TrendEcoSystemMap.TrendId = @TrendID        
     and TrendEcoSystemMap.IsDeleted = 0        
     and TrendEcoSystemMap.EcoSystemId = Segment.Id) then 1 else 0 end as bit) as IsMapped,        
 CASE        
  WHEN @OrdbyByColumnName = 'EcoSystemName' THEN Segment.SegmentName        
  WHEN @OrdbyByColumnName = 'Description' THEN Segment.[Description]
  when @OrdbyByColumnName = 'IsMapped' THEN cast(case when TrendEcoSystemMap.TrendId is null then 1 else 0 end as varchar(2)) 
  ELSE Segment.SegmentName        
 END AS SortColumn        
        
FROM [dbo].Segment      
LEFT JOIN dbo.TrendEcoSystemMap       
ON Segment.Id = TrendEcoSystemMap.EcoSystemId
and TrendEcoSystemMap.TrendId = @TrendID
and TrendEcoSystemMap.IsDeleted = 0
where (@EcoSystemName = '' or Segment.SegmentName LIKE @EcoSystemName)
AND (@includeDeleted = 1 or Segment.IsDeleted = 0)        
)        
        
 SELECT         
    CTE_TBL.*,        
    tCountResult.TotalItems        
  FROM CTE_TBL        
  CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult        
  ORDER BY         
    CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,        
    CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC         
  OFFSET @PageStart ROWS        
  FETCH NEXT @PageSize ROWS ONLY;        
  END        
        
END