﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avira.MarketingResearch.Repository.Implementations.CompanyProfile
{
    public class CompanyFinancialAnalysisRepository : ICompanyFinancialAnalysisRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "CompanyFinancialAnalysis";

        public CompanyFinancialAnalysisRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public async Task<MainCompanyFinancialAnalysisInsertUpdateViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search = "", bool isDeletedInclude = false, string id = "")
        {
            MainCompanyFinancialAnalysisInsertUpdateViewModel model = new MainCompanyFinancialAnalysisInsertUpdateViewModel();
            CompanyFinancialAnalysisInsertUpdateViewModel CFAModel = new CompanyFinancialAnalysisInsertUpdateViewModel();
            List<ExpensesBreakdownViewModel> breakModelList = new List<ExpensesBreakdownViewModel>();
            List<AssetsLiabilitiesViewModel> liabltyModelList = new List<AssetsLiabilitiesViewModel>();
            List<CashFlowsViewModel> cashModelList = new List<CashFlowsViewModel>();
            List<CompanyTransactionsInsertUpdateViewModel> transModelList = new List<CompanyTransactionsInsertUpdateViewModel>();

            try
            {
                var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
                using (var conn = new SqlConnection(dapper.ConnectionString))
                {
                    Guid ID = new Guid(id);
                    await conn.OpenAsync();
                    var results = await conn.QueryMultipleAsync(CompanyFinancialAnalysisQueries.StoreProcSelectAll,
                                    new { @CompanyID = ID, @includeDeleted = false },
                                    null, null, CommandType.StoredProcedure);

                    // read as IEnumerable<dynamic>
                    var table1 = await results.ReadAsync<CompanyFinancialAnalysisInsertUpdateViewModel>();
                    var table2 = await results.ReadAsync<ExpensesBreakdownViewModel>();
                    var table3 = await results.ReadAsync<AssetsLiabilitiesViewModel>();
                    var table4 = await results.ReadAsync<CashFlowsViewModel>();
                    var table5 = await results.ReadAsync<CompanyTransactionsInsertUpdateViewModel>();
                    var baseYear = await results.ReadAsync<BaseYearModel>();

                    if (table1 != null)
                        foreach (var item in table1)
                        {
                            CFAModel.ApproverNotes = item.ApproverNotes;
                            CFAModel.AuthorNotes = item.AuthorNotes;
                            CFAModel.CompanyId = item.CompanyId;
                            CFAModel.CompanyStatusId = item.CompanyStatusId;
                            CFAModel.CreatedOn = item.CreatedOn;
                            CFAModel.CurrencyId = item.CurrencyId;
                            CFAModel.DeletedOn = item.DeletedOn;
                            CFAModel.Id = item.Id;
                            CFAModel.IsDeleted = item.IsDeleted;
                            CFAModel.ModifiedOn = item.ModifiedOn;
                            CFAModel.ValueConversionId = item.ValueConversionId;
                            CFAModel.UserCreatedById = item.UserCreatedById;
                            CFAModel.UserDeletedById = item.UserDeletedById;
                            CFAModel.UserModifiedById = item.UserModifiedById;
                        }


                    if (table2 != null)
                        foreach (var item in table2)
                        {
                            ExpensesBreakdownViewModel brmodel = new ExpensesBreakdownViewModel();
                            brmodel.ActualAmount = item.ActualAmount;
                            //brmodel.CompanyFinancialID = item.CompanyFinancialID;
                            brmodel.ConversionAmount = item.ConversionAmount;
                            brmodel.ExpensesType = item.ExpensesType;
                            brmodel.BaseYearMinus_1 = item.BaseYearMinus_1;
                            brmodel.BaseYearMinus_2 = item.BaseYearMinus_2;
                            brmodel.BaseYearMinus_3 = item.BaseYearMinus_3;
                            brmodel.BaseYearMinus_4 = item.BaseYearMinus_4;
                            brmodel.BaseYearMinus_5 = item.BaseYearMinus_5;
                            //brmodel.Id = item.Id;
                            breakModelList.Add(brmodel);
                        }

                    if (table3 != null)
                        foreach (var item in table3)
                        {
                            AssetsLiabilitiesViewModel liabltyModel = new AssetsLiabilitiesViewModel();
                            liabltyModel.ActualAmount = item.ActualAmount;
                            liabltyModel.AssetsType = item.AssetsType;
                            //liabltyModel.CompanyFinancialID = item.CompanyFinancialID;
                            liabltyModel.ConversionAmount = item.ConversionAmount;
                            liabltyModel.BaseYearMinus_1 = item.BaseYearMinus_1;
                            liabltyModel.BaseYearMinus_2 = item.BaseYearMinus_2;
                            liabltyModel.BaseYearMinus_3 = item.BaseYearMinus_3;
                            liabltyModel.BaseYearMinus_4 = item.BaseYearMinus_4;
                            liabltyModel.BaseYearMinus_5 = item.BaseYearMinus_5;
                            //liabltyModel.Id = item.Id;
                            liabltyModelList.Add(liabltyModel);
                        }

                    if (table4 != null)
                        foreach (var item in table4)
                        {
                            CashFlowsViewModel cashModel = new CashFlowsViewModel();
                            cashModel.ActualAmount = item.ActualAmount;
                            cashModel.CashFlowType = item.CashFlowType;
                            //cashModel.CompanyFinancialID = item.CompanyFinancialID;
                            cashModel.ConversionAmount = item.ConversionAmount;
                            cashModel.BaseYearMinus_1 = item.BaseYearMinus_1;
                            cashModel.BaseYearMinus_2 = item.BaseYearMinus_2;
                            cashModel.BaseYearMinus_3 = item.BaseYearMinus_3;
                            cashModel.BaseYearMinus_4 = item.BaseYearMinus_4;
                            cashModel.BaseYearMinus_5 = item.BaseYearMinus_5;
                            //cashModel.Id = item.Id;
                            cashModelList.Add(cashModel);
                        }
                    if (table5 != null)
                        foreach (var item in table5)
                        {
                            CompanyTransactionsInsertUpdateViewModel trasModel = new CompanyTransactionsInsertUpdateViewModel();
                            trasModel.CategoryId = item.CategoryId;
                            trasModel.Date = item.Date;
                            trasModel.Rationale = item.Rationale;
                            trasModel.OtherParty = item.OtherParty;
                            trasModel.Value = item.Value;
                            trasModel.CreatedOn = item.CreatedOn;
                            trasModel.DeletedOn = item.DeletedOn;
                            trasModel.Id = item.Id;
                            trasModel.IsDeleted = item.IsDeleted;
                            trasModel.ModifiedOn = item.ModifiedOn;
                            trasModel.UserCreatedById = item.UserCreatedById;
                            trasModel.UserDeletedById = item.UserDeletedById;
                            trasModel.UserModifiedById = item.UserModifiedById;
                            transModelList.Add(trasModel);
                        }

                    BaseYearModel baseModel = new BaseYearModel();
                    if (baseYear != null)
                    {
                        foreach (var item in baseYear)
                        {

                            baseModel.Id = item.Id;
                            baseModel.BaseYear = item.BaseYear;
                        }
                    }

                    model.companyFinancialAnalysisInsertUpdateViewModel = CFAModel;
                    model.assetsLiabilitiesViewModels = liabltyModelList;
                    model.cashFlowsViewModel = cashModelList;
                    model.expensesBreakdownViewModel = breakModelList;
                    model.companyTransactionsInsertUpdateViewModel = transModelList;
                    model.baseYearModel = baseModel;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            model.expensesBreakdownViewModel = model.expensesBreakdownViewModel.OrderByDescending(e => e.ExpensesType).ToList();
            model.assetsLiabilitiesViewModels = model.assetsLiabilitiesViewModels.OrderBy(e => e.AssetsType).ToList();
            return model;
        }

        public Result<SpTransactionMessage> InsertUpdate(CompanyFinancialAnalysisInsertUpdateModel mainCompanyFinancialAnalysisInsertUpdateViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(CompanyFinancialAnalysisQueries.StoreProcInsertUpdate, new
            {
                //@ID = mainCompanyFinancialAnalysisInsertUpdateViewModel.Id,
                @CompanyId = mainCompanyFinancialAnalysisInsertUpdateViewModel.CompanyId,
                @CurrencyId = mainCompanyFinancialAnalysisInsertUpdateViewModel.CurrencyId,
                //@CompanyFinancialId = mainCompanyFinancialAnalysisInsertUpdateViewModel.CompanyFinancialId,
                @UnitId = mainCompanyFinancialAnalysisInsertUpdateViewModel.ValueConversionId,
                //@CompanyStatusId = null,
                @AuthorNotes = mainCompanyFinancialAnalysisInsertUpdateViewModel.AuthorNotes,
                @ApproverNotes = mainCompanyFinancialAnalysisInsertUpdateViewModel.ApproverNotes,
                @jsonAssets = mainCompanyFinancialAnalysisInsertUpdateViewModel.jsonAssets,
                @jsonExpence = mainCompanyFinancialAnalysisInsertUpdateViewModel.jsonExpence,
                @jsonCash = mainCompanyFinancialAnalysisInsertUpdateViewModel.jsonCash,
                @jsonTrans = mainCompanyFinancialAnalysisInsertUpdateViewModel.jsonTrans,
                @UserCreatedById = mainCompanyFinancialAnalysisInsertUpdateViewModel.UserCreatedById
            }, CommandType.StoredProcedure);
            return spTransactionMessage;

        }


        public IEnumerable<ValueConversionViewModel> GetAllValueConversion()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<ValueConversionViewModel>(CompanyFinancialAnalysisQueries.GetAllValueConversion
                            , null
                            , null, true, null, CommandType.StoredProcedure);
        }

        public IEnumerable<CurrencyViewModel> GetAllCurrencies()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<CurrencyViewModel>(CompanyFinancialAnalysisQueries.StoreProcGetAllCurrencies
                            , null
                            , null, true, null, CommandType.StoredProcedure);

        }

        public IEnumerable<CategoryViewModel> GetAllCategories()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<CategoryViewModel>(CompanyFinancialAnalysisQueries.StoreProcGetAllCategories
                            , null
                            , null, true, null, CommandType.StoredProcedure);

        }
    }
}
