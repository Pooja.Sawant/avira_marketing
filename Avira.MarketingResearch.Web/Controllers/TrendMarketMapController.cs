﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.Models;
using Avira.MarketingResearch.Web.Utils;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Avira.MarketingResearch.Web.Controllers
{
    [NoCache]
    [Authorization]
    public class TrendMarketMapController : BaseController
    {
        private readonly IServiceRepository _serviceRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;
        string contentRootPath = string.Empty;

        private ILoggerManager _logger;
        public TrendMarketMapController(IServiceRepository serviceRepository, ILoggerManager logger, IHostingEnvironment hostingEnvironment, IConfiguration configuration)
        {
            _hostingEnvironment = hostingEnvironment;
            _serviceRepository = serviceRepository;
            _logger = logger;
            _configuration = configuration;
            contentRootPath = _hostingEnvironment.ContentRootPath;
        }

        public ActionResult Index(string id, string currentFilter, string search, int PageStart, string Direction, int totalRecords, int currentpage)
        {
            SetRoleList(); 
            MainTrendMarketMapInsertUpdateDbViewModel model = new MainTrendMarketMapInsertUpdateDbViewModel();
            try
            {
                model.TrendId = new Guid(id);
                model.MarketName = search;
                //int PageStart = 0;
                int pagesize = int.Parse(_configuration.GetSection("Pagination")["TrendPagingSize"]);
                int SortDirection = 1;

                if (Direction == "Next" && (PageStart == 0 || PageStart < (totalRecords - pagesize)))
                {
                    PageStart = PageStart + pagesize;
                }
                else if (Direction == "Previous")
                {
                    if (PageStart >= pagesize)
                        PageStart = PageStart - pagesize;
                    else
                    {
                        PageStart = 0;
                    }
                }
                if (string.IsNullOrEmpty(Direction) && currentpage > 0)
                {
                    PageStart = currentpage * pagesize;
                    PageStart = (PageStart - pagesize);
                }
                else
                {
                    //PageStart = PageStart ?? 0 + pagesize;
                }

                //------Get master data for Impact           
                HttpResponseMessage responseImpactType = _serviceRepository.GetResponse("ImpactType");
                responseImpactType.EnsureSuccessStatusCode();
                ViewBag.ImpactList = responseImpactType.Content.ReadAsAsync<List<ImpactTypeViewModel>>().Result;

                HttpResponseMessage responseTrend = _serviceRepository.GetByIdResponse("Trend", id);

                TrendViewModel trend = responseTrend.Content.ReadAsAsync<TrendViewModel>().Result;
                model.TrendModel = trend ?? new TrendViewModel();

                //------Get trend note by trend Id           
                HttpResponseMessage responseTrendNote = _serviceRepository.GetByIdResponse("TrendNote", id, "Market", "id");
                responseTrendNote.EnsureSuccessStatusCode();
                var tnm = responseTrendNote.Content.ReadAsAsync<TrendNoteViewModel>().Result;
                model.TrendNoteModel = tnm ?? new TrendNoteViewModel();
                model.TrendModel.TrendStatusName = model.TrendNoteModel.TrendStatusName;
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.GetResponse(contollerName, model.TrendId, PageStart, pagesize, SortDirection, "IsMapped", search);
                response.EnsureSuccessStatusCode();
                List<TrendMarketMapViewModel> trendMarketMap = response.Content.ReadAsAsync<List<TrendMarketMapViewModel>>().Result;
                model.TrendMarketMapInsertUpdate = trendMarketMap;

                if (trendMarketMap != null && trendMarketMap.Count > 0)
                {
                    totalRecords = trendMarketMap.FirstOrDefault().TotalItems;
                    ViewBag.totalPage = (int)Math.Ceiling(totalRecords / (double)pagesize);

                }

                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, model.TrendModel.ProjectID);
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                }

                ViewBag.totalRecords = totalRecords;
                ViewBag.PageStart = PageStart;
                ViewBag.currentpage = currentpage;
                ViewBag.Id = model.TrendId;
                //model.TrendMarketMapInsertUpdate = PaginatedList<TrendMarketMapViewModel>.CreateAsync(trendMarketMap.AsQueryable(), 1, pageNumber??0);
                //LoadData();
                //return View(model);

                //int pageSize = 3;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(model);
        }

        private void SetRoleList()
        {
            try
            {
                var roles = HttpContext.Session.GetString("userRoles");
                var userRoles = JsonConvert.DeserializeObject<List<RoleViewModel>>(roles);
                List<string> RoleListItems = new List<string>();
                foreach (var item in userRoles)
                    RoleListItems.Add(item.UserRoleName);
                ViewBag.UserRoleList = RoleListItems;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult Index(MainTrendMarketMapInsertUpdateDbViewModel model, string actionName, int PageStart, string Direction, int totalRecords, int currentpage)
        {
            SetRoleList();
            ViewBag.Message = string.Empty;
            bool bResult = false;
            int pagesize = int.Parse(_configuration.GetSection("Pagination")["TrendPagingSize"]);
            int SortDirection = 1;
            try
            {
                model.TrendNoteModel.TrendType = "Market";
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                if (actionName.Equals("save"))
                {

                    bResult = validate(model);
                    if (bResult)
                    {
                        model.TrendNoteModel.TrendStatusName = "Draft";
                       
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            //ViewBag.Message = viewModel.Message;
                            return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.MarketName });
                        }
                    }

                }
                else if (actionName.Equals("Submitted"))
                {

                    bResult = validate(model);
                    if (bResult)
                    {
                        model.TrendNoteModel.TrendStatusName = "Submitted";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            //ViewBag.Message = viewModel.Message;
                            return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.MarketName });
                        }
                    }

                }
                else if (actionName.Equals("Rejected"))
                {
                    bResult = validate(model);
                    if (bResult)
                    {
                        model.TrendNoteModel.TrendStatusName = "Rejected";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            //ViewBag.Message = viewModel.Message;
                            return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.MarketName });
                        }
                    }
                }
                else if (actionName.Equals("Approve"))
                {
                    bResult = validate(model);
                    if (bResult)
                    {
                        model.TrendNoteModel.TrendStatusName = "Approved";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            //ViewBag.Message = viewModel.Message;
                            return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.MarketName });
                        }
                    }
                }
                else if (actionName.Equals("search"))
                {
                    return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.MarketName });
                }
                else if (actionName.Equals("Next"))
                {
                    return RedirectToAction("Index", "TrendProjectMap", new { id = model.TrendId.ToString() });
                }
                else if (actionName.Equals("Previous"))
                {
                    return RedirectToAction("Index", "TrendIndustryMap", new { id = model.TrendId.ToString() });
                }
                else if (actionName.Equals("Cancel"))
                {
                    return RedirectToAction("Index", "TrendMarketMap", new { id = model.TrendId.ToString() });
                }

                HttpResponseMessage responseImpactType = _serviceRepository.GetResponse("ImpactType");
                responseImpactType.EnsureSuccessStatusCode();
                ViewBag.ImpactList = responseImpactType.Content.ReadAsAsync<List<ImpactTypeViewModel>>().Result;

                HttpResponseMessage responseTrend = _serviceRepository.GetByIdResponse("Trend", model.TrendId.ToString());

                TrendViewModel trend = responseTrend.Content.ReadAsAsync<TrendViewModel>().Result;
                model.TrendModel = trend ?? new TrendViewModel();

                HttpResponseMessage responseTrendNote = _serviceRepository.GetByIdResponse("TrendNote", model.TrendId.ToString(), "Company Group", "id");
                responseTrendNote.EnsureSuccessStatusCode();
                var tnm = responseTrendNote.Content.ReadAsAsync<TrendNoteViewModel>().Result;
                model.TrendNoteModel = tnm ?? new TrendNoteViewModel();
                model.TrendModel.TrendStatusName = model.TrendNoteModel.TrendStatusName;

                HttpResponseMessage response = _serviceRepository.GetResponse(contollerName, model.TrendId, PageStart, pagesize, SortDirection, "IsMapped", string.Empty);
                response.EnsureSuccessStatusCode();
                List<TrendMarketMapViewModel> trendMarketMap = response.Content.ReadAsAsync<List<TrendMarketMapViewModel>>().Result;
                model.TrendMarketMapInsertUpdate = trendMarketMap;
                if (trendMarketMap != null && trendMarketMap.Count > 0)
                {
                    totalRecords = trendMarketMap.FirstOrDefault().TotalItems;
                    ViewBag.totalPage = totalRecords / pagesize;
                }

                ViewBag.totalRecords = totalRecords;
                ViewBag.PageStart = PageStart;
                ViewBag.currentpage = currentpage;
                ViewBag.Id = model.TrendId;

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }


            return View(model);
        }


        /// <summary>
        /// Method to validate Form
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private bool validate(MainTrendMarketMapInsertUpdateDbViewModel model)
        {
            if (model.TrendMarketMapInsertUpdate != null && model.TrendMarketMapInsertUpdate.Count(x => x.ImpactId != Guid.Empty) < 2)
            {
                ViewBag.Message = "Please select atleast two markets for the trend mapping.";
                return false;
            }
            else if (model.TrendMarketMapInsertUpdate != null && model.TrendMarketMapInsertUpdate.Count(x => (x.ImpactId != Guid.Empty && x.EndUser)) < 1)
            {
                ViewBag.Message = "Please select atleast one end user for the trend mapping.";
                return false;
            }
            else if (model.TrendMarketMapInsertUpdate != null && model.TrendMarketMapInsertUpdate.Where(x => x.ImpactId != Guid.Empty).Select(x => x.ImpactId).Distinct().Count() == 1)
            {
                ViewBag.Message = "Minimum 1 Direct and Minimum 1 Indirect to be selected.";
                return false;
            }
            else if (model.TrendMarketMapInsertUpdate != null && model.TrendMarketMapInsertUpdate.Any(x => x.ImpactId == Guid.Empty && x.EndUser))
            {
                ViewBag.Message = "Please select direct/indirect impact value.";
                return false;
            }
            if (model.TrendNoteModel.AuthorRemark == null || model.TrendNoteModel.AuthorRemark == "" || model.TrendNoteModel.AuthorRemark == string.Empty)
            {
                ViewBag.Message = "Please enter Author Remark!!";
                return false;
            }
            else
            {
                ViewBag.Message = string.Empty;
                return true;
            }
        }
    }
}
