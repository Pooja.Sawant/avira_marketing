﻿using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.CompanyProfile
{
    public class CompanyProfitAndLossService : ICompanyProfitAndLossService
    {
        private readonly ICompanyProfitAndLossRepository _companyProfitAndLossRepository;
        private readonly ICompanyNoteRepository _iCompanyNoteRepository;

        public CompanyProfitAndLossService(ICompanyProfitAndLossRepository CompanyProfitAndLossRepository, ICompanyNoteRepository CompanyNoteRepository)
        {
            _companyProfitAndLossRepository = CompanyProfitAndLossRepository;
            _iCompanyNoteRepository = CompanyNoteRepository;
        }

        public IEnumerable<CompanyProfitAndLossViewModel> GetCompanyProfitAndLossData(Guid CompanyId, AviraUser user)
        {
            var res = _companyProfitAndLossRepository.GetCompanyProfitAndLossData(CompanyId, user);
            return res;
        }
    }
}
