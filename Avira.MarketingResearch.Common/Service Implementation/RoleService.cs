﻿using System;
using System.Collections.Generic;
using System.Text;
using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;

namespace Avira.MarketingResearch.Common.Service_Implementation
{
    public class RoleService : IRoleService
    {
        private readonly IRoleRepository _roleRepository;
        public RoleService(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }
        public Result<SpTransactionMessage> Create(RoleInsertDbViewModel roleInsertDbViewModel, AviraUser currentUser)
        {
            return _roleRepository.Create(roleInsertDbViewModel, currentUser);
        }

        public Result<SpTransactionMessage> Put(RoleUpdateViewModel roleInsertDbViewModel, AviraUser currentUser)
        {
            return _roleRepository.Put(roleInsertDbViewModel, currentUser);
        }

        public Result<SpTransactionMessage> Update(RoleUpdateViewModel roleInsertDbViewModel, AviraUser currentUser)
        {
            return _roleRepository.Update(roleInsertDbViewModel, currentUser);
        }

        public IEnumerable<RoleViewModel> GetAllPermissions()
        {
            return _roleRepository.GetAllPermissions();
        }

        public IEnumerable<RoleViewModel> Get(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false)
        {
            return _roleRepository.Get(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude);
        }

        public IEnumerable<RoleEditViewModel> GetById(Guid guid)
        {
            return _roleRepository.GetById(guid);
        }
    }
}
