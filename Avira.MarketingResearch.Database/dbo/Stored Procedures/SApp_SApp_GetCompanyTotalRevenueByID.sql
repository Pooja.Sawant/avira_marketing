﻿
--sp_helptext SApp_SApp_GetCompanyTotalRevenueByID

-- =============================================      
-- Author:  Laxmikant      
-- Create date: 05/29/2019      
-- Description: Get Company total revenue by Company Id
-- =============================================      
CREATE PROCEDURE [dbo].[SApp_SApp_GetCompanyTotalRevenueByID]           
 @ComapanyID uniqueidentifier = null,       
 @ProjectID uniqueidentifier = null,       
 @includeDeleted bit = 0           
AS      
BEGIN       
 SET NOCOUNT ON;        
 SELECT [CompanyRevenue].[Id] 
		,[CompanyRevenue].[CompanyId]  
		--,[CompanyRevenue].[projectId]
		,[CompanyRevenue].[CurrencyId]
		,[CompanyRevenue].[UnitId]
		,[CompanyRevenue].[TotalRevenue]
		,[CompanyRevenue].[CreatedOn]
		,[CompanyRevenue].[UserCreatedById]
		,[CompanyRevenue].[ModifiedOn]
		,[CompanyRevenue].[UserModifiedById]
		,[CompanyRevenue].[IsDeleted]
		,[CompanyRevenue].[DeletedOn]
		,[CompanyRevenue].[UserDeletedById]
		,[CompanyRevenue].[Year]
  FROM [dbo].[CompanyRevenue]      
   
  WHERE ([CompanyRevenue].CompanyId = @ComapanyID )--and [CompanyRevenue].[projectId] = @ProjectID)            
 AND (@includeDeleted = 0 or [CompanyRevenue].IsDeleted = 1)        
END