﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using Microsoft.AspNetCore.Http;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avira.MarketingResearch.Repository.Implementations.CompanyProfile
{
    public class CompanyShareVolumeRepository : ICompanyShareVolumeRepository
    {
        protected IDbHelper _dbHelper;
        public CompanyShareVolumeRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public async Task<MainCompanyShareVolModel> GetShareVolume(string Id)
        {
            MainCompanyShareVolModel mainModel = new MainCompanyShareVolModel();
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            using (var conn = new SqlConnection(dapper.ConnectionString))
            {
                var results = await conn.QueryMultipleAsync(CompanyShareVolumeQueries.StoreProcCompanyShareVolume,
                                   new { @CompanyID = new Guid(Id), @includeDeleted = false },
                                   null, null, CommandType.StoredProcedure);

                var table1 = await results.ReadAsync<CompanyShareVolumeModel>();
                var table2 = await results.ReadAsync<CompanyShareImageModel>();

 
                List<CompanyShareVolumeModel> dataList = new List<CompanyShareVolumeModel>();
                CompanyShareImageModel imageModel = new CompanyShareImageModel();
                if (table1 != null)
                {
                    foreach (var item in table1)
                    {
                        CompanyShareVolumeModel dataModel = new CompanyShareVolumeModel();
                        dataModel.BaseDate = item.BaseDate;
                        dataModel.CompanyId = item.CompanyId;
                        dataModel.Id = item.Id;
                        dataModel.Period = item.Period;
                        dataModel.CurrencyId = item.CurrencyId;
                        dataModel.Date = item.Date;
                        dataModel.Price = item.Price;
                        dataModel.Volume = item.Volume;
                        dataModel.IsDeleted = item.IsDeleted;
                        dataModel.UserCreatedById = item.UserCreatedById;
                        dataList.Add(dataModel);
                    }
                }

                if (table2 != null)
                {
                    foreach (var item in table2)
                    {
                        imageModel.CompanyId = item.CompanyId;
                        imageModel.Id = item.Id;
                        imageModel.ImageActualName = item.ImageActualName;
                        imageModel.ImageDisplayName = item.ImageDisplayName;
                        imageModel.ImageFile = item.ImageFile;
                        imageModel.ImageURL = item.ImageURL;
                        imageModel.ProjectId = item.ProjectId;
                        imageModel.ParentCompanyId = item.ParentCompanyId;
                    }
                }

                mainModel.CompanyShareImageModel = imageModel;
                mainModel.CompanyShareVolumeDataModel = dataList;
            }

            return mainModel;
            //return dapper.Query<CompanyShareVolumeModel>(CompanyShareVolumeQueries.StoreProcCompanyShareVolume
            //                , new { @CompanyID = Id, @includeDeleted = false }
            //                , null, true, null, CommandType.StoredProcedure);
        }

        public IEnumerable<CurrencyViewModel> GetAllCurrencies()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<CurrencyViewModel>(CompanyShareVolumeQueries.StoreProcGetAllCurrencies
                            , null
                            , null, true, null, CommandType.StoredProcedure);

        }

        public Result<SpTransactionMessage> InsertUpdate(CompanyShareVolumeListModel companyShareVolumeModel)
        {
            DataTable companyShareVolMapTable = new DataTable();
            companyShareVolMapTable.Columns.Add("Id", typeof(Guid));
            companyShareVolMapTable.Columns.Add("IntValue", typeof(int));
            companyShareVolMapTable.Columns.Add("DecimalOne", typeof(decimal));
            companyShareVolMapTable.Columns.Add("DecimaTwo", typeof(string));

            if (companyShareVolumeModel != null & companyShareVolumeModel.companyShareVolumeModels.Count > 0)
            {
                foreach (var item in companyShareVolumeModel.companyShareVolumeModels)
                {
                    companyShareVolMapTable.Rows.Add(new Object[] {
                        item.Id,
                        item.Date,
                        item.Price,
                        item.Volume });
                }
            }

            var spTransactionMessage = _dbHelper.ExecuteQuery(CompanyShareVolumeQueries.StoreProcInsertUpdate,
               new
               {
                   @CompanyId = companyShareVolumeModel.CompanyId,
                   @CurrencyId = companyShareVolumeModel.CurrencyId,
                   @Period = companyShareVolumeModel.Period,
                   @BaseDate = companyShareVolumeModel.BaseDate,
                   @ShareVolumeData = companyShareVolMapTable,
                   @AviraUserId = companyShareVolumeModel.companyShareVolumeModels.FirstOrDefault().AviraUserId
               }
                    , CommandType.StoredProcedure);
            return spTransactionMessage;
        }

        public Result<SpTransactionMessage> SaveImageInServer(MainCompanyShareVolModel Model, AviraUser currenUser)
        {
            try
            {
                var spTransactionMessage = _dbHelper.ExecuteQuery(CompanyShareVolumeQueries.StoreProcGetSaveImageLogo,
               new
               {
                   @Id = Model.CompanyShareImageModel.Id,
                   @ImageURL = Model.CompanyShareImageModel.ImageURL,
                   @ImageDisplayName = Model.CompanyShareImageModel.ImageDisplayName,
                   @ActualImageName = Model.CompanyShareImageModel.ImageActualName,
                   @CompanyId = Model.CompanyShareImageModel.CompanyId,
                   @ProjectId = Model.CompanyShareImageModel.ProjectId,
                   @ParentCompanyId = Model.CompanyShareImageModel.ParentCompanyId,
                   @UserCreatedById = Model.CompanyShareImageModel.UserCreatedById
               }
                    , CommandType.StoredProcedure);
                return spTransactionMessage;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
