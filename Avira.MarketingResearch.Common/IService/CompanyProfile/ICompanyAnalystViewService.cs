﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.CompanyProfile
{
    public interface ICompanyAnalystViewService
    {
        CompanyAnalystViewModel GetById(Guid companyId, Guid projectId);
        Result<SpTransactionMessage> InsertUpdate(MainCompanyAnalystViewModel tfc, AviraUser currenUser);
    }
}
