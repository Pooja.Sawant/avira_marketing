﻿CREATE procedure [dbo].[SFab_GetAllIndustries]
(@UserId uniqueidentifier =  null)
as
/*================================================================================
Procedure Name: [SApp_GetAllIndustries]
Author: Sai Krishna
Create date: 08/05/2019
Description: Get list from Industry
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
08/05/2019	Sai Krishna		Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT [Industry].[Id], 
		[Industry].SubSegmentName AS [IndustryName]
	--FROM [dbo].[Industry]

	FROM [dbo].SubSegment [Industry]
INNER JOIN Segment ON [Industry].SegmentId = Segment.Id AND Segment.SegmentName = 'End User'

where ([Industry].[IsDeleted] = 0)  
end