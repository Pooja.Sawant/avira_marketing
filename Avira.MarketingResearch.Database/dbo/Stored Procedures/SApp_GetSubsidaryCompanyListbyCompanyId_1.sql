﻿CREATE procedure [dbo].[SApp_GetSubsidaryCompanyListbyCompanyId]
(@CompanyID uniqueidentifier)
as
/*================================================================================
Procedure Name: [SApp_GetSubsidaryCompanyListbyCompanyId]
Author: Sharath
Create date: 05/06/2019
Description: Get list from Company table by ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
05/06/2019	Sai Krishna		Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT [Id] as CompanySubsidaryId,
		[CompanyName] as CompanySubsidaryName,
		[CompanyLocation] as CompanySubsidaryLocation,		
		[IsHeadQuarters]
FROM [dbo].[Company]
where [Company].ParentId = @CompanyID and [Company].[IsDeleted] = 0

end