﻿
CREATE PROCEDURE [dbo].[SApp_FundamentalGridWithFilters]
	(@FundamentalIDList dbo.udtUniqueIdentifier READONLY,
	@FundamentalName nvarchar(256)='',
	@Description nvarchar(256)='',
	@includeDeleted bit = 0,
	@OrdbyByColumnName NVARCHAR(50) ='CreatedDate',
	@SortDirection INT = -1,
	@PageStart INT = 0,
	@PageSize INT = null)
	AS
/*================================================================================
Procedure Name: SApp_FundamentalGridWithFilters
Author: Adarsh
Create date: 04/02/2019
Description: Get list from Fundamental table by Name or ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
04/02/2019	Adarsh			Initial Version
__________	____________	______________________________________________________
04/04/2019	Pooja			Some changes for Paging, sorting, searching with CRUD
================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
if @includeDeleted is null set @includeDeleted = 0;
if len(@FundamentalName)>2 set @FundamentalName = '%'+ @FundamentalName + '%'
else set @FundamentalName = ''
if len(@Description)>2 set @Description = '%'+ @Description + '%';
else set @Description = '';
	
	if @PageSize is null or @PageSize =0
		set @PageSize = 10
begin
;WITH CTE_TBL AS (

	SELECT [Fundamental].[Id],
		[Fundamental].[FundamentalName],
		[Fundamental].[Description],
		[Fundamental].[IsDeleted],
		CASE
		WHEN @OrdbyByColumnName = 'FundamentalName' THEN [Fundamental].FundamentalName
		WHEN @OrdbyByColumnName = 'Description' THEN [Fundamental].[Description]
		ELSE [Fundamental].[FundamentalName]
	END AS SortColumn

FROM [dbo].[Fundamental]
where (NOT EXISTS (SELECT * FROM @FundamentalIDList) or  [Fundamental].Id  in (select * from @FundamentalIDList))
	AND (@FundamentalName = '' or [Fundamental].[FundamentalName] like @FundamentalName)
	AND (@Description = '' or [Fundamental].[Description] like @Description)
	AND (@includeDeleted = 1 or [Fundamental].IsDeleted = 0)
	)

	SELECT 
		  CTE_TBL.*,
		  tCountResult.TotalItems
		FROM CTE_TBL
		CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult
		ORDER BY 
		  CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,
		  CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC 
		OFFSET @PageStart ROWS
		FETCH NEXT @PageSize ROWS ONLY;
end
	END