﻿/*================================================================================  
Procedure Name: [SFab_InsertUpdateCustomMarketName]  
Author: Nitin Tarkar  
Create date: 04-Sep-2019  
Description: Insert / Update Custom Market Name
Change History  
Date        Developer     Reason  
__________  ____________  ______________________________________________________  
04-Sep-2019 Nitin Tarkar   Initial Version  
================================================================================*/  
CREATE   PROCEDURE SFab_InsertUpdateCustomMarketName
(
	@UserId uniqueidentifier,
	@ProjectId uniqueidentifier,
	@TrendId uniqueidentifier=null,
	@Id uniqueidentifier=null,
	@CustomMarketName nvarchar(250),
	@CustomMarketRank int,
	@Description nvarchar(250)
)  
AS  
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	Declare @ErrorMsg NVARCHAR(2048) 
	Declare @CustomerId uniqueidentifier
	Set @CustomerId = dbo.fun_GetCustomerIdForLoggedInUser(@UserId)

	BEGIN TRY         
		IF Exists(SELECT 1 FROM MakeMyMarket WHERE Id = @Id)
		BEGIN

			UPDATE MakeMyMarket
			SET CustomMarketRank				=	@CustomMarketRank,
				CustomMarketName                =   @CustomMarketName,
				[Description]					=	@Description,				
				ModifiedOn						=   GETDATE(),
				UserModifiedById                =   @UserId
			WHERE CustomerId = @CustomerId AND 
				  ProjectId = @ProjectId AND 
				  (
						(@TrendId Is Null And TrendId Is Null) Or 
						(@TrendId Is Not Null And TrendId = @TrendId)
				   ) AND 
				  Id = @Id

			SELECT StoredProcedureName ='SFab_InsertUpdateCustomMarketName - Update',Message =@ErrorMsg, Success = 1;  
		END
		ELSE
		BEGIN
			
			IF Not Exists(SELECT 1 FROM MakeMyMarket 
					  WHERE CustomMarketName = @CustomMarketName And CustomerId = @CustomerId And ProjectId = @ProjectId  )
			BEGIN

				INSERT INTO MakeMyMarket
				(				
					ID, 
					CustomerId,
					TrendId,
					ProjectId,
					CustomMarketRank,
					CustomMarketName,				
					--IsMarketSelected,
					CreatedOn,
					UserCreatedById,
					IsDeleted
				) 
				Values
				(				
					NEWID(),
					@CustomerId,
					@TrendId,
					@ProjectId,
					@CustomMarketRank,
					@CustomMarketName,				
					--@IsMarketSelected,
					GETDATE(),
					@UserId,
					0
				)  
				SELECT StoredProcedureName ='SFab_InsertUpdateCustomMarketName - Insert',Message =@ErrorMsg,Success = 1;    
			End
			Else
			Begin

				SELECT StoredProcedureName ='SFab_InsertUpdateCustomMarketName - Insert',Message ='This market custom name is already exist.',Success = 0; 
			End			   
		END    
	END TRY      
	BEGIN CATCH      
		-- Execute the error retrieval routine.      
		DECLARE @ErrorNumber int;      
		DECLARE @ErrorSeverity int;      
		DECLARE @ErrorProcedure varchar(100);      
		DECLARE @ErrorLine int;      
		DECLARE @ErrorMessage varchar(500);      
      
		SELECT @ErrorNumber = ERROR_NUMBER(),      
			@ErrorSeverity = ERROR_SEVERITY(),      
			@ErrorProcedure = ERROR_PROCEDURE(),      
			@ErrorLine = ERROR_LINE(),      
			@ErrorMessage = ERROR_MESSAGE()      
      
		INSERT INTO dbo.Errorlog(ErrorNumber,      
			   ErrorSeverity,      
			   ErrorProcedure,      
			   ErrorLine,      
			   ErrorMessage,      
			   ErrorDate)      
				values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());

		SET @ErrorMsg = 'Error while insert a new TrendProjectMap, please contact Admin for more details.';    
		SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine,Message =@ErrorMsg,Success = 0;   
		SELECT StoredProcedureName ='SFab_InsertUpdateCustomMarketName',Message =@ErrorMsg,Success = 0;    
	END CATCH
END