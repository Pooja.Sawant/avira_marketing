﻿

CREATE procedure [dbo].[SApp_InsertUpdateCompanyProductCategoryMap]      
(
@CompanyProductId uniqueidentifier,   
@CompanyProductCategoryMap dbo.[udtUniqueIdentifier] readonly,
@AviraUserId uniqueidentifier
)      
as      
/*================================================================================      
Procedure Name: SApp_InsertUpdateCompanyProductCategoryMap      
Author: Harshal      
Create date: 04/22/2019      
Description: Insert a record into CompanyProductCategoryMap table      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
04/22/2019 Harshal   Initial Version    
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)  
    
BEGIN TRY         
--mark records as delete which are not in list
update [dbo].[CompanyProductCategoryMap]
set IsDeleted = 1,
UserDeletedById = @AviraUserId,
DeletedOn = GETDATE()
from [dbo].[CompanyProductCategoryMap]
where [CompanyProductCategoryMap].CompanyProductId = @CompanyProductId
and not exists (select 1 from @CompanyProductCategoryMap map where map.ID = [CompanyProductCategoryMap].ProductCategoryId)

 --Update records for change
update [dbo].[CompanyProductCategoryMap]
set ProductCategoryId = map.ID,
ModifiedOn = GETDATE(),
UserModifiedById = @AviraUserId
from [dbo].[CompanyProductCategoryMap]
inner join @CompanyProductCategoryMap map 
on map.ID = [CompanyProductCategoryMap].ProductCategoryId
where [CompanyProductCategoryMap].CompanyProductId = @CompanyProductId;

--Insert new records
insert into [dbo].[CompanyProductCategoryMap]([Id],      
        [CompanyProductId],      
        [ProductCategoryId], 
        [IsDeleted],      
        [UserCreatedById],      
        [CreatedOn])      
 select NEWID(),
   @CompanyProductId,       
   map.ID as [ProductCategoryId],
   0,      
   @AviraUserId,       
   GETDATE()
 from @CompanyProductCategoryMap map
 where not exists (select 1 from [dbo].[CompanyProductCategoryMap] map1
					where map1.CompanyProductId = @CompanyProductId
					and map.ID = map1.ProductCategoryId);       
     
SELECT StoredProcedureName ='SApp_InsertUpdateCompanyProductCategoryMap ',Message =@ErrorMsg,Success = 1;       
    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(2048);      
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());      
    
 set @ErrorMsg = 'Error while insert a new CompanyProductCategoryMap, please contact Admin for more details.';    
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
       
END CATCH      
      
      
end