﻿        
        
CREATE procedure [dbo].[SApp_insertupdateCompanyRevenueSegmentationSplit]      
(            
@SegmentationList dbo.udtGUIDSegmentationSplitMultiValue READONLY,     
@CompanyRevenueId uniqueidentifier,   
@CompanyId uniqueidentifier,    
@Split int,    
@Year int,  
@UserCreatedById uniqueidentifier         
--@CreatedOn Datetime        
)            
as            
/*================================================================================            
Procedure Name: SApp_insertupdateCompanyRevenueSegmentationSplit      
Change History            
Date  Developer  Reason            
__________ ____________ ______________________________________________________            
05/06/2019 Gopi   Initial Version            
================================================================================*/            
BEGIN            
SET NOCOUNT ON;            
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;          
declare @ErrorMsg NVARCHAR(2048)        
      
BEGIN TRY               
               
--DELETE FROM [DBO].[CompanyRevenueSegmentationSplitMap]     
--WHERE [CompanyRevenueId] = @CompanyRevenueId  
--AND NOT EXISTS(SELECT 1 FROM @SegmentationList LIST   
--             WHERE LIST.Segmentation = [CompanyRevenueSegmentationSplitMap].[Segmentation])  
 declare @UnitId uniqueidentifier;

select @UnitId = UnitId
from CompanyRevenue  
where id = @CompanyRevenueId;

  DELETE FROM [DBO].[CompanyRevenueSegmentationSplitMap]     
WHERE [CompanyId] = @CompanyId    

      
INSERT INTO [dbo].[CompanyRevenueSegmentationSplitMap]([Id],    
  [CompanyRevenueId],    
  [CompanyId], 
  [Split],        
  [Segmentation],  
  [SegmentValue],  
  [Year],  
        [IsDeleted],            
        [UserCreatedById],            
        [CreatedOn])            
 SELECT NEWID(),     
 @CompanyRevenueId,   
 @CompanyId,  
 @Split,        
   Segmentation,  
   dbo.fun_UnitConversion(@UnitId,SegmentValue) as SegmentValue,
   @Year,  
   0,            
   @UserCreatedById,             
   GETDATE()      
   from @SegmentationList list      
   where not exists (select 1 from [CompanyRevenueSegmentationSplitMap]       
    where list.Segmentation = [CompanyRevenueSegmentationSplitMap].[Segmentation])      
      
SELECT StoredProcedureName ='SApp_insertupdateCompanyRevenueSegmentationSplit',Message =@ErrorMsg,Success = 1;             
          
END TRY            
BEGIN CATCH            
    -- Execute the error retrieval routine.            
 DECLARE @ErrorNumber int;            
 DECLARE @ErrorSeverity int;            
 DECLARE @ErrorProcedure varchar(100);            
 DECLARE @ErrorLine int;            
 DECLARE @ErrorMessage varchar(500);            
            
  SELECT @ErrorNumber = ERROR_NUMBER(),            
        @ErrorSeverity = ERROR_SEVERITY(),            
        @ErrorProcedure = ERROR_PROCEDURE(),            
        @ErrorLine = ERROR_LINE(),            
        @ErrorMessage = ERROR_MESSAGE()            
            
 insert into dbo.Errorlog(ErrorNumber,            
       ErrorSeverity,            
       ErrorProcedure,            
       ErrorLine,            
       ErrorMessage,            
       ErrorDate)            
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());            
          
 set @ErrorMsg = 'Error while creating a new CompanyRevenueSegmentationSplit, please contact Admin for more details.';            
        
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine          
    ,Message =@ErrorMsg,Success = 0;             
             
END CATCH            
            
            
end