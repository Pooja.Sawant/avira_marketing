﻿CREATE PROCEDURE [dbo].[SApp_ImpactTypeGetAll]
 AS      
/*================================================================================      
Procedure Name: SApp_ImpactTypeGetAll      
Author: Praveen      
Create date: 04/04/2019      
Description: Get list from Impact Type      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
04/04/2019 Praveen   Initial Version      
================================================================================*/      
      
BEGIN      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED      

SELECT Id, ImpactTypeName FROM ImpactType WHERE IsDeleted  = 0
      
 END