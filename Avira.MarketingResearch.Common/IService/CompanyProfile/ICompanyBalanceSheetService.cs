﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.CompanyProfile
{
    public interface ICompanyBalanceSheetService
    {
        IEnumerable<CompanyBalanceSheetViewModel> GetCompanyBalanceSheetData(Guid CompanyId, AviraUser user);
    }
}
