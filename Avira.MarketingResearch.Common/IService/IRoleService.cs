﻿using System;
using System.Collections.Generic;
using System.Text;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.Helpers;

namespace Avira.MarketingResearch.Common.IService
{
    public interface IRoleService
    {
        Result<SpTransactionMessage> Create(RoleInsertDbViewModel roleInsertDbViewModel, AviraUser currentUser);

        Result<SpTransactionMessage> Put(RoleUpdateViewModel roleInsertDbViewModel, AviraUser currentUser);

        Result<SpTransactionMessage> Update(RoleUpdateViewModel roleInsertDbViewModel, AviraUser currentUser);

        IEnumerable<RoleViewModel> GetAllPermissions();

        IEnumerable<RoleViewModel> Get(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false);

        IEnumerable<RoleEditViewModel> GetById(Guid guid);
    }
}
