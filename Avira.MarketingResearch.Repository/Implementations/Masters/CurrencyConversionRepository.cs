﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces.Masters;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.Masters
{
    public class CurrencyConversionRepository : ICurrencyConversionRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "Currency";
        public CurrencyConversionRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public Result<SpTransactionMessage> Create(CurrencyConversionInsertDbViewModel currencyConversionInsertDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(CurrencyQueries.StoreProcInsertUpdate, new
            {
                @Id = currencyConversionInsertDbViewModel.CurrencyConversionId,
                @CurrencyId = currencyConversionInsertDbViewModel.CurrencyId,
                @ConversionRangeType = currencyConversionInsertDbViewModel.ConversionRangeType,
                @ConversionRate = currencyConversionInsertDbViewModel.ConversionRate,
                @StartDate = currencyConversionInsertDbViewModel.StartDate,
                @EndDate = currencyConversionInsertDbViewModel.EndDate,
                @Year = currencyConversionInsertDbViewModel.Year,
                @Quarter = currencyConversionInsertDbViewModel.Quarter,
                @UserCreatedById = currencyConversionInsertDbViewModel.UserCreatedById
            }, CommandType.StoredProcedure);
            //var spTransactionMessage = _dbHelper.ExecuteQuery(CurrencyQueries.StoreProcInsertUpdate, currencyConversionInsertDbViewModel, CommandType.StoredProcedure);
            return spTransactionMessage;

        }

        private IEnumerable<CurrencyConversionViewModel> GetAllCurrency(Guid? guid, int PageStart = 0, int pageSize = 0, int SortDirection = -1, string OrdbyByColumnName = "", string Search = "", bool includeDeleted = false)
        {

            DataTable currencyIdsTable = new DataTable();
            currencyIdsTable.Columns.Add("ID", typeof(Guid));

            if (guid != null)
            {
                currencyIdsTable.Rows.Add(guid);
            }
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<CurrencyConversionViewModel>(CurrencyQueries.StoreProcSelectGetById
                            , new { @CurrencyIDList = currencyIdsTable.AsTableValuedParameter("dbo.udtUniqueIdentifier"), @includeDeleted = includeDeleted, @PageStart = PageStart, @pageSize = pageSize, @SortDirection = SortDirection, @OrdbyByColumnName = OrdbyByColumnName, @CurrencyName = Search }
                            , null, true, null, CommandType.StoredProcedure);
        }

        CurrencyConversionViewModel GetById(Guid guid)
        {
            CurrencyConversionViewModel currencyViewModel = new CurrencyConversionViewModel();
            currencyViewModel = GetAllCurrency(guid).FirstOrDefault();
            return currencyViewModel;
        }

        IEnumerable<CurrencyConversionInsertDbViewModel> ICurrencyConversionRepository.GetById(Guid guid)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<CurrencyConversionInsertDbViewModel>(CurrencyQueries.StoreProcGetById, new { Id = guid }, null, true, null,
                CommandType.StoredProcedure);
        }

        public IEnumerable<CurrencyConversionInsertDbViewModel> GetByIdAll(CurrencyConversionInsertDbViewModel cci)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<CurrencyConversionInsertDbViewModel>(CurrencyQueries.StoreProcSelectGrid, new
            {
                @CurrencyID = cci.CurrencyId,
                @ConversionRangeType = cci.ConversionRangeType,
                @StartDate = cci.StartDate,
                @EndDate = cci.EndDate,
                @Year = cci.Year,
                @Quarter = cci.Quarter,
                @includeDeleted = 0,
                @OrdbyByColumnName = cci.CurrencyName,
                @SortDirection = 1,
                @PageStart = 0,
                @PageSize = 10
            }, null, true, null,
                CommandType.StoredProcedure);
        }
    }
}
