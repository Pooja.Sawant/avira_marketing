﻿/*================================================================================
-- Author:  Laxmikant  
-- Create date: 04/15/2019  
-- Description: Insert /  Update Trend Notes 
Date  Developer  Reason          
__________ ____________ ______________________________________________________          
04/15/2019  Laxmikant   Initial Version  
-------------------------------------------------------------------------------
04/26/2019  Harshal  
   Added ModificationON and UserModifiedById in Update Part   
================================================================================*/      
  

CREATE procedure [dbo].[SApp_InsertTrendNotes]   
 -- Add the parameters for the stored procedure here  
 @Id uniqueidentifier,  
 @TrendId uniqueidentifier,     
 @TrendType NCHAR(512),  
 @AuthorRemark NVARCHAR(512) = null,  
 @ApproverRemark NVARCHAR(512) = null,  
 @TrendStatusName VARCHAr(512),    
 @AviraUserId uniqueidentifier   
AS   
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
 Declare @TrendNoteStatusID uniqueidentifier  
 --Declare @ExistingAuthorRemark NVARCHAR(512)  
 --Declare @ExistingApproverRemark NVARCHAR(512)  
 declare @ErrorMsg NVARCHAR(2048)    
  
BEGIN TRY       
 SELECT @TrendNoteStatusID =  Id FROM TrendStatus where TrendStatusName = @TrendStatusName  
 --SELECT @ExistingApproverRemark = ApproverRemark FROM Trend Where id =  @TrendId  
 --SELECT @ExistingAuthorRemark = AuthorRemark FROM Trend Where id =  @TrendId   
  
 if exists (select TrendId from [TrendNote] where TrendId = @TrendId and TrendType = @TrendType)  
  begin  
   update [dbo].[TrendNote]  
   set AuthorRemark =  @AuthorRemark, ApproverRemark = @ApproverRemark, TrendStatusID = @TrendNoteStatusID,ModifiedOn=GETDATE(),UserModifiedById=@AviraUserId WHERE TrendId = @TrendId and TrendType = @TrendType   
  end  
 else  
  begin  
   if NOT exists (select TrendId from [TrendNote] where TrendId = @TrendId and TrendType = @TrendType)  
    begin  
     INSERT INTO [dbo].[TrendNote]  
     (  
       [Id], [TrendId], [TrendType], [AuthorRemark], [ApproverRemark], [TrendStatusID], [CreatedOn],[UserCreatedById], IsDeleted  
     )      
     VALUES (@Id, @TrendId, @TrendType, @AuthorRemark, @ApproverRemark, @TrendNoteStatusID, getdate(), @AviraUserId, 0);   
    end  
  end  
  SELECT StoredProcedureName ='SApp_InsertTrendNotes',Message =@ErrorMsg,Success = 1;    
END TRY   
BEGIN CATCH        
    -- Execute the error retrieval routine.        
 DECLARE @ErrorNumber int;        
 DECLARE @ErrorSeverity int;        
 DECLARE @ErrorProcedure varchar(100);        
 DECLARE @ErrorLine int;        
 DECLARE @ErrorMessage varchar(500);        
        
  SELECT @ErrorNumber = ERROR_NUMBER(),        
        @ErrorSeverity = ERROR_SEVERITY(),        
        @ErrorProcedure = ERROR_PROCEDURE(),        
        @ErrorLine = ERROR_LINE(),        
        @ErrorMessage = ERROR_MESSAGE()        
        
 insert into dbo.Errorlog(ErrorNumber,        
       ErrorSeverity,        
       ErrorProcedure,        
       ErrorLine,        
       ErrorMessage,        
       ErrorDate)        
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());        
      
 set @ErrorMsg = 'Error while insert a new TrendMarketMap, please contact Admin for more details.';      
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine      
    ,Message =@ErrorMsg,Success = 0;         
         
END CATCH