﻿using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.CompanyProfile
{
    public interface IImpactDirectionService
    {
        IEnumerable<ImpactDirectionViewModel> GetAll();
    }
}
