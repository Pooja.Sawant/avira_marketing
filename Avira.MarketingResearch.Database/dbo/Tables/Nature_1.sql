﻿CREATE TABLE [dbo].[Nature] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [NatureTypeName]   NVARCHAR (256)   CONSTRAINT [DF_Nature_NatureTypeName] DEFAULT ('') NOT NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_Nature_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_Nature] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Nature_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_Nature_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id])
);

