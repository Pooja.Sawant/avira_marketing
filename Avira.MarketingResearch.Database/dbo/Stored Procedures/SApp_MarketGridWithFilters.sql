﻿CREATE PROCEDURE [dbo].[SApp_MarketGridWithFilters]
	(@MarketIDList dbo.udtUniqueIdentifier READONLY,
	@MarketName nvarchar(256)='',
	@Description nvarchar(256)='',
	@ParentId uniqueidentifier= null,
	@includeDeleted bit = 0,
	@OrdbyByColumnName NVARCHAR(50) ='CreatedDate',
	@SortDirection INT = -1,
	@PageStart INT = 0,
	@PageSize INT = Null)
	AS
/*================================================================================
Procedure Name:SApp_MarketGridWithFilters
Author: Pooja
Create date: 03/25/2019
Description: Get list from Market table by Name or ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
03/25/2019	Pooja			Initial Version
================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
if @includeDeleted is null set @includeDeleted = 0;
if len(@MarketName)>2 set @MarketName = '%'+ @MarketName + '%'
else set @MarketName = ''
if len(@Description)>2 set @Description = '%'+ @Description + '%';
else set @Description = '';
	if @PageSize is null or @PageSize =0
	 set @PageSize = 10  
	begin
	;WITH CTE_TBL AS (

	SELECT [Market].[Id],
		[Market].[MarketName],
		[Market].[Description],
		[Market].[ParentId],
		ParentMarket.[MarketName] as ParentMarketName,
		[Market].[IsDeleted],
		CASE
		WHEN @OrdbyByColumnName = 'MarketName' THEN [Market].MarketName
		WHEN @OrdbyByColumnName = 'Description' THEN [Market].[Description]
		WHEN @OrdbyByColumnName = 'ParentMarketName' THEN ParentMarket.[MarketName]
		
		ELSE [Market].[MarketName]
	END AS SortColumn

FROM [dbo].[Market]
LEFT JOIN [dbo].[Market] ParentMarket
on  [Market].ParentId = ParentMarket.Id
where (NOT EXISTS (SELECT * FROM @MarketIDList) or  [Market].Id  in (select * from @MarketIDList))
	AND (@MarketName = '' or [Market].[MarketName] like @MarketName)
	AND (@Description = '' or [Market].[Description] like @Description)
	AND (@ParentId is null or [Market].[ParentId]=@ParentId)
	AND (@includeDeleted = 1 or [Market].IsDeleted = 0)
	)

	SELECT 
		  CTE_TBL.*,
		  tCountResult.TotalItems
		FROM CTE_TBL
		CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult
		ORDER BY 
		  CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,
		  CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC 
		OFFSET @PageStart ROWS
		FETCH NEXT @PageSize ROWS ONLY;
		end
		

	END