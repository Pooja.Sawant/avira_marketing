﻿ 

/*================================================================================      
-- Author:  Laxmikant        
-- Create date: 06/19/2019        
-- Description: Insert /  Update QualitativeNote Notes       
Date  Developer  Reason                
__________ ____________ ______________________________________________________                
06/19/2019  Laxmikant   Initial Version       
================================================================================*/            
        
      
CREATE PROCEDURE [dbo].[SApp_InsertQualitativeNotes]         
 -- Add the parameters for the stored procedure here        
 @Id uniqueidentifier,        
 @QualitativeId uniqueidentifier,      
 @ProjectId uniqueidentifier=null,       
 @AuthorRemark NVARCHAR(512) = null,        
 @ApproverRemark NVARCHAR(512) = null,        
 @QualitativeStatusName VARCHAR(512),          
 @AviraUserId uniqueidentifier         
AS         
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
 SET NOCOUNT ON;        
        
 Declare @QualitativeNoteStatusID uniqueidentifier        
     
 declare @ErrorMsg NVARCHAR(2048)       
BEGIN TRY             
 SELECT @QualitativeNoteStatusID = (SELECT  Id FROM QualitativeStatus where QualitativeStatusName = @QualitativeStatusName  )      
     
        
      
  IF(@ProjectId IS NULL)      
  BEGIN      
      
 if exists (select QualitativeId from [QualitativeNote] where (QualitativeId = @QualitativeId)       
 --and (@ProjectId is null or ProjectId=@ProjectId)      
 )       
  begin        
   update [dbo].[QualitativeNote]        
   set AuthorRemark =  @AuthorRemark, ApproverRemark = @ApproverRemark, QualitativeStatusID = @QualitativeNoteStatusID,ModifiedOn=GETDATE(),UserModifiedById=@AviraUserId WHERE QualitativeId = @QualitativeId  
  end        
 else        
  begin        
   if NOT exists (select QualitativeId  from QualitativeNote where QualitativeId = @QualitativeId)        
    begin        
     INSERT INTO [dbo].[QualitativeNote]        
     (        
       [Id], [QualitativeId],[ProjectId], [AuthorRemark], [ApproverRemark], [QualitativeStatusID], [CreatedOn],[UserCreatedById], IsDeleted        
     )            
     VALUES (@Id, @QualitativeId,@ProjectId, @AuthorRemark, @ApproverRemark, @QualitativeNoteStatusID, getdate(), @AviraUserId, 0);         
    end        
  end        
      
  END      
  ELSE       
  BEGIN      
      
      
  if exists (select QualitativeId from [QualitativeNote] where (QualitativeId = @QualitativeId AND ProjectId = @ProjectId)       
     
 )       
  begin        
   update [dbo].[QualitativeNote]        
   set AuthorRemark =  @AuthorRemark, ApproverRemark = @ApproverRemark, QualitativeStatusID = @QualitativeNoteStatusID,ModifiedOn=GETDATE(),UserModifiedById=@AviraUserId       
   WHERE QualitativeId = @QualitativeId AND ProjectId = @ProjectId     
  end        
 else        
  begin        
   if NOT exists (select QualitativeId from [QualitativeNote] where QualitativeId = @QualitativeId AND ProjectId = @ProjectId)        
    begin        
     INSERT INTO [dbo].[QualitativeNote]        
     (        
       [Id], [QualitativeId],[ProjectId],  [AuthorRemark], [ApproverRemark], [QualitativeStatusID], [CreatedOn],[UserCreatedById], IsDeleted        
     )            
     VALUES (@Id, @QualitativeId,@ProjectId,  @AuthorRemark, @ApproverRemark, @QualitativeNoteStatusID, getdate(), @AviraUserId, 0);         
    end        
  end        
      
  END      
  SELECT StoredProcedureName ='SApp_InsertQualitativeNotes',Message =@ErrorMsg,Success = 1;       
END TRY         
BEGIN CATCH              
    -- Execute the error retrieval routine.              
 DECLARE @ErrorNumber int;              
 DECLARE @ErrorSeverity int;              
 DECLARE @ErrorProcedure varchar(50);              
 DECLARE @ErrorLine int;              
 DECLARE @ErrorMessage varchar(500);              
              
  SELECT @ErrorNumber = ERROR_NUMBER(),              
        @ErrorSeverity = ERROR_SEVERITY(),              
     @ErrorProcedure = ERROR_PROCEDURE(),              
        @ErrorLine = ERROR_LINE(),              
        @ErrorMessage = ERROR_MESSAGE()              
              
 insert into dbo.Errorlog(ErrorNumber,              
       ErrorSeverity,              
       ErrorProcedure,            
       ErrorLine,              
       ErrorMessage,              
       ErrorDate)              
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());              
            
 set @ErrorMsg = 'Error while insert a new Qualitative, please contact Admin for more details.';            
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine            
    ,Message =@ErrorMsg,Success = 0;               
               
END CATCH