﻿using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;

namespace Avira.MarketingResearch.Common.Service_Implementation
{
    public class RawMaterialService : IRawMaterialService
    {
        private readonly IRawMaterialRepository _rawMaterialRepository;

        public RawMaterialService(IRawMaterialRepository buildLevelRepository)
        {
            _rawMaterialRepository = buildLevelRepository;
        }

        public Result<SpTransactionMessage> Create(RawMaterialInsertDbViewModel tfc, AviraUser currentUser)
        {
            return _rawMaterialRepository.Create(tfc);
        }


        public IEnumerable<RawMaterialViewModel> GetAll(Guid Id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<RawMaterialViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false)
        {
            return _rawMaterialRepository.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName,Search, isDeletedInclude);
        }

        public RawMaterialViewModel GetById(Guid guid)
        {
            return _rawMaterialRepository.GetById(guid);
        }


        public Result<SpTransactionMessage> Update(RawMaterialUpdateDbViewModel tfc, AviraUser currenUser)
        {
            return _rawMaterialRepository.Update(tfc);
        }
    }
}
