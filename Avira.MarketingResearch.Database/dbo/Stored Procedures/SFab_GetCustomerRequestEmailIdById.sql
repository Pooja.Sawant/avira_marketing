﻿create procedure [dbo].[SFab_GetCustomerRequestEmailIdById]    
(@Id uniqueidentifier)    
as    
/*================================================================================    
Procedure Name: [SFab_GetCustomerRequestEmailIdById]    
Author: Pooja Sawant    
Create date: 08/31/2019    
Description: Get customer details from CustomerRequest table    
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
 08/31/2019 Pooja   Initial Version    
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    
    
	SELECT * from CustomerRequest where id=@Id

END