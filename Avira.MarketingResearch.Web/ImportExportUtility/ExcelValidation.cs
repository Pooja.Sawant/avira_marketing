﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Http;

namespace Avira.MarketingResearch.Web.ImportExportUtility
{
    public class ExcelValidation:BaseImportValidation
    {
        DataTable _dt;
        public readonly IServiceRepository _serviceRepository;
        private ILoggerManager _logger;
        object model;
        public ExcelValidation(IServiceRepository serviceRepository, ILoggerManager logger,DataTable dt, object ValideModule)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
            _dt = dt;
            model = ValideModule;
        }
        public override bool IsSatisfiedBy()
        {
            throw new NotImplementedException();
        }
        public override List<ImportCommonResponseModel> ValidateDataType()
        {
            List<ImportCommonResponseModel> responseErrorList = new List<ImportCommonResponseModel>();
            Type type = model.GetType();
            var properties = type.GetProperties();
            Dictionary<string, Type> TypeDct = new Dictionary<string, Type>();
            bool res;
            foreach (var pro in properties)
            {
                TypeDct.Add(pro.Name, pro.PropertyType);
            }
            for (int i = 0; i < _dt.Rows.Count; i++)
            {
                foreach(KeyValuePair<string, Type> colType in TypeDct)
                {
                    for(int j=0;j<_dt.Columns.Count;j++)
                    {
                        if(_dt.Columns[j].ColumnName== colType.Key)
                        {
                            switch (colType.Value.ToString())
                            {
                                case "string":
                                case "String":
                                    break;
                                case "int":
                                    int outInt;
                                    res = int.TryParse(_dt.Rows[i][j].ToString(),out outInt);
                                    if(!res)
                                    {
                                        ImportCommonResponseModel respInt = new ImportCommonResponseModel();
                                        respInt.ErrorNotes = "Column Name" + colType.Key + ": Invalide integer Format";
                                        respInt.RowNo = i;
                                        respInt.Success = "false";
                                        responseErrorList.Add(respInt);
                                    }
                                    break;
                                case "Int16":
                                    Int16 outInt16;
                                    res = Int16.TryParse(_dt.Rows[i][j].ToString(), out outInt16);
                                    if (!res)
                                    {
                                        ImportCommonResponseModel respInt16 = new ImportCommonResponseModel();
                                        respInt16.ErrorNotes = "Column Name" + colType.Key + ": Invalide integer Format";
                                        respInt16.RowNo = i;
                                        respInt16.Success = "false";
                                        responseErrorList.Add(respInt16);
                                    }
                                    break;
                                case "Int32":
                                    Int32 outInt32;
                                    res = Int32.TryParse(_dt.Rows[i][j].ToString(), out outInt32);
                                    if (!res)
                                    {
                                        ImportCommonResponseModel respInt32 = new ImportCommonResponseModel();
                                        respInt32.ErrorNotes = "Column Name" + colType.Key + ": Invalide integer Format";
                                        respInt32.RowNo = i;
                                        respInt32.Success = "false";
                                        responseErrorList.Add(respInt32);
                                    }
                                    break;
                                case "Int64":
                                    Int64 outInt64;
                                    res = Int64.TryParse(_dt.Rows[i][j].ToString(), out outInt64);
                                    if (!res)
                                    {
                                        ImportCommonResponseModel respInt64 = new ImportCommonResponseModel();
                                        respInt64.ErrorNotes = "Column Name" + colType.Key + ": Invalide integer Format";
                                        respInt64.RowNo = i;
                                        respInt64.Success = "false";
                                        responseErrorList.Add(respInt64);
                                    }
                                    break;
                                case "double":
                                case "Double":
                                    double outDouble;
                                    res = double.TryParse(_dt.Rows[i][j].ToString(), out outDouble);
                                    if (!res)
                                    {
                                        ImportCommonResponseModel respDouble = new ImportCommonResponseModel();
                                        respDouble.ErrorNotes = "Column Name" + colType.Key + ": Invalide Double Format";
                                        respDouble.RowNo = i;
                                        respDouble.Success = "false";
                                        responseErrorList.Add(respDouble);
                                    }
                                    break;
                                case "decimal":
                                case "Decimal":
                                    decimal outDecimal;
                                    res = decimal.TryParse(_dt.Rows[i][j].ToString(), out outDecimal);
                                    if (!res)
                                    {
                                        ImportCommonResponseModel respDecimal = new ImportCommonResponseModel();
                                        respDecimal.ErrorNotes = "Column Name" + colType.Key + ": Invalide Decimal Format";
                                        respDecimal.RowNo = i;
                                        respDecimal.Success = "false";
                                        responseErrorList.Add(respDecimal);
                                    }
                                    break;
                                case "DateTime":
                                    DateTime outDate;
                                    res = DateTime.TryParse(_dt.Rows[i][j].ToString(), out outDate);
                                    if (!res)
                                    {
                                        ImportCommonResponseModel respDate = new ImportCommonResponseModel();
                                        respDate.ErrorNotes = "Column Name" + colType.Key + ": Invalide Datatime Format";
                                        respDate.RowNo = i;
                                        respDate.Success = "false";
                                        responseErrorList.Add(respDate);
                                    }
                                    break;
                                default:
                                    ImportCommonResponseModel resp = new ImportCommonResponseModel();
                                    resp.ErrorNotes = "Column Name" + colType.Key + ": Invalide DataType Format";
                                    resp.RowNo = i;
                                    resp.Success = "false";
                                    responseErrorList.Add(resp);
                                    break;
                            }
                        }
                    }
                }
            }

            return responseErrorList;
        }

        public override List<ImportCommonResponseModel> ValidateDataLenth()
        {
            List<ImportCommonResponseModel> responseErrorList = new List<ImportCommonResponseModel>();
            SchemaModel inputModel = new SchemaModel();
            inputModel.SchemaType = "Columns";
            inputModel.TableName = "AviraUser";
            inputModel.DataBaseName = "Avira";
            HttpResponseMessage response = _serviceRepository.PostResponse("SqlTableSchema", inputModel);
            response.EnsureSuccessStatusCode();
            DataTable schemaDT = response.Content.ReadAsAsync<DataTable>().Result;
            if (schemaDT.Rows.Count > 0)
            {
                for (int i = 0; i < schemaDT.Rows.Count; i++)
                {
                    for (int j = 0; j < _dt.Columns.Count; j++)
                    {
                        if (schemaDT.Rows[i].ItemArray[3].ToString() == _dt.Columns[j].ColumnName)
                        {
                            switch (schemaDT.Rows[i].ItemArray[3].ToString())
                            {
                                case "nvarchar":
                                case "varchar":
                                case "char":
                                    int maxlength= Convert.ToInt32(schemaDT.Rows[i].ItemArray[8]);
                                    for (int k = 0; k < _dt.Rows.Count; k++)
                                    {
                                        if(_dt.Rows[k][j].ToString().Length> maxlength)
                                        {
                                            ImportCommonResponseModel resp = new ImportCommonResponseModel();
                                            resp.ErrorNotes ="Only "+ maxlength+" characters allowed";
                                            resp.RowNo = k;
                                            resp.Success = "false";
                                            responseErrorList.Add(resp);
                                        }
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
            return responseErrorList;
        }

        public override List<ImportCommonResponseModel> ValidateColumnExits()
        {
            List<ImportCommonResponseModel> responseErrorList = new List<ImportCommonResponseModel>();
            Type type = model.GetType();
            var properties = type.GetProperties();
            var columnNames = _dt.Columns.Cast<DataColumn>().Select(c => c.ColumnName.ToLower()).ToList();
            foreach (var pro in properties)
            {
                if (columnNames.Contains(pro.Name.ToLower()))
                {
                    
                }
                else
                {
                    ImportCommonResponseModel resp = new ImportCommonResponseModel();
                    resp.ErrorNotes = "Column Name" + pro.Name + "is missing";
                    resp.RowNo = 6;
                    resp.Success = "false";
                    responseErrorList.Add(resp);
                }
            }

            return responseErrorList;
        }
    }
}
