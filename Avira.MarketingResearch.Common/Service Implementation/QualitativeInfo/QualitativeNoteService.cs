﻿using Avira.MarketingResearch.Common.IService.QualitativeInfo;
using Avira.MarketingResearch.Model.ViewModels.QualitativeInfo;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.QualitativeInfo;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.QualitativeInfo
{
    public class QualitativeNoteService : IQualitativeNoteService
    {
        private readonly IQualitativeNoteRepository _iqualitativeNoteRepository;

        public QualitativeNoteService(IQualitativeNoteRepository iqualitativeNoteRepository)
        {
            _iqualitativeNoteRepository = iqualitativeNoteRepository;
        }

        public QualitativeNoteViewModel GetById(Guid Id, Guid ProjectId)
        {
            return _iqualitativeNoteRepository.GetById(Id, ProjectId);
        }
    }
}
