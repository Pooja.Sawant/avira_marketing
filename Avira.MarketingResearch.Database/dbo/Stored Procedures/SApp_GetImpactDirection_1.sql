﻿      
      
CREATE PROCEDURE [dbo].[SApp_GetImpactDirection]      
 (      
  @includeDeleted bit = 0      
 )      
 AS      
/*================================================================================      
Procedure Name: [SApp_GetImpactDirection]      
Author: Gopi      
Create date: 05/03/2019      
Description: Get list of ImpactDirection from table       
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
05/03/2019 Gopi   Initial Version      
================================================================================*/      
      
BEGIN      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED      
      
 SELECT [ImpactDirection].[Id],      
  [ImpactDirection].[ImpactDirectionName],      
  [ImpactDirection].[Description],      
  [ImpactDirection].[IsDeleted]      
FROM [dbo].[ImpactDirection]      
where ([ImpactDirection].IsDeleted = 0)       
 END