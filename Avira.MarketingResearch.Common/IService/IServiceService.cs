﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService
{
    public interface IServiceService
    {
        IEnumerable<ServiceViewModel> GetAll(Guid Id);
        IEnumerable<ServiceViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false);
        ServiceViewModel GetById(Guid guid);
        IEnumerable<ServiceViewModel> GetAllParentService();

        Result<SpTransactionMessage> Create(ServiceInsertDbViewModel tfc, AviraUser currentUser);
        Result<SpTransactionMessage> Update(ServiceUpdateDbViewModel tfc, AviraUser currenUser);
    }
}
