﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.ImportExport;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyBulkImportExportController : AviraBaseController
    {
        private ILoggerManager _logger;
        public readonly ICompanyBulkImportExportService _companyBulkImportExportService;

        public CompanyBulkImportExportController(ICompanyBulkImportExportService companyBulkImportExportService, ILoggerManager logger)
        {
            _companyBulkImportExportService = companyBulkImportExportService;
            _logger = logger;
        }

        [HttpPost]
        public ActionResult Post([FromBody] CompanyBulkImportViewModel viewModel)
        {
            viewModel.UserCreatedById = CurrentUser.Id;
            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new CPFundamentals, please contact Admin for more details."
            };
            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                var result = _companyBulkImportExportService.Create(viewModel, CurrentUser);

                var res = result.Where(s => s.ErrorNotes != null).ToList();

                if (res.Count <= 0)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Company Bulk imported successfully.";
                }
                else
                {
                    viewData.Message = "Company Bulk Import has some errors";
                    //Export to Excel data.
                    //foreach (var item in result.ToList())
                    //{
                    //    item.ErrorNotes = item.ErrorNotes.Replace(",", " ");
                    //}
                    viewData.cpData = result.ToList();

                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);

                viewData = new ViewData()
                {
                    IsSuccess = false,
                    Message = "Error while creating a new Company Bulk Import. Error Details: " + ex.Message + ". please contact Admin for more details."
                };
            }
            return Ok(viewData);
        }
    }
}