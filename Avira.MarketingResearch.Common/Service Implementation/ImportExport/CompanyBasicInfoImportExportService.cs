﻿using Avira.MarketingResearch.Common.IService.ImportExport;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.ImportExport
{
    public class CompanyBasicInfoImportExportService : ICompanyBasicInfoImportExportService
    {
        private readonly ICompanyBasicInfoImportExportRepository _companyBasicInfoImportExportRepository;

        public CompanyBasicInfoImportExportService(ICompanyBasicInfoImportExportRepository companyBasicInfoImportExportRepository)
        {
            _companyBasicInfoImportExportRepository = companyBasicInfoImportExportRepository;
        }

        public IEnumerable<ImportCommonResponseModel> Create(CompanyBasicInfoImportInsertModel viewModel, AviraUser currentUser)
        {
            viewModel.JsonFundamentals = JsonConvert.SerializeObject(viewModel.CompanyBasicInfoList);
            viewModel.JsonRevenue= JsonConvert.SerializeObject(viewModel.YearWiseRevenueList);
            IEnumerable<ImportCommonResponseModel> trendImportResponse = _companyBasicInfoImportExportRepository.Create(viewModel);
            return trendImportResponse;
        }
    }
}
