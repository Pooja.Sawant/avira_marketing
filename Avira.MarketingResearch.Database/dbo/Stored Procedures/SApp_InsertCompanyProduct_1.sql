﻿

CREATE PROCEDURE [dbo].[SApp_InsertCompanyProduct]
(@Id	UNIQUEIDENTIFIER=null,
@ProductName	NVARCHAR(256),
@Description	NVARCHAR(512)=null,
@Patents	    NVARCHAR(512)=null,
@LaunchDate		DATETIME=null,
@ImageURL	NVARCHAR(256)=null,
@ImageDisplayName	NVARCHAR(256)=null,
@ActualImageName	NVARCHAR(256)=null,
@StatusId	UNIQUEIDENTIFIER=null,
@CompanyId	UNIQUEIDENTIFIER=null,
@ProjectId	UNIQUEIDENTIFIER=null,
@Amount decimal(18,2)=null,
@CurrencyId UNIQUEIDENTIFIER=null,
@ValueConversionId UNIQUEIDENTIFIER=null,
@UserCreatedById UNIQUEIDENTIFIER
)
as
/*================================================================================
Procedure Name: SApp_InsertCompanyProduct
Author: Harshal
Create date: 05/02/2019
Description: Insert or updare a record in CampanyProduct table
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
05/02/2019	Harshal			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ErrorMsg NVARCHAR(2048);

IF exists(select 1 from [dbo].[CompanyProduct] 
		 where ((@Id is null or [Id] !=  @Id)
		 and  productName = @ProductName AND IsDeleted = 0 and CompanyId=@CompanyId))
begin
set @ErrorMsg = 'CompanyProduct with Name "'+ @ProductName + '" already exists.';
SELECT StoredProcedureName ='SApp_InsertCompanyProduct',Message =@ErrorMsg,Success = 0;  
RETURN;  
end
--End of Validations
BEGIN TRY

INSERT INTO [dbo].[CompanyProduct]
           ([ID]
           ,[ProductName]
           ,[LaunchDate]
           ,[Description]
           ,[Patents]
           ,[StatusId]
           ,[ImageURL]
           ,[ImageDisplayName]
		   ,[ActualImageName]
           ,[CompanyId]
		   ,[ProjectId]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
	values(@Id, 
			@ProductName,
			@LaunchDate, 
			@Description, 
			@Patents,
			@StatusId,
			@ImageURL,
			@ImageDisplayName,
			@ActualImageName,
			@CompanyId,
			@ProjectId,
			GETDATE(), --as [CreatedOn]
			@UserCreatedById, 
			0); --as [IsDeleted]


INSERT INTO [dbo].[CompanyProductValue]
(  [Id]
  ,[CompanyProductId]
  ,[Year]
  ,[CurrencyId]
  ,[ValueConversionId]
  ,[Amount])
  values(NEWID(),
         @Id,
		 YEAR(GETDATE()),
		 @CurrencyId,
		 @ValueConversionId,
		 dbo.fun_UnitConversion(@ValueConversionId, @Amount));


SELECT StoredProcedureName ='SApp_InsertCompanyProduct',Message =@ErrorMsg,Success = 1;   

  

END TRY
BEGIN CATCH
    -- Execute the error retrieval routine.
	DECLARE @ErrorNumber	int;
	DECLARE @ErrorSeverity	int;
	DECLARE @ErrorProcedure	varchar(100);
	DECLARE @ErrorLine	int;
	DECLARE @ErrorMessage	varchar(500);

	 SELECT @ErrorNumber = ERROR_NUMBER(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorProcedure = ERROR_PROCEDURE(),
        @ErrorLine = ERROR_LINE(),
        @ErrorMessage = ERROR_MESSAGE()

	insert into dbo.Errorlog(ErrorNumber,
							ErrorSeverity,
							ErrorProcedure,
							ErrorLine,
							ErrorMessage,
							ErrorDate)
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());

 set @ErrorMsg = 'Error while creating a new ProductCampany, please contact Admin for more details.';    
--THROW 50000,@ErrorMsg,1;      
--return(1)  
SELECT Number = @ErrorNumber, 
		Severity =@ErrorSeverity,
		StoredProcedureName =@ErrorProcedure,
		LineNumber= @ErrorLine,
		Message =@ErrorMsg,
		Success = 0;     
END CATCH
end