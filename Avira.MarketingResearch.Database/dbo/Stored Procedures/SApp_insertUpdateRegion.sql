﻿ 
CREATE procedure [dbo].[SApp_insertUpdateRegion]    
(    
 @RegionId uniqueidentifier,    
 @RegionName nvarchar(256),      
 @RegionLevel int, 
 @CountryIDList DBO.udtUniqueIdentifier READONLY,   
 @ParentRegionId uniqueidentifier,      
 @IsDeleted bit,      
 @AviraUserId uniqueidentifier    
)      
as    
/*================================================================================      
Procedure Name: SApp_insertUpdateRegion      
Author: Laxmikant     
Create date: 04/03/2019      
Description: Add Edit Region     
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
04/03/2019 Laxmikant   Initial Version      
================================================================================*/    
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
      
IF exists(select 1 from [dbo].[Region]       
   where ((@RegionId is null or [Id] !=  @RegionId)      
   and  RegionName = @RegionName AND IsDeleted = 0))      
begin      
declare @ErrorMsg NVARCHAR(2048);      
set @ErrorMsg = 'Region with Name "'+ @RegionName + '" already exists.';      
THROW 50000,@ErrorMsg,1;      
end      
      
IF (@RegionId is null)      
begin      
--Insert new record      
 set @RegionId = NEWID();    
      
 insert into 
		[dbo].[Region]([Id],      
        [RegionName],      
        [RegionLevel],    
		[ParentRegionId],     
        [IsDeleted],      
        [UserCreatedById],      
        [CreatedOn])      
 values
	(	@RegionId,       
		@RegionName,       
		@RegionLevel,    
		@ParentRegionId,       
		0,      
		@AviraUserId,       
		getdate()
	); 
	
	if  @@ROWCOUNT > 0 
	begin 
	  Insert into [dbo].[CountryRegionMap]
	  ([Id],CountryId,RegionId, [IsDeleted],[UserCreatedById], [CreatedOn]) 
	  select NEWID(),id, @RegionId,0, @AviraUserId, getdate()
	  FROM @CountryIDList;

	end     
end      
else      
begin 
update 
	[dbo].[Region]      
 set 
	[RegionName] = @RegionName,      
	[RegionLevel] = @RegionLevel,    
	[ParentRegionId] = @ParentRegionId,      
	[ModifiedOn] = getdate(),      
	DeletedOn = case when IsDeleted = 0 and @IsDeleted = 1 then getdate() else DeletedOn end,      
	UserDeletedById = case when IsDeleted = 0 and @IsDeleted = 1 then @AviraUserId else UserDeletedById end,      
	IsDeleted = isnull(@IsDeleted, IsDeleted),      
	[UserModifiedById] = @AviraUserId      
    where ID = @RegionId ;
	
	--Refresh country list      
--Mark all the countries which are not in list as deleted      
UPDATE lkup      
SET IsDeleted = 1,      
 DeletedOn = CASE WHEN IsDeleted = 0 THEN getdate() ELSE DeletedOn END,          
 UserDeletedById = CASE WHEN IsDeleted = 0 THEN @AviraUserId ELSE UserDeletedById END         
FROM [dbo].[CountryRegionMap] lkup      
WHERE lkup.RegionId = @RegionId      
AND NOT EXISTS(SELECT 1 FROM @CountryIDList list      
    WHERE list.id = lkup.CountryId)  
	
	
--Mark all countries which are in list as not deleted---

UPDATE lkup      
SET IsDeleted = 0,      
 DeletedOn = CASE WHEN IsDeleted = 1 THEN getdate() ELSE DeletedOn END,          
 UserDeletedById = CASE WHEN IsDeleted = 1 THEN @AviraUserId ELSE UserDeletedById END         
FROM [dbo].[CountryRegionMap] lkup      
WHERE IsDeleted = 1 AND lkup.RegionId = @RegionId      
AND EXISTS(SELECT 1 FROM @CountryIDList list      
    WHERE list.id = lkup.CountryId)  

---------------------------------------------------------
      
--Add newly added list if not exists in mapping      
 INSERT INTO [dbo].[CountryRegionMap]      
  ([Id],      
  CountryId,      
  RegionId,       
  [IsDeleted],      
  [UserCreatedById],       
  [CreatedOn])       
   SELECT NEWID() as ID,      
   list.ID as CountryId,       
   @RegionId as RegionId,      
   0 as [IsDeleted],       
   @AviraUserId as [UserCreatedById],       
   getdate() as [CreatedOn]      
   FROM @CountryIDList list      
   WHERE NOT EXISTS (SELECT 1 FROM [dbo].[CountryRegionMap] lkup(NOLOCK)      
      WHERE list.id = lkup.CountryId      
      AND lkup.RegionId = @RegionId);      
      
	  
end      
      
select @RegionId;      
end