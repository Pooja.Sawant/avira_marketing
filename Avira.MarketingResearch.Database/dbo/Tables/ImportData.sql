﻿CREATE TABLE [dbo].[ImportData] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [TemplateName]     NVARCHAR (200)   NULL,
    [ImportFileName]   NVARCHAR (200)   NULL,
    [ImportStatusId]   UNIQUEIDENTIFIER NULL,
    [ImportException]  NVARCHAR (MAX)   NULL,
    [ImportedOn]       DATETIME2 (7)    NULL,
    [UserImportedById] UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL
);

