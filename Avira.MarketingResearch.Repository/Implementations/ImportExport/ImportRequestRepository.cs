﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.ImportExport
{
    public class ImportRequestRepository : IImportRequestRepository
    {
        protected IDbHelper _dbHelper;
      
        public ImportRequestRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public IEnumerable<ImportRequestViewModel> GetAll(ImportRequestViewModel importRequestViewModel)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<ImportRequestViewModel>(ImportRequestQueries.StoreProcSelectAll
                            , new { @UserId=importRequestViewModel.UserImportedById,
                                    @ImportStatusName = importRequestViewModel.StatusName,
                                    @TemplateName = importRequestViewModel.TemplateName
                            }
                            , null, true, null, CommandType.StoredProcedure);
        }

        public ImportRequestViewModel GetRequest(Guid Id)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<ImportRequestViewModel>(ImportRequestQueries.StoreProcSelectRecord
                            , new
                            {
                                @Id=Id
                            }
                            , null, true, null, CommandType.StoredProcedure).FirstOrDefault();
        }

        public Result<SpTransactionMessage> Create(ImportRequestViewModel importRequestViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(ImportRequestQueries.StoreProcInsert, 
                                          new {
                                              @TemplateName= importRequestViewModel.TemplateName,
                                              @FileName= importRequestViewModel.FileName,
                                              @ImportFileName = importRequestViewModel.ImportFileName,
                                              @ImportFilePath= importRequestViewModel.ImportFilePath,
                                              @ImportStatusName= importRequestViewModel.StatusName,
                                              @ImportException= importRequestViewModel.ImportException,
                                              @ImportExceptionFilePath= importRequestViewModel.ImportExceptionFilePath,
                                              @ImportedOn = importRequestViewModel.ImportedOn,
                                              @UserImportedById = importRequestViewModel.UserImportedById
                                          }, CommandType.StoredProcedure);
            return spTransactionMessage;

        }

        public Result<SpTransactionMessage> Update(ImportRequestViewModel importRequestViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(ImportRequestQueries.StoreProcUpdate,
               new
               {
                   @Id= importRequestViewModel.Id,
                   @ImportStatusName = importRequestViewModel.StatusName,
                   @ImportException = importRequestViewModel.ImportException,
                   @ImportExceptionFilePath = importRequestViewModel.ImportExceptionFilePath,
                   //@ModifiedOn = importRequestViewModel.ModifiedOn,
                   @UserModifiedById = importRequestViewModel.UserModifiedById
               }, 
               CommandType.StoredProcedure);
            return spTransactionMessage;
        }

        public IEnumerable<ImportRequestViewModel> GetImportRequest(string TemplateName, string StatusName)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<ImportRequestViewModel>(ImportRequestQueries.StoreProcGetImportRequest, new
            {
                @TemplateName = TemplateName,
                @StatusName = StatusName

            }, null, true, null,
                CommandType.StoredProcedure);
        }

    }
}
