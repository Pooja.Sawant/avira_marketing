﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyAnalystViewController : AviraBaseController
    {
        private readonly ICompanyAnalystViewService _companyAnalystViewService;
        private ILoggerManager _logger;

        public CompanyAnalystViewController(
            ICompanyAnalystViewService companyAnalystViewService,
             ILoggerManager logger)
        {
            _companyAnalystViewService = companyAnalystViewService;
            _logger = logger;

        }

        [HttpGet]
        public ActionResult <CompanyAnalystViewModel> Get(Guid companyId, Guid projectId)
        {
            CompanyAnalystViewModel res = null;
            try
            {
                res = _companyAnalystViewService.GetById(companyId, projectId);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }


        [HttpPost]
        public ActionResult Post([FromBody] MainCompanyAnalystViewModel viewModel)
        {
            viewModel.CreatedOn = System.DateTime.UtcNow;
            viewModel.UserCreatedById = CurrentUser.Id;

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while Adding , please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                var result = _companyAnalystViewService.InsertUpdate(viewModel, CurrentUser);
                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Company Analyst View is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }

    }
}