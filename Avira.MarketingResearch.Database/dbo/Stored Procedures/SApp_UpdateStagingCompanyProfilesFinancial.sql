﻿CREATE procedure [dbo].[SApp_UpdateStagingCompanyProfilesFinancial]                  
                  
as                  
/*================================================================================                  
Procedure Name: SApp_UpdateStagingCompanyProfilesFinancial                 
Author: Gopi                  
Create date: 07/25/2019                  
Description: to update CompanyId,CurrencyId,UnitsId                 
Change History                  
Date  Developer  Reason                  
__________ ____________ ______________________________________________________                  
07/25/2019 Gopi   Initial Version                  
================================================================================*/                  
BEGIN                  
SET NOCOUNT ON;                  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;                  
declare @ErrorMsg NVARCHAR(2048)                    
                  
BEGIN TRY                   
                  
 ;With cteCompany as (                  
 SELECT C.Id as CompanyId,  
     CASE  
  WHEN C.Id is null THEN 'No Company'  
  ELSE NULL  
  END AS CompanyIdError,  
  SCPF.Id AS StaggingId  
 FROM StagingCompanyProfilesFinancial SCPF  
 LEFT JOIN Company C  
 ON C.CompanyName like SCPF.CompanyName+'%'             
  ),  
  cteCompanyExist as (                  
  SELECT SCPF.Id AS StaggingId,
  CASE 
  WHEN SCPF.CompanyName IS NULL THEN 'Please provide CompanyName' 
  ELSE NULL
  END AS CompanyNameError
  FROM StagingCompanyProfilesFinancial SCPF        
  ),   
  cteCurrency AS(            
   SELECT CY.Id as CurrencyId,            
  CASE             
   WHEN CY.Id is null THEN 'No Currency'              
     ELSE Null                  
     END AS CurrencyIdError,            
     SCPF.Id AS StaggingId                  
    FROM  StagingCompanyProfilesFinancial SCPF                  
   LEFT JOIN Currency CY                  
   ON CY.CurrencyCode = SCPF.Currency              
   ), 
   cteCurrencyExist as (                  
  SELECT SCPF.Id AS StaggingId,
  CASE 
  WHEN SCPF.Currency IS NULL THEN 'Please provide Currency' 
  ELSE NULL
  END AS CurrencyNameError
  FROM StagingCompanyProfilesFinancial SCPF        
  ),    
   cteVal AS(            
  SELECT VC.Id as ValueConversionId,            
  CASE             
   WHEN VC.Id is null THEN 'No ValueConversion'              
     ELSE Null                  
     END AS ValueConversionIdError,            
     SCPF.Id AS StaggingId                  
    FROM  StagingCompanyProfilesFinancial SCPF                     
   LEFT JOIN ValueConversion VC                  
   ON VC.ValueName = SCPF.Units              
   ),
    cteUnitsExist as (                  
  SELECT SCPF.Id AS StaggingId,
  CASE 
  WHEN SCPF.Units IS NULL THEN 'Please provide Units' 
  ELSE NULL
  END AS UnitsError
  FROM StagingCompanyProfilesFinancial SCPF        
  )                
                  
  update StagingCompanyProfilesFinancial                   
  set CompanyId = cteCompany.CompanyId,             
      CurrencyId = cteCurrency.CurrencyId,        
   ValueConversionId = cteVal.ValueConversionId,                  
   ErrorNotes = CONCAT_WS(', ', cteCompany.CompanyIdError, cteCompanyExist.CompanyNameError, cteCurrency.CurrencyIdError, cteCurrencyExist.CurrencyNameError, cteVal.ValueConversionIdError, cteUnitsExist.UnitsError)           
  From cteCompany                  
  Inner join StagingCompanyProfilesFinancial SCPF                  
  On cteCompany.StaggingId = SCPF.Id                  
  Inner Join cteCurrency                   
  On cteCurrency.StaggingId = SCPF.Id                  
  Inner Join cteVal                   
  On cteVal.StaggingId = SCPF.Id 
  Inner Join cteCompanyExist
  On cteCompanyExist.StaggingId = SCPF.Id  
  Inner Join cteCurrencyExist
  On cteCurrencyExist.StaggingId = SCPF.Id           
  Inner Join cteUnitsExist
  On cteUnitsExist.StaggingId = SCPF.Id 

  ;With cteComp as (                  
 SELECT C.Id as CompanyId,  
     CASE  
  WHEN C.Id is null THEN 'No Company'  
  ELSE NULL  
  END AS CompanyIdError,  
  SCSI.Id AS StaggingId  
 FROM StagingCompanySegmentInformation SCSI  
 LEFT JOIN Company C  
 ON C.CompanyName like SCSI.CompanyName+'%'             
  ),  
  cteCompanyExist as (                  
  SELECT SCSI.Id AS StaggingId,
  CASE 
  WHEN SCSI.CompanyName IS NULL THEN 'Please provide CompanyName' 
  ELSE NULL
  END AS CompanyNameError
  FROM StagingCompanySegmentInformation SCSI        
  ),   
  cteCurrency AS(            
   SELECT CY.Id as CurrencyId,            
  CASE             
   WHEN CY.Id is null THEN 'No Currency'              
     ELSE Null                  
     END AS CurrencyIdError,            
     SCSI.Id AS StaggingId                  
    FROM  StagingCompanySegmentInformation SCSI                  
   LEFT JOIN Currency CY                  
   ON CY.CurrencyCode = SCSI.Currency              
   ), 
   cteCurrencyExist as (                  
  SELECT SCSI.Id AS StaggingId,
  CASE 
  WHEN SCSI.Currency IS NULL THEN 'Please provide Currency' 
  ELSE NULL
  END AS CurrencyNameError
  FROM StagingCompanySegmentInformation SCSI         
  ),    
   cteVal AS(            
  SELECT VC.Id as ValueConversionId,            
  CASE             
   WHEN VC.Id is null THEN 'No ValueConversion'              
     ELSE Null                  
     END AS ValueConversionIdError,            
     SCSI.Id AS StaggingId                  
    FROM  StagingCompanySegmentInformation SCSI                      
   LEFT JOIN ValueConversion VC                  
   ON VC.ValueName = SCSI.Units              
   ),
    cteUnitsExist as (                  
  SELECT SCSI.Id AS StaggingId,
  CASE 
  WHEN SCSI.Units IS NULL THEN 'Please provide Units' 
  ELSE NULL
  END AS UnitsError
  FROM StagingCompanySegmentInformation SCSI        
  ) 
  update StagingCompanySegmentInformation                   
  set CompanyId = cteComp.CompanyId,             
      CurrencyId = cteCurrency.CurrencyId,        
   ValueConversionId = cteVal.ValueConversionId,                  
   ErrorNotes = CONCAT_WS(', ', cteComp.CompanyIdError, cteCompanyExist.CompanyNameError, cteCurrency.CurrencyIdError, cteCurrencyExist.CurrencyNameError, cteVal.ValueConversionIdError, cteUnitsExist.UnitsError)           
  From cteComp                  
  Inner join StagingCompanySegmentInformation SCS                  
  On cteComp.StaggingId = SCS.Id                  
  Inner Join cteCurrency                   
  On cteCurrency.StaggingId = SCS.Id                  
  Inner Join cteVal                   
  On cteVal.StaggingId = SCS.Id 
  Inner Join cteCompanyExist
  On cteCompanyExist.StaggingId = SCS.Id  
  Inner Join cteCurrencyExist
  On cteCurrencyExist.StaggingId = SCS.Id           
  Inner Join cteUnitsExist
  On cteUnitsExist.StaggingId = SCS.Id

                 
Declare @Notes int;              
set @Notes = (Select COUNT(*) from StagingCompanyProfilesFinancial where ErrorNotes != '')              
IF(@Notes > 0)              
BEGIN  
           
SELECT DISTINCT * FROM (
	--Select RowNo, 'CP_Financial_Header' as Module, ErrorNotes, Success = 1 from StagingCompanySegmentInformation  WHERE ErrorNotes != '' 
	--UNION ALL
   Select  RowNo, 'CP_Financial' as Module, ErrorNotes, Success = 1 from StagingCompanyProfilesFinancial  WHERE ErrorNotes != '' 
   ) res
   ORDER BY RowNo; 
                
   Delete from ImportData where Id in (select Distinct ImportId from StagingCompanyProfilesFinancial SCPF);              
   DELETE from StagingCompanyProfilesFinancial;
   DELETE FROM StagingCompanySegmentInformation;               
END              
ELSE          
BEGIN              
    exec SApp_InsertCompanyProfilesFinancial;          
    Delete from ImportData where Id in (select Distinct ImportId from StagingCompanyProfilesFinancial SCPF);              
    DELETE from StagingCompanyProfilesFinancial;  
    DELETE FROM StagingCompanySegmentInformation;             
END 


         
END TRY                        
BEGIN CATCH                        
    -- Execute the error retrieval routine.                        
 DECLARE @ErrorNumber int;                        
 DECLARE @ErrorSeverity int;                        
 DECLARE @ErrorProcedure varchar(100);                        
 DECLARE @ErrorLine int;                
 DECLARE @ErrorMessage varchar(500);                        
                        
  SELECT @ErrorNumber = ERROR_NUMBER(),                        
        @ErrorSeverity = ERROR_SEVERITY(),                        
        @ErrorProcedure = ERROR_PROCEDURE(),                        
        @ErrorLine = ERROR_LINE(),                        
        @ErrorMessage = ERROR_MESSAGE()                        
                        
 insert into dbo.Errorlog(ErrorNumber,                        
       ErrorSeverity,                        
       ErrorProcedure,                        
       ErrorLine,                        
       ErrorMessage,                        
       ErrorDate)                        
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());                        
                      
 set @ErrorMsg = 'Error while updating StagingCompanyProfilesFinancial, please contact Admin for more details.';                      
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine                      
,Message =@ErrorMsg,Success = 0;                         
                         
END CATCH                     
                  
                  
end