﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.Utils;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;

namespace Avira.MarketingResearch.Web.Controllers
{
    [NoCache]
    [Authorization]
    public class TrendKeyCompanyMapController : BaseController
    {
        public readonly IServiceRepository _serviceRepository;
        private ILoggerManager _logger;
        public TrendKeyCompanyMapController(IServiceRepository serviceRepository, ILoggerManager logger)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
        }
        [HttpGet]
        public IActionResult Index(string id, string currentFilter, int PageStart, string Direction, int totalRecords, int currentpage)
        {
            SetRoleList();
            MainTrendKeyCompanyMapInsertUpdateDbViewModel model = new MainTrendKeyCompanyMapInsertUpdateDbViewModel();
            try
            {
                model.TrendId = new Guid(id);
                model.CompanyName = string.Empty;
                int pagesize = Common.TrendPagingSize;
                int SortDirection = 1;

                HttpResponseMessage responseTrend = _serviceRepository.GetByIdResponse("Trend", id);
                TrendViewModel trend = responseTrend.Content.ReadAsAsync<TrendViewModel>().Result;
                model.TrendModel = trend ?? new TrendViewModel();
                //------Get trend note by trend Id           
                HttpResponseMessage responseTrendNote = _serviceRepository.GetByIdResponse("TrendNote", id, "KeyCompany", "id");
                responseTrendNote.EnsureSuccessStatusCode();
                var tnm = responseTrendNote.Content.ReadAsAsync<TrendNoteViewModel>().Result;
                model.TrendNoteModel = tnm ?? new TrendNoteViewModel();

                model.TrendModel.TrendStatusName = model.TrendNoteModel.TrendStatusName;

                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.GetResponse(contollerName, model.TrendId, PageStart, pagesize, SortDirection, "IsMapped", string.Empty);
                response.EnsureSuccessStatusCode();
                List<TrendKeyCompanyMapViewModel> trendKeyCompanyMap = response.Content.ReadAsAsync<List<TrendKeyCompanyMapViewModel>>().Result;
                model.TrendKeyCompanyMapInsertUpdate = trendKeyCompanyMap.Where(x => x.IsMapped).ToList();

                ViewBag.TrendKeyCompanyMapList = new SelectList(trendKeyCompanyMap.Where(x => !x.IsMapped).ToList(), "CompanyId", "CompanyName");
                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, model.TrendModel.ProjectID);
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                }

                ViewBag.Id = model.TrendId;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(model);
        }

        private void SetRoleList()
        {
            try
            {
                var roles = HttpContext.Session.GetString("userRoles");
                var userRoles = JsonConvert.DeserializeObject<List<RoleViewModel>>(roles);
                List<string> RoleListItems = new List<string>();
                foreach (var item in userRoles)
                    RoleListItems.Add(item.UserRoleName);
                ViewBag.UserRoleList = RoleListItems;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }
        [HttpPost]
        public IActionResult Index(MainTrendKeyCompanyMapInsertUpdateDbViewModel model, string actionName, int PageStart, string Direction, int totalRecords, int currentpage)
        {
            SetRoleList();
            ViewBag.Message = string.Empty;
            bool bResult = false;
            int pagesize = 0;
            int SortDirection = 1;
            try
            {
                model.TrendNoteModel.TrendType = "KeyCompany";
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();

                model.TrendKeyCompanyMapInsertUpdate = model.CompanyIds != null ? model.CompanyIds.Select(x => new TrendKeyCompanyMapViewModel { CompanyId = x }).ToList(): new List<TrendKeyCompanyMapViewModel>();
                
                if (actionName.Equals("save"))
                { 
                    bResult = validate(model);
                    if (bResult)
                    {
                        {
                        model.TrendNoteModel.TrendStatusName = "Draft";

                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            //ViewBag.Message = viewModel.Message;
                            return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.CompanyName });
                        }
                    }

                }
                else if (actionName.Equals("Submitted"))
                { 
                        bResult = validate(model);
                        if (bResult)
                        {
                            model.TrendNoteModel.TrendStatusName = "Submitted";
                            HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                            responseSave.EnsureSuccessStatusCode();
                            ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                            if (viewModel != null)
                            {
                                //ViewBag.Message = viewModel.Message;
                                return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.CompanyName });
                            }
                        }
                    }

                }
                else if (actionName.Equals("Rejected"))
                {
                   
                        model.TrendNoteModel.TrendStatusName = "Rejected";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            //ViewBag.Message = viewModel.Message;
                            return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.CompanyName });
                        }
                    
                }
                else if (actionName.Equals("Approve"))
                {
                    
                        model.TrendNoteModel.TrendStatusName = "Approved";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            //ViewBag.Message = viewModel.Message;
                            return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.CompanyName });
                        }
                    
                }
                else if (actionName.Equals("search"))
                {
                    return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.CompanyName });
                }
                else if (actionName.Equals("NewCompany"))
                {
                    if(string.IsNullOrEmpty(model.CompanyName))
                    {
                        ViewBag.Message = "Please enter company name.";
                    }
                    else
                    {
                        CompanyInsertDbViewModel objCompanyModel = new CompanyInsertDbViewModel();
                        objCompanyModel.CompanyCode = model.CompanyName.Replace(" ", "");
                        objCompanyModel.CompanyName = model.CompanyName;
                        //objCompanyModel.NatureId = new Guid("6001B1CA-227D-4392-B5CB-5445ACF8B4A0");
                       HttpResponseMessage responseCompany = _serviceRepository.PostResponse("Company", objCompanyModel);
                        responseCompany.EnsureSuccessStatusCode();
                        ViewData viewModel = responseCompany.Content.ReadAsAsync<ViewData>().Result;
                        //if (viewModel != null)
                        //{
                        //    ViewBag.Message = viewModel.Message;
                        //}
                        ViewBag.Message = null;
                    } 
                    
                }
                else if(actionName.Contains("RemoveKeyCompany"))
                {
                    var delCompanyID = actionName.Split('_')[1];

                    //model.TrendNoteModel.TrendStatusName = "Draft";
                    model.TrendKeyCompanyMapInsertUpdate = new List<TrendKeyCompanyMapViewModel> { new TrendKeyCompanyMapViewModel {  CompanyId = new Guid(delCompanyID) } };

                    HttpResponseMessage responseSave = _serviceRepository.DeleteResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            //ViewBag.Message = viewModel.Message;
                            return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.CompanyName });
                        }
                    
                }
                else if (actionName.Equals("Next")) 
                    return RedirectToAction("Index", "TrendValueMap", new { id = model.TrendId.ToString() });
                else if (actionName.Equals("Previous"))
                    return RedirectToAction("Index", "TrendEcoSystemMap", new { id = model.TrendId.ToString() });
                else if (actionName.Equals("Cancel")) 
                    return RedirectToAction("Index", "TrendKeyCompanyMap", new { id = model.TrendId.ToString() });
               
                HttpResponseMessage responseImpactType = _serviceRepository.GetResponse("ImpactType");
                responseImpactType.EnsureSuccessStatusCode();
                ViewBag.ImpactList = responseImpactType.Content.ReadAsAsync<List<ImpactTypeViewModel>>().Result;

                HttpResponseMessage responseTrend = _serviceRepository.GetByIdResponse("Trend", model.TrendId.ToString());

                TrendViewModel trend = responseTrend.Content.ReadAsAsync<TrendViewModel>().Result;
                model.TrendModel = trend ?? new TrendViewModel();

                HttpResponseMessage responseTrendNote = _serviceRepository.GetByIdResponse("TrendNote", model.TrendId.ToString(), "KeyCompany", "id");
                responseTrendNote.EnsureSuccessStatusCode();
                var tnm = responseTrendNote.Content.ReadAsAsync<TrendNoteViewModel>().Result;
                model.TrendNoteModel = tnm ?? new TrendNoteViewModel();

                HttpResponseMessage response = _serviceRepository.GetResponse(contollerName, model.TrendId, PageStart, pagesize, SortDirection, "IsMapped", string.Empty);
                response.EnsureSuccessStatusCode();
                List<TrendKeyCompanyMapViewModel> trendKeyCompanyMap = response.Content.ReadAsAsync<List<TrendKeyCompanyMapViewModel>>().Result;
                model.TrendKeyCompanyMapInsertUpdate = trendKeyCompanyMap.Where(x => x.IsMapped).ToList();
                ViewBag.TrendKeyCompanyMapList = new SelectList(trendKeyCompanyMap.Where(x => !x.IsMapped).ToList(), "CompanyId", "CompanyName");

                ViewBag.Id = model.TrendId;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }


            return View(model);
        }


        /// <summary>
        /// Method to validate Form
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private bool validate(MainTrendKeyCompanyMapInsertUpdateDbViewModel model)
        {
            if (model.TrendKeyCompanyMapInsertUpdate != null && model.TrendKeyCompanyMapInsertUpdate.Count() < 1)
            {
                ViewBag.Message = "Please select alest one company for the trend mapping.";
                return false; 
            }
            else if (model.TrendNoteModel.AuthorRemark == null || model.TrendNoteModel.AuthorRemark == "" || model.TrendNoteModel.AuthorRemark == string.Empty)
            {
                ViewBag.Message = "Please enter Author Remark!!";
                return false;
            }
            else
            {
                ViewBag.Message = string.Empty;
                return true;
            }
        }
    }
}