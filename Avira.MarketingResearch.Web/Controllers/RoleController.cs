﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.Models;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Routing;

namespace Avira.MarketingResearch.Web.Controllers
{
    [NoCache]
    [Authorization]
    public class RoleController : BaseController
    {
        public readonly IServiceRepository _serviceRepository;
        private ILoggerManager _logger;
        public RoleController(IServiceRepository serviceRepository, ILoggerManager logger)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
        }

        public void LoadDropDown()
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.GetAllParentResponse(contollerName);
                response.EnsureSuccessStatusCode();
                List<RoleViewModel> roleList = response.Content.ReadAsAsync<List<RoleViewModel>>().Result;
                ViewBag.RoleList = roleList;

                List<SelectListItem> listItems = new List<SelectListItem>();

                listItems.Add(new SelectListItem
                {
                    Text = "Read",
                    Value = "1"
                });
                listItems.Add(new SelectListItem
                {
                    Text = "Create",
                    Value = "2"
                });
                listItems.Add(new SelectListItem
                {
                    Text = "Update",
                    Value = "3"
                });
                listItems.Add(new SelectListItem
                {
                    Text = "Delete",
                    Value = "4"
                });

                ViewBag.listItems = listItems;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        public void checkDropDownValues()
        {
            RoleInsertDbViewModel rvm = new RoleInsertDbViewModel();
            //for (int i = 0; i <= rvm.PermissionLevel.Length; i++)
            //{
            //    rvm.PermissionLevel = null;
            //}
        }
        // GET: Roles
        //[Route("Resources/CreateRoles")]
        [HttpGet]
        public ActionResult CreateRoles()
        {
            LoadDropDown();
            return View();
        }

        //[Route("Resources/Index")]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LoadData(RoleViewModel keyword)
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            int totalRecords = 0;
            List<RoleViewModel> data = null;
            try
            {
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                string search = Request.Form["search[value]"];

                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDir = Request.Form["order[0][dir]"].FirstOrDefault();
                var SortDirection = -1;
                if (sortColumnDir == "asc")
                {
                    SortDirection = 1;
                }
                else
                {
                    SortDirection = -1;
                }

                int skip = start != null ? Convert.ToInt32(start) : 0;
                int pageSize = length.ToString() != null ? Convert.ToInt32(length) : 0;
                int PageStart = start.ToString() != null ? Convert.ToInt32(start) : 0;
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();

                HttpResponseMessage response = _serviceRepository.GetResponse(contollerName, PageStart, pageSize, SortDirection, sortColumn, search);
                response.EnsureSuccessStatusCode();
                List<RoleViewModel> trendKeywords = response.Content.ReadAsAsync<List<RoleViewModel>>().Result;
                ViewBag.Title = "All Roles";
                data = trendKeywords;
             
                foreach (var item in trendKeywords)
                {
                    totalRecords = item.TotalItems;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data });

        }

        // POST: Roles/Create
        //[Route("Resources/CreateRoles")]
        [HttpPost]
        public ActionResult CreateRoles(RoleInsertDbViewModel roles, IFormCollection collection)
        {
            if (roles.UserRoleName != null || roles.UserRoleName == "")
            {
                try
                {
                    if (!string.IsNullOrEmpty(collection["selectedPermission1"]) && roles.PermissionLevel.Length != 0)
                    {
                        string strSelectedShops = collection["selectedPermission1"];
                        string lstpl = collection["PermissionLevel"];
                        string[] guids = strSelectedShops.Split(',');
                        List<string> guidLst = new List<string>();

                        foreach (var guid in guids)
                        {
                            guidLst.Add(guid);
                        }
                        roles.UserPermisionID = guidLst;
                    }
                    else
                    {
                        ViewBag.Message = "Please select at least one Permission Name and Permission Level.";
                        LoadDropDown();
                        ModelState.Clear();
                        roles.IsChecked = false;
                        return View(roles);
                    }

                    var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                    HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, roles);
                    response.EnsureSuccessStatusCode();
                    ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                    if (viewModel != null)
                    {
                        ViewBag.Message = viewModel.Message;
                    }
                    LoadDropDown();
                    ModelState.Clear();
                    roles.IsChecked = false;
                    return View("CreateRoles");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    LoadDropDown();
                    ModelState.Clear();
                    roles.IsChecked = false;
                }
            }
            ViewBag.Message = "Please enter User Role Name.";
            LoadDropDown();
            ModelState.Clear();
            roles.IsChecked = false;
            return View(roles);
        }

        //[Route("Resources/Edit/{id}")]
        [HttpGet]
        public IActionResult Edit(string id, string msg, string roleName)
        {
            if (msg != null)
            {
                ViewBag.Message = msg;
                msg = null;
            }
            try
            {
                LoadDropDown();
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.GetByIdResponse(contollerName, id);
                response.EnsureSuccessStatusCode();
                IEnumerable<RoleEditViewModel> userRolesEditList = response.Content.ReadAsAsync<IEnumerable<RoleEditViewModel>>().Result.ToList();
                ViewBag.RolesData = userRolesEditList;
                ViewBag.UserRoleName = roleName;

                if (userRolesEditList.Count().ToString() != "0")
                {
                    return View(userRolesEditList);
                }
                else
                {
                    ViewBag.Msg = "No Details found for this Role.";
                    return View();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                ErrorViewModel err = new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier };
                return View("Error", err);
            }
        }

        // POST: Roles/Create
        //[Route("Resources/Edit/{id}")]
        [HttpPost]
        public ActionResult Edit(RoleUpdateViewModel roles, IFormCollection fc)
        {
            var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            try
            {
                if (!string.IsNullOrEmpty(fc["userRoleId"]) && roles.PermissionLevel.Length != 0)
                {
                    string strSelectedShops = fc["userRoleId"];
                    string[] guids1 = strSelectedShops.Split(',');
                    List<Guid> guidLst1 = new List<Guid>();

                    foreach (var guid in guids1)
                    {
                        Guid gid = new Guid(guid);
                        guidLst1.Add(gid);
                    }
                    roles.UserPermisionID = guidLst1;
                }
                else
                {
                    LoadDropDown();
                    roles.IsChecked = false;
                    return RedirectToAction("Edit", "Role", new { msg = "Please select Permission Name and Permission Level.", roleName = roles.UserRoleName });
                }

                HttpResponseMessage response1 = _serviceRepository.PutResponse(contollerName, roles);
                response1.EnsureSuccessStatusCode();

                ViewData viewModel = response1.Content.ReadAsAsync<ViewData>().Result;
                if (viewModel != null)
                {
                    ViewBag.Message = viewModel.Message;
                }

                return RedirectToAction("Edit", "Role", new { msg = viewModel.Message, roleName = roles.UserRoleName });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                LoadDropDown();
                roles.IsChecked = false;
                return RedirectToAction("Edit", "Role", new { msg = "", roleName = roles.UserRoleName });
            }
        }

        public ActionResult Delete(string id, string roleName)
        {
            ViewBag.UserRoleId = new Guid(id);
            ViewBag.UserRoleName = roleName;
            return View();
        }

        [HttpPost]
        public ActionResult Delete(RoleUpdateViewModel roles)
        {
            var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            try
            {
                HttpResponseMessage response = _serviceRepository.DeleteResponse(contollerName, roles);
                response.EnsureSuccessStatusCode();

                ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                if (viewModel != null)
                {
                    ViewBag.Message = viewModel.Message;
                }

                return View("Index");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return View("Index");
            }
        }

    }
}