﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.Masters;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces.Masters;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CurrencyConversionController : AviraBaseController
    {
        private ILoggerManager _logger;
        public readonly ICurrencyConversionRepository _currencyConversionRepository;
        public readonly ICurrencyConversionService _currencyConversionService;
        public CurrencyConversionController(
            ICurrencyConversionService currencyConversionService,
            ICurrencyConversionRepository currencyConversionRepository,
            ILoggerManager logger)
        {
            _currencyConversionRepository = currencyConversionRepository;
            _currencyConversionService = currencyConversionService;
            _logger = logger;
        }

        [Route("GetAllCurrencyConversions")]
        public IEnumerable<CurrencyConversionInsertDbViewModel> GetAllCurrency(CurrencyConversionInsertDbViewModel viewmodel)
        {
            List<CurrencyConversionInsertDbViewModel> currency = _currencyConversionService.GetByIdAll(viewmodel).ToList();
            //if (currency == null)
            //{
            //    return NotFound();
            //}

            return currency;
        }

        [HttpGet("{id}")]
        public ActionResult<IEnumerable<CurrencyConversionInsertDbViewModel>> Get(Guid id)
        {
            List<CurrencyConversionInsertDbViewModel> res = null;
            try
            {
                return _currencyConversionService.GetById(id).ToList();
        }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpPost]
        public ActionResult Post([FromBody] CurrencyConversionInsertDbViewModel viewModel)
        {
            viewModel.UserCreatedById = CurrentUser.Id;

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while Editing a Currency, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                var result = _currencyConversionService.Create(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    if(viewModel.CurrencyConversionId.ToString() == "00000000-0000-0000-0000-000000000000")
                    {
                        viewData.Message = "Currency Conversion for " + viewModel.CurrencyName + " is added  successfully.";
                    }
                    else
                    {
                        viewData.Message = "Currency Conversion for " + viewModel.CurrencyName + " is updated  successfully.";
                    }
                    
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }

            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);

        }
    }
}