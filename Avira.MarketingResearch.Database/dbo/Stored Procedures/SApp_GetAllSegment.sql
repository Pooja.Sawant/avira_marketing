﻿

CREATE PROCEDURE [dbo].[SApp_GetAllSegment]
	(
		@includeDeleted bit = 0
	)
	AS
/*================================================================================
Procedure Name: [dbo].[SApp_GetAllSegment]
Author: Harshal
Create date: 03/19/2019
Description: Get list from Product Segment table by Id
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
03/19/2019	Adarsh Dubey			Initial Version
================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT [Segment].[Id],
		[Segment].[SegmentName],
		[Segment].[Description],
		[Segment].[IsDeleted]
		
FROM [dbo].[Segment]
where  ([Segment].IsDeleted = 0)
	
	END