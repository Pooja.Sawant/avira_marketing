﻿using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces.Masters;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.Masters
{
    public class CurrencyRepository : ICurrencyRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "Currency";
        public CurrencyRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }
        public CurrencyViewModel GetById(Guid guid)
        {
            CurrencyViewModel currencyViewModel = new CurrencyViewModel();
            currencyViewModel = GetAllCurrency(guid).FirstOrDefault();
            return currencyViewModel;
        }
        public IEnumerable<CurrencyViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted)
        {
            return GetAllCurrency(null, PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, includeDeleted);
        }

        public List<CurrencyViewModel> GetAllList()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<CurrencyViewModel>(CurrencyQueries.StoreProcSelectall).ToList();
            //return dapper.Query<CurrencyViewModel>(CurrencyQueries.StoreProcSelectall, null, null, true, null, CommandType.StoredProcedure);
        }

        private IEnumerable<CurrencyViewModel> GetAllCurrency(Guid? guid, int PageStart = 0, int pageSize = 0, int SortDirection = -1, string OrdbyByColumnName = "", string Search = "", bool includeDeleted = false)
        {

            DataTable currencyIdsTable = new DataTable();
            currencyIdsTable.Columns.Add("ID", typeof(Guid));

            if (guid != null)
            {
                currencyIdsTable.Rows.Add(guid);
            }
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<CurrencyViewModel>(CurrencyQueries.StoreProcSelectGetById
                            , new { @CurrencyIDList = currencyIdsTable.AsTableValuedParameter("dbo.udtUniqueIdentifier"), @includeDeleted = includeDeleted, @PageStart = PageStart, @pageSize = pageSize, @SortDirection = SortDirection, @OrdbyByColumnName = OrdbyByColumnName, @CurrencyName = Search }
                            , null, true, null, CommandType.StoredProcedure);
        }
    }
}
