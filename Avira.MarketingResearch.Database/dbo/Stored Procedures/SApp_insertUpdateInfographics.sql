﻿CREATE procedure [dbo].[SApp_insertUpdateInfographics]
(

@Id	uniqueidentifier,
@ProjectId	uniqueidentifier,
@LookupCategoryId	uniqueidentifier,
@InfogrTitle	nvarchar(128),
@InfogrDescription	nvarchar(max),
@SequenceNo	smallint,
@ImageActualName	nvarchar(200),
@ImageName	nvarchar(100),
@ImagePath	nvarchar(200),
@AviraUserId uniqueidentifier

)
as
/*================================================================================
Procedure Name: SApp_insertUpdateInfographics
Author: Praveen
Create date: 10/25/2019
Description: Insert and update for Infographics
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
10/25/2019	Praveen			Initial Version
================================================================================*/
declare @ErrorMsg NVARCHAR(2048),@CreatedOn Datetime
BEGIN
	SET @CreatedOn = GETDATE()
	BEGIN TRY   


IF not exists(select 1 from Infographics WHERE Id = @Id)
begin

IF exists(select 1 from Infographics WHERE ProjectId = @ProjectId AND LookupCategoryId = @LookupCategoryId AND InfogrTitle = @InfogrTitle)
begin	
	SELECT StoredProcedureName ='SApp_insertUpdateInfographics',Message ='Image title can’t be duplicated in same category under same project',Success = 0;  	
end
else	
begin
--Insert new record
	set @Id = NEWID();
	insert into Infographics(	
Id,
ProjectId,
LookupCategoryId,
InfogrTitle,
InfogrDescription,
SequenceNo,
ImageActualName,
ImageName,
ImagePath,
CreatedOn,
UserCreatedById
	)
	values(
	@Id,
	@ProjectId,
	@LookupCategoryId,
	@InfogrTitle,
	@InfogrDescription,
	@SequenceNo,
	@ImageActualName,
	@ImageName,
	@ImagePath,
	@CreatedOn,
	@AviraUserId
	);
	end
end
else
begin
--update record
IF exists(select 1 from Infographics WHERE [Id] != @Id and ProjectId = @ProjectId AND LookupCategoryId = @LookupCategoryId AND InfogrTitle = @InfogrTitle)
begin	
	SELECT StoredProcedureName ='SApp_insertUpdateInfographics',Message ='Image title can’t be duplicated in same category under same project',Success = 0;  	
end
else	
begin
	UPDATE [dbo].[Infographics]
   SET 
       [ProjectId] =@ProjectId
      ,[LookupCategoryId] = @LookupCategoryId
      ,[InfogrTitle] = @InfogrTitle
      ,[InfogrDescription] = @InfogrDescription
      ,[SequenceNo] = @SequenceNo
      ,[ImageActualName] = CASE WHEN ISNULL(@ImageActualName,'') = '' THEN [ImageActualName] ELSE @ImageActualName END
      ,[ImageName] = CASE WHEN ISNULL(@ImageName,'') = '' THEN ImageName ELSE @ImageName END 
      ,[ImagePath] = CASE WHEN ISNULL(@ImagePath,'') = '' THEN ImagePath ELSE @ImagePath END      
      ,[ModifiedOn] = @CreatedOn
      ,[UserModifiedById] = @AviraUserId
 WHERE [Id] = @Id
 end
end
SELECT StoredProcedureName ='SApp_insertUpdateInfographics',Message ='Success',Success = 1;   
END TRY    
BEGIN CATCH    
    -- Execute the error retrieval routine.    
 DECLARE @ErrorNumber int;            
 DECLARE @ErrorSeverity int;            
 DECLARE @ErrorProcedure varchar(100);            
 DECLARE @ErrorLine int;    
 DECLARE @ErrorMessage varchar(500);   
    
  SELECT @ErrorNumber = ERROR_NUMBER(),    
        @ErrorSeverity = ERROR_SEVERITY(),    
        @ErrorProcedure = ERROR_PROCEDURE(),    
        @ErrorLine = ERROR_LINE(),    
        @ErrorMessage = ERROR_MESSAGE()    
    
 insert into dbo.Errorlog(ErrorNumber,    
       ErrorSeverity,    
       ErrorProcedure,    
       ErrorLine,    
       ErrorMessage,    
       ErrorDate)    
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,@CreatedOn);    
  
 set @ErrorMsg = 'Error while importing trend, please contact Admin for more details.';    
--THROW 50000,@ErrorMsg,1;      
--return(1)  
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;     
     
END CATCH     


END