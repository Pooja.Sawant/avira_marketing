﻿using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Avira.MarketingResearch.Web.ImportExportUtility
{
    public abstract class BaseImportValidation
    {
        public abstract bool IsSatisfiedBy();
        public abstract List<ImportCommonResponseModel> ValidateDataType();
        public abstract List<ImportCommonResponseModel> ValidateDataLenth();
        public abstract List<ImportCommonResponseModel> ValidateColumnExits();
    }
}
