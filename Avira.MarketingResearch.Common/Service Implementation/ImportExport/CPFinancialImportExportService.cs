﻿using Avira.MarketingResearch.Common.IService.ImportExport;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.ImportExport
{
    public class CPFinancialImportExportService : ICPFinancialImportExportService
    {
        private readonly ICPFinancialImportExportRepository _cpFinancialImportExportRepository;

        public CPFinancialImportExportService(ICPFinancialImportExportRepository buildLevelRepository)
        {
            _cpFinancialImportExportRepository = buildLevelRepository;
        }
        public List<ImportCommonResponseModel> Create(CPFinancialImportInsertModel viewModel, AviraUser currentUser)
        {
            viewModel.TrendJson = JsonConvert.SerializeObject(viewModel.CPFinancialImportModel);
            viewModel.SegmentJson= JsonConvert.SerializeObject(viewModel.CPFinancialSegmentInformationModel);
            var cpFinancialImportResponse = _cpFinancialImportExportRepository.Create(viewModel);
            return cpFinancialImportResponse;
        }
    }
}
