﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.Trend
{
    public interface ITrendRepository
    {
        TrendViewModel GetById(Guid guid);
        IEnumerable<TrendViewModel>GetAll(Guid ProjectId,int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted);
        Result<SpTransactionMessage> Create(TrendInsertDbViewModel viewModel);
        Result<SpTransactionMessage> Update(TrendUpdateDbViewModel viewModel);
    }
}
