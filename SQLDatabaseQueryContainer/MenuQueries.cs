﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public static class MenuQueries
    {
        public static readonly string StoreProcInsert = "SApp_insertMenu";

        public static readonly string StoreProcUpdate = "SApp_UpdateMenu";

        public static readonly string StoreProcSelectById = "[SApp_GetMenubyID]";

        public static string StoreProcGetAllByLocale = "";
    }
}
