﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.Models;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Avira.MarketingResearch.Web.Controllers
{
    [NoCache]
    [Authorization]
    public class CompanyProfileController : BaseController
    {
        public readonly IServiceRepository _serviceRepository;
        private ILoggerManager _logger;
        public CompanyProfileController(IServiceRepository serviceRepository, ILoggerManager logger)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
        }
        public IActionResult Index(Guid Id)
        {
            try
            {
                int pageSize = 0;
                int PageStart = 0;
                var SortDirection = -1;
                var sortColumn = "";
                string search = "";
                var LoginId = HttpContext.Session.GetString("UserId");
                HttpResponseMessage response = _serviceRepository.GetResponseByUserId("Project", Id, new Guid(LoginId), PageStart, pageSize, SortDirection, sortColumn, search);
                response.EnsureSuccessStatusCode();
                List<ProjectViewModel> project = response.Content.ReadAsAsync<List<ProjectViewModel>>().Result;
                foreach (var proj in project)
                {
                    if (!string.IsNullOrEmpty(proj.MApproverApprover))
                    {
                        var jApprover = JsonConvert.DeserializeObject<List<object>>(proj.MApproverApprover);
                        var definition = new[] { new { Id = "", ResourceName = "" } };
                        var customer = JsonConvert.DeserializeAnonymousType(proj.MApproverApprover, definition);
                        var name = customer.Select(x => x.ResourceName).ToList();
                        proj.Approver = (string)name[0];
                    }
                    if (!string.IsNullOrEmpty(proj.TrendsFormsAuthor))
                    {
                        var jApprover = JsonConvert.DeserializeObject<List<object>>(proj.TrendsFormsAuthor);
                        var definition = new[] { new { Id = "", ResourceName = "" } };
                        var customer = JsonConvert.DeserializeAnonymousType(proj.TrendsFormsAuthor, definition);
                        var name = customer.Select(x => x.ResourceName).ToList();
                        proj.ProjectAuthorName = (string)name[0];
                    }
                    if (!string.IsNullOrEmpty(proj.TrendsFormsCoAuthor))
                    {
                        var jApprover = JsonConvert.DeserializeObject<List<object>>(proj.TrendsFormsCoAuthor);
                        var definition = new[] { new { Id = "", ResourceName = "" } };
                        var customer = JsonConvert.DeserializeAnonymousType(proj.TrendsFormsCoAuthor, definition);
                        var name = customer.Select(x => x.ResourceName).ToList();
                        proj.ProjectCoAuthorName = (string)name[0];
                    }
                }
                ViewBag.ProjectId = Id;
                if (project != null && project.Count > 0)
                {
                    ViewBag.ProjectName = project[0].ProjectName;
                    ViewBag.ProjectCode = project[0].ProjectCode;
                    ViewBag.Approver = project[0].Approver;
                    ViewBag.ProjectAuthorName = project[0].ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project[0].ProjectCoAuthorName;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View();
        }
    }
}