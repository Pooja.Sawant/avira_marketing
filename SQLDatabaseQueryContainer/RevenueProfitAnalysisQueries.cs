﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public static class RevenueProfitAnalysisQueries
    {
        public static string StoreProcSelectAllCurrencies = "SApp_GetAllCurrencies";
        public static string StoreProcSelectAllUnits = "SApp_GetAllValueConversion";
        public static string StoreProcSelectAllRegions = "SApp_GetAllRegions";

        public static readonly string StoreProcInsertUpdateCompanyRevenue = "SApp_insertCompanyRevenueAnalysis";
        public static readonly string StoreProcInsertUpdateCompanyRevenueGeographic = "SApp_insertupdateCompanyRevenueGeographicSplit";
        public static readonly string StoreProcInsertUpdateCompanyRevenueSegmentation = "SApp_insertupdateCompanyRevenueSegmentationSplit";

        public static readonly string StoreProcInsertUpdateCompanyProfit = "SApp_insertCompanyProfitAnalysis";
        public static readonly string StoreProcInsertUpdateCompanyProfitGeographic = "SApp_insertupdateCompanyProfitGeographicSplit";
        public static readonly string StoreProcInsertUpdateCompanyProfitSegmentation = "SApp_insertupdateCompanyProfitSegmentationSplit";

        public static string StoreProcGetProfitAndLossData = "SApp_GetProfitAndLossDataByCompanyID";
        public static string StoreProcGetCompanyBalanceSheetData = "SApp_GetCompanyBalanceSheetDataByCompanyID";
        public static string StoreProcGetCompanyCashFlowStatementData = "SApp_GetCompanyCashFlowStatementDataByCompanyID";
        public static string StoreProcGetCompanySegmentInformationData = "SApp_GetCompanySegmentInformationDataByCompanyID";
    }
}
