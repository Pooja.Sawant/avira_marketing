﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.QualitativeInfo;
using Avira.MarketingResearch.Common.Service_Implementation.QualitativeInfo;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.QualitativeInfo;
using Avira.MarketingResearch.Repository.CoreInterfaces.QualitativeInfo;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QualitativeAddTableController : AviraBaseController
    {
        private readonly IQualitativeAddTableService _iQualitativeAddTableService;
        private readonly IQualitativeAddTableRepository _iQualitativeAddTableRepository;
        private ILoggerManager _logger;


        public QualitativeAddTableController(
           IQualitativeAddTableService iQualitativeAddTableService,
           IQualitativeAddTableRepository iQualitativeAddTableRepository,
            ILoggerManager logger)
        {
            _iQualitativeAddTableService = iQualitativeAddTableService;
            _iQualitativeAddTableRepository = iQualitativeAddTableRepository;
            _logger = logger;

        }

        [HttpGet("{id}")]
        public ActionResult<IEnumerable<QualitativeAddTableViewModel>> Get(Guid Id)
        {
            List<QualitativeAddTableViewModel> res = null;
            try
            {
                res = _iQualitativeAddTableService.GetAll(Id).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        //[HttpGet("{id}")]
        //public ActionResult<IEnumerable<QualitativeAddTableViewModel>> Get(Guid? Id, int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false)
        //{
        //    var res = _iQualitativeAddTableService.GetAll(Id, PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude).ToList();
        //    return res;
        //}

        [HttpPost]
        public ActionResult Post([FromBody] QualitativeGenerateTableCRUDViewModel viewModel)
        {
            viewModel.CreatedOn = System.DateTime.UtcNow;
            viewModel.AviraUserId = CurrentUser.Id;
            viewModel.UserCreatedById = CurrentUser.Id;

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new Qualitative Add Table, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                var result = _iQualitativeAddTableService.InsertUpdate(viewModel);
                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Qualitative Add Table is created successfully.";
                    viewData.Id= viewModel.Id;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }
    }
}