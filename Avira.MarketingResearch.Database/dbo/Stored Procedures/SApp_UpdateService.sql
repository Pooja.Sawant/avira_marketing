﻿
CREATE procedure [dbo].[SApp_UpdateService]    
(@Id uniqueidentifier,    
@Description nvarchar(256),    
@ServiceName nvarchar(256), 
@ParentServiceId uniqueidentifier,  
@IsDeleted bit=0,  
@UserModifiedById  uniqueidentifier,  
@DeletedOn datetime = null,  
@UserDeletedById uniqueidentifier = null,  
@ModifiedOn Datetime   
)
as    
/*================================================================================    
Procedure Name: SApp_UpdateService    
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
03/26/2019 Harshal   Initial Version  
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
  
IF exists(select 1 from 
--[dbo].[Service]     
[dbo].SubSegment [Service]
INNER JOIN Segment ON [Service].SegmentId = Segment.Id AND Segment.SegmentName = 'End User'

   where [Service].[Id] !=  @Id    
   and  SubSegmentName = @ServiceName AND [Service].IsDeleted = 0)    
begin    
declare @ErrorMsg NVARCHAR(2048);    
set @ErrorMsg = 'Service with Name "'+ @ServiceName + '" already exists.';    
--SELECT 50000,@ErrorMsg,1;   
--return(1);   
SELECT StoredProcedureName ='SApp_UpdateService','Message' =@ErrorMsg,'Success' = 0;  
RETURN;
end    
  
BEGIN TRY     
--update record    
update [dbo].SubSegment    
set SubSegmentName = @ServiceName,    
 [Description] = @Description,
 ParentId=@ParentServiceId,    
 [ModifiedOn] = @ModifiedOn,    
 DeletedOn = case when IsDeleted = 0 and @IsDeleted = 1 then @DeletedOn else DeletedOn end,    
 UserDeletedById = case when IsDeleted = 0 and @IsDeleted = 1 then @UserDeletedById else UserDeletedById end,    
 IsDeleted = isnull(@IsDeleted, IsDeleted),    
 [UserModifiedById] = @UserModifiedById    
    where ID = @Id   
   
SELECT StoredProcedureName ='SApp_UpdateService',Message =@ErrorMsg,Success = 1;     
  
END TRY    
BEGIN CATCH    
    -- Execute the error retrieval routine.    
 DECLARE @ErrorNumber int;    
 DECLARE @ErrorSeverity int;    
 DECLARE @ErrorProcedure varchar(100);    
 DECLARE @ErrorLine int;    
 DECLARE @ErrorMessage varchar(500);    
    
  SELECT @ErrorNumber = ERROR_NUMBER(),    
        @ErrorSeverity = ERROR_SEVERITY(),    
        @ErrorProcedure = ERROR_PROCEDURE(),    
        @ErrorLine = ERROR_LINE(),    
        @ErrorMessage = ERROR_MESSAGE()    
    
 insert into dbo.Errorlog(ErrorNumber,    
       ErrorSeverity,    
       ErrorProcedure,    
       ErrorLine,    
       ErrorMessage,    
       ErrorDate)    
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE()); 

set @ErrorMsg = 'Error while updating Service, please contact Admin for more details.' 		   
 --set @ErrorMsg = 'Unspecified Error';    
--THROW 50000,@ErrorMsg,1;    
 --return(1)  
 SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;     
END CATCH    
    
  
  
    
end