﻿

CREATE procedure [dbo].[SApp_GetUserRolesById]    
(@UserRoleId uniqueidentifier)    
as    
/*================================================================================    
Procedure Name: [SApp_GetUserRolesById]    
Author: Gopi    
Create date: 04/10/2019    
Description: Get userRoles by Id    
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
04/23/2019 Sai Krishna   Initial Version    
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    
    
	with ur as (select ur.Id, ur.UserRoleName
				from UserRole ur
				where ur.Id = @UserRoleId 
				and ur.IsDeleted = 0)
	select ur.Id as UserRoleId, 
	ur.UserRoleName,
	 p.Id as userPermisionID, 
	 p.PermissionName, 
	 p.UserType, 
	 urpm.PermissionLevel
	from  ur 
	inner Join UserRolePermissionMap urpm
	on urpm.UserRoleId = ur.Id 
	and urpm.IsDeleted = 0 
	right join Permission p
	on p.Id = urpm.PermissionId
	where p.IsDeleted = 0
	order by PermissionName
END