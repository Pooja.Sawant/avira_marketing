﻿CREATE TYPE [dbo].[udtGUIDCompanySubsidaryDetails] AS TABLE (
    [Id]       UNIQUEIDENTIFIER NOT NULL,
    [Name]     NVARCHAR (256)   NOT NULL,
    [Location] NVARCHAR (256)   NOT NULL,
    [bitValue] BIT              NOT NULL);

