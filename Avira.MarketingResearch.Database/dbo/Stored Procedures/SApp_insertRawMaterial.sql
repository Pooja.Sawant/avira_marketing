﻿CREATE procedure [dbo].[SApp_insertRawMaterial]    
(    
@RawMaterialName nvarchar(256),    
@Description nvarchar(256),    
@UserCreatedById uniqueidentifier,  
@Id uniqueidentifier,  
@CreatedOn Datetime
)    
as    
/*================================================================================    
Procedure Name: SApp_insertRawMaterial    
Author: Sharath    
Create date: 02/19/2019    
Description: Insert a record into RawMaterial table    
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
02/19/2019 Sharath   Initial Version    
03/18/2019 Nimishekh 1) All the paramter of Model should be decalre as input parameter   
      with default value is null if those parameter is not mandetory.      
         2) Return should select with all the value of standard Model that we are using in Code  
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    
declare @ErrorMsg NVARCHAR(2048)
declare @SegmentId uniqueidentifier;
select @SegmentId = Id from Segment
where SegmentName = 'Raw materials';
  
--Validations    
IF exists(select 1 from [dbo].[SubSegment]     
   where SubSegmentName = @RawMaterialName 
   AND SegmentId = @SegmentId
   AND IsDeleted = 0)    
begin    
--declare @ErrorMsg NVARCHAR(2048);    
set @ErrorMsg = 'Raw-Material Name "'+ @RawMaterialName + '" already exists.';    
--THROW 50000,@ErrorMsg,1;    
--return(1)    
SELECT StoredProcedureName ='SApp_insertRawMaterial',Message =@ErrorMsg,Success = 0;  
RETURN;  
end    
--End of Validations    
BEGIN TRY       
       
insert into [dbo].[SubSegment]([Id],    
		SegmentId,
		SubSegmentName,
		[Description],    
        [IsDeleted],    
        [UserCreatedById],    
        [CreatedOn])    
 values(@Id,
 @SegmentId,
   @RawMaterialName,     
   @Description,     
   0,    
   @UserCreatedById,     
   @CreatedOn);    
    
--select @RawMaterialID;    
--return(0)    
SELECT StoredProcedureName ='SApp_insertRawMaterial',Message =@ErrorMsg,Success = 1;     
  
END TRY    
BEGIN CATCH    
    -- Execute the error retrieval routine.    
 DECLARE @ErrorNumber int;    
 DECLARE @ErrorSeverity int;    
 DECLARE @ErrorProcedure varchar(100);    
 DECLARE @ErrorLine int;    
 DECLARE @ErrorMessage varchar(500);    
    
  SELECT @ErrorNumber = ERROR_NUMBER(),    
        @ErrorSeverity = ERROR_SEVERITY(),    
        @ErrorProcedure = ERROR_PROCEDURE(),    
        @ErrorLine = ERROR_LINE(),    
        @ErrorMessage = ERROR_MESSAGE()    
    
 insert into dbo.Errorlog(ErrorNumber,    
       ErrorSeverity,    
       ErrorProcedure,    
       ErrorLine,    
       ErrorMessage,    
       ErrorDate)    
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,@CreatedOn);    
  
 set @ErrorMsg = 'Error while creating a new RawMaterial, please contact Admin for more details.';    
--THROW 50000,@ErrorMsg,1;      
--return(1)  
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;     
     
END CATCH    
    
    
end