﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.Helpers;
using System.Collections.Generic;
using System;
using System.Data;

namespace Avira.MarketingResearch.Repository
{
    public interface IRegionRepository
    {
        RegionViewModel GetById(Guid guid);
        IEnumerable<RegionViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted);
        Result<SpTransactionMessage> Create(RegionInsertDbViewModel viewModel);
        Result<SpTransactionMessage> Update(RegionUpdateDbViewModel viewModel);
        IEnumerable<RegionCountryModel> GetRegionByTrendID(Guid trendId);
    }
}
