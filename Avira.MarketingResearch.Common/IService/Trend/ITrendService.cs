﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.Trend
{
    public interface ITrendService
    {
        IEnumerable<TrendViewModel> GetAll(Guid ProjectId,int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false);
        TrendViewModel GetById(Guid guid);

        Result<SpTransactionMessage> Create(TrendInsertDbViewModel tfc, AviraUser currentUser);
        Result<SpTransactionMessage> Update(TrendUpdateDbViewModel tfc, AviraUser currenUser);
    }
}
