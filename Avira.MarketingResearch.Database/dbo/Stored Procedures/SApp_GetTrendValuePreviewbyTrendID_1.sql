﻿
 CREATE PROCEDURE [dbo].[SApp_GetTrendValuePreviewbyTrendID]          
 (    
 @TrendID uniqueidentifier =null
 )          
 AS          
/*================================================================================          
Procedure Name: SApp_GetTrendValuePreviewbyTrendID    
Author: Harshal          
Create date: 04/26/2019          
Description: Get trend value Mapping Status of by TrendID        
Change History          
Date  Developer  Reason          
__________ ____________ ______________________________________________________          
04/26/2019 Harshal   Initial Version          
================================================================================*/         
          
BEGIN          
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED    
 declare @SubmittedStatusID uniqueidentifier;
 select @SubmittedStatusID = id from TrendStatus
 where TrendStatusName = 'Submitted'

;WITH CTE_TST as (select TrendType, 
[Submitted] as SubmittedDate,
[Approved] as ApprovedDate
from (
select 
TrendStatusTracking.TrendType,
TrendStatus.TrendStatusName,
TrendStatusTracking.CreatedOn as StatusDate
from TrendStatusTracking
inner join TrendStatus
on TrendStatusTracking.TrendStatusID = TrendStatus.Id
where TrendStatusTracking.TrendId = @TrendID
) src
pivot
(max(StatusDate)
  for TrendStatusName in ([Submitted], [Approved])) piv)

SELECT 
      TrendValueMap.Year,
	  TrendValueMap.Amount, 
	  TrendValueMap.Rationale,
	  Val.ValueName,
	  (SELECT SubmittedDate FROM CTE_TST WHERE  TrendType='Value' ) As ValueSubmittedDate,
	  (SELECT ApprovedDate FROM CTE_TST WHERE  TrendType='Value' )  As ValueApprovedDate
	   FROM TrendValueMap TrendValueMap
	   INNER JOIN ValueConversion Val ON Val.Id=TrendValueMap.ValueConversionId 
where TrendValueMap.TrendId = @TrendId 
      
END