﻿CREATE procedure [dbo].[SFab_GetMyViewPoint]
(
	@TrendId uniqueidentifier,
	@AviraUserID uniqueidentifier = null
	--@CustomerId uniqueidentifier
)
as
/*================================================================================
Procedure Name: SFab_GetMyViewPoint
Author: Jagan
Create date: 02/07/2019
Description: Get list from Company table by Name or ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
05/07/2019	Jagan			Initial Version
02/19/2020  Pooja			pass customerid to get data
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @TrendApprovedStatusId uniqueidentifier;
declare @CustomerId uniqueidentifier;

	select @TrendApprovedStatusId = Id
	from TrendStatus
	where TrendStatusName = 'Approved';

	select @CustomerId = CustomerId
	from AviraUser
	where id = @AviraUserID;
	

SELECT	[Id],
		[CustomerId],
		[Year],
		[TrendValue]
FROM [dbo].[TrendBuildMyViewPoint] VP
WHERE TrendId = @TrendId
AND CustomerId = @CustomerId
and exists(select 1 from TrendValueMap VM
			where VP.TrendId = VM.TrendId
			and VP.[Year] = VM.[Year]
			and VM.IsDeleted= 0)
AND VP.IsDeleted = 0
AND exists(select 1 from trend where VP.TrendId = Trend.Id and  Trend.TrendStatusID = @TrendApprovedStatusId)
END