﻿CREATE TYPE [dbo].[udtGUIDCompanySwotMapMultiValue] AS TABLE (
    [Id]              UNIQUEIDENTIFIER NOT NULL,
    [CompanyId]       UNIQUEIDENTIFIER NULL,
    [ProjectId]       UNIQUEIDENTIFIER NULL,
    [CategoryId]      UNIQUEIDENTIFIER NULL,
    [SWOTTypeName]    NVARCHAR (256)   NULL,
    [SWOTDescription] NVARCHAR (1024)  NULL);

