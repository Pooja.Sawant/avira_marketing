﻿
  
  
CREATE PROCEDURE [dbo].[SFab_GetMarketingSizingData]
(
 @SegmentID uniqueidentifier=null,
 @SegmentIdList dbo.[udtUniqueIdentifier] readonly,
 @RegionId uniqueidentifier = null,
 @CountryIdList dbo.[udtUniqueIdentifier] readonly,
 @TimeTagId uniqueidentifier = null,
 @CurrencyId uniqueidentifier = null,
 @MatricType nvarchar(100)=null
)  
AS  
/*================================================================================  
Procedure Name: [SFab_GetMarketingSizingData]  
Author: Swami Aluri  
Create date: 06/04/2019  
Description: Get list of Marketing sizing from table   
Change History  
Date        Developer     Reason  
__________  ____________  ______________________________________________________  
06/04/2019   Swami Aluri   Initial Version  
================================================================================*/  

BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	
    DECLARE @Years int;
    DECLARE @YearMin int;
    DECLARE @YearMax int;
    

    SELECT @Years = DurationYears FROM TimeTag WHERE Id =@TimeTagId 
    select @YearMin = BaseYear from BaseYearMaster
    select @YearMax = @YearMin + @Years

	;WITH segCTE AS (
				select id as SegmentId,
				SubSegmentName as SegmentName
				from SubSegment
				where SegmentId = @SegmentID
				and (id IN (SELECT id FROM @SegmentIdList) OR not exists(select 1 from @SegmentIdList ))
				and IsDeleted = 0
				)

, CTE_MV AS(
SELECT	   MV.[Value] AS [Value], 
		   MV.Volume AS Volume, 
		   MV.GrowthRatePercentage AS GrowthRatePercentage, 
		   MV.TrendGrowthRatePercentage AS TrendGrowthRatePercentage,
		   MV.Amount AS AMOUNT,
		   MV.[Year] AS Year,
		   MV.RegionId,
		   MV.AvgSalePrice AS AvgSalePrice,
		   GraphOptions AS GraphOptions,
		   MVSM.SubSegmentId as SegmentId

   FROM MarketValue as MV
   inner join MarketValueSegmentMapping MVSM
   on MVSM.MarketValueId = mv.Id
   and MVSM.SegmentId = @SegmentID
   and MVSM.SubSegmentId in (SELECT id FROM @SegmentIdList)
   WHERE mv.Year between @YearMin and @YearMax )


SELECT CTE_MV.SegmentId,
segCTE.SegmentName,
AVG(CTE_MV.[Value]) AS [Value],
AVG(CTE_MV.Volume) AS Volume, 
AVG(CTE_MV.GrowthRatePercentage) AS GrowthRatePercentage, 
AVG(CTE_MV.TrendGrowthRatePercentage) AS TrendGrowthRatePercentage,
AVG(CTE_MV.AMOUNT) AS AMOUNT,
CTE_MV.[Year],
AVG(CTE_MV.AvgSalePrice) AS AvgSalePrice,
--STRING_AGG(CTE_MV.GraphOptions, ',') WITHIN GROUP (ORDER BY CTE_MV.GraphOptions) as GraphOptions
MAX(CTE_MV.GraphOptions) AS GraphOptions 
FROM CTE_MV 
left JOIN segCTE ON CTE_MV.SegmentId = segCTE.SegmentId
where CTE_MV.RegionId = @RegionId
GROUP BY  CTE_MV.SegmentId,
segCTE.SegmentName,
CTE_MV.[Year]
ORDER BY segCTE.SegmentName,
CTE_MV.[Year]

END