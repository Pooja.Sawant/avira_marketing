﻿CREATE TABLE [dbo].[ProjectSegmentMapping](
    [ID] [uniqueidentifier] NOT NULL,
    [ProjectID] [uniqueidentifier] NULL,
    [SegmentType] [nvarchar](100) NULL,
    [SegmentID] [uniqueidentifier] NULL,
    [CreatedOn] [datetime2](7) NULL,
    [UserCreatedById] [uniqueidentifier] NULL,
    [ModifiedOn] [datetime2](7) NULL,
    [UserModifiedById] [uniqueidentifier] NULL,
    [IsDeleted] [bit] NULL,
    [DeletedOn] [datetime2](7) NULL,
    [UserDeletedById] [uniqueidentifier] NULL,
 CONSTRAINT [PK_ProjectSegmentMapping] PRIMARY KEY CLUSTERED 
(
    [ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProjectSegmentMapping]  WITH CHECK ADD  CONSTRAINT [FK__ProjectSe__Proje__414EAC47] FOREIGN KEY([ProjectID])
REFERENCES [dbo].[Project] ([ID])
GO
ALTER TABLE [dbo].[ProjectSegmentMapping] CHECK CONSTRAINT [FK__ProjectSe__Proje__414EAC47]
GO