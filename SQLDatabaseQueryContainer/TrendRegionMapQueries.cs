﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public class TrendRegionMapQueries
    {
        public static readonly string StoreProcInsertUpdate = "SApp_InsertUpdateTrendRegionMap";

        public static readonly string StoreProcInsertUpdateCountries = "SApp_InsertUpdateTrendCountryMap";

        public static string StoreProcSelectAll = "SApp_GetRegionByTrendID";

        public static string StoreProcCountryMapping = "SApp_GetCountrybyRegionID";

        public static string StoreProcSelectAllRegions = "SApp_GetRegionByTrendID";

        public static string StoreProcSelectAllCountries = "SApp_GetCountryByTrendID";
    }
}
