﻿CREATE TABLE [dbo].[AviraUser](
	[Id] [uniqueidentifier] NOT NULL,
	[FirstName] [nvarchar](64) NOT NULL,
	[LastName] [nvarchar](64) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[DisplayName] [nvarchar](128) NOT NULL,
	[EmailAddress] [nvarchar](128) NULL,
	[PhoneNumber] [nvarchar](64) NULL,
	[UserTypeId] [tinyint] NOT NULL,
	[PassWord] [nvarchar](500) NOT NULL,
	[CustomerId] [uniqueidentifier] NULL,
	[IsEverLoggedIn] [bit] NULL,
	[LastLoggedOn] [datetime2](7) NULL,
	[UserRoleId] [uniqueidentifier] NULL,
	[Salt] [nvarchar](128) NULL,
	[CreatedOn] [datetime2](7) NULL,
	[UserCreatedById] [uniqueidentifier] NULL,
	[ModifiedOn] [datetime2](7) NULL,
	[UserModifiedById] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeletedOn] [datetime2](7) NULL,
	[UserDeletedById] [uniqueidentifier] NULL,
	[ConcurrentLoginFailureCount] [tinyint] NULL,
	[IsUserLocked] [bit] NOT NULL,
	[DateofJoining] [datetime] NULL,
	[LocationID] [uniqueidentifier] NULL,
 CONSTRAINT [PK_AviraUser] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AviraUser] ADD  DEFAULT ('') FOR [PassWord]
GO
ALTER TABLE [dbo].[AviraUser] ADD  CONSTRAINT [DF_AviraUser_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[AviraUser] ADD  CONSTRAINT [DF_AviraUser_IsUserLocked]  DEFAULT ((0)) FOR [IsUserLocked]
GO
ALTER TABLE [dbo].[AviraUser]  WITH CHECK ADD  CONSTRAINT [FK_AviraUser_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([Id])
GO
ALTER TABLE [dbo].[AviraUser] CHECK CONSTRAINT [FK_AviraUser_Customer]
GO
ALTER TABLE [dbo].[AviraUser]  WITH CHECK ADD  CONSTRAINT [FK_AviraUser_Location] FOREIGN KEY([LocationID])
REFERENCES [dbo].[Location] ([Id])
GO
ALTER TABLE [dbo].[AviraUser] CHECK CONSTRAINT [FK_AviraUser_Location]
GO
ALTER TABLE [dbo].[AviraUser]  WITH CHECK ADD  CONSTRAINT [FK_AviraUser_UserRole] FOREIGN KEY([UserRoleId])
REFERENCES [dbo].[UserRole] ([Id])
GO
ALTER TABLE [dbo].[AviraUser] CHECK CONSTRAINT [FK_AviraUser_UserRole]
GO