﻿CREATE TABLE [dbo].[Market] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [MarketName]       NVARCHAR (256)   CONSTRAINT [DF_Market_MarketName] DEFAULT ('') NOT NULL,
    [Description]      NVARCHAR (256)   NULL,
    [ParentId]         UNIQUEIDENTIFIER NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_Market_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_Market] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Market_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_Market_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_Market_AviraUser2] FOREIGN KEY ([UserDeletedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_Market_ParentId] FOREIGN KEY ([ParentId]) REFERENCES [dbo].[Market] ([Id])
);

