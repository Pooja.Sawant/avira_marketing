﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.CompanyProfile
{
    public class CompanyOtherInfoViewModel
    {
        public Guid CompanyId { get; set; }
        public string TableInfoName { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public Guid Id { get; set; }
        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "CompanyOtherInfo";
            }
        }
    }
    public class CompanyOtherInfoInsertUpdateViewModel
    {
        public Guid CompanyId { get; set; }
        public string TableInfoName { get; set; }
        public string Description { get; set; }
        public CompanyNoteViewModel CompanyNoteViewModel { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public Guid Id { get; set; }

    }
    public class MainCompanyOtherInfoInsertUpdateViewModel
    {
        public Guid CompanyId { get; set; }
        public Guid ProjectId { get; set; }
        public CompanyNoteViewModel CompanyNoteViewModel { get; set; }
        public List<CompanyOtherInfoInsertUpdateViewModel> companyOtherInfoInsertUpdateViewModel { get; set; }
        public string jsonOtherInfo { get; set; }
        public Guid? UserCreatedById { get; set; }
    }
}
