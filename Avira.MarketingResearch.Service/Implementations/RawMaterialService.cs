﻿using Avira.MarketingResearch.Models.Entities;
using Avira.MarketingResearch.Models.Response;
using Avira.MarketingResearch.Repository;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Service.CoreInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Avira.MarketingResearch.Service.Implementations
{
    public class RawMaterialService : IRawMaterialService
    {
        private readonly IRawMaterialRepository _rawMaterialRepository;

        public RawMaterialService(IRawMaterialRepository rawMaterialRepository)
        {
            _rawMaterialRepository = rawMaterialRepository;
        }

        public void Add(RawMaterial rawMaterial)
        {
            _rawMaterialRepository.Insert(rawMaterial);
        }

        public async Task AddAsync(RawMaterial rawMaterial)
        {
            await _rawMaterialRepository.AddAsync(rawMaterial);
        }

        public void DeleteAsync(RawMaterial rawMaterial)
        {
            RawMaterialResponse rawmaterialResponse = new RawMaterialResponse();
            _rawMaterialRepository.DeleteAsync(rawMaterial);
        }

        public async Task<RawMaterialResponse> GetAllAsync()
        {
            RawMaterialResponse rawMaterialResponse = new RawMaterialResponse();
            IEnumerable<RawMaterial> rawmaterial = await _rawMaterialRepository.GetAllAsync();

            if (rawmaterial.ToList().Count == 0)
            {
                rawMaterialResponse.Message = "No record found for RawMaterial";
            }
            else
            {
                rawMaterialResponse.rawmaterial.AddRange(rawmaterial);
            }

            return rawMaterialResponse;
        }

        public async Task<RawMaterialResponse> GetAsync(Guid Id)
        {
            RawMaterialResponse rawmaterialResponse = new RawMaterialResponse();
            var rawmaterial = await _rawMaterialRepository.GetAsync(Id);

            if (rawmaterial == null)
            {
                rawmaterialResponse.Message = "No record found for RawMaterial";
            }
            else
            {
                rawmaterialResponse.rawmaterial.Add(rawmaterial);
            }

            return rawmaterialResponse;
        }

        public async Task UpdateAsync(RawMaterial rawmaterialRequest)
        {
            RawMaterial rawmaterial = new RawMaterial()
            {
                Id = rawmaterialRequest.Id,
                RawMaterialName = rawmaterialRequest.RawMaterialName,
                Description = rawmaterialRequest.Description,
                IsDeleted = rawmaterialRequest.IsDeleted,
            };

            await _rawMaterialRepository.UpdateAsync(rawmaterial);
        }
    }
}
