﻿CREATE TABLE [dbo].[TrendCategoryMap](
    [ID] [uniqueidentifier] NOT NULL,
    [TrendId] [uniqueidentifier] NOT NULL,
    [CategoryId] [uniqueidentifier] NOT NULL,
    [CreatedOn] [datetime2](7) NULL,
    [UserCreatedById] [uniqueidentifier] NULL,
    [ModifiedOn] [datetime2](7) NULL,
    [UsermodifiedById] [uniqueidentifier] NULL,
    [IsDeleted] [bit] NULL,
    [DeletedOn] [datetime2](7) NULL,
    [UserDeletedById] [uniqueidentifier] NULL,
PRIMARY KEY CLUSTERED 
(
    [ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO