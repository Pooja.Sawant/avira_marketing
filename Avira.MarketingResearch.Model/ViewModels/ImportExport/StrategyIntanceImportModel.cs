﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.ImportExport
{

    public class StrategyIntanceImportModel
    {
        public string TemplateName { get; set; }
        public string ImportFileName { get; set; }
        public string JsonStrategyList { get; set; }

        public IEnumerable<StrategyIntanceModel> StrategyList { get; set; }
        public Guid UserCreatedById { get; set; }
    }

    public class StrategyIntanceModel
    {
        public int RowNo { get; set; }
        public string MarketName { get; set; }
        public DateTime? Date { get; set; }
        public string CompanyName { get; set; }
        public string SegmentName { get; set; }
        public string StrategyType { get; set; }
        public string StrategyDescription { get; set; }
        public string Importance { get; set; }
        public string Impact { get; set; }
    }

}
