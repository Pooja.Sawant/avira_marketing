﻿
  
CREATE procedure [dbo].[SFab_ActivatedCustomerUserReg]  
(@Id uniqueidentifier)  
as  
/*================================================================================  
Procedure Name: SFab_ActivatedCustomerUserReg  
Author: Pooja Sawant  
Create date: 05/15/2019  
Description: Insert a record into AviraUser/Customer/Location table from CustomerRequest table by id
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
05/15/2019 Pooja   Initial Version  
08/31/2019 Praveen Added random number for Salt 
================================================================================*/  
BEGIN  
SET NOCOUNT ON;  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
declare @ErrorMsg NVARCHAR(2048);  
  
--Validations  
if not exists (select 1 from [dbo].[CustomerRequest] where ID=@Id and CreatedOn  >= DATEADD(day,-2, getdate()) 
and   CreatedOn  <= getdate() )
	begin  	
     set @ErrorMsg = 'Your activation link is expired, or if you alredy activated please login with credentials';  
     SELECT StoredProcedureName ='SFab_ActivatedCustomerUserReg',Message =@ErrorMsg,Success = 0;    
     RETURN;    
    end
  
if exists (select 1 from [dbo].[AviraUser] where UserName=(select Email from CustomerRequest where ID=@Id ) )
	begin  	
     set @ErrorMsg = 'User is already exists.';  
     SELECT StoredProcedureName ='SFab_ActivatedCustomerUserReg',Message =@ErrorMsg,Success = 0;    
     RETURN;    
    end
BEGIN TRY  
--Insert into Customer table from customerrequest table
declare @custmerid uniqueidentifier
set @custmerid=NEWID()
INSERT INTO Customer (Id, CustomerName, DisplayName, CreatedOn ,IsDeleted,PrimaryPurpose,FunctionalArea,PrimaryPurposeId,FunctionalAreaId)
SELECT @custmerid,CustomerRequest.Company, CustomerRequest.FullName, GETDATE(),0,CustomerRequest.PrimaryPurpose,CustomerRequest.FunctionalArea,CustomerRequest.PrimaryPurposeId ,CustomerRequest.FunctionalAreaId
FROM CustomerRequest																											
WHERE CustomerRequest.ID = @Id;


--end
--Insert into Location table from customerrequest table
declare @locationid uniqueidentifier
set @locationid=NEWID()
INSERT INTO Location (Id, LocationName, Description, Address1,Address2,City,State,Zip,CountryId,CreatedOn,UserCreatedById ,IsDeleted)
SELECT @locationid,CustomerRequest.AddressLine1, CustomerRequest.AddressLine1,CustomerRequest.AddressLine1,CustomerRequest.AddressLine2
,'NA',CustomerRequest.State,CustomerRequest.ZipCode,CustomerRequest.Country, GETDATE(),CustomerRequest.UserCreatedById,0
FROM CustomerRequest
WHERE CustomerRequest.ID = @Id;

--end
--Insert into Avira table from customerrequest table, location table, userrole table
declare @UserRoleId uniqueidentifier
set @UserRoleId=(select id from UserRole where UserRoleName='Primary Contact')

declare @avirauserid uniqueidentifier--, @Salt smallint 
set @avirauserid=NEWID()
--set @Salt = convert(numeric(4,0),rand() * 8999) + 1000

INSERT INTO[dbo].[AviraUser]  ([Id] ,[FirstName],[LastName]  
           ,[UserName]  ,[DisplayName] ,[EmailAddress]  
           ,[PhoneNumber] ,[UserTypeId] ,[PassWord],[CustomerId],
		   [IsEverLoggedIn],[UserRoleId] ,[LocationID] ,[Salt],[CreatedOn] ,[UserCreatedById]  ,[IsDeleted] ,[DateofJoining],[IsUserLocked])
     SELECT @avirauserid,CustomerRequest.FullName, '.',CustomerRequest.Email, CustomerRequest.FullName,CustomerRequest.Email,
	 CustomerRequest.HandPhone,2,CustomerRequest.Password,@custmerid,0,@UserRoleId, @locationid,Salt,GETDATE(),
	 CustomerRequest.UserCreatedById,0,GETDATE(),0
     FROM CustomerRequest
     WHERE CustomerRequest.ID=@Id ;

--end
--Insert into AviraUserRoleMap table

INSERT INTO[dbo].[AviraUserRoleMap]  ([Id] ,[AviraUserId],[UserRoleId]  
           ,[CreatedOn] ,[UserCreatedById]  ,[IsDeleted] ,[IsPrimary])
     SELECT NEWID(),@avirauserid, @UserRoleId,GETDATE(), CustomerRequest.UserCreatedById,0,1
     FROM CustomerRequest
     WHERE CustomerRequest.ID=@Id ;

--end
--Hard Delete record from CustomerRequest table
 delete from CustomerRequest where ID=@Id ;
--end
SELECT StoredProcedureName ='SFab_ActivatedCustomerUserReg',Message = @ErrorMsg,Success = 1;  
END TRY  
BEGIN CATCH  
    -- Execute the error retrieval routine.  
 DECLARE @ErrorNumber int;  
 DECLARE @ErrorSeverity int;  
 DECLARE @ErrorProcedure varchar(100);  
 DECLARE @ErrorLine int;  
 DECLARE @ErrorMessage varchar(500);  
  
  SELECT @ErrorNumber = ERROR_NUMBER(),  
        @ErrorSeverity = ERROR_SEVERITY(),  
        @ErrorProcedure = ERROR_PROCEDURE(),  
        @ErrorLine = ERROR_LINE(),  
        @ErrorMessage = ERROR_MESSAGE()  
  
 insert into dbo.Errorlog(ErrorNumber,  
       ErrorSeverity,  
       ErrorProcedure,  
       ErrorLine,  
       ErrorMessage,  
       ErrorDate)  
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());  
  
 set @ErrorMsg = 'Error while creating a new AviraUser and Customere, please contact Admin for more details.';  
  
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;  
  
END CATCH  
  
  
end