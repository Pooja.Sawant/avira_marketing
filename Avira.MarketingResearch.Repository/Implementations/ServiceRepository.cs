﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations
{
    public class ServiceRepository : IServiceRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "Service";
        public ServiceRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }


        public ServiceViewModel GetById(Guid guid)
        {
            ServiceViewModel serviceViewModel = new ServiceViewModel();
            serviceViewModel = GetAllService(guid).FirstOrDefault();
            return serviceViewModel;
        }

        public IEnumerable<ServiceViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted)
        {
            return GetAllService(null, PageStart, pageSize, SortDirection, OrdbyByColumnName,Search, includeDeleted);
        }

        public IEnumerable<ServiceViewModel> GetAllParentService()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<ServiceViewModel>(ServiceQueries.StoreProcSelectAllParent
                            , null
                            , null, true, null, CommandType.StoredProcedure);
        }

        private IEnumerable<ServiceViewModel> GetAllService(Guid? guid, int PageStart = 0, int pageSize = 0, int SortDirection = -1, string OrdbyByColumnName = "", string Search = "", bool includeDeleted = false)
        {
            DataTable serviceIdsTable = new DataTable();
            serviceIdsTable.Columns.Add("ID", typeof(Guid));

            if (guid != null)
            {
                serviceIdsTable.Rows.Add(guid);

            }

            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<ServiceViewModel>(ServiceQueries.StoreProcSelectAll
                            , new { @serviceIDList = serviceIdsTable.AsTableValuedParameter("dbo.udtUniqueIdentifier"), @includeDeleted = includeDeleted, @PageStart = PageStart, @pageSize = pageSize, @SortDirection = SortDirection, @OrdbyByColumnName = OrdbyByColumnName, @ServiceName=Search }
                            , null, true, null, CommandType.StoredProcedure);
        }

        public Result<SpTransactionMessage> Create(ServiceInsertDbViewModel serviceInsertDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(ServiceQueries.StoreProcInsert, serviceInsertDbViewModel, CommandType.StoredProcedure);
            return spTransactionMessage;

        }

        public Result<SpTransactionMessage> Update(ServiceUpdateDbViewModel serviceUpdateDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(ServiceQueries.StoreProcUpdate, serviceUpdateDbViewModel, CommandType.StoredProcedure);
            return spTransactionMessage;
        }

    }
}
