﻿
 CREATE PROCEDURE [dbo].[SFab_GetCompanyProjectMapbyID]        
 (@ProjectID uniqueidentifier = null, 
  @CompanyID uniqueidentifier = null,   
  @includeDeleted bit = 0    
 )        
 AS        
/*================================================================================        
Procedure Name: [SFab_GetCompanyProjectMapbyID]  
Author: Harshal        
Create date: 07/13/2019        
Description: Get list from CompanyProjectMap table by ProjectID and CompanyId       
Change History        
Date  Developer  Reason        
__________ ____________ ______________________________________________________        
07/13/2019 Harshal   Initial Version        
================================================================================*/       
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if @includeDeleted is null set @includeDeleted = 0

SELECT  [CompanyProjectMap].CompanyId,
		[CompanyCode],
		[CompanyName],
		[Description],
		[Telephone],
		[Telephone2],
		[Email],
		[Email2],
		[Fax],
		[Fax2],
		[Website],
		[ParentCompanyName],
		[ProfiledCompanyName],
		[NoOfEmployees],
		[YearOfEstb],
		[ParentCompanyYearOfEstb],
		[RankNumber],
		[Project].Id As ProjectId,
		[Project].ProjectName
FROM [dbo].[CompanyProjectMap]
INNER JOIN [dbo].[Company] ON [Company].Id=[CompanyProjectMap].CompanyId
INNER JOIN [dbo].[Project] ON [Project].ID=[CompanyProjectMap].ProjectId
WHERE ([CompanyProjectMap].CompanyId=@CompanyID AND [CompanyProjectMap].ProjectId=@ProjectID)
AND (@includeDeleted = 1 or [Company].IsDeleted = 0)

end