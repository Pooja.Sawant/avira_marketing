﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService
{
    public interface IMenuService
    {
        IEnumerable<MenuViewModel> GetAll(Guid Id);
        IEnumerable<MenuViewModel> GetAll();

        Result<SpTransactionMessage> Create(MenuInsertDbViewModel tfc, AviraUser currentUser);
        Result<SpTransactionMessage> Update(MenuUpdateDbViewModel tfc, AviraUser currenUser);
    }
}
