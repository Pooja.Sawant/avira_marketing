﻿
CREATE procedure [dbo].[SFab_GetSubsidaryCompanyListbyCompanyId]
(@CompanyID uniqueidentifier)
as
/*================================================================================
Procedure Name: [SFab_GetSubsidaryCompanyListbyCompanyId]
Author: Harshal
Create date: 07/05/2020
Description: Get list from Company table by ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
07/05/2020	Harshal		Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT  [Id] as CompanySubsidaryId,
		[CompanyName] as SubsidaryCompanyName,
		[CompanyLocation] as SubsidaryCompanyLocation,		
		[IsHeadQuarters]
FROM [dbo].[Company]
where [Company].ParentId = @CompanyID and [Company].[IsDeleted] = 0

end