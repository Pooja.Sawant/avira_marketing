﻿
CREATE PROCEDURE [dbo].[SApp_GetFinancialByCompanyID]             
 @CompanyID uniqueidentifier = null,         
 @includeDeleted bit = 0             
AS
/*================================================================================  
Procedure Name: [SApp_GetFinancialByCompanyID]  
Author: Harshal 
Create date: 08/09/2019  
Description:Getting Data From Table WRT CompanyId 
Change History  
Date        Developer     Reason  
__________  ____________  ______________________________________________________  
08/09/2019  Harshal   Initial Version  
================================================================================*/          
BEGIN         
 SET NOCOUNT ON;          
 SELECT  Id,
CompanyId,
CurrencyId,
ValueConversionId,
CompanyName,
Currency,
Units,
Year,
Revenues,
DirectCost,
CostofProduction,
CostofSales,
ResearchandDevelopmentExpenses,
SalesandMarketingCost,
SellingandInformationalandAdministrativeExpenses,
Depreciation,
Amortization,
DepreciationandAmortization,
InterestExpense,
TaxExpense,
ProvisionorBenefitforTaxesonIncome,
IncomefromContinuingOperations,
NetIncome,
DilutedEPS,
WeightedAverageSharesOutstanding,
LongTermDebt,
ShortTermDebt,
ShortTermBorrowings,
CurrentPortionofLongTermDebt,
CashandCashEquivalents,
ShortTermInvestments,
MarketableSecurities,
MinorityInterest,
NonControllingInterests,
LongTermInvestments,
NetcashProvidedbyorUsedinOperatingActivities,
NetcashProvidedbyorUsedinInvestingActivities,
NetCashProvidedbyorUsedinFinancingActivities,
PurchasesofPropertyandPlantandEquipment,
PurchaseofFixedAssets,
PurchasesofShortTermInvestments,
PurchasesofLongTermInvestments,
TotalCurrentAssets,
Inventories,
AccountsReceivables,
TotalCurrentLiabilities,
PropertyandPlantandEquipment,
OtherFixedAssets,
OtherTangibleAssets,
IntangibleAssets,
Goodwill,
OtherNoncurrentLiabilities,
TotalshareholdersEquity,
RetainedEarnings,
AccumulatedIncomeandReserves,
AccountsPayable,
PurchasesofFixedAssets,
EffectiveTaxRate,
TotalPurchases,
IHc,
EHc,
TotalReportableSegments,
UnitedStates,
DevelopedEurope_a,
DevelopedRestofWorld_b,
EmergingMarkets_c,
TotalGeographicSegments
FROM [dbo].[CompanyProfilesFinancial]        
  
  WHERE ([CompanyProfilesFinancial].CompanyId = @CompanyID)              
 AND (@includeDeleted = 0 or [CompanyProfilesFinancial].IsDeleted = 0)          
END