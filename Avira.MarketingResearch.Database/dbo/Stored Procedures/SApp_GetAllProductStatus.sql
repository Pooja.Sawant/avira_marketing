﻿

  
CREATE PROCEDURE [dbo].[SApp_GetAllProductStatus]    
 AS  
/*================================================================================  
Procedure Name: [SApp_GetAllProductStatus]  
Author: Harshal  
Create date: 05/15/2019  
Description: Get All Recordes from ProductStatus Table  
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
05/15/2019 Harshal   Initial Version  
================================================================================*/  
  
BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
	
	SELECT Id , ProductStatusName, [Description] AS ImportanceDescription FROM ProductStatus
	WHERE IsDeleted = 0 
	order by ProductStatusName asc

END