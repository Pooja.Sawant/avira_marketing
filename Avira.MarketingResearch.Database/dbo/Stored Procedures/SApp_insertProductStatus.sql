﻿



CREATE procedure [dbo].[SApp_insertProductStatus]
(@Id uniqueidentifier,
@ProductStatusName nvarchar(256),
@Description nvarchar(256),
@UserCreatedById uniqueidentifier, 
@CreatedOn Datetime)
as
/*================================================================================
Procedure Name: SApp_insertProductStatus
Author: Swami
Create date: 10/19/2019
Description: Insert a record into Industry table
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
03/19/2019	Swami			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ErrorMsg NVARCHAR(2048);

--Validations
IF exists(select 1 from [dbo].ProductStatus 
		 where ProductStatusName = @ProductStatusName AND IsDeleted = 0)
begin
--declare @ErrorMsg NVARCHAR(2048);
set @ErrorMsg = 'ProductStatus with Name "'+ @ProductStatusName + '" already exists.';
--THROW 50000,@ErrorMsg,1;
SELECT StoredProcedureName ='SApp_insertProductStatus',Message =@ErrorMsg,Success = 0;  
RETURN;  

return(1)
end

--End of Validations
BEGIN TRY

insert into [dbo].ProductStatus([Id],
								ProductStatusName,
								[Description],
								[IsDeleted],
								[UserCreatedById],
								[CreatedOn])
	values(@Id, 
			@ProductStatusName, 
			@Description,
			0,
			@UserCreatedById, 
			@CreatedOn);

SELECT StoredProcedureName ='SApp_insertProductStatus',Message =@ErrorMsg,Success = 1;     

END TRY
BEGIN CATCH
    -- Execute the error retrieval routine.
	DECLARE @ErrorNumber	int;
	DECLARE @ErrorSeverity	int;
	DECLARE @ErrorProcedure	varchar;
	DECLARE @ErrorLine	int;
	DECLARE @ErrorMessage	varchar;

	 SELECT @ErrorNumber = ERROR_NUMBER(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorProcedure = ERROR_PROCEDURE(),
        @ErrorLine = ERROR_LINE(),
        @ErrorMessage = ERROR_MESSAGE()

	insert into dbo.Errorlog(ErrorNumber,
							ErrorSeverity,
							ErrorProcedure,
							ErrorLine,
							ErrorMessage,
							ErrorDate)
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());

 set @ErrorMsg = 'Error while creating a new ProductStatus, please contact Admin for more details.';    
--THROW 50000,@ErrorMsg,1;      
--return(1)  
SELECT Number = @ErrorNumber, 
		Severity =@ErrorSeverity,
		StoredProcedureName =@ErrorProcedure,
		LineNumber= @ErrorLine,
		Message =@ErrorMsg,
		Success = 0;     
END CATCH
end