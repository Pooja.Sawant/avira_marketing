﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels
{
    public class AviraUser
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string DisplayName { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public bool? IsEverLoggedIn { get; set; }
        public DateTime? LastLoggedOn { get; set; }
        public DateTime? CreatedOn { get; set; }
        public Guid? UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        //public Guid UserTypeId { get; set; }
        public int UserTypeId { get; set; }
        public Guid? CustomerId { get; set; }
        public string PassWord { get; set; }
        public Guid? UserRoleId { get; set; }
        public string salt { get; set; }
        public bool IsUserLocked { get; set; }
        public Int16 ConcurrentLoginFailureCount { get; set; }
        public int TotalItems { get; set; }
    }

    public class GetAllAviraUsers
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserName { get; set; }

        public string DisplayName { get; set; }

        public string EmailAddress { get; set; }

        public string PhoneNumber { get; set; }

        public bool? IsEverLoggedIn { get; set; }

        public DateTime? LastLoggedOn { get; set; }

        public DateTime? CreatedOn { get; set; }

        public Guid? UserCreatedById { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public Guid? UserModifiedById { get; set; }

        public bool? IsDeleted { get; set; }

        public DateTime? DeletedOn { get; set; }

        public Guid? UserDeletedById { get; set; }

        public int UserTypeId { get; set; }

        public Guid? CustomerId { get; set; }

        public string PassWord { get; set; }

        public string salt { get; set; }

        public bool IsUserLocked { get; set; }

        public Int16 ConcurrentLoginFailureCount { get; set; }

        public Guid? UserRoleId { get; set; }
        public string UserRoleName { get; set; }

        public Guid LocationId { get; set; }
        public string LocationName { get; set; }

        public int TotalItems { get; set; }
    }


    public class AviraUserInsertDbViewModel

    {
        public Guid? AviraUserID { get; set; }

        /// <summary>
        /// User first name
        /// </summary>
        [Required(ErrorMessage = "First name is required")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        /// <summary>
        /// User last name
        /// </summary>
        [Required(ErrorMessage = "Last name is required")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        /// <summary>
        /// User name
        /// </summary>
        [Required(ErrorMessage = "User name name is required")]
        [Display(Name = "User Name")]
        public string Username { get; set; }

        /// <summary>
        /// Display name
        /// </summary>
        [Required(ErrorMessage = "Display name is required")]
        [Display(Name = "Display Name")]
        public string DisplayName { get; set; }

        /// <summary>
                ///  User email
                /// </summary>
        [Required(ErrorMessage = "Email Address is required")]
        [RegularExpression(pattern: "[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,}", ErrorMessage = "Please enter a valid Email.")]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }

        /// <summary>
        /// User phoneNumber
        /// </summary>
        [Required(ErrorMessage = "Phone Number is required")]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// User password
        /// </summary>
        [Required(ErrorMessage = "Password Is Required")]
        [StringLength(255, ErrorMessage = "Must Be Greater Than Or Equal To 8 characters", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string PassWord { get; set; }

        /// <summary>
        /// User ConfirmPassWord
        /// </summary>
        //[Required(ErrorMessage = "Confirm Password Is Required")]
        //[StringLength(255, ErrorMessage = "Must Be Greater Than Or Equal To 8 characters", MinimumLength = 8)]
        //[Compare("PassWord", ErrorMessage = "Password and Confirm Password must match.")]
        //[DataType(DataType.Password)]
        //[Display(Name = "Confirm Password")]
        //public string ConfirmPassWord { get; set; }

        [Required(ErrorMessage = "Date Of Joining is required")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Date Of Joining")]
        public string DateofJoining { get; set; }

        //[Required(ErrorMessage = "Location Is Required")]
        //[Display(Name = "Select Location")]
        //[DataType(DataType.Text)]
        //public int LocationID { get; set; }

        public bool? IsEverLoggedIn { get; set; }

        public DateTime? CreatedOn { get; set; }

        public Guid? UserCreatedById { get; set; }

        public bool? IsDeleted { get; set; }

        public int UserTypeId { get; set; }

        public Guid? CustomerId { get; set; }

        //public Guid? Id { get; set; }

        //[Required(ErrorMessage = "User Role Is Required")]
        //[Display(Name = "Select User Role")]
        //public Guid? UserRoleId { get; set; }
        //public List<Guid> UserRoleIDList { get; set; }
        public List<UserRoleList> UserRolelist { get; set; }
        //public string UserRoleName { get; set; }

        public string salt { get; set; }

        public Guid LocationID { get; set; }

    }

    public class AviraUserInsertRequest

    {
        public Guid? AviraUserID { get; set; }

        /// <summary>
        /// User first name
        /// </summary>
        [Required(ErrorMessage = "First name is required")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        /// <summary>
        /// User last name
        /// </summary>
        [Required(ErrorMessage = "Last name is required")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        /// <summary>
        /// User name
        /// </summary>
        [Required(ErrorMessage = "User name name is required")]
        [Display(Name = "User Name")]
        public string Username { get; set; }

        /// <summary>
        /// Display name
        /// </summary>
        [Required(ErrorMessage = "Display name is required")]
        [Display(Name = "Display Name")]
        public string DisplayName { get; set; }

        /// <summary>
        ///  User email
        /// </summary>
        [Required(ErrorMessage = "Email Address is required")]
        [RegularExpression(pattern: "[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,}", ErrorMessage = "Please enter a valid Email.")]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }

        /// <summary>
        /// User phoneNumber
        /// </summary>
        [Required(ErrorMessage = "Phone Number is required")]
        [Display(Name = "Phone Number")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Please enter valid Phone Number.")]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// User password
        /// </summary>
        [Required(ErrorMessage = "Password Is Required")]
        //[StringLength(255, ErrorMessage = "Must Be Greater Than Or Equal To 8 characters", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [RegularExpression(@"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$", ErrorMessage = "Please enter one Captial letter, one special character, one numeric value and min 8 characters.")]
        public string PassWord { get; set; }

        /// <summary>
        /// User ConfirmPassWord
        /// </summary>
        [Required(ErrorMessage = "Confirm Password Is Required")]
        [StringLength(255, ErrorMessage = "Must Be Greater Than Or Equal To 8 characters", MinimumLength = 8)]
        [Compare("PassWord", ErrorMessage = "Password and Confirm Password must match.")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        public string ConfirmPassWord { get; set; }

        [Required(ErrorMessage = "Date Of Joining is required")]
        //[DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Date Of Joining")]
        public string DateofJoining { get; set; }

        [Required(ErrorMessage = "Location Is Required")]
        [Display(Name = "Select Location")]
        [DataType(DataType.Text)]
        public Guid LocationID { get; set; }

        public bool? IsEverLoggedIn { get; set; }

        public DateTime? CreatedOn { get; set; }

        public Guid? UserCreatedById { get; set; }

        public bool? IsDeleted { get; set; }

        public int UserTypeId { get; set; }

        public Guid? CustomerId { get; set; }

        //public Guid? Id { get; set; }

        //[Required(ErrorMessage = "User Role Is Required")]
        //[Display(Name = "Select User Role")]
        //public Guid? UserRoleId { get; set; }

        public List<Guid> UserRoleIDList { get; set; }
        public List<bool> IsPrimary { get; set; }

        //public List<UserRoleList> UserRolelist { get; set; }
        //public string UserRoleName { get; set; }

        public string salt { get; set; }
    }

    public class UserRoleList
    {
        public Guid UserRoleID { get; set; }
        public bool IsPrimary { get; set; }
    }

    public class AviraUserLogin
    {
        [Required(ErrorMessage = "Please enter Email Address.")]
        [RegularExpression(pattern: "[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,}", ErrorMessage = "Please enter a valid Email.")]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "Please Enter Password.")]
        [Display(Name = "Password")]
        public string PassWord { get; set; }
    }

    public class AviraUserChangePassword
    {
        public Guid? AviraUserID { get; set; }

        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "Please Enter OldPassword.")]
        [Display(Name = "OldPassword")]
        public string OldPassWord { get; set; }

        [Required(ErrorMessage = "Please Enter NewPassword.")]
        [RegularExpression(@"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$", ErrorMessage = "Please enter one Captial letter, one special character, one numeric value and min 8 characters.")]
        [DataType(DataType.Password)]
        [Display(Name = "NewPassword")]
        public string NewPassWord { get; set; }

        [Required(ErrorMessage = "Please Enter ConfirmPassword.")]
        [Compare("NewPassWord", ErrorMessage = "NewPassWord and ConfirmPassword must match.")]
        [DataType(DataType.Password)]
        [Display(Name = "ConfirmPassword")]
        public string ConfirmPassWord { get; set; }
    }
}
