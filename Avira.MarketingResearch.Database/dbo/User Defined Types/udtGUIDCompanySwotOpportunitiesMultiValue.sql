﻿CREATE TYPE [dbo].[udtGUIDCompanySwotOpportunitiesMultiValue] AS TABLE (
    [Id]                    UNIQUEIDENTIFIER NOT NULL,
    [CompanyId]             UNIQUEIDENTIFIER NULL,
    [OpportunitiesCategory] UNIQUEIDENTIFIER NULL,
    [Description]           NVARCHAR (512)   NULL);

