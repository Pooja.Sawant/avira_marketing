﻿using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using static Avira.MarketingResearch.Model.ViewModels.ComponentViewModel;

namespace Avira.MarketingResearch.Common.Service_Implementation
{
    public class ComponentService:IComponentService
    {
        private readonly IComponentRepository _ComponentRepository;

        public ComponentService(IComponentRepository buildLevelRepository)
        {
            _ComponentRepository = buildLevelRepository;
        }

        public Result<SpTransactionMessage> Create(ComponentInsertDbViewModel tfc, AviraUser currentUser)
        {
            return _ComponentRepository.Create(tfc);
        }


        public IEnumerable<ComponentViewModel> GetAll(Guid Id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ComponentViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false)
        {
            return _ComponentRepository.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName,Search, isDeletedInclude);
        }

        public ComponentViewModel GetById(Guid guid)
        {
            return _ComponentRepository.GetById(guid);
        }

        public Result<SpTransactionMessage> Update(ComponentUpdateDbViewModel tfc, AviraUser currenUser)
        {
            return _ComponentRepository.Update(tfc);
        }
    }
}
