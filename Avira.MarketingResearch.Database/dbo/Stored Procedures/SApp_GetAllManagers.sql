﻿CREATE procedure [dbo].[SApp_GetAllManagers]
(
	@CompanyId uniqueidentifier
)
as
/*================================================================================
Procedure Name: [SApp_GetAllManagers]
Author: Sai Krishna
Create date: 05/02/2019
Description: Get list from Region
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
05/06/2019	Sai Krishna		Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT Id as ManagerId,
		KeyEmployeeName as ManagerName
FROM CompanyKeyEmployeesMap
WHERE IsDeleted = 0
And CompanyId = @CompanyId

END