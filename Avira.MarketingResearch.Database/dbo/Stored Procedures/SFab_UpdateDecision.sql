﻿CREATE PROCEDURE [dbo].[SFab_UpdateDecision]
(
	@DecisionId uniqueidentifier,
	@ResDecisionId uniqueidentifier Out,
	@BusinessObjectivesJsonList nvarchar(max) = null,
	@TargetedOutcomeJsonList nvarchar(max),
	@ResourceAssigned nvarchar(512),
	@DecisionDeadLine datetime2(7),
	@UserId	uniqueidentifier
)
As   

/*================================================================================    
Procedure Name: SFab_UpdateDecision   
Author: Praveen    
Create date: 07-Jan-2020
Description: Update new decision by user
Change History    
Date  Developer  Reason  
__________ ____________ ______________________________________________________    
07-Jan-2020  Praveen   Initial Version    
28-Jan-2020  Praveen   Added new paramenter and implement insertion for BusinessObjectives
================================================================================*/    
BEGIN    

--Declare @newDecisionId uniqueidentifier
--set  @NewDecisionId = newid()

------------------
	--Required Tables: Decision,  DecisionOutcome
	--Notes: N/A
	--Case 1: N/A
	--Expected Result :SELECT StoredProcedureName ='SFab_UpdateDecision',Message = @ErrorMsg,Success = 1/0;
	
Declare @ErrorMsg NVARCHAR(2048) 
declare @DecisionStatusDecisionMadeId uniqueidentifier  
Select @DecisionStatusDecisionMadeId = ID From LookupStatus
	Where StatusName = 'Decision made' And StatusType = 'DMDecisionStatus'
	BEGIN TRY
		If Exists (Select ID from Decision Where Id = @DecisionId and UserId= @UserID and StatusId=@DecisionStatusDecisionMadeId)
	Begin
        	set @ErrorMsg = 'Can not add/edit any new details since this decision has been made.';   
        		SELECT StoredProcedureName ='SFab_UpdateDecision','Message' =@ErrorMsg,'Success' = 0;  
    End
	else 
	begin
		Execute SFab_CopyDecisionFromBase @InDecisionId = @DecisionId, @UserId = @UserId, @CopyDecisionParametersFromBase = 0, @OutDecisionId = @DecisionId OUTPUT
			
		Update [dbo].[Decision]
		SET [ResourceAssigned]= @ResourceAssigned,
			[Deadline] = @DecisionDeadLine,
			[UserId]= @UserId,           
			[UserCreatedById]=@UserId
		Where Id = @DecisionId And UserId = @UserId
		
		IF(ISNULL(@BusinessObjectivesJsonList, '') != '' AND LEN(ISNULL(@BusinessObjectivesJsonList, '')) > 1)
	BEGIN

		DELETE FROM DecisionObjective Where DecisionId = @DecisionId			

		INSERT INTO DecisionObjective (Id,DecisionId, BusinessObjective, CreatedOn, UserCreatedById, SeqNumber)  
		SELECT NEWID(), @DecisionId ,businessobjective, GETDATE(), @UserId, ROW_NUMBER() OVER (order by (select 1))
		FROM OPENJSON(@BusinessObjectivesJsonList)  
		WITH (      
			BusinessObjective NVARCHAR(1024) '$.BusinessObjective'  
		);  
  END

		IF(ISNULL(@TargetedOutcomeJsonList, '') != '' AND LEN(ISNULL(@TargetedOutcomeJsonList, '')) > 1)
		BEGIN
			
			Delete From DecisionOutcome Where DecisionId = @DecisionId			

			INSERT INTO DecisionOutcome (Id, DecisionId, TargetedOutcome, CreatedOn, UserCreatedById, SeqNumber)  
				SELECT NEWID(), @DecisionId ,TargetedOutcome, GETDATE(), @UserId, ROW_NUMBER() OVER (order by (select 1))
				FROM OPENJSON(@TargetedOutcomeJsonList)  
				WITH (      
					TargetedOutcome NVARCHAR(1024) '$.TargetedOutcome'  
				);
		END			
		SET @ResDecisionId = @DecisionId
		SELECT StoredProcedureName ='SFab_UpdateDecision', Message = @ErrorMsg, Success = 1;   
	end
	END TRY
	BEGIN CATCH      
    -- Execute the error retrieval routine.      
		exec  [dbo].[SApp_LogErrorInfo]    
    
		set @ErrorMsg = 'Error while updating Decision, please contact Admin for more details.'; 
		SELECT StoredProcedureName ='SFab_UpdateDecision', Message = @ErrorMsg, Success = 0;   
  
      END CATCH 
END