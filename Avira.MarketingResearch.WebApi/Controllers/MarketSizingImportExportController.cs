﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.ImportExport;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MarketSizingImportExportController : AviraBaseController
    {
        private ILoggerManager _logger;
        public readonly IMarketSizingImportExportService _marketSizingImportExportService;

        public MarketSizingImportExportController(
            IMarketSizingImportExportService marketSizingImportExportService,
            ILoggerManager logger)
        {
            _marketSizingImportExportService = marketSizingImportExportService;
            _logger = logger;
        }

        [HttpPost]
        public ActionResult Post([FromBody] MarketSizingImportInsertViewModel viewModel)
        {
            viewModel.UserCreatedById = CurrentUser.Id;
            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new MarketSizing, please contact Admin for more details."
            };
            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                var result = _marketSizingImportExportService.Create(viewModel, CurrentUser);

                var res = result.Where(s => s.ErrorNotes != null).ToList();
                if (res.Count <= 0 || res.First().ErrorNotes==null)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "MarketSizing Details successfully Imported"; 
                }
                else
                {
                    viewData.Message = "Import marketSizing have some errors";
                    viewData.IsSuccess = false;
                    //Export to Excel data.
                    viewData.Data = result.ToList();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Ok(viewData);
        }
    }
}