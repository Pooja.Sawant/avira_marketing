﻿using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;

namespace Avira.MarketingResearch.Web.ImportExportUtility
{
    public class CompanyBulkImport : IExcelImport
    {
        public readonly IServiceRepository _serviceRepository;
        private ILoggerManager _logger;

        public CompanyBulkImport(IServiceRepository serviceRepository, ILoggerManager logger)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
        }

        public ImportResponse Import(IFormFile formFile)
        {
            var res = new ImportResponse();
            var inputModel = new CompanyBulkImportViewModel();
            using (var stream = new MemoryStream())
            {
                formFile.CopyTo(stream);

                using (var package = new ExcelPackage(stream))
                {
                    try
                    {
                        if (package.Workbook.Worksheets["CompanyData"] != null)
                        {
                            inputModel.CompanyBasicInfoList = GetBulkCompanyBasic(package.Workbook.Worksheets["CompanyData"]);
                            inputModel.CompanyFinancialList = GetBulkCompanyFinancial(package.Workbook.Worksheets["CompanyData"]);
                                                        
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'CompanyData' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'CompanyData' worksheet is missing.");
                            return res;
                        }

                    }
                    catch (Exception ex)
                    {
                        res.ResponseStatus = "Error";
                        res.ResponseMessage = "Error while reading template. Error Message: " + ex.Message + ". Please check input excel. More details logged in error log.";
                        res.TotalFailedRecords = 1;
                        _logger.LogError("Error Message: " + ex.Message + " Stack Trace: " + ex.StackTrace.ToString());
                        return res;
                    }
                }
            }

            //--------Call api to post data            
            inputModel.TemplateName = "Company Bulk Import Template";
            inputModel.ImportFileName = formFile.FileName;

            try
            {

                HttpResponseMessage response = _serviceRepository.PostResponse("CompanyBulkImportExport", inputModel);
                response.EnsureSuccessStatusCode();
                ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;

                res.ResponseStatus = viewModel.IsSuccess ? "Success" : "Failed";
                res.ResponseMessage = viewModel.Message;
                res.TotalRecords = inputModel.CompanyBasicInfoList.Count;
                if (res.ResponseStatus == "Success")
                {
                    res.TotalSuccessRecords = inputModel.CompanyBasicInfoList.Count;
                    res.TotalFailedRecords = 0;
                }
                else
                {
                    res.TotalSuccessRecords = 0;
                    res.TotalFailedRecords = inputModel.CompanyBasicInfoList.Count;
                }
                res.GetCompanyProfileErrorList = viewModel.cpData;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        private List<CompanyBulkFinancialImportModel> GetBulkCompanyFinancial(ExcelWorksheet worksheet)
        {
            List<CompanyBulkFinancialImportModel> companyFinancialInfoList = new List<CompanyBulkFinancialImportModel>();

            var rowCount = worksheet.Dimension.Rows;
            int colIndexCompanyName = GetColIndex(worksheet, "Company Name");
            int colIndexTotalAssets = GetColIndex(worksheet, "Total Assets");
            int colIndexTotalCurrentAssets = GetColIndex(worksheet, "Total Current Assets");
            int colIndexCashandcashequivalents = GetColIndex(worksheet, "Cash and cash equivalents");
            int colIndexShortterminvestments = GetColIndex(worksheet, "Short-term investments");
            int colIndexTotalequity = GetColIndex(worksheet, "Total equity");
            int colIndexTotalliabilities = GetColIndex(worksheet, "Total liabilities");
            int colIndexTotalcurrentliabilities = GetColIndex(worksheet, "Total current liabilities");
            int colIndexNetDebt = GetColIndex(worksheet, "Net Debt");
            int colIndexRevenue = GetColIndex(worksheet, "Revenue");
            int colIndexEBITDA = GetColIndex(worksheet, "EBITDA");
            int colIndexEBIT = GetColIndex(worksheet, "EBIT");
            int colIndexNetIncome = GetColIndex(worksheet, "Net income ");
            int colIndexCompanyListedon = GetColIndex(worksheet, "Company Listed on");
            int colIndexFiscalYear = GetColIndex(worksheet, "Fiscal Year");

            for (int row = 7; row <= rowCount; row++)
            {
                CompanyBulkFinancialImportModel companyFinancialInfo = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

                //if (!string.IsNullOrEmpty(cn))
                //{
                companyFinancialInfo = new CompanyBulkFinancialImportModel
                {
                    RowNo = row,
                    CompanyName = worksheet.Cells[row, colIndexCompanyName].Value?.ToString().Trim(),
                    TotalAssets = worksheet.Cells[row, colIndexTotalAssets].Value?.ToString().Trim(),
                    TotalCurrentAssets = worksheet.Cells[row, colIndexTotalCurrentAssets].Value?.ToString().Trim(),
                    CashAndCashEquivalents = worksheet.Cells[row, colIndexCashandcashequivalents].Value?.ToString().Trim(),
                    ShortTermInvestments = worksheet.Cells[row, colIndexShortterminvestments].Value?.ToString().Trim(),
                    TotalEquity = worksheet.Cells[row, colIndexTotalequity].Value?.ToString().Trim(),
                    TotalLiabilities = worksheet.Cells[row, colIndexTotalliabilities].Value?.ToString().Trim(),
                    TotalCurrentLiabilities = worksheet.Cells[row, colIndexTotalcurrentliabilities].Value?.ToString().Trim(),
                    NetDebt = worksheet.Cells[row, colIndexNetDebt].Value?.ToString().Trim(),
                    Revenue = worksheet.Cells[row, colIndexRevenue].Value?.ToString().Trim(),
                    EBITDA = worksheet.Cells[row, colIndexEBITDA].Value?.ToString().Trim(),
                    EBIT = worksheet.Cells[row, colIndexEBIT].Value?.ToString().Trim(),
                    NetIncome = worksheet.Cells[row, colIndexNetIncome].Value?.ToString().Trim(),
                    CompanyListedOn = worksheet.Cells[row, colIndexCompanyListedon].Value?.ToString().Trim(),
                    FinanicialYear = worksheet.Cells[row, colIndexFiscalYear].Value?.ToString().Trim(),
                    Currency = "USD",
                    Units= "Millions"

                };
                companyFinancialInfoList.Add(companyFinancialInfo);
                //}
            }
            return companyFinancialInfoList;
        }

        private int GetColIndex(ExcelWorksheet worksheet, string colName)
        {
            int colIndex = worksheet.Cells["6:6"].First(c => c.Value.ToString() == colName).Start.Column;

            return colIndex;
        }

        private List<CompanyBulkCompanyDetailsViewModel> GetBulkCompanyBasic(ExcelWorksheet worksheet)
        {
            List<CompanyBulkCompanyDetailsViewModel> companyBasicInfoList = new List<CompanyBulkCompanyDetailsViewModel>();

            var rowCount = worksheet.Dimension.Rows;

            int colIndexHeadquarter = GetColIndex(worksheet, "Headquarter");
            int colIndexCompanyName = GetColIndex(worksheet, "Company Name");
            int colIndexKeyExecutives = GetColIndex(worksheet, "Key Executives");
            int colIndexNumberOfEmployee = GetColIndex(worksheet, "Number Of Employee");
            int colIndexAsOnDate = GetColIndex(worksheet, "As on Date");
            int colIndexNature = GetColIndex(worksheet, "Nature");
            int colIndexShareholders = GetColIndex(worksheet, "Shareholders");
            int colIndexPhoneNumber1 = GetColIndex(worksheet, "Phone Number1");
            int colIndexWebsite = GetColIndex(worksheet, "Website");
            int colIndexDescription = GetColIndex(worksheet, "Description");
            int colIndexFoundedYear = GetColIndex(worksheet, "Founded year");

            for (int row = 7; row <= rowCount; row++)
            {
                CompanyBulkCompanyDetailsViewModel companyBasicInfo = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

                //if (!string.IsNullOrEmpty(cn))
                //{
                companyBasicInfo = new CompanyBulkCompanyDetailsViewModel
                {
                    RowNo = row,
                    Headquarter = worksheet.Cells[row, colIndexHeadquarter].Value?.ToString().Trim(),
                    CompanyName = worksheet.Cells[row, colIndexCompanyName].Value?.ToString().Trim(),
                    KeyExecutives = worksheet.Cells[row, colIndexKeyExecutives].Value?.ToString().Trim(),
                    NumberofEmployees = worksheet.Cells[row, colIndexNumberOfEmployee].Value?.ToString().Trim(),
                    AsonDate = worksheet.Cells[row, colIndexAsOnDate].Value?.ToString().Trim(),
                    Nature = worksheet.Cells[row, colIndexNature].Value?.ToString().Trim(),
                    Shareholders = worksheet.Cells[row, colIndexShareholders].Value?.ToString().Trim(),
                    PhoneNumber1 = worksheet.Cells[row, colIndexPhoneNumber1].Value?.ToString().Trim(),
                    Website = worksheet.Cells[row, colIndexWebsite].Value?.ToString().Trim(),
                    Decription = worksheet.Cells[row, colIndexDescription].Value?.ToString().Trim(),
                    FoundedYear = worksheet.Cells[row, colIndexFoundedYear].Value?.ToString().Trim()
                };
                companyBasicInfoList.Add(companyBasicInfo);
                //}
            }
            return companyBasicInfoList;
        }
    }
}
