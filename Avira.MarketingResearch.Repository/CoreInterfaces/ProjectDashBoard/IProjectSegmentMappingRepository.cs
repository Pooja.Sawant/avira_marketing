﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.ProjectDashBoard
{
    public interface IProjectSegmentMappingRepository
    {
        ProjectSegmentMappingViewModel GetById(Guid guid);
        IEnumerable<ProjectSegmentMappingViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted);
        Result<SpTransactionMessage> Create(ProjectSegmentMappingInsertDbViewModel viewModel);
        Result<SpTransactionMessage> CreateProjectCode(ProjectSegmentMappingInsertDbViewModel viewModel);
        Result<SpTransactionMessage> Update(ProjectSegmentMappingUpdateDbViewModel viewModel);
    }
}
