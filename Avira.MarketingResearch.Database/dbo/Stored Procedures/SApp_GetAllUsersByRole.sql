﻿  
  
CREATE PROCEDURE [dbo].[SApp_GetAllUsersByRole]  
 (    
  @RoleName nvarchar(200)=null
 )  
 AS  
/*================================================================================  
Procedure Name: [SApp_GetAllUsersByRole]  
Author: Pooja  
Create date: 04/12/2019  
Description: Get list of users from table by role   
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
04/12/2019 Pooja   Initial Version  
================================================================================*/  
  
BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

declare @UserId uniqueidentifier;
set @UserId=(select Id from UserRole
where UserRole.UserRoleName = @RoleName and IsDeleted=0)


	SELECT        UserRole.UserRoleName, UserRole.UserType, AviraUser.*,AviraUser.FirstName + ' ' + AviraUser.LastName as DisplayName
	FROM            AviraUserRoleMap INNER JOIN
	AviraUser ON AviraUserRoleMap.AviraUserId = AviraUser.Id INNER JOIN
	UserRole ON AviraUserRoleMap.UserRoleId = UserRole.Id
	where UserRole.Id=@UserId and ([UserRole].IsDeleted = 0)  and AviraUserRoleMap.IsDeleted=0 and AviraUser.IsDeleted=0 order by AviraUser.FirstName 
 END