﻿
create procedure [dbo].[SApp_EcoSystemPresenceCompetitersByCompanyID]
(@CompanyID uniqueidentifier,
@includeDeleted bit = 0)
as
/*================================================================================
Procedure Name: SApp_EcoSystemPresenceCompetitersByCompanyID
Author: Harshal
Create date: 05/20/2019
Description: Get list from CompanyEcosystemPresenceKeyCompetitersMap table by CompanyID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
05/20/2019	Harshal			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT [Id],
	   [KeyCompetiters],
	   [CompanyId]
FROM [dbo].[CompanyEcosystemPresenceKeyCompetitersMap]
where (CompanyId = @CompanyID)
and ( IsDeleted = 0)

end