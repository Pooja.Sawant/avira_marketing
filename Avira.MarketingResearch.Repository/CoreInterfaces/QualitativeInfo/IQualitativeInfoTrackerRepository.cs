﻿using Avira.MarketingResearch.Model.ViewModels.QualitativeInfo;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.QualitativeInfo
{
    public interface IQualitativeInfoTrackerRepository
    {
        IEnumerable<QualitativeInfoTrackerViewModel> GetAll(Guid? guid, int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted);
    }
}
