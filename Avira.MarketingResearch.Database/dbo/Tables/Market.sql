﻿CREATE TABLE [dbo].[Market](
	[Id] [uniqueidentifier] NOT NULL,
	[MarketName] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](256) NULL,
	[ParentId] [uniqueidentifier] NULL,
	[CreatedOn] [datetime2](7) NULL,
	[UserCreatedById] [uniqueidentifier] NULL,
	[ModifiedOn] [datetime2](7) NULL,
	[UserModifiedById] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeletedOn] [datetime2](7) NULL,
	[UserDeletedById] [uniqueidentifier] NULL,
 CONSTRAINT [PK_Market] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Market] ADD  CONSTRAINT [DF_Market_MarketName]  DEFAULT ('') FOR [MarketName]
GO
ALTER TABLE [dbo].[Market] ADD  CONSTRAINT [DF_Market_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Market]  WITH CHECK ADD  CONSTRAINT [FK_Market_AviraUser] FOREIGN KEY([UserCreatedById])
REFERENCES [dbo].[AviraUser] ([Id])
GO
ALTER TABLE [dbo].[Market] CHECK CONSTRAINT [FK_Market_AviraUser]
GO
ALTER TABLE [dbo].[Market]  WITH CHECK ADD  CONSTRAINT [FK_Market_AviraUser1] FOREIGN KEY([UserModifiedById])
REFERENCES [dbo].[AviraUser] ([Id])
GO
ALTER TABLE [dbo].[Market] CHECK CONSTRAINT [FK_Market_AviraUser1]
GO
ALTER TABLE [dbo].[Market]  WITH CHECK ADD  CONSTRAINT [FK_Market_AviraUser2] FOREIGN KEY([UserDeletedById])
REFERENCES [dbo].[AviraUser] ([Id])
GO
ALTER TABLE [dbo].[Market] CHECK CONSTRAINT [FK_Market_AviraUser2]
GO
ALTER TABLE [dbo].[Market]  WITH CHECK ADD  CONSTRAINT [FK_Market_ParentId] FOREIGN KEY([ParentId])
REFERENCES [dbo].[Market] ([Id])
GO
ALTER TABLE [dbo].[Market] CHECK CONSTRAINT [FK_Market_ParentId]
GO