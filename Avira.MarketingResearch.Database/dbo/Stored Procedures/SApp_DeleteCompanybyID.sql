﻿create procedure [dbo].[SApp_DeleteCompanybyID]
(@CompanyID uniqueidentifier,
@AviraUserId uniqueidentifier)
as
/*================================================================================
Procedure Name: SApp_DeleteCompanybyID
Author: Sharath
Create date: 02/11/2019
Description: soft delete a record from Company table by ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
02/11/2019	Sharath			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


UPDATE [dbo].[Company]
   SET [IsDeleted] = 1,
   [DeletedOn] = getdate(),
   [UserDeletedById] = @AviraUserId
where ID = @CompanyID
and IsDeleted = 0 -- no need to delete record which is already deleted

end