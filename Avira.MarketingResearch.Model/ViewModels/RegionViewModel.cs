﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Avira.MarketingResearch.Model.ViewModels;
using System.Data;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Avira.MarketingResearch.Model.ViewModels
{
    public class RegionViewModel
    {
        public Guid Id { get; set; }
        public string RegionName { get; set; }
        public Guid ParentRegionId { get; set; }
        public int RegionLevel { get; set; }
        //public IEnumerable<CountryViewModel> CountryList { get; set; }
        public List<CountryViewModel> CountryList { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "Region";
            }
        }

    }
    public class RegionInsertDbViewModel
    {
        [Required]
        public Guid Id { get; set; }
        public string RegionName { get; set; }
        public int RegionLevel { get; set; }
        public Guid ParentRegionId { get; set; }       
        public List<Guid> CountryIDList { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime CreatedOn { get; set; } 
        
    }
    public class RegionUpdateDbViewModel
    {
        [Required]
        public string RegionName { get; set; }        
        public int RegionLevel { get; set; }
        public Guid ParentRegionId { get; set; }        
        public bool IsDeleted { get; set; } = false;
        public List<Guid> CountryIDList { get; set; }
        //public List<CountryViewModel> CountryIDList { get; set; }
        public Guid? UserModifiedById { get; set; }
        public Guid? UserDeletedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        [Required]
        public Guid Id { get; set; }


        public string[] CountryId { get; set; }
        public MultiSelectList Countries  { get; set; }
    }

    public class RegionCountryModel
    {
        public Guid RegionId { get; set; }
        public string RegionName { get; set; }
        public int RegionLevel { get; set; }
        public bool? IsDeleted { get; set; }
        public Guid ParentRegionId { get; set; }
        public string RegionImpact { get; set; }
        public bool RegionEndUser { get; set; }
        public Guid TrendId { get; set; }
        public Guid TrendRegionMapId { get; set; }
        public Guid CountryId { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public string CountryImpact { get; set; }
        public bool CountryEndUser { get; set; }
        public Guid TrendCountryMapId { get; set; }
        public int IsAssigned { get; set; }
       // public string SortColumn { get; set; }
       // public string Impact { get; set; }
        //public bool EndUser { get; set; }
        //public IEnumerable<CountryViewModel> CountryList { get; set; }
        public List<CountryViewModel> CountryList { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "Region";
            }
        }

    }
}
