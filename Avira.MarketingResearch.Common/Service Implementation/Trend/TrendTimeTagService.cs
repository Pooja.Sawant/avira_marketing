﻿using Avira.MarketingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.Trend
{
    public class TrendTimeTagService : ITrendTimeTagService
    {
        private readonly ITrendTimeTagRepository _trendTimeTagRepository;
        private readonly ITrendNoteRepository _iTrendNoteRepository;

        public TrendTimeTagService(ITrendTimeTagRepository trendCompanyGroupMapRepository, ITrendNoteRepository iTrendNoteRepository)
        {
            _trendTimeTagRepository = trendCompanyGroupMapRepository;
            _iTrendNoteRepository = iTrendNoteRepository;
        }
        public IEnumerable<TrendTimeTagViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false, string id = "")
        {
            return _trendTimeTagRepository.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude = false, id);
            //throw new NotImplementedException();
        }

        public Result<SpTransactionMessage> InsertUpdate(MainTrendTimeTagInsertUpdateDbViewModel tfc, AviraUser currenUser)
        {
            var res = _trendTimeTagRepository.InsertUpdate(tfc);
            if (res.IsSuccess == true)
            {
                tfc.TrendNoteModel.Id = Guid.NewGuid();
                tfc.TrendNoteModel.UserCreatedById = tfc.UserCreatedById;
                tfc.TrendNoteModel.TrendId = tfc.TrendId;
                _iTrendNoteRepository.Insert(tfc.TrendNoteModel);
            }

            return res;
            //throw new NotImplementedException();
        }
    }
}
