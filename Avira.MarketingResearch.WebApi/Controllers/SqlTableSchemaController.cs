﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.ImportExport;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SqlTableSchemaController : AviraBaseController
    {
        public readonly ISqlTableSchemaService _iSqlTableSchemaService;
        private ILoggerManager _logger;

        public SqlTableSchemaController(ISqlTableSchemaService iSqlTableSchemaService, ILoggerManager logger)
        {
            _iSqlTableSchemaService = iSqlTableSchemaService;
            _logger = logger;
        }

        [HttpPost]
        public DataTable GetTableSchema(SchemaModel model)
        {
            DataTable dt=new DataTable();
            try
            {
                dt = _iSqlTableSchemaService.GetTableSchema(model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return dt;
        }

        [HttpPost]
        public DataTable GetTableSchema1()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = _iSqlTableSchemaService.GetTableSchema(new SchemaModel());
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return dt;
        }
    }
}