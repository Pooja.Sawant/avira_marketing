﻿CREATE TABLE [dbo].[Decision] (
    [Id]               UNIQUEIDENTIFIER CONSTRAINT [DF__Decision__Id__646F6200] DEFAULT (newid()) NOT NULL,
    [Description]      NVARCHAR (1024)  NOT NULL,
    [ResourceAssigned] NVARCHAR (899)   NULL,
    [UserId]           UNIQUEIDENTIFIER NOT NULL,
    [BaseDecisionId]   UNIQUEIDENTIFIER NULL,
    [Deadline]         DATETIME2 (7)    NULL,
    [TimeTagOutlookId] UNIQUEIDENTIFIER NULL,
    [DecisionTypeId]   UNIQUEIDENTIFIER NULL,
    [StatusId]         UNIQUEIDENTIFIER NOT NULL,
    [Score]            DECIMAL (19, 2)  NULL,
    [CreatedOn]        DATETIME2 (7)    CONSTRAINT [DF__Decision__Create__6A283B56] DEFAULT (getdate()) NOT NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_Decision_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK__Decision__3214EC076FC16F17] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__Decision__BaseDe__6657AA72] FOREIGN KEY ([BaseDecisionId]) REFERENCES [dbo].[BaseDecision] ([Id]),
    CONSTRAINT [FK__Decision__Decisi__683FF2E4] FOREIGN KEY ([DecisionTypeId]) REFERENCES [dbo].[GrowthType] ([Id]),
    CONSTRAINT [FK__Decision__Status__6934171D] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[LookupStatus] ([Id]),
    CONSTRAINT [FK__Decision__TimeTa__674BCEAB] FOREIGN KEY ([TimeTagOutlookId]) REFERENCES [dbo].[TimeTag] ([Id]),
    CONSTRAINT [FK__Decision__UserCr__6B1C5F8F] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK__Decision__UserDe__6D04A801] FOREIGN KEY ([UserDeletedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK__Decision__UserId__65638639] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK__Decision__UserMo__6C1083C8] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id])
);

