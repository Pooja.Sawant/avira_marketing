﻿CREATE procedure [dbo].[SApp_GetAllRegions]

as
/*================================================================================
Procedure Name: [SApp_GetAllRegions]
Author: Laxmikant
Create date: 05/02/2019
Description: Get list from Region
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
05/02/2019	Sai Krishna			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT [Id], 
		[RegionName],
		[RegionLevel]
FROM [dbo].[Region]
where (Region.IsDeleted = 0)  
end