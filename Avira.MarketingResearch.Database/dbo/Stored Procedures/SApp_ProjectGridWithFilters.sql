﻿    
CREATE PROCEDURE [dbo].[SApp_ProjectGridWithFilters]        
 (@ProjectIDList dbo.udtUniqueIdentifier  READONLY,        
 @ProjectCode nvarchar(256)='',      
 @ProjectName nvarchar(256)='',     
 @StartDate datetime=null,    
 @EndDate datetime=null,    
 @ProjectStatusID UniqueIdentifier =null,    
 @includeDeleted bit = 0,        
 @OrdbyByColumnName NVARCHAR(50) ='StartDate',        
 @SortDirection INT = -1,        
 @PageStart INT = 0,        
 @PageSize INT = NULL,
 @AviraUserId uniqueidentifier)        
 AS        
/*================================================================================        
Procedure Name: SApp_ProjectGridWithFilters        
Author: Adarsh        
Create date: 03/19/2019        
Description: Get list from Project table by Name or ID        
Change History        
Date  Developer  Reason        
__________ ____________ ______________________________________________________        
03/19/2019 Adarsh   Initial Version    
__________ ____________ ______________________________________________________        
03/22/2019 Pooja   Change Sp for Multiple list of records with no. of table        
================================================================================*/        
        
BEGIN        
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED        
if @includeDeleted is null set @includeDeleted = 0;    

declare @IsAdmin bit;
declare @AdminRoleId uniqueidentifier

select @AdminRoleId = id 
from UserRole
where UserRoleName = 'Admin'
and IsDeleted = 0;

if exists (select 1 from AviraUser where id = @AviraUserId and UserRoleId = @AdminRoleId)
or exists (select 1 from AviraUserRoleMap where AviraUserId = @AviraUserId and UserRoleId = @AdminRoleId)
set @IsAdmin = 1
else 
set @IsAdmin = 0;
        
if len(@ProjectName)>2 set @ProjectName = '%'+ @ProjectName + '%'      
else set @ProjectName = ''      
    
if len(@ProjectCode)>2 set @ProjectCode = '%'+ @ProjectCode + '%'      
else set @ProjectCode = ''      
    
if @StartDate  is null set @StartDate = '01/01/1900'    
if @EndDate is null set @EndDate = '12/31/2999'    
      
      
if @PageSize is null or @PageSize =0    
 set @PageSize = 10      
         
 ;WITH CTE_proj AS (        
        
 SELECT [Project].[Id], --as ProjectId,        
  [Project].[ProjectName],        
  [Project].[ProjectCode],    
  [Project].[Remark],      
  [Project].[StartDate] As StartDate,    
  [Project].[EndDate]  As EndDate,    
  [Project].[MinTrends],    
  [Project].[MinCompanyProfiles],    
  [Project].[MinPrimaries],    
  [Project].[ProjectStatusID],    
  [ProjectStatus].[ProjectStatusName],    
  [Project].[IsDeleted]  
FROM [dbo].[Project]    
  
INNER JOIN [dbo].[ProjectStatus] ProjectStatus        
on [Project].ProjectStatusID = [ProjectStatus].Id     
where (NOT EXISTS (SELECT * FROM @ProjectIDList) or  [Project].Id  in (select * from @ProjectIDList))        
 AND (@ProjectName = '' or [Project].[ProjectName] like @ProjectName)        
 AND (@ProjectCode = '' or [Project].[ProjectCode] like @ProjectCode)     
 and  ([Project].StartDate <= @EndDate and [Project].EndDate > @StartDate)    
 and (@ProjectStatusID is null or [Project].ProjectStatusID = @ProjectStatusID)    
 AND (@includeDeleted = 0 or [Project].IsDeleted = 1)  
 and (@IsAdmin = 1 or exists (select 1 from ProjectAnalistDetails where AnalystUserID = @AviraUserID and Project.id= ProjectAnalistDetails.ProjectID))   
 ),  
 CTE_PPL as (SELECT ProjectID,  
     [AnalysisType],  
     [Author],   
     [Approver],   
     [Co-Approver],   
     [Co-Author]  
   FROM(SELECT distinct Analysis.ProjectID,   
         Analysis.[AnalysisType],  
         Analysis.[AnalystType],  
         --case when Analysis.[AnalystType] in('Co-Approver', 'Co-Author') then Analist.ResourceName  
         --else AviraUser.FirstName + ' ' + AviraUser.LastName  
         --end as ResourceName  
         Analist.ResourceName as ResourceName  
    FROM [dbo].[ProjectAnalistDetails] Analysis  
    INNER JOIN [AviraUser] AviraUser   
    ON  AviraUser.Id=Analysis.AnalystUserID  
    CROSS APPLY(select ResourceName from(  
        SELECT AviraUser.Id,  
        AviraUser.FirstName + ' ' + AviraUser.LastName as ResourceName  
        FROM [dbo].[ProjectAnalistDetails] Analist  
        INNER JOIN [AviraUser]   
         ON  AviraUser.Id=Analist.AnalystUserID  
        where Analysis.[AnalysisType] = Analist.[AnalysisType]  
       and Analysis.[AnalystType] = Analist.[AnalystType]  
       and Analysis.ProjectID = Analist.ProjectID  
       and Analist.IsDeleted = 0  
       --and Analysis.[AnalystType] in('Co-Approver', 'Co-Author')  
        FOR JSON PATH)  
        as Analist(ResourceName)) as Analist  
    where Analysis.AnalysisType in ('Company Profiles', 'Market Sizing', 'Primaries Info', 'Quantative info', 'Trends Forms','Approver','Co-Approver')  
    and Analysis.ProjectId in (select projectId from CTE_proj)  
    and Analysis.IsDeleted=0  
   ) src  
   pivot(max(ResourceName)  
    for [AnalystType] in ([Author], [Approver], [Co-Approver], [Co-Author]  
    )  
  ) piv)      
, CTE_TBL as (  
 select CTE_proj.*,  
 CompanyProfiles.Author as CompanyProfilesAuthor,  
 CompanyProfiles.Approver as CompanyProfilesApprover,  
 CompanyProfiles.[Co-Author] as CompanyProfilesCoAuthor,  
 CompanyProfiles.[Co-Approver] as CompanyProfilesCoApprover,  
 MarketSizing.Author as MarketSizingAuthor,  
 MarketSizing.Approver as MarketSizingApprover,  
 MarketSizing.[Co-Author] as MarketSizingCoAuthor,  
 MarketSizing.[Co-Approver] as MarketSizingCoApprover,  
 PrimariesInfo.Author as PrimariesInfoAuthor,  
 PrimariesInfo.Approver as PrimariesInfoApprover,  
 PrimariesInfo.[Co-Author] as PrimariesInfoCoAuthor,  
 PrimariesInfo.[Co-Approver] as PrimariesInfoCoApprover,  
 Quantativeinfo.Author as QuantativeinfoAuthor,  
 Quantativeinfo.Approver as QuantativeinfoApprover,  
 Quantativeinfo.[Co-Author] as QuantativeinfoCoAuthor,  
 Quantativeinfo.[Co-Approver] as QuantativeinfoCoApprover,  
 TrendsForms.Author as TrendsFormsAuthor,  
 TrendsForms.Approver as TrendsFormsApprover,  
 TrendsForms.[Co-Author] as TrendsFormsCoAuthor,  
 TrendsForms.[Co-Approver] as TrendsFormsCoApprover,  
   
 MApprover.Approver as MApproverApprover,  
 MApprover.[Co-Approver] as MApproverCoApprover,  
  
  
 MCoApprover.Approver as MCoApproverApprover,   
 MCoApprover.[Co-Approver] as MCoApproverCoApprover,  
 CASE        
   WHEN @OrdbyByColumnName = 'ProjectName' THEN CTE_proj.ProjectName        
   WHEN @OrdbyByColumnName = 'ProjectCode' THEN CTE_proj.ProjectCode        
   WHEN @OrdbyByColumnName = 'StartDate' THEN convert(varchar(12), CTE_proj.StartDate, 23)    
   WHEN @OrdbyByColumnName = 'EndDate' THEN convert(varchar(12), CTE_proj.EndDate, 23)    
   ELSE CTE_proj.[ProjectName]        
  END AS SortColumn  
  
 from CTE_proj  
 LEFT JOIN CTE_PPL CompanyProfiles  
 on CTE_proj.ID = CompanyProfiles.ProjectId  
 and CompanyProfiles.AnalysisType = 'Company Profiles'  
  
 LEFT JOIN CTE_PPL MarketSizing  
 on CTE_proj.ID = MarketSizing.ProjectId  
 and MarketSizing.AnalysisType = 'Market Sizing'  
  
 LEFT join CTE_PPL PrimariesInfo  
 on CTE_proj.ID = PrimariesInfo.ProjectId  
 and PrimariesInfo.AnalysisType = 'Primaries Info'  
  
 LEFT join CTE_PPL Quantativeinfo  
 on CTE_proj.ID = Quantativeinfo.ProjectId  
 and Quantativeinfo.AnalysisType = 'Quantative info'  
  
 LEFT  join CTE_PPL TrendsForms  
 on CTE_proj.ID = TrendsForms.ProjectId  
 and TrendsForms.AnalysisType = 'Trends Forms'  
  
   
 LEFT  join CTE_PPL MApprover  
 on CTE_proj.ID = MApprover.ProjectId  
 and MApprover.AnalysisType = 'Approver'  
  
   
 LEFT  join CTE_PPL MCoApprover  
 on CTE_proj.ID = MCoApprover.ProjectId  
 and MCoApprover.AnalysisType = 'Co-Approver'  
)  
 SELECT         
    CTE_TBL.*,        
    tCountResult.TotalItems        
  FROM CTE_TBL        
  CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult        
  ORDER BY         
    CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,        
    CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC         
  OFFSET @PageStart ROWS        
  FETCH NEXT @PageSize ROWS ONLY;        
        
 END