﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Model.CustomValidation
{
    public class ValidateImageAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            int MaxContentLength = 1024 * 1024 * 1; //Max 1 MB file
            string[] AllowedFileExtensions = new
                string[] { ".jpg", ".jpeg", ".gif", ".png" };

            var file = value as IFormFile;


            if (file == null)
                return true;
            else if (!AllowedFileExtensions.Contains((file != null) ?
                file.FileName.Substring(file.FileName.LastIndexOf('.')).ToLower()
                : string.Empty))
            {
                ErrorMessage = "Please upload Your Photo of type: " +
                    string.Join(", ", AllowedFileExtensions);
                return false;
            }
            else if (file.Length > MaxContentLength)
            {
                ErrorMessage = "Your Image is too large, maximum allowed size is : "
                    + (MaxContentLength / 1024).ToString() + "MB";
                return false;
            }
            else
            {
                var imageHeight = 0;
                var imageWidth = 0;
                using (var image = Image.FromStream(file.OpenReadStream()))
                {
                    imageHeight = image.Height;
                    imageWidth = image.Width;
                    // use image.Width and image.Height
                }

                if (imageHeight > 134.4 || imageWidth > 220.8)
                {
                    ErrorMessage = "Your Image dimension is too large, maximum allowed size is 1.4px * 2.3px ";
                    return false;
                }
                else
                {
                    return true;
                }
            }

        }
    }
}
