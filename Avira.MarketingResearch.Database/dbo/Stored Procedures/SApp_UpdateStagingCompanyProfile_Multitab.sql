﻿CREATE procedure [dbo].[SApp_UpdateStagingCompanyProfile_Multitab]
        
as        
/*================================================================================        
Procedure Name: SApp_UpdateStagingCompanyProfile_Multitab       
Author: Sharath        
Create date: 08/22/2019        
Description: to update Company Profiles 
Change History        
Date  Developer  Reason        
__________ ____________ ______________________________________________________        
08/22/2019 Sharath   Initial Version        
================================================================================*/        
BEGIN        
SET NOCOUNT ON;        
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;        
declare @ErrorMsg NVARCHAR(2048)          
        
BEGIN TRY         
        

declare @CompanyId [UNIQUEIDENTIFIER] = NEWID();
--Fundamental verification
update staging
set ErrorNotes = isnull(ErrorNotes,'') + case when exists (select 1 from company where company.CompanyName = staging.CompanyName) then 'Company Already Exists, ' else '' end
+ case when not exists (select 1 from Nature where Nature.NatureTypeName = staging.Nature) then 'Invalid Nature, ' else '' end
+ case when not exists (select 1 from CompanyStage where CompanyStage.CompanyStageName = staging.CompanyStage) then 'Invalid Company Stage, ' else '' end
--+ case when isnumeric(Foundedyear) = 0 then 'Invalid Founded year, ' else '' end
--+ case when isnumeric(NumberOfEmployee) = 0 then 'Invalid NumberOfEmployee, ' else '' end
--+ case when isdate(AsonDate) = 0 then 'Invalid AsonDate, ' else '' end
+ case when not exists (select 1 from Currency where Currency.Currencycode = staging.Currency1) then 'Invalid Currency, ' else '' end
+ case when exists (select 1 from dbo.FUN_STRING_TOKENIZER(GeographicPresence,',') geo
					left join Country 
					on geo.value = Country.CountryName
					left join Region
					on geo.value = Region.RegionName 
					where Country.ID is null and Region.ID is null) then 'Invalid GeographicPresence, ' else '' end
, CurrencyId = (select top 1 id from Currency where Currency.Currencycode = staging.Currency1)
,NatureId = (select top 1 id from Nature where Nature.NatureTypeName = staging.Nature)
,CompanyStageId = (select top 1 id from CompanyStage where CompanyStage.CompanyStageName = staging.CompanyStage),
CompanyId = @CompanyId
from [StagingCompanyProfileFundamentals] staging

--Share Holding Pattern verification
update staging
set ErrorNotes = isnull(ErrorNotes,'') + case when not exists (select 1 from EntityType where EntityType.EntityTypeName = staging.Entity) then 'Invalid EntityTypeName, ' else '' end
,EntityTypeId = (select top 1 id from EntityType where EntityType.EntityTypeName = staging.Entity)
, CompanyId = @CompanyId
from [StagingCompanyProfileShareHolding] staging

--Subsidiaries Verification
update staging
set ErrorNotes = isnull(ErrorNotes,'') + case when exists (select 1 from company where company.CompanyName = staging.CompanySubsdiaryName) then 'CompanySubsdiaryName Already Exists, ' else '' end
, CompanyId = @CompanyId
from [StagingCompanyProfileSubsidiaries] staging

--KeyEmployees Verification
update staging
set CompanyId = @CompanyId
from [StagingCompanyKeyEmployees] staging;

--BoD Verification
update staging
set CompanyId = @CompanyId
from [StagingCompanyBoardOfDirectors] staging;

--Product Verification
update staging
set ErrorNotes = isnull(ErrorNotes,'') + case when exists (select 1 from dbo.FUN_STRING_TOKENIZER(staging.TargetedGeogrpahy,',') geo
					left join Country 
					on geo.value = Country.CountryName
					left join Region
					on geo.value = Region.RegionName 
					where Country.ID is null and Region.ID is null) then 'Invalid GeographicPresence, ' else '' end
+ case when not exists (select 1 from Currency where Currency.Currencycode = staging.Currency) and staging.Revenue !=0 then 'Invalid Currency, ' else '' end
+ case when not exists (select 1 from Segment where Segment.SegmentName = staging.Segment) then 'Invalid Segment, ' else '' end
+ case when not exists (select 1 from Valueconversion where Valueconversion.ValueName = staging.Units) and staging.Revenue !=0 then 'Invalid Units, ' else '' end
+ case when not exists (select 1 from ProductCategory where ProductCategory.ProductCategoryName = staging.ProductCategory) then 'Invalid ProductCategory, ' else '' end
+ case when not exists (select 1 from Project where Project.ProjectName = staging.Market) and staging.Market!='' then 'Invalid Market/Project Name, ' else '' end
, CurrencyId = (select top 1 id from Currency where Currency.Currencycode = staging.Currency)
, SegmentId = (select top 1 id from Segment where Segment.SegmentName = staging.Segment)
, ValueConversionId = (select top 1 id from ValueConversion where ValueConversion.ValueName = staging.Units)
, ProductCategoryId = (select top 1 id from ProductCategory where ProductCategory.ProductCategoryName = staging.ProductCategory)
, ProjectId = (select top 1 id from Project where Project.ProjectName = staging.Market) 
, CompanyId = @CompanyId
from [StagingCompanyProduct] staging;

--Company Strategy Verification
update staging
set ErrorNotes = isnull(ErrorNotes,'') + 
+ case when not exists (select 1 from Approach where Approach.ApproachName = staging.OrganicInorganic) then 'Invalid Approach Name, ' else '' end
+ case when not exists (select 1 from Category where Category.CategoryName = staging.Category) then 'Invalid Category Name, ' else '' end
+ case when not exists (select 1 from Importance where Importance.ImportanceName = staging.Importance) then 'Invalid Importance Name, ' else '' end
+ case when not exists (select 1 from ImpactDirection where ImpactDirection.ImpactDirectionName = staging.Impact) and isnull(staging.Impact,'') != '' then 'Invalid Impact, ' else '' end
, ApproachId = (select top 1 id from Approach where Approach.ApproachName = staging.OrganicInorganic) 
, CategoryId = (select top 1 id from Category where Category.CategoryName = staging.Category)
, ImportanceId = (select top 1 id from Importance where Importance.ImportanceName = staging.Importance) 
, ImpactId = (select top 1 id from ImpactDirection where ImpactDirection.ImpactDirectionName = staging.Impact)
, CompanyId = @CompanyId
from StagingCompanyStrategy staging;

--Company Clients Verification
update staging
set CompanyId = @CompanyId
from [StagingCompanyClients] staging;

--Company News Verification
update staging
set ErrorNotes = isnull(ErrorNotes,'') + 
+ case when not exists (select 1 from NewsCategory where NewsCategory.NewsCategoryName = staging.NewsCategory) then 'Invalid News Category Name, ' else '' end
+ case when not exists (select 1 from Importance where Importance.ImportanceName = staging.Importance) then 'Invalid Importance Name, ' else '' end
+ case when not exists (select 1 from ImpactDirection where ImpactDirection.ImpactDirectionName = staging.Impact) then 'Invalid Impact, ' else '' end
, CategoryId = (select top 1 id from NewsCategory where NewsCategory.NewsCategoryName = staging.NewsCategory)
, ImportanceId = (select top 1 id from Importance where Importance.ImportanceName = staging.Importance) 
, ImpactId = (select top 1 id from ImpactDirection where ImpactDirection.ImpactDirectionName = staging.Impact)
, CompanyId = @CompanyId
from [StagingCompanyNews] staging;

--Company ESM Verification
update staging
set ErrorNotes = isnull(ErrorNotes,'') + 
+ case when exists (select 1 from dbo.FUN_STRING_TOKENIZER(ESMPresence,',') ESM
					left join Segment
					on ESM.value = Segment.SegmentName
					where Segment.ID is null) then 'Invalid ESM Presence, ' else '' end
+ case when exists (select 1 from dbo.FUN_STRING_TOKENIZER(SectorPresence,',') SP
					left join Industry
					on SP.value = Industry.IndustryName
					and Industry.IsDeleted = 0
					where Industry.ID is null) then 'Invalid Sector Presence, ' else '' end
, CompanyId = @CompanyId
from [StagingCompanyEcosystemPresence] staging;

--Company SWOT Verification
update staging
set ErrorNotes = isnull(ErrorNotes,'') + 
+ case when not exists (select 1 from Project where Project.ProjectName = staging.Market) then 'Invalid Market/Project Name, ' else '' end
+ case when not exists (select 1 from SWOTType where SWOTType.SWOTTypeName = staging.SWOTType) then 'Invalid SWOT Type Name, ' else '' end
+ case when not exists (select 1 from Category where Category.CategoryName = staging.Category) then 'Invalid Category Name, ' else '' end
, SWOTTypeId = (select top 1 id from [SWOTType] where [SWOTType].SWOTTypeName = staging.SWOTType)
, CategoryId = (select top 1 id from Category where Category.CategoryName = staging.Category)
, ProjectId = (select top 1 id from Project where Project.ProjectName = staging.Market) 
, CompanyId = @CompanyId
from [StagingCompanySWOT] staging;

--Company Trends Verification
update staging
set ErrorNotes = isnull(ErrorNotes,'') + 
+ case when not exists (select 1 from Importance where Importance.ImportanceName = staging.Importance) then 'Invalid Importance Name, ' else '' end
+ case when not exists (select 1 from ImpactDirection where ImpactDirection.ImpactDirectionName = staging.Impact) then 'Invalid Impact, ' else '' end
+ case when not exists (select 1 from Project where Project.ProjectName = staging.Market) and staging.Market!='' then 'Invalid Market/Project Name, ' else '' end
, ImportanceId = (select top 1 id from Importance where Importance.ImportanceName = staging.Importance) 
, ImpactId = (select top 1 id from ImpactDirection where ImpactDirection.ImpactDirectionName = staging.Impact)
, ProjectId = (select top 1 id from Project where Project.ProjectName = staging.Market) 
, CompanyId = @CompanyId
from [StagingCompanyTrends] staging;

--Company AnalystView Verification
update staging
set ErrorNotes = isnull(ErrorNotes,'') + 
+ case when not exists (select 1 from ClientStrategyCategory where ClientStrategyCategory.CategoryName = staging.Category) then 'Invalid Category Name, ' else '' end
+ case when not exists (select 1 from Importance where Importance.ImportanceName = staging.Importance) then 'Invalid Importance Name, ' else '' end
+ case when not exists (select 1 from Project where Project.ProjectName = staging.Market) then 'Invalid Market/Project Name, ' else '' end
, ProjectId = (select top 1 id from Project where Project.ProjectName = staging.Market) 
, CategoryId = (select top 1 id from Category where Category.CategoryName = staging.Category)
, ImportanceId = (select top 1 id from Importance where Importance.ImportanceName = staging.Importance) 
, CompanyId = @CompanyId
from [StagingCompanyAnalystView] staging;


IF( EXISTS(SELECT 1 FROM StagingCompanyAnalystView WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingCompanyBoardOfDirectors WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingCompanyClients WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingCompanyEcosystemPresence WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingCompanyKeyEmployees WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingCompanyNews WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingCompanyProduct WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingCompanyProfileFundamentals WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingCompanyProfileShareHolding WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingCompanyProfileSubsidiaries WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingCompanyStrategy WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingCompanySWOT WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingCompanyTrends WHERE ErrorNotes != ''))    
BEGIN   
Select 'Fundamentals' as Module, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingCompanyProfile_Multitab' AS StoredProcedureName   FROM StagingCompanyProfileFundamentals WHERE ErrorNotes != '' UNION ALL
Select 'Share Holding Pattern' as Module, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingCompanyProfile_Multitab' AS StoredProcedureName   FROM StagingCompanyProfileShareHolding WHERE ErrorNotes != '' UNION ALL
Select 'Subsidiaries' as Module, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingCompanyProfile_Multitab' AS StoredProcedureName   FROM StagingCompanyProfileSubsidiaries WHERE ErrorNotes != '' UNION ALL
Select 'Key Employees' as Module, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingCompanyProfile_Multitab' AS StoredProcedureName   FROM StagingCompanyKeyEmployees WHERE ErrorNotes != '' UNION ALL
Select 'Board Of Directors' as Module, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingCompanyProfile_Multitab' AS StoredProcedureName   FROM StagingCompanyBoardOfDirectors WHERE ErrorNotes != '' UNION ALL
Select 'Product' as Module, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingCompanyProfile_Multitab' AS StoredProcedureName   FROM StagingCompanyProduct WHERE ErrorNotes != '' UNION ALL
Select 'Client' as Module, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingCompanyProfile_Multitab' AS StoredProcedureName   FROM StagingCompanyClients WHERE ErrorNotes != '' UNION ALL
Select 'Strategies' as Module, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingCompanyProfile_Multitab' AS StoredProcedureName   FROM StagingCompanyStrategy WHERE ErrorNotes != '' UNION ALL
Select 'News' as Module, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingCompanyProfile_Multitab' AS StoredProcedureName   FROM StagingCompanyNews WHERE ErrorNotes != '' UNION ALL
Select 'SWOT' as Module, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingCompanyProfile_Multitab' AS StoredProcedureName   FROM StagingCompanySWOT WHERE ErrorNotes != '' UNION ALL
Select 'Ecosystem' as Module, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingCompanyProfile_Multitab' AS StoredProcedureName   FROM StagingCompanyEcosystemPresence WHERE ErrorNotes != '' UNION ALL
Select 'Trends' as Module, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingCompanyProfile_Multitab' AS StoredProcedureName   FROM StagingCompanyTrends WHERE ErrorNotes != '' UNION ALL
Select 'Analyst Views' as Module, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingCompanyProfile_Multitab' AS StoredProcedureName   FROM StagingCompanyAnalystView WHERE ErrorNotes != '' 

DELETE FROM ImportData where Id in (select ImportId from StagingCompanyProfileFundamentals sm);    
DELETE FROM StagingCompanyProfileFundamentals;
DELETE FROM StagingCompanyProfileShareHolding;
DELETE FROM StagingCompanyProfileSubsidiaries;
DELETE FROM StagingCompanyKeyEmployees;
DELETE FROM StagingCompanyBoardOfDirectors;
DELETE FROM StagingCompanyProduct;
DELETE FROM StagingCompanyClients;
DELETE FROM StagingCompanyStrategy;
DELETE FROM StagingCompanyNews;
DELETE FROM StagingCompanySWOT;
DELETE FROM StagingCompanyEcosystemPresence;
DELETE FROM StagingCompanyTrends;
DELETE FROM StagingCompanyAnalystView;

END  
ELSE  
Begin
	Exec [dbo].[SApp_insertAllCompanyProfileFromStagging_MultiTab];
	DELETE FROM StagingCompanyProfileFundamentals;
	DELETE FROM StagingCompanyProfileShareHolding;
	DELETE FROM StagingCompanyProfileSubsidiaries;
	DELETE FROM StagingCompanyKeyEmployees;
	DELETE FROM StagingCompanyBoardOfDirectors;
	DELETE FROM StagingCompanyProduct;
	DELETE FROM StagingCompanyClients;
	DELETE FROM StagingCompanyStrategy;
	DELETE FROM StagingCompanyNews;
	DELETE FROM StagingCompanySWOT;
	DELETE FROM StagingCompanyEcosystemPresence;
	DELETE FROM StagingCompanyTrends;
	DELETE FROM StagingCompanyAnalystView;

End 

END TRY              
BEGIN CATCH              
    -- Execute the error retrieval routine.              
 DECLARE @ErrorNumber int;              
 DECLARE @ErrorSeverity int;              
 DECLARE @ErrorProcedure varchar(100);              
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);              
              
  SELECT @ErrorNumber = ERROR_NUMBER(),              
        @ErrorSeverity = ERROR_SEVERITY(),              
        @ErrorProcedure = ERROR_PROCEDURE(),              
        @ErrorLine = ERROR_LINE(),              
        @ErrorMessage = ERROR_MESSAGE()              
              
 insert into dbo.Errorlog(ErrorNumber,              
       ErrorSeverity,              
       ErrorProcedure,              
       ErrorLine,              
       ErrorMessage,              
       ErrorDate)              
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());              
            
 set @ErrorMsg = 'Error while updating StagingCompanyProfile, please contact Admin for more details.';            
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine            
,Message =@ErrorMsg,Success = 0;               
               
END CATCH              
        
        
end