﻿

CREATE procedure [dbo].[SApp_insertProjectSegmentMapping]
(@ProjectID uniqueidentifier, 
@SegmentType nvarchar(100),
@SegmentIDlist [dbo].[udtUniqueIdentifier] readonly, 
@UserCreatedById uniqueidentifier 
--@CreatedOn Datetime
)
as
/*================================================================================
Procedure Name: SApp_insertProjectSegmentMapping
Author: Swami
Create date: 10/19/2019
Description: Insert a record into Industry table
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
03/19/2019	Swami			Initial Version
__________	____________	______________________________________________________
4/17/2019	Pooja			Initial null declaration
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ErrorMsg NVARCHAR(2048);

--Validations
IF exists(select 1 from [dbo].[ProjectSegmentMapping] 
		 where ProjectID = @ProjectID 
		 AND SegmentType = @SegmentType 
		 and SegmentID in (select id from @SegmentIDlist)
		 AND IsDeleted = 0)
begin
--declare @ErrorMsg NVARCHAR(2048);
set @ErrorMsg = 'Project Segment mapping already exists.';
--THROW 50000,@ErrorMsg,1;
SELECT StoredProcedureName ='SApp_insertProjectSegmentMapping',Message =@ErrorMsg,Success = 0;  
RETURN;  

--return(1)
end

--End of Validations
BEGIN TRY

insert into [dbo].[ProjectSegmentMapping]([Id],
								ProjectID,
								SegmentType,
								SegmentID,
								[IsDeleted],
								[UserCreatedById],
								[CreatedOn])
select NEWID(), 
			@ProjectID,
			@SegmentType,
			ID,
			0,
			@UserCreatedById, 
			GETDATE()
from @SegmentIDlist;

SELECT StoredProcedureName ='SApp_insertProjectSegmentMapping',Message =@ErrorMsg,Success = 1;     

END TRY
BEGIN CATCH
    -- Execute the error retrieval routine.
	DECLARE @ErrorNumber	int;
	DECLARE @ErrorSeverity	int;
	DECLARE @ErrorProcedure	varchar(100);
	DECLARE @ErrorLine	int;
	DECLARE @ErrorMessage	varchar(500);

	 SELECT @ErrorNumber = ERROR_NUMBER(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorProcedure = ERROR_PROCEDURE(),
        @ErrorLine = ERROR_LINE(),
        @ErrorMessage = ERROR_MESSAGE()

	insert into dbo.Errorlog(ErrorNumber,
							ErrorSeverity,
							ErrorProcedure,
							ErrorLine,
							ErrorMessage,
							ErrorDate)
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());

 set @ErrorMsg = 'Error while creating a new ProjectSegmentMapping, please contact Admin for more details.';    
--THROW 50000,@ErrorMsg,1;      
--return(1)  
SELECT Number = @ErrorNumber, 
		Severity =@ErrorSeverity,
		StoredProcedureName =@ErrorProcedure,
		LineNumber= @ErrorLine,
		Message =@ErrorMsg,
		Success = 0;     
END CATCH
end