﻿using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.CompanyProfile
{
    public class CompanyTrackerRepository : ICompanyTrackerRepository
    {
        protected IDbHelper _dbHelper;

        public CompanyTrackerRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }
        public IEnumerable<CompanyTrackerViewModel> GetAll(Guid ProjectId, int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<CompanyTrackerViewModel>(CompanyTrackerQueries.StoreProcSelectById
                            , new { @ProjectID = ProjectId, @includeDeleted = includeDeleted, @PageStart = PageStart, @pageSize = pageSize, @SortDirection = SortDirection, @OrdbyByColumnName = OrdbyByColumnName, @Search = Search }
                            , null, true, null, CommandType.StoredProcedure);
        }

        public ProjectInformationViewModel GetProjectInfo(Guid Id)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<ProjectInformationViewModel>(CompanyTrackerQueries.StoreProcGetProjectInfoById
                            , new { @ProjectID = Id, @includeDeleted = false }
                            , null, true, null, CommandType.StoredProcedure).FirstOrDefault();
        }

        public CompanyTrackerViewModel GetById(Guid MapId)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<CompanyTrackerViewModel>(CompanyTrackerQueries.StoreProcSelectMappingByMapId
                            , new { @MapID = MapId }
                            , null, true, null, CommandType.StoredProcedure).FirstOrDefault();
        }
    }
}
