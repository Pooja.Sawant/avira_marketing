﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;

namespace Avira.MarketingResearch.Repository.Implementations
{
    public class UserRepository : IUserRepository
    {
        protected IDbHelper _dbHelper;
        public UserRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public Result<SpTransactionMessage> Create(AviraUserInsertDbViewModel aviraUserInsertDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(UserQueries.StoreProcInsert,
                new
                {
                    @AviraUserID = aviraUserInsertDbViewModel.AviraUserID,
                    @FirstName = aviraUserInsertDbViewModel.FirstName,
                    @LastName = aviraUserInsertDbViewModel.LastName,
                    @UserName = aviraUserInsertDbViewModel.Username,
                    @DisplayName = aviraUserInsertDbViewModel.DisplayName,
                    @EmailAddress = aviraUserInsertDbViewModel.EmailAddress,
                    @PhoneNumber = aviraUserInsertDbViewModel.PhoneNumber,
                    @UserTypeId = aviraUserInsertDbViewModel.UserTypeId,
                    @PassWord = aviraUserInsertDbViewModel.PassWord,
                    @CustomerId = aviraUserInsertDbViewModel.CustomerId,
                    @IsEverLoggedIn = aviraUserInsertDbViewModel.IsEverLoggedIn,
                    @Salt = aviraUserInsertDbViewModel.salt,
                    @CreatedOn = aviraUserInsertDbViewModel.CreatedOn,
                    @UserCreatedById = aviraUserInsertDbViewModel.UserCreatedById,
                    @IsDeleted = aviraUserInsertDbViewModel.IsDeleted,
                    @DateofJoining = aviraUserInsertDbViewModel.DateofJoining,
                    @LocationID = aviraUserInsertDbViewModel.LocationID
                }
                , CommandType.StoredProcedure);

            //--for inserting roles in avirauserrolemap table--
            if (spTransactionMessage.IsSuccess == true)
            {
                DataTable userRoleListIdsTable = new DataTable();
                userRoleListIdsTable.Columns.Add("ID", typeof(Guid));
                userRoleListIdsTable.Columns.Add("bitValue", typeof(bool));

                if (aviraUserInsertDbViewModel.UserRolelist.Count > 0)
                {
                    foreach (var item in aviraUserInsertDbViewModel.UserRolelist)
                    {
                        userRoleListIdsTable.Rows.Add(new Object[] { item.UserRoleID, item.IsPrimary });
                    }
                }
                var spTransactionMessage1 = _dbHelper.ExecuteQuery(UserQueries.StoreProcInsertUserRole,
                    new
                    {
                        @AviraUserID = aviraUserInsertDbViewModel.AviraUserID,
                        @UserRoleList = userRoleListIdsTable.AsTableValuedParameter("dbo.udtGUIDBitValue"),
                        @UserCreatedById = aviraUserInsertDbViewModel.UserCreatedById,
                        @CreatedOn = aviraUserInsertDbViewModel.CreatedOn
                    }, CommandType.StoredProcedure);
                return spTransactionMessage1;
            }

            return spTransactionMessage;
        }

        public IEnumerable<LocationViewModel> GetAllLocation()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<LocationViewModel>(UserQueries.StoreProcSelectAllLocation
                            , null
                            , null, true, null, CommandType.StoredProcedure);
        }

        public IEnumerable<RoleViewModel> GetAllRoles()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<RoleViewModel>(UserQueries.StoreProcSelectAllParent
                            , null
                            , null, true, null, CommandType.StoredProcedure);
        }

        public AviraUser GetByEmail(string email)
        {
            AviraUser aviraUser = new AviraUser();
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return
                dapper.Query<AviraUser>(UserQueries.StoreProcGetByEmail, new { EmailAddress = email }, null, true, null,
                CommandType.StoredProcedure).FirstOrDefault();
        }

        public PermissionsByUserRoleIdViewModel GetPermissionsByUserRoleId(Guid UserRoleId)
        {
            PermissionsByUserRoleIdViewModel model = new PermissionsByUserRoleIdViewModel();

            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);

            model.Permissions = dapper.Query<PermissionsByUserRoleIdViewModel.PermissionByRoleId>(UserQueries.StoreProcGetPermissionsByUserRoleId, new { UserRoleId = UserRoleId }, null, true, null,
               CommandType.StoredProcedure).ToList();
            //

            model.Forms = dapper.Query<PermissionsByUserRoleIdViewModel.PermissionByForm>(UserQueries.StoreProcGetFormsByUserRoleId, new { UserRoleId = UserRoleId }, null, true, null,
                CommandType.StoredProcedure).ToList();

            model.Menus = dapper.Query<PermissionsByUserRoleIdViewModel.MenuList>(UserQueries.StoreProcGetMenusByUserRoleId, new { UserRoleId = UserRoleId }, null, true, null,
                CommandType.StoredProcedure).ToList();

            return model;
        }

        public IEnumerable<RoleViewModel> GetRolesByAviraUserId(Guid aviraUserId)
        {
            //AviraUser aviraUser = new AviraUser();
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return
                dapper.Query<RoleViewModel>(UserQueries.StoreProcGetUserRoles, new { AviraUserId = aviraUserId }, null, true, null,
                CommandType.StoredProcedure);
        }

        public AviraUser GetUserLogin(string userName)
        {
            AviraUser aviraUser = new AviraUser();
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return
                dapper.Query<AviraUser>(UserQueries.StoreProcGetUserLogin, new { UserName = userName }, null, true, null,
                CommandType.StoredProcedure).FirstOrDefault();
        }

        public AviraUser UpdateUserLogin(string userName, bool IsSuccess)
        {
            AviraUser aviraUser = new AviraUser();
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return
                dapper.Query<AviraUser>(UserQueries.StoreProcUpdateUserLogin, new { UserName = userName, Success = IsSuccess }, null, true, null,
                CommandType.StoredProcedure).FirstOrDefault();
        }

        public IEnumerable<AviraUser> GetAllUsersByRole(string roleName)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<AviraUser>(UserQueries.StoreProcGetAllUsersByRole, new { RoleName = roleName }, null, true, null,
                CommandType.StoredProcedure);
        }

        public IEnumerable<GetAllAviraUsers> GetAll(Guid? guid, int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted)
        {
            //var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            //return dapper.Query<GetAllAviraUsers>(UserQueries.StoreProcGetAllUsers);
            return GetAllUsers(guid, PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, includeDeleted);
        }

        private IEnumerable<GetAllAviraUsers> GetAllUsers(Guid? guid, int PageStart = 0, int pageSize = 0, int SortDirection = -1, string OrdbyByColumnName = "", string Search = "", bool includeDeleted = false)
        {

            DataTable ramMaterialIdsTable = new DataTable();
            ramMaterialIdsTable.Columns.Add("ID", typeof(Guid));

            if (guid != null)
            {
                ramMaterialIdsTable.Rows.Add(guid);
            }
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<GetAllAviraUsers>(UserQueries.StoreProcGetAllUsers
                            , new
                            {
                                @UserIDList = ramMaterialIdsTable.AsTableValuedParameter("dbo.udtUniqueIdentifier"),
                                @UserName = Search,
                                @includeDeleted = includeDeleted,
                                @OrdbyByColumnName = OrdbyByColumnName,
                                @SortDirection = SortDirection,
                                @PageStart = PageStart,
                                @pageSize = pageSize
                            }
                            , null, true, null, CommandType.StoredProcedure);
        }

        public AviraUser GetByUserId(string userId)
        {
            AviraUser aviraUser = new AviraUser();
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return
                dapper.Query<AviraUser>(UserQueries.StoreProcGetByUserId, new { AviraUserId = userId }, null, true, null,
                CommandType.StoredProcedure).FirstOrDefault();
        }
    }
}
