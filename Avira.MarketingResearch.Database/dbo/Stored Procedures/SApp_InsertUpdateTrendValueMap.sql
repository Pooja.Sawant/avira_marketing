﻿

CREATE procedure [dbo].[SApp_InsertUpdateTrendValueMap]      
(
@TrendId uniqueidentifier, 
@ValueConversionId uniqueidentifier, 
@ImportanceId uniqueidentifier, 
@ImpactDirectionId uniqueidentifier, 
@TrendValuemap dbo.udtGUIDMultiValue readonly,
@AviraUserId uniqueidentifier
)      
as      
/*================================================================================      
Procedure Name: SApp_InsertUpdateTrendValueMap      
Author: Swami      
Create date: 04/24/2019      
Description: Insert a record into TrendValueMap table      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
04/24/2019 Swami   Initial Version    
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)  
    
--Validations      
--IF exists(select 1 from [dbo].[TrendValueMap]       
--   where TrendId = @TrendId AND ValueId = @ValueId AND IsDeleted = 0)      
--begin       
--set @ErrorMsg = 'Value with Id "'+ CONVERT (nvarchar(30), @ValueId) + '" already exists.';    
--SELECT StoredProcedureName ='SApp_InsertUpdateTrendValueMap',Message =@ErrorMsg,Success = 0;    
--RETURN;    
--end      
--End of Validations      
BEGIN TRY         
--mark records as delete which are not in list
delete [TrendValueMap]
where TrendId = @TrendId;

--Insert new records
INSERT INTO [dbo].[TrendValueMap]
           ([Id]
           ,[TrendId]
           ,[ValueConversionId]
           ,[TimeTagId]
           ,[Rationale]
           ,[Amount]
           ,[Year]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
     select
           NEWID()
           ,@TrendId
           ,@ValueConversionId
           ,map.Id
           ,map.VarcharValue
           ,map.DecimalValue
           ,map.IntValue
           ,GETDATE(),
		   @AviraUserId
           ,0
from @TrendValuemap map
 --where not exists (select 1 from [dbo].[TrendValueMap] map1
	--				where map1.TrendId = @TrendId
	--				and map.Id = map1.TimeTagId
	--				and map.IntValue = map1.[Year]);     

declare @TrendValue decimal(19,4) ;
/*
declare @EndYearValue decimal(19,4);
declare @StartYearValue decimal(19,4);
declare @NumberofYears  decimal(19,4);

select top 1 @EndYearValue = DecimalValue
from @TrendValuemap
order by IntValue desc;
select top 1 @StartYearValue = DecimalValue
from @TrendValuemap
order by IntValue asc;
select @NumberofYears = count(*) 
from @TrendValuemap;
select @trendvalue = POWER((@EndYearValue/@StartYearValue),(1.0000/@NumberofYears))-1
*/
select @trendvalue = avg(map.DecimalValue)
from @TrendValuemap map;

--Updating Trend Table
UPDATE dbo.Trend
SET ImpactDirectionId = @ImpactDirectionId,
	ImportanceId = @ImportanceId,
	TrendValue = @TrendValue
	where Trend.Id = @TrendId

SELECT StoredProcedureName ='SApp_InsertUpdateTrendValueMap',Message =@ErrorMsg,Success = 1;       
    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);      
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());      
    
 set @ErrorMsg = 'Error while insert a new TrendValueMap, please contact Admin for more details.';    
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
       
END CATCH      
        
END