﻿using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.CompanyProfile
{
    public class CompanyStructureRepository : ICompanyStructureRepository
    {
        protected IDbHelper _dbHelper;
        public CompanyStructureRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        

        public IEnumerable<CompanyStructureModel> GetAll(Guid Id)
        {
            //string id = Convert.ToString(Id);
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            //return dapper.Query<CompanyStructureModel>(CompanyStructureQueries.StoreProcSelectAll
            // , new
            // {
            //     @Id= Id
            // }
            // , null, true, null, CommandType.StoredProcedure);

            return dapper.Query<CompanyStructureModel>(CompanyStructureQueries.StoreProcSelectAll, new { @Id = Id }, null, true, null, CommandType.StoredProcedure);
        }
    }
}
