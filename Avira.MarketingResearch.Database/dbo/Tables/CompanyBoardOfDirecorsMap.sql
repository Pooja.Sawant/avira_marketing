﻿CREATE TABLE [dbo].[CompanyBoardOfDirecorsMap] (
    [Id]                                            UNIQUEIDENTIFIER NOT NULL,
    [CompanyBoardOfDirecorsName]                    NVARCHAR (256)   NULL,
    [CompanyBoardOfDirecorsEmailId]                 NVARCHAR (256)   NULL,
    [CompanyBoardOfDirecorsOtherCompanyName]        NVARCHAR (256)   NULL,
    [CompanyBoardOfDirecorsOtherCompanyDesignation] NVARCHAR (256)   NULL,
    [CreatedOn]                                     DATETIME2 (7)    NULL,
    [UserCreatedById]                               UNIQUEIDENTIFIER NULL,
    [ModifiedOn]                                    DATETIME2 (7)    NULL,
    [UserModifiedById]                              UNIQUEIDENTIFIER NULL,
    [IsDeleted]                                     BIT              CONSTRAINT [DF_CompanyBoardOfDirecorsMap_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]                                     DATETIME2 (7)    NULL,
    [UserDeletedById]                               UNIQUEIDENTIFIER NULL,
    [CompanyId]                                     UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [Pk_CompanyBoardOfDirecorsMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CompanyBoardOfDirecorsMap_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyBoardOfDirecorsMap_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyBoardOfDirecorsMap_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([Id])
);

