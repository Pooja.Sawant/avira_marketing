﻿CREATE PROCEDURE [dbo].[SFab_GetTrendListByUserId]
--(@UserId uniqueidentifier='E57281A1-F8C8-43EB-A1CB-6587CDCF24FE',
--@UserRoleId uniqueidentifier='E8600D3A-1686-4DF3-A670-598D513EC447')
(
 @ProjectId uniqueidentifier = null,
	@TrendKeyWord NVARCHAR(50) = null,
	  @EcoSystem [dbo].[udtUniqueIdentifier] READONLY ,  
  @OutlookList [dbo].[udtUniqueIdentifier] READONLY ,  
  @ImportanceList [dbo].[udtUniqueIdentifier] READONLY ,  
  @IndustryIdList [dbo].[udtUniqueIdentifier] READONLY , 
	--@ImportanceIdList [dbo].[udtUniqueIdentifier] READONLY,
	@CompanyGroupIdList [dbo].[udtUniqueIdentifier] READONLY,
	--@Outlook UNIQUEIDENTIFIER = null,
	@Value NVARCHAR(10) = null,
	--@UserId uniqueidentifier=null,
	--@UserRoleId uniqueidentifier=null,
	 @OrdbyByColumnName NVARCHAR(50) ='TrendName',          
  @SortDirection INT = -1,          
  @PageStart INT = 0,        
  @TrendName NVARCHAR(512)='',          
  @PageSize INT = null,      
  @AviraUserID uniqueidentifier = null,      
  @AviraUserRoleId uniqueidentifier = null  

)
AS
BEGIN
	declare @OutLookNewList [dbo].[udtUniqueIdentifier];
	declare @timetagseq tinyint;
	declare @TrendApprovedStatusId uniqueidentifier;

	select @TrendApprovedStatusId = Id
	from TrendStatus
	where TrendStatusName = 'Approved';

	if (@Value is null) set @Value = '';
	if exists (select 1 from @OutLookList)
	begin
		select @timetagseq = max(seq)
	from TimeTag where id in (select id from @OutLookList)
	end
	else	
	begin
		select @timetagseq = max(seq)
	from TimeTag --where id in (select id from @OutLookList)
	end
	
	insert into @OutLookNewList
	select id 
	from TimeTag
	where (@timetagseq < 3 and TimeTag.Seq < 3)
	or (@timetagseq = 3 and TimeTag.Seq = 3)
	or (@timetagseq > 3 and TimeTag.Seq > 3);


	--temparay action
	Declare @ImpIdList [dbo].[udtUniqueIdentifier];
		;With cteImp as (
			Select ImportanceName,
				   Id 
			from Importance 
			Where Id in (Select Id from @ImportanceList)
		),
		cte as (
		Select cteImp.Id,
			Case When (cteImp.ImportanceName = 'High') Then 'High,VeryHigh'
			When (cteImp.ImportanceName = 'Low') Then 'Low,VeryLow'
			When (cteImp.ImportanceName = 'Mid') Then 'Mid'
			Else 'Mid'
			End As ImportanceName
			From cteImp
		),
		cteImpId as(
		Select im.Id as ImportanceId,
		Imp.value as ImportanceName 
		from cte
		Cross Apply string_split(ImportanceName,',') as Imp
		Inner Join Importance im
		On Imp.value = im.ImportanceName and im.IsDeleted = 0
		)
		Insert Into @ImpIdList
		Select ImportanceId from cteImpId

	;with cte as(
		select TrendKeyWord,
				TrendId 
		from TrendKeywords
		inner join trend
		on Trend.Id = TrendKeywords.TrendId
		Where Trend.ProjectID = @ProjectId
		and TrendKeyWord like '%'+@TrendKeyWord+'%' 

		and TrendKeywords.IsDeleted = 0
		),cte_TTT as (
			select ROW_NUMBER() over (partition by trend.ID order by TimeTag.seq desc) as Rnum,
			Trend.Id as TrendId,
			TrendTimeTag.TimeTagId
			from TrendTimeTag
			Inner Join Trend
			On TrendTimeTag.TrendId = Trend.Id
			inner join TimeTag
			on TrendTimeTag.TimeTagId = TimeTag.ID
			where trend.projectId = @projectId
	     )
	select  Trend.Id,
			Trend.TrendName,
			Trend.TrendValue as Amount,
			0 as [Year],
			--cte1.Amount,
			--cte1.Year,
			Trend.ImportanceId,
			imp.ImportanceName,
			null as TimeTagId
			--cte1.TimeTagId
	from Trend
	left join Importance imp
	On Trend.ImportanceId = imp.Id
	--Left Join cte1
	--On Trend.Id = cte1.TrendId
	----------------------
	where Trend.IsDeleted=0
	and (not exists (select 1 from cte) or Trend.Id in(select TrendId from cte))
	and (not exists (select 1 from @ImpIdList) or imp.id in (select ID from @ImpIdList))
	--modification
	------------------
	and (not exists (select 1 from @CompanyGroupIdList) 
	or Trend.id in (select TrendID from @CompanyGroupIdList list
					inner join TrendCompanyGroupMap 
					on list.ID = TrendCompanyGroupMap.CompanyGroupId))
	and exists (select 1 from cte_TTT ttt
				inner join @OutLookNewList list
				on ttt.TimeTagId = list.ID
				where Trend.id = ttt.TrendId
				--and ttt.TimeTagId in (select id from @OutLookNewList
				and ttt.rnum =1)
	and (@Value = '' or (@Value = '<5%' and Trend.TrendValue< '5.0') or (@Value = '>5%' and Trend.TrendValue >= '5.0') or @Value = 'All' or @Value is null)
    --and cte1.TimeTagId = @Outlook
	AND Trend.TrendStatusID=@TrendApprovedStatusId
	order by Trend.ImportanceId desc
	-----------------	
END