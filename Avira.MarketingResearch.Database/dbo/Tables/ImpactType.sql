﻿CREATE TABLE [dbo].[ImpactType] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [ImpactTypeName]   NVARCHAR (256)   CONSTRAINT [DF_ImpactType_ImpactTypeName] DEFAULT ('') NOT NULL,
    [Description]      NVARCHAR (256)   NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_ImpactType_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_ImpactType] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ImpactType_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_ImpactType_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id])
);


GO

GO

GO

GO

GO

GO

GO