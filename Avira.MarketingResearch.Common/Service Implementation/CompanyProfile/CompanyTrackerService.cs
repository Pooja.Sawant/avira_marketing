﻿using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.CompanyProfile
{
    public class CompanyTrackerService : ICompanyTrackerService
    {
        private readonly ICompanyTrackerRepository _iCompanyTrackerRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;

        public CompanyTrackerService(IConfiguration configuration,
            IHostingEnvironment hostingEnvironment,
            ICompanyTrackerRepository iCompanyTrackerRepository
            )
        {
            _iCompanyTrackerRepository = iCompanyTrackerRepository;
            _hostingEnvironment = hostingEnvironment;
            _configuration = configuration;
        }

        public MainCompanyTrackerViewModel GetAll(Guid ProjectId, int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false)
        {
            MainCompanyTrackerViewModel mainCompanyTrackerViewModel = new MainCompanyTrackerViewModel();
            List<CompanyTrackerViewModel> CompanyTrackerViewMoel = _iCompanyTrackerRepository.GetAll(ProjectId, PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude).ToList();
            mainCompanyTrackerViewModel.CompanyTrackerList = CompanyTrackerViewMoel ?? new List<CompanyTrackerViewModel>();
            ProjectInformationViewModel projectInformationViewModel = null;// _iCompanyTrackerRepository.GetProjectInfo(ProjectId);
            mainCompanyTrackerViewModel.ProjectInformationViewModel = projectInformationViewModel ?? new ProjectInformationViewModel();
            return mainCompanyTrackerViewModel;
        }

        public CompanyTrackerViewModel GetById(Guid mapId)
        {
            return _iCompanyTrackerRepository.GetById(mapId);
        }
    }
}
