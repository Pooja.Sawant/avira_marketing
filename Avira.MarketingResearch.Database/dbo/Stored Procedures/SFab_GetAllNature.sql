﻿
  
CREATE PROCEDURE [dbo].[SFab_GetAllNature] 
(@UserID uniqueidentifier = null)
 AS  
/*================================================================================  
Procedure Name: [SFab_GetAllNature]  
Author: Sai Krishna  
Create date: 02/07/2019  
Description: Get list of Nature from table  
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
02/07/2019 Sai Krishna   Initial Version  
================================================================================*/  
  
BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
	
	SELECT Id as NatureId,
	ImpactDirectionName as Nature 
	FROM ImpactDirection 
	WHERE ISDELETED = 0

END