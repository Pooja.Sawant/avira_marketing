﻿using Avira.MarketingResearch.Common.IService.ImportExport;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.ImportExport
{
    public class CPFundamentalImportExportService : ICPFundamentalImportExportService
    {
        private readonly ICPFundamentalImportExportRepository _cpFundamentalImportExportRepository;

        public CPFundamentalImportExportService(ICPFundamentalImportExportRepository cpFundamentalImportExportRepository)
        {
            _cpFundamentalImportExportRepository = cpFundamentalImportExportRepository;
        }
        public List<ImportCommonResponseModel> Create(CompanyFundamentalsViewModel viewModel, AviraUser currentUser)
        {
            viewModel.JsonCompanyBasic = JsonConvert.SerializeObject(viewModel.CompanyBasicInfoList);
            viewModel.JsonRevenue = JsonConvert.SerializeObject(viewModel.CompanyRevenueList);
            viewModel.JsonCompanySegment = JsonConvert.SerializeObject(viewModel.CompanySegmentList);
            viewModel.JsonCompanyKeyword = JsonConvert.SerializeObject(viewModel.CompanyKeywordList);
            viewModel.JsonCompanyAnalystView = JsonConvert.SerializeObject(viewModel.CompanyAnalystViewList);
            var cpFundamentalsImportResponse = _cpFundamentalImportExportRepository.Create(viewModel);

            return cpFundamentalsImportResponse.ToList();
        }
    }
}
