﻿

CREATE procedure [dbo].[SApp_UpdateCurrencyConversion]
(@Id uniqueidentifier,
@CurrencyId uniqueidentifier,
@ConversionRangeType tinyint,
@ConversionRate decimal(19,4),
@StartDate Datetime,
@EndDate Datetime,
@Year int,
@Quarter varchar(2),
@UserModifiedById  uniqueidentifier,  
@ModifiedOn Datetime)
as
/*================================================================================
Procedure Name: SApp_UpdateCurrencyConversion
Author: Sharath
Create date: 04/16/2019
Description: update a record in CurrencyConversion table by ID.
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
04/16/2019	Sharath			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ErrorMsg NVARCHAR(2048);
declare @CheckStartDate datetime;
if @StartDate is null 
set @CheckStartDate = '01/01/1900'
else set @CheckStartDate = @StartDate; 
declare @CheckEndDate datetime;
if @EndDate is null
set @CheckEndDate ='12/31/2999'
else set @CheckEndDate = convert(varchar, @EndDate, 101) + ' 23:59:59.997';

--Validations
IF exists(select 1 from [dbo].CurrencyConversion 
		 where CurrencyId = @CurrencyId 
		 AND ((@ConversionRangeType = 1 AND (StartDate between @CheckStartDate and @CheckEndDate 
											or EndDate between @CheckStartDate and @CheckEndDate
											or (StartDate is null and @StartDate is null)
											or (EndDate is null and @EndDate is null))) --Time Range
			or (@ConversionRangeType = 2 AND [Year] = @Year) --Yearly
			or (@ConversionRangeType = 3 AND [Year] = @Year and [Quarter] = @Quarter)) --Quarterly
		 )
begin
--declare @ErrorMsg NVARCHAR(2048);
set @ErrorMsg = 'Currency conversion already exists.';
--THROW 50000,@ErrorMsg,1;
SELECT StoredProcedureName ='SApp_UpdateCurrencyConversion',Message =@ErrorMsg,Success = 0;  
RETURN;  

return(1)
end
--End of Validations

BEGIN TRY 
--update record

UPDATE [dbo].[CurrencyConversion]
   SET [CurrencyId] = @CurrencyId,
      [ConversionRangeType] = @ConversionRangeType,
      [ConversionRate] = @ConversionRate, 
      [StartDate] = @StartDate,
      [EndDate] = @EndDate, 
      [Year] = @Year,
      [Quarter] = @Quarter,
      [ModifiedOn] = @ModifiedOn,
      [UserModifiedById] = @UserModifiedById
where ID = @Id

SELECT StoredProcedureName ='SApp_UpdateCurrencyConversion',Message =@ErrorMsg,Success = 1;     
  
END TRY    
BEGIN CATCH    
    -- Execute the error retrieval routine.    
 DECLARE @ErrorNumber int;    
 DECLARE @ErrorSeverity int;    
 DECLARE @ErrorProcedure varchar(100);    
 DECLARE @ErrorLine int;    
 DECLARE @ErrorMessage varchar(500);    
    
  SELECT @ErrorNumber = ERROR_NUMBER(),    
        @ErrorSeverity = ERROR_SEVERITY(),    
        @ErrorProcedure = ERROR_PROCEDURE(),    
        @ErrorLine = ERROR_LINE(),    
        @ErrorMessage = ERROR_MESSAGE()    
    
 insert into dbo.Errorlog(ErrorNumber,    
       ErrorSeverity,    
       ErrorProcedure,    
       ErrorLine,    
       ErrorMessage,    
       ErrorDate)    
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE()); 

set @ErrorMsg = 'Error while updating Currency Conversion, please contact Admin for more details.' 		   
 --set @ErrorMsg = 'Unspecified Error';    
--THROW 50000,@ErrorMsg,1;    
 --return(1)  
 SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;     
END CATCH   
end