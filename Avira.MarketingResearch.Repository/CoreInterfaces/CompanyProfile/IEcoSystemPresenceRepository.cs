﻿using System;
using System.Collections.Generic;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using System.Text;
using System.Threading.Tasks;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile
{
    public interface IEcoSystemPresenceRepository
    {

        IEnumerable<EcoSystemPresenceMapViewModel> GetEcoSystemPresenceMapByCompanyId(Guid CompanyId);
        IEnumerable<SectorPresenceMapViewModel> GetSectorPresenceMapByCompanyId(Guid CompanyId);
        IEnumerable<EcoSystemPresenceKeyCompViewModel> GetEcoSystemPresenceKeyCompByCompanyId(Guid CompanyId);
        Result<SpTransactionMessage> InsertUpdate(EcoSystemPresenceDbViewModel ecoSystemPresenceDbViewModel);
    }
}
