﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.Masters
{
    public interface IStandardHoursService
    {
        IEnumerable<StandardHoursViewModel> GetAll(Guid Id);
        IEnumerable<StandardHoursViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false);
        StandardHoursViewModel GetById(Guid guid);
        Result<SpTransactionMessage> Update(StandardHoursUpdateDbViewModel tfc, AviraUser currenUser);
    }
}
