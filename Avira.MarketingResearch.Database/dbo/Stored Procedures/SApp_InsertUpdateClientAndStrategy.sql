﻿
--sp_helptext SApp_InsertUpdateCompanyTrendsMap
CREATE procedure [dbo].[SApp_InsertUpdateClientAndStrategy]        
(  
 @CompanyStrategy [dbo].[udtGUIDCompanyStrategyMultiValue] readonly,  
 @CompanyClients [dbo].[udtGUIDCompanyClientsMultiValue] readonly,  
 @CompanyId uniqueidentifier,  
 @UserCreatedById uniqueidentifier  
)        
as        
/*================================================================================        
Procedure Name: [SApp_InsertUpdateCompanyTrendsMap]        
Author: Laxmikant      
Create date: 05/08/2019        
Description: Insert a record into Company Strategy and client table        
Change History        
Date  Developer  Reason        
__________ ____________ ______________________________________________________        
05/08/2019 Laxmikant   Initial Version      
================================================================================*/        
BEGIN        
SET NOCOUNT ON;        
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;        
declare @ErrorMsg NVARCHAR(2048)    
      
        
BEGIN TRY           
--mark records as delete which are not in list  
update [dbo].[CompanyStrategyMapping]  
set IsDeleted = 1,  
UserDeletedById = @UserCreatedById,  
DeletedOn = GETDATE()  
from [dbo].[CompanyStrategyMapping]  
where [CompanyStrategyMapping].CompanyId = @CompanyId  
and not exists (select 1 from @CompanyStrategy Strategymap where Strategymap.Id = [CompanyStrategyMapping].Id)  

update [dbo].[CompanyClientsMapping]  
set IsDeleted = 1,  
UserDeletedById = @UserCreatedById,  
DeletedOn = GETDATE()  
from [dbo].[CompanyClientsMapping]  
where [CompanyClientsMapping].CompanyId = @CompanyId  
and not exists (select 1 from @CompanyClients Clientsmap where Clientsmap.Id = [CompanyClientsMapping].Id)
  
 --Update records for change  
update [dbo].[CompanyStrategyMapping]  
set Date = Strategymap1.Date,  
Approach = Strategymap1.Approach,  
Strategy = Strategymap1.Strategy,  
Category = Strategymap1.Category,  
Importance = Strategymap1.Importance,  
Impact = Strategymap1.Impact,  
Remark = Strategymap1.Remark,  
ModifiedOn = GETDATE(),  
UserModifiedById = @UserCreatedById  
from [dbo].[CompanyStrategyMapping]  
inner join @CompanyStrategy Strategymap1   
on Strategymap1.Id = [CompanyStrategyMapping].Id  
where [CompanyStrategyMapping].CompanyId = @CompanyId;

update [dbo].[CompanyClientsMapping]  
set ClientName = Clientsmap1.ClientName,  
ClientIndustry = Clientsmap1.ClientIndustry,
ClientRemark = Clientsmap1.ClientRemark,
ModifiedOn = GETDATE(),  
UserModifiedById = @UserCreatedById  
from [dbo].[CompanyClientsMapping]  
inner join @CompanyClients Clientsmap1   
on Clientsmap1.Id = [CompanyClientsMapping].Id  
where [CompanyClientsMapping].CompanyId = @CompanyId;    
  
--Insert new records  
insert into [dbo].[CompanyStrategyMapping](  
	[Id] 
	,[CompanyId]     
	,[Date]  
	,[Approach]  
	,[Strategy]  
	,[Category]  
	,[Importance]
	,[Impact]
	, [Remark] 
	,[IsDeleted]        
	,[UserCreatedById]        
	,[CreatedOn])        
	select Strategymap.Id, 
   Strategymap.CompanyId, 
   Strategymap.date,         
   Strategymap.Approach,  
   Strategymap.Strategy,  
   Strategymap.Category,  
   Strategymap.Importance,  
   Strategymap.Impact,  
   Strategymap.Remark,  
   0,        
   @UserCreatedById,         
   GETDATE()  
 from @CompanyStrategy Strategymap  
 where not exists (select 1 from [dbo].[CompanyStrategyMapping] Strategymap1  
     where Strategymap1.CompanyId = @CompanyId  
     and Strategymap.Id = Strategymap1.Id);    
	 
insert into [dbo].[CompanyClientsMapping](  
[Id]
,[CompanyId]      
,[ClientName]  
,[ClientIndustry]  
,[ClientRemark] 
,[IsDeleted]        
,[UserCreatedById]        
,[CreatedOn])        
 select ClientsMap.Id, 
   ClientsMap.CompanyId, 
   ClientsMap.ClientName,         
   ClientsMap.ClientIndustry,  
   ClientsMap.ClientRemark,
   0,        
   @UserCreatedById,         
   GETDATE()  
 from @CompanyClients ClientsMap  
 where not exists (select 1 from [dbo].[CompanyClientsMapping] ClientsMap1  
     where ClientsMap1.CompanyId = @CompanyId  
     and ClientsMap.Id = ClientsMap1.Id);     
       
SELECT StoredProcedureName ='SApp_InsertUpdateCompanyTrendsMap',Message =@ErrorMsg,Success = 1;         
      
END TRY        
BEGIN CATCH        
    -- Execute the error retrieval routine.        
 DECLARE @ErrorNumber int;        
 DECLARE @ErrorSeverity int;        
 DECLARE @ErrorProcedure varchar(100);          
 DECLARE @ErrorLine int;        
 DECLARE @ErrorMessage varchar(500);       
        
  SELECT @ErrorNumber = ERROR_NUMBER(),        
        @ErrorSeverity = ERROR_SEVERITY(),        
        @ErrorProcedure = ERROR_PROCEDURE(),        
        @ErrorLine = ERROR_LINE(),        
        @ErrorMessage = ERROR_MESSAGE()        
        
 insert into dbo.Errorlog(ErrorNumber,        
       ErrorSeverity,        
       ErrorProcedure,        
       ErrorLine,        
       ErrorMessage,        
       ErrorDate)        
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,ERROR_MESSAGE(),GETDATE());        
      
 set @ErrorMsg = 'Error while insert a new Company client and Strategy map, please contact Admin for more details.';      
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine      
    ,Message =@ErrorMsg,Success = 0; 
END CATCH  
end