﻿CREATE procedure [dbo].[SFab_GetQualitativeCountryMapbyID]
(@ProjectID uniqueidentifier=null,
@RegionIdList dbo.[udtUniqueIdentifier] readonly,
@includeDeleted bit = 0,
@AviraUserID uniqueidentifier = null)
as
/*================================================================================
Procedure Name: SFab_GetQualitativeCountryMapbyID
Author: Harshal 
Create date: 09/04/2019
Description: Get list from SFab_GetQualitativeCountryMapbyID table by Filters
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
09/04/2019	Praveen			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if @includeDeleted is null set @includeDeleted = 0

SELECT distinct
       Country.Id, Country.CountryName, Country.CountryUNCode, Country.CountryCode
	  
FROM Country
where (@includeDeleted = 1 or Country.IsDeleted = 0)
and 
exists (select 1 from @RegionIdList Rlist
			INNER JOIN CountryRegionMap 
			on Rlist.ID = CountryRegionMap.RegionId
			INNER JOIN [dbo].[QualitativeRegionMapping] MRP ON MRP.CountryId = CountryRegionMap.CountryId		
			where Country.Id = CountryRegionMap.CountryId
			and CountryRegionMap.IsDeleted=0 AND MRP.ProjectId = @ProjectID) 
ORDER BY CountryName 

end