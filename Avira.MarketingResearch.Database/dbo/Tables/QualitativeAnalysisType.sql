﻿CREATE TABLE [dbo].[QualitativeAnalysisType] (
    [Id]                          UNIQUEIDENTIFIER NOT NULL,
    [QualitativeAnalysisTypeName] NVARCHAR (256)   CONSTRAINT [DF_QualitativeAnalysisType_QualitativeAnalysisTypeName] DEFAULT ('') NOT NULL,
    [Description]                 NVARCHAR (256)   NULL,
    [CreatedOn]                   DATETIME2 (7)    NULL,
    [UserCreatedById]             UNIQUEIDENTIFIER NULL,
    [ModifiedOn]                  DATETIME2 (7)    NULL,
    [UserModifiedById]            UNIQUEIDENTIFIER NULL,
    [IsDeleted]                   BIT              CONSTRAINT [DF_QualitativeAnalysisType_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]                   DATETIME2 (7)    NULL,
    [UserDeletedById]             UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_QualitativeAnalysisType] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_QualitativeAnalysisType_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_QualitativeAnalysisType_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id])
);

