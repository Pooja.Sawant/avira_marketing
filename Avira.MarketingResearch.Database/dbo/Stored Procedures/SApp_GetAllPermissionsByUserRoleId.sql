﻿CREATE procedure [dbo].[SApp_GetAllPermissionsByUserRoleId]    
(@UserRoleId uniqueidentifier)    
as    
/*================================================================================    
Procedure Name: [SApp_GetPermissionsByUserRolesId]    
Author: Gopi    
Create date: 04/10/2019    
Description: Get Permissions, Forms and Menu Items Based on logged in User Role
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
04/10/2019 Gopi   Initial Version    
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    
--Permission list
select 
Permission.Id as PermissionId,
Permission.PermissionName,
UserRolePermissionMap.PermissionLevel,
UserType
from permission
inner join UserRolePermissionMap 
on Permission.Id = UserRolePermissionMap.PermissionId 
where UserRolePermissionMap.UserRoleId = @UserRoleId and UserRolePermissionMap.IsDeleted=0;

--Forms
select Id as FormId,
FormName
from Form
where exists (select 1 from FormPermissionMap 
			inner join UserRolePermissionMap
			on FormPermissionMap.PermissionId = UserRolePermissionMap.PermissionId
			where UserRolePermissionMap.UserRoleId = @UserRoleId
			and form.Id = FormPermissionMap.FormId)

--Menus

;with menu_src as (
select Id as MenuID,
MenuName,
ParentMenuID
from Menu
where exists (select 1 from MenuFormMap
			inner join FormPermissionMap
			on MenuFormMap.FormId = FormPermissionMap.FormId 
			inner join UserRolePermissionMap
			on FormPermissionMap.PermissionId = UserRolePermissionMap.PermissionId
			where UserRolePermissionMap.UserRoleId = @UserRoleId
			and menu.Id = MenuFormMap.MenuId)
union all
select ParentMenu.Id as MenuID,
ParentMenu.MenuName,
ParentMenu.ParentMenuID
from menu ParentMenu
inner join menu_src
on ParentMenu.Id = menu_src.ParentMenuID
)
select distinct 
MenuID,
MenuName,
ParentMenuID
 from menu_src

END