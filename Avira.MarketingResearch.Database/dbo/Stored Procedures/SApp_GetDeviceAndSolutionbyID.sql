﻿create procedure [dbo].[SApp_GetDeviceAndSolutionbyID]
(@DeviceAndSolutionID uniqueidentifier,
@IsProduct	bit,
@IsSoftware	bit,
@includeDeleted bit = 0)
as
/*================================================================================
Procedure Name: SApp_GetDeviceAndSolutionbyID
Author: Sharath
Create date: 02/19/2019
Description: Get list from DeviceAndSolution table by Name or ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
02/19/2019	Sharath			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if @includeDeleted is null set @includeDeleted = 0

SELECT [Id],
		[Name],
		IsProduct,
		IsSoftware,
		[Description]
FROM [dbo].[DeviceAndSolution]
where (@DeviceAndSolutionID is null or ID = @DeviceAndSolutionID)
and (@IsProduct is null or IsProduct = @IsProduct)
and (@IsSoftware is null or IsSoftware = @IsSoftware)
and (@includeDeleted = 1 or IsDeleted = 0)



end