﻿CREATE TABLE [dbo].[TrendTimeTag] (
    [ID]               UNIQUEIDENTIFIER NOT NULL,
    [TrendId]          UNIQUEIDENTIFIER NOT NULL,
    [TimeTagId]        UNIQUEIDENTIFIER NOT NULL,
    [Impact]           UNIQUEIDENTIFIER NULL,
    [EndUser]          BIT              NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UsermodifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    FOREIGN KEY ([UsermodifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    FOREIGN KEY ([UsermodifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    FOREIGN KEY ([UsermodifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK__TrendTime__Trend__7306036C] FOREIGN KEY ([TrendId]) REFERENCES [dbo].[Trend] ([Id]),
    CONSTRAINT [FK__TrendTime__Trend__76D69450] FOREIGN KEY ([TrendId]) REFERENCES [dbo].[Trend] ([Id]),
    CONSTRAINT [FK__TrendTime__UserD__092A4EB5] FOREIGN KEY ([TrendId]) REFERENCES [dbo].[Trend] ([Id])
);




GO
CREATE NONCLUSTERED INDEX [IX_TrendTimeTag_1]
    ON [dbo].[TrendTimeTag]([TimeTagId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_TrendTimeTag]
    ON [dbo].[TrendTimeTag]([TrendId] ASC);

