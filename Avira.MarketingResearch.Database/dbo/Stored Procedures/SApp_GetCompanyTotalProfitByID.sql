﻿--sp_helptext SApp_GetCompanyTotalProfitByID
-- =============================================              
-- Author:  Laxmikant              
-- Create date: 05/29/2019              
-- Description: Get Revenue Company total profit by Company Id        
-- exec SApp_GetCompanyTotalProfitByID '29DAC29E-04CC-4FB0-86B7-0E5471C23C17' ,0    
-- =============================================              
CREATE PROCEDURE [dbo].[SApp_GetCompanyTotalProfitByID]                   
 @ComapanyID uniqueidentifier = null,    
 @includeDeleted bit = 0                   
AS              
BEGIN               
 SET NOCOUNT ON;                
      
DECLARE @BaseYear int;    
SELECT  @BaseYear =  BaseYear from BaseYearMaster    
    
 SELECT CompanyProfit.[Id]      
   ,CompanyProfit.[CompanyId]      
      ,CompanyProfit.[ProjectId]      
      ,CompanyProfit.[CurrencyId]      
      ,CompanyProfit.[UnitId]      
      ,dbo.fun_UnitDeConversion(CompanyProfit.[UnitId],CompanyProfit.TotalProfit) as TotalProfit
   ,CompanyProfit.[Year]         
      ,CompanyProfit.[CreatedOn]        
      ,CompanyProfit.[UserCreatedById]        
      ,CompanyProfit.[ModifiedOn]        
      ,CompanyProfit.[UserModifiedById]        
      ,CompanyProfit.[IsDeleted]        
      ,CompanyProfit.[DeletedOn]        
      ,CompanyProfit.[UserDeletedById]        
   ,@BaseYear as BaseYearProfit     
    
  FROM [dbo].CompanyProfit              
           
  WHERE (CompanyProfit.CompanyId = @ComapanyID) --and CompanyProfit.[projectId] = @ProjectID)                    
 AND (@includeDeleted = 0 or CompanyProfit.IsDeleted = 1)                
END