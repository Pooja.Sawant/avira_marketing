﻿CREATE TABLE [dbo].[TrendStatus] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [TrendStatusName]  NVARCHAR (256)   CONSTRAINT [DF_Table_1_ProjectStatusName] DEFAULT ('') NOT NULL,
    [Description]      NVARCHAR (256)   NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_TrendStatus_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_TrendStatus] PRIMARY KEY CLUSTERED ([Id] ASC)
);