﻿

CREATE procedure [dbo].[SApp_insertAllCompanyProfileFromStagging_MultiTab_OtherInfo]
 @AviraUserId uniqueidentifier =null
as    
/*================================================================================    
Procedure Name: [SApp_insertAllCompanyProfileFromStagging_MultiTab_OtherInfo]    
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
19/06/2019 Praveen   Initial Version    
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    
declare @ErrorMsg NVARCHAR(2048)
declare @CompanyId	UNIQUEIDENTIFIER;

BEGIN TRY       
BEGIN TRANSACTION
Declare @USDCurrencyId uniqueidentifier	
Set @USDCurrencyId = dbo.[fun_GetCurrencyIDForUSD]()
select @CompanyId = CompanyId	
from [StagingCompanyProfileFundamentals];

	Exec [dbo].[SApp_DeleteCompanyProfileDetails] @CompanyId

--Fundamentals
UPDATE Company SET
Description = CPFStaging.Description,
Telephone=	CPFStaging.PhoneNumber1,
Telephone2=	CPFStaging.PhoneNumber2,
Telephone3=	CPFStaging.PhoneNumber3,
Website=	CPFStaging.Website,
YearOfEstb=	CPFStaging.Foundedyear,
CompanyStageId=	CPFStaging.CompanyStageId,
NoOfEmployees=	CPFStaging.NumberOfEmployee,
IsHeadQuarters=	1 ,
ModifiedOn=	GETDATE(),
UserModifiedById=	@AviraUserId,
IsDeleted=	0,
RankRationale=	CPFStaging.CompanyRank_Rationale,
RankNumber=	CPFStaging.CompanyRank_Rank,
EntityTypeId	=null 

--FROM 
--Select CPFStaging.CompanyId,
--CPFStaging.CompanyName,
----CPFStaging.CompanyCode, commented by praveen
----dbo.fun_NextCompanyCode(),
--CPFStaging.NatureId,
--CPFStaging.PhoneNumber1,
--CPFStaging.PhoneNumber2,
--CPFStaging.PhoneNumber3,
--CPFStaging.Website,
--CPFStaging.Foundedyear,
--CPFStaging.CompanyStageId,
--CPFStaging.NumberOfEmployee,
--CPFStaging.Headquarter,
--1 as IsHeadQuarters,
--GETDATE(),
--@AviraUserId,
--0,
--CPFStaging.CompanyRank_Rationale,
--CPFStaging.CompanyRank_Rank,
--null as EntityTypeId
FROM Company 
inner join  [StagingCompanyProfileFundamentals] CPFStaging
on CPFStaging.CompanyId = Company.Id


--Share Holding Pattern
insert into [ShareHoldingPatternMap](
Id,
ShareHoldingName,
CompanyId,
EntityTypeId,
ShareHoldingPercentage,
CreatedOn,
UserCreatedById,
IsDeleted)
select newid(),
csh.Name,
csh.CompanyId,
csh.EntityTypeId,
csh.Holding,
getdate(),
@AviraUserId,
0
from [StagingCompanyProfileShareHolding] csh;



	Update Company
	Set CompanyLocation = CPS.CompanySubsdiaryLocation
	From Company C 
	Inner Join [StagingCompanyProfileSubsidiaries] CPS 
		On  C.CompanyName = CPS.CompanySubsdiaryName And C.ParentId = CPS.CompanyId 
	

	--Subsidiaries
	Insert Into Company(Id,
		--CompanyCode,
		CompanyName,
		ParentCompanyYearOfEstb,
		ProfiledCompanyName,
		ParentId,
		RankNumber,
		RankRationale,
		CompanyLocation,
		IsHeadQuarters,
		IsDeleted,
		CreatedOn,
		UserCreatedById)
		SELECT NEWID(),
		--CompanyCode,Commented by praveen
		--dbo.fun_NextCompanyCode(),
		CPSStaging.CompanySubsdiaryName,
		SPF.Foundedyear as ParentCompanyYearOfEstb,
		SPF.CompanyName as ParentCompanyName,
		SPF.CompanyId as ParentId,
		SPF.CompanyRank_Rank as RankNumber,
		SPF.CompanyRank_Rationale as RankRationale,
		CPSStaging.CompanySubsdiaryLocation as CompanyLocation,
		0 as IsHeadQuarters,
		0 as IsDeleted,
		getdate(),
		@AviraUserId
		--,Company.Id	
	From [StagingCompanyProfileSubsidiaries] CPSStaging Inner Join [StagingCompanyProfileFundamentals] SPF On CPSStaging.CompanyId = SPF.CompanyId
		Left Join Company On Company.CompanyName = CPSStaging.CompanySubsdiaryName 
	Where Company.Id is Null;
	

--Import for Organization 
;with CTED as (select KeyEmployeeDesignation as Designation
			FROM [StagingCompanyKeyEmployees]
			WHERE KeyEmployeeDesignation != ''
			UNION 
			select BoardofDirectorOtherCompanyDesignation
			FROM [StagingCompanyBoardOfDirectors]
			WHERE BoardofDirectorOtherCompanyDesignation != ''
			)
insert into Designation
(Id,
Designation,
Description,
CreatedOn,
UserCreatedById,
IsVisible,
IsDeleted)
select distinct 
NEWID(),
CTED.Designation,
CTED.Designation,
getdate(),
@AviraUserId,
1,
0
from CTED
where Designation != ''
and not exists (select 1 from Designation d1
				where d1.Designation = CTED.Designation
				and d1.IsDeleted =0);

		--BoDs
		Insert Into CompanyKeyEmployeesMap(
					Id,
					KeyEmployeeName,
					DesignationId,
					KeyEmployeeEmailId,
					CompanyBoardOfDirecorsOtherCompanyName,
					CompanyBoardOfDirecorsOtherCompanyDesignation,
					IsDirector,
					CompanyId,
					CreatedOn,
					UserCreatedById,
					IsDeleted)
		Select NEWID(),
				cgkemp.BoardofDirectorName,
				D.Id as DesignationId,
				cgkemp.BoardofDirectorEmail,
				cgkemp.BoardofDirectorOtherCompany,
				cgkemp.BoardofDirectorOtherCompanyDesignation,
				1 as IsDirector,
				cgkemp.companyId,
					GETDATE(),
				@AviraUserId,
				0
		From [StagingCompanyBoardOfDirectors] cgkemp
		inner join Designation D
		on cgkemp.BoardofDirectorOtherCompanyDesignation = D.Designation;

--Key Employees
Insert Into CompanyKeyEmployeesMap(
			Id,
			KeyEmployeeName,
			DesignationId,
			KeyEmployeeEmailId,
			KeyEmployeeComments,
			IsDirector,
			CompanyId,
			CreatedOn,
			UserCreatedById,
			IsDeleted)
Select NEWID(),
	   cgkemp.KeyEmployeeName,
	   D.Id as DesignationId,
	   cgkemp.KeyEmployeeEmail,
	   '' as KeyEmployeeComments,
	   0 as IsDirector,
	   cgkemp.CompanyId,
		 GETDATE(),
	   @AviraUserId,
	   0
From [StagingCompanyKeyEmployees] cgkemp
inner join Designation D
on cgkemp.KeyEmployeeDesignation = D.Designation;

;with CTE_Kemp as (SELECT KeyEmployeeName,
						KeyEmployeeEmail,
						D.Id as MGRDesignationID,
						D.Designation as MGRDesignation
					FROM [StagingCompanyKeyEmployees] cgkemp
					INNER JOIN Designation D
					ON cgkemp.ReportingManager = D.Designation
					)
update ckemp
set ManagerId = (select top 1 ID from CompanyKeyEmployeesMap mgr 
				 where mgr.DesignationId = CTE_Kemp.MGRDesignationID 
				 and mgr.CompanyId = @CompanyId )
from CompanyKeyEmployeesMap ckemp
inner join CTE_Kemp
ON ckemp.KeyEmployeeName = CTE_Kemp.KeyEmployeeName
where ckemp.CompanyId = @CompanyId;



	Execute [SApp_ImportCompanyProductForCompanyProfile] @CompanyId, @AviraUserId

declare @SegmentId uniqueidentifier;
select @SegmentId = Id from Segment
where SegmentName = 'End User';

--Client 
CREATE TABLE #tmp_Client(
                        [Id] [uniqueidentifier],
						[CompanyId] [uniqueidentifier],
						[ClientName] [nvarchar](256),
						[ClientIndustry] [nvarchar](256),
                        [ClientRemark] [nvarchar](512),
						[CreatedOn] [datetime2],
                        [UserCreatedById] [uniqueidentifier],
                        [IsDeleted] [bit]);

insert into #tmp_Client
select NEWID(),
       SCC.CompanyId,
       CN.Value,
       (select top 1 ID from SubSegment ind where ind.SubSegmentName = SCC.ClientIndustry and ind.SegmentId = @SegmentId) as ClientIndustryID,
       SCC.Remarks as  ClientRemark,
	   getdate(),
	   @AviraUserId as UserCreatedById,
       0 as IsDeleted
FROM StagingCompanyClients SCC
CROSS APPLY dbo.FUN_STRING_TOKENIZER(SCC.ClientName,',') CN
Where Not Exists(SELECT ClientName FROM CompanyClientsMapping where ClientName=CN.Value AND CompanyId=@CompanyId
     AND ClientIndustry=(select top 1 ID from SubSegment ind where ind.SubSegmentName = SCC.ClientIndustry and ind.SegmentId = @SegmentId));



INSERT INTO [dbo].[CompanyClientsMapping]
           ([Id]
           ,[CompanyId]
           ,[ClientName]
           ,[ClientIndustry]
           ,[ClientRemark]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted]
           )
 SELECT     [Id]
           ,[CompanyId]
           ,[ClientName]
           ,[ClientIndustry]
           ,[ClientRemark]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted] 
		   from
(
Select Row_Number() over(Partition By CompanyId,ClientName,ClientIndustry order By CompanyId,ClientName,ClientIndustry) as row,* from #tmp_Client
) a
 where a.row=1;

 drop table #tmp_Client; 
 
--select NEWID(),
--       SCC.CompanyId,
--       CN.Value,
--       (select top 1 ID from SubSegment ind where ind.SubSegmentName = SCC.ClientIndustry and ind.SegmentId = @SegmentId) as ClientIndustryID,
--       SCC.Remarks as  ClientRemark,
--		getdate(),
--		@AviraUserId as UserCreatedById,
--0 as IsDeleted
--FROM StagingCompanyClients SCC
--CROSS APPLY dbo.FUN_STRING_TOKENIZER(SCC.ClientName,',') CN
--Where Not Exists(SELECT ClientName FROM CompanyClientsMapping where ClientName=CN.Value AND CompanyId=@CompanyId);

--Strategies
INSERT INTO [dbo].[CompanyStrategyMapping]
           ([Id]
           ,[CompanyId]
           ,[Date]
           ,[Approach]
           ,[Strategy]
           ,[Category]
           ,[Importance]
		   ,Impact
           ,[Remark]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
select newid(),
       Strategy.CompanyId as CompanyId,
       Strategy.Date,
       Strategy.ApproachId,
	   Strategy.Strategy,
       Strategy.CategoryId,
       Strategy.ImportanceId,
	   Strategy.ImpactId,
       Strategy.Remarks,
	   getdate() as CreatedOn,
       @AviraUserId as UserCreatedById,
       0 as IsDeleted
from StagingCompanyStrategy Strategy;

--News
INSERT INTO [dbo].[News]
           ([Id]
           ,[Date]
           ,[News]
           ,[NewsCategoryId]
           ,[ImportanceId]
           ,[ImpactDirectionId]
           ,[OtherParticipants]
           ,[Remarks]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted]
           ,[CompanyID])
     select
           newid(),
           News.NewsDate,
           News.News,
           News.CategoryId,
           News.ImportanceId, 
           News.ImpactId, 
           News.OtherParticipants, 
           News.Remarks,
		   getdate() as CreatedOn,
		   @AviraUserId as UserCreatedById,
		   0 as IsDeleted,
           News.CompanyID
from [StagingCompanyNews] News

--Ecosystem
INSERT INTO [dbo].[CompanyEcosystemPresenceESMMap]
           ([Id]
           ,[CompanyId]
           ,[IsChecked]
           ,[ESMId]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
select NEWID(),
    SCESM.CompanyId,
    1 AS IsChecked,
    Segment.ID as  ESMId,
    GETDATE(),
    @AviraUserId,
    0 as IsDeleted
FROM [StagingCompanyEcosystemPresence] SCESM
cross apply dbo.FUN_STRING_TOKENIZER(SCESM.ESMPresence, ',') ESMP
inner join Segment
on ESMP.Value = Segment.SegmentName;

--ESM Segment and Subsegment
INSERT INTO [dbo].[CompanyEcosystemSegmentMap]
           ([Id]
           ,[CompanyId]
           ,[SegmentId]
		   ,[SubSegmentId]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
select NEWID(),
    SCPES.CompanyId,   
    S.Id As [SegmentId],
	SS.Id As [SubSegmentId],
    GETDATE(),
    @AviraUserId,
    0 as IsDeleted
FROM StagingCompanyEcosystemSegmentMap SCPES
	Outer Apply (Select Position, [Value] From dbo.FUN_STRING_TOKENIZER(SCPES.SubsegmentName, ',') Where [Value] is not null And [Value] != '') SCPESubSegment
	Inner Join Segment S On
		SCPES.SegmentName = S.SegmentName 
	Left Join SubSegment SS On
		SCPESubSegment.Value = SS.SubSegmentName And
		S.Id = SS.SegmentId

INSERT INTO [dbo].[CompanyEcosystemPresenceSectorMap]
           ([Id]
           ,[CompanyId]
           ,[IsChecked]
           ,[SectorId]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted]
           )
select NEWID(),
    SCESM.CompanyId,
    1 AS IsChecked,
    Industry.ID as  [SectorId],
    GETDATE(),
    @AviraUserId,
    0 as IsDeleted
FROM [StagingCompanyEcosystemPresence] SCESM
cross apply dbo.FUN_STRING_TOKENIZER(SCESM.SectorPresence, ',') ESMP
inner join SubSegment Industry
on ESMP.Value = Industry.SubSegmentName
and Industry.SegmentId = @SegmentId;



CREATE TABLE #tmp_KeyCompetiters(
                        [Id] [uniqueidentifier],
						[CompanyId] [uniqueidentifier],
						[KeyCompetiters] [nvarchar](256),
						[CreatedOn] [datetime2],
                        [UserCreatedById] [uniqueidentifier],
                        [IsDeleted] [bit]);

insert into #tmp_KeyCompetiters
select NEWID(),
       SCESM.CompanyId,
       ESMP.Value,
	   getdate(),
	   @AviraUserId as UserCreatedById,
       0 as IsDeleted
FROM [StagingCompanyEcosystemPresence] SCESM
CROSS APPLY dbo.FUN_STRING_TOKENIZER(SCESM.KeyCompetitor,',') ESMP
Where Not Exists(SELECT KeyCompetiters FROM CompanyEcosystemPresenceKeyCompetitersMap where CompanyEcosystemPresenceKeyCompetitersMap.KeyCompetiters=ESMP.Value AND CompanyId=@CompanyId);


INSERT INTO [dbo].[CompanyEcosystemPresenceKeyCompetitersMap]
           ([Id]
           ,[CompanyId]
           ,[KeyCompetiters]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted]
           )
 SELECT     [Id]
           ,[CompanyId]
           ,[KeyCompetiters]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted] 
		   from
(
Select Row_Number() over(Partition By CompanyId,KeyCompetiters order By CompanyId,KeyCompetiters) as row,* from #tmp_KeyCompetiters
) a
 where a.row=1;

 drop table #tmp_KeyCompetiters; 



--INSERT INTO [dbo].[CompanyEcosystemPresenceKeyCompetitersMap]
--           ([Id]
--           ,[CompanyId]
--           ,[KeyCompetiters]
--           ,[CreatedOn]
--           ,[UserCreatedById]
--           ,[IsDeleted])
--select NEWID(),
--    SCESM.CompanyId,
--    ESMP.Value,
--    GETDATE(),
--    @AviraUserId,
--    0 as IsDeleted
--FROM [StagingCompanyEcosystemPresence] SCESM
--CROSS APPLY dbo.FUN_STRING_TOKENIZER(SCESM.KeyCompetitor,',') ESMP




--SWOT
INSERT INTO [dbo].[CompanySWOTMap]
           ([Id]
           ,[CompanyId]
		   ,[ProjectId]
           ,[CategoryId]
           ,[SWOTTypeId]
           ,[SWOTDescription]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
select NEWID(),
    SWOT.CompanyId,
	SWOT.ProjectId,
    SWOT.CategoryId,
    SWOT.SWOTTypeId,
     SWOT.AnalysisPoint as SWOTDescription,
    getdate() as CreatedOn,
    @AviraUserId,
	0 as IsDeleted
FROM [StagingCompanySWOT] SWOT;

--Trends
INSERT INTO [dbo].[CompanyTrendsMap]
           ([Id]
           ,[TrendName]
           ,[Department]
           ,[ImportanceId]
           ,[ImpactId]
           ,[CompanyId]
           ,[ProjectId]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted]
           )
SELECT NEWID(),
SCT.Trends,
SCT.[Department],
SCT.ImportanceId,
SCT.ImpactId,
SCT.CompanyId,
SCT.ProjectId,
getdate() as [CreatedOn],
@AviraUserId,
0 as [IsDeleted]
FROM [StagingCompanyTrends] SCT;

--Analyst Views
 INSERT INTO [dbo].[CompanyAnalystView]
           ([Id]
           ,[CompanyId]
           ,[ProjectId]
           ,[AnalystView]
		   ,[AnalystViewDate]
		   ,[CategoryId]
		   ,[ImportanceId]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
select NEWID(),
    SCAV.CompanyId,
	SCAV.ProjectId,
    STRING_AGG(SCAV.AnalystView, char(13)+ char(10)) ,
	SCAV.DateOfUpdate,
	SCAV.CategoryId,
	SCAV.ImportanceId,
    getdate() as CreatedOn,
    @AviraUserId,
	0 as IsDeleted
	FROM StagingCompanyAnalystView SCAV
	group by SCAV.CompanyId, SCAV.ProjectId,SCAV.CategoryId,SCAV.ImportanceId,SCAV.DateOfUpdate;

--Price Volume
 INSERT INTO [dbo].[CompanyPriceVolume]
           ( [Id]
            ,[Date]
            ,[Price]
            ,[CurrencyId]
            ,[CurrencyUnitId]
            ,[Volume]
            ,[VolumeUnitId]
            ,[CompanyId]
            ,[CreatedOn]
            ,[UserCreatedById]
            ,[IsDeleted])
select NEWID(),
       SCPV.[Date]
      ,--SCPV.[Price]
	  [dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](SCPV.[CurrencyUnitId], SCPV.[Price]), SCPV.[CurrencyId], @USDCurrencyId, getdate())
      ,SCPV.[CurrencyId]
	  ,SCPV.[CurrencyUnitId]
      ,--SCPV.[Volume]
	  [dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](SCPV.[VolumeUnitId], SCPV.[Volume]), SCPV.[CurrencyId], @USDCurrencyId, getdate())
      ,SCPV.[VolumeUnitId]
      ,SCPV.[CompanyId]
      ,getdate() as CreatedOn
      ,@AviraUserId
      ,0 as IsDeleted
	FROM StagingCompanyPriceVolume SCPV;

;with CTEProj as ( select projectID from StagingCompanyAnalystView union 
select projectID from StagingCompanySWOT union 
select projectID from StagingCompanyTrends)
INSERT INTO [dbo].[CompanyProjectMap]
           ([Id]
           ,[CompanyId]
           ,[ProjectId]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
select newid(),
@CompanyId,
CTEProj.ProjectId,
GETDATE(),
@AviraUserId,
0 as [IsDeleted]
from CTEProj
where ProjectId is not null;



declare @CompanyProfileStatusId uniqueidentifier;
select @CompanyProfileStatusId = ID
from CompanyProfileStatus cps
where cps.CompanyStatusName = 'Approved'

INSERT INTO [dbo].[CompanyNote]
           ([Id]
           ,[CompanyId]
           ,[ProjectId]
           ,[CompanyType]
           ,[AuthorRemark]
           ,[ApproverRemark]
           ,[CompanyStatusID]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
select NEWID(), @CompanyId, null as [ProjectId], 'CompanyOtherInfo' as [CompanyType], 'From Import' as [AuthorRemark], 'From Import' as [ApproverRemark], @CompanyProfileStatusId as CompanyStatusId, GETDATE(), @AviraUserId, 0 as [IsDeleted] union all
select NEWID(), @CompanyId, null as [ProjectId], 'CompanyShare' as [CompanyType], 'From Import' as [AuthorRemark], 'From Import' as [ApproverRemark], @CompanyProfileStatusId as CompanyStatusId, GETDATE(), @AviraUserId, 0 as [IsDeleted] union all
select NEWID(), @CompanyId, null as [ProjectId], 'CompanyClientAndStrategy' as [CompanyType], 'From Import' as [AuthorRemark], 'From Import' as [ApproverRemark], @CompanyProfileStatusId as CompanyStatusId, GETDATE(), @AviraUserId, 0 as [IsDeleted] union all
select NEWID(), @CompanyId, null as [ProjectId], 'CompanyNews' as [CompanyType], 'From Import' as [AuthorRemark], 'From Import' as [ApproverRemark], @CompanyProfileStatusId as CompanyStatusId, GETDATE(), @AviraUserId, 0 as [IsDeleted] union all
--select NEWID(), @CompanyId, null as [ProjectId], 'CompanyProduct' as [CompanyType], 'From Import' as [AuthorRemark], 'From Import' as [ApproverRemark], @CompanyProfileStatusId as CompanyStatusId, GETDATE(), @AviraUserId, 0 as [IsDeleted] union all
select NEWID(), @CompanyId, null as [ProjectId], 'EcoSystemPresence' as [CompanyType], 'From Import' as [AuthorRemark], 'From Import' as [ApproverRemark], @CompanyProfileStatusId as CompanyStatusId, GETDATE(), @AviraUserId, 0 as [IsDeleted] union all
select NEWID(), @CompanyId, null as [ProjectId], 'PriceVolume' as [CompanyType], 'From Import' as [AuthorRemark], 'From Import' as [ApproverRemark], @CompanyProfileStatusId as CompanyStatusId, GETDATE(), @AviraUserId, 0 as [IsDeleted];

with CTEProj as ( select 'CompanyAnalystView' as [CompanyType], ProjectID from StagingCompanyAnalystView union 
select 'CompanySWOT' as [CompanyType], ProjectID from StagingCompanySWOT union 
select 'CompanyProduct' as [CompanyType], ProjectID from StagingCompanyProduct union 
select 'CompanyTrends' as [CompanyType], ProjectID from StagingCompanyTrends)
INSERT INTO [dbo].[CompanyNote]
           ([Id]
           ,[CompanyId]
           ,[ProjectId]
           ,[CompanyType]
           ,[AuthorRemark]
           ,[ApproverRemark]
           ,[CompanyStatusID]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
select NEWID(), @CompanyId, [ProjectId], [CompanyType], 'From Import' as [AuthorRemark], 'From Import' as [ApproverRemark], @CompanyProfileStatusId as CompanyStatusId, GETDATE(), @AviraUserId, 0 as [IsDeleted] 
from CTEProj;

SELECT StoredProcedureName ='[SApp_insertAllCompanyProfileFromStagging_MultiTab_OtherInfo]',Message =@ErrorMsg,Success = 1;     
COMMIT TRANSACTION
END TRY    
BEGIN CATCH    
 if @@TRANCOUNT > 0 
 ROLLBACK TRANSACTION;

  ---Update Statics--
 UPDATE STATISTICS [Company] WITH FULLSCAN;
 UPDATE STATISTICS [ShareHoldingPatternMap] WITH FULLSCAN;
 UPDATE STATISTICS [CompanyKeyEmployeesMap] WITH FULLSCAN;
 UPDATE STATISTICS [News] WITH FULLSCAN;
 UPDATE STATISTICS [CompanyEcosystemPresenceESMMap] WITH FULLSCAN;
 UPDATE STATISTICS [CompanyEcosystemPresenceSectorMap] WITH FULLSCAN;
 UPDATE STATISTICS [CompanyEcosystemPresenceKeyCompetitersMap] WITH FULLSCAN;
 UPDATE STATISTICS [CompanySWOTMap] WITH FULLSCAN;
 UPDATE STATISTICS [CompanyTrendsMap] WITH FULLSCAN;
 UPDATE STATISTICS [CompanyAnalystView] WITH FULLSCAN;
 UPDATE STATISTICS [CompanyProjectMap] WITH FULLSCAN;
 UPDATE STATISTICS [CompanyNote] WITH FULLSCAN;
 UPDATE STATISTICS [CompanyClientsMapping] WITH FULLSCAN;
 UPDATE STATISTICS [CompanyStrategyMapping] WITH FULLSCAN;
 UPDATE STATISTICS [CompanyPriceVolume] WITH FULLSCAN;
 UPDATE STATISTICS [CompanyEcosystemSegmentMap] WITH FULLSCAN;
 UPDATE STATISTICS [CompanyNote] WITH FULLSCAN;
 
 ---End Statics----
 
    -- Execute the error retrieval routine.    
 DECLARE @ErrorNumber int;    
 DECLARE @ErrorSeverity int;    
 DECLARE @ErrorProcedure varchar(100);    
 DECLARE @ErrorLine int;    
 DECLARE @ErrorMessage varchar(500);    
    
  SELECT @ErrorNumber = ERROR_NUMBER(),    
        @ErrorSeverity = ERROR_SEVERITY(),    
        @ErrorProcedure = 'SApp_insertAllCompanyProfileFromStagging_MultiTab_OtherInfo',    
        @ErrorLine = ERROR_LINE(),    
        @ErrorMessage = ERROR_MESSAGE()    
    
 insert into dbo.Errorlog(ErrorNumber,    
       ErrorSeverity,    
       ErrorProcedure,    
       ErrorLine,    
       ErrorMessage,    
       ErrorDate)    
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());    
  
 set @ErrorMsg = 'Error while creating a new Company Profile, please contact Admin for more details.';    
--THROW 50000,@ErrorMsg,1;      
--return(1)  
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;     
     
END CATCH    
    
    
end