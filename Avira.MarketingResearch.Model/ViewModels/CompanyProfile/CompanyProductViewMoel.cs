﻿using Avira.MarketingResearch.Model.CustomValidation;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using static Avira.MarketingResearch.Model.ViewModels.CompanyProfile.CompanyNoteViewModel;
using static Avira.MarketingResearch.Model.ViewModels.CompanyProfile.CompanyProductCountryMapViewModel;

namespace Avira.MarketingResearch.Model.ViewModels.CompanyProfile
{
    public class CompanyProductViewMoel
    {
        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
        public string ProjectName { get; set; }
        public Guid ProjectId { get; set; }
        public string ProductName { get; set; }
        public DateTime LaunchDate { get; set; }
        public string Patents { get; set; }
        public string Description { get; set; }
        public string ProductStatusName { get; set; }
        public Guid ProductStatusId { get; set; }
        [ValidateImageAttribute]
        public IFormFile ImageFile { get; set; }
        public string ImageURL { get; set; }
        public string ImageDisplayName { get; set; }
        public string ImageActualName { get; set; }
        public string fileExtension { get; set; }
        public byte[] fileByte { get; set; }
        public string CurrencyName { get; set; }
        public Guid CurrencyId { get; set; }
        public string ValueName { get; set; }
        public Guid ValueConversionId { get; set; }
        public string Amount { get; set; }
        public int Year { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        //public List<MainCompanyProductCountryMapInsertUpdateDbViewModel> MainCompanyProductCountryMapInsertUpdateDbViewModel { get; set; }
        //public List<MainCompanyProductSegmentMapInsertUpdateDbViewModel> MainCompanyProductSegmentMapInsertUpdateDbViewModel { get; set; }
        //public List<MainCompanyProductCategoryMapInsertUpdateDbViewModel> MainCompanyProductCategoryMapInsertUpdateDbViewModel { get; set; }

        public Guid[] ProductCategoryId { get; set; }
        public Guid[] ProductSegmentId { get; set; }
        public Guid[] ProductCountryId { get; set; }

        public string RowType
        {
            get
            {
                return "CompanyProduct";
            }
        }
    }

    public class MainCompanyProductViewMoel
    {
        public Guid CompanyId { get; set; }
        public Guid ProjectId { get; set; }
        public List<CompanyProductViewMoel> CompanyProductViewMoel { get; set; }
        public CompanyNoteViewModel CompanyNoteViewModel { get; set; }
    }

   


}
