﻿using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Utility;
using Avira.MarketingResearch.WebApi;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.Repository.CoreInterfaces.Masters;
using Avira.MarketingResearch.Common.IService.Masters;
using Avira.MarketingResearch.Model.ViewModels.Masters;

namespace Avira.MarketingResearch.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyGroupController : AviraBaseController
    {
        private ILoggerManager _logger;
        public readonly ICompanyGroupRepository _companyGroupRepository;
        public readonly ICompanyGroupService _companyGroupService;
        public CompanyGroupController(
            ICompanyGroupService companyGroupService,
            ICompanyGroupRepository companyGroupRepository,
            ILoggerManager logger)
        {
            _companyGroupRepository = companyGroupRepository;
            _companyGroupService = companyGroupService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<CompanyGroupViewModel>> Get( int PageStart=0, int pageSize=0, int SortDirection=1, string OrdbyByColumnName= "CreatedDate", string Search = "", bool isDeletedInclude = false)
        {
            List<CompanyGroupViewModel> res = null;
            try
            {
                res= _companyGroupService.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }


        [HttpGet("{id}")]
        public ActionResult<CompanyGroupViewModel> Get(Guid id)
        {
            CompanyGroupViewModel companyGroup = null;
            try
            {
                 companyGroup = _companyGroupService.GetById(id);
            if (companyGroup == null)
            {
                return NotFound();
            }
        }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
                        return companyGroup;
        }

        [HttpPost]
        public ActionResult Post([FromBody] CompanyGroupInsertDbViewModel viewModel)
        {
            viewModel.CreatedOn = System.DateTime.UtcNow;
            viewModel.UserCreatedById = CurrentUser.Id;
            viewModel.Id = Guid.NewGuid();

            
            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new CompanyGroup, please contact Admin for more details."
            };

            try
            {

                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                var result = _companyGroupService.Create(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "CompanyGroup name " + viewModel.CompanyGroupName + " is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }

            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);

        }


        [HttpPut]
        public ActionResult Put([FromBody] CompanyGroupUpdateDbViewModel viewModel)
        {

            viewModel.ModifiedOn = System.DateTime.UtcNow;
            viewModel.UserModifiedById = CurrentUser.Id;


            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while updating a new CompanyGroup, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                Result<SpTransactionMessage> result = _companyGroupService.Update(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "CompanyGroup name " + viewModel.CompanyGroupName + " is updated successfully.";

                }
                else
                {
                    viewData.Message = result.Error;
                }
            }

            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }

        [Route("DeleteSegment")]
        [HttpPut]
        public ActionResult DeleteSegment([FromBody] CompanyGroupUpdateDbViewModel viewModel)
        {
           
                viewModel.DeletedOn = System.DateTime.UtcNow;
                viewModel.UserDeletedById = CurrentUser.Id;
                viewModel.IsDeleted = true;


                IViewModel viewData = new ViewData()
                {
                    IsSuccess = false,
                    Message = "Error while deleting a new CompanyGroup, please contact Admin for more details."
                };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                Result<SpTransactionMessage> result = _companyGroupService.Update(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;

                    viewData.Message = "CompanyGroup name " + viewModel.CompanyGroupName + " is deleted.";

                }
                else
                {
                    viewData.Message = result.Error;
                }
            }

            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }

    }
}
