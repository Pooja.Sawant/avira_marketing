﻿  
--udtUniqueIdentifier  
CREATE procedure [dbo].[SApp_insertAviraUserRoleMap]  
(  
@AviraUserId uniqueidentifier,  
--@UserRoleList dbo.udtGUIDIntValue READONLY,  
@UserRoleList dbo.udtGUIDBitValue READONLY,  
@UserCreatedById uniqueidentifier,    
@CreatedOn Datetime)  
as  
/*================================================================================  
Procedure Name: SApp_insertAviraUserRoleMap  
Author: Adarsh  
Create date: 03/16/2019  
Description: Insert a record into Application table  
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
03/16/2019 Adarsh   Initial Version  
================================================================================*/  
BEGIN  
SET NOCOUNT ON;  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
DECLARE @ErrorMsg NVARCHAR(2048);  
  
BEGIN TRY  
  
delete [dbo].[AviraUserRoleMap]  
where [AviraUserId] = @AviraUserId AND IsDeleted = 0;  
  
INSERT INTO [dbo].[AviraUserRoleMap]  
           ([Id]  
           ,AviraUserId  
     ,UserRoleId  
           ,[CreatedOn]  
           ,[UserCreatedById]  
           ,[IsDeleted]  
     ,[IsPrimary]  
           )  
   select NEWID(),  
   @AviraUserId,  
   ID,  
   @CreatedOn,  
   @UserCreatedById,  
   0,  
   bitValue  
   from @UserRoleList; 
   
   
   update AviraUser 
   set  UserRoleId = lst.ID 
   from @UserRoleList lst
   inner join AviraUser 
   on AviraUser.Id = @AviraUserId
  
--select @Id;  
--return(0)  
SELECT StoredProcedureName ='SApp_insertAviraUserRoleMap',Message =@ErrorMsg,Success = 1;    
END TRY  
BEGIN CATCH  
    -- Execute the error retrieval routine.  
 DECLARE @ErrorNumber int;  
 DECLARE @ErrorSeverity int;  
 DECLARE @ErrorProcedure varchar;  
 DECLARE @ErrorLine int;  
 DECLARE @ErrorMessage varchar;  
  
  SELECT @ErrorNumber = ERROR_NUMBER(),  
        @ErrorSeverity = ERROR_SEVERITY(),  
        @ErrorProcedure = ERROR_PROCEDURE(),  
        @ErrorLine = ERROR_LINE(),  
        @ErrorMessage = ERROR_MESSAGE()  
  
 insert into dbo.Errorlog(ErrorNumber,  
       ErrorSeverity,  
       ErrorProcedure,  
       ErrorLine,  
       ErrorMessage,  
       ErrorDate)  
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,@CreatedOn);  
  
 set @ErrorMsg = 'Error while adding role to User, please contact Admin for more details.';  
  
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;  
  
END CATCH  
  
  
end