﻿CREATE procedure [dbo].[SFab_GetCIAnalyisPageAllTableHeadersByProjectId]
(@ProjectId uniqueidentifier=null,
@includeDeleted bit = 0,
@AviraUserID uniqueidentifier = null)
as
/*================================================================================
Procedure Name: SFab_GetCIAnlyisPageAllTableHeadersByProjectId 
Author: Harshal
Create date: 07/16/2019
Description: Get list from QualitativeTableTab by Project Id
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
07/16/2019	Harshal			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ProjectApprovedStatusId uniqueidentifier
select @ProjectApprovedStatusId = ID from ProjectStatus
where ProjectStatusName = 'Completed';

if @includeDeleted is null set @includeDeleted = 0

SELECT        QualitativeTableTab.Id,QualitativeTableTab.ProjectId, QualitativeTableTab.TableName
FROM            QualitativeTableTab 
WHERE        (QualitativeTableTab.ProjectId = @ProjectId) 
and exists (select 1 from project 
			where project.ID = QualitativeTableTab.ProjectID
			and project.ProjectStatusID = @ProjectApprovedStatusId)
and (QualitativeTableTab.IsDeleted=0 or QualitativeTableTab.IsDeleted is null)
order by QualitativeTableTab.TableName asc
end