﻿
var popup, dataTable; var pp;
//$(document).ready(Load); 
$(document).ready(function () {
   
    Load();
   
});

function Load() {
    $.ajax({
        url: "https://localhost:44331/api/v1/country",
        method: "GET",
        dataType: "JSON",
        success: function (data) {
           
                    //var tr;
                    //for (var i = 0; i < data.length; i++) {
                    //    tr = $('<tr/>');
                    //    tr.append("<td>" + data[i].countryName + "</td>");
                    //    tr.append("<td>" + data[i].countryCode + "</td>");
                    //    tr.append("<td>" + data[i].createdOn + "</td>");
                    //    tr.append("<td>" + data[i].userCreatedById + "</td>");
                    //    tr.append("<td>" + data[i].modifiedOn + "</td>");
                    //    tr.append("<td>" + data[i].userModifiedById + "</td>");
                    //    tr.append("<td>" + data[i].isDeleted + "</td>");
                    //    tr.append("<td>" + data[i].deletedOn + "</td>");
                    //    tr.append("<td>" + data[i].userDeletedById + "</td>");                   
                    //    tr.append("<td><a class='btn btn-default btn-xs btn-rounded p-l-10 p-r-10 editor_edit' onclick=ShowPopup('/Home/AddEditCountry/" + data[i].id + "')><i class='fa fa-cog'></i>  Edit</a><a class='btn btn-default btn-xs btn-rounded p-l-10 p-r-10 editor_remove' style='margin-left:5px' onclick=Delete('" + data[i].id + "')><i class='fa fa-trash'></i> Delete</a></td>");
                      
                    //    $("#data-table tbody").append(tr);
                    //}



         //   var cand = document.getElementById("cand");
            ////$("#tbodyid").empty();
            ////var json_arr = [];
            ////var tr;
            ////$.each(data.countries, function (key, value) {
            ////    tr = $('<tr/>');
            ////    tr.append("<td>" + value.countryName + "</td>");
            ////    tr.append("<td>" + value.countryCode + "</td>");
            ////    tr.append("<td style='display: none'>" + value.createdOn + "</td>");
            ////    tr.append("<td style='display: none'>" + value.userCreatedById + "</td>");
            ////    tr.append("<td style='display: none'>" + value.modifiedOn + "</td>");
            ////    tr.append("<td style='display: none'>" + value.userModifiedById + "</td>");
            ////    tr.append("<td style='display: none'>" + value.isDeleted + "</td>");
            ////    tr.append("<td style='display: none'>" + value.deletedOn + "</td>");
            ////    tr.append("<td style='display: none'>" + value.userDeletedById + "</td>");
            ////    tr.append("<td ><a class='btn btn-default btn-xs btn-rounded p-l-10 p-r-10 editor_edit' onclick=ShowPopup('/Home/AddEditCountry/" + value.id + "')><i class='fa fa-cog'></i>  Edit</a><a class='btn btn-default btn-xs btn-rounded p-l-10 p-r-10 editor_remove' style='margin-left:5px' onclick=Delete('" + value.id + "')><i class='fa fa-trash'></i> Delete</a></td>");

            ////    $("#data-table tbody").append(tr);
            ////    //json_arr.push(key + ' . ' + value.name + '<br>');
            ////    //cand.innerHTML = json_arr;
            ////});

            pp = $("#data-table").DataTable({
                destroy: true,
                data: data.countries,
                "columns": [
                    { data: "countryName" },
                    { data: "countryCode" },
                    { data: "createdOn" },
                    { data: "userCreatedById" },
                    { data: "modifiedOn" },
                    { data: "userModifiedById" },
                    { data: "isDeleted" },
                    { data: "deletedOn" },
                    { data: "userDeletedById" },
                    {
                        data: "id",
                        render: function (data) {
                            return "<a class='btn btn-default btn-xs btn-rounded p-l-10 p-r-10 editor_edit' onclick=ShowPopup('/Home/AddEditCountry/" + data + "')><i class='fa fa-cog'></i>  Edit</a><a class='btn btn-default btn-xs btn-rounded p-l-10 p-r-10 editor_remove' style='margin-left:5px' onclick=Delete('" + data + "')><i class='fa fa-trash'></i> Delete</a>";
                        }
                    }
                ],

                "columnDefs": [
                    {
                        "targets": [2],
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": [3],
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": [4],
                        "visible": false
                    },
                    {
                        "targets": [5],
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": [6],
                        "visible": false
                    }, {
                        "targets": [7],
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": [8],
                        "visible": false
                    },
                ],
                language: {
                    "emptyTable": "no data found."
                }
            });
        }
    });


}
function SubmitAddEdit(form) {
    //$.validator.unobtrusive.parse(form);
    //if ($(form).valid()) {
    var data = $(form).serializeJSON();
    data = JSON.stringify(data);
    $.ajax({
        type: 'POST',
        url: 'https://localhost:44331/api/v1/country/',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: data,
        success: function (data) {
            if (data.success) {
              
                popup.dialog('close');
                //window.location.reload();
                //Load();
               // $("#data-table").DataTable.ajax.reload();
        
                //pp.fnDraw();
                //location.reload(); 
               
                //window.location.reload();
               // $("#data-table").DataTable.ajax.reload();
                //ShowMessage(data.message);
                pp.destroy();
                 Load();
                //dataTable.ajax.reload();  
            }
        }
    });

    //}
    return false;
};
function ShowPopup(url) {
    var formDiv = $('<div/>');   
    $.get(url)
        .done(function (response) {
            formDiv.html(response);
            popup = formDiv.dialog({
                autoOpen: true,
                resizeable: false,
                width: 600,
                height: 400,
                title: 'Add or Edit Data',
                close: function () {
                    popup.dialog('destroy').remove();
                }
            });
        });
};



function Delete(url) {
    swal({
        title: "Are you sure want to Delete?",
        text: "You will not be able to restore the file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: true
    }, function () {
        $.ajax({
            type: 'DELETE',
            url: '/api/v1/country/' + url,
            success: function (data) {
                if (data.success) {
                    ShowMessage(data.message);
                   // dataTable.ajax.reload();  
                    Load();
                }
            }
        });
    });


};