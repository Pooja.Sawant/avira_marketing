﻿CREATE procedure [dbo].[SApp_insertUpdateCustomer]
(@CustomerID uniqueidentifier,
@CustomerName nvarchar(250),
@DisplayName nvarchar(128),
@IsDeleted bit,
@AviraUserId uniqueidentifier)
as
/*================================================================================
Procedure Name: SApp_DeleteCustomerbyID
Author: Sharath
Create date: 02/11/2019
Description: soft delete a record from Customer table by ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
02/11/2019	Sharath			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

IF exists(select 1 from [dbo].[Customer] 
		 where ((@CustomerID is null or [Id] !=  @CustomerID)
		 and  CustomerName = @CustomerName AND IsDeleted = 0))
begin
declare @ErrorMsg NVARCHAR(2048);
set @ErrorMsg = 'Customer with Name "'+ @CustomerName + '" already exists.';
THROW 50000,@ErrorMsg,1;
end

IF (@CustomerID is null)
begin
--Insert new record
	set @CustomerID = NEWID();

	insert into [dbo].[Customer]([Id],
								[CustomerName],
								[DisplayName],
								[IsDeleted],
								[UserCreatedById],
								[CreatedOn])
	values(@CustomerID, 
			@CustomerName, 
			@DisplayName, 
			0,
			@AviraUserId, 
			getdate());
end
else
begin
--update record
	update [dbo].[Customer]
	set [CustomerName] = @CustomerName,
		[DisplayName] = @DisplayName,
		[ModifiedOn] = getdate(),
		DeletedOn = case when IsDeleted = 0 and @IsDeleted = 1 then getdate() else DeletedOn end,
		UserDeletedById = case when IsDeleted = 0 and @IsDeleted = 1 then @AviraUserId else UserDeletedById end,
		IsDeleted = isnull(@IsDeleted, IsDeleted),
		[UserModifiedById] = @AviraUserId
     where ID = @CustomerID
end

select @CustomerID;
end