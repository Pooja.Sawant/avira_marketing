﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyTrendsMapController : AviraBaseController
    {
        private readonly ICompanyTrendsMapService _companyTrendsMapService;
        private readonly ICompanyTrendsMapRepository _companyTrendsMapRepository;
        private ILoggerManager _logger;

        public CompanyTrendsMapController(
            ICompanyTrendsMapService companyTrendsMapService,
            ICompanyTrendsMapRepository companyTrendsMapRepository,
             ILoggerManager logger)
        {
            _companyTrendsMapRepository = companyTrendsMapRepository;
            _companyTrendsMapService = companyTrendsMapService;
            _logger = logger;

        }

        [HttpGet("{id}")]
        public ActionResult<IEnumerable<CompanyTrendsMapViewModel>> Get(Guid Id)
        {
            List<CompanyTrendsMapViewModel> res = null;
            try
            {
                 res = _companyTrendsMapService.GetAll(Id).ToList();
        }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }


        [HttpPost]
        public ActionResult Post([FromBody] MainCompanyTrendsMapInsertUpdateDbViewModel viewModel)
        {
            viewModel.CreatedOn = System.DateTime.UtcNow;
            viewModel.AviraUserId = CurrentUser.Id;

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new CompanyTrendsMap, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                var result = _companyTrendsMapService.InsertUpdate(viewModel, CurrentUser);
                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "CompanyTrendMap is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }

       
    }
}