﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels
{
    public class RoleViewModel
    {
        public Guid Id { get; set; }

        public string UserRoleName { get; set; }

        public string PermissionName { get; set; }

        public int PermissionLevel { get; set; }

        public int UserType { get; set; }

        public DateTime? CreatedOn { get; set; }

        public Guid? UserCreatedById { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public Guid? UserModifiedById { get; set; }

        public bool? IsDeleted { get; set; }

        public DateTime? DeletedOn { get; set; }

        public Guid? UserDeletedById { get; set; }

        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "Role";
            }
        }
    }

    public class RoleInsertDbViewModel
    {
        public Guid? UserRoleID { get; set; }

        public List<string> UserPermisionID { get; set; }

        [Required(ErrorMessage = "Please enter User Role Name")]
        [Display(Name = "User Role Name")]
        public string UserRoleName { get; set; }

        [Display(Name = "Permission Name")]
        public string PermissionName { get; set; }

        public int[] PermissionLevel { get; set; }

        public int UserType { get; set; }

        public DateTime? CreatedOn { get; set; }

        public Guid? UserCreatedById { get; set; }

        public bool? IsDeleted { get; set; }

        public bool IsChecked { get; set; }
    }

    public class RoleEditViewModel
    {
        public Guid Id { get; set; }

        public Guid UserPermisionID { get; set; }

        public string UserRoleName { get; set; }

        public string PermissionName { get; set; }

        public int PermissionLevel { get; set; }

        public int UserType { get; set; }

        public DateTime? CreatedOn { get; set; }

        public Guid? UserCreatedById { get; set; }

        public bool? IsDeleted { get; set; }

        public bool IsChecked { get; set; }

        public string RowType
        {
            get
            {
                return "Role";
            }
        }
    }

    public class RoleUpdateViewModel
    {
        public Guid Id { get; set; }

        public List<Guid> UserPermisionID { get; set; }

        public string UserRoleName { get; set; }

        public string PermissionName { get; set; }

        public int[] PermissionLevel { get; set; }

        public int UserType { get; set; }

        public DateTime? CreatedOn { get; set; }

        public Guid? UserCreatedById { get; set; }

        public bool? IsDeleted { get; set; }

        public Guid UserDeletedById { get; set; }

        public bool IsChecked { get; set; }

        public string RowType
        {
            get
            {
                return "Role";
            }
        }
    }

}
