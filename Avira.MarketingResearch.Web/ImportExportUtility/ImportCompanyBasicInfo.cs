﻿using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Avira.MarketingResearch.Web.ImportExportUtility
{
    public class ImportCompanyBasicInfo : Controller, IExcelImport
    {
        public readonly IServiceRepository _serviceRepository;
        private ILoggerManager _logger;

        public ImportCompanyBasicInfo(IServiceRepository serviceRepository, ILoggerManager logger)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
        }

        public ImportResponse Import(IFormFile formFile)
        {
            var res = new ImportResponse();
            var inputModel = new CompanyBasicInfoImportInsertModel();
            using (var stream = new MemoryStream())
            {
                formFile.CopyTo(stream);

                using (var package = new ExcelPackage(stream))
                {
                    try
                    {
                        if (package.Workbook.Worksheets["Basics"] != null)
                        {
                            inputModel.CompanyBasicInfoList = GetCompanyBasic(package.Workbook.Worksheets["Basics"]);
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'Basics' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'Basics' worksheet is missing.");
                            return res;
                        }

                        List<CompanyBasicInfoRevenueImportModel> YearWiseRevenueList = new List<CompanyBasicInfoRevenueImportModel>();
                        if (inputModel.CompanyBasicInfoList.Count > 0)
                        {
                            for (int i = 0; i < inputModel.CompanyBasicInfoList.Count; i++)
                            {
                                CompanyBasicInfoRevenueImportModel model = new CompanyBasicInfoRevenueImportModel();
                                //    DateTime yearValue;
                                //    decimal value;
                                //    if (DateTime.TryParseExact(inputModel.CompanyBasicInfoList[i].Year1, "yyyy-MM", CultureInfo.InvariantCulture,DateTimeStyles.None, out yearValue))
                                //    {
                                //        CompanyBasicInfoRevenueImportModel model = new CompanyBasicInfoRevenueImportModel();
                                //        model.Year = inputModel.CompanyBasicInfoList[i].Year1;
                                //        if (Decimal.TryParse(inputModel.CompanyBasicInfoList[i].RevenueYear1, out value))
                                //        {
                                //            if(value<0)
                                //            {
                                //                res.ResponseStatus = "Error";
                                //                res.ResponseMessage = "Error while reading template. 'RevenueYear1 value not less than zero.'";
                                //                res.TotalFailedRecords = 1;
                                //                _logger.LogError("Error while reading template. 'RevenueYear1 value not less than zero.'");
                                //                return res;
                                //            }
                                //            model.Revenue = inputModel.CompanyBasicInfoList[i].RevenueYear1;
                                //        }
                                //        else
                                //        {
                                //            if (!string.IsNullOrEmpty(inputModel.CompanyBasicInfoList[i].RevenueYear1))
                                //            {
                                //                res.ResponseStatus = "Error";
                                //                res.ResponseMessage = "Error while reading template. 'RevenueYear1 value not in proper Format.'";
                                //                res.TotalFailedRecords = 1;
                                //                _logger.LogError("Error while reading template. 'RevenueYear1 value not in proper Format.'");
                                //                return res;
                                //            }
                                //        }

                                //        model.Currency = inputModel.CompanyBasicInfoList[i].Currency;
                                //        model.Units = inputModel.CompanyBasicInfoList[i].Units;
                                //        model.CompanyName = inputModel.CompanyBasicInfoList[i].CompanyName;
                                //        model.RowNo= inputModel.CompanyBasicInfoList[i].RowNo;
                                //        YearWiseRevenueList.Add(model);

                                //    }
                                //    else
                                //    {
                                //        if (!string.IsNullOrEmpty(inputModel.CompanyBasicInfoList[i].Year1))
                                //        {
                                //            res.ResponseStatus = "Error";
                                //            res.ResponseMessage = "Error while reading template. 'Year1 value not in proper Format.'";
                                //            res.TotalFailedRecords = 1;
                                //            _logger.LogError("Error while reading template. 'Year1 value not in proper Format.'");
                                //            return res;
                                //        }
                                //    }
                                //    if (DateTime.TryParseExact(inputModel.CompanyBasicInfoList[i].Year2, "yyyy-MM", CultureInfo.InvariantCulture, DateTimeStyles.None, out yearValue))
                                //    {
                                //        CompanyBasicInfoRevenueImportModel model = new CompanyBasicInfoRevenueImportModel();
                                //        model.Year = inputModel.CompanyBasicInfoList[i].Year2;
                                //        if (Decimal.TryParse(inputModel.CompanyBasicInfoList[i].RevenueYear2, out value))
                                //        {
                                //            if (value < 0)
                                //            {
                                //                res.ResponseStatus = "Error";
                                //                res.ResponseMessage = "Error while reading template. 'RevenueYear2 value not less than zero.'";
                                //                res.TotalFailedRecords = 1;
                                //                _logger.LogError("Error while reading template. 'RevenueYear2 value not less than zero.'");
                                //                return res;
                                //            }
                                //            model.Revenue = inputModel.CompanyBasicInfoList[i].RevenueYear2;
                                //        }
                                //        else
                                //        {
                                //            if (!string.IsNullOrEmpty(inputModel.CompanyBasicInfoList[i].RevenueYear2))
                                //            {
                                //                res.ResponseStatus = "Error";
                                //                res.ResponseMessage = "Error while reading template. 'RevenueYear2 value not in proper Format.'";
                                //                res.TotalFailedRecords = 1;
                                //                _logger.LogError("Error while reading template. 'RevenueYear2 value not in proper Format.'");
                                //                return res;
                                //            }
                                //        }
                                //        model.Currency = inputModel.CompanyBasicInfoList[i].Currency;
                                //        model.Units = inputModel.CompanyBasicInfoList[i].Units;
                                //        model.CompanyName = inputModel.CompanyBasicInfoList[i].CompanyName;
                                //        model.RowNo = inputModel.CompanyBasicInfoList[i].RowNo;
                                //        YearWiseRevenueList.Add(model);
                                //    }
                                //    else
                                //    {
                                //        if (!string.IsNullOrEmpty(inputModel.CompanyBasicInfoList[i].Year2))
                                //        {
                                //            res.ResponseStatus = "Error";
                                //            res.ResponseMessage = "Error while reading template. 'Year2 value not in proper Format.'";
                                //            res.TotalFailedRecords = 1;
                                //            _logger.LogError("Error while reading template. 'Year2 value not in proper Format.'");
                                //            return res;
                                //        }
                                //    }


                                model.CompanyName = inputModel.CompanyBasicInfoList[i].CompanyName;
                                model.RowNo = inputModel.CompanyBasicInfoList[i].RowNo;
                                YearWiseRevenueList.Add(model);
                            }
                          
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Company Basic sheet is mandatory.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Company Basic sheet is mandatory.");
                            return res;
                        }
                        inputModel.YearWiseRevenueList = YearWiseRevenueList;
                    }
                    catch (Exception ex)
                    {
                        res.ResponseStatus = "Error";
                        res.ResponseMessage = "Error while reading data. Error Message: " + ex.Message + ". Please check input excel. More details logged in error log.";
                        res.TotalFailedRecords = 1;
                        _logger.LogError(ex.Message);
                        return res;
                    }
                }
            }

            //--------Call api to post data            
            inputModel.TemplateName = "CompanyProfile basic Import Template";
            inputModel.ImportFileName = formFile.FileName;

            try
            {

                HttpResponseMessage response = _serviceRepository.PostResponse("CompanyBasicInfoImportExport", inputModel);
                response.EnsureSuccessStatusCode();
                ImportCommonResponseMainModel viewModel = response.Content.ReadAsAsync<ImportCommonResponseMainModel>().Result;

                res.ResponseStatus = viewModel.IsSuccess ? "Success" : "Failed";
                res.ResponseMessage = viewModel.Message;
                res.TotalRecords = inputModel.CompanyBasicInfoList.Count;
                if (viewModel.ResponseModel == null)
                {
                    res.ResponseStatus = "Success";
                    res.TotalSuccessRecords = (inputModel.CompanyBasicInfoList == null ? 0 : inputModel.CompanyBasicInfoList.Count);
                    res.TotalFailedRecords = 0;
                }
                else
                {
                    res.ResponseMessage = "Please check downloaded file for company basic error list";
                    res.TotalSuccessRecords = ((inputModel.CompanyBasicInfoList == null ? 0 : inputModel.CompanyBasicInfoList.Count)) - (viewModel.ResponseModel.Count());
                    res.TotalFailedRecords = viewModel.ResponseModel.Count();
                }
                res.ResponseModel = viewModel.ResponseModel;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }            
            return res;
        }

        private int GetColIndex(ExcelWorksheet worksheet, string colName)
        {
            int colIndex = worksheet.Cells["6:6"].First(c => c.Value.ToString() == colName).Start.Column;

            return colIndex;
        }

        private List<CompanyBasicInfoImportModel> GetCompanyBasic(ExcelWorksheet worksheet)
        {
            List<CompanyBasicInfoImportModel> companyBasicInfoList = new List<CompanyBasicInfoImportModel>();

            var rowCount = worksheet.Dimension.Rows;

            int colIndexCompanyName = GetColIndex(worksheet, "Company Name");
            //int colIndexHeadquarter = GetColIndex(worksheet, "Headquarter");
            //int colIndexNature = GetColIndex(worksheet, "Nature");
            //int colIndexGeographicPresnece = GetColIndex(worksheet, "Geographic Presence");
            //int colIndexCurrency = GetColIndex(worksheet, "Currency");
            //int colIndexUnits = GetColIndex(worksheet, "Units");
            //int colIndexYear1= GetColIndex(worksheet, "Year1");
            //int colIndexRevenueYear1 = GetColIndex(worksheet, "RevenueYear1");
            //int colIndexYear2 = GetColIndex(worksheet, "Year2");
            //int colIndexRevenueYear2 = GetColIndex(worksheet, "RevenueYear2");

            for (int row = 7; row <= rowCount; row++)
            {
                CompanyBasicInfoImportModel companyBasicInfo = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

                //if (!string.IsNullOrEmpty(cn))
                //{
                    companyBasicInfo = new CompanyBasicInfoImportModel
                    {
                        RowNo = row,
                        CompanyName = worksheet.Cells[row, colIndexCompanyName].Value?.ToString().Trim()
                        //Headquarter = worksheet.Cells[row, colIndexHeadquarter].Value?.ToString().Trim(),
                        //Nature = worksheet.Cells[row, colIndexNature].Value?.ToString().Trim(),
                        //GeographicPresence = worksheet.Cells[row, colIndexGeographicPresnece].Value?.ToString().Trim(),
                        //Currency = worksheet.Cells[row, colIndexCurrency].Value?.ToString().Trim(),
                        //Units = worksheet.Cells[row, colIndexUnits].Value?.ToString().Trim(),
                        //Year1 = worksheet.Cells[row, colIndexYear1].Value?.ToString().Trim(),
                        //RevenueYear1 = worksheet.Cells[row, colIndexRevenueYear1].Value?.ToString().Trim(),
                        //Year2 = worksheet.Cells[row, colIndexYear2].Value?.ToString().Trim(),
                        //RevenueYear2 = worksheet.Cells[row, colIndexRevenueYear2].Value?.ToString().Trim()
                    };
                    companyBasicInfoList.Add(companyBasicInfo);
                //}
            }
            return companyBasicInfoList;
        }
    }
}
