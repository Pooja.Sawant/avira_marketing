﻿

CREATE procedure [dbo].[SApp_insertUpdateCompanyBoardOfDirectorsMap]      
(
	@KeyBoardOfDirectorsListmap [dbo].[udtGUIDKeyEmpMultiValue] readonly,
	@CompanyId uniqueidentifier,
	@UserCreatedById uniqueidentifier
)      
as      
/*================================================================================      
Procedure Name: [SApp_insertUpdateCompanyBoardOfDirectorsMap]      
Author: Sai Krishna      
Create date: 05/06/2019      
Description: Insert a record into CompanyBoardOfDirecorsMap table      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
05/06/2019 Sai Krishna   Initial Version    
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)  
    
      
--BEGIN TRY         

--Delete from [dbo].[CompanyKeyEmployeesMap]
--Where [CompanyKeyEmployeesMap].[CompanyId] = @CompanyId
--and IsDirector = 1;
--Update [CompanyKeyEmployeesMap]
--Set [KeyEmployeeName] = keymap.[KeyEmployeeName],
--	DesignationId = keymap.DesignationId,
--	[KeyEmployeeEmailId] = keymap.[KeyEmployeeEmailId],
--	[CompanyBoardOfDirecorsOtherCompanyName] =keymap.[CompanyBoardOfDirecorsOtherCompanyName],
--	[CompanyBoardOfDirecorsOtherCompanyDesignation] = keymap.[CompanyBoardOfDirecorsOtherCompanyDesignation],
--	[CompanyId] = @CompanyId,
--	ManagerId = keymap.ManagerId,
--	[UserModifiedById] =  @UserCreatedById,
--	[ModifiedOn] = GETDATE()
--	From CompanyKeyEmployeesMap
--	Inner Join @KeyBoardOfDirectorsListmap keymap
--	on CompanyKeyEmployeesMap.CompanyId = @CompanyId
--	and CompanyKeyEmployeesMap.Id = keymap.ID
--	and CompanyKeyEmployeesMap.IsDirector = 1
--	Where IsDeleted = 0
--	--if exists (select 1 from @KeyBoardOfDirectorsListmap keyboardmap
--	--		  inner join CompanyKeyEmployeesMap ComKeyEmp on 
--	--		  ComKeyEmp.Id = keyboardmap.ID
--	--		  where ComKeyEmp.Id = @CompanyId
--	--		  and ComKeyEmp.IsDirector = 1)

--mark records as delete which are not in list
--update [dbo].CompanyKeyEmployeesMap
--set IsDeleted = 1,
--UserDeletedById = @UserCreatedById,
--DeletedOn = GETDATE()
--from [dbo].CompanyKeyEmployeesMap
--where CompanyKeyEmployeesMap.CompanyId = @CompanyId
--and exists (select 1 from @KeyBoardOfDirectorsListmap map where map.id = CompanyKeyEmployeesMap.Id and CompanyKeyEmployeesMap.IsDirector = 1)


 --Update records for change
update [dbo].CompanyKeyEmployeesMap
Set [KeyEmployeeName] = keymap.[KeyEmployeeName],
	DesignationId = keymap.DesignationId,
	[KeyEmployeeEmailId] = keymap.[KeyEmployeeEmailId],
	[CompanyBoardOfDirecorsOtherCompanyName] =keymap.[CompanyBoardOfDirecorsOtherCompanyName],
	[CompanyBoardOfDirecorsOtherCompanyDesignation] = keymap.[CompanyBoardOfDirecorsOtherCompanyDesignation],
	[CompanyId] = @CompanyId,
	ManagerId = keymap.ManagerId,
	[UserModifiedById] =  @UserCreatedById,
	[ModifiedOn] = GETDATE()
	From CompanyKeyEmployeesMap
	Inner Join @KeyBoardOfDirectorsListmap keymap
	on CompanyKeyEmployeesMap.Id = keymap.ID
	--and CompanyKeyEmployeesMap.IsDirector = 1
	Where CompanyKeyEmployeesMap.CompanyId = @CompanyId
	and exists (select 1 from @KeyBoardOfDirectorsListmap map where map.id = CompanyKeyEmployeesMap.Id and CompanyKeyEmployeesMap.IsDirector = 1)




--Insert new records
insert into [dbo].[CompanyKeyEmployeesMap](
		[Id],      
        [KeyEmployeeName], 
		DesignationId,
		[KeyEmployeeEmailId],
		[CompanyBoardOfDirecorsOtherCompanyName],
		[CompanyBoardOfDirecorsOtherCompanyDesignation],
		IsDirector,
		[CompanyId],
		ManagerId,
        [IsDeleted],      
        [UserCreatedById],      
        [CreatedOn])      
 select NEWID(),
   map.[KeyEmployeeName], 
   map.DesignationId,      
   map.[KeyEmployeeEmailId],
   map.CompanyBoardOfDirecorsOtherCompanyName,
   map.CompanyBoardOfDirecorsOtherCompanyDesignation,
   1,
   @CompanyId,    
   map.[ManagerID],
   0,      
   @UserCreatedById,       
   GETDATE()
 from @KeyBoardOfDirectorsListmap map
 where not exists (select 1 from [dbo].[CompanyKeyEmployeesMap] map2
					where map2.CompanyId = @CompanyId
					and map.ID = map2.Id and map2.IsDirector = 1);       
     
SELECT StoredProcedureName ='SApp_insertUpdateCompanyBoardOfDirectorsMap',Message =@ErrorMsg,Success = 1;       
    
--END TRY      
--BEGIN CATCH      
    -- Execute the error retrieval routine.      
-- DECLARE @ErrorNumber int;      
-- DECLARE @ErrorSeverity int;      
-- DECLARE @ErrorProcedure varchar(100);      
-- DECLARE @ErrorLine int;      
-- DECLARE @ErrorMessage varchar(500);      
      
--  SELECT @ErrorNumber = ERROR_NUMBER(),      
--        @ErrorSeverity = ERROR_SEVERITY(),      
--        @ErrorProcedure = ERROR_PROCEDURE(),      
--        @ErrorLine = ERROR_LINE(),      
--        @ErrorMessage = ERROR_MESSAGE()      
      
-- insert into dbo.Errorlog(ErrorNumber,      
--       ErrorSeverity,      
--       ErrorProcedure,      
--       ErrorLine,      
--       ErrorMessage,      
--       ErrorDate)      
--        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());      
    
-- set @ErrorMsg = 'Error while insert a new TrendMarketMap, please contact Admin for more details.';    
--SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
--    ,Message =@ErrorMsg,Success = 0;       
       
--END CATCH      
      
      
end