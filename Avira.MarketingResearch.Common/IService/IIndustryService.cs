﻿using Avira.MarketingResearch.Model;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService
{
    public interface IIndustryService
    {
        IEnumerable<IndustryViewModel> GetAll(Guid Id);
        IEnumerable<IndustryViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false);
        IEnumerable<IndustryViewModel> GetAllParentIndustry();
        IndustryViewModel GetById(Guid guid);

        Result<SpTransactionMessage> Create(IndustryInsertDbViewModel tfc, AviraUser currentUser);
        Result<SpTransactionMessage> Update(IndustryUpdateDbViewModel tfc, AviraUser currenUser);
    }
}
