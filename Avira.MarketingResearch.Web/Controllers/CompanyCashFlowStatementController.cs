﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.Utils;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Avira.MarketingResearch.Web.Controllers
{
    [Authorize]
    [NoCache]
    public class CompanyCashFlowStatementController : BaseController
    {
        public readonly IServiceRepository _serviceRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;
        string contentRootPath = string.Empty;
        private ILoggerManager _logger;

        public CompanyCashFlowStatementController(IServiceRepository serviceRepository, ILoggerManager logger, IHostingEnvironment hostingEnvironment, IConfiguration configuration)
        {
            _hostingEnvironment = hostingEnvironment;
            _serviceRepository = serviceRepository;
            _logger = logger;
            _configuration = configuration;
            contentRootPath = _hostingEnvironment.ContentRootPath;
        }

        public IActionResult Index(string CompanyId, string ProjectId)
        {
            ViewBag.Message = string.Empty;
            SetRoleList();
            MainCompanyCashFlowStatementViewModel Model = new MainCompanyCashFlowStatementViewModel();
            List<CompanyCashFlowStatementViewModel> CompanyBalanceSheetViewModelList = new List<CompanyCashFlowStatementViewModel>();
            Model.ProjectId = new Guid(ProjectId);
            Model.CompanyId = new Guid(CompanyId);
            var ContollerName = this.ControllerContext.RouteData.Values["controller"].ToString();

            HttpResponseMessage response = _serviceRepository.GetByIdResponse(ContollerName, CompanyId);
            response.EnsureSuccessStatusCode();
            List<CompanyCashFlowStatementViewModel> companyCashFlowStatementViewModelList = response.Content.ReadAsAsync<List<CompanyCashFlowStatementViewModel>>().Result;
            if (companyCashFlowStatementViewModelList != null && companyCashFlowStatementViewModelList.Count > 0)
            {
                Model.CompanyCashFlowStatementViewModelList = companyCashFlowStatementViewModelList.OrderByDescending(x => x.Year).ToList(); 
                Model.YearList = Model.CompanyCashFlowStatementViewModelList.Select(x => x.Year).ToList();
            }
            else
            {
                Model.CompanyCashFlowStatementViewModelList = new List<CompanyCashFlowStatementViewModel>();
                Model.YearList = new List<int>();
            }

            HttpResponseMessage CompanyNoteResponse = _serviceRepository.GetCompanyNoteByIdResponse("CompanyNote", CompanyId, "CompanyCashFlowStatement", null);
            CompanyNoteViewModel companyNoteViewModel = CompanyNoteResponse.Content.ReadAsAsync<CompanyNoteViewModel>().Result;
            Model.CompanyNoteViewModel = companyNoteViewModel ?? new CompanyNoteViewModel();
            ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, new Guid(ProjectId));
            HttpResponseMessage CompanyResponse = _serviceRepository.GetByIdResponse("Company", CompanyId);
            CompanyViewModel companyViewModel = CompanyResponse.Content.ReadAsAsync<CompanyViewModel>().Result;
            if (project != null)
            {
                ViewBag.ProjectId = project.Id;
                ViewBag.ProjectName = project.ProjectName;
                ViewBag.ProjectCode = project.ProjectCode;
                ViewBag.Approver = project.Approver;
                ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                ViewBag.StatusName = Model.CompanyNoteViewModel.StatusName;
            }
            ViewBag.CompanyId = Model.CompanyId;
            ViewBag.CompanyName = companyViewModel.CompanyName;

            return View(Model);
        }

        [HttpPost]
        public IActionResult Index(MainCompanyCashFlowStatementViewModel model, string actionName)
        {
            try
            {
                if (actionName.Equals("Save"))
                {

                }
                else if (actionName.Equals("Submitted"))
                {

                }
                else if (actionName.Equals("Approve"))
                {

                }
                else if (actionName.Equals("Rejected"))
                {

                }
                else if (actionName.Equals("Next"))
                {
                    return RedirectToAction("Index", "CompanySegmentInformation", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                }
                else if (actionName.Equals("Previous"))
                {
                    return RedirectToAction("Index", "CompanyBalanceSheet", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View();
        }

        private void SetRoleList()
        {
            try
            {
                var roles = HttpContext.Session.GetString("userRoles");
                var userRoles = JsonConvert.DeserializeObject<List<RoleViewModel>>(roles);
                List<string> RoleListItems = new List<string>();
                foreach (var item in userRoles)
                    RoleListItems.Add(item.UserRoleName);
                ViewBag.UserRoleList = RoleListItems;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }
    }
}