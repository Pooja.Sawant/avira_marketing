﻿CREATE TABLE [dbo].[PrimaryPurpose] (
    [Id]                 UNIQUEIDENTIFIER NOT NULL,
    [PrimaryPurposeName] NVARCHAR (256)   CONSTRAINT [DF_Table_1_FunctionalAreaName] DEFAULT ('') NOT NULL,
    [Description]        NVARCHAR (256)   NULL,
    [CreatedOn]          DATETIME2 (7)    NULL,
    [UserCreatedById]    UNIQUEIDENTIFIER NULL,
    [ModifiedOn]         DATETIME2 (7)    NULL,
    [UserModifiedById]   UNIQUEIDENTIFIER NULL,
    [IsDeleted]          BIT              CONSTRAINT [DF_PrimaryPurpose_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]          DATETIME2 (7)    NULL,
    [UserDeletedById]    UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_PrimaryPurpose] PRIMARY KEY CLUSTERED ([Id] ASC)
);

