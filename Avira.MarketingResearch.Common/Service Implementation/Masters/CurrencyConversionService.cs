﻿using Avira.MarketingResearch.Common.IService.Masters;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces.Masters;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;

namespace Avira.MarketingResearch.Common.Service_Implementation.Masters
{
    public class CurrencyConversionService : ICurrencyConversionService
    {
        private readonly ICurrencyConversionRepository _currencyConversionRepository;

        public CurrencyConversionService(ICurrencyConversionRepository currencyConversionRepository)
        {
            _currencyConversionRepository = currencyConversionRepository;
        }

        public Result<SpTransactionMessage> Create(CurrencyConversionInsertDbViewModel tfc, AviraUser currentUser)
        {
            return _currencyConversionRepository.Create(tfc);
        }

        public IEnumerable<CurrencyConversionInsertDbViewModel> GetById(Guid guid)
        {
            return _currencyConversionRepository.GetById(guid);
        }

        public IEnumerable<CurrencyConversionInsertDbViewModel> GetByIdAll(CurrencyConversionInsertDbViewModel tfc)
        {
            return _currencyConversionRepository.GetByIdAll(tfc);
        }
    }
}
