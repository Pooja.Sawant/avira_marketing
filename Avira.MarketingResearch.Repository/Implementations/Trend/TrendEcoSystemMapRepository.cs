﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.Trend
{
    public class TrendEcoSystemMapRepository : ITrendEcoSystemMapRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "TrendEcoSystemMapping";

        public TrendEcoSystemMapRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public IEnumerable<TrendEcoSystemMapViewModel>GetAll(Guid? Id,int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<TrendEcoSystemMapViewModel>(TrendEcoSystemMapQueries.StoreProcSelectAll
                            , new {@TrendID = Id, @includeDeleted = false, @OrdbyByColumnName = @OrdbyByColumnName, @PageStart = PageStart, @pageSize = pageSize, @SortDirection = SortDirection,@EcoSystemName=Search }
                            , null, true, null, CommandType.StoredProcedure);
        }

        public Result<SpTransactionMessage> InsertUpdate(MainTrendEcoSystemMapInsertUpdateDbViewModel trendEcoSystemMapInsertDbViewModel)
        {

            DataTable trendEcoSystemMapTable = new DataTable();
            trendEcoSystemMapTable.Columns.Add("ID", typeof(Guid));
            trendEcoSystemMapTable.Columns.Add("MapId", typeof(Guid));
            trendEcoSystemMapTable.Columns.Add("bitValue", typeof(bool));
            if (trendEcoSystemMapInsertDbViewModel.TrendEcoSystemMapInsertUpdate != null & trendEcoSystemMapInsertDbViewModel.TrendEcoSystemMapInsertUpdate.Count > 0)
            {
                foreach (var item in trendEcoSystemMapInsertDbViewModel.TrendEcoSystemMapInsertUpdate)//.Where(x => x.ImpactId != Guid.Empty)
                {
                    trendEcoSystemMapTable.Rows.Add(new Object[] { item.EcoSystemId, item.ImpactId, item.EndUser });
                }
            }


            var spTransactionMessage = _dbHelper.ExecuteQuery(TrendEcoSystemMapQueries.StoreProcInsertUpdate,
               new
               {
                   @TrendId = trendEcoSystemMapInsertDbViewModel.TrendId,
                   @TrendEcoSytemMap = trendEcoSystemMapTable,
                   @AviraUserId = trendEcoSystemMapInsertDbViewModel.AviraUserId
               }
                    , CommandType.StoredProcedure);
            return spTransactionMessage;
        }
    }
}
