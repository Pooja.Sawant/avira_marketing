﻿using Avira.MarketingResearch.Model.ViewModels.Masters;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.Masters
{
    public interface IStatusRepository
    {
        IEnumerable<StatusViewModel> GetAll(string statusType);
    }
}
