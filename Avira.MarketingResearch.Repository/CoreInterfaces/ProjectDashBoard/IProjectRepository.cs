﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.ProjectDashBoard
{
    public interface IProjectRepository
    {
        //DisplayProjectDBViewModel GetById(Guid guid, Guid AviraUserId);
        IEnumerable<ProjectViewModel> GetAll(Guid? guid, Guid AviraUserId, int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted);
        Result<SpTransactionMessage> Create(ProjectInsertDbViewModel viewModel);
        Result<SpTransactionMessage> Update(ProjectUpdateDbViewModel viewModel);
        IEnumerable<IndustryViewModel> GetIndustrySegmentList(string id, bool includeParentOnly);
        IEnumerable<MarketViewModel> GetMarketSegmentList(string id, bool includeParentOnly);
        IEnumerable<ProjectViewModel> GetAllProject();
    }
}
