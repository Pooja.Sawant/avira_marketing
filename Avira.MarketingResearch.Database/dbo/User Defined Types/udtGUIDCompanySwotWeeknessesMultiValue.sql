﻿CREATE TYPE [dbo].[udtGUIDCompanySwotWeeknessesMultiValue] AS TABLE (
    [Id]                 UNIQUEIDENTIFIER NOT NULL,
    [CompanyId]          UNIQUEIDENTIFIER NULL,
    [WeaknessesCategory] UNIQUEIDENTIFIER NULL,
    [Description]        NVARCHAR (512)   NULL);

