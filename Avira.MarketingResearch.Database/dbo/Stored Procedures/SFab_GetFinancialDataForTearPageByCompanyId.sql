﻿

  
CREATE PROCEDURE [dbo].[SFab_GetFinancialDataForTearPageByCompanyId]
(
 @CompanyID uniqueidentifier=null
)  
AS  
/*================================================================================  
Procedure Name: [SFab_GetFinancialDataForTearPageByCompanyId]  
Author: Harshal 
Create date: 26/08/2019  
Description: Get Financial Data by CompanyId  
Change History  
Date        Developer     Reason  
__________  ____________  ______________________________________________________  
26/08/2019  Harshal       Initial Version  
================================================================================*/  

BEGIN  
	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	--DECLARE @USDCurrencyId UNIQUEIDENTIFIER	
	--SELECT @USDCurrencyId = Id 
	--FROM Currency
	--WHERE CurrencyName = 'United States dollar';
	--declare @Today datetime = getdate();
	
	declare @UnitsId uniqueidentifier	
	select @UnitsId = Id from ValueConversion
	where ValueName = 'Millions';
	DECLARE @BaseYear INT = 0;
	 select @BaseYear = BaseYear from BaseYearMaster

	SELECT Top 1 
	--[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitDeConversion](@UnitsId,Revenue), @USDCurrencyId, PL.CurrencyId,@Today) As Revenue,
	--[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitDeConversion](@UnitsId,GrossProfit), @USDCurrencyId, PL.CurrencyId,@Today) As GrossProfit,
	--[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitDeConversion](@UnitsId,EBITDA), @USDCurrencyId, PL.CurrencyId,@Today) As EBITDA,
	--[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitDeConversion](@UnitsId,NetIncome), @USDCurrencyId, PL.CurrencyId,@Today) As NetIncome,
	--[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitDeConversion](@UnitsId,DilutedEPS), @USDCurrencyId, PL.CurrencyId,@Today) As DilutedEPS,
	--[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitDeConversion](@UnitsId,TotalCurrentAssets), @USDCurrencyId, BS.CurrencyId,@Today) As TotalCurrentAssets,
	--[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitDeConversion](@UnitsId,TotalCurrentLiabilities), @USDCurrencyId, BS.CurrencyId,@Today) As TotalCurrentLiabilities,
	--[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitDeConversion](@UnitsId,LongTermDebt), @USDCurrencyId, BS.CurrencyId,@Today) As LongTermDebt 

	[dbo].[fun_UnitDeConversion](@UnitsId,Revenue) As Revenue,
	[dbo].[fun_UnitDeConversion](@UnitsId,GrossProfit) As GrossProfit,
	[dbo].[fun_UnitDeConversion](@UnitsId,EBITDA) As EBITDA,
	[dbo].[fun_UnitDeConversion](@UnitsId,NetIncome) As NetIncome,
	[dbo].[fun_UnitDeConversion](@UnitsId,DilutedEPS) As DilutedEPS,
	[dbo].[fun_UnitDeConversion](@UnitsId,TotalCurrentAssets) As TotalCurrentAssets,
	[dbo].[fun_UnitDeConversion](@UnitsId,TotalCurrentLiabilities) As TotalCurrentLiabilities,
	[dbo].[fun_UnitDeConversion](@UnitsId,LongTermDebt) As LongTermDebt

	FROM CompanyPLStatement PL
		LEFT Join CompanyBalanceSheet BS 
		ON BS.CompanyId=PL.CompanyId AND PL.Year = BS.Year
	Where PL.CompanyId =@CompanyID AND PL.Year=@BaseYear
	Order By PL.Year desc

END