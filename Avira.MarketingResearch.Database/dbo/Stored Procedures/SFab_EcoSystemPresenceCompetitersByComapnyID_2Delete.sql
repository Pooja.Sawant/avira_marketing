﻿
CREATE procedure [dbo].[SFab_EcoSystemPresenceCompetitersByComapnyID_2Delete] 
(@CompanyID uniqueidentifier,
@includeDeleted bit = 0)
as
/*================================================================================
Procedure Name: SFab_EcoSystemPresenceCompetitersByComapnyID
Author: Jagan
Create date: 26/06/2019
Description: Get list from CompanyEcosystemPresenceKeyCompetitersMap table by CompanyID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
26/06/2019	JAGAN			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT [Id],
	   [KeyCompetiters],
	   [CompanyId]
FROM [dbo].[CompanyEcosystemPresenceKeyCompetitersMap]
where (CompanyId = @CompanyID)
and ( IsDeleted = 0)

end