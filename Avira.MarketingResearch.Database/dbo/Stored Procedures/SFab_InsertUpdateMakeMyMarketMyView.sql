﻿
CREATE procedure [dbo].[SFab_InsertUpdateMakeMyMarketMyView]      
(
	@CustomerId uniqueidentifier,
	@ProjectId uniqueidentifier,
	@MarketId uniqueidentifier,
	@SegmentId uniqueidentifier,
	@YearValuesList dbo.udtIntDecimal readonly,
	@CAGR decimal(18,4)
)      
AS      
/*================================================================================      
Procedure Name: [SFab_InsertUpdateMakeMyMarketMyView]      
Author: Sai Krishna      
Create date: 18/07/2019      
Description: Insert/Update a record into MakeMyMarketMyView table      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
18/07/2019 Sai Krishna   Initial Version    
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)  
BEGIN TRY         
IF Exists(SELECT 1 FROM MakeMyMarketMyView WHERE CustomerId = @CustomerId
 AND ProjectId = @ProjectId and MarketId = @MarketId and SegmentId = @SegmentId)
BEGIN
UPDATE MakeMyMarketMyView
SET --Year			=	@YearValuesList.IntValue,
	MyViewValue     =   list.DecimalValue,
	CAGR			=	@CAGR,
	ModifiedOn		=   GETDATE(),
	ModifiedBy      =   @CustomerId
FROM MakeMyMarketMyView mv
inner join @YearValuesList list
on mv.Year = list.IntValue
WHERE mv.CustomerId = @CustomerId
AND mv.ProjectId = @ProjectId
and MarketId = @MarketId 
and SegmentId = @SegmentId

SELECT StoredProcedureName ='SFab_InsertUpdateMakeMyMarketMyView',Message =@ErrorMsg,Success = 1;  
END
ELSE
BEGIN
INSERT INTO MakeMyMarketMyView
(
	Id,
	ProjectId,
	CustomerId,
	MarketId,
	SegmentId,
	Year,
	MyViewValue,
	CAGR,
	CreatedOn,
	CreatedBy,
	IsDeleted
) 
SELECT NEWID(),
	   @ProjectId,	
	   @CustomerId,
	   @MarketId,
	   @SegmentId,
	   list.IntValue,
	   list.DecimalValue,
	   @CAGR,
	   GETDATE(),
	   @CustomerId,
	   0
FROM @YearValuesList list;
	   	  
SELECT StoredProcedureName ='SFab_InsertUpdateMakeMyMarketMyView',Message =@ErrorMsg,Success = 1;       
END    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);      
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());      
    
 set @ErrorMsg = 'Error while insert a new TrendProjectMap, please contact Admin for more details.';    
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
	       
END CATCH            
END