﻿CREATE TABLE [dbo].[MMM_VariableSelection] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [UserId]           UNIQUEIDENTIFIER NOT NULL,
    [ProjectID]        UNIQUEIDENTIFIER NOT NULL,
    [MarketID]         UNIQUEIDENTIFIER NOT NULL,
    [SelectionId]      UNIQUEIDENTIFIER NOT NULL,
    [SelectionType]    VARCHAR (50)     NULL,
    [CreatedOn]        DATETIME2 (7)    NOT NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NOT NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_MMM_VariableSelection] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [IX_MMM_VariableSelection] UNIQUE NONCLUSTERED ([UserId] ASC, [ProjectID] ASC, [MarketID] ASC, [SelectionId] ASC, [SelectionType] ASC)
);

