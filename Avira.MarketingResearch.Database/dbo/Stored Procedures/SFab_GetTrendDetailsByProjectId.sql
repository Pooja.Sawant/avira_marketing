﻿
CREATE PROCEDURE [dbo].[SFab_GetTrendDetailsByProjectId]  
(
@ProjectId uniqueidentifier,
@TrendImportanceType nvarchar(256),
@AviraUserID uniqueidentifier=null
)
AS
/*================================================================================
Procedure Name:[SFab_GetTrendDetailsByProjectId]
Author: Harshal
Create date: 06/05/2020
Description: Get All Trend Details of Project with its importance
Change History
Date		Developer		Reason
================================================================================
06-05-20    Harshal       Initial
20-05-20    Pooja Sawant  Modification acording to requirement
---------------------------------------------------------------------------------*/
BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
declare @TrendApprovedStatusId uniqueidentifier;
declare @ImportanceIdList [dbo].[udtUniqueIdentifier];
declare @SegmentId uniqueidentifier;

select @TrendApprovedStatusId = Id
from TrendStatus
where TrendStatusName = 'Approved';

insert into @ImportanceIdList
	Select Id
	From Importance
	Where (GroupName = @TrendImportanceType)

select @SegmentId = Id from Segment
where SegmentName = 'End User';

	Select t.Id,
		   t.TrendName,
		   t.TrendValue as Value,
		   i.ImpactDirectionName as Nature,
		   ttg.TimeTagName as Outlook,
		   t.ImportanceId,
		   imp.GroupName as ImportanceName,
		   Impt.ImpactTypeName As Impact,
		   t.CreatedOn as UpdatedDate,
		   @ProjectID as ProjectId,
		   t.ImpactDirectionId as NatureId,
		   (Select STRING_AGG(C.CountryName, ', ') from TrendCountryMap TCM
            INNER JOIN Country C ON TCM.CountryId = C.Id
			Where TCM.TrendId=t.Id AND TCM.IsDeleted = 0 AND C.IsDeleted = 0 ) as CountriesList,
		   (Select STRING_AGG(C.CompanyName, ', ') from TrendKeyCompanyMap TKCM
            INNER JOIN Company C ON TKCM.CompanyId = C.Id
			Where TKCM.TrendId=t.Id AND TKCM.IsDeleted = 0 AND C.IsDeleted = 0 ) as KeyCompanyList,
			(Select STRING_AGG(S.SegmentName, ', ') from TrendEcoSystemMap TESM
            INNER JOIN Segment S ON TESM.EcoSystemId = S.Id
			Where TESM.TrendId=t.Id AND TESM.IsDeleted = 0 AND S.IsDeleted = 0 ) as EcosystemList,
			(Select STRING_AGG(SS.SubSegmentName, ', ') from TrendIndustryMap TIM
            INNER JOIN SubSegment SS ON TIM.IndustryId = SS.Id AND SegmentId=@SegmentId
			Where TIM.TrendId=t.Id AND TIM.IsDeleted = 0 AND SS.IsDeleted = 0 ) as Industries
From Trend t
INNER JOIN TrendProjectMap tpm ON tpm.TrendId = t.Id  
INNER JOIN ImpactType Impt ON tpm.Impact = Impt.Id 
INNER JOIN ImpactDirection i ON t.ImpactDirectionId=i.Id
INNER JOIN Importance imp ON t.ImportanceId=imp.Id
INNER JOIN TimeTag ttg ON ttg.Id=(SELECT TOP 1 TimeTagId FROM TrendTimeTag Where TrendTimeTag.TrendId=t.Id Order by Seq desc)
where t.IsDeleted = 0 
AND (t.ProjectID=@ProjectID AND t.TrendStatusID=@TrendApprovedStatusId )--'29C5FF4B-D602-4943-865E-45581C1BCCAF'
AND t.ImportanceId in (SELECT Id From @ImportanceIdList )
END