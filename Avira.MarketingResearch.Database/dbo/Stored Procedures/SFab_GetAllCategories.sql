﻿
CREATE procedure [dbo].[SFab_GetAllCategories]
@UserId uniqueidentifier= null
as
/*================================================================================
Procedure Name: [SFab_GetAllCategories]
Author: Harshal
Create date: 07/08/2019
Description: Get list from Category
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
07/08/2019	Harshal  	Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT [Id]
      ,[CategoryName]
      ,[Description]       
 FROM [dbo].[Category]
where ([Category].IsDeleted = 0)    
order By [CategoryName]
end