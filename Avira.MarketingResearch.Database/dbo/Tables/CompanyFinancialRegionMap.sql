﻿CREATE TABLE [dbo].[CompanyFinancialRegionMap] (
    [Id]                 UNIQUEIDENTIFIER NOT NULL,
    [CompanyId]          UNIQUEIDENTIFIER NOT NULL,
    [Year]               INT              NOT NULL,
    [CountryRegionId]    UNIQUEIDENTIFIER NOT NULL,
    [CountryRegionValue] DECIMAL (19, 4)  NULL,
    [CreatedOn]          DATETIME2 (7)    NULL,
    [UserCreatedById]    UNIQUEIDENTIFIER NULL,
    [ModifiedOn]         DATETIME2 (7)    NULL,
    [UserModifiedById]   UNIQUEIDENTIFIER NULL,
    [IsDeleted]          BIT              NOT NULL,
    [DeletedOn]          DATETIME2 (7)    NULL,
    [UserDeletedById]    UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CompanyFinancialRegionMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CompanyFinancialRegionMap_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyFinancialRegionMap_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyFinancialRegionMap_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([Id])
);

