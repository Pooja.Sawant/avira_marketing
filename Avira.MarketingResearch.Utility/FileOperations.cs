﻿
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;

namespace Avira.MarketingResearch.Utility
{
    public static class FileOperations
    {
        public static void Move(string fileName, string sourcePath, string targetPath)
        {            
            string sourceFile = Path.Combine(sourcePath, fileName);
            string destFile = Path.Combine(targetPath, fileName);

            if (File.Exists(sourceFile) && Directory.Exists(targetPath.Replace(@"\\", @"\")))
            {
                File.Move(sourceFile, destFile);
            }
        }

        public static void DeleteFile(string filePath, string fileName)
        {
            try
            {
                // Check if file exists with its full path    
                if (File.Exists(Path.Combine(filePath, fileName)))
                {
                    // If file found, delete it    
                    File.Delete(Path.Combine(filePath, fileName));
                }
            }
            catch (IOException ioExp)
            {
                // Console.WriteLine(ioExp.Message);
            }
        }

        public static string GetImageBase64Data(string imgPath)
        {
            var imgDataURL = string.Empty;
            try
            {
                // Convert image to byte array  
                if (System.IO.File.Exists(imgPath))
                {

                    using (Image image = Image.FromFile(imgPath))
                    {
                        using (MemoryStream m = new MemoryStream())
                        {
                            image.Save(m, image.RawFormat);
                            byte[] imageBytes = m.ToArray();
                            var imageFormatID = image.RawFormat.Guid;
                            var imageType = GetImageMimeType(imageFormatID);
                            // Convert byte[] to Base64 String
                            string base64String = Convert.ToBase64String(imageBytes);
                            imgDataURL = string.Format("data:" + imageType + ";base64,{0}", base64String);
                            // imgDataURL = base64String;
                        }
                    }
                    //Passing image data in viewbag to view
                }
            }
            catch (Exception ex)
            {
                imgDataURL = string.Empty;
            }
            return imgDataURL;
        }
        public static string GetImageMimeType(Guid id)
        {
            String mimeType = "image/unknown";

            try
            {
                if (id == ImageFormat.Png.Guid)
                {
                    mimeType = "image/png";
                }
                else if (id == ImageFormat.Bmp.Guid)
                {
                    mimeType = "image/bmp";
                }
                else if (id == ImageFormat.Emf.Guid)
                {
                    mimeType = "image/x-emf";
                }
                else if (id == ImageFormat.Exif.Guid)
                {
                    mimeType = "image/jpeg";
                }
                else if (id == ImageFormat.Gif.Guid)
                {
                    mimeType = "image/gif";
                }
                else if (id == ImageFormat.Icon.Guid)
                {
                    mimeType = "image/ico";
                }
                else if (id == ImageFormat.Jpeg.Guid)
                {
                    mimeType = "image/jpeg";
                }
                else if (id == ImageFormat.MemoryBmp.Guid)
                {
                    mimeType = "image/bmp";
                }
                else if (id == ImageFormat.Tiff.Guid)
                {
                    mimeType = "image/tiff";
                }
                else if (id == ImageFormat.Wmf.Guid)
                {
                    mimeType = "image/wmf";
                }
            }
            catch
            {
            }

            return mimeType;
        }
    }
}
