﻿
  
CREATE PROCEDURE [dbo].[SFab_GetAllTimeTag]  
(@AviraUserID uniqueidentifier = null)  
 AS  
/*================================================================================  
Procedure Name: [SFab_GetAllTimeTag]  
Author: Pooja Sawant  
Create date: 05/25/2019  
Description: Get list of TimeTag from table  
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
05/25/2019 Pooja   Initial Version  
================================================================================*/  
  
BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
	
	select * from TimeTag WHERE IsDeleted = 0 
	and TimeTag.TimeTagName in('ShortTerm','LongTerm','MidTerm')
	order by seq asc

END