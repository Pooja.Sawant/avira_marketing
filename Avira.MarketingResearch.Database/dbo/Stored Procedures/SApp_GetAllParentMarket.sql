﻿

CREATE PROCEDURE [dbo].[SApp_GetAllParentMarket]
	(
		@includeDeleted bit = 0
	)
	AS
/*================================================================================
Procedure Name: [SApp_GetAllParentMarket]
Author: Harshal
Create date: 03/19/2019
Description: Get list from Market table by ParentId
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
03/19/2019	Adarsh			Initial Version
================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT [Market].[Id],
		[Market].[MarketName],
		[Market].[Description],
		[Market].[ParentId],
		[Market].[IsDeleted]
		
FROM [dbo].[Market]
where ([Market].ParentId is NULL )
	AND ([Market].IsDeleted = 0)
	
	END