﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.Models;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;

namespace Avira.MarketingResearch.Web.Controllers
{
    [NoCache]
    [Authorization]
    public class CurrencyConversionController : BaseController
    {
        private ILoggerManager _logger;
        public readonly IServiceRepository _serviceRepository;
        public CurrencyConversionController(IServiceRepository serviceRepository, ILoggerManager logger)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Index(string id, string msg)
        {
            try
            {
                ViewBag.CId = id;
                ViewBag.Message = msg;
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.GetByIdResponse(contollerName, id);
                response.EnsureSuccessStatusCode();
                IEnumerable<CurrencyConversionInsertDbViewModel> currencyConversion = response.Content.ReadAsAsync<IEnumerable<CurrencyConversionInsertDbViewModel>>().Result;

                var currency = HttpContext.Session.GetString("currencyData");
                var currencyList = JsonConvert.DeserializeObject<List<CurrencyViewModel>>(currency);

                List<SelectListItem> listItems = new List<SelectListItem>();
                foreach (var item in currencyList)
                {
                    listItems.Add(new SelectListItem
                    {
                        Text = item.CurrencyName + "(" + item.CurrencyCode + ")",
                        Value = item.Id.ToString(),
                        Selected = (item.Id.ToString() == id)
                    });
                }

                ViewBag.Currency = listItems;

                List<SelectListItem> listItems1 = new List<SelectListItem>();

                listItems1.Add(new SelectListItem
                {
                    Text = "Time Range",
                    Value = "1"
                });
                listItems1.Add(new SelectListItem
                {
                    Text = "Yearly",
                    Value = "2"
                });
                listItems1.Add(new SelectListItem
                {
                    Text = "Quarterly",
                    Value = "3"
                });

                ViewBag.ConversionRangeType = listItems1;

                return View();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View();
        }

        public ActionResult LoadData(CurrencyConversionInsertDbViewModel currency)
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            int totalRecords = 0;
            List<CurrencyConversionInsertDbViewModel> data = null;
            try
            {
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                string search = Request.Form["search[value]"];

                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDir = Request.Form["order[0][dir]"].FirstOrDefault();
                var SortDirection = -1;
                if (sortColumnDir == "asc")
                {
                    SortDirection = 1;
                }
                else
                {
                    SortDirection = -1;
                }

                int skip = start != null ? Convert.ToInt32(start) : 0;
                int pageSize = length.ToString() != null ? Convert.ToInt32(length) : 0;
                int PageStart = start.ToString() != null ? Convert.ToInt32(start) : 0;
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();

                HttpResponseMessage response = _serviceRepository.GetByAllResponse(contollerName, currency);
                response.EnsureSuccessStatusCode();
                List<CurrencyConversionInsertDbViewModel> currencies = response.Content.ReadAsAsync<List<CurrencyConversionInsertDbViewModel>>().Result;
                ViewBag.Title = "All Currency";
                data = currencies;               
                foreach (var item in currencies)
                {
                    totalRecords = item.TotalItems;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data });

        }

        [HttpPost]
        public IActionResult Index(CurrencyConversionInsertDbViewModel currencyConversionInsertDbViewModel)
        {
            if (currencyConversionInsertDbViewModel.ConversionRangeType == 1)
            {
                if (currencyConversionInsertDbViewModel.StartDate == null && currencyConversionInsertDbViewModel.EndDate == null && currencyConversionInsertDbViewModel.ConversionRate.ToString() == "0")
                {
                    ViewBag.Message = "Please enter the start date, end date and conversion rate.";
                    return RedirectToAction("Index", new { msg = ViewBag.Message });
                }
                else
                {
                    currencyConversionInsertDbViewModel.Year = 0;
                    currencyConversionInsertDbViewModel.Quarter = null;
                }
            }
            else if (currencyConversionInsertDbViewModel.ConversionRangeType == 2)
            {
                if (currencyConversionInsertDbViewModel.Year == null && currencyConversionInsertDbViewModel.ConversionRate.ToString() == "0")
                {
                    ViewBag.Message = "Please enter the Year and conversion rate.";
                    return RedirectToAction("Index", new { msg = ViewBag.Message });
                }
                else
                {
                    currencyConversionInsertDbViewModel.StartDate = null;
                    currencyConversionInsertDbViewModel.EndDate = null;
                    currencyConversionInsertDbViewModel.Quarter = null;
                }
            }
            else if (currencyConversionInsertDbViewModel.ConversionRangeType == 3)
            {
                if (currencyConversionInsertDbViewModel.Quarter == null && currencyConversionInsertDbViewModel.ConversionRate.ToString() == "0")
                {
                    ViewBag.Message = "Please enter the Quarter and conversion rate.";
                    return RedirectToAction("Index", new { msg = ViewBag.Message });
                }
                else
                {
                    currencyConversionInsertDbViewModel.Year = 0;
                    currencyConversionInsertDbViewModel.StartDate = null;
                    currencyConversionInsertDbViewModel.EndDate = null;
                }
            }
            else
            {
                ViewBag.Message = "Please select Conversion Range Type.";
                return RedirectToAction("Index", new { msg = ViewBag.Message });
            }
            var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, currencyConversionInsertDbViewModel);
            response.EnsureSuccessStatusCode();
            ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
            if (viewModel != null)
            {
                ViewBag.Message = viewModel.Message;
            }
            return RedirectToAction("Index", new { msg = ViewBag.Message });
        }
    }
}