﻿
CREATE PROCEDURE [dbo].[SStaging_InsertImportCompanyProfiles_Multitab]      
@TemplateName NVARCHAR(200)='',      
@ImportFileName NVARCHAR(200)='',      
@JsonFundamentals	NVARCHAR(MAX) ,
@JsonShareHolding	NVARCHAR(MAX) ,
@JsonSubsidiaries	NVARCHAR(MAX) ,
@JsonKeyEmployees	NVARCHAR(MAX) ,
@JsonBoardOfDirectors	NVARCHAR(MAX) ,
@JsonProduct	NVARCHAR(MAX) ,
--@JsonClientsAndStrategy	NVARCHAR(MAX) ,--old changed by praveen
@JsonClients	NVARCHAR(MAX) ,--added by praveen
@JsonStrategy	NVARCHAR(MAX) ,--added by praveen
@JsonNews	NVARCHAR(MAX) ,
@JsonSWOT	NVARCHAR(MAX) ,
@JsonEcosystemPresence	NVARCHAR(MAX) ,
@JsonTrends	NVARCHAR(MAX) ,
@JsonAnalystView	NVARCHAR(MAX),
@JsonPriceVolume	NVARCHAR(MAX),--- added by Harshal
@JsonESMSegment NVarChar(Max),
@UserCreatedById UNIQUEIDENTIFIER       
AS      
/*================================================================================  
Procedure Name: SStaging_InsertImportCompanyProfiles_Multitab 
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
08/25/2019 Sharath   Initial Version  
================================================================================*/       
DECLARE @ImportId UNIQUEIDENTIFIER = NEWID()      
DECLARE @ErrorMsg NVARCHAR(2048), @CreatedOn Datetime

BEGIN      
 SET @CreatedOn = GETDATE()      
 BEGIN TRY 

	--------------Clear old data if exists-------------------------
	DELETE FROM ImportData where Id in (select ImportId from StagingCompanyProfileFundamentals sm);    
	DELETE FROM StagingCompanyProfileFundamentals;
	DELETE FROM StagingCompanyProfileShareHolding;
	DELETE FROM StagingCompanyProfileSubsidiaries;
	DELETE FROM StagingCompanyKeyEmployees;
	DELETE FROM StagingCompanyBoardOfDirectors;
	DELETE FROM StagingCompanyProduct;
	DELETE FROM StagingCompanyClients;
	DELETE FROM StagingCompanyStrategy;
	DELETE FROM StagingCompanyNews;
	DELETE FROM StagingCompanySWOT;
	DELETE FROM StagingCompanyEcosystemPresence;
	DELETE FROM StagingCompanyTrends;
	DELETE FROM StagingCompanyAnalystView;
	DELETE FROM StagingCompanyPriceVolume;
	Delete From [StagingCompanyEcosystemSegmentMap]
	--------------Clear old data if exists-------------------------

 INSERT INTO ImportData(Id, TemplateName, ImportFileName, ImportedOn, UserImportedById)      
VALUES(@ImportId, @TemplateName, @ImportFileName,@CreatedOn, @UserCreatedById)  
---------------------------------------------------      

INSERT INTO [dbo].[StagingCompanyProfileFundamentals]
   ([Id]   
   ,[ImportId]
   ,[RowNo]
   ,[CompanyName]
   ,[Description]
   ,[Headquarter]
   ,[Foundedyear]
   ,[Nature]
   ,[CompanyStage]
   ,[NumberOfEmployee]
   ,[AsonDate]
   ,[Website]
   ,[CompanyRank_Rank]
   ,[CompanyRank_Rationale]
   ,[Currency1]
   ,[PhoneNumber1]
   ,[PhoneNumber2]
   ,[PhoneNumber3]
   ,[GeographicPresence])
SELECT	NEWID()
		,@ImportId
		,[RowNo]
		,[CompanyName]
		,[Description]
	   ,[Headquarter]
	   ,[Foundedyear]
	   ,[Nature]
	   ,[CompanyStage]
	   ,Case When IsNull([NumberOfEmployee], '') = ''Then 0 Else CONVERT(bigint,[NumberOfEmployee]) End
	   ,Case When IsNull([AsonDate], '') = '' OR [AsonDate] = '0001-01-01T00:00:00' Then Null Else convert(datetime, [AsonDate], 126) End
	   ,[Website]
	   ,[CompanyRank_Rank]
	   ,[CompanyRank_Rationale]
	   ,[Currency1]
	   ,[PhoneNumber1]
	   ,[PhoneNumber2]
	   ,[PhoneNumber3]
	   ,[GeographicPresence]    
FROM OPENJSON(@JsonFundamentals)      
WITH (
	RowNo Int,
	CompanyName	nvarchar(255),
	Description	nvarchar(512),
	Headquarter	nvarchar(255),
	Foundedyear	int,
	Nature	nvarchar(50),
	CompanyStage	nvarchar(50),
	NumberOfEmployee	bigint,
	AsonDate	nvarchar(20),
	Website	nvarchar(255),
	CompanyRank_Rank	tinyint,
	CompanyRank_Rationale	nvarchar(512),
	Currency1	nvarchar(3),
	PhoneNumber1	nvarchar(50),
	PhoneNumber2	nvarchar(50),
	PhoneNumber3	nvarchar(50),
	GeographicPresence	nvarchar(512)) AS jsonFundamentals;
       
INSERT INTO [dbo].[StagingCompanyProfileShareHolding]
   ([Id]
   ,[ImportId]
   ,[RowNo]
   ,[Name]
   ,[Entity]
   ,[Holding])
SELECT NEWID(),
		@ImportId,
		[RowNo],
		Name,
		Entity,
		Holding
FROM OPENJSON(@JsonShareHolding)      
     with(Name nvarchar(255),
   [RowNo] int, Entity nvarchar(255),
   Holding decimal(9,4)) as JsonShareHolding;

INSERT INTO [dbo].[StagingCompanyProfileSubsidiaries]
           ([Id]
           ,[ImportId]
		   ,[RowNo]
           ,[CompanySubsdiaryName]
           ,[CompanySubsdiaryLocation]
           )
SELECT NEWID(),
		@ImportId
		,[RowNo]
		,[CompanySubsdiaryName]
        ,[CompanySubsdiaryLocation]
FROM OPENJSON(@JsonSubsidiaries)      
     with([RowNo] Int, CompanySubsdiaryName nvarchar(255),
   CompanySubsdiaryLocation nvarchar(255)) as JsonSubsidiaries;

INSERT INTO [dbo].[StagingCompanyKeyEmployees]
           ([Id]
           ,[ImportId]
		   ,[RowNo]
           ,[KeyEmployeeName]
           ,[KeyEmployeeDesignation]
           ,[KeyEmployeeEmail]
           ,[ReportingManager])
SELECT NEWID(),
		@ImportId
		,[RowNo]
		,[KeyEmployeeName]
           ,[KeyEmployeeDesignation]
           ,[KeyEmployeeEmail]
           ,[ReportingManager]
FROM OPENJSON(@JsonKeyEmployees)      
     with([RowNo] Int, [KeyEmployeeName] nvarchar(255),
	 [KeyEmployeeDesignation] nvarchar(255),
	 [KeyEmployeeEmail] nvarchar(255),
   [ReportingManager] nvarchar(255)) as JsonKeyEmployees;

INSERT INTO [dbo].[StagingCompanyBoardOfDirectors]
           ([Id]
           ,[ImportId]
		   ,[RowNo]
           ,[BoardofDirectorName]
           ,[BoardofDirectorEmail]
           ,[BoardofDirectorOtherCompany]
           ,[BoardofDirectorOtherCompanyDesignation])
SELECT NEWID(),
		@ImportId
		,[RowNo]
		,[BoardofDirectorName]
           ,[BoardofDirectorEmail]
           ,[BoardofDirectorOtherCompany]
           ,[BoardofDirectorOtherCompanyDesignation]
FROM OPENJSON(@JsonBoardOfDirectors)      
     with([RowNo] Int, [BoardofDirectorName] nvarchar(255),
	 [BoardofDirectorEmail] nvarchar(255),
	 [BoardofDirectorOtherCompany] nvarchar(255),
   [BoardofDirectorOtherCompanyDesignation] nvarchar(255)) as JsonBoardOfDirectors;

INSERT INTO [dbo].[StagingCompanyProduct]
           ([Id]
           ,[ImportId]
		   ,[RowNo]
		   ,[Market]
           ,[ProductServiceName]
           ,[LaunchDate]
           ,[ProductCategory]
           ,[Segment]
           ,[Revenue]
           ,[Units]
           ,[Currency]
           ,[TargetedGeogrpahy]
           ,[Patents]
           ,[DecriptionRemarks]
           ,[Status]
           )
SELECT NEWID(),
		@ImportId
		,[RowNo]
		,[Market]
           ,[ProductServiceName]
           ,Case When Isnull([LaunchDate], '') = '' OR [LaunchDate] = '0001-01-01T00:00:00' Then Null Else convert(datetime, [LaunchDate], 126) End
           ,[ProductCategory]
           ,[Segment]
           ,[Revenue]		   
           ,[Units]
           ,[Currency]
           ,[TargetedGeogrpahy]
           ,[Patents]
           ,[DecriptionRemarks]
           ,[Status]
FROM OPENJSON(@JsonProduct)      
with (     [RowNo] Int,
		   [Market] nvarchar(255),
		   [ProductServiceName] nvarchar(255),
           [LaunchDate] nvarchar(20),
           [ProductCategory] nvarchar(255),
           [Segment] nvarchar(255),
           [Revenue] decimal(19,4),
           [Units] nvarchar(255),
           [Currency] nvarchar(3),
           [TargetedGeogrpahy] nvarchar(512),
           [Patents] nvarchar(512),
           [DecriptionRemarks] nvarchar(512),
           [Status] nvarchar(255)) as JsonProduct;

INSERT INTO [dbo].[StagingCompanyClients]
           ([Id]
           ,[ImportId]
		   ,[RowNo]
           ,[ClientName]
           ,[ClientIndustry]
           ,[Remarks])
SELECT NEWID(),
		@ImportId
			,[RowNo]
           ,[ClientName]
           ,[ClientIndustry]
           ,[Remarks]
FROM OPENJSON(@JsonClients)      
with (     [RowNo] Int, [ClientName] nvarchar(255),
           [ClientIndustry] nvarchar(255),
           [Remarks] nvarchar(512)) as JsonClients; --- Modified by praveen nvarchar(255) to (512)

INSERT INTO [dbo].[StagingCompanyStrategy]
           ([Id]
           ,[ImportId]
		   ,[RowNo]
           ,[Date]
           ,[OrganicInorganic]
           ,[Strategy]
           ,[Category]
           ,[Importance]
           ,[Impact]
           ,[Remarks])
SELECT NEWID(),
		@ImportId
		   ,[RowNo]
		   ,Case When IsNull([Date], '') = ''  OR [Date] = '0001-01-01T00:00:00' Then Null Else convert(datetime, [Date], 126) End
           ,[OrganicInorganic]
           ,[Strategy]
           ,[Category]
           ,[Importance]
           ,[ImpactDirection]
           ,[Remarks]
FROM OPENJSON(@JsonStrategy)      
with ( [RowNo] Int, 
	   [Date] nvarchar(20),
	   [OrganicInorganic] nvarchar(255),
       [Strategy] nvarchar(255),
	   [Category] nvarchar(255),
       [Importance] nvarchar(4),
	   [ImpactDirection] nvarchar(20),
	   [Remarks] nvarchar(512)
	   ) as JsonStrategy;

INSERT INTO [dbo].[StagingCompanyNews]
           ([Id]
           ,[ImportId]
		   ,[RowNo]
           ,[NewsDate]
           ,[News]
           ,[NewsCategory]
           ,[Importance]
           ,[Impact]
           ,[OtherParticipants]
           ,[Remarks])
SELECT NEWID(),
		@ImportId
		,[RowNo]
		   ,Case When IsNull([NewsDate], '') = ''  OR [NewsDate] = '0001-01-01T00:00:00'  Then Null Else convert(datetime, [NewsDate], 126) End
		   [Date]
           ,[News]
           ,[NewsCategory]
           ,[Importance]
           ,[Impact]
           ,[OtherParticipants]
           ,[Remarks]
FROM OPENJSON(@JsonNews)      
with ( [RowNo] Int, [NewsDate] nvarchar(20),
		[News] nvarchar(1000),
       [NewsCategory] nvarchar(255),
	   [OtherParticipants] nvarchar(255),
       [Importance] nvarchar(255),
	   [Impact] nvarchar(255),
	   [Remarks] nvarchar(512)
	   ) as JsonNews;

INSERT INTO [dbo].[StagingCompanySWOT]
           ([Id]
           ,[ImportId]
		   ,[RowNo]
           ,[AnalysisPoint]
           ,[Market]
           ,[Category]
           ,[SWOTType])
SELECT NEWID(),
		@ImportId
		   ,[RowNo]
           ,[AnalysisPoint]
           ,[Market]
           ,[Category]
           ,[SWOTType]
FROM OPENJSON(@JsonSWOT)      
with ( [RowNo] Int, [AnalysisPoint] nvarchar(1000),
       [Market] nvarchar(255),
	   [Category] nvarchar(255),
       [SWOTType] nvarchar(255)
	   ) as JsonSWOT;

INSERT INTO [dbo].[StagingCompanyEcosystemPresence]
           ([Id]
           ,[ImportId]
		   ,[RowNo]
           ,[ESMPresence]
           ,[SectorPresence]
           ,[KeyCompetitor]
           )
SELECT NEWID(),
		@ImportId
			,[RowNo]
            ,[ESMPresence]
           ,[SectorPresence]
           ,[KeyCompetitor]
FROM OPENJSON(@JsonEcosystemPresence)      
with ( [RowNo] Int, [ESMPresence] nvarchar(255),
	   [SectorPresence] nvarchar(512),
       [KeyCompetitor] nvarchar(512)
	   ) as JsonEcosystemPresence;

Insert Into [dbo].[StagingCompanyEcosystemSegmentMap] 
	(ID, ImportId, [RowNo], SegmentName, SubsegmentName)
Select NewID(), @ImportId, [RowNo], ESMSegmentName, ESMSubSegmentName
FROM OPENJSON(@JsonESMSegment)
with ( [RowNo] Int, [ESMSegmentName] nvarchar(512),
	   [ESMSubSegmentName] nvarchar(Max)	 
	   ) as JsonESMSegment;

INSERT INTO [dbo].[StagingCompanyTrends]
           ([Id]
           ,[ImportId]
		   ,[RowNo]
           ,[Trends]
           ,[Department]
           ,[Category]
           ,[Ecosystem]
           ,[Importance]
           ,[Impact]
           ,[Market])
SELECT NEWID(),
		@ImportId
		   ,[RowNo]
           ,[Trends]
           ,[Department]
           ,[Category]
           ,[Ecosystem]
           ,[Importance]
           ,[Impact]
           ,[Market]
FROM OPENJSON(@JsonTrends)      
with ( [RowNo] Int, [Trends] nvarchar(1000),
       [Department] nvarchar(255),
	   [Category] nvarchar(255),
       [Ecosystem] nvarchar(255),
	   [Importance] nvarchar(255),
	   [Impact] nvarchar(255),
	   [Market] nvarchar(255)
	   ) as JsonTrends;

INSERT INTO [dbo].[StagingCompanyAnalystView]
           ([Id]
           ,[ImportId]
		   ,[RowNo]
           ,[Market]
           ,[AnalystView]
           ,[DateOfUpdate]
           ,[Category]
           ,[Importance])
SELECT NEWID(),
		@ImportId
		   ,[RowNo]
           ,[Market]
           ,[AnalystView]
           ,CASE WHEN ISNULL(DateOfUpdate,'') = '' OR DateOfUpdate = '0001-01-01T00:00:00' THEN NULL ELSE convert(datetime, [DateOfUpdate], 126) END
           ,[Category]
           ,[Importance]
FROM OPENJSON(@JsonAnalystView)      
with ( [RowNo] Int, [Market] nvarchar(255),
	   [AnalystView] nvarchar(1000),
	   [DateOfUpdate] nvarchar(20),
	   [Category] nvarchar(255),
	   [Importance] nvarchar(255)
	   ) as JsonAnalystView;

 INSERT INTO [dbo].[StagingCompanyPriceVolume]
           ([Id]
           ,[ImportId]
		   ,[RowNo]
           ,[Date]
           ,[Price]
           ,[Currency]
           ,[CurrencyUnit]
           ,[Volume]
		   ,[VolumeUnit])
SELECT NEWID(),   --SELECT REPLACE(CONVERT(NVARCHAR(9), '19-Nov-19', 6), ' ', '-') 
		@ImportId
		,[RowNo]
		,CASE WHEN ISNULL(Date,'') = '' OR Date = '0001-01-01T00:00:00' THEN NULL ELSE convert(datetime, [Date], 126) END
		--,CASE WHEN ISDATE([Date]) = 1 THEN CASE WHEN [Date] != (REPLACE(CONVERT(NVARCHAR(9), [Date], 6), ' ', '-')) THEN NULL ELSE convert(datetime, [Date], 126) END ELSE NULL END
        ,[Price]
        ,[Currency]
        ,[CurrencyUnit]
        ,[Volume]
		,[VolumeUnit]
FROM OPENJSON(@JsonPriceVolume)      
with ( [RowNo] Int, 
	   [Date] nvarchar(20),
       [Price] decimal(19,4),
	   [Currency] nvarchar(3),
	   [CurrencyUnit] nvarchar(255),
	   [Volume] decimal(19,4),
	   [VolumeUnit] nvarchar(255)
	   ) as JsonPriceVolume;

SELECT StoredProcedureName ='SStaging_InsertImportCompanyProfiles_Multitab',Message =@ErrorMsg,Success = 1; 
END TRY  
BEGIN CATCH  
    -- Execute the error retrieval routine.  
 DECLARE @ErrorNumber int;  
 DECLARE @ErrorSeverity int;  
 DECLARE @ErrorProcedure varchar(100);  
 DECLARE @ErrorLine int;  
 DECLARE @ErrorMessage varchar(500);  
  
  SELECT @ErrorNumber = ERROR_NUMBER(),  
@ErrorSeverity = ERROR_SEVERITY(),  
@ErrorProcedure = ERROR_PROCEDURE(),  
@ErrorLine = ERROR_LINE(),  
@ErrorMessage = ERROR_MESSAGE()  
  
 insert into dbo.Errorlog(ErrorNumber,  
       ErrorSeverity,  
       ErrorProcedure,  
       ErrorLine,  
       ErrorMessage,  
       ErrorDate)  
values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,@CreatedOn);  

 set @ErrorMsg = 'Error while importing CompanyProfiles, please contact Admin for more details.';  
--THROW 50000,@ErrorMsg,1;    
--return(1)
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine
    ,Message =@ErrorMsg,Success = 0;   
   
END CATCH   
      
      
END