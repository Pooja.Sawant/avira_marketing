﻿CREATE TABLE [dbo].[DownloadHistory] (
    [Id]            UNIQUEIDENTIFIER NOT NULL,
    [ModuleId]      UNIQUEIDENTIFIER NULL,
    [ProjectId]     UNIQUEIDENTIFIER NULL,
    [FileName]      NVARCHAR (256)   NULL,
    [UserId]        UNIQUEIDENTIFIER NULL,
    [DownloadDate]  DATETIME2 (7)    NULL,
    [StatusId]      UNIQUEIDENTIFIER NULL,
    [DownloadCount] INT              NULL,
    CONSTRAINT [PK_DownloadHistory] PRIMARY KEY CLUSTERED ([Id] ASC)
);

