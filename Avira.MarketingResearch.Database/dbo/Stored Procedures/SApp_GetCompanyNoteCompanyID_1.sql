﻿CREATE procedure [dbo].[SApp_GetCompanyNoteCompanyID]    
(@CompanyId uniqueidentifier,
@ProjectId uniqueidentifier=null,
@CompanyType  NVARCHAR(512))    
as    
/*================================================================================    
Procedure Name: SApp_GetCompanyNoteCompanyID    
Author: Praveen    
Create date: 04/15/2019    
Description: Get Company note for perticular Company type by Company id    
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
03/04/2019 Praveen   Initial Version    
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    
    --mentioned those CompanyTypes here which are only by companyId
    IF EXISTS (SELECT * FROM STRING_SPLIT('CompanyClientAndStrategy,CompanyNews,CompanyOtherInfo,CompanyShare,CompanyShareVolume,CompanyTrends,EcoSystemPresence,CompanyBalanceSheet,CompanyPLStatement,CompanyCashFlowStatement,CompanySegmentInformation',',') WHERE value = @CompanyType)
BEGIN
	SELECT TOP 1 CompanyNote.Id,    
	CompanyId,
	projectId,
	CompanyType,    
	AuthorRemark,    
	ApproverRemark,    
	CompanyStatusID,
	CompanyProfileStatus.CompanyStatusName As StatusName    
	FROM [dbo].[CompanyNote] 
	INNER JOIN [dbo].CompanyProfileStatus ON [CompanyNote].CompanyStatusID= CompanyProfileStatus.Id   
	WHERE (CompanyId = @CompanyId AND CompanyType = @CompanyType)  
	
	ORDER BY CASE WHEN CompanyNote.ModifiedOn IS NULL THEN CompanyNote.CreatedOn ELSE CompanyNote.ModifiedOn END  
END
ELSE	
BEGIN
	SELECT TOP 1 CompanyNote.Id,    
	CompanyId,
	projectId,
	CompanyType,    
	AuthorRemark,    
	ApproverRemark,    
	CompanyStatusID,
	CompanyProfileStatus.CompanyStatusName As StatusName   
	FROM [dbo].[CompanyNote] 
	INNER JOIN [dbo].CompanyProfileStatus ON [CompanyNote].CompanyStatusID= CompanyProfileStatus.Id   
	WHERE (CompanyId = @CompanyId AND CompanyType = @CompanyType) and 
	ProjectId=@ProjectId  

	ORDER BY CASE WHEN CompanyNote.ModifiedOn IS NULL THEN CompanyNote.CreatedOn ELSE CompanyNote.ModifiedOn END 
END
END