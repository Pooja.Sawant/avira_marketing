﻿using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.CompanyProfile
{
    public class RevenueProfitAnalysisService : IRevenueProfitAnalysisService
    {
        private readonly IRevenueProfitAnalysisRepository _revenueProfitAnalysisRepository;
        private readonly ICompanyNoteRepository _iCompanyNoteRepository;

        public RevenueProfitAnalysisService(IRevenueProfitAnalysisRepository revenueProfitAnalysisRepository, ICompanyNoteRepository iCompanyNoteRepository)
        {
            _revenueProfitAnalysisRepository = revenueProfitAnalysisRepository;
            _iCompanyNoteRepository = iCompanyNoteRepository;
        }
        public IEnumerable<CurrencyMapViewModel> GetAllCurrencies()
        {
            return _revenueProfitAnalysisRepository.GetAllCurrencies();
        }

        public IEnumerable<RegionMapViewModel> GetAllRegions()
        {
            return _revenueProfitAnalysisRepository.GetAllRegions();
        }

        public IEnumerable<ValueConversionViewModel> GetAllValueConversion()
        {
            return _revenueProfitAnalysisRepository.GetAllValueConversion();
        }

        public Result<SpTransactionMessage> InsertUpdate(MainRevenueProfitAnalysisInsertUpdateDbViewModel tfc, AviraUser currenUser)
        {
            var res = _revenueProfitAnalysisRepository.InsertUpdate(tfc);
            tfc.CompanyNoteModel.Id = Guid.NewGuid();
            tfc.CompanyNoteModel.UserCreatedById = tfc.AviraUserId;
            tfc.CompanyNoteModel.CompanyId = tfc.CompanyId;
            _iCompanyNoteRepository.Insert(tfc.CompanyNoteModel);

            return res;
        }
    }
}
