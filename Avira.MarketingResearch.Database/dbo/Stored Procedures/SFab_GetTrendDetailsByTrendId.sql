﻿CREATE PROCEDURE [dbo].[SFab_GetTrendDetailsByTrendId]  
(@TrendId uniqueidentifier,
@LoginUserID uniqueidentifier=null,
@ProjectID uniqueidentifier=null,
@LoginRoleID uniqueidentifier=null
)
AS
/*================================================================================
Procedure Name:[SFab_GetTrendDetailsByTrendId]
Author: Jagan
Create date: 07/03/2019
Description: Get list from Market table by Name or ID
Change History
Date		Developer		Reason

================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
declare @Amount decimal(19,4);
declare @TagCnt int;
declare @TimeTagId uniqueidentifier;
declare @KeyCompanyList nvarchar(4000);
declare @TrendApprovedStatusId uniqueidentifier;

	select @TrendApprovedStatusId = Id
	from TrendStatus
	where TrendStatusName = 'Approved';

select @KeyCompanyList = STRING_AGG(c.CompanyName, ', ')
from TrendKeyCompanyMap TCM
inner join Company C
on TCM.CompanyId = C.Id
where TCM.TrendId = @TrendId
and TCM.IsDeleted = 0
and C.IsDeleted = 0;

declare @seq tinyint
	select TOP 1 @TimeTagId = TimeTag.Id
	from TrendTimeTag
	JOIN TimeTag ON TrendTimeTag.TimeTagId = TimeTag.Id
	where TrendId = @TrendId ORDER BY Seq DESC

--set @seq = case when @TagCnt<=4 then 2
--				when @TagCnt in(5,6,7) then 3
--				when @TagCnt >7 then 4
--			else 2
--			end;


--select top 1 @TimeTagId = Id from TimeTag 
--WHERE IsDeleted = 0 
----and TimeTag.TimeTagName in('ShortTerm','LongTerm','MidTerm')
--and Seq = @seq;

		--select @TimeTagId = TT.TimeTagId
		--from TrendTimeTag TT 
		--where TT.TrendId = @TrendId

	Select t.Id,
		t.TrendName,
		t.ImportanceId,
		i.ImpactDirectionName,
		ttg.TimeTagName,
		imp.GroupName as ImportanceName,
		t.ImpactDirectionId as NatureId,
		@TimeTagId as Outlook,
		t.TrendValue as Amount,
		@KeyCompanyList as KeyCompanyList
From Trend t
-----jagan
left join 
ImpactDirection i
on t.ImpactDirectionId=i.Id
left join
Importance imp
on t.ImportanceId=imp.Id
left join 
TimeTag ttg
on ttg.Id=@TimeTagId
-----jagan
where t.IsDeleted = 0 
AND (t.Id = @TrendId and  t.TrendStatusID = @TrendApprovedStatusId AND t.ProjectID=@ProjectID )--'29C5FF4B-D602-4943-865E-45581C1BCCAF'

END