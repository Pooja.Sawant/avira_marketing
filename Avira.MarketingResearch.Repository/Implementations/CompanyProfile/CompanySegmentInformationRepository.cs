﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.CompanyProfile
{
    public class CompanySegmentInformationRepository : ICompanySegmentInformationRepository
    {
        protected IDbHelper _dbHelper;
      
        public CompanySegmentInformationRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public IEnumerable<CompanySegmentInformationViewModel> GetCompanySegmentInformationData(Guid CompanyId, AviraUser User)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<CompanySegmentInformationViewModel>(RevenueProfitAnalysisQueries.StoreProcGetCompanySegmentInformationData
                            , new { @CompanyID = CompanyId }
                            , null, true, null, CommandType.StoredProcedure);
        }
    }
}
