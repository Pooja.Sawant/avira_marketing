﻿CREATE TYPE [dbo].[udtIntDecimalValue] AS TABLE (
    [intValue]     INT          NULL,
    [decimalValue] DECIMAL (18) NULL);

