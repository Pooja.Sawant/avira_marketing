﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.Utils;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Avira.MarketingResearch.Web.Controllers
{
    [Authorize]
    [NoCache]
    public class CompanyOtherInfoController : BaseController
    {
        public readonly IServiceRepository _serviceRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;
        string contentRootPath = string.Empty;
        private ILoggerManager _logger;

        public CompanyOtherInfoController(IServiceRepository serviceRepository, ILoggerManager logger, IHostingEnvironment hostingEnvironment, IConfiguration configuration)
        {
            _hostingEnvironment = hostingEnvironment;
            _serviceRepository = serviceRepository;
            _logger = logger;
            _configuration = configuration;
            contentRootPath = _hostingEnvironment.ContentRootPath;
        } 

        public IActionResult Index(string CompanyId,string ProjectId)
        {
            SetRoleList(); 
            ViewBag.Message = string.Empty;
            var ContollerName = this.ControllerContext.RouteData.Values["controller"].ToString(); 
            MainCompanyOtherInfoInsertUpdateViewModel mainCompanyOtherInfoInsertUpdateViewModel = new MainCompanyOtherInfoInsertUpdateViewModel();
            try
            {
                mainCompanyOtherInfoInsertUpdateViewModel.CompanyId = new Guid(CompanyId);
                mainCompanyOtherInfoInsertUpdateViewModel.ProjectId = new Guid(ProjectId);

                // Get all Clients mapped to the Company
                HttpResponseMessage responseOtherInfo = _serviceRepository.GetCompanyOtherInfo(ContollerName, CompanyId);
                responseOtherInfo.EnsureSuccessStatusCode();
                List<CompanyOtherInfoInsertUpdateViewModel> companyOtherInfoInsertUpdateViewModel = responseOtherInfo.Content.ReadAsAsync<List<CompanyOtherInfoInsertUpdateViewModel>>().Result;

                //List<string> TableInfoNameList = new List<string>();
                //TableInfoNameList.Add("Add Vendor Info Table");
                //TableInfoNameList.Add("Add Litigation and other legal info");
                //TableInfoNameList.Add("Add Patent related info");
                //TableInfoNameList.Add("Add any other info");

                var TableInfoNameList = new List<KeyValuePair<int, string>>();
                TableInfoNameList.Add(new KeyValuePair<int, string>(1, "Add Vendor Info Table"));
                TableInfoNameList.Add(new KeyValuePair<int, string>(2, "Add Litigation and other legal info"));
                TableInfoNameList.Add(new KeyValuePair<int, string>(3, "Add Patent related info"));
                TableInfoNameList.Add(new KeyValuePair<int, string>(4, "Add any other info"));
                if (companyOtherInfoInsertUpdateViewModel.Count == 0)
                {
                    foreach (var lst in TableInfoNameList)
                    {
                        CompanyOtherInfoInsertUpdateViewModel objEmpty = new CompanyOtherInfoInsertUpdateViewModel();
                        objEmpty.TableInfoName = lst.Value; //lst.ToString(); 
                        companyOtherInfoInsertUpdateViewModel.Add(objEmpty);
                    }

                }
                mainCompanyOtherInfoInsertUpdateViewModel.companyOtherInfoInsertUpdateViewModel = companyOtherInfoInsertUpdateViewModel;
                HttpResponseMessage CompanyNoteResponse = _serviceRepository.GetCompanyNoteByIdResponse("CompanyNote", CompanyId, "CompanyOtherInfo", null);
                CompanyNoteViewModel companyNoteViewModel = CompanyNoteResponse.Content.ReadAsAsync<CompanyNoteViewModel>().Result;
                mainCompanyOtherInfoInsertUpdateViewModel.CompanyNoteViewModel = companyNoteViewModel ?? new CompanyNoteViewModel();
                HttpResponseMessage CompanyResponse = _serviceRepository.GetByIdResponse("Company", CompanyId);
                CompanyViewModel companyViewModel = CompanyResponse.Content.ReadAsAsync<CompanyViewModel>().Result;
                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, new Guid(ProjectId));
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                    ViewBag.StatusName = mainCompanyOtherInfoInsertUpdateViewModel.CompanyNoteViewModel.StatusName;
                }
                ViewBag.CompanyName = companyViewModel.CompanyName;
                ViewBag.CompanyId = mainCompanyOtherInfoInsertUpdateViewModel.CompanyId;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(mainCompanyOtherInfoInsertUpdateViewModel);
        }

        [HttpPost]
        public IActionResult Index(MainCompanyOtherInfoInsertUpdateViewModel model, string actionName)
        {
            ViewBag.Message = string.Empty;
            bool bResult = false;
            var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            model.CompanyNoteViewModel.CompanyType = "CompanyOtherInfo";
            try
            { 
                if (actionName.Equals("Save"))
                {
                    bResult = validate(model);
                    if (bResult)
                    {
                        model.CompanyNoteViewModel.CompanyStatusName = "Draft";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            return RedirectToAction("Index", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                        }
                    }
                  
                }
                else if (actionName.Equals("Submitted"))
                {
                    bResult = validate(model);
                    if (bResult)
                    { 
                        model.CompanyNoteViewModel.CompanyStatusName = "Submitted";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            return RedirectToAction("Index", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                        }
                    }
                }
                else if (actionName.Equals("Reject"))
                {
                    bResult = validate(model);
                    if (bResult)
                    {
                        model.CompanyNoteViewModel.CompanyType = "CompanyOtherInfo";
                        model.CompanyNoteViewModel.CompanyStatusName = "Rejected";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            return RedirectToAction("Index", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                        }
                    }
                }
                else if (actionName.Equals("Approve"))
                {
                    bResult = validate(model);
                    if (bResult)
                    {
                        model.CompanyNoteViewModel.CompanyType = "CompanyOtherInfo";
                        model.CompanyNoteViewModel.CompanyStatusName = "Approved";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            return RedirectToAction("Index", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                        }
                    }
                }

                else if (actionName.Equals("Next"))
                    return RedirectToAction("Index", "CompanyAnalystView", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                else if (actionName.Equals("Previous"))
                    return RedirectToAction("Index", "CompanyTrendsMap", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                else if (actionName.Equals("Cancel"))
                    return RedirectToAction("Index", "#", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() }); 
                else if (actionName.Equals("AddTable"))
                {
                    CompanyOtherInfoInsertUpdateViewModel objEmpty = new CompanyOtherInfoInsertUpdateViewModel();
                    objEmpty.TableInfoName = string.Empty;
                    model.companyOtherInfoInsertUpdateViewModel.Add(objEmpty);

                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            SetRoleList();
            HttpResponseMessage CompanyNoteResponse = _serviceRepository.GetCompanyNoteByIdResponse("CompanyNote", model.CompanyId.ToString(), "CompanyOtherInfo", null);
            CompanyNoteViewModel companyNoteViewModel = CompanyNoteResponse.Content.ReadAsAsync<CompanyNoteViewModel>().Result;
            companyNoteViewModel = companyNoteViewModel ?? new CompanyNoteViewModel();
            HttpResponseMessage CompanyResponse = _serviceRepository.GetByIdResponse("Company", model.CompanyId.ToString());
            CompanyViewModel companyViewModel = CompanyResponse.Content.ReadAsAsync<CompanyViewModel>().Result;
            ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, model.ProjectId);
            if (project != null)
            {
                ViewBag.ProjectId = project.Id;
                ViewBag.ProjectName = project.ProjectName;
                ViewBag.ProjectCode = project.ProjectCode;
                ViewBag.Approver = project.Approver;
                ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                ViewBag.StatusName = companyNoteViewModel.StatusName ?? string.Empty;
            }
            ViewBag.CompanyName = companyViewModel.CompanyName;
            ViewBag.CompanyId = model.CompanyId;
            return View(model);
        }
        private void SetRoleList()
        {
            try
            {
                var roles = HttpContext.Session.GetString("userRoles");
                var userRoles = JsonConvert.DeserializeObject<List<RoleViewModel>>(roles);
                List<string> RoleListItems = new List<string>();
                foreach (var item in userRoles)
                    RoleListItems.Add(item.UserRoleName);
                ViewBag.UserRoleList = RoleListItems;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        /// <summary>
        /// Method to validate Form
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private bool validate(MainCompanyOtherInfoInsertUpdateViewModel model)
        {
            for (int k = 0; k < model.companyOtherInfoInsertUpdateViewModel.Count; k++)
            {

                if (model.companyOtherInfoInsertUpdateViewModel[k].TableInfoName == string.Empty || model.companyOtherInfoInsertUpdateViewModel[k].TableInfoName == null || model.companyOtherInfoInsertUpdateViewModel[k].TableInfoName == "")
                {
                    ViewBag.Message = "Please enter Table Info Name!!";
                    return false;
                }
                else if (model.companyOtherInfoInsertUpdateViewModel[k].Description == string.Empty || model.companyOtherInfoInsertUpdateViewModel[k].Description == null || model.companyOtherInfoInsertUpdateViewModel[k].Description == "")
                {
                    ViewBag.Message = "Please enter Description for : " + model.companyOtherInfoInsertUpdateViewModel[k].TableInfoName.ToString();
                    return false;
                }
            }
            if (model.CompanyNoteViewModel.AuthorRemark == null || model.CompanyNoteViewModel.AuthorRemark == string.Empty || model.CompanyNoteViewModel.AuthorRemark == "")
            {
                ViewBag.Message = "Please enter Author Notes!!";
                return false;
            } 
            else
            {
                ViewBag.Message = string.Empty;
                return true;
            }
        }
    }
}