﻿
      
 CREATE PROCEDURE [dbo].[SApp_GetCompanyShareVolumeByCompanyID]            
 (@CompanyID uniqueidentifier = null,    
 @includeDeleted bit = 0)            
 AS            
/*================================================================================            
Procedure Name: SApp_GetCompanyShareVolumeByCompanyID        
Author: Swami            
Create date: 05/03/2019            
Description: Get list from Value table by CompanyID            
Change History            
Date  Developer  Reason            
__________ ____________ ______________________________________________________            
05/03/2019 Swami   Initial Version 
================================================================================*/           
            
BEGIN            
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED         

--@CompanyId as CompanyId,          
SELECT CSV.Id,
	   CSV.CompanyId,
	   CR.Id AS CurrencyId,
	   --CONCAT(CR.CurrencyName, ' ', '(', CR.CurrencyCode, ')') as CurrencyNameAndCode,
	   CSV.[Period],
	   CSV.BaseDate,
	   CSV.[Date],
	   CSV.Price,
	   CSV.Volume,
	   CSV.IsDeleted,
	   CSV.UserCreatedById
	   FROM dbo.CompanyShareVolume AS CSV
	   INNER JOIN Company AS C ON CSV.CompanyId = C.Id 
	   INNER JOIN Currency AS CR ON CSV.CurrencyId = CR.Id
	   AND CSV.CompanyId = @CompanyID AND CSV.IsDeleted = 0
END