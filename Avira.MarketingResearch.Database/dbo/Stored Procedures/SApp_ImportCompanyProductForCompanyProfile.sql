﻿
CREATE   Procedure [dbo].[SApp_ImportCompanyProductForCompanyProfile]
	@CompanyId Uniqueidentifier,
	@AviraUserId Uniqueidentifier
As    
/*================================================================================    
Procedure Name: [SApp_ImportCompanyProductForCompanyProfile] 
Import Company Products from Staging table
Insert new products/Update existing/Delete not present  
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
20-Sep-2019 Nitin   Initial Version    
================================================================================*/    
BEGIN

	Declare @CTE_Prod2Edit Table (Id UNIQUEIDENTIFIER, ProductName nvarchar(1024))
	Declare @CTE_Prod2Delete Table (Id UNIQUEIDENTIFIER, ProductName nvarchar(1024))
	Declare @CTE_Prod2Insert Table (ProductName nvarchar(1024),ProjectId UNIQUEIDENTIFIER)

	Declare @InsertCount int, @DeleteCount int, @EditCoount Int
	Declare @Today datetime = getdate()
	
	Declare @USDCurrencyId uniqueidentifier	
	Set @USDCurrencyId = dbo.[fun_GetCurrencyIDForUSD]()

	Insert Into @CTE_Prod2Edit (Id, ProductName) 
		Select Distinct CP.ID, CP.ProductName
		From CompanyProduct CP Inner Join [StagingCompanyProduct] SCP
			On CP.CompanyId = @CompanyId And
			   CP.ProjectId= SCP.ProjectId AND
			   CP.ProductName = SCP.ProductServiceName

	Insert Into @CTE_Prod2Delete (Id, ProductName) 
		Select CP.ID, CP.ProductName
		From CompanyProduct CP 
			Left Join [StagingCompanyProduct] SCP
				On CP.CompanyId = @CompanyId And CP.ProductName = SCP.ProductServiceName AND CP.ProjectId= SCP.ProjectId
		Where SCP.ProductServiceName Is Null and CP.CompanyId = @CompanyId AND CP.ProjectId= SCP.ProjectId
			
	Insert Into @CTE_Prod2Insert (ProductName,ProjectId)
		Select SCP.ProductServiceName,SCP.ProjectId 
		From [StagingCompanyProduct] SCP 
			Left Join CompanyProduct CP 
				On SCP.CompanyId = CP.CompanyId And 
				   SCP.ProductServiceName = CP.ProductName And CP.ProjectId= SCP.ProjectId And
				   SCP.CompanyId = @CompanyId
		Where CP.Id Is Null

	Select @InsertCount = Count(*) From @CTE_Prod2Insert;
	Select @DeleteCount = Count(*) From @CTE_Prod2Delete
	Select @EditCoount = Count(*) From @CTE_Prod2Edit

	--Insert Into Errorlog (ErrorNumber, ErrorSeverity, ErrorProcedure, ErrorLine, ErrorMessage, ErrorDate)
	--	Values (99,99, 'Company Product - I: ' + Cast(@InsertCount as nvarchar(5)) + 
	--					'D: ' + Cast(@DeleteCount as nvarchar(5)) + 
	--					'E: ' + Cast(@EditCoount as nvarchar(5)), 99, @CompanyId, GETDATE())
	
	Begin -- Delete Products that are not present in new import sheet ----------------
		Delete CPCM 
		From [CompanyProductCategoryMap] CPCM 
			Inner Join @CTE_Prod2Delete CPD 
				On CPCM.CompanyProductId = CPD.Id

		Delete CPSM
		From [CompanyProductSegmentMap] CPSM
			Inner Join @CTE_Prod2Delete CPD 
				On CPSM.CompanyProductId = CPD.Id

		Delete CPCM 
		From [CompanyProductCountryMap] CPCM
			Inner Join @CTE_Prod2Delete CPD 
				On CPCM.CompanyProductId = CPD.Id

		Delete CPV
		From [CompanyProductValue] CPV
			Inner Join @CTE_Prod2Delete CPD 
				On CPV.CompanyProductId = CPD.Id

		Delete CP
		From CompanyProduct CP 
			Inner Join @CTE_Prod2Delete CPD 
				On CP.ID = CPD.Id --And CompanyId = @CompanyId
	End

	Begin --Update Products that are present in new import sheet but not in database

		Delete CPCM 
		From [CompanyProductCategoryMap] CPCM 
			Inner Join @CTE_Prod2Edit CPD 
				On CPCM.CompanyProductId = CPD.Id

		Delete CPSM
		From [CompanyProductSegmentMap] CPSM
			Inner Join @CTE_Prod2Edit CPD 
				On CPSM.CompanyProductId = CPD.Id

		Delete CPCM 
		From [CompanyProductCountryMap] CPCM
			Inner Join @CTE_Prod2Edit CPD 
				On CPCM.CompanyProductId = CPD.Id

		Delete CPV
		From [CompanyProductValue] CPV
			Inner Join @CTE_Prod2Edit CPD 
				On CPV.CompanyProductId = CPD.Id

		Update CompanyProduct
		Set LaunchDate = SCP.LaunchDate,
			[Description] = SCP.DecriptionRemarks,
			Patents = SCP.Patents,
			StatusId = Case When SCP.Status Is Null OR SCP.Status = '' Then (Select Top 1 ID From ProductStatus Where ProductStatus.ProductStatusName = 'Marketed')
							Else (Select Top 1 ID From ProductStatus Where ProductStatus.ProductStatusName = SCP.Status) 
						End,
			ProjectId = SCP.ProjectId,
			UserModifiedById = @AviraUserId,
			ModifiedOn = GetDate()
		From CompanyProduct CP 
			Inner Join [StagingCompanyProduct] SCP 
				On CP.CompanyId = @CompanyId And
					CP.ProductName = SCP.ProductServiceName	AND
					CP.ProjectId = SCP.ProjectId	
	End

	Begin -- Insert Products that are present in new import sheet but not in database ----------------
		
		--;With CTE_Prod As(Select ROW_NUMBER() Over (Partition By ProductServiceName, ProjectId Order By id) as rnum, * From [StagingCompanyProduct])
			Insert Into CompanyProduct(Id, ProductName, LaunchDate, Description, Patents, 
						StatusId, CompanyId, ProjectId, CreatedOn, UserCreatedById, IsDeleted)
			Select NewId(), ProductServiceName, LaunchDate, DecriptionRemarks, Patents,
					Case When ST.Status Is Null OR ST.Status = '' Then (Select Top 1 ID From ProductStatus Where ProductStatus.ProductStatusName = 'Marketed')
						 Else (Select Top 1 ID From ProductStatus Where ProductStatus.ProductStatusName = ST.Status) 
					End As StatusId, CompanyId, ST.ProjectId, GetDate(), @AviraUserId, 0 as isDeleted
			--From CTE_Prod st
			From @CTE_Prod2Insert CPI Inner Join StagingCompanyProduct ST On CPI.ProductName = ST.ProductServiceName AND CPI.ProjectId = ST.ProjectId
			--Where RNum = 1;	

		Insert Into [dbo].[CompanyProductCategoryMap] ([Id], [CompanyProductId], [ProductCategoryId], [IsDeleted], [CreatedOn], [UserCreatedById])
		Select NEWID(), CP.Id, SCP.ProductCategoryId, 0 as [IsDeleted], GetDate(), @AviraUserId
		From CompanyProduct CP
			Inner Join [StagingCompanyProduct] SCP
				On SCP.ProductServiceName = cp.ProductName
		Where CP.CompanyId = @CompanyId;

		INSERT INTO [dbo].[CompanyProductSegmentMap] ([Id], [CompanyProductId], [SegmentId], [CreatedOn], [UserCreatedById], [IsDeleted])
		Select NEWID(), CP.Id, SCP.SegmentId, GetDate(), @AviraUserId, 0 as [IsDeleted]
		From CompanyProduct CP
			Inner Join [StagingCompanyProduct] SCP
				On SCP.ProductServiceName = CP.ProductName
		Where CP.CompanyId = @CompanyId;

		;with CTE_SCP as (select ProductServiceName, GEO.Value as RegionName
				from [StagingCompanyProduct] SCP
				cross apply dbo.FUN_STRING_TOKENIZER(scp.TargetedGeogrpahy, ',') GEO ),
		CTE_PRMap as (select ProductServiceName, Region.Id as RegionID, CRM.CountryId
					from CTE_SCP
					inner join Region
					on CTE_SCP.RegionName = Region.RegionName
					inner join CountryRegionMap CRM
					on CRM.RegionId = Region.Id
					inner join Country C 
						On CRM.CountryId = C.Id
					Where ((Region.RegionName in ('Rest of World (ROW)', 'Global') and  C.CountryName = 'Global')
							or (C.CountryName = 'All of '+ Region.RegionName))
					)
		INSERT INTO [dbo].[CompanyProductCountryMap]
				   ([Id]
				   ,[CompanyProductId]
				   ,[CountryId]
				   ,[CreatedOn]
				   ,[UserCreatedById]
				   ,[IsDeleted])
		select distinct NewID(),
		CP.Id,
		CTE_PRMap.CountryId,
		getdate(),
		@AviraUserId,
		0 as [IsDeleted]
		from CompanyProduct CP
		inner join CTE_PRMap
		on CTE_PRMap.ProductServiceName = cp.ProductName
		where cp.CompanyId = @CompanyId ;

		INSERT INTO [dbo].[CompanyProductValue]
				   ([Id]
				   ,[CompanyProductId]
				   ,[Year]
				   ,[CurrencyId]
				   ,[ValueConversionId]
				   ,[Amount])
		select NEWID(),
		CP.Id,
		YEAR(getdate()),
		SCP.CurrencyId,
		SCP.ValueConversionId,
		--SCP.Revenue
		[dbo].[fun_CurrencyConversion]([dbo].[fun_UnitConversion](SCP.ValueConversionId, SCP.Revenue), CurrencyId, @USDCurrencyId, @Today)
		from CompanyProduct CP
		inner join [StagingCompanyProduct] SCP
		on SCP.ProductServiceName = cp.ProductName
		where cp.CompanyId = @CompanyId
		and SCP.revenue is not null;
	End
END