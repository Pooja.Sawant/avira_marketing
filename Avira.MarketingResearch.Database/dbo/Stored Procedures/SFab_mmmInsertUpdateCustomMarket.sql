﻿
CREATE PROCEDURE [dbo].[SFab_mmmInsertUpdateCustomMarket]
(
@ProjectId uniqueidentifier,
@Name nvarchar(50),
@Description nvarchar(500),
@UserId	uniqueidentifier,
@SequenceNo int,
@Id uniqueidentifier = null,
@ResId uniqueidentifier out
)
As   

/*================================================================================    
Procedure Name: SFab_MMMInsertUpdateCustomMarket   
Author: Harshal   
Create date: 02-June-2020
Description: Create/Update new Custom market by user
Change History    
Date  Developer  Reason  
__________ ____________ ______________________________________________________    
02-June-20  Harshal   Initial Version    
================================================================================*/    
BEGIN  

SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)     
 BEGIN TRY 

Declare @NewId uniqueidentifier
set  @NewId = newid()

------------------
	--Required Tables: MakeMyMarkets
	--Notes: N/A
	--Case 1: N/A
	--Expected Result :SELECT StoredProcedureName ='SFab_MMMInsertUpdateCustomMarket',Message = @ErrorMsg,Success = 1/0;
IF (@Id='00000000-0000-0000-0000-000000000000' OR @Id=null )
Begin
	IF EXISTS (SELECT 1 FROM MakeMyMarkets
				WHERE ProjectID = @ProjectId AND UserId=@UserId AND Name=@Name)
	BEGIN      
     
		Set @ErrorMsg = 'Custom Market Name is already exists.';      
  
		SELECT StoredProcedureName ='SFab_MMMInsertUpdateCustomMarket','Message' =@ErrorMsg,'Success' = 0;    
		--RETURN;  
	End   
	Else
	Begin
	   INSERT INTO [dbo].[MakeMyMarkets]
		 		   ([Id],
		 		   [ProjectID],
		 		   [UserId],
		 		   [Name],
				   [Description],
				   [SequenceNo],
		 		   [CreatedOn],
		 		   [UserCreatedById]
		 		)
		 	 VALUES(
		 	@NewId,
		 	@ProjectId,
		 	@UserId,
		 	@Name,
		 	@Description,
			@SequenceNo,
		 	GETDATE(),
		 	@UserId)

			SET @ResId = @NewId
		 	SELECT StoredProcedureName ='SFab_MMMInsertUpdateCustomMarket',Message =@ErrorMsg,Success = 1; 
				
		END
   End
   else
   begin
         Update [dbo].[MakeMyMarkets]
		 SET [Name]= @Name,
			 [Description] = @Description,
			 [SequenceNo]=@SequenceNo,
			 [UserModifiedById]=@UserId,
			 [ModifiedOn]= GETDATE()
		 Where Id = @Id And UserId = @UserId and ProjectID=@ProjectId

		 SET @ResId = @Id
		 SELECT StoredProcedureName ='SFab_MMMInsertUpdateCustomMarket',Message =@ErrorMsg,Success = 1; 
   end
END TRY      
BEGIN CATCH      
  exec SApp_LogErrorInfo
 set @ErrorMsg = 'Error while insert/update a custom market, please contact Admin for more details.';    
SELECT StoredProcedureName ='SFab_MMMInsertUpdateCustomMarket',Message =@ErrorMsg,Success = 0;  
END CATCH   
		
END