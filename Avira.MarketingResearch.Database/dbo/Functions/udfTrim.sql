﻿CREATE FUNCTION [dbo].[udfTrim] 
(
	@StringToClean as varchar(8000)
)
RETURNS varchar(8000)
AS
BEGIN	
	--Replace all non printing whitespace characers with Characer 32 whitespace
	--NULL
	Set @StringToClean = Replace(@StringToClean,CHAR(0),'');
	--Horizontal Tab
	Set @StringToClean = Replace(@StringToClean,CHAR(9),'');
	--Line Feed
	Set @StringToClean = Replace(@StringToClean,CHAR(10),'');
	--Vertical Tab
	Set @StringToClean = Replace(@StringToClean,CHAR(11),'');
	--Form Feed
	Set @StringToClean = Replace(@StringToClean,CHAR(12),'');
	--Carriage Return
	Set @StringToClean = Replace(@StringToClean,CHAR(13),'');
	--Column Break
	Set @StringToClean = Replace(@StringToClean,CHAR(14),'');
	--Non-breaking space
	Set @StringToClean = Replace(@StringToClean,CHAR(160),'');

	Set @StringToClean = LTRIM(RTRIM(@StringToClean));
	Return @StringToClean
END