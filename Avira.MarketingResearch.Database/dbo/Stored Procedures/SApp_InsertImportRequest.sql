﻿
CREATE procedure [dbo].[SApp_InsertImportRequest]    
(    
@TemplateName	nvarchar(200),
@ImportFileName nvarchar(200),
@FileName nvarchar(200),
@ImportFilePath nvarchar(1000),
@ImportStatusName nvarchar(100),  
@ImportException nvarchar(MAX)=null,
@ImportExceptionFilePath nvarchar(1000)=null,
@ImportedOn datetime =null,
@UserImportedById uniqueidentifier)    
as    
/*================================================================================    
Procedure Name: SApp_InsertImportRequest    
Author: Harshal    
Create date: 10/15/2019    
Description: Insert a record into ImportRequest table    
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
10/15/2019  Harshal   Initial Version    
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    
declare @ErrorMsg NVARCHAR(2048),
        @StatusId uniqueidentifier

SELECT @StatusId=Id FROM LookupStatus where StatusName=@ImportStatusName
  
BEGIN TRY       
       
insert into [dbo].[ImportData]([Id]
      ,[TemplateName]
      ,[ImportFileName]
	  ,[FileName]
      ,[ImportFilePath]
      ,[ImportStatusId]
      ,[ImportException]
      ,[ImportExceptionFilePath]
      ,[ImportedOn]
      ,[UserImportedById])    
 values(NEWID(),     
   @TemplateName,     
   @ImportFileName,
   @FileName,  
   @ImportFilePath,   
   @StatusId,    
   @ImportException, 
   @ImportExceptionFilePath,
   @ImportedOn,   
   @UserImportedById);    
    
SELECT StoredProcedureName ='SApp_InsertImportRequest',Message =@ErrorMsg,Success = 1;     
  
END TRY    
BEGIN CATCH    
    -- Execute the error retrieval routine.    
 DECLARE @ErrorNumber int;    
 DECLARE @ErrorSeverity int;    
 DECLARE @ErrorProcedure varchar(100);    
 DECLARE @ErrorLine int;    
 DECLARE @ErrorMessage varchar(MAX);    
    
  SELECT @ErrorNumber = ERROR_NUMBER(),    
        @ErrorSeverity = ERROR_SEVERITY(),    
        @ErrorProcedure = ERROR_PROCEDURE(),    
        @ErrorLine = ERROR_LINE(),    
        @ErrorMessage = ERROR_MESSAGE()    
    
 insert into dbo.Errorlog(ErrorNumber,    
       ErrorSeverity,    
       ErrorProcedure,    
       ErrorLine,    
       ErrorMessage,    
       ErrorDate)    
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,@ImportedOn);    
  
 set @ErrorMsg = 'Error while creating a new Import Data, please contact Admin for more details.';    

SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;     
     
END CATCH    
    
    
end