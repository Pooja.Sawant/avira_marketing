﻿using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.CompanyProfile
{
    public class CompanyOrganizationService : ICompanyOrganizationService
    {
        private readonly ICompanyOrganizationRepository _companyOrganizationRepository;

        public CompanyOrganizationService(ICompanyOrganizationRepository companyOrganizationRepository)
        {
            _companyOrganizationRepository = companyOrganizationRepository;

        } 

        public IEnumerable<CompanyOrganizationViewModel> GetOrganizationList(Guid? Id)
        {
            var res = _companyOrganizationRepository.GetOrganizationList(Id);
            return res;
        }
    }
}
