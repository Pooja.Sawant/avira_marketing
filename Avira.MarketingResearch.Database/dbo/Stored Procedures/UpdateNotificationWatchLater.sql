﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE UpdateNotificationWatchLater
	-- Add the parameters for the stored procedure here
	
@ActionName	varchar(1000),
@IsBroadCosted	bit = 1,
@AssignedById	uniqueidentifier,
@AssignedToId	uniqueidentifier,
@Description	nvarchar(1000) ,
@LowId	uniqueidentifier,
@HighId	uniqueidentifier,
@ModifiedOn	datetime,
@UserModifiedById	uniqueidentifier,
@UserId	uniqueidentifier
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

UPDATE [dbo].[NotificationWatcher]

  SET [ActionName] = @ActionName,
      [AssignedById] =@UserId,
      [AssignedToId] = @AssignedToId,
      [Description] = @Description,
      [LowId] = @LowId,
      [HighId] = @HighId,
      [ModifiedOn] = getdate(),
      [UserModifiedById] = UserModifiedById

END