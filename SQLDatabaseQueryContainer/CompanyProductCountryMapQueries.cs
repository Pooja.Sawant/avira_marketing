﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
   public  class CompanyProductCountryMapQueries
    {
        public static readonly string StoreProcInsertUpdate = "SApp_InsertUpdateCompanyProductCountryMap";

        public static string StoreProcSelectAll = "SApp_GetCountryByCompanyProductID";
    }
}
