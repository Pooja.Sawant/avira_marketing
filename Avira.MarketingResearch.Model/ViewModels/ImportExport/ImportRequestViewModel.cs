﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.ImportExport
{
    public class ImportRequestViewModel
    {
        public Guid Id { get; set; }
        public string TemplateName { get; set; }
        public string ImportFileName { get; set; }
        public string FileName { get; set; }
        public string ImportFilePath { get; set; }
        public Guid ImportStatusId { get; set; }
        public string StatusName { get; set; }
        public string ImportException { get; set; }
        public string ImportExceptionFilePath { get; set; }
        public DateTime ImportedOn { get; set; }
        public Guid UserImportedById { get; set; }
        public DateTime ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public byte[] fileByte { get; set; }
        public IFormFile ImportFile { get; set; }
    }
}
