﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.Trend
{
    public class ImpactDirectionViewModel
    {
        public Guid ImpactDirectionId { get; set; }
        public string ImpactDirectionName { get; set; }
        public string ImpactDirectionDescription { get; set; }
    }
}
