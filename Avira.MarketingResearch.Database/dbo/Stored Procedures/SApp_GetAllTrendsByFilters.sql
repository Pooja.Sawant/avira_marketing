﻿CREATE PROCEDURE [dbo].[SApp_GetAllTrendsByFilters]
(
	@ImportanceIdList [dbo].[udtId] READONLY,
	@OutlookId UniqueIdentifier = null	
)
as
/*================================================================================
Procedure Name: [SApp_GetAllTrendsByAllIds]
Author: Sai Krishna
Create date: 15/05/2019
Description: Get list from Trends by project Id, Market Id, Indiustry Id
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
15/05/2019	Sai Krishna		Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

Declare @impidlist bit;
set @impidlist = (select count(*) from @ImportanceIdList);

Select @OutlookId,@impidlist;
select distinct Trend.Id,
			Trend.TrendName,
			dbo.fun_UnitDeConversion(TrendValueMap.ValueConversionId, TrendValueMap.Amount) as Amount,
			TrendValueMap.Year,
			Trend.ImportanceId,
			imp.ImportanceName
	from Trend
	left join TrendValueMap
	on Trend.Id=TrendValueMap.TrendId
	inner join Importance imp
	On Trend.ImportanceId = imp.Id
	and Trend.ImportanceId = @OutlookId
	where Trend.IsDeleted=0
	and (not exists (select 1 from @ImportanceIdList) or exists (select 1 from @ImportanceIdList list
																where Trend.ImportanceId = list.ID))
	order by Trend.ImportanceId desc

END