﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.Trend
{
    public class TrendValueViewModel
    {
        public Guid TrendId { get; set; }
        public Guid ImportanceId { get; set; }
        public string ImportanceName { get; set; }
        public string ImportanceDescription { get; set; }
        public Guid ImpactDirectionId { get; set; }
        public string ImpactDirectionName { get; set; }
        public string ImpactDirectionDescription { get; set; }
        public decimal? TrendValue { get; set; }
        public Guid TimeTagId { get; set; }
        public string TimeTagName { get; set; }
        public string TimeTagDescription { get; set; }
        public decimal Amount { get; set; }
        public int? Year { get; set; }
        public bool EndUser { get; set; }
        public string Rationale { get; set; }
        public Guid ValueConversionId { get; set; }
        public string ValueName { get; set; }
        public decimal Conversion { get; set; }
        public Guid ImpactTypeId { get; set; }
        public string ImpactTypeName { get; set; }
        public string ImpactDescription { get; set; }
        public bool IsDeleted { get; set; }
        public string RowType
        {
            get
            {
                return "TrendValue";
            }
        }
    }

    public class MainTrendValueInsertUpdateDbViewModel
    {
        public Guid TrendId { get; set; }
        public Guid ValueConversionId { get; set; }
        public Guid ImportanceId { get; set; }
        public Guid ImpactDirectionId { get; set; }
        public string TimeTagName { get; set; }
        public string ValueName { get; set; }
        public List<TrendValueViewModel> TrendValueInsertUpdate { get; set; }
        public List<ImpactDirectionViewModel> ImpactDirectionViewModel { get; set; }
        public List<ImportanceViewModel> ImportanceViewModel { get; set; }
        public Guid AviraUserId { get; set; }
        public Guid UserCreatedById { get; set; }
        public TrendNoteInsertUpdateDbViewModel TrendNote { get; set; }
        public TrendViewModel TrendModel { get; set; }
        public TrendNoteViewModel TrendNoteModel { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class ValueConversionViewModel
    {
        public Guid ValueConversionId { get; set; }
        public string ValueName { get; set; }
        public decimal Conversion { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class TrendValuePreviewViewModel
    {
        public int Year { get; set; }
        public decimal Amount { get; set; }
        public string Rationale { get; set; }
        public string ValueName { get; set; }
        public DateTime? ValueSubmittedDate { get; set; }
        public DateTime? ValueApprovedDate { get; set; }
    }
}
