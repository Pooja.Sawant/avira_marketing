﻿CREATE TABLE [dbo].[CompanyRevenue] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [CompanyId]        UNIQUEIDENTIFIER NOT NULL,
    [ProjectId]        UNIQUEIDENTIFIER NULL,
    [CurrencyId]       UNIQUEIDENTIFIER NOT NULL,
    [UnitId]           UNIQUEIDENTIFIER NOT NULL,
    [TotalRevenue]     DECIMAL (18, 4)  NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_CompanyRevenue_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    [Year]             INT              NULL,
    CONSTRAINT [PK_CompanyRevenue] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CompanyRevenue_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyRevenue_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyRevenue_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([Id]),
    CONSTRAINT [FK_CompanyRevenue_CompanyRevenue] FOREIGN KEY ([ProjectId]) REFERENCES [dbo].[Project] ([ID]),
    CONSTRAINT [FK_CompanyRevenue_Currency] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[Currency] ([Id]),
    CONSTRAINT [FK_CompanyRevenue_ValueConversion] FOREIGN KEY ([UnitId]) REFERENCES [dbo].[ValueConversion] ([Id])
);

