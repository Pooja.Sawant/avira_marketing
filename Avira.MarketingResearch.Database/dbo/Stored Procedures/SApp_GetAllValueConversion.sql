﻿  
    
    
CREATE PROCEDURE [dbo].[SApp_GetAllValueConversion]      
 AS    
/*================================================================================    
Procedure Name: [SApp_GetAllValueConversion]    
Author: Gopi    
Create date: 05/07/2019    
Description: Get list of Units from table     
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
05/07/2019 Gopi Krishna   Initial Version    
================================================================================*/    
    
BEGIN    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED    
    
 SELECT *    
 FROM [dbo].[ValueConversion]    
 where ([ValueConversion].IsDeleted = 0)     
 END