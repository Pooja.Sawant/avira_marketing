﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.CompanyProfile
{
    public class CompanyAnalystViewModel
    {
        public Guid CompanyId { get; set; }
        public Guid ProjectId { get; set; }
        public string AnalystView { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public Guid Id { get; set; }
        public string RowType
        {
            get
            {
                return "CompanyAnalystView";
            }
        }
    }

    public class MainCompanyAnalystViewModel
    {
        public Guid CompanyId { get; set; }
        public Guid ProjectId { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public CompanyAnalystViewModel CompanyAnalystViewModel { get; set; }
        public CompanyNoteViewModel CompanyNoteViewModel { get; set; }
    }
}
