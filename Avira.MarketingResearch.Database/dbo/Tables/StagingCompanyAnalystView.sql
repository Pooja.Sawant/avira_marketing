﻿CREATE TABLE [dbo].[StagingCompanyAnalystView] (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [ImportId]     UNIQUEIDENTIFIER NULL,
    [Market]       NVARCHAR (255)   NULL,
    [AnalystView]  NVARCHAR (MAX)   NULL,
    [DateOfUpdate] DATETIME         NULL,
    [Category]     NVARCHAR (255)   NULL,
    [Importance]   NVARCHAR (255)   NULL,
    [CategoryId]   UNIQUEIDENTIFIER NULL,
    [ImportanceId] UNIQUEIDENTIFIER NULL,
    [CompanyId]    UNIQUEIDENTIFIER NULL,
    [ProjectId]    UNIQUEIDENTIFIER NULL,
    [ErrorNotes]   NVARCHAR (1000)  NULL,
    [RowNo]        INT              NULL,
    CONSTRAINT [PK_StagingCompanyAnalystView] PRIMARY KEY CLUSTERED ([Id] ASC)
);





