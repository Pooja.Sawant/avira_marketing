﻿CREATE PROCEDURE [dbo].[proc_BackEnd_UserRolePermissionMap_Create]
(
@RoleName NVARCHAR(MAX),
@PermissionName NVARCHAR(MAX),
@PermissionLevel tinyint
)
AS
/*================================================================================
Procedure Name: UserRolePermissionMap
Author: Praveen
Create date: 07/02/2019
Description: To Map user role Permission from backend
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
07/02/2019	Praveen			Initial Version
================================================================================*/
BEGIN
	declare @RoleId uniqueidentifier,  @PermissionId uniqueidentifier
SELECT @RoleId = Id FROM UserRole WHERE UserRoleName = @RoleName
SELECT @PermissionId = Id FROM Permission WHERE PermissionName = @PermissionName

	
INSERT INTO UserRolePermissionMap(Id,
UserRoleId        , 
PermissionId	  ,
PermissionLevel	  ,
CreatedOn		  ,
UserCreatedById	  
)
VALUES( NEWID(),
@RoleId,
@PermissionId	  ,
@PermissionLevel	  ,
GETDATE()		  ,
'6B5A19C0-45E6-4FFC-815F-75F7C3D22B79' )

END