﻿CREATE TABLE [dbo].[Region](
	[Id] [uniqueidentifier] NOT NULL,
	[RegionName] [nvarchar](256) NOT NULL,
	[RegionLevel] [tinyint] NOT NULL,
	[ParentRegionId] [uniqueidentifier] NULL,
	[CreatedOn] [datetime2](7) NULL,
	[UserCreatedById] [uniqueidentifier] NULL,
	[ModifiedOn] [datetime2](7) NULL,
	[UserModifiedById] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeletedOn] [datetime2](7) NULL,
	[UserDeletedById] [uniqueidentifier] NULL,
 CONSTRAINT [PK_Region] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Region] ADD  CONSTRAINT [DF_Region_RegionName]  DEFAULT ('') FOR [RegionName]
GO
ALTER TABLE [dbo].[Region] ADD  CONSTRAINT [DF_Region_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Region]  WITH CHECK ADD  CONSTRAINT [FK_Region_AviraUser] FOREIGN KEY([UserCreatedById])
REFERENCES [dbo].[AviraUser] ([Id])
GO
ALTER TABLE [dbo].[Region] CHECK CONSTRAINT [FK_Region_AviraUser]
GO
ALTER TABLE [dbo].[Region]  WITH CHECK ADD  CONSTRAINT [FK_Region_AviraUser1] FOREIGN KEY([UserModifiedById])
REFERENCES [dbo].[AviraUser] ([Id])
GO
ALTER TABLE [dbo].[Region] CHECK CONSTRAINT [FK_Region_AviraUser1]
GO
ALTER TABLE [dbo].[Region]  WITH CHECK ADD  CONSTRAINT [FK_Region_ParentRegionId] FOREIGN KEY([ParentRegionId])
REFERENCES [dbo].[Region] ([Id])
GO
ALTER TABLE [dbo].[Region] CHECK CONSTRAINT [FK_Region_ParentRegionId]
GO