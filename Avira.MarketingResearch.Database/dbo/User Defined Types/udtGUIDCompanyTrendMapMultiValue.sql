﻿CREATE TYPE [dbo].[udtGUIDCompanyTrendMapMultiValue] AS TABLE (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [TrendName]    NVARCHAR (1024)  NOT NULL,
    [Department]   NVARCHAR (256)   NOT NULL,
    [CompanyId]    UNIQUEIDENTIFIER NOT NULL,
    [ImportanceId] UNIQUEIDENTIFIER NOT NULL,
    [ImpactId]     UNIQUEIDENTIFIER NOT NULL);

