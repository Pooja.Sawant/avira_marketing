﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Model.CustomValidation
{
    public class ValidateFileAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            int MaxContentLength = 1024 * 1024 * 1; //Max 1 MB file
            string[] AllowedFileExtensions = new
                string[] { ".pdf", ".doc", ".docx", ".ppt", ".xlsx", ".xls", ".txt" };

            var file = value as IFormFile;

            if (file == null)
                return false;
            else if (!AllowedFileExtensions.Contains((file != null) ?
                file.FileName.Substring(file.FileName.LastIndexOf('.')).ToLower()
                : string.Empty))
            {
                ErrorMessage = "Please upload Your file of type: " +
                    string.Join(", ", AllowedFileExtensions);
                return false;
            }
            else if (file.Length > MaxContentLength)
            {
                ErrorMessage = "Your file is too large, maximum allowed size is : "
                    + (MaxContentLength / 1024).ToString() + "MB";
                return false;
            }
            else
                return true;
        }
    }
}
