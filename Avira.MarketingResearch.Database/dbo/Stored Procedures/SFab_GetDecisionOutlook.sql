﻿CREATE PROCEDURE [dbo].[SFab_GetDecisionOutlook]
As   

/*================================================================================    
Procedure Name: SFab_GetDecisionOutlook   
Author: Praveen    
Create date: 07-Jan-2020
Description: To return all outlook
Change History    
Date  Developer  Reason  
__________ ____________ ______________________________________________________    
07-Jan-2020  Praveen   Initial Version    
================================================================================*/    
BEGIN    
------------------
	--Required Tables: TimeTag
	--Notes: Only these values are rquired (Immediate,Short Term,Mid Term and Long Term)
	--Case 1: N/A
	--Expected Columns :Id, OutlookName
	SELECT id,TimeTagName 
		from TimeTag 
		where id not in('C002928F-4245-4C50-AE79-07B9159CB2C2','02D28148-A9E7-4468-A098-6014CA344F62') order by TimeTagName
END


--exec [dbo].[SFab_GetDecisionOutlook]
--select * from TimeTag