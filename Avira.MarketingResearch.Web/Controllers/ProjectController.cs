﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.Models;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Avira.MarketingResearch.Web.Controllers
{
    [Authorization]
    [NoCache]    
    public class ProjectController : BaseController
    {
        private ILoggerManager _logger;
        public readonly IServiceRepository _serviceRepository;
        public ProjectController(IServiceRepository serviceRepository, ILoggerManager logger)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
        }

        public IActionResult Index()
        {
            if (TempData["Message"] != null)
            {
                ViewBag.Message = TempData["Message"];
            }
            return View();
        }


        [HttpGet]
        public IActionResult ProjectTracker(Guid Id)
        {
            int pageSize = 0;
            int PageStart = 0;
            var SortDirection = -1;
            var sortColumn = "";
            string search = "";
            List<ProjectViewModel> project = null;
            try
            {
                var LoginId = HttpContext.Session.GetString("UserId");
                HttpResponseMessage response = _serviceRepository.GetResponseByUserId("Project", Id,new Guid(LoginId), PageStart, pageSize, SortDirection, sortColumn, search);
                response.EnsureSuccessStatusCode();
                project = response.Content.ReadAsAsync<List<ProjectViewModel>>().Result;
                foreach (var proj in project)
                {
                    if (!string.IsNullOrEmpty(proj.MApproverApprover))
                    {
                        var jApprover = JsonConvert.DeserializeObject<List<object>>(proj.MApproverApprover);
                        var definition = new[] { new { Id = "", ResourceName = "" } };
                        var customer = JsonConvert.DeserializeAnonymousType(proj.MApproverApprover, definition);
                        var name = customer.Select(x => x.ResourceName).ToList();
                        proj.Approver = (string)name[0];
                    }
                    if (!string.IsNullOrEmpty(proj.TrendsFormsAuthor))
                    {
                        var jApprover = JsonConvert.DeserializeObject<List<object>>(proj.TrendsFormsAuthor);
                        var definition = new[] { new { Id = "", ResourceName = "" } };
                        var customer = JsonConvert.DeserializeAnonymousType(proj.TrendsFormsAuthor, definition);
                        var name = customer.Select(x => x.ResourceName).ToList();
                        proj.ProjectAuthorName = (string)name[0];
                    }
                    if (!string.IsNullOrEmpty(proj.TrendsFormsCoAuthor))
                    {
                        var jApprover = JsonConvert.DeserializeObject<List<object>>(proj.TrendsFormsCoAuthor);
                        var definition = new[] { new { Id = "", ResourceName = "" } };
                        var customer = JsonConvert.DeserializeAnonymousType(proj.TrendsFormsCoAuthor, definition);
                        var name = customer.Select(x => x.ResourceName).ToList();
                        proj.ProjectCoAuthorName = (string)name[0];
                    }
                }
                ViewBag.ProjectId = Id;
                if (project != null && project.Count > 0)
                {
                    ViewBag.ProjectName = project[0].ProjectName;
                    ViewBag.ProjectCode = project[0].ProjectCode;
                    ViewBag.Approver = project[0].Approver;
                    ViewBag.ProjectAuthorName = project[0].ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project[0].ProjectCoAuthorName;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(project[0]);
        }
        public ActionResult LoadData(ProjectViewModel project)
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            int totalRecords = 0;
            IEnumerable<ProjectViewModel> data =null;
            try
            {
                var roles = HttpContext.Session.GetString("userRoles");
                var LoginId = HttpContext.Session.GetString("UserId");
                string LoginUserName = HttpContext.Session.GetString("Username");
                var userRoles = JsonConvert.DeserializeObject<List<RoleViewModel>>(roles);
                List<string> RoleListItems = new List<string>();
                foreach (var item in userRoles)
                    RoleListItems.Add(item.UserRoleName);
                ViewBag.UserRoleList = RoleListItems;


                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                string search = Request.Form["search[value]"];
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();

                var sortColumnDir = Request.Form["order[0][dir]"].FirstOrDefault();

                var SortDirection = -1;
                if (sortColumnDir == "asc")
                {
                    SortDirection = 1;
                }
                else
                {
                    SortDirection = -1;
                }

                int skip = start != null ? Convert.ToInt32(start) : 0;
                int pageSize = length.ToString() != null ? Convert.ToInt32(length) : 0;
                int PageStart = start.ToString() != null ? Convert.ToInt32(start) : 0;
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
               
                HttpResponseMessage response = _serviceRepository.GetResponseByUserId(contollerName,Guid.Empty, new Guid(LoginId), PageStart, pageSize, SortDirection, sortColumn, search);

                response.EnsureSuccessStatusCode();
                List<ProjectViewModel> projects = response.Content.ReadAsAsync<List<ProjectViewModel>>().Result;
                foreach (var proj in projects)
                {
                    if (!string.IsNullOrEmpty(proj.MApproverApprover))
                    {
                        var jApprover = JsonConvert.DeserializeObject<List<object>>(proj.MApproverApprover);
                        var definition = new[] { new { Id = "", ResourceName = "" } };
                        var customer = JsonConvert.DeserializeAnonymousType(proj.MApproverApprover, definition);
                        var name = customer.Select(x => x.ResourceName).ToList();
                        proj.Approver = (string)name[0];
                    }
                    if (!string.IsNullOrEmpty(proj.MCoApproverCoApprover))
                    {
                        var jApprover = JsonConvert.DeserializeObject<List<object>>(proj.MCoApproverCoApprover);
                        var definition = new[] { new { Id = "", ResourceName = "" } };
                        var customer = JsonConvert.DeserializeAnonymousType(proj.MCoApproverCoApprover, definition);
                        var name = customer.Select(x => x.ResourceName).ToList();
                        proj.CoApprover = (string)name[0];
                    }
                }

                ViewBag.Title = "All Projects";
                //var data = projects;
                //As per Dicussion with Sharath will skip that condition(07-17-2019)
                //data = RoleListItems.Contains("Admin") ? projects : projects.Where(x => x.ProjectApproverName == LoginUserName || x.ProjectAuthorName == LoginUserName || x.ProjectCoAuthorName == LoginUserName);
                data = projects;
                foreach (var item in projects)
                {
                    totalRecords = item.TotalItems;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data });

        }

        [HttpGet]
        public IActionResult Edit(string id)
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                MainProjectUpdateDBViewModel project = new MainProjectUpdateDBViewModel() {  projectUpdateDbViewModel = new ProjectUpdateDbViewModel(),projectAnalistDetailsUpdateApproverDbViewModel= new ProjectAnalistDetailsUpdateDbViewModel()
                ,projectAnalistDetailsUpdateCoApproverDbViewModel= new ProjectAnalistDetailsUpdateDbViewModel(),projectAnalistDetailsUpdateTrendDbViewModel= new ProjectAnalistDetailsUpdateDbViewModel(),projectAnalistDetailsUpdateQuantDbViewModel= new ProjectAnalistDetailsUpdateDbViewModel(),projectAnalistDetailsUpdateCompanyDbViewModel=new ProjectAnalistDetailsUpdateDbViewModel()
                ,projectAnalistDetailsUpdateMarketDbViewModel= new ProjectAnalistDetailsUpdateDbViewModel(), projectAnalistDetailsUpdatePrimiDbViewModel= new ProjectAnalistDetailsUpdateDbViewModel()};
                //var LoginId = HttpContext.Session.GetString("UserId");
                //HttpResponseMessage response = _serviceRepository.GetProjectByIdResponse(contollerName, id, new Guid(LoginId));
                //response.EnsureSuccessStatusCode();
                //DisplayProjectDBViewModel projectData = response.Content.ReadAsAsync<DisplayProjectDBViewModel>().Result;
                int pageSize = 0;
                int PageStart = 0;
                var SortDirection = -1;
                var sortColumn = "";
                string search = "";
                List<ProjectViewModel> projectDataNew = null;
                var LoginId = HttpContext.Session.GetString("UserId");
                HttpResponseMessage response = _serviceRepository.GetResponseByUserId("Project", new Guid(id), new Guid(LoginId), PageStart, pageSize, SortDirection, sortColumn, search);
                response.EnsureSuccessStatusCode();
                projectDataNew = response.Content.ReadAsAsync<List<ProjectViewModel>>().Result;

                ProjectViewModel projectData = new ProjectViewModel();
                projectData = projectDataNew.ElementAtOrDefault(0);
                var tempstr = projectData.TrendsFormsApprover.Split("$");

                ViewBag.BroadIndustry = tempstr[0].ToString();
                ViewBag.Industry = tempstr[1].ToString();
                ViewBag.Market = tempstr[2].ToString();
                ViewBag.SubMarket = tempstr[3].ToString();
                project.projectUpdateDbViewModel.ProjectID = projectData.Id;
                project.projectUpdateDbViewModel.ProjectStatusName = projectData.ProjectStatusName;
                project.projectUpdateDbViewModel.ProjectName = projectData.ProjectName;
                project.projectUpdateDbViewModel.Remark = projectData.Remark;
                project.projectUpdateDbViewModel.StartDate = projectData.StartDate;
                project.projectUpdateDbViewModel.EndDate = projectData.EndDate;
                project.projectUpdateDbViewModel.MinCompanyProfiles = projectData.MinCompanyProfiles;
                project.projectUpdateDbViewModel.MinPrimaries = projectData.MinPrimaries;
                project.projectUpdateDbViewModel.MinTrends = projectData.MinTrends;
                project.projectAnalistDetailsUpdateTrendDbViewModel.AnalysisType = "Trends Forms";
                project.projectAnalistDetailsUpdateTrendDbViewModel.AnalystType = "Author";
                var definition = new[] { new { Id = "" } };
                var customer1 = JsonConvert.DeserializeAnonymousType(projectData.TrendsFormsAuthor, definition);
                project.projectAnalistDetailsUpdateTrendDbViewModel.AuthorUserIDs = customer1.Select(x => new Guid(x.Id)).ToArray();
                if (projectData.TrendsFormsCoAuthor != null && projectData.TrendsFormsCoAuthor !="")
                {
                    customer1 = JsonConvert.DeserializeAnonymousType(projectData.TrendsFormsCoAuthor, definition);
                    project.projectAnalistDetailsUpdateTrendDbViewModel.CoAuthorUserIDs = customer1.Select(x => new Guid(x.Id)).ToArray();
                }

                HttpResponseMessage responseCoAuthor = _serviceRepository.GetUserByRoleResponse("User", "Co-Author");
                List<AviraUser> aviraUsersCoList = responseCoAuthor.Content.ReadAsAsync<List<AviraUser>>().Result;
                ViewBag.CoAuthorList = new SelectList(aviraUsersCoList, "Id", "DisplayName");
             
               // project.projectAnalistDetailsUpdateTrendDbViewModel.Values = new MultiSelectList(aviraUsersCoList, "Id", "DisplayName", project.projectAnalistDetailsUpdateTrendDbViewModel.CoAuthorUserIDs);

                customer1 = JsonConvert.DeserializeAnonymousType(projectData.CompanyProfilesAuthor, definition);
                project.projectAnalistDetailsUpdateCompanyDbViewModel.AuthorUserIDs = customer1.Select(x => new Guid(x.Id)).ToArray();
                if (projectData.CompanyProfilesCoAuthor != null && projectData.CompanyProfilesCoAuthor != "")
                {
                    customer1 = JsonConvert.DeserializeAnonymousType(projectData.CompanyProfilesCoAuthor, definition);
                    project.projectAnalistDetailsUpdateCompanyDbViewModel.CoAuthorUserIDs = customer1.Select(x => new Guid(x.Id)).ToArray();
                }

                customer1 = JsonConvert.DeserializeAnonymousType(projectData.MarketSizingAuthor, definition);
                project.projectAnalistDetailsUpdateMarketDbViewModel.AuthorUserIDs = customer1.Select(x => new Guid(x.Id)).ToArray();
                if (projectData.MarketSizingCoAuthor != null && projectData.MarketSizingCoAuthor != "")
                {
                    customer1 = JsonConvert.DeserializeAnonymousType(projectData.MarketSizingCoAuthor, definition);
                    project.projectAnalistDetailsUpdateMarketDbViewModel.CoAuthorUserIDs = customer1.Select(x => new Guid(x.Id)).ToArray();
                }

                customer1 = JsonConvert.DeserializeAnonymousType(projectData.PrimariesInfoAuthor, definition);
                project.projectAnalistDetailsUpdatePrimiDbViewModel.AuthorUserIDs = customer1.Select(x => new Guid(x.Id)).ToArray();
                if (projectData.PrimariesInfoCoAuthor != null && projectData.PrimariesInfoCoAuthor != "")
                {
                    customer1 = JsonConvert.DeserializeAnonymousType(projectData.PrimariesInfoCoAuthor, definition);
                    project.projectAnalistDetailsUpdatePrimiDbViewModel.CoAuthorUserIDs = customer1.Select(x => new Guid(x.Id)).ToArray();
                }

                customer1 = JsonConvert.DeserializeAnonymousType(projectData.QuantativeinfoAuthor, definition);
                project.projectAnalistDetailsUpdateQuantDbViewModel.AuthorUserIDs = customer1.Select(x => new Guid(x.Id)).ToArray();
                if (projectData.QuantativeinfoCoAuthor != null && projectData.QuantativeinfoCoAuthor != "")
                {
                    customer1 = JsonConvert.DeserializeAnonymousType(projectData.QuantativeinfoCoAuthor, definition);
                    project.projectAnalistDetailsUpdateQuantDbViewModel.CoAuthorUserIDs = customer1.Select(x => new Guid(x.Id)).ToArray();
                }

                customer1 = JsonConvert.DeserializeAnonymousType(projectData.MApproverApprover, definition);
                project.projectAnalistDetailsUpdateApproverDbViewModel.AuthorUserIDs = customer1.Select(x => new Guid(x.Id)).ToArray();
                //customer1 = JsonConvert.DeserializeAnonymousType(projectData.MApproverCoApprover, definition);
                //project.projectAnalistDetailsUpdateApproverDbViewModel.CoAuthorUserIDs = customer1.Select(x => new Guid(x.Id)).ToArray();

                //customer1 = JsonConvert.DeserializeAnonymousType(projectData.MCoApproverApprover, definition);
                //project.projectAnalistDetailsUpdateCoApproverDbViewModel.AuthorUserIDs = customer1.Select(x => new Guid(x.Id)).ToArray();
                if (projectData.MCoApproverCoApprover != null && projectData.MCoApproverCoApprover != "")
                {
                    customer1 = JsonConvert.DeserializeAnonymousType(projectData.MCoApproverCoApprover, definition);
                    project.projectAnalistDetailsUpdateCoApproverDbViewModel.CoAuthorUserIDs = customer1.Select(x => new Guid(x.Id)).ToArray();
                }

                HttpResponseMessage responsAauthor = _serviceRepository.GetUserByRoleResponse("User", "Author");
                List<AviraUser> aviraUsersList = responsAauthor.Content.ReadAsAsync<List<AviraUser>>().Result;
                ViewBag.AuthorList = new SelectList(aviraUsersList, "Id", "DisplayName");

        

                HttpResponseMessage responseApprover = _serviceRepository.GetUserByRoleResponse("User", "Approver");
                List<AviraUser> aviraUsersAproveList = responseApprover.Content.ReadAsAsync<List<AviraUser>>().Result;
                ViewBag.ApproverList = new SelectList(aviraUsersAproveList, "Id", "DisplayName");

                HttpResponseMessage responseCoApprover = _serviceRepository.GetUserByRoleResponse("User", "Co-Approver");
                List<AviraUser> aviraUsersCoAproveList = responseCoApprover.Content.ReadAsAsync<List<AviraUser>>().Result;
                ViewBag.CoApproverList = new SelectList(aviraUsersCoAproveList, "Id", "DisplayName");
                return View(project);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                ErrorViewModel err = new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier };
                return View("Error", err);
            }
        }

        [HttpPost]
        public IActionResult Edit(MainProjectUpdateDBViewModel project)
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.PutResponse(contollerName, project);
                ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                if (viewModel != null)
                {
                    ViewBag.Message = viewModel.Message;
                    TempData["Message"] = viewModel.Message;
                }
                response.EnsureSuccessStatusCode();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            //return View(project);
            return RedirectToAction("Index", "Project");
        }
      

        

        public IActionResult Create()
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage responsAauthor = _serviceRepository.GetUserByRoleResponse("User", "Author");
                List<AviraUser> aviraUsersList = responsAauthor.Content.ReadAsAsync<List<AviraUser>>().Result;
                ViewBag.AuthorList = new SelectList(aviraUsersList, "Id", "DisplayName");

                HttpResponseMessage responseCoAuthor = _serviceRepository.GetUserByRoleResponse("User", "Co-Author");
                List<AviraUser> aviraUsersCoList = responseCoAuthor.Content.ReadAsAsync<List<AviraUser>>().Result;
                ViewBag.CoAuthorList = new SelectList(aviraUsersCoList, "Id", "DisplayName");

                HttpResponseMessage responseApprover = _serviceRepository.GetUserByRoleResponse("User", "Approver");
                List<AviraUser> aviraUsersAproveList = responseApprover.Content.ReadAsAsync<List<AviraUser>>().Result;
                ViewBag.ApproverList = new SelectList(aviraUsersAproveList, "Id", "DisplayName");

                HttpResponseMessage responseCoApprover = _serviceRepository.GetUserByRoleResponse("User", "Co-Approver");
                List<AviraUser> aviraUsersCoAproveList = responseCoApprover.Content.ReadAsAsync<List<AviraUser>>().Result;
                ViewBag.CoApproverList = new SelectList(aviraUsersCoAproveList, "Id", "DisplayName");

                HttpResponseMessage responseSegmentIndustryList = _serviceRepository.GetAllSegmentListIdResponse(contollerName, null, true, "Industry");
                List<IndustryViewModel> industryList = responseSegmentIndustryList.Content.ReadAsAsync<List<IndustryViewModel>>().Result;
                ViewBag.IndustryList = new SelectList(industryList, "Id", "IndustryName");

                HttpResponseMessage responseSegmentMarketList = _serviceRepository.GetAllSegmentListIdResponse(contollerName, null, true, "Market");
                List<MarketViewModel> marketList = responseSegmentMarketList.Content.ReadAsAsync<List<MarketViewModel>>().Result;
                if (marketList.Count == 0)
                {
                    ViewBag.MarketList = null;
                }
                else
                {
                    ViewBag.MarketList = new SelectList(marketList, "Id", "MarketName");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return View();
        }
        [HttpPost]
        public IActionResult IndustryDdlId(ProjectInsertDbViewModel projectmodel)
        {
            var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
            List<IndustryViewModel> allIndustry = new List<IndustryViewModel>();
            try
            {
                if (projectmodel.Remark != null && projectmodel.Remark.ToString() != "")
                {

                    string[] strP = projectmodel.Remark.Split(new string[] { "," }, StringSplitOptions.None);

                    List<IndustryViewModel> industryList = new List<IndustryViewModel>();
                    if (strP.Length > 0)
                    {
                        for (var i = 0; i < strP.Length; i++)
                        {
                            HttpResponseMessage responseSegmentIndustryList = _serviceRepository.GetAllSegmentListIdResponse(contollerName, strP[i].ToString().Trim(), false, "Industry");
                            industryList = responseSegmentIndustryList.Content.ReadAsAsync<List<IndustryViewModel>>().Result;
                            allIndustry.AddRange(industryList);
                            industryList.Clear();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Json(allIndustry.Select(x => new
            {
                Id = x.Id,
                IndustryName = x.IndustryName
            }).ToList());
          
        }

        [HttpPost]
        public IActionResult MarketDdlId(ProjectInsertDbViewModel projectmodel)
        {
           
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                string[] strP = projectmodel.Remark.Split(new string[] { "," }, StringSplitOptions.None);
                List<MarketViewModel> allMarket = new List<MarketViewModel>();
                try
                {
                List<MarketViewModel> marketList = new List<MarketViewModel>();
                if (projectmodel.Remark != null && projectmodel.Remark.ToString() != "")
                {
                    if (strP.Length > 0)
                    {
                        for (var i = 0; i < strP.Length; i++)
                        {
                            HttpResponseMessage responseSegmentMarketList = _serviceRepository.GetAllSegmentListIdResponse(contollerName, strP[i].ToString().Trim(), false, "Market");
                            marketList = responseSegmentMarketList.Content.ReadAsAsync<List<MarketViewModel>>().Result;
                            allMarket.AddRange(marketList);
                            marketList.Clear();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Json(allMarket.Select(x => new
            {
                Id = x.Id,
                MarketName = x.MarketName
            }).ToList());
            // return Json(new { allIndustry = allIndustry, recordsFiltered = totalRecords, recordsTotal = totalRecords });

        }
        [HttpPost]
        public IActionResult Create(MainProjectInsertDBViewModel project, IFormCollection fC)
        {        
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, project);
                response.EnsureSuccessStatusCode();
                ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                if (viewModel != null)
                {
                    ViewBag.Message = viewModel.Message;
                    TempData["Message"] = viewModel.Message;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }          
            return RedirectToAction("Index", "Project");
        }

   
        public ActionResult ProjectStatus(MainProjectUpdateDBViewModel project, Guid id)
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();    
                HttpResponseMessage response = _serviceRepository.PutdataResponse(contollerName, id);
                response.EnsureSuccessStatusCode();
                ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                if (viewModel != null)
                {
                    ViewBag.Message = viewModel.Message;
                    TempData["Message"] = viewModel.Message;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            
            return RedirectToAction("Index", "Project");

        }
    }
}