﻿CREATE procedure [dbo].[SFab_GetAllNewsByFilters]
(@MarketList [dbo].[udtUniqueIdentifier] READONLY ,
@CatrgoryList [dbo].[udtUniqueIdentifier] READONLY ,
@IndustryIdList [dbo].[udtUniqueIdentifier] READONLY ,
@CompanyIdList [dbo].[udtUniqueIdentifier] READONLY ,
@ProjectId uniqueidentifier = null,
@NewsName NVARCHAR(512)='',
@FilterDate nvarchar(50)=null,
@AviraUserID uniqueidentifier = null
)
as
/*================================================================================
Procedure Name: [dbo].[SFab_GetAllNewsByFilters]
Author: Pooja Sawant
Create date: 05/22/2019
Description: Get list from News table by filter
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
05/22/2019	Pooja			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
DECLARE @StartDate DATETIME;
DECLARE @EndDate DATETIME;
declare @ProjectApprovedStatusId uniqueidentifier
select @ProjectApprovedStatusId = ID from ProjectStatus
where ProjectStatusName = 'Completed';


IF(@FilterDate is null or @FilterDate = '')
begin 
set @StartDate = '01/01/1900';
set @EndDate = '12/13/2999'
end
else
begin
select @StartDate= CONVERT(DATETIME, SUBSTRING(@FilterDate,1,10), 103), @EndDate = CONVERT(DATETIME,SUBSTRING(@FilterDate,14,10),103)
end
If @StartDate >= CONVERT(varchar(10), GETDATE(), 101)
set @StartDate = '01/01/1900';
set @EndDate = CONVERT(varchar(10), @EndDate, 101) + ' 23:59:59.997';

if (@NewsName is null or @NewsName = '') set @NewsName = ''
else set @NewsName = '%'+ @NewsName+'%'

;with cteProjectCompany as 
(
select CompanyId from CompanyProjectMap
									where CompanyProjectMap.ProjectId = @ProjectId
									--and Company.Id = CompanyProjectMap.CompanyId
)


SELECT distinct News.Id, 
News.CompanyID,
News.ProjectID,
		News.Date, 
		News.News, 
		News.NewsCategoryId, 
		NewsCategory.NewsCategoryName
FROM News 
--LEFT JOIN Company 
--ON News.CompanyID = Company.Id 
INNER JOIN NewsCategory 
ON News.NewsCategoryId = NewsCategory.Id

where 
  (exists (select 1 from project 
			where project.ID = News.ProjectID
			and project.ProjectStatusID = @ProjectApprovedStatusId or ProjectId is null)
			
			)
 

AND (NOT exists(select 1 from @CatrgoryList) or exists(select ID from @CatrgoryList WHERE Id = News.NewsCategoryId))
AND (NOT exists(select 1 from @MarketList) or exists (select 1 from @MarketList MarketList
													 inner join TrendMarketMap
													 on TrendMarketMap.MarketId = MarketList.ID
													 inner join Trend
													 on TrendMarketMap.TrendId = Trend.ID
													 inner join CompanyProjectMap
													 on CompanyProjectMap.ProjectId = Trend.ProjectID --and News.CompanyID = CompanyProjectMap.CompanyId
													 where CompanyProjectMap.ProjectId = @ProjectId
													 
													 ))
and 
(exists (select 1 from @CompanyIdList WHERE ID = News.CompanyID ) 
or
 News.ProjectId = @ProjectId)
						--)
AND (NOT exists(select 1 from @IndustryIdList) or exists (select 1 from @IndustryIdList IndustryIdList
													 inner join ProjectSegmentMapping
													 on ProjectSegmentMapping.SegmentID= IndustryIdList.ID
													 and ProjectSegmentMapping.SegmentType in ('Industries', 'Broad Industries')
													 inner join CompanyProjectMap
													 on CompanyProjectMap.ProjectId = ProjectSegmentMapping.ProjectID
													 where CompanyProjectMap.ProjectId = @ProjectId  --and News.CompanyID = CompanyProjectMap.CompanyId
													))
and (@NewsName = '' or @NewsName is null or News.News like @NewsName)
and News.Date >= @StartDate
and News.Date <= @EndDate;
end