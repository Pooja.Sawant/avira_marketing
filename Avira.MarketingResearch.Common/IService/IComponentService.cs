﻿using Avira.MarketingResearch.Model;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using static Avira.MarketingResearch.Model.ViewModels.ComponentViewModel;

namespace Avira.MarketingResearch.Common.IService
{
    public interface IComponentService
    {
        IEnumerable<ComponentViewModel> GetAll(Guid Id);
        IEnumerable<ComponentViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false);
        ComponentViewModel GetById(Guid guid);

        Result<SpTransactionMessage> Create(ComponentInsertDbViewModel tfc, AviraUser currentUser);
        Result<SpTransactionMessage> Update(ComponentUpdateDbViewModel tfc, AviraUser currenUser);
    }
}
