﻿CREATE PROCEDURE [dbo].[SFab_CheckCustomerSubscription] 
(
	@CustomerId uniqueidentifier
)
AS
/*================================================================================      
Procedure Name: [SFab_CheckCustomerSubscription]     
Author: Sai Krishna      
Create date: 25/05/2019      
Description: Check the customer is exists or not
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
25/05/2019 Sai Krishna   Initial Version      
================================================================================*/
BEGIN
	SELECT COUNT(*) CHECKCUSTOMER
	FROM CustomerSubscriptionMap 
	WHERE CustomerId = @CustomerId
END