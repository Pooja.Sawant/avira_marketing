﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.CompanyProfile
{
    public class CompanyFinancialViewModel
    {
        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
        public Guid CurrencyId { get; set; }
        public Guid ValueConversionId { get; set; }
        public string CompanyName { get; set; }
        public string Currency { get; set; }
        public string Units { get; set; }
        public int Year { get; set; }
        public decimal Revenues { get; set; }
        public decimal DirectCost { get; set; }
        public decimal CostofProduction { get; set; }
        public decimal CostofSales { get; set; }
        public decimal ResearchandDevelopmentExpenses { get; set; }
        public decimal SalesandMarketingCost { get; set; }
        public decimal SellingandInformationalandAdministrativeExpenses { get; set; }
        public decimal Depreciation { get; set; }
        public decimal Amortization { get; set; }
        public decimal DepreciationandAmortization { get; set; }
        public decimal InterestExpense { get; set; }
        public decimal TaxExpense { get; set; }
        public decimal ProvisionorBenefitforTaxesonIncome { get; set; }
        public decimal IncomefromContinuingOperations { get; set; }
        public decimal NetIncome { get; set; }
        public decimal DilutedEPS { get; set; }
        public decimal WeightedAverageSharesOutstanding { get; set; }
        public decimal LongTermDebt { get; set; }
        public decimal ShortTermDebt { get; set; }
        public decimal ShortTermBorrowings { get; set; }
        public decimal CurrentPortionofLongTermDebt { get; set; }
        public decimal CashandCashEquivalents { get; set; }
        public decimal ShortTermInvestments { get; set; }
        public decimal MarketableSecurities { get; set; }
        public decimal MinorityInterest { get; set; }
        public decimal NonControllingInterests { get; set; }
        public decimal LongTermInvestments { get; set; }
        public decimal NetcashProvidedbyorUsedinOperatingActivities { get; set; }
        public decimal NetcashProvidedbyorUsedinInvestingActivities { get; set; }
        public decimal NetCashProvidedbyorUsedinFinancingActivities { get; set; }
        public decimal PurchasesofPropertyandPlantandEquipment { get; set; }
        public decimal PurchaseofFixedAssets { get; set; }
        public decimal PurchasesofShortTermInvestments { get; set; }
        public decimal PurchasesofLongTermInvestments { get; set; }
        public decimal TotalCurrentAssets { get; set; }
        public decimal Inventories { get; set; }
        public decimal AccountsReceivables { get; set; }
        public decimal TotalCurrentLiabilities { get; set; }
        public decimal PropertyandPlantandEquipment { get; set; }
        public decimal OtherFixedAssets { get; set; }
        public decimal OtherTangibleAssets { get; set; }
        public decimal IntangibleAssets { get; set; }
        public decimal Goodwill { get; set; }
        public decimal OtherNoncurrentLiabilities { get; set; }
        public decimal TotalshareholdersEquity { get; set; }
        public decimal RetainedEarnings { get; set; }
        public decimal AccumulatedIncomeandReserves { get; set; }
        public decimal AccountsPayable { get; set; }
        public decimal PurchasesofFixedAssets { get; set; }
        public decimal EffectiveTaxRate { get; set; }
        public decimal TotalPurchases { get; set; }
        public decimal IHc { get; set; }
        public decimal EHc { get; set; }
        public decimal TotalReportableSegments { get; set; }
        public decimal UnitedStates { get; set; }
        public decimal DevelopedEurope_a { get; set; }
        public decimal DevelopedRestofWorld_b { get; set; }
        public decimal EmergingMarkets_c { get; set; }
        public decimal TotalGeographicSegments { get; set; }
        public DateTime? CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid UserModifiedById { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid UserDeletedById { get; set; }

    }

    public class MainCompanyFinancialViewModel
    {
        public Guid ProjectId { get; set; }
        public Guid CompanyId { get; set; }
        public List<CompanyFinancialViewModel> CompanyFinancialViewModelList { get; set; }
        public CompanyNoteViewModel CompanyNoteViewModel { get; set; }
        public List<int> YearList { get; set; }

    }
}
