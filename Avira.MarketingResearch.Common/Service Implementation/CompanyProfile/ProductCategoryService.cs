﻿using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.CompanyProfile
{
    public class ProductCategoryService:IProductCategoryService
    {
        private readonly IProductcategoryRepository _ProductCategoryRepository;

        public ProductCategoryService(IProductcategoryRepository productCategoryRepository)
        {
            _ProductCategoryRepository = productCategoryRepository;
        }
        public IEnumerable<ProductCategoryViewModel> GetAll()
        {
            return _ProductCategoryRepository.GetAll();
        }

    }
}
