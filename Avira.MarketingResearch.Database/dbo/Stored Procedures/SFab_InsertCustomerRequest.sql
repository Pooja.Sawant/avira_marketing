﻿
  
CREATE procedure [dbo].[SFab_InsertCustomerRequest]  
(
@ID uniqueidentifier=null,
@FullName nvarchar(200)=null,  
@Email nvarchar(100)=null, 
@Salt smallint=null, 
@Password nvarchar(500)=null,
@StatusId uniqueidentifier=null,
@HandPhone nvarchar(20)=null,  
@DeskPhone nvarchar(20)=null,
@Company nvarchar(100)=null,
@Designation nvarchar(100),
@PrimaryPurpose nvarchar(500)=null,
@FunctionalArea nvarchar(100)=null,
@AddressLine1 nvarchar(500)=null, 
@AddressLine2 nvarchar(500)=null,  
@CountrySTD nvarchar(10)=null,
@Country uniqueidentifier=null,
@State nvarchar(100)=null,  
@City nvarchar(100)=null, 
@ZipCode nvarchar(50)=null, 
@Fax nvarchar(120)=null, 
@UserCreatedById uniqueidentifier=null,
@PrimaryPurposeId uniqueidentifier=null,
@FunctionalAreaId uniqueidentifier=null
)  
as  
/*================================================================================  
Procedure Name: SFab_InsertCustomerRequest  
Author: Pooja Sawant  
Create date: 05/09/2019  
Description: Insert a record into CustomerRequest table  
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
05/09/2019 Pooja   Initial Version  
================================================================================*/  
BEGIN  
SET NOCOUNT ON;  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
declare @ErrorMsg NVARCHAR(2048);  

--Validations  
IF exists(select 1 from [dbo].AviraUser   
   where EmailAddress = @Email and UserName=@Email AND  IsDeleted = 0)  
begin  
set @ErrorMsg = 'User with Email "'+ @Email + '" already exists.';  
SELECT StoredProcedureName ='SFab_InsertCustomerRequest',Message =@ErrorMsg,Success = 0;    
RETURN;    
end    
--Validations  
IF exists(select 1 from [dbo].[CustomerRequest]   
   where Email = @Email AND IsDeleted = 0)  
begin  
set @ErrorMsg = 'User with Email "'+ @Email + '" already exists.';  
SELECT StoredProcedureName ='SFab_InsertCustomerRequest',Message =@ErrorMsg,Success = 0;    
RETURN;    
end  
--End of Validations  
BEGIN TRY  
  
INSERT INTO [dbo].[CustomerRequest]  
           ([Id]  
            ,[FullName]
            ,[Email]
            ,[Password]
			,[Salt]
            ,[StatusId]
            ,[HandPhone]
            ,[DeskPhone]
            ,[Company]
            ,[Designation]
            ,[PrimaryPurpose]
            ,[FunctionalArea]
            ,[AddressLine1]
            ,[AddressLine2]
            ,[CountrySTD]
            ,[Country]
            ,[State]
            ,[City]
            ,[ZipCode]
            ,[IsValidate]
            ,[Fax]
            ,[CreatedOn]
            ,[UserCreatedById]      
            ,[IsDeleted] 
			,[PrimaryPurposeId]
           ,[FunctionalAreaId] )
     VALUES  
           (@ID,  
			@FullName,  
			@Email,
			@Password, 
			@Salt, 
			@StatusId,  
     @HandPhone,  
	 @DeskPhone,  
	 @Company,  
	 @Designation, 
	 @PrimaryPurpose,
	 @FunctionalArea,
     @AddressLine1,
	 @AddressLine2,
	 @CountrySTD,
     @Country,  
     @State,  
     @City,
	 @ZipCode,
	 0,
	 @Fax,
	 GETDATE(),
     @UserCreatedById  ,    
     0,
	 @PrimaryPurposeId,
	 @FunctionalAreaId)  
   
 
SELECT StoredProcedureName ='SFab_InsertCustomerRequest',Message = @ErrorMsg,Success = 1;  
END TRY  
BEGIN CATCH  
    -- Execute the error retrieval routine.  
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);  
  
  SELECT @ErrorNumber = ERROR_NUMBER(),  
        @ErrorSeverity = ERROR_SEVERITY(),  
        @ErrorProcedure = ERROR_PROCEDURE(),  
        @ErrorLine = ERROR_LINE(),  
        @ErrorMessage = ERROR_MESSAGE()  
  
 insert into dbo.Errorlog(ErrorNumber,  
       ErrorSeverity,  
       ErrorProcedure,  
       ErrorLine,  
       ErrorMessage,  
       ErrorDate)  
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());  
  
 set @ErrorMsg = 'Error while creating a new CustomerRequest, please contact Admin for more details.';  
  
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;  
  
END CATCH  
  
  
end