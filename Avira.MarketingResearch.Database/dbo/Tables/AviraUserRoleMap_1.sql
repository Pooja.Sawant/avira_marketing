﻿CREATE TABLE [dbo].[AviraUserRoleMap] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [AviraUserId]      UNIQUEIDENTIFIER NOT NULL,
    [UserRoleId]       UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_AviraUserRoleMap_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    [IsPrimary]        BIT              CONSTRAINT [DF__AviraUser__IsPri__2F8501C7] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_AviraUserRoleMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_AviraUserRoleMap_CreatedUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_AviraUserRoleMap_DeletedUser] FOREIGN KEY ([UserDeletedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_AviraUserRoleMap_ModifiedUser] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_AviraUserRoleMap_UserId] FOREIGN KEY ([AviraUserId]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_AviraUserRoleMap_UserRoleId] FOREIGN KEY ([UserRoleId]) REFERENCES [dbo].[UserRole] ([Id]),
    CONSTRAINT [IX_AviraUserRoleMap] UNIQUE NONCLUSTERED ([UserRoleId] ASC, [AviraUserId] ASC)
);



