﻿CREATE TABLE [dbo].[MarketValueSegmentMapping] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [ProjectId]        UNIQUEIDENTIFIER NOT NULL,
    [MarketValueId]    UNIQUEIDENTIFIER NOT NULL,
    [SegmentId]        UNIQUEIDENTIFIER NOT NULL,
    [SubSegmentId]     UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_MarketValueSegmentMapping] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_MarketValueSegmentMapping_MarketValue] FOREIGN KEY ([MarketValueId]) REFERENCES [dbo].[MarketValue] ([Id]),
    CONSTRAINT [FK_MarketValueSegmentMapping_Project] FOREIGN KEY ([ProjectId]) REFERENCES [dbo].[Project] ([ID]),
    CONSTRAINT [FK_MarketValueSegmentMapping_Segment] FOREIGN KEY ([SegmentId]) REFERENCES [dbo].[Segment] ([Id]),
    CONSTRAINT [IX_MarketValueSegmentMapping] UNIQUE NONCLUSTERED ([MarketValueId] ASC, [SegmentId] ASC)
);






GO
CREATE NONCLUSTERED INDEX [IX_MarketValueSegmentMapping_Subsegment]
    ON [dbo].[MarketValueSegmentMapping]([SegmentId] ASC)
    INCLUDE([MarketValueId], [SubSegmentId]);




GO
CREATE NONCLUSTERED INDEX [IX_MarketValueSegmentMapping_MarketValue]
    ON [dbo].[MarketValueSegmentMapping]([MarketValueId] ASC);

