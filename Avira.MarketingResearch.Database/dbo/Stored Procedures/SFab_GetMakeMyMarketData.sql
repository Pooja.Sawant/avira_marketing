﻿  
  
CREATE PROCEDURE [dbo].[SFab_GetMakeMyMarketData]
(	
	@ProjectId uniqueidentifier,
	@TrendId uniqueidentifier=null,
	@UserId uniqueidentifier
)  
AS  
/*================================================================================  
Procedure Name: [SFab_GetMakeMyMarketData]  
Author: Swami Aluri  
Create date: 07/03/2019  
Description: Get list of Make my market from table   
Change History  
Date        Developer     Reason  
__________  ____________  ______________________________________________________  
07/03/2019   Swami Aluri   Initial Version  
================================================================================*/  

BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
	
	Declare @CustomerId uniqueidentifier
	Set @CustomerId = dbo.fun_GetCustomerIdForLoggedInUser(@UserId)

	SELECT * 
	FROM MakeMyMarket 
	WHERE IsDeleted = 0
	AND ProjectId = @ProjectId
	AND TrendId = @TrendId
	AND CustomerId = @CustomerId
END