﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public static class TrendEcoSystemMapQueries
    {
        public static readonly string StoreProcInsert = "";

        public static readonly string StoreProcUpdate = "";

        public static readonly string StoreProcInsertUpdate = "SApp_InsertUpdateTrendEcoSystemMap";

        public static string StoreProcSelectAll = "SApp_GetEcoSystemByTrendID";

        public static string StoreProcSelectAllParent = "";
    }
}
