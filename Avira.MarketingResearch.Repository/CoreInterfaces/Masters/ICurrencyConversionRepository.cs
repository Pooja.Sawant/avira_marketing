﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.Masters
{
    public interface ICurrencyConversionRepository
    {
        IEnumerable<CurrencyConversionInsertDbViewModel> GetById(Guid guid);

        IEnumerable<CurrencyConversionInsertDbViewModel> GetByIdAll(CurrencyConversionInsertDbViewModel tfc);

        Result<SpTransactionMessage> Create(CurrencyConversionInsertDbViewModel viewModel);
    }
}
