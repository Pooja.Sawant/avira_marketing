﻿CREATE TABLE [dbo].[ResearchType] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [ResearchTypeName] NVARCHAR (256)   CONSTRAINT [DF_ResearchType_ResearchTypeName] DEFAULT ('') NOT NULL,
    [Description]      NVARCHAR (256)   NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_ResearchType_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_ResearchType] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ResearchType_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_ResearchType_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id])
);


GO

GO

GO

GO

GO

GO

GO