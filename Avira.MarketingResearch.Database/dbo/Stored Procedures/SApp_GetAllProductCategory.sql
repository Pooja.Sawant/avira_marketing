﻿

CREATE PROCEDURE [dbo].[SApp_GetAllProductCategory]
	(
		@includeDeleted bit = 0
	)
	AS
/*================================================================================
Procedure Name: [dbo].[SApp_GetAllProductCategory]
Author: Harshal
Create date: 03/19/2019
Description: Get list from Product Category table by Id
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
03/19/2019	Adarsh Dubey			Initial Version
================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT [ProductCategory].[Id],
		[ProductCategory].[ProductCategoryName],
		[ProductCategory].[Description],
		[ProductCategory].[IsDeleted]
		
FROM [dbo].[ProductCategory]
where  ([ProductCategory].IsDeleted = 0)
	
	END