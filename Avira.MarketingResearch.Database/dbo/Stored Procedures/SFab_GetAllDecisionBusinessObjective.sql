﻿CREATE PROCEDURE [dbo].[SFab_GetAllDecisionBusinessObjective]      
(    
 @DecisionId UNIQUEIDENTIFIER,
 @AviraUserId UNIQUEIDENTIFIER  
)  
As      
  
/*================================================================================      
Procedure Name: SFab_GetAllDecisionBusinessObjective     
Author: Pooja Sawant      
Create date: 24-Jan-2020  
Description: To return all Decision business objecive by decisionId of the user  
Change History      
Date  Developer  Reason    
__________ ____________ ______________________________________________________      
24-Jan-2020  Pooja S   Initial Version      
================================================================================*/      
BEGIN      
 
 declare @DecisionStatusId uniqueidentifier
select @DecisionStatusId = ID from LookupStatus
where StatusName = 'Decision made' and StatusType='DMDecisionStatus';



SELECT        DecisionObjective.Id, DecisionObjective.BusinessObjective
FROM            DecisionObjective inner JOIN
                         Decision ON DecisionObjective.DecisionId = Decision.Id

where Decision.Id=@DecisionId and Decision.UserId=@AviraUserId	   
       order by DecisionObjective.BusinessObjective
    
END