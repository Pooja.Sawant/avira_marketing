﻿CREATE procedure [dbo].[SApp_GetPermissionsByUserRoleId]    
(@UserRoleId uniqueidentifier)    
as    
/*================================================================================    
Procedure Name: [SApp_GetPermissionsByUserRolesId]    
Author: Gopi    
Create date: 04/10/2019    
Description: Get Permissions Based on logged in User Role
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
04/10/2019 Gopi   Initial Version    
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    
--Permission list
select 
Permission.Id as PermissionId,
Permission.PermissionName,
UserRolePermissionMap.PermissionLevel,
UserType
from Permission
inner join UserRolePermissionMap 
on Permission.Id = UserRolePermissionMap.PermissionId 
where UserRolePermissionMap.UserRoleId = @UserRoleId and UserRolePermissionMap.IsDeleted=0;

END