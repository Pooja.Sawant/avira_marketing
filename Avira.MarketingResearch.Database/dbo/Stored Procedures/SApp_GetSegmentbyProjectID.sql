﻿CREATE procedure [dbo].[SApp_GetSegmentbyProjectID]
(@ProjectId uniqueidentifier)
as
/*================================================================================
Procedure Name: SApp_GetSegmentbyProjectID
Author: Pooja
Create date: 04/26/2019
Description: Get list from segment table by project ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
04/26/2019	Pooja			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @IndustrySegmentId uniqueidentifier;
select @IndustrySegmentId = Id from Segment
where SegmentName = 'End User';

select STUFF((SELECT ','+ [Market].MarketName
        from ProjectSegmentMapping
        inner join [dbo].[Market]
        on ProjectSegmentMapping.SegmentID = Market.Id
        where SegmentType= 'Markets'
        and ProjectSegmentMapping.ProjectId = Project.Id
        FOR XML PATH('')), 1, 1, '')AS Markets,
STUFF((SELECT ','+ [Market].MarketName
        from ProjectSegmentMapping
        inner join [dbo].[Market]
        on ProjectSegmentMapping.SegmentID = Market.Id
        where ProjectSegmentMapping.SegmentType= 'Sub-Markets'
        and ProjectSegmentMapping.ProjectId = Project.Id
        FOR XML PATH('')), 1, 1, '')AS SubMarkets,
STUFF((SELECT ','+ SubSegment.SubSegmentName
        from ProjectSegmentMapping
        inner join [dbo].SubSegment
        on ProjectSegmentMapping.SegmentID = SubSegment.Id
        where SegmentType= 'Broad Industries'
		and SubSegment.SegmentId = @IndustrySegmentId
        and ProjectSegmentMapping.ProjectId = Project.Id
        FOR XML PATH('')), 1, 1, '')AS BroadIndustries,
STUFF((SELECT ','+ SubSegment.SubSegmentName
        from ProjectSegmentMapping
        inner join [dbo].SubSegment
        on ProjectSegmentMapping.SegmentID = SubSegment.Id
        where ProjectSegmentMapping.SegmentType= 'Industries'
		and SubSegment.SegmentId = @IndustrySegmentId
        and ProjectSegmentMapping.ProjectId = Project.Id
        FOR XML PATH('')), 1, 1, '')AS Industries
from Project
where Project.Id = @ProjectId



end