﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.CompanyProfile
{
    public class CompanyProductRepository: ICompanyProductRepository
    {
        protected IDbHelper _dbHelper;

        public CompanyProductRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }
        public IEnumerable<CompanyProductViewMoel> GetAll(Guid Id)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<CompanyProductViewMoel>(CompanyProductQueries.StoreProcSelectAll
                            , new { @CompanyID = Id }
                            , null, true, null, CommandType.StoredProcedure);
        }

        public Result<SpTransactionMessage> Insert(CompanyProductViewMoel companyProductViewMoel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(CompanyProductQueries.StoreProcInsert,
               new
               {
                   @Id= companyProductViewMoel.Id,
                   @ProductName = companyProductViewMoel.ProductName,
                   @Description = companyProductViewMoel.Description,
                   @Patents     = companyProductViewMoel.Patents,
                   @LaunchDate  = companyProductViewMoel.LaunchDate,
                   @ImageURL    =  companyProductViewMoel.ImageURL,
                   @ImageDisplayName =companyProductViewMoel.ImageDisplayName,
                   @ActualImageName = companyProductViewMoel.ImageActualName,
                   @StatusId   = companyProductViewMoel.ProductStatusId,
                   @CompanyId  = companyProductViewMoel.CompanyId,
                   @ProjectId = companyProductViewMoel.ProjectId,
                   @Amount = companyProductViewMoel.Amount,
                   @CurrencyId =companyProductViewMoel.CurrencyId,
                   @ValueConversionId =companyProductViewMoel.ValueConversionId,
                   @UserCreatedById = companyProductViewMoel.UserCreatedById
               }
                    , CommandType.StoredProcedure);
            return spTransactionMessage;
        }

        public Result<SpTransactionMessage> Update(CompanyProductViewMoel companyProductViewMoel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(CompanyProductQueries.StoreProcUpdate,
               new
               {
                   @Id = companyProductViewMoel.Id,
                   @ProductName = companyProductViewMoel.ProductName,
                   @Description = companyProductViewMoel.Description,
                   @Patents = companyProductViewMoel.Patents,
                   @LaunchDate = companyProductViewMoel.LaunchDate,
                   @ImageURL = companyProductViewMoel.ImageURL,
                   @ImageDisplayName = companyProductViewMoel.ImageDisplayName,
                   @ActualImageName = companyProductViewMoel.ImageActualName,
                   @StatusId = companyProductViewMoel.ProductStatusId,
                   @CompanyId = companyProductViewMoel.CompanyId,
                   @ProjectId = companyProductViewMoel.ProjectId,
                   @Amount = companyProductViewMoel.Amount,
                   @CurrencyId = companyProductViewMoel.CurrencyId,
                   @ValueConversionId = companyProductViewMoel.ValueConversionId,
                   @Year = companyProductViewMoel.Year,
                   @UserModifiedById = companyProductViewMoel.UserModifiedById
               }
                    , CommandType.StoredProcedure);
            return spTransactionMessage;
        }
    }
}
