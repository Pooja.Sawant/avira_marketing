﻿

CREATE procedure [dbo].[SApp_InsertUpdateTrendCountryMap]      
(
@TrendId uniqueidentifier,   
@TrendCountryMap dbo.[udtGUIDGUIDMapValue] readonly,
@AviraUserId uniqueidentifier
)      
as      
/*================================================================================      
Procedure Name: SApp_InsertTrendCountryMap      
Author: Swami      
Create date: 04/22/2019      
Description: Insert a record into TrendCountryMap table      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
04/22/2019 Swami   Initial Version    
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)  
    
     
BEGIN TRY         
--mark records as delete which are not in list
update [dbo].TrendCountryMap
set IsDeleted = 1,
UserDeletedById = @AviraUserId,
DeletedOn = GETDATE()
from [dbo].TrendCountryMap
where TrendCountryMap.TrendId = @TrendId
--and IsDeleted = 0
and not exists (select 1 from @TrendCountryMap map where map.id = TrendCountryMap.CountryId)


 --Update records for change
update [dbo].TrendCountryMap
set Impact = map.mapid,
EndUser = map.bitvalue,
ModifiedOn = GETDATE(),
UserModifiedById = @AviraUserId,
IsDeleted = 0
from [dbo].TrendCountryMap
inner join @TrendCountryMap map 
on map.id = TrendCountryMap.CountryId
where TrendCountryMap.TrendId = @TrendId;

--Insert new records
insert into [dbo].TrendCountryMap([Id],      
        [TrendId],      
        [CountryId], 
		[Impact],
		[EndUser],
        [IsDeleted],      
        [UserCreatedById],      
        [CreatedOn])      
 select NEWID(),
   @TrendId,       
   map.ID as [CountryId],
   map.mapid as [Impact],     
   map.bitvalue,     
   0,      
   @AviraUserId,       
   GETDATE()
 from @TrendCountryMap map
 where not exists (select 1 from [dbo].TrendCountryMap map1
					where map1.TrendId = @TrendId
					and map.ID = map1.CountryId);       
     
SELECT StoredProcedureName ='SApp_InsertUpdateTrendCountryMap',Message =@ErrorMsg,Success = 1;       
    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);      
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());      
    
 set @ErrorMsg = 'Error while insert a new TrendCountryMap, please contact Admin for more details.';    
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
       
END CATCH      
      
      
end