﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;

namespace Avira.MarketingResearch.Repository.Implementations
{
    public class RoleRepository : IRoleRepository
    {
        protected IDbHelper _dbHelper;
        public RoleRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }
        public Result<SpTransactionMessage> Create(RoleInsertDbViewModel roleInsertDbViewModel, AviraUser currentUser)
        {
            var spUserRoleMsg = _dbHelper.ExecuteQuery(RoleQueries.StoreProcInsert, new { @Id = roleInsertDbViewModel.UserRoleID, @UserRoleName = roleInsertDbViewModel.UserRoleName, @UserType = 1, @CreatedOn = roleInsertDbViewModel.CreatedOn, @UserCreatedById = currentUser.Id }, CommandType.StoredProcedure);
            if (spUserRoleMsg.IsFailure == true)
            {
                return spUserRoleMsg;
            }
            DataTable userPermissionIdsTable = new DataTable();
            userPermissionIdsTable.Columns.Add("ID", typeof(Guid));
            userPermissionIdsTable.Columns.Add("Value", typeof(int));

            if (roleInsertDbViewModel.UserPermisionID.Count >= 1 && roleInsertDbViewModel.PermissionLevel.Length >= 1)
            {
                for (int i = 0; i < roleInsertDbViewModel.UserPermisionID.Count; i++)
                {
                    DataRow row = userPermissionIdsTable.NewRow();
                    row["ID"] = roleInsertDbViewModel.UserPermisionID[i];
                    row["Value"] = roleInsertDbViewModel.PermissionLevel[i];
                    userPermissionIdsTable.Rows.Add(row);
                }
            }

            var spUserRolePermissionMsg = _dbHelper.ExecuteQuery(RoleQueries.StroreProcInsertPermissionmap, new { @UserRoleId = roleInsertDbViewModel.UserRoleID, @PermissionList = userPermissionIdsTable.AsTableValuedParameter("dbo.udtGUIDIntValue"), @UserCreatedById = currentUser.Id, @CreatedOn = roleInsertDbViewModel.CreatedOn }, CommandType.StoredProcedure);

            return spUserRolePermissionMsg;
        }

        public Result<SpTransactionMessage> Put(RoleUpdateViewModel roleInsertDbViewModel, AviraUser currentUser)
        {
            DataTable userPermissionIdsTable = new DataTable();
            userPermissionIdsTable.Columns.Add("ID", typeof(Guid));
            userPermissionIdsTable.Columns.Add("Value", typeof(int));

            if (roleInsertDbViewModel.UserPermisionID.Count >= 1 && roleInsertDbViewModel.PermissionLevel.Length >= 1)
            {
                for (int i = 0; i < roleInsertDbViewModel.UserPermisionID.Count; i++)
                {
                    DataRow row = userPermissionIdsTable.NewRow();
                    row["ID"] = roleInsertDbViewModel.UserPermisionID[i];
                    row["Value"] = roleInsertDbViewModel.PermissionLevel[i];
                    userPermissionIdsTable.Rows.Add(row);
                }
            }

            var spUserRolePermissionMsg = _dbHelper.ExecuteQuery(RoleQueries.StroreProcInsertPermissionmap, new { @UserRoleId = roleInsertDbViewModel.Id, @PermissionList = userPermissionIdsTable.AsTableValuedParameter("dbo.udtGUIDIntValue"), @UserCreatedById = currentUser.Id, @CreatedOn = roleInsertDbViewModel.CreatedOn }, CommandType.StoredProcedure);

            return spUserRolePermissionMsg;
        }

        public Result<SpTransactionMessage> Update(RoleUpdateViewModel roleInsertDbViewModel, AviraUser currentUser)
        {
            var spUserRolePermissionMsg = _dbHelper.ExecuteQuery(RoleQueries.StroreProcDeleteRolePermission, new
            {
                @Id = roleInsertDbViewModel.Id,
                @IsDeleted = roleInsertDbViewModel.IsDeleted,
                @UserDeletedById = roleInsertDbViewModel.UserDeletedById
            }, CommandType.StoredProcedure);

            return spUserRolePermissionMsg;
        }

        public IEnumerable<RoleViewModel> GetAllPermissions()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            var query = "SELECT * FROM [AviraDev].[dbo].[Permission] where IsDeleted = 0 order by PermissionName";
            return dapper.Query<RoleViewModel>(query);
        }

        public IEnumerable<RoleViewModel> Get(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude)
        {

            return Getallroles(null, PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude);
        }

        public IEnumerable<RoleViewModel> Getallroles(Guid? guid, int PageStart = 0, int pageSize = 0, int SortDirection = -1, string OrdbyByColumnName = "", string Search = "", bool includeDeleted = false)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);

            DataTable roleIdsTable = new DataTable();
            roleIdsTable.Columns.Add("ID", typeof(Guid));

            if (guid != null)
            {
                roleIdsTable.Rows.Add(guid);
            }

            return dapper.Query<RoleViewModel>(RoleQueries.StoreProcSelectAll, new { @UserRolesIDList = roleIdsTable.AsTableValuedParameter("dbo.udtUniqueIdentifier"), @UserRoleName = Search, @includeDeleted = false, @OrdbyByColumnName = @OrdbyByColumnName, @PageStart = PageStart, @pageSize = pageSize, @SortDirection = SortDirection }
                            , null, true, null, CommandType.StoredProcedure);
        }

        public IEnumerable<RoleEditViewModel> GetById(Guid guid)
        {
            return GetAllRole(guid);
        }

        private IEnumerable<RoleEditViewModel> GetAllRole(Guid guid)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<RoleEditViewModel>(RoleQueries.StoreProcSelectById
                            , new
                            {
                                @UserRoleId = guid
                            }
                            , null, true, null, CommandType.StoredProcedure);
        }
    }
}
