﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public static class ServiceQueries
    {
        public static readonly string StoreProcInsert = "SApp_insertService";

        public static readonly string StoreProcUpdate = "SApp_UpdateService";

        public static string StoreProcSelectAll = "SApp_ServiceGridWithFilters";

        public static string StoreProcSelectAllParent = "SApp_GetAllParentService";
    }
}
