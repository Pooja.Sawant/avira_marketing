﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.Helpers;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Avira.MarketingResearch.Common.IService.CompanyProfile
{
    public interface ICompanyShareVolumeService
    {
        Task<MainCompanyShareVolModel> GetShareVolume(string Id);
        Result<SpTransactionMessage> SaveImageInServer(MainCompanyShareVolModel Model, AviraUser currenUser);
        Result<SpTransactionMessage> InsertUpdate(CompanyShareVolumeListModel tfc, AviraUser currenUser);
        IEnumerable<CurrencyViewModel> GetAllCurrencies();
    }
}
