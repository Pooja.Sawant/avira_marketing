﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public static class TrendIndustryMappingQueries
    {
        public static readonly string StoreProcInsert = "SApp_InsertUpdateTrendIndustryMap"; 

        public static readonly string StoreProcSelectById = "SApp_GetIndustrybyTrendID";
    }
}
