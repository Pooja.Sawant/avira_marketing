﻿CREATE TYPE [dbo].[udtGUIDKeyEmpMultiValue] AS TABLE (
    [ID]                     UNIQUEIDENTIFIER NOT NULL,
    [KeyEmployeeName]        NVARCHAR (256)   NOT NULL,
    [KeyEmployeeDesignation] NVARCHAR (100)   NOT NULL,
    [KeyEmployeeEmailId]     NVARCHAR (256)   NOT NULL,
    [KeyEmployeeComments]    NVARCHAR (MAX)   NULL);

