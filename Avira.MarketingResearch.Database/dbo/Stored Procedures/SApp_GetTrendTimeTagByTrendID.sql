﻿
  
 CREATE PROCEDURE [dbo].[SApp_GetTrendTimeTagByTrendID]            
 (@TrendID uniqueidentifier = null,  
 @TimeTagName nvarchar(256)='',      
 @includeDeleted bit = 0,        
 @OrdbyByColumnName varchar(2) ='Seq',    
 @SortDirection INT = 1,        
 @PageStart INT = 0,        
 @PageSize INT = null)            
 AS            
/*================================================================================            
Procedure Name: SApp_GetCompanyGroupByTrendID      
Author: Swami            
Create date: 04/16/2019            
Description: Get list from Company Group table by TrendID            
Change History            
Date  Developer  Reason            
__________ ____________ ______________________________________________________            
04/23/2019  Jagan   Initial Version  
================================================================================*/           
            
BEGIN            
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED         
if @PageSize is null or @PageSize =0        
set @PageSize = 10        
 
if isnull(@TimeTagName,'') = '' set @TimeTagName = '' 
else set @TimeTagName = '%'+ @TimeTagName +'%';         

begin        
;WITH CTE_TBL AS (        
 SELECT         
 --@TrendID as TrendID,          
   [TimeTag].[Id] AS [TimeTagId],  
   [TimeTag].[TimeTagName],           
   [TimeTag].[Description],        
   [TimeTag].IsDeleted,        
   TrendTimeTag.Impact as ImpactId,      
   TrendTimeTag.EndUser,      
   TrendTimeTag.TrendId,      
   TrendTimeTag.Id as TrendTimeTagId, 
   [TimeTag].[Seq],     
  cast (case when exists(select 1 from dbo.TrendTimeTag         
     where TrendTimeTag.TrendId = @TrendID        
     and TrendTimeTag.IsDeleted = 0        
     and TrendTimeTag.TimeTagId = TimeTag.Id) then 1 else 0 end as bit) as IsMapped,        
 CASE        
  WHEN @OrdbyByColumnName = 'TimeTagName' THEN TimeTag.TimeTagName
  WHEN @OrdbyByColumnName = 'Description' THEN [TimeTag].[Description] 
  when @OrdbyByColumnName= 'Seq' THEN cast([TimeTag].[seq] as varchar(2))
  when @OrdbyByColumnName = 'IsMapped' THEN cast(case when TrendTimeTag.TrendId is null then 1 else 0 end as varchar(2))  
  ELSE cast([TimeTag].[seq] as varchar(2))
 END AS SortColumn        
        
FROM [dbo].TimeTag     
LEFT JOIN dbo.TrendTimeTag       
ON TimeTag.Id = TrendTimeTag.TimeTagId   
and TrendTimeTag.TrendId = @TrendID
and TrendTimeTag.IsDeleted = 0        
where (@TimeTagName = '' or [TimeTag].[TimeTagName] LIKE @TimeTagName)
AND (@includeDeleted = 1 or TimeTag.IsDeleted = 0)     
)            
        
 SELECT         
    CTE_TBL.*,        
    tCountResult.TotalItems        
  FROM CTE_TBL        
  CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult        
  ORDER BY         
    CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,        
    CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC         
  OFFSET @PageStart ROWS        
  FETCH NEXT @PageSize ROWS ONLY;        
  END        
        
END