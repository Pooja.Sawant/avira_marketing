﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public static class ProjectSegmentMappingQueries
    {
        public static readonly string StoreProcInsert = "SApp_insertProjectSegmentMapping";

        public static readonly string StoreProcUpdate = "SApp_UpdateProjectSegmentMapping";

        public static readonly string StoreProcSelectById = "[SApp_GetProjectSegmentMappingbyID]";

        public static readonly string StoreProcSelectAll = "SApp_ProjectSegmentMappingGridWithFilters";
        public static readonly string StoreProcUpdatePCode = "SApp_GenerateProjectCode";
    }
}
