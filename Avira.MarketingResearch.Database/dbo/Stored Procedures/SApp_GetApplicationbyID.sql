﻿CREATE procedure [dbo].[SApp_GetApplicationbyID]
(@ApplicationID uniqueidentifier,
@includeDeleted bit = 0)
as
/*================================================================================
Procedure Name: SApp_GetApplicationbyID
Author: Sharath
Create date: 02/19/2019
Description: Get list from Application table by Name or ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
02/19/2019	Sharath			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @SegmentId uniqueidentifier;
select @SegmentId = Id from Segment
where SegmentName = 'Applications';

if @includeDeleted is null set @includeDeleted = 0

SELECT [Id],
		SubSegmentName AS [ApplicationName],
		[Description]
FROM [dbo].[SubSegment]
where (@ApplicationID is null or ID = @ApplicationID)
and SegmentId = @SegmentId
and (@includeDeleted = 1 or IsDeleted = 0)



end