﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.CompanyProfile
{
    public class CompanyNoteViewModel
    {
       
            public Guid Id { get; set; }
            public Guid CompanyId { get; set; }
            public Guid? ProjectId { get; set; }
            public string CompanyType { get; set; }
            public string CompanyStatusName { get; set; }
            public string AuthorRemark { get; set; }
            public string ApproverRemark { get; set; }
            public string StatusName { get; set; }
            public DateTime CreatedOn { get; set; }
            public Guid UserCreatedById { get; set; }
            public DateTime? ModifiedOn { get; set; }
            public Guid? UserModifiedById { get; set; }
            public bool? IsDeleted { get; set; }
            public DateTime? DeletedOn { get; set; }
            public Guid? UserDeletedById { get; set; }
            public int TotalItems { get; set; }
            public string RowType
            {
                get
                {
                    return "CompanyNote";
                }
            }
      
        public class CompanyNoteInsertUpdateDbViewModel
        {
            public Guid Id { get; set; }
            public Guid CompanyId { get; set; }
            public Guid? ProjectId { get; set; }
            public string CompanyType { get; set; }
            public string CompanyStatusName { get; set; }
            public string Description { get; set; }
            public string ApproverRemark { get; set; }
            public string AuthorRemark { get; set; }
            public DateTime CreatedOn { get; set; }
            public Guid UserCreatedById { get; set; }
        }
    }
}
