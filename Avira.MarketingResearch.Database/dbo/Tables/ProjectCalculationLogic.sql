﻿CREATE TABLE [dbo].[ProjectCalculationLogic] (
    [ID]                          UNIQUEIDENTIFIER NOT NULL,
    [ProjectID]                   UNIQUEIDENTIFIER NOT NULL,
    [CalculationLogicDescription] NVARCHAR (4000)  NOT NULL,
    [CreatedOn]                   DATETIME2 (7)    NULL,
    [UserCreatedById]             UNIQUEIDENTIFIER NULL,
    [ModifiedOn]                  DATETIME2 (7)    NULL,
    [UserModifiedById]            UNIQUEIDENTIFIER NULL,
    [IsDeleted]                   BIT              NULL,
    [DeletedOn]                   DATETIME2 (7)    NULL,
    [UserDeletedById]             UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_ProjectCalculationLogic] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_ProjectCalculationLogic_Project] FOREIGN KEY ([ProjectID]) REFERENCES [dbo].[Project] ([ID])
);

