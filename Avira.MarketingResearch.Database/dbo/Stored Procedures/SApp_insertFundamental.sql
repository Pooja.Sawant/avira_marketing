﻿
CREATE procedure [dbo].[SApp_insertFundamental]    
(    
@FundamentalName nvarchar(256),    
@Description nvarchar(256),    
@UserCreatedById uniqueidentifier,  
@Id uniqueidentifier,  
@CreatedOn Datetime
)    
as    
/*================================================================================    
Procedure Name: SApp_insertFundamental    
Author: Adarsh    
Create date: 04/02/2019    
Description: Insert a record into Fundamental table    
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
04/02/2019 Adarsh   Initial Version    
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    
declare @ErrorMsg NVARCHAR(2048)
  
--Validations    
IF exists(select 1 from [dbo].[Fundamental]
   where FundamentalName = @FundamentalName AND IsDeleted = 0)    
begin    
--declare @ErrorMsg NVARCHAR(2048);    
set @ErrorMsg = 'Fundamental with Name "'+ @FundamentalName + '" already exists.';    
--THROW 50000,@ErrorMsg,1;    
--return(1)    
SELECT StoredProcedureName ='SApp_insertFundamental',Message =@ErrorMsg,Success = 0;  
RETURN;  
end    
--End of Validations    
BEGIN TRY       
       
insert into [dbo].[Fundamental]([Id],    
        [FundamentalName],    
        [Description],    
        [IsDeleted],    
        [UserCreatedById],    
        [CreatedOn])    
 values(@Id,     
   @FundamentalName,     
   @Description,     
   0,    
   @UserCreatedById,     
   @CreatedOn);    
    
SELECT StoredProcedureName ='SApp_insertFundamental',Message =@ErrorMsg,Success = 1;     
  
END TRY    
BEGIN CATCH    
    -- Execute the error retrieval routine.    
 DECLARE @ErrorNumber int;    
 DECLARE @ErrorSeverity int;    
 DECLARE @ErrorProcedure varchar(100);    
 DECLARE @ErrorLine int;    
 DECLARE @ErrorMessage varchar(500);    
    
  SELECT @ErrorNumber = ERROR_NUMBER(),    
        @ErrorSeverity = ERROR_SEVERITY(),    
        @ErrorProcedure = ERROR_PROCEDURE(),    
        @ErrorLine = ERROR_LINE(),    
        @ErrorMessage = ERROR_MESSAGE()    
    
 insert into dbo.Errorlog(ErrorNumber,    
       ErrorSeverity,    
       ErrorProcedure,    
       ErrorLine,    
       ErrorMessage,    
       ErrorDate)    
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,@CreatedOn);    
  
 set @ErrorMsg = 'Error while creating a new Fundamental, please contact Admin for more details.';    
--THROW 50000,@ErrorMsg,1;      
--return(1)  
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;     
     
END CATCH    
    
    
end