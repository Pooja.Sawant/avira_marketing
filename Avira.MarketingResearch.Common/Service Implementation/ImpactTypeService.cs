﻿using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation
{
    public class ImpactTypeService : IImpactTypeService
    {
        private readonly IImpactTypeRepository _iImpactTypeRepository;

        public ImpactTypeService(IImpactTypeRepository iImpactTypeRepository)
        {
            _iImpactTypeRepository = iImpactTypeRepository;
        }
        public IEnumerable<ImpactTypeViewModel> GetAll()
        {
            return _iImpactTypeRepository.GetAll();
        }
    }
}
