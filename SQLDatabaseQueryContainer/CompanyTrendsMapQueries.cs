﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public class CompanyTrendsMapQueries
    {
        public static readonly string StoreProcInsert = "SApp_InsertUpdateCompanyTrendsMap";

        public static readonly string StoreProcSelectById = "SApp_GetCompanyTrendsMapByCompanyID";
    }
}
