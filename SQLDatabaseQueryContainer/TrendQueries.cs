﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public static class TrendQueries
    {
        public static readonly string StoreProcInsert = "SApp_insertTrend";

        public static readonly string StoreProcUpdate = "SApp_UpdateTrend";

        public static string StoreProcSelectAllTrendByProjectId = "SApp_GetTrendsbyProjectID";

        public static string StoreProcSelectTrendByTrendId = "SApp_GetTrendsbyTrendID";

        public static string StoreProcSelectAllParent = "";
    }
}
