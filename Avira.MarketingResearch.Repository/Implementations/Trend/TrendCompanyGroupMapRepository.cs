﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.Trend
{
    public class TrendCompanyGroupMapRepository : ITrendCompanyGroupMapRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "TrendCompanyGroupMapping";

        public TrendCompanyGroupMapRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }
        /// <summary>
        /// To get all market with mapped/unmapped status by trend id
        /// </summary>
        /// <param name="TrendIdint"></param>
        /// <param name="PageStart"></param>
        /// <param name="pageSize"></param>
        /// <param name="SortDirection"></param>
        /// <param name="OrdbyByColumnName"></param>
        /// <param name="Search"></param>
        /// <param name="isDeletedInclude"></param>
        /// <returns></returns>
        public IEnumerable<TrendCompanyGroupMapViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search = "", bool isDeletedInclude = false, string id = "")
        {
            TrendCompanyGroupMapViewModel trnd = new TrendCompanyGroupMapViewModel();
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<TrendCompanyGroupMapViewModel>(TrendCompanyGroupMapQueries.StoreProcSelectAll
                            , new
                            {
                                @CompanyGroupName = Search,
                                @TrendID = id,
                                @includeDeleted = false,
                                @OrdbyByColumnName = @OrdbyByColumnName,
                                @PageStart = PageStart,
                                @pageSize = pageSize,
                                @SortDirection = SortDirection
                            }
                            , null, true, null, CommandType.StoredProcedure);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="trendCompanyGroupMapUpdateDbViewModel"></param>
        /// <returns></returns>
        public Result<SpTransactionMessage> InsertUpdate(MainTrendCompanyGroupMapInsertUpdateDbViewModel trendCompanyGroupMapUpdateDbViewModel)
        {

            DataTable trendCompanyGroupMapTable = new DataTable();
            trendCompanyGroupMapTable.Columns.Add("ID", typeof(Guid));
            trendCompanyGroupMapTable.Columns.Add("MapId", typeof(Guid));
            trendCompanyGroupMapTable.Columns.Add("bitValue", typeof(bool));
            if (trendCompanyGroupMapUpdateDbViewModel.TrendCompanyGroupMapInsertUpdate != null & trendCompanyGroupMapUpdateDbViewModel.TrendCompanyGroupMapInsertUpdate.Count > 0)
            {
                foreach (var item in trendCompanyGroupMapUpdateDbViewModel.TrendCompanyGroupMapInsertUpdate)//.Where(x => x.ImpactId != Guid.Empty)
                {
                    trendCompanyGroupMapTable.Rows.Add(new Object[] { item.CompanyGroupId, item.ImpactId, item.EndUser });
                }
            }

            var spTransactionMessage = _dbHelper.ExecuteQuery(TrendCompanyGroupMapQueries.StoreProcInsertUpdate,
               new
               {
                   @TrendId = trendCompanyGroupMapUpdateDbViewModel.TrendId,
                   @TrendCompanyGroupmap = trendCompanyGroupMapTable,
                   @UserCreatedById = trendCompanyGroupMapUpdateDbViewModel.UserCreatedById,
               }
                    , CommandType.StoredProcedure);
            return spTransactionMessage;
        }
    }
}
