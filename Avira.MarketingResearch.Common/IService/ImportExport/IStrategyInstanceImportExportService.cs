﻿using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.ImportExport
{
    public interface IStrategyInstanceImportExportService
    {
        IEnumerable<ImportCommonResponseModel> Create(StrategyIntanceImportModel viewModel);
    }
}
