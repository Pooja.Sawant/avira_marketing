﻿      
      
CREATE PROCEDURE [dbo].[SApp_GetImportance]      
 (      
  @includeDeleted bit = 0      
 )      
 AS      
/*================================================================================      
Procedure Name: [SApp_GetImportance]      
Author: Gopi      
Create date: 05/03/2019      
Description: Get list of Importance from table       
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
05/03/2019 Gopi   Initial Version      
================================================================================*/      
      
BEGIN      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED      
      
 SELECT [Importance].[Id],      
  [Importance].[ImportanceName],      
  [Importance].[Description],      
  [Importance].[IsDeleted]      
FROM [dbo].[Importance]      
where ([Importance].IsDeleted = 0)       
 END