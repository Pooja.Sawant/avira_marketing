﻿CREATE TABLE [dbo].[StagingQualitativeQuote] (
    [Id]                    UNIQUEIDENTIFIER NOT NULL,
    [ImportId]              UNIQUEIDENTIFIER NULL,
    [RowNo]                 INT              NULL,
    [MarketName]            NVARCHAR (255)   NULL,
    [SubMarketName]         NVARCHAR (255)   NULL,
    [ResourceName]          NVARCHAR (255)   NULL,
    [ResourceCompany]       NVARCHAR (255)   NULL,
    [ResourceDesignation]   NVARCHAR (255)   NULL,
    [DateOfQuote]           DATETIME         NULL,
    [Quote]                 NVARCHAR (1000)  NULL,
    [OtherRemarks]          NVARCHAR (1000)  NULL,
    [MarketId]              UNIQUEIDENTIFIER NULL,
    [SubMarketId]           UNIQUEIDENTIFIER NULL,
    [ProjectId]             UNIQUEIDENTIFIER NULL,
    [CompanyId]             UNIQUEIDENTIFIER NULL,
    [ResourceDesignationId] UNIQUEIDENTIFIER NULL,
    [ErrorNotes]            NVARCHAR (1000)  NULL,
    CONSTRAINT [PK_StagingQualitativeQuote] PRIMARY KEY CLUSTERED ([Id] ASC)
);



