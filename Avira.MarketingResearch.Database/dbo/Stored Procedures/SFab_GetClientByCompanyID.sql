﻿-- =============================================        
-- Author:  Jagan        
-- Create date: 27/06/2019        
-- Description: Get Company by CompanyId         
-- =============================================        
CREATE PROCEDURE [dbo].[SFab_GetClientByCompanyID]             
 @ComapanyID uniqueidentifier = null,         
 @includeDeleted bit = 0,
 @AviraUserID uniqueidentifier = null            
AS        
BEGIN         
 SET NOCOUNT ON;          
 SELECT [CompanyClientsMapping].[Id]        
      ,[CompanyId]    
      ,[ClientName]        
      ,[ClientIndustry]      
	,Industry.SubSegmentName As IndustryName        
      ,[ClientRemark]        
      ,[CompanyClientsMapping].[CreatedOn]        
      ,[CompanyClientsMapping].[UserCreatedById]        
      ,[CompanyClientsMapping].[ModifiedOn]        
      ,[CompanyClientsMapping].[UserModifiedById]        
      ,[CompanyClientsMapping].[IsDeleted]        
      ,[CompanyClientsMapping].[DeletedOn]        
      ,[CompanyClientsMapping].[UserDeletedById]        
  FROM [dbo].[CompanyClientsMapping]    
  --LEFT JOIN [dbo].[Industry] Industry ON [CompanyClientsMapping].ClientIndustry = Industry.Id 
  LEFT JOIN [dbo].SubSegment [Industry] ON [CompanyClientsMapping].ClientIndustry = Industry.Id 
INNER JOIN Segment ON [Industry].SegmentId = Segment.Id AND Segment.SegmentName = 'End User'
       
  WHERE ([CompanyClientsMapping].CompanyId = @ComapanyID)              
 AND (@includeDeleted = 0 or [CompanyClientsMapping].IsDeleted = 0)          
END