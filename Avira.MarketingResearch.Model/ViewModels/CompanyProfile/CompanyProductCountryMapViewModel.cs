﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.CompanyProfile
{
        public class CompanyProductCountryMapViewModel
        {
            public Guid Id { get; set; }
            public Guid CompanyProductId { get; set; }
            public Guid ProductCountryId { get; set; }
            public string ProductCountryName { get; set; }
            public bool IsMapped { get; set; }
        }

        public class MainCompanyProductCountryMapInsertUpdateDbViewModel
        {
            public Guid CompanyProductId { get; set; }
            public string ProductCountryName { get; set; }
            public List<CompanyProductCountryMapViewModel> CompanyProductCountryMapInsertUpdate { get; set; }
            public Guid AviraUserId { get; set; }
            public DateTime CreatedOn { get; set; }
            public Guid UserCreatedById { get; set; }
        }
    
}
