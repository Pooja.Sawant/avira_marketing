﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.Trend
{
    public interface ITrendNoteRepository
    {
        TrendNoteViewModel GetById(Guid guid, string trendType);
        IEnumerable<TrendNoteViewModel> GetAllByTrendId(Guid guid, string StatusName);
        Result<SpTransactionMessage> Insert(TrendNoteViewModel viewModel);
    }
}
