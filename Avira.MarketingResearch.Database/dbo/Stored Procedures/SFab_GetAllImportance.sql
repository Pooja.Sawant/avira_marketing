﻿
  
CREATE PROCEDURE [dbo].[SFab_GetAllImportance] 
(@UserId uniqueidentifier = null)   
 AS  
/*================================================================================  
Procedure Name: [SFab_GetAllImportance]  
Author: Pooja Sawant  
Create date: 05/25/2019  
Description: Get list of conversion values from table  
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
05/25/2019 Pooja   Initial Version  
================================================================================*/  
  
BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
	
	SELECT Id as ImportanceId, ImportanceName,GroupName, [Description] AS ImportanceDescription FROM Importance
	WHERE IsDeleted = 0 
	order by sequence asc

END