﻿
CREATE PROCEDURE [dbo].[SFab_GetTrendListProjectId]
(
@SegmentID uniqueidentifier=null,
@ProjectID uniqueidentifier=null,
@AviraUserID uniqueidentifier = null,
@SegmentIdList dbo.[udtUniqueIdentifier] readonly
)
AS
/*Make chnages by Pooja add importancecolorcode column 18/06/2019*/
BEGIN


	Declare @SegmentType NVARCHAR(100);
	declare @TrendApprovedStatusId uniqueidentifier;

	select @TrendApprovedStatusId = Id
	from TrendStatus
	where TrendStatusName = 'Approved';


	SELECT @SegmentType = SegmentName FROM Segment
	WHERE Segment.Id = @SegmentID

	CREATE table #tmp_Segment (SegmentId uniqueidentifier,
						SegmentName nvarchar(256));
	INSERT INTO #tmp_Segment
	SELECT Id AS SegmentId,
		SubSegmentName AS SubSegmentName
		FROM SubSegment
		WHERE (id IN (SELECT id FROM @SegmentIdList) OR not exists(select 1 from @SegmentIdList))
		--AND @SegmentType = 'Raw materials'

		--UNION ALL

		----SELECT Id AS SegmentId,
		----IndustryName AS SegmentName
		----FROM prod
		----WHERE (id IN (SELECT id FROM @SegmentIdList) OR not exists(select 1 from @SegmentIdList ))
		----AND @SegmentType = 'Product'

		----UNION ALL

		--SELECT Id AS SegmentId,
		--ApplicationName AS SegmentName
		--FROM [Application]
		--WHERE (id IN (SELECT id FROM @SegmentIdList) OR not exists(select 1 from @SegmentIdList ))
		--AND @SegmentType = 'Applications'

	
	SELECT Trend.Id,
			Trend.TrendName,
			Trend.ImportanceId,
			imp.ImportanceName,
			isnull(Trend.TrendValue,0) AS ImpactAmount,CASE WHEN imp.GroupName = 'High' THEN 'success' WHEN imp.GroupName = 'Mid' THEN 'yellow' WHEN imp.GroupName = 'Low' THEN 'red' ELSE 'red' END AS ImportanceColorCode
	FROM Trend
	LEFT JOIN Importance imp ON Trend.ImportanceId = imp.Id	

	WHERE Trend.ProjectID = @ProjectID AND Trend.TrendStatusID=@TrendApprovedStatusId
	AND ((@SegmentType IN ('Raw materials','Product','Applications')AND EXISTS (SELECT 1 FROM TrendKeywords
				INNER JOIN #tmp_Segment LIST
				ON TrendKeywords.TrendKeyWord LIKE '%'+ LIST.SegmentName + '%'
				AND TrendKeywords.TrendId = Trend.Id))
		OR (@SegmentType = 'End User') AND EXISTS (SELECT 1 FROM TrendIndustryMap
													INNER JOIN @SegmentIdList LIST
													ON TrendIndustryMap.IndustryId = LIST.ID
													where TrendIndustryMap.TrendId = trend.Id)
		OR (@SegmentType = 'Region') AND EXISTS (SELECT 1 FROM TrendRegionMap
													INNER JOIN @SegmentIdList LIST
													ON TrendRegionMap.RegionId = LIST.ID
													where TrendRegionMap.TrendId = Trend.Id)
	)
	
END