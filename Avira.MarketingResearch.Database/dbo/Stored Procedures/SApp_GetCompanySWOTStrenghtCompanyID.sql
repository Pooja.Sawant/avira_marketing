﻿--sp_helptext SApp_GetCompanySWOTStrenghtCompanyID

-- =============================================            
-- Author:  Laxmikant            
-- Create date: 05/15/2019            
-- Description: Get Company swot Strenght by CompanyId  
-- exec SApp_GetCompanySWOTStrenghtCompanyID 'f0fa2269-04f9-44c5-9bea-3aa8bb4bc4a4',0             
-- =============================================            
CREATE PROCEDURE dbo.SApp_GetCompanySWOTStrenghtCompanyID   
 @ComapanyID uniqueidentifier = null,             
 @includeDeleted bit = 0                 
AS  
BEGIN      
SET NOCOUNT ON;    
 SELECT [dbo].[CompanySwotStrengthMap].[Id]  
      ,[dbo].[CompanySwotStrengthMap].[CompanyId]  
      ,[dbo].[CompanySwotStrengthMap].[CategoryName]  
      ,[dbo].[CompanySwotStrengthMap].[Description]  
      ,[dbo].[CompanySwotStrengthMap].[CreatedOn]  
      ,[dbo].[CompanySwotStrengthMap].[UserCreatedById]  
      ,[dbo].[CompanySwotStrengthMap].[ModifiedOn]  
      ,[dbo].[CompanySwotStrengthMap].[UserModifiedById]  
      ,[dbo].[CompanySwotStrengthMap].[IsDeleted]  
      ,[dbo].[CompanySwotStrengthMap].[DeletedOn]  
      ,[dbo].[CompanySwotStrengthMap].[UserDeletedById]  
  FROM [dbo].[CompanySwotStrengthMap]     
  LEFT JOIN  Category Category ON CompanySwotStrengthMap.CategoryName = Category.Id          
  WHERE (CompanySwotStrengthMap.CompanyId = @ComapanyID                  
  AND (@includeDeleted = 0 or [dbo].[CompanySwotStrengthMap].IsDeleted = 0))     
END