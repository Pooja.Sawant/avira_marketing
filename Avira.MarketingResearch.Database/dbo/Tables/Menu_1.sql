﻿CREATE TABLE [dbo].[Menu] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [MenuName]         NVARCHAR (256)   CONSTRAINT [DF_Menu_MenuName] DEFAULT ('') NOT NULL,
    [ParentMenuID]     UNIQUEIDENTIFIER NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_Menu_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_Menu] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Menu_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_Menu_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_Menu_ParentMenu] FOREIGN KEY ([ParentMenuID]) REFERENCES [dbo].[Menu] ([Id]),
    CONSTRAINT [IX_Menu] UNIQUE NONCLUSTERED ([MenuName] ASC)
);



