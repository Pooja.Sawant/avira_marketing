﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile
{
    public interface INewsRepository
    {
        IEnumerable<NewsViewModel> GetAll(Guid companyId, Guid projectId);
        IEnumerable<NewsCategoryViewModel> GetAllNewsCategory();
        IEnumerable<NewsImportanceViewModel> GetAllImportance();
        IEnumerable<NewsImpactDirectionViewModel> GetAllImpactDirection();
        Result<SpTransactionMessage> InsertUpdate(MainNewsInsertUpdateDbViewModel newsUpdateDbViewModel);
    }
}
