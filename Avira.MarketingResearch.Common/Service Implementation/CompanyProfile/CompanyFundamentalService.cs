﻿using System;
using System.Collections.Generic;
using Avira.MarketingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Avira.MarketingResearch.Utility;

namespace Avira.MarketingResearch.Common.Service_Implementation.CompanyProfile
{
    public class CompanyFundamentalService : ICompanyFundamentalService
    {
        private readonly ICompanyFundamentalRepository _companyFundamentalRepository;
        private readonly ITrendNoteRepository _iTrendNoteRepository;
        private readonly IConfiguration _configuration;
        private readonly IHostingEnvironment _hostingEnvironment;

        public CompanyFundamentalService(ICompanyFundamentalRepository companyFundamentalRepository, ITrendNoteRepository iTrendNoteRepository, IConfiguration configuration, IHostingEnvironment hostingEnvironment)
        {
            _companyFundamentalRepository = companyFundamentalRepository;
            _iTrendNoteRepository = iTrendNoteRepository;
            _configuration = configuration;
            _hostingEnvironment = hostingEnvironment;
        }

        public IEnumerable<CompanyFundamentalViewModel> GetAll()
        {
            return _companyFundamentalRepository.GetAll();
        }

        public IEnumerable<CompanyRegionViewModel> GetAllRegion()
        {
            return _companyFundamentalRepository.GetAllRegion();
        }

        public IEnumerable<CompanyEntityViewModel> GetAllEntities()
        {
            return _companyFundamentalRepository.GetAllEntities();
        }

        public IEnumerable<CompanyManagerViewModel> ManagersList(string companyId)
        {
            return _companyFundamentalRepository.ManagersList(companyId);
        }

        public IEnumerable<CompanySubsidaryViewModel> GetCompanySubsidaries(Guid Id)
        {
            return _companyFundamentalRepository.GetCompanySubsidaries(Id);
        }
        //public IEnumerable<CompanyDetailsViewModel> GetCompanyDetails(Guid CompanyId, Guid ProjectId)
        //{
        //    return _companyFundamentalRepository.GetCompanyDetails(CompanyId, ProjectId);
        //}

        public Task<CompanyDetailsInsertUpdateViewModel> GetCompanyDetails(Guid CompanyId, Guid ProjectId)
        {
            return _companyFundamentalRepository.GetCompanyDetails(CompanyId, ProjectId);
        }

        public IEnumerable<AllCompaniesListViewModel> GetAllCompaniesList()
        {
            return _companyFundamentalRepository.GetAllCompaniesList();
        }

        public IEnumerable<DesignationViewModel> AllDesignations()
        {
            return _companyFundamentalRepository.AllDesignations();
        }

        public IEnumerable<AllCompanyStageListViewModel> GetAllCompanyStageList()
        {
            return _companyFundamentalRepository.GetAllCompanyStageList();
        }

        public Result<SpTransactionMessage> InsertUpdate(CompanyDetailsInsertUpdateViewModel tfc, AviraUser currenUser)
        {
            if (tfc.fileByte != null)
            {
                var path = _configuration.GetSection("AppUploadPath").GetSection("Company").GetSection("CompanyLogo").Value;
                var fullPath = Path.Combine(_hostingEnvironment.ContentRootPath, path);
                var newFileName = FileUpload.ByteArrayToFile(tfc.ImageDisplayName, tfc.fileByte, fullPath, tfc.fileExtension);
                if (!string.IsNullOrEmpty(newFileName))
                {
                    tfc.ImageActualName = newFileName;
                    tfc.ImageURL = path + newFileName;
                }
            }



            var res = _companyFundamentalRepository.InsertUpdate(tfc, currenUser);
            return res;
        }

        public IEnumerable<CompanyNatureViewModel> NaturesList()
        {
            return _companyFundamentalRepository.NaturesList();
        }
    }
}
