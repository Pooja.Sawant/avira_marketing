﻿using Avira.MarketingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.Trend
{
    public class TrendKeywordMapService : ITrendKeywordMapService
    {
        private readonly ITrendKeywordMapRepository _trendKeywordMapRepository;
        private readonly ITrendNoteRepository _iTrendNoteRepository;


        public TrendKeywordMapService(ITrendKeywordMapRepository trendKeywordMapRepository, ITrendNoteRepository iTrendNoteRepository)
        {
            _trendKeywordMapRepository = trendKeywordMapRepository;
            _iTrendNoteRepository = iTrendNoteRepository;
        }
        public Result<SpTransactionMessage> Create(TrendKeyWordMapInsertDbViewModel1 tfc, AviraUser currentUser)
        {
            var res = _trendKeywordMapRepository.Create(tfc);
            if (res.IsSuccess == true)
            {
                //string tnoteId = tfc.TrendNoteModel.Id.ToString();
                //Guid trendNoteId = new Guid(tnoteId);
                //if (tnoteId == "00000000-0000-0000-0000-000000000000")
                //{
                //    tfc.TrendNoteModel.Id = Guid.NewGuid();
                //    tfc.TrendNoteModel.UserCreatedById = tfc.UserCreatedById;
                //    tfc.TrendNoteModel.TrendId = tfc.TrendId;
                //    _iTrendNoteRepository.Insert(tfc.TrendNoteModel);
                //}
                //else
                //{
                //    tfc.TrendNoteModel.Id = tfc.TrendNoteModel.Id;
                //    tfc.TrendNoteModel.UserCreatedById = tfc.UserCreatedById;
                //    tfc.TrendNoteModel.TrendId = tfc.TrendId;
                //    _iTrendNoteRepository.Insert(tfc.TrendNoteModel);

                tfc.TrendNoteModel.Id = Guid.NewGuid();
                tfc.TrendNoteModel.UserCreatedById = tfc.UserCreatedById;
                tfc.TrendNoteModel.TrendId = tfc.TrendId;
                _iTrendNoteRepository.Insert(tfc.TrendNoteModel);
                //}

            }
            return res;
        }

        public IEnumerable<TrendKeyWordMapViewModel> GetAll(Guid? TrendId, int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TrendKeyWordMapViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false)
        {
            return _trendKeywordMapRepository.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude);
        }

        //public IEnumerable<TrendViewModel> GetAllTrendNames()
        //{
        //    return _trendKeywordMapRepository.GetAllTrendNames();
        //}

        public IEnumerable<TrendKeywordNames> GetAllTrendNames()
        {
            return _trendKeywordMapRepository.GetAllTrendNames();
        }

        public IEnumerable<TrendKeyWordMapViewModel> GetById(Guid guid)
        {
            return _trendKeywordMapRepository.GetById(guid);
        }
        public TrendKeywordNames GetTrendNameByID(Guid guid)
        {
            return _trendKeywordMapRepository.GetTrendNameByID(guid);
        }


        public Result<SpTransactionMessage> Update(TrendKeyWordMapUpdateDbViewModel tfc, AviraUser currenUser)
        {
            return _trendKeywordMapRepository.Update(tfc);
        }
    }
}
