﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.CompanyProfile
{
    public class EcoSystemPresenceMapViewModel
    {
        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
        public Guid EcoSystemId { get; set; }
        public string EcoSystemName { get; set; }
        public bool Ischecked { get; set; }
        public bool IsMapped { get; set; }
        public string RowType
        {
            get
            {
                return "EcoSystemPresenceMap";
            }
        }
    }

    public class SectorPresenceMapViewModel
    {
        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
        public Guid IndustryId { get; set; }
        public string IndustryName { get; set; }
        public bool Ischecked { get; set; }
        public bool IsMapped { get; set; }
        public string RowType
        {
            get
            {
                return "SectorPresenceMap";
            }
        }
    }

    public class EcoSystemPresenceKeyCompViewModel
    {
        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
        public string KeyCompetiters { get; set; }
        public bool IsMapped { get; set; }
        public string RowType
        {
            get
            {
                return "EcoSystemPresenceKeyComp";
            }
        }
    }

  
    public class EcoSystemPresenceDbViewModel
    {
        public Guid CompanyId { get; set; }
        public Guid ProjectId { get; set; }
        public List<EcoSystemPresenceMapViewModel> EcoSystemPresences { get; set; }
        public List<SectorPresenceMapViewModel> SectorPresences { get; set; }
        public List<EcoSystemPresenceKeyCompViewModel> KeyCompetiters { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public CompanyNoteViewModel CompanyNoteModel { get; set; }
    }

}
