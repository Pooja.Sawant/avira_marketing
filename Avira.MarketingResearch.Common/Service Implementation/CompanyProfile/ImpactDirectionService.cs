﻿using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.CompanyProfile
{
    public class ImpactDirectionService : IImpactDirectionService
    {
        private readonly IImpactDirectionRepository _iImpactDirectionRepository;

        public ImpactDirectionService(IImpactDirectionRepository iImpactDirectionRepository)
        {
            _iImpactDirectionRepository = iImpactDirectionRepository;
        }
        public IEnumerable<ImpactDirectionViewModel> GetAll()
        {
            return _iImpactDirectionRepository.GetAll();
        }
    }
}
