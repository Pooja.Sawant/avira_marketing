﻿CREATE procedure [dbo].[SApp_GetAllCompanyStage]

as
/*================================================================================
Procedure Name: [SApp_GetAllCompanyStage]
Author: Sai Krishna
Create date: 09/05/2019
Description: Get list from Company Stage
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
09/05/2019	Sai Krishna		Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT [Id], 
		[CompanyStageName]
	FROM [dbo].[CompanyStage]
where ([CompanyStage].[IsDeleted] = 0)  order by CompanyStageName
end