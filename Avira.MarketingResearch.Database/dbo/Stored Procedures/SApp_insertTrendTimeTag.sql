﻿



CREATE procedure [dbo].[SApp_insertTrendTimeTag]      
(
@TrendId uniqueidentifier,   
@TrendTimeTag dbo.[udtGUIDGUIDMapValue] readonly,
@UserCreatedById uniqueidentifier
)      
as      
/*================================================================================      
Procedure Name: SApp_InsertTrendMarketMap      
Author: Swami      
Create date: 04/12/2019      
Description: Insert a record into TrendTimeTag table      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
04/24/2019 Jagan Initial Version    
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)  
    
--Validations      
--IF exists(select 1 from [dbo].[TrendMarketMap]       
--   where TrendId = @TrendId AND MarketId = @MarketId AND IsDeleted = 0)      
--begin       
--set @ErrorMsg = 'Market with Id "'+ CONVERT (nvarchar(30), @MarketId) + '" already exists.';    
--SELECT StoredProcedureName ='SApp_InsertTrendMarketMap',Message =@ErrorMsg,Success = 0;    
--RETURN;    
--end      
--End of Validations      
BEGIN TRY         
--mark records as delete which are not in list
update [dbo].[TrendTimeTag]
set IsDeleted = 1,
UserDeletedById = @UserCreatedById,
DeletedOn = GETDATE()
from [dbo].[TrendTimeTag]
where [TrendTimeTag].TrendId = @TrendId
and not exists (select 1 from @TrendTimeTag map where map.id = [TrendTimeTag].TimeTagId)

 --Update records for change
update [dbo].[TrendTimeTag]
set Impact = map.mapid,
EndUser = map.bitvalue,
IsDeleted = 0,
DeletedOn = null,
ModifiedOn = GETDATE(),
UserModifiedById = @UserCreatedById
from [dbo].[TrendTimeTag]
inner join @TrendTimeTag map 
on map.id = [TrendTimeTag].TimeTagId
where [TrendTimeTag].TrendId = @TrendId;

--Insert new records
insert into [dbo].[TrendTimeTag]([Id],      
        [TrendId],      
        [TimeTagId], 
		[Impact],
		[EndUser],
        [IsDeleted],      
        [UserCreatedById],      
        [CreatedOn])      
 select NEWID(),
   @TrendId,       
   map.ID as [TimeTagId],
   map.mapid as [Impact],     
   map.bitvalue,     
   0,      
   @UserCreatedById,       
   GETDATE()
 from @TrendTimeTag map
 where not exists (select 1 from [dbo].[TrendTimeTag] map1
					where map1.TrendId = @TrendId
					and map.ID = map1.TimeTagId);       
     
SELECT StoredProcedureName ='SApp_InsertTrendTimeTag',Message =@ErrorMsg,Success = 1;       
    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);      
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());      
    
 set @ErrorMsg = 'Error while insert a new TrendMarketMap, please contact Admin for more details.';    
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
       
END CATCH      
      
      
end