﻿      
CREATE PROCEDURE [dbo].[SFab_GetTrendsForComparision]
 (  
 @ProjectId uniqueidentifier = null,     
  @TrendKeyWord NVARCHAR(50) = null,  
  @TrendValue INT = 0,  
  @EcoSystem [dbo].[udtUniqueIdentifier] READONLY ,  
  @OutlookList [dbo].[udtUniqueIdentifier] READONLY ,  
  @ImportanceList nvarchar(100),  
  @IndustryIdList [dbo].[udtUniqueIdentifier] READONLY ,  
  @CompanyIdList [dbo].[udtUniqueIdentifier] READONLY ,
  @CompanyGroupIdList  [dbo].[udtUniqueIdentifier] READONLY ,
  @OrdbyByColumnName NVARCHAR(50) ='Sequence',          
  @SortDirection INT = -1,          
  @PageStart INT = 0,        
  @TrendName NVARCHAR(512)='',          
  @PageSize INT = null,      
  @AviraUserID uniqueidentifier = null,      
  @AviraUserRoleId uniqueidentifier = null,
  @ImpactDirection NVARCHAR(512) = NULL
 )              
 AS              
/*================================================================================              
Procedure Name: SFab_GetTrendsbySubscription         
Author: Praveen              
Create date: 04/15/2019              
Description: Get list from Trends table based on the customer subscription      
Change History              
Date  Developer  Reason              
__________ ____________ ______________________________________________________              
04/15/2019 Praveen   Initial Version  
19/06/2019 Jagan     For getting the amount and year from trend values    
09/09/2019 Pooja     add value and change in table values according to requirement          
================================================================================*/             
              
BEGIN              
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 
SET @OrdbyByColumnName = 'Sequence'
DECLARE @ValueName NVARCHAR(200) = 'Percentage'
DECLARE @TrendStatusName NVARCHAR(200) = 'Approved'
CREATE TABLE #cte_TTT_TABLE (Rnum int, TrendId uniqueidentifier, TimeTagId uniqueidentifier)
     
if len(@TrendName)>2 set @TrendName = '%'+ @TrendName + '%'        
else set @TrendName = ''        
if @PageSize is null or @PageSize =0          
set @PageSize = 50          

declare @TrendApprovedStatusId uniqueidentifier;

	select @TrendApprovedStatusId = Id
	from TrendStatus
	where TrendStatusName = @TrendStatusName;

declare @OutLookNewList [dbo].[udtUniqueIdentifier];
	declare @timetagseq tinyint;
	if exists (select 1 from @OutLookList)
	begin
		select @timetagseq = max(seq)
	from TimeTag where id in (select id from @OutLookList)
	end
	else	
	begin
		select @timetagseq = max(seq)
	from TimeTag --where id in (select id from @OutLookList)
	end
	
	insert into @OutLookNewList
	select id 
	from TimeTag
	where (@timetagseq < 3 and TimeTag.Seq < 3)
	or (@timetagseq = 3 and TimeTag.Seq = 3)
	or (@timetagseq > 3 and TimeTag.Seq > 3);


if isnull(@TrendKeyWord,'') = ''   
set @TrendKeyWord = ''  
else   
set @TrendKeyWord = '%'+ @TrendKeyWord + '%';  
 DECLARE @PercentConversionID uniqueidentifier  
 select @PercentConversionID = id from valueConversion   
 where ValueName = @ValueName;  

Declare @ImportanceIdList [dbo].[udtUniqueIdentifier];
;With cteImpMap as (Select 
imp.Id as ImportanceID,
imp.ImportanceName as ImportanceName
from Importance imp
where imp.GroupName in (select value from STRING_SPLIT(@ImportanceList,','))
or not exists(select value from STRING_SPLIT(@ImportanceList,','))
)
insert into @ImportanceIdList
select ImportanceID from cteImpMap

declare @TrendNameList [dbo].[udtVarcharValue];
insert into @TrendNameList
SELECT distinct '%'+ltrim(rtrim(value)) +'%'
FROM STRING_SPLIT(@TrendName, ' ')
where value != '' ;

begin          

 INSERT INTO #cte_TTT_TABLE(Rnum, TrendId, TimeTagId)
 --;WITH cte_TTT as (
			select ROW_NUMBER() over (partition by trend.ID order by TimeTag.seq desc) as Rnum,
			Trend.Id as TrendId,
			TrendTimeTag.TimeTagId
			from TrendTimeTag
			Inner Join Trend
			On TrendTimeTag.TrendId = Trend.Id
			inner join TimeTag
			on TrendTimeTag.TimeTagId = TimeTag.ID
			where trend.projectId = @projectId
			and TrendTimeTag.IsDeleted = 0
	     --),
 
 ;WITH CTE_TBL AS (           
 SELECT           
  [Trend].[Id] ,              
  [Trend].[TrendName],             
  Importance.ImportanceName,  
  trend.TrendValue as Amount, 
 Trend.ProjectID as ProjectId ,
 Importance.Sequence,
  --Year,      
  CASE       
  WHEN Importance.GroupName = 'High' THEN 'high-bg'      
  WHEN Importance.GroupName = 'Mid' THEN 'mid-bg'      
  WHEN Importance.GroupName = 'Low' THEN 'low-bg' ELSE 'low-bg' END      
  AS ImportanceColorCode,      
 CASE          
  WHEN @OrdbyByColumnName = 'TrendName' THEN [Trend].TrendName        
  WHEN @OrdbyByColumnName = 'Description' THEN [Trend].[Description]   
  WHEN @OrdbyByColumnName = 'Sequence' THEN CONVERT(VARCHAR,Importance.Sequence)
  ELSE [Trend].TrendName           
 END AS SortColumn          
          
FROM [dbo].[Trend]  
LEFT JOIN Importance ON  [dbo].[Trend].ImportanceId = Importance.Id  
LEFT JOIN ImpactDirection ImpD ON trend.ImpactDirectionId = ImpD.Id  
WHERE (Trend.ProjectID = @ProjectId) 
AND Trend.IsDeleted=0   
--and (@TrendName is null or @TrendName = '')
--AND (Trend.TrendName like '%' + @TrendName + '%' OR @TrendName is null or @TrendName = '')  
AND (not exists (select 1 from @TrendNameList) or exists  (SELECT 1 from @TrendNameList list WHERE Trend.TrendName like list.[value]))

AND Trend.TrendStatusID=@TrendApprovedStatusId
--AND (@ProjectId IS NULL OR @ProjectId = '00000000-0000-0000-0000-000000000000' or Trend.ProjectID = @ProjectId)  
--AND @TrendValue = CASE WHEN ISNULL(@TrendValue,0) = 0 THEN @TrendValue WHEN @TrendValue = -1 THEN ISNULL(TrendValue,0)  
AND(@TrendValue = 0 or @TrendValue is null   
or (@TrendValue =1 and Trend.TrendValue <0)--between '-5.0' and '0.0'
--or (@TrendValue =1 and Trend.TrendValue >= '5.0')
--Jagan Adding <10 and >10 as trendvalues
or (@TrendValue =2 and Trend.TrendValue >0  )--not between '-5.0' and '0.0'
or (@TrendValue=3 and Trend.TrendValue=0 )--between '-10.0' and '10.0'
or (@TrendValue=4 and Trend.TrendValue not between '-10.0' and '10.0')
--Jagan
)  
AND (@TrendKeyWord = '' 

OR
--or Trend.Id in(select TrendId from TrendKeywords Where TrendKeyWord like @TrendKeyWord and IsDeleted = 0)
exists(select 1 from TrendKeywords Where TrendKeywords.TrendId = Trend.Id AND TrendKeyWord like @TrendKeyWord and IsDeleted = 0)


)  
and  
 (NOT exists(select 1 from @IndustryIdList) or exists (select 1 from TrendIndustryMap TIM
														inner join @IndustryIdList indlist
														on TIM.IndustryId = indlist.ID
														where Trend.id = TIM.TrendId AND TIM.Impact ! = '00000000-0000-0000-0000-000000000000'
														and TIM.IsDeleted = 0))
AND  
 (NOT exists(select 1 from @CompanyIdList) or exists (select 1 from TrendKeyCompanyMap TKCM
														inner join @CompanyIdList complist
														on TKCM.CompanyId = complist.ID
														where Trend.id = TKCM.TrendId 
														and TKCM.IsDeleted = 0))
AND  
 (NOT exists(select 1 from @CompanyGroupIdList) or exists (select 1 from TrendCompanyGroupMap TCGM
															inner join @CompanyGroupIdList list
															on TCGM.CompanyGroupId = list.ID
															where Trend.Id = TCGM.TrendId AND TCGM.Impact ! = '00000000-0000-0000-0000-000000000000'
															and TCGM.IsDeleted = 0))
 AND  (Trend.ImportanceId in (select ID from @ImportanceIdList))  
 AND (NOT exists(select 1 from @OutlookList) or exists (select 1 from #cte_TTT_TABLE ttt
				inner join @OutLookNewList list
				on ttt.TimeTagId = list.ID
				where Trend.id = ttt.TrendId
				--and ttt.TimeTagId in (select id from @OutLookNewList
				and ttt.rnum =1))
And (@ImpactDirection is null or @ImpactDirection = '' or  exists(select value from STRING_SPLIT(@ImpactDirection,',') WHERE value = ImpD.ImpactDirectionName ))
AND (NOT exists(select 1 from @EcoSystem) or exists (select 1 from TrendEcoSystemMap TESM
															inner join @EcoSystem list
															on TESM.EcoSystemId = list.ID
															where Trend.Id = TESM.TrendId AND TESM.Impact ! = '00000000-0000-0000-0000-000000000000'
															and TESM.IsDeleted = 0))
   
)          
      
  
 SELECT           
    CTE_TBL.*,          
    tCountResult.TotalItems          
  FROM CTE_TBL          
  CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult
   ORDER BY Sequence DESC,Amount DESC          
  OFFSET @PageStart ROWS          
  --FETCH NEXT @PageSize ROWS ONLY;  
          
  END          
     
	 DROP TABLE #cte_TTT_TABLE
	      
END 