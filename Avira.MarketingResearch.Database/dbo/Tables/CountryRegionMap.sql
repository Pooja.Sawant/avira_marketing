﻿CREATE TABLE [dbo].[CountryRegionMap](
    [Id] [uniqueidentifier] NOT NULL,
    [CountryId] [uniqueidentifier] NOT NULL,
    [RegionId] [uniqueidentifier] NOT NULL,
    [CreatedOn] [datetime2](7) NULL,
    [UserCreatedById] [uniqueidentifier] NULL,
    [ModifiedOn] [datetime2](7) NULL,
    [UserModifiedById] [uniqueidentifier] NULL,
    [IsDeleted] [bit] NOT NULL,
    [DeletedOn] [datetime2](7) NULL,
    [UserDeletedById] [uniqueidentifier] NULL,
 CONSTRAINT [PK_CountryRegionMap] PRIMARY KEY CLUSTERED 
(
    [Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CountryRegionMap] ADD  CONSTRAINT [DF_CountryRegionMap_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[CountryRegionMap]  WITH CHECK ADD  CONSTRAINT [FK_CountryRegionMap_CountryId] FOREIGN KEY([CountryId])
REFERENCES [dbo].[Country] ([Id])
GO
ALTER TABLE [dbo].[CountryRegionMap] CHECK CONSTRAINT [FK_CountryRegionMap_CountryId]
GO
ALTER TABLE [dbo].[CountryRegionMap]  WITH CHECK ADD  CONSTRAINT [FK_CountryRegionMap_CreatedUser] FOREIGN KEY([UserCreatedById])
REFERENCES [dbo].[AviraUser] ([Id])
GO
ALTER TABLE [dbo].[CountryRegionMap] CHECK CONSTRAINT [FK_CountryRegionMap_CreatedUser]
GO
ALTER TABLE [dbo].[CountryRegionMap]  WITH CHECK ADD  CONSTRAINT [FK_CountryRegionMap_ModifiedUser] FOREIGN KEY([UserModifiedById])
REFERENCES [dbo].[AviraUser] ([Id])
GO
ALTER TABLE [dbo].[CountryRegionMap] CHECK CONSTRAINT [FK_CountryRegionMap_ModifiedUser]
GO
ALTER TABLE [dbo].[CountryRegionMap]  WITH CHECK ADD  CONSTRAINT [FK_CountryRegionMap_RegionId] FOREIGN KEY([RegionId])
REFERENCES [dbo].[Region] ([Id])
GO
ALTER TABLE [dbo].[CountryRegionMap] CHECK CONSTRAINT [FK_CountryRegionMap_RegionId]
GO