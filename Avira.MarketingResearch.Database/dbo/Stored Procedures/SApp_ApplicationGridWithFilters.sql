﻿CREATE PROCEDURE [dbo].[SApp_ApplicationGridWithFilters]
	(@ApplicationIDList dbo.udtUniqueIdentifier READONLY,
	@ApplicationName nvarchar(256)='',
	@Description nvarchar(256)='',
	@includeDeleted bit = 0,
	@OrdbyByColumnName NVARCHAR(50) ='CreatedDate',
	@SortDirection INT = -1,
	@PageStart INT = 0,
	@PageSize INT = Null)
	AS
/*================================================================================
Procedure Name: SApp_GetApplicationbyID
Author: Adarsh
Create date: 03/16/2019
Description: Get list from Application table by Name or ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
03/16/2019	Adarsh			Initial Version
__________	____________	______________________________________________________
04/05/2019	Pooja			Paging Changes
================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
if @includeDeleted is null set @includeDeleted = 0;
declare @SegmentId uniqueidentifier;
select @SegmentId = Id from Segment
where SegmentName = 'Applications';

if len(@ApplicationName)>2 set @ApplicationName = '%'+ @ApplicationName + '%'
else set @ApplicationName = ''
if len(@Description)>2 set @Description = '%'+ @Description + '%';
else set @Description = '';
	if @PageSize is null or @PageSize =0
	set @PageSize = 10
	begin
	;WITH CTE_TBL AS (

	SELECT [SubSegment].[Id],
		[SubSegment].[SubSegmentName] AS ApplicationName,
		[SubSegment].[Description],
		[SubSegment].[IsDeleted],
		CASE
		WHEN @OrdbyByColumnName = 'ApplicationName' THEN [SubSegment].[SubSegmentName]
		WHEN @OrdbyByColumnName = 'Description' THEN [SubSegment].[Description]
		ELSE [SubSegment].[SubSegmentName]
	END AS SortColumn

FROM [dbo].[SubSegment]
where SegmentId = @SegmentId
	AND (NOT EXISTS (SELECT * FROM @ApplicationIDList) or  [SubSegment].Id  in (select * from @ApplicationIDList))
	AND (@ApplicationName = '' or [SubSegment].[SubSegmentName] like @ApplicationName)
	AND (@Description = '' or [SubSegment].[Description] like @Description)
	AND (@includeDeleted = 1 or [SubSegment].IsDeleted = 0)
	)

	SELECT 
		  CTE_TBL.*,
		  tCountResult.TotalItems
		FROM CTE_TBL
		CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult
		ORDER BY 
		  CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,
		  CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC 
		OFFSET @PageStart ROWS
		FETCH NEXT @PageSize ROWS ONLY;
		END
	END