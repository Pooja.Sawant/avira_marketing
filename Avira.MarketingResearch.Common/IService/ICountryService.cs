﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;

namespace Avira.MarketingResearch.Common.IService
{
   public interface ICountryService
    {
        IEnumerable<CountryViewModel> GetAll(Guid Id);
        IEnumerable<CountryViewModel> GetAll(bool isDeletedInclude = false);
        CountryViewModel GetById(Guid guid);
    }
}
