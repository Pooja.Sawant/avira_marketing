﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.CompanyProfile
{
    public class NewsViewModel
    {
        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
        public Guid ProjectId { get; set; }
        public DateTime? Date { get; set; }
        public string News { get; set; }
        public Guid NewsCategoryId { get; set; }
        public Guid ImportanceId { get; set; }
        public Guid ImpactDirectionId { get; set; }
        public string OtherParticipants { get; set; }
        public string Remarks { get; set; }
        public string RowType
        {
            get
            {
                return "News";
            }
        }
    }
    public class NewsCategoryViewModel
    {
        public Guid Id { get; set; }
        public string NewsCategoryName { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedOn { get; set; }
        public Guid? UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public string RowType
        {
            get
            {
                return "NewsCategory";
            }
        }
    }

    public class NewsImpactDirectionViewModel
    {
        public Guid Id { get; set; }
        public string ImpactDirectionName { get; set; }
        public string ImpactDirectionDescription { get; set; }
        public string RowType
        {
            get
            {
                return "ImpactDirection";
            }
        }
    }

    public class NewsImportanceViewModel
    {
        public Guid Id { get; set; }
        public string ImportanceName { get; set; }
        public string ImportanceDescription { get; set; }
        public string RowType
        {
            get
            {
                return "Importance";
            }
        }
    }

    public class MainNewsInsertUpdateDbViewModel
    {
        public Guid CompanyId { get; set; }
        public Guid ProjectId { get; set; }
        public List<NewsViewModel> NewsInsertUpdate { get; set; }
        public Guid AviraUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public CompanyViewModel CompanyModel { get; set; }
        public CompanyNoteViewModel CompanyNoteModel { get; set; }
    }
}
