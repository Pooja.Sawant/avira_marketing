﻿CREATE TABLE [dbo].[TrendKeywords] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [TrendKeyWord]     NVARCHAR (512)   NULL,
    [TrendId]          UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_TrendKeywords_IsDeleted] DEFAULT ((0)) NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_TrendKeywords] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TrendKeywords_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_TrendKeywords_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_TrendKeywords_Trend] FOREIGN KEY ([TrendId]) REFERENCES [dbo].[Trend] ([Id])
);






GO

GO

GO

GO

GO

GO

GO

GO
CREATE NONCLUSTERED INDEX [IX_TrendKeywords_1]
    ON [dbo].[TrendKeywords]([IsDeleted] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_TrendKeywords]
    ON [dbo].[TrendKeywords]([TrendId] ASC);

