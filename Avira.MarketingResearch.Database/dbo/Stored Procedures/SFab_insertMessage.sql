﻿
CREATE PROCEDURE [dbo].[SFab_insertMessage]
(@ID		UNIQUEIDENTIFIER=null,
@MessageTemplateId	UNIQUEIDENTIFIER=null,
@SenderFrom	NVARCHAR(500)=null,
@ReceiverTos	NVARCHAR(500)=null,
@ReceiverCcs	NVARCHAR(500)=null,
@ReceiverBccs	NVARCHAR(500)=null,
@Subject		NVARCHAR(500)=null,
@Message		NVARCHAR(max)=null,
@StatusId UNIQUEIDENTIFIER=null
)
as
/*================================================================================
Procedure Name: SFab_insertMessage
Author: Pooja Sawant
Create date: 05/14/2019
Description: Insert a record in Message table 
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
05/14/2019	Pooja			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ErrorMsg NVARCHAR(2048);


if @StatusId is null
set @StatusId=(select Id from MessageStatus
where StatusName = 'Pending')

--End of Validations
BEGIN TRY

INSERT INTO [dbo].[Message]
           ([Id]
           ,[MessageTemplateId]
		   ,[SenderFrom]
           ,[ReceiverTos]
           ,[ReceiverCcs]
           ,[ReceiverBccs]
           ,[Subject]
           ,[Message]
           ,[StatusId]
           ,[IsDeleted]
           ,[CreatedOn]
           )
     VALUES
           (@ID
           ,@MessageTemplateId
		   ,@SenderFrom
           ,@ReceiverTos
           ,@ReceiverCcs
           ,@ReceiverBccs
           ,@Subject
           ,@Message
           ,@StatusId
           ,0
           ,GETDATE()
         )

SELECT StoredProcedureName ='SFab_insertMessage',Message =@ErrorMsg,Success = 1;     

END TRY
BEGIN CATCH
    -- Execute the error retrieval routine.
	DECLARE @ErrorNumber	int;
	DECLARE @ErrorSeverity	int;
	DECLARE @ErrorProcedure	varchar(100);
	DECLARE @ErrorLine	int;
	DECLARE @ErrorMessage	varchar(500);

	 SELECT @ErrorNumber = ERROR_NUMBER(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorProcedure = ERROR_PROCEDURE(),
        @ErrorLine = ERROR_LINE(),
        @ErrorMessage = ERROR_MESSAGE()

	insert into dbo.Errorlog(ErrorNumber,
							ErrorSeverity,
							ErrorProcedure,
							ErrorLine,
							ErrorMessage,
							ErrorDate)
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());

 set @ErrorMsg = 'Error while creating a new Message, please contact Admin for more details.';    
 
SELECT Number = @ErrorNumber, 
		Severity =@ErrorSeverity,
		StoredProcedureName =@ErrorProcedure,
		LineNumber= @ErrorLine,
		Message =@ErrorMsg,
		Success = 0;     
END CATCH
end