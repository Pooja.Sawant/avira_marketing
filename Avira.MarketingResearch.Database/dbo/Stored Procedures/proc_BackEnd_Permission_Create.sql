﻿CREATE PROCEDURE [dbo].[proc_BackEnd_Permission_Create]
(
@UserType tinyint,
@PermissionName NVARCHAR(MAX)
)
AS
/*================================================================================
Procedure Name: proc_BackEnd_Permission_Create
Author: Praveen
Create date: 07/02/2019
Description: To insert permission from backend
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
07/02/2019	Praveen			Initial Version
================================================================================*/
BEGIN

	INSERT INTO Permission (Id ,
	PermissionName			   ,
	UserType				   ,
	CreatedOn				   ,
	UserCreatedById			   
	)
	VALUES (NEWID() ,@PermissionName,@UserType,GETDATE(),'6B5A19C0-45E6-4FFC-815F-75F7C3D22B79' )

END