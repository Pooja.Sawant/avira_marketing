﻿using System;
using System.Collections.Generic;
using Avira.MarketingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.CompanyProfile
{
    public class CompanyStructureService : ICompanyStructureService
    {
        //private readonly ICompanyStructureService _CompanyStructureRepository;

        private readonly ICompanyStructureRepository _companyStructureRepository;
        private readonly ITrendNoteRepository _iTrendNoteRepository;

        public CompanyStructureService(ICompanyStructureRepository companyStructureRepository, ITrendNoteRepository iTrendNoteRepository)
        {
            _companyStructureRepository = companyStructureRepository;
            _iTrendNoteRepository = iTrendNoteRepository;
        }

        public IEnumerable<CompanyStructureModel> GetAll(Guid Id)
        {
            return _companyStructureRepository.GetAll(Id);
            //throw new NotImplementedException();
        }
    }
}
