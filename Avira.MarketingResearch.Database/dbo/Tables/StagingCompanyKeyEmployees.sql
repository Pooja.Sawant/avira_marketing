﻿CREATE TABLE [dbo].[StagingCompanyKeyEmployees] (
    [Id]                     UNIQUEIDENTIFIER NOT NULL,
    [ImportId]               UNIQUEIDENTIFIER NULL,
    [KeyEmployeeName]        NVARCHAR (255)   NULL,
    [KeyEmployeeDesignation] NVARCHAR (255)   NULL,
    [KeyEmployeeEmail]       NVARCHAR (254)   NULL,
    [ReportingManager]       NVARCHAR (255)   NULL,
    [CompanyId]              UNIQUEIDENTIFIER NULL,
    [ErrorNotes]             NVARCHAR (1000)  NULL,
    [RowNo]                  INT              NULL,
    CONSTRAINT [PK_StagingCompanyKeyEmployees] PRIMARY KEY CLUSTERED ([Id] ASC)
);





