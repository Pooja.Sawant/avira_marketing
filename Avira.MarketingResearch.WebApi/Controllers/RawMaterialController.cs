﻿using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Utility;
using Avira.MarketingResearch.WebApi;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Avira.MarketingResearch.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RawMaterialController : AviraBaseController
    {
        private ILoggerManager _logger;
        public readonly IRawMaterialRepository _rawMaterialRepository;
        public readonly IRawMaterialService _rawMaterialService;
        public RawMaterialController(
            IRawMaterialService rawMaterialService,
            IRawMaterialRepository rawMaterialRepository,
            ILoggerManager logger)
        {
            _rawMaterialRepository = rawMaterialRepository;
            _rawMaterialService = rawMaterialService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<RawMaterialViewModel>> Get( int PageStart=0, int pageSize=0, int SortDirection=1, string OrdbyByColumnName= "CreatedDate", string Search = "", bool isDeletedInclude = false)
        {
            List<RawMaterialViewModel> res = null;
            try
            {
                res = _rawMaterialService.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName,Search, isDeletedInclude).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }


        [HttpGet("{id}")]
        public ActionResult<RawMaterialViewModel> Get(Guid id)
        {
            RawMaterialViewModel rawMaterial = null;
            try
            {
                rawMaterial = _rawMaterialService.GetById(id);
                if (rawMaterial == null)
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return rawMaterial;
        }

        [HttpPost]
        public ActionResult Post([FromBody] RawMaterialInsertDbViewModel viewModel)
        {
            viewModel.CreatedOn = System.DateTime.UtcNow;
            viewModel.UserCreatedById = CurrentUser.Id;
            viewModel.Id = Guid.NewGuid();

            
            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new RawMaterial, please contact Admin for more details."
            };

            try
            {

                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                var result = _rawMaterialService.Create(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Raw-Material " + viewModel.RawMaterialName + " is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }

            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);

        }


        [HttpPut]
        public ActionResult Put([FromBody] RawMaterialUpdateDbViewModel viewModel)
        {

            viewModel.ModifiedOn = System.DateTime.UtcNow;
            viewModel.UserModifiedById = CurrentUser.Id;


            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while updating a new Raw-Material, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                Result<SpTransactionMessage> result = _rawMaterialService.Update(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Raw-Material  " + viewModel.RawMaterialName + " is updated successfully.";

                }
                else
                {
                    viewData.Message = result.Error;
                }
            }

            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }

        [Route("DeleteSegment")]
        [HttpPut]
        public ActionResult DeleteSegment([FromBody] RawMaterialUpdateDbViewModel viewModel)
        {
           
                viewModel.DeletedOn = System.DateTime.UtcNow;
                viewModel.UserDeletedById = CurrentUser.Id;
                viewModel.IsDeleted = true;


                IViewModel viewData = new ViewData()
                {
                    IsSuccess = false,
                    Message = "Error while deleting a new RawMaterial, please contact Admin for more details."
                };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                Result<SpTransactionMessage> result = _rawMaterialService.Update(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;

                    viewData.Message = "Raw-Material " + viewModel.RawMaterialName + " is deleted.";

                }
                else
                {
                    viewData.Message = result.Error;
                }
            }

            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }

    }
}
