﻿CREATE TABLE [dbo].[TrendKeyCompanyMap] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [TrendId]          UNIQUEIDENTIFIER NOT NULL,
    [CompanyId]        UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_TrendKeyCompanyMap] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_TrendKeyCompanyMap_1]
    ON [dbo].[TrendKeyCompanyMap]([CompanyId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_TrendKeyCompanyMap]
    ON [dbo].[TrendKeyCompanyMap]([TrendId] ASC);

