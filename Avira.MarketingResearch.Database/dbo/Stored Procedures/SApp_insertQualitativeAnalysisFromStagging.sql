﻿

CREATE procedure [dbo].[SApp_insertQualitativeAnalysisFromStagging]
@AviraUserId uniqueidentifier =null
as    
/*================================================================================    
Procedure Name: [SApp_insertQualitativeAnalysisFromStagging]    
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
19/06/2019 Sharath   Initial Version    
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    
declare @ErrorMsg NVARCHAR(2048)
declare @ProjectId	UNIQUEIDENTIFIER;

BEGIN TRY       
BEGIN TRANSACTION
select @ProjectId = ProjectId	
from [StagingQualitativeAnalysis];

--News
--TO Delete Existing Records
--DELETE FROM News Where News.ProjectID=@ProjectId
INSERT INTO [dbo].[News]
           ([Id]
           ,[Date]
           ,[News]
           ,[NewsCategoryId]
           ,[ImportanceId]
           ,[ImpactDirectionId]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted]
		   ,ProjectID
		   ,MarketId)
     select
           newid(),
           NewsStaging.NewsDate,
           NewsStaging.News,
           NewsStaging.CategoryId,
           NewsStaging.ImportanceId, 
           NewsStaging.ImpactId, 
           getdate() as CreatedOn,
		   @AviraUserId as UserCreatedById,
		   0 as IsDeleted,
           NewsStaging.ProjectID,
		   NewsStaging.SubMarketId
from StagingQualitativeNews NewsStaging
WHERE NOT EXISTS (SELECT 1 FROM [News] N1 
				  where NewsStaging.ProjectID = N1.ProjectID
				  and (NewsStaging.SubMarketId = N1.MarketId or (NewsStaging.SubMarketId is null and N1.MarketId is null))
				  and NewsStaging.News = N1.News)

--QualitativeAnalysis
--TO Delete Existing Records
DELETE FROM QualitativeAnalysis Where QualitativeAnalysis.ProjectID=@ProjectId
INSERT INTO [dbo].[QualitativeAnalysis]
           ([Id]
           ,[ProjectId]
           ,[MarketId]
           ,[QualitativeAnalysisTypeId]
           ,[QualitativeAnalysisSubTypeId]
           ,[ImportanceId]
           ,[ImpactId]
           ,[AnalysisText]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
Select NEWID(),
	   QAStaging.ProjectId,
	   QAStaging.SubMarketId,
	   QAStaging.QualitativeAnalysisTypeId,
	   QAStaging.QualitativeAnalysisSubTypeId,
	   QAStaging.ImportanceId,
	   QAStaging.ImpactId,
	   QAStaging.AnalysisText,
		 GETDATE(),
	   @AviraUserId,
	   0
From [StagingQualitativeAnalysis] QAStaging;
--WHERE NOT EXISTS (SELECT 1 FROM [QualitativeAnalysis] QA1 
--				  where QAStaging.ProjectID = QA1.ProjectID
--				  and (QAStaging.SubMarketId = QA1.MarketId or (QAStaging.SubMarketId is null and QA1.MarketId is null))
--				  and QAStaging.AnalysisText = QA1.AnalysisText);

--QualitativeDescription
--TO Delete Existing Records
DELETE FROM QualitativeDescription Where QualitativeDescription.ProjectID=@ProjectId
INSERT INTO [dbo].[QualitativeDescription]
           ([Id]
           ,[ProjectId]
           ,[MarketId]
           ,[QualitativeAnalysisTypeId]
           ,[QualitativeAnalysisSubTypeId]
           ,[DescriptionText]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
SELECT NEWID(),
		QDStaging.ProjectId,
		QDStaging.SubMarketId,
		QDStaging.QualitativeAnalysisTypeId,
		QDStaging.QualitativeAnalysisSubTypeId,
		QDStaging.[DescriptionText],
	 GETDATE(),
	   @AviraUserId,
	   0
FROM StagingQualitativeDescription QDStaging;
--WHERE NOT EXISTS (SELECT 1 FROM [QualitativeDescription] QA1 
--				  where QDStaging.ProjectID = QA1.ProjectID
--				  and (QDStaging.SubMarketId = QA1.MarketId or (QDStaging.SubMarketId is null and QA1.MarketId is null))
--				  and QDStaging.AnalysisText = QA1.AnalysisText);


--QualitativeQuote
--TO Delete Existing Records
DELETE FROM QualitativeQuotes Where QualitativeQuotes.ProjectID=@ProjectId
INSERT INTO [dbo].[QualitativeQuotes]
           ([Id]
           ,[ProjectId]
           ,[MarketId]
           ,[ResourceName]
           ,[ResourceDesignation]
           ,[ResourceDesignationId]
           ,[ResourceCompanyName]
           ,[ResourceCompanyId]
           ,[DateOfQuote]
           ,[Quote]
           ,[OtherRemarks]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
SELECT NEWID(),
		QQStaging.ProjectId,
		QQStaging.SubMarketId,
		QQStaging.ResourceName,
		QQStaging.ResourceDesignation,
		QQStaging.[ResourceDesignationId],
		QQStaging.[ResourceCompany],
		QQStaging.[CompanyId],
		QQStaging.[DateOfQuote],
        QQStaging.[Quote],
        QQStaging.[OtherRemarks],
		GETDATE(),
		@AviraUserId,
	   0
FROM StagingQualitativeQuote QQStaging;

--QualitativeSegmentMapping
--TO Delete Existing Records
DELETE FROM QualitativeSegmentMapping Where QualitativeSegmentMapping.ProjectID=@ProjectId
	
INSERT INTO [dbo].[QualitativeSegmentMapping]
           ([Id]
           ,[ProjectId]
           ,[MarketId]
           ,[SegmentId]
           ,[SubSegmentId]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
SELECT NEWID(),
		QSMapStaging.ProjectId,
		QSMapStaging.MarketId,
		QSMapStaging.SegmentId,
		QSMapStaging.SubSegmentId,
	 GETDATE(),
	   @AviraUserId,
	   0
FROM StagingQualitativeSegmentMapping QSMapStaging
--WHERE NOT EXISTS (SELECT 1 FROM [QualitativeSegmentMapping] QS1 
--				  where QSMapStaging.ProjectID = QS1.ProjectID
--				  and (QSMapStaging.MarketId = QS1.MarketId or (QSMapStaging.MarketId is null and QS1.MarketId is null))
--				  and QSMapStaging.SegmentId = QS1.SegmentId
--				  and QSMapStaging.SubSegmentId = QS1.SubSegmentId);

--Qualitative Region Mapping
--TO Delete Existing Records
DELETE FROM QualitativeRegionMapping Where QualitativeRegionMapping.ProjectID=@ProjectId

;with CTE_Val as (
select SQRM.*, t.Value as Country from StagingQualitativeRegionMapping SQRM
CROSS APPLY dbo.FUN_STRING_TOKENIZER(SQRM.CountryName, ',') T)
INSERT INTO [dbo].[QualitativeRegionMapping]
           ([Id]
           ,[ProjectId]
           ,[MarketId]
           ,[RegionId]
           ,[CountryId]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
SELECT NEWID(),
		QRMStaging.ProjectId,
		QRMStaging.MarketId,
		QRMStaging.RegionId,
		Country.id as CountryId,
	 GETDATE(),
	   @AviraUserId,
	   0
FROM StagingQualitativeRegionMapping QRMStaging
inner join CTE_Val
ON QRMStaging.Id = CTE_Val.Id
LEFT JOIN Country
ON CTE_Val.Country = Country.CountryName
--WHERE NOT EXISTS (SELECT 1 FROM [QualitativeRegionMapping] QRM1 
--				  where QRMStaging.ProjectID = QRM1.ProjectID
--				  and (QRMStaging.MarketId = QRM1.MarketId or (QRMStaging.MarketId is null and QRM1.MarketId is null))
--				  and QRMStaging.RegionId = QRM1.RegionId
--				  and (Country.id = QRM1.CountryId or (Country.id is null and QRM1.CountryId is null)));

--ESMSegmentMapping
--TO Delete Existing Records
DELETE FROM ESMSegmentMapping Where ESMSegmentMapping.ProjectID=@ProjectId
	
INSERT INTO [dbo].[ESMSegmentMapping]
           ([Id]
           ,[ProjectId]
           ,[SegmentId]
           ,[SubSegmentId]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
SELECT NEWID(),
		ESMapStaging.ProjectId,
		ESMapStaging.SegmentId,
		ESMapStaging.SubSegmentId,
	    GETDATE(),
	    @AviraUserId,
	    0
FROM StagingESMSegmentMapping ESMapStaging


--ESMAnalysis
--TO Delete Existing Records
DELETE FROM ESMAnalysis Where ESMAnalysis.ProjectID=@ProjectId
INSERT INTO [dbo].[ESMAnalysis]
           ([Id]
           ,[ProjectId]
		   ,[SegmentId]
		   ,[SubSegmentId]
           ,[QualitativeAnalysisTypeId]
           ,[ImportanceId]
           ,[ImpactId]
           ,[AnalysisText]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
Select NEWID(),
	   ESMStaging.ProjectId,
	   ESMStaging.SegmentId,
	   ESMStaging.SubSegmentId,
	   ESMStaging.QualitativeAnalysisTypeId,
	   ESMStaging.ImportanceId,
	   ESMStaging.ImpactId,
	   ESMStaging.AnalysisText,
		 GETDATE(),
	   @AviraUserId,
	   0
From [StagingESMAnalysis] ESMStaging;

declare @QualitativeStatusId uniqueidentifier;
select @QualitativeStatusId = ID
from [QualitativeStatus] 
where QualitativeStatusName = 'Approved'
--TO Delete Existing Records
--DELETE FROM QualitativeNoteHistory Where QualitativeNoteHistory.ProjectID=StagingQualitativeRegionMapping.ProjectID
DELETE  from QualitativeAnalysisNote  Where QualitativeAnalysisNote.ProjectID=@ProjectId

INSERT INTO [dbo].[QualitativeAnalysisNote]
           ([Id]
           ,[ProjectId]
		   ,[QualitativeAnalysisType]
           ,[AuthorRemark]
           ,[ApproverRemark]
           ,[QualitativeStatusID]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
select NEWID(), [ProjectId], 'News' as [QualitativeAnalysisType], 'From Import' as [AuthorRemark], 'From Import' as [ApproverRemark], @QualitativeStatusId as QualitativeStatusId, 
GETDATE(), @AviraUserId, 0 as [IsDeleted] from StagingQualitativeNews union
select NEWID(), [ProjectId], 'Qualitative Analysis' as [QualitativeAnalysisType], 'From Import' as [AuthorRemark], 'From Import' as [ApproverRemark], @QualitativeStatusId as QualitativeStatusId, 
GETDATE(), @AviraUserId, 0 as [IsDeleted] from StagingQualitativeAnalysis union
select NEWID(), [ProjectId], 'Qualitative Description' as [QualitativeAnalysisType], 'From Import' as [AuthorRemark], 'From Import' as [ApproverRemark], @QualitativeStatusId as QualitativeStatusId, 
GETDATE(), @AviraUserId, 0 as [IsDeleted] from StagingQualitativeDescription union
select NEWID(), [ProjectId], 'Qualitative Quote' as [QualitativeAnalysisType], 'From Import' as [AuthorRemark], 'From Import' as [ApproverRemark], @QualitativeStatusId as QualitativeStatusId, 
GETDATE(), @AviraUserId, 0 as [IsDeleted] from StagingQualitativeQuote union
select NEWID(), [ProjectId], 'Qualitative Segment Mapping' as [QualitativeAnalysisType], 'From Import' as [AuthorRemark], 'From Import' as [ApproverRemark], @QualitativeStatusId as QualitativeStatusId, 
GETDATE(), @AviraUserId, 0 as [IsDeleted] from StagingQualitativeSegmentMapping union
select NEWID(), [ProjectId], 'Qualitative Region Mapping' as [QualitativeAnalysisType], 'From Import' as [AuthorRemark], 'From Import' as [ApproverRemark], @QualitativeStatusId as QualitativeStatusId, 
GETDATE(), @AviraUserId, 0 as [IsDeleted] from StagingQualitativeRegionMapping union
select NEWID(), [ProjectId], 'ESM Region Mapping' as [QualitativeAnalysisType], 'From Import' as [AuthorRemark], 'From Import' as [ApproverRemark], @QualitativeStatusId as QualitativeStatusId, 
GETDATE(), @AviraUserId, 0 as [IsDeleted] from StagingQualitativeRegionMapping union
select NEWID(), [ProjectId], 'ESM Analysis' as [QualitativeAnalysisType], 'From Import' as [AuthorRemark], 'From Import' as [ApproverRemark], @QualitativeStatusId as QualitativeStatusId, 
GETDATE(), @AviraUserId, 0 as [IsDeleted] from StagingQualitativeRegionMapping;

---update Statics for Tables---
UPDATE STATISTICS [News] WITH FULLSCAN;
UPDATE STATISTICS [QualitativeAnalysis] WITH FULLSCAN;
UPDATE STATISTICS [QualitativeDescription] WITH FULLSCAN;
UPDATE STATISTICS [QualitativeQuotes] WITH FULLSCAN;
UPDATE STATISTICS [QualitativeSegmentMapping] WITH FULLSCAN;
UPDATE STATISTICS [QualitativeRegionMapping] WITH FULLSCAN;
UPDATE STATISTICS [ESMSegmentMapping] WITH FULLSCAN;
UPDATE STATISTICS [ESMAnalysis] WITH FULLSCAN;
UPDATE STATISTICS [SubSegment] WITH FULLSCAN;
UPDATE STATISTICS [QualitativeAnalysisNote] WITH FULLSCAN;
---End Statics----



SELECT StoredProcedureName ='[SApp_insertQualitativeAnalysisFromStagging]',Message =@ErrorMsg,Success = 1;     
COMMIT TRANSACTION
END TRY    
BEGIN CATCH    
 if @@TRANCOUNT > 0 
 ROLLBACK TRANSACTION;
 
    -- Execute the error retrieval routine.    
 DECLARE @ErrorNumber int;    
 DECLARE @ErrorSeverity int;    
 DECLARE @ErrorProcedure varchar(100);    
 DECLARE @ErrorLine int;    
 DECLARE @ErrorMessage varchar(500);    
    
  SELECT @ErrorNumber = ERROR_NUMBER(),    
        @ErrorSeverity = ERROR_SEVERITY(),    
        @ErrorProcedure = ERROR_PROCEDURE(),    
        @ErrorLine = ERROR_LINE(),    
        @ErrorMessage = ERROR_MESSAGE()    
    
 insert into dbo.Errorlog(ErrorNumber,    
       ErrorSeverity,    
       ErrorProcedure,    
       ErrorLine,    
       ErrorMessage,    
       ErrorDate)    
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());    
  
 set @ErrorMsg = 'Error while importing a new Qualititive details, please contact Admin for more details.';    
--THROW 50000,@ErrorMsg,1;      
--return(1)  
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;     
     
END CATCH    
    
    
end