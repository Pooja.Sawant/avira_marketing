﻿using System;
using System.Collections.Generic;
using Avira.MarketingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using System.Text;
using System.Threading.Tasks;

namespace Avira.MarketingResearch.Common.Service_Implementation.CompanyProfile
{
    public class EcoSystemPresenceService : IEcoSystemPresenceService
    {
        private readonly IEcoSystemPresenceRepository _ecoSystemPresenceRepository;
        private readonly ICompanyNoteRepository _iCompanyNoteRepository;

        public EcoSystemPresenceService(IEcoSystemPresenceRepository ecoSystemPresenceRepository, ICompanyNoteRepository iCompanyNoteRepository)
        {
            _ecoSystemPresenceRepository = ecoSystemPresenceRepository;
            _iCompanyNoteRepository = iCompanyNoteRepository;
        }

        public IEnumerable<EcoSystemPresenceMapViewModel> GetEcoSystemPresenceMapByCompanyId(Guid CompanyId)
        {
            return _ecoSystemPresenceRepository.GetEcoSystemPresenceMapByCompanyId(CompanyId);
            
        }
        public IEnumerable<SectorPresenceMapViewModel> GetSectorPresenceMapByCompanyId(Guid CompanyId)
        {
            return _ecoSystemPresenceRepository.GetSectorPresenceMapByCompanyId(CompanyId);

        }
        public IEnumerable<EcoSystemPresenceKeyCompViewModel> GetEcoSystemPresenceKeyCompByCompanyId(Guid CompanyId)
        {
            return _ecoSystemPresenceRepository.GetEcoSystemPresenceKeyCompByCompanyId(CompanyId);

        }
        
        public Result<SpTransactionMessage> InsertUpdate(EcoSystemPresenceDbViewModel tfc, AviraUser currenUser)
        {
            var res = _ecoSystemPresenceRepository.InsertUpdate(tfc);
            if (res.IsSuccess == true)
            {
                tfc.CompanyId = tfc.CompanyId;
                tfc.CompanyNoteModel.UserCreatedById = tfc.UserCreatedById;
                tfc.CompanyNoteModel.CompanyId = tfc.CompanyId;
                tfc.CompanyNoteModel.Id = Guid.NewGuid();
                _iCompanyNoteRepository.Insert(tfc.CompanyNoteModel);
            }

            return res;
        }
    }
}
