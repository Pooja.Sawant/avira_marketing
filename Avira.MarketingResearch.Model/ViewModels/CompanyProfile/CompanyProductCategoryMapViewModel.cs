﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.CompanyProfile
{
    public class CompanyProductCategoryMapViewModel
    {
        public Guid Id { get; set; }
        public Guid CompanyProductId { get; set; }
        public Guid ProductCategoryId { get; set; }
        public string ProductCategoryName { get; set; }
        public bool IsMapped { get; set; }
    }

    public class MainCompanyProductCategoryMapInsertUpdateDbViewModel
    {
        public Guid CompanyProductId { get; set; }
        public string ProductCategoryName { get; set; }
        public List<CompanyProductCategoryMapViewModel> CompanyProductCategoryMapInsertUpdate { get; set; }
        public Guid AviraUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
    }
}
