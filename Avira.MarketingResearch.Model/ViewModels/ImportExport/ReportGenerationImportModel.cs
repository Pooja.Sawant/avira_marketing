﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.ImportExport
{
    public class ReportGenerationImportModel
    {
        public string TemplateName { get; set; }
        public string ImportFileName { get; set; }
        public string JsonReportGeration { get; set; }
        public List<ReportGenerationViewModel> ReportGenerationViewModel { get; set; }
        public Guid UserCreatedById { get; set; }
    }

    public class ReportGenerationViewModel
    {
        public string ProjectName { get; set; }
        public string ModuleName { get; set; }
        public string Header { get; set; }
        public string Footer { get; set; }
        public string ReportTitle { get; set; }
        public string CoverPageImageLink { get; set; }
        public byte[] CoverPageImageFileByte { get; set; }
        public string CoverPageImageName { get; set; }
        public string Overview { get; set; }
        public string KeyPoints { get; set; }
        public string Methodology { get; set; }
        public string MethodologyImageName { get; set; }
        public string MethodologyImageLink { get; set; }
        public byte[] MethodologyImageFileByte { get; set; }
        public string ContactUsAnalystName { get; set; }
        public string ContactUsOfficeContactNo { get; set; }
        public string ContactUsAnalystEmailId { get; set; }
    }


}
