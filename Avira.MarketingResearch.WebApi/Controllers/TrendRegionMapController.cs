﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrendRegionMapController : AviraBaseController
    {
        private readonly ITrendRegionMapService _trendRegionMapService;
        private readonly ITrendRegionMapRepository _trendRegionMapRepository;
        private ILoggerManager _logger;
        public TrendRegionMapController(
            ITrendRegionMapService trendRegionMapService,
            ITrendRegionMapRepository trendRegionMapRepository,
             ILoggerManager logger)
        {
            _trendRegionMapRepository = trendRegionMapRepository;
            _trendRegionMapService = trendRegionMapService;
            _logger = logger;

        }

        //[HttpGet]
        //[Route("GetAllRegions")]
        //public ActionResult<IEnumerable<RegionCountryModel>> GetAllRegionsByTrendId(Guid trendId)
        //{
        //    return _regionRepository.GetRegionByTrendID(trendId).ToList();
        //}

        [HttpGet]
        public ActionResult<IEnumerable<TrendRegionMapViewModel>> Get(int PageStart = 0, int pageSize = 0, int SortDirection = 1, string OrdbyByColumnName = "CreatedDate", string Search = "", bool isDeletedInclude = false)
        {
            List<TrendRegionMapViewModel> res = null;
            try
            {
                res = _trendRegionMapService.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpPost]
        public ActionResult Post([FromBody] MainTrendRegionMapInsertUpdateDbViewModel viewModel)
        {
            viewModel.CreatedOn = System.DateTime.UtcNow;
            viewModel.AviraUserId = CurrentUser.Id;

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new TrendRegionMap, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                var result = _trendRegionMapService.InsertUpdate(viewModel, CurrentUser);
                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "TrendRegionMap is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }
    }
}