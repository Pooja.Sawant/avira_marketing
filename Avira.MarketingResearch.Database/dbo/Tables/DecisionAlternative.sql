﻿CREATE TABLE [dbo].[DecisionAlternative] (
    [Id]               UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [DecisionId]       UNIQUEIDENTIFIER NOT NULL,
    [Description]      NVARCHAR (1024)  NOT NULL,
    [ResourceAssigned] NVARCHAR (899)   NOT NULL,
    [StatusId]         UNIQUEIDENTIFIER NOT NULL,
    [PriorityId]       UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]        DATETIME2 (7)    DEFAULT (getdate()) NOT NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([PriorityId]) REFERENCES [dbo].[LookupCategory] ([Id]),
    FOREIGN KEY ([StatusId]) REFERENCES [dbo].[LookupStatus] ([Id]),
    FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK__DecisionA__Decis__16FAE1CD] FOREIGN KEY ([DecisionId]) REFERENCES [dbo].[Decision] ([Id])
);

