﻿

CREATE PROCEDURE [dbo].[SApp_GetCompanyCashFlowStatementDataByCompanyID]             
 @CompanyID uniqueidentifier = null,         
 @includeDeleted bit = 0             
AS
/*================================================================================  
Procedure Name: [SApp_GetCompanyCashFlowStatementDataByCompanyID]  
Author: Harshal 
Create date: 08/09/2019  
Description:Getting Data From Table WRT CompanyId 
Change History  
Date        Developer     Reason  
__________  ____________  ______________________________________________________  
08/09/2019  Harshal   Initial Version  
================================================================================*/          
BEGIN         
 SET NOCOUNT ON;          
 SELECT  Id,
CompanyId,
CurrencyId,
ValueConversionId,
CompanyName,
Currency,
Units,
Year,
OperatingActivities_,
AdjustmentsToOperatingActivities_,
DepreciationAndAmortization,
Impairments,
LossOnSaleOfAssets,
OtherLossesOrGains,
DeferredTaxes,
ShareBasedCompensationExpense,
BenefitPlanContributions,
OtherAdjustmentsNet,
OtherChangesInAssetsAndLiabilitiesNetOfAcquisitionsAndDistitures_,
AccountsReceivable,
OtherAssets,
AccountsPayable,
OtherLiabilities,
OtherTaxAccountsNet,
NetCashProvidedByOrUsedInOperatingActivities,
InvestingActivities_,
PurchasesOfPropertyPlantAndEquipment,
PurchasesOfShortTermInvestments,
SaleOfShortTermInvestments,
RedemptionsOfShortTermInvestments,
PurchasesOfLongTermInvestments,
SaleOfLongTermInvestments,
BusinessAcquisitions,
AcquisitionsOfIntangibleAssets,
OtherInvestingActivitiesNet,
NetCashProvidedByOrUsedInInvestingActivities,
FinancingActivities_,
ProceedsFromShortTermDebt,
PrincipalPaymentsOnShortTermDebt,
NetPaymentsonOrProceedsFromShortTermBorrowingsWithOrigilMaturitiesOfThreeMonthsOrLess,
IssuanceOfLongTermDebt,
PrincipalPaymentsOnLongTermDebt,
PurchasesOfCommonStock,
CommonStockRepurchased,
CashDividendsPaid,
ProceedsFromExerciseOfStockOptions,
OtherFinancingActivitiesNet,
CapitalExpenditure,
NetCashProvidedByOrUsedInFinancingActivities,
ForeignExchangeImpact,
DecreaseInCashAndCashEquivalents,
OpeningBalanceOfCashAndCashEquivalents,
ClosingBalanceOfCashAndCashEquivalents,
CashPaidReceivedDuringThePeriodFor_,
IncomeTaxes,
InterestRateHedges
FROM [dbo].[CompanyCashFlowStatement]        
  
  WHERE ([CompanyCashFlowStatement].CompanyId = @CompanyID)              
 AND (@includeDeleted = 0 or [CompanyCashFlowStatement].IsDeleted = 0)          
END