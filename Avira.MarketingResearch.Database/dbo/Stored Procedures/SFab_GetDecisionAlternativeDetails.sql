﻿CREATE PROCEDURE [dbo].[SFab_GetDecisionAlternativeDetails]    
(  
	@DecisionId UNIQUEIDENTIFIER,
	@DecisionAlternativeId UNIQUEIDENTIFIER,
	@AviraUserId UNIQUEIDENTIFIER
)
As    
/*================================================================================    
Procedure Name: SFab_GetDecisionAlternativeDetails   
Author: Pooja S    
Create date: 20-Jan-2020
Description: To return Decision & Alternative details by DecisionAlternativeId of the user
Change History    
Date  Developer  Reason  
__________ ____________ ______________________________________________________    
20-Jan-2020  Pooja S   Initial Version
30-Jan-2020  Nitin	   Decision Readonly added    
================================================================================*/    
BEGIN    
------------------

Declare @DecisionStatusDecisionMadeId uniqueidentifier

	Select @DecisionStatusDecisionMadeId = ID From LookupStatus
	Where StatusName = 'Decision made' And StatusType = 'DMDecisionStatus'


SELECT Decision.Description as Decision, Decision.ResourceAssigned AS DecisionOwner, CASE WHEN ISNULL(Decision.Deadline, '') = '' THEN '' ELSE FORMAT(Decision.Deadline, 'dd/MMM/yyyy ') END AS Deadline, Decision.Id AS DecisionId, 
DecisionAlternative.Id AS AlternativeId, DecisionAlternative.Description AS AlternativeName, 
 DecisionAlternative.ResourceAssigned AS AlternativeOwner, LookupCategory.CategoryName as PriorityName,
	CASE WHEN (Decision.StatusId = @DecisionStatusDecisionMadeId) THEN 'true' 
		 ELSE 'false' 
	END AS IsReadonly
FROM   Decision INNER JOIN
    DecisionAlternative ON Decision.Id = DecisionAlternative.DecisionId INNER JOIN
   LookupCategory ON DecisionAlternative.PriorityId = LookupCategory.Id
WHERE        (Decision.Id = @DecisionId) AND (DecisionAlternative.Id = @DecisionAlternativeId) and Decision.UserId=@AviraUserId
and LookupCategory.CategoryType = 'DecisionAlternativePriority'
		
END