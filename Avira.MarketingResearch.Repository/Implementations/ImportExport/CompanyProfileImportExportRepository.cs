﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;


namespace Avira.MarketingResearch.Repository.Implementations.ImportExport
{
    public class CompanyProfileImportExportRepository: ICompanyProfileImportExportRepository
    {

        protected IDbHelper _dbHelper;
        private const string CacheRegion = "CompanyProfileImportExport";

        public CompanyProfileImportExportRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        //public Result<SpTransactionMessage> Create(CompanyProfileImportInsertViewModel CompanyProfileImportInsertViewModel)
        //{
        //    var spTransactionMessage = _dbHelper.ExecuteQuery(ImportExportQueries.StoreProcInsertCompanyProfile, new
        //    {
        //        @TemplateName = CompanyProfileImportInsertViewModel.TemplateName,
        //        @ImportFileName = CompanyProfileImportInsertViewModel.ImportFileName,
        //        @jsonTrend = CompanyProfileImportInsertViewModel.TrendJson,
        //        @UserCreatedById = CompanyProfileImportInsertViewModel.UserCreatedById
        //    }, CommandType.StoredProcedure);
        //    return spTransactionMessage;
        //}


        List<ImportCommonResponseModel> ICompanyProfileImportExportRepository.Create(CompanyProfileImportInsertViewModel viewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(ImportExportQueries.StoreProcInsertCompanyProfile, new
            {
                @TemplateName = viewModel.TemplateName,
                @ImportFileName = viewModel.ImportFileName,

                @JsonFundamentals = viewModel.JsonFundamentals,
                @JsonShareHolding = viewModel.JsonShareHolding,
                @JsonSubsidiaries = viewModel.JsonSubsidiaries,
                @JsonKeyEmployees = viewModel.JsonKeyEmployees,
                @JsonBoardOfDirectors = viewModel.JsonBoardOfDirectors,
                @JsonProduct = viewModel.JsonProduct,
                //@JsonClientsAndStrategy = viewModel.JsonClientsAndStrategy,
                @JsonClients = viewModel.JsonClients,
                @JsonStrategy = viewModel.JsonStrategy,
                @JsonNews = viewModel.JsonNews,
                @JsonSWOT = viewModel.JsonSWOT,
                @JsonEcosystemPresence = viewModel.JsonEcosystemPresence,
                @JsonTrends = viewModel.JsonTrends,
                //@JsonAnalystView = viewModel.JsonAnalystView,
                @JsonPriceVolume = viewModel.JsonPriceVolume,
               //@JsonESMSegment = viewModel.JsonESMSegment,
                @UserCreatedById = viewModel.UserCreatedById
            }, CommandType.StoredProcedure);

            var spTransactionMessage1 = new List<ImportCommonResponseModel>();
            if (spTransactionMessage.IsSuccess == true)
            {
                //var spTransactionMessage1 = _dbHelper.ExecuteQuery(ImportExportQueries.StoreProcUpdateMarketSizing1, null, CommandType.StoredProcedure);
                var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
                var spTransactionMain = dapper.Query<ImportCommonResponseModel>(ImportExportQueries.StoreProcUpdateCompanyProfile
                            , null
                            , null, true, null, CommandType.StoredProcedure);

                if (spTransactionMessage1 != null)
                {
                    spTransactionMessage1 = spTransactionMain.ToList();
                }
            }
            else
            {
                spTransactionMessage1.Add(new ImportCommonResponseModel { ErrorNotes = "Error while inserting data to staging tables.", Module = "Company Profile Import." });
            }
            
            return spTransactionMessage1;
        }
    }

}
