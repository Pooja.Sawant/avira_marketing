﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.ImportExport;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CPFinancialImportExportController : AviraBaseController
    {
        private ILoggerManager _logger;
        public readonly ICPFinancialImportExportService _cpFinancialImportExportService;

        public CPFinancialImportExportController(
            ICPFinancialImportExportService cpFinancialImportExportService,
            ILoggerManager logger)
        {
            _cpFinancialImportExportService = cpFinancialImportExportService;
            _logger = logger;
        }

        [HttpPost]
        public ActionResult Post([FromBody] CPFinancialImportInsertModel viewModel)
        {
            viewModel.UserCreatedById = CurrentUser.Id;
            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new CPFinancial, please contact Admin for more details."
            };
            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                var result = _cpFinancialImportExportService.Create(viewModel, CurrentUser);

                var res = result.Where(s => s.ErrorNotes != null).ToList();
                if (res.Count <= 0 || res.First().ErrorNotes == null)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "CPFinancial Details successfully Imported";
                }
                else
                {
                    viewData.Message = "Import CPFinancial have some errors";
                    viewData.IsSuccess = false;
                    //Export to Excel data.
                    viewData.CBIData = result.ToList();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Ok(viewData);
        }
    }
}