﻿CREATE TABLE [dbo].[TrendEcoSystemMap] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [TrendId]          UNIQUEIDENTIFIER NOT NULL,
    [EcoSystemId]      UNIQUEIDENTIFIER NOT NULL,
    [SubSegmentId]     UNIQUEIDENTIFIER NULL,
    [Impact]           UNIQUEIDENTIFIER NULL,
    [EndUser]          BIT              NOT NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_TrendEcoSystemMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TrendEcoSystemMap_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_TrendEcoSystemMap_Segment] FOREIGN KEY ([EcoSystemId]) REFERENCES [dbo].[Segment] ([Id]),
    CONSTRAINT [FK_TrendEcoSystemMap_SubSegment] FOREIGN KEY ([SubSegmentId]) REFERENCES [dbo].[SubSegment] ([Id]),
    CONSTRAINT [FK_TrendEcoSystemMap_Trend] FOREIGN KEY ([TrendId]) REFERENCES [dbo].[Trend] ([Id])
);






GO

GO

GO

GO

GO

GO

GO
CREATE NONCLUSTERED INDEX [IX_TrendEcoSystemMap_2]
    ON [dbo].[TrendEcoSystemMap]([TrendId] ASC, [EcoSystemId] ASC, [SubSegmentId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_TrendEcoSystemMap_1]
    ON [dbo].[TrendEcoSystemMap]([EcoSystemId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_TrendEcoSystemMap]
    ON [dbo].[TrendEcoSystemMap]([TrendId] ASC);

