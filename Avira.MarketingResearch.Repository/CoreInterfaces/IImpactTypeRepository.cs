﻿using Avira.MarketingResearch.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces
{
    public interface IImpactTypeRepository
    {
        IEnumerable<ImpactTypeViewModel> GetAll();
    }
}
