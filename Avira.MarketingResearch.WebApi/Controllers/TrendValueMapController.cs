﻿using Avira.MarketingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrendValueMapController : AviraBaseController
    {
        private readonly ITrendValueMapService _trendValueMapService;
        private readonly ITrendValueMapRepository _trendValueMapRepository;
        private ILoggerManager _logger;
        public TrendValueMapController(ITrendValueMapService trendValueMapService, ITrendValueMapRepository trendValueMapRepository, ILoggerManager logger)
        {
            _trendValueMapRepository = trendValueMapRepository;
            _trendValueMapService = trendValueMapService;
            _logger = logger;

        }
        [HttpGet]
        public ActionResult<IEnumerable<TrendValueViewModel>> Get(int PageStart = 0, int pageSize = 0, int SortDirection = 1, string OrdbyByColumnName = "CreatedDate", string Search = "", bool isDeletedInclude = false, string id = "")
        {
            List<TrendValueViewModel> res = null;
            try
            {
                res = _trendValueMapService.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude, id).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet]
        [Route("GetAllValueConversion")]
        public ActionResult<IEnumerable<ValueConversionViewModel>> GetAllValueConversion()
        {
            List<ValueConversionViewModel> res = null;
            try
            {
                res = _trendValueMapService.GetAllValueConversion().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet]
        [Route("GetImportance")]
        public ActionResult<IEnumerable<ImportanceViewModel>> GetAll_Importance()
        {
            List<ImportanceViewModel> res = null;
            try
            {
                res = _trendValueMapService.GetAllImportance().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet]
        [Route("GetImpactDirection")]
        public ActionResult<IEnumerable<ImpactDirectionViewModel>> GetAll_ImpactDirection()
        {
            List<ImpactDirectionViewModel> res = null;
            try
            {
                res = _trendValueMapService.GetAllImpactDirection().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet("{id}")]
        public ActionResult<IEnumerable<TrendValuePreviewViewModel>> GetById(Guid id)
        {
            List<TrendValuePreviewViewModel> res = null;
            try
            {
                res = _trendValueMapService.GetById(id).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpPost]
        public ActionResult Post([FromBody] MainTrendValueInsertUpdateDbViewModel viewModel)
        {
            viewModel.AviraUserId = CurrentUser.Id;

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new TrendValuesMap, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                var result = _trendValueMapService.InsertUpdate(viewModel, CurrentUser);
                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "TrendRegionMap is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }
    }
}