﻿CREATE TYPE [dbo].[udtIntDecimal] AS TABLE (
    [IntValue]     INT             NULL,
    [DecimalValue] DECIMAL (19, 4) NULL);

