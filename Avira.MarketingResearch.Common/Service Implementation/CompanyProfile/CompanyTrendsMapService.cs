﻿using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.CompanyProfile
{
    public class CompanyTrendsMapService : ICompanyTrendsMapService
    {
        private readonly ICompanyTrendsMapRepository _companyTrendsMapRepository;
        private readonly ICompanyNoteRepository _iCompanyNoteRepository;

        public CompanyTrendsMapService(ICompanyTrendsMapRepository companyTrendsMapRepository, ICompanyNoteRepository iCompanyNoteRepository)
        {
            _companyTrendsMapRepository = companyTrendsMapRepository;
            _iCompanyNoteRepository = iCompanyNoteRepository;
        }

        public IEnumerable<CompanyTrendsMapViewModel> GetAll(Guid companyId)
        {
            return _companyTrendsMapRepository.GetAll(companyId);
        }

        public Result<SpTransactionMessage> InsertUpdate(MainCompanyTrendsMapInsertUpdateDbViewModel tfc, AviraUser currenUser)
        {

            var res = _companyTrendsMapRepository.InsertUpdate(tfc);
            tfc.CompanyNoteModel.Id = Guid.NewGuid();
            tfc.CompanyNoteModel.UserCreatedById = tfc.AviraUserId;
            tfc.CompanyNoteModel.CompanyId = tfc.CompanyId;
            _iCompanyNoteRepository.Insert(tfc.CompanyNoteModel);

            return res;
        }
    }
}
