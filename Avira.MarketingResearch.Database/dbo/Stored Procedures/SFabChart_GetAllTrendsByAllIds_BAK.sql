﻿CREATE PROCEDURE [dbo].[SFabChart_GetAllTrendsByAllIds_BAK]
(
	--@IndustryIdList [dbo].[udtUniqueIdentifier] READONLY,
	--@MarketIdList [dbo].[udtUniqueIdentifier] READONLY,
	--@ProjectIdList [dbo].[udtUniqueIdentifier] READONLY,
	@ProjectId UniqueIdentifier,
	@ImpactDirection NVARCHAR(512) = NULL,
	@Impact NVARCHAR(512) = NULL,
	@ImportanceName NVARCHAR(512) = NULL,
	--@ImportanceIdList [dbo].[udtUniqueIdentifier] READONLY,
	--@ImpactDirectionIdList [dbo].[udtUniqueIdentifier] READONLY,
	@OutlookId UniqueIdentifier = null,
	@TimeTagName NVARCHAR(512) = NULL, 
	@AvirauserId UniqueIdentifier = null
		
)
as
/*================================================================================
Procedure Name: [SFabChart_GetAllTrendsByAllIds]
Author: Sai Krishna
Create date: 15/05/2019
Description: Get list from Trends by project Id, Market Id, Indiustry Id
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
15/05/2019	Sai Krishna		Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

------------Added by Praveen Start-----------------
DECLARE @ImportanceId UniqueIdentifier 

--SELECT * FROM ImpactType

--select rand() * 20 + 1
;with cteTrend (
TrendId,	TrendName	,ImportanceId,	ImportanceValue	,ImportanceName	,ImpactDirectionId	,TrendValue	)
AS
(
SELECT DISTINCT t.Id as TrendId,
		t.TrendName,
		t.ImportanceId,
		ISNULL(Imp.[Sequence],5) ImportanceValue,
		ISNULL(Imp.ImportanceName,'VeryLow') ImportanceName,
		t.ImpactDirectionId,
		ISNULL(t.TrendValue,0) TrendValue
		--,
		 --CASE WHEN ((ROW_NUMBER() over (order by t.Id)%2)) =  0 THEN (rand() * 20 + 1) ELSE (rand() * -20 + 1) END  as TrendValueWithDirection
		--ISNULL(t.TrendValue,0) * (CASE WHEN ISNULL(ImpD.ImpactDirectionName,'Neutral') = 'Positive' THEN 1 WHEN ISNULL(ImpD.ImpactDirectionName,'Neutral') = 'Negative' THEN -1 ELSE 0 END) TrendValueWithDirection
		--,
		--(
		--CASE WHEN (
		--SELECT TOP 1 Impt.ImpactTypeName FROM TrendProjectMap tpm 
		--JOIN ImpactType Impt ON tpm.Impact = Impt.Id
		--WHERE tpm.TrendId = t.Id AND tpm.ProjectId = t.ProjectID
		--) = '' THEN 'rgba(223, 83, 83, .5)' ELSE 'rgba(223, 83, 83, .5)' END
		--) 
		 --CASE WHEN ((ROW_NUMBER() over (order by t.Id)%2)) =  0 THEN 'rgba(223, 83, 83, .5)' ELSE 'rgba(119, 152, 191, .5)' END AS TrendImpactColor
from Trend t
INNER JOIN TrendProjectMap tpm ON tpm.TrendId = t.Id
LEFT JOIN Importance Imp ON t.ImportanceId = Imp.Id
LEFT JOIN ImpactDirection ImpD ON t.ImpactDirectionId = ImpD.Id
WHERE 
--t.ProjectID = @ProjectId
--ISNULL(ImpD.Id,'00000000-0000-0000-0000-000000000000') = CASE WHEN @ImpactDirectionId IS NOT NULL THEN @ImpactDirectionId ELSE ISNULL(ImpD.Id,'00000000-0000-0000-0000-000000000000') END 
--AND
 exists(select value from STRING_SPLIT(@ImpactDirection,',') WHERE value = ImpD.ImpactDirectionName )
and 
--ISNULL(@ImportanceName,'') = '' OR 
exists(select value from STRING_SPLIT(@ImportanceName,',') WHERE value = Imp.ImportanceName )


AND
ISNULL(@ImportanceId,'00000000-0000-0000-0000-000000000000') = 
--(CASE 
--				WHEN ISNULL(@Impact, '') != '' 
--				THEN 
				(SELECT TOP 1 Impt.Id FROM TrendProjectMap tpm JOIN ImpactType Impt ON tpm.Impact = Impt.Id WHERE Impt.ImpactTypeName = @Impact and tpm.TrendId = t.Id) 
				--ELSE
				--ISNULL(@ImportanceId,'00000000-0000-0000-0000-000000000000')
				--END)
AND
 --ISNULL(@TimeTagName,'') = '' or 
 exists (select 1 from TrendTimeTag
													inner join TimeTag tt
													on TrendTimeTag.TimeTagId = tt.id
													where t.Id = TrendTimeTag.TrendId and tt.TimeTagName = @TimeTagName)
)
SELECT *, 
CASE WHEN ((ROW_NUMBER() over (order by TrendId)%2)) =  0 THEN (rand() * 20 + 1) ELSE (rand() * -20 + 1) END  as TrendValueWithDirection,
CASE WHEN ((ROW_NUMBER() over (order by TrendId)%2)) =  0 THEN 'rgba(223, 83, 83, .5)' ELSE 'rgba(119, 152, 191, .5)' END AS TrendImpactColor
 FROM cteTrend

------------Added by Praveen End-------------------

--SELECT * FROM Trend



--Declare @impidlist bit,
--		@impdirelist bit,
--		@OutlookIdList [dbo].[udtUniqueIdentifier],
--		@Outlookname nvarchar(25) = null,
--		@UserRoleName nvarchar(50) = null,
--		@IndustryIdList [dbo].[udtUniqueIdentifier],
--		@MarketIdList [dbo].[udtUniqueIdentifier];
----set @impidlist = (select count(*) from @ImportanceIdList);
----set @impdirelist = (select count(*) from @ImpactDirectionIdList);

--Set @Outlookname = (select TimeTagName from TimeTag
--Where Id = @OutlookId)

--If(@Outlookname = 'ShortTerm')
--Begin
--	insert into @OutlookIdList
--	Select Id
--	From TimeTag
--	Where (TimeTagName = 'ShortTerm' or TimeTagName = 'Immediate')
--End

--If(@Outlookname = 'MidTerm')
--Begin
--	insert into @OutlookIdList
--	Select Id
--	From TimeTag
--	Where (TimeTagName = 'MidTerm')
--End

--If(@Outlookname = 'LongTerm')
--Begin
--	insert into @OutlookIdList
--	Select Id
--	From TimeTag
--	Where (TimeTagName = 'LongTerm' or TimeTagName = 'VeryLongTerm' or TimeTagName = 'Perennial')
--End
--Set @UserRoleName = (Select UserRoleName from UserRole 
--where Id = @AvirauserId)

--IF(@UserRoleName = 'Admin')
--Begin
--;with cteTIM as (
--Select tim.TrendId,tim.IndustryId
--from TrendIndustryMap tim
--Inner Join Industry indlist
--On tim.IndustryId = indlist.ID
--Where tim.IsDeleted = 0)
--,cteTimeTag as (
--select 
--row_number() over(partition by ttg.trendid order by seq desc) as rnum,
--ttg.TimeTagId,
--ttg.TrendId,
--ttg.Impact 
--from TrendTimeTag ttg
--inner join TimeTag tt
-- on ttg.TimeTagId = tt.Id
-- where ttg.TrendId in(select trendid from cteTIM)
-- and ttg.isDeleted = 0
-- )
--Select  t.Id as TrendId,
--		t.TrendName,
--		t.ImportanceId,
--		t.ImpactDirectionId,
--		ttag.TimeTagId as Outlook,
--		ttag.Impact
--from Trend t
--left Join cteTimeTag ttag 
--On ttag.TrendId = t.Id
--and ttag.rnum = 1
--Where  t.IsDeleted = 0
--and exists (select 1 from cteTIM
--	where t.Id = cteTIM.TrendId)
--	--and ttag.TimeTagId = @OutlookId
--	and ttag.TimeTagId in (Select Id from @OutlookIdList)
--	and (not exists(select 1 from @ImportanceIdList) or t.ImportanceId in (select ID from @ImportanceIdList))
--	and (not exists(select 1 from @ImpactDirectionIdList) or t.ImpactDirectionId in (select ID from @ImpactDirectionIdList)) 
--Order By t.TrendName

--;with cteTMM as (
--Select tmm.TrendId,tmm.MarketId
--from TrendMarketMap tmm
--Inner Join Market marlist
--On tmm.MarketId = marlist.ID
--Where tmm.IsDeleted = 0)
--,cteTimeTag as (
--select 
--row_number() over(partition by ttg.trendid order by seq desc) as rnum,
--ttg.TimeTagId,
--ttg.TrendId,
--ttg.Impact 
--from TrendTimeTag ttg
--inner join TimeTag tt
-- on ttg.TimeTagId = tt.Id
-- where ttg.TrendId in(select trendid from cteTMM)
-- and ttg.isDeleted = 0
-- )

--Select  t.Id as TrendId,
--		t.TrendName,
--		t.ImportanceId,
--		t.ImpactDirectionId,
--		ttag.TimeTagId	as Outlook,
--		ttag.Impact
--from Trend t
--Left Join cteTimeTag ttag 
--On ttag.TrendId = t.Id
--and ttag.rnum = 1
--Where t.IsDeleted = 0 
--and exists (select 1 from cteTMM 
--where t.Id = cteTMM.TrendId)
----and ttag.TimeTagId = @OutlookId
--	and ttag.TimeTagId in (Select Id from @OutlookIdList)
--	and (not exists(select 1 from @ImportanceIdList) or t.ImportanceId in (select ID from @ImportanceIdList))
--	and (not exists(select 1 from @ImpactDirectionIdList) or t.ImpactDirectionId in (select ID from @ImpactDirectionIdList))
--Order By t.TrendName


--;with cteTPM as (
--Select tpm.TrendId,tpm.ProjectId
--from TrendProjectMap tpm
--Where tpm.IsDeleted = 0 
--and tpm.ProjectId = @ProjectId
--)
--,cteTimeTag as (
--select 
--row_number() over(partition by ttg.trendid order by seq desc) as rnum,
--ttg.TimeTagId,
--ttg.TrendId,
--ttg.Impact 
--from TrendTimeTag ttg
--inner join TimeTag tt
-- on ttg.TimeTagId = tt.Id
-- where ttg.TrendId in(select trendid from cteTPM)
-- and ttg.isDeleted = 0
-- )

--Select  t.Id as TrendId,
--		t.TrendName,
--		t.ImportanceId,
--		t.ImpactDirectionId,
--		ttag.TimeTagId	as Outlook,
--		ttag.Impact
--from Trend t
--Left Join cteTimeTag ttag 
--On ttag.TrendId = t.Id
--and ttag.rnum = 1
--Where t.IsDeleted = 0 
--and exists (select 1 from cteTPM 
--where t.Id = cteTPM.TrendId)
----and ttag.TimeTagId = @OutlookId
--and ttag.TimeTagId in (Select Id from @OutlookIdList)
--and (not exists(select 1 from @ImportanceIdList) or t.ImportanceId in (select ID from @ImportanceIdList))
--and (not exists(select 1 from @ImpactDirectionIdList) or t.ImpactDirectionId in (select ID from @ImpactDirectionIdList))
--Order By t.TrendName
--End

--Else
--Begin
--Insert Into @IndustryIdList
--Select SegmentId
--From UserSegmentMap
--where CustomerUserId = @AvirauserId
--AND (SegmentType = 'BoardIndustries' 
--     OR SegmentType = 'Industries')

--Insert Into @MarketIdList
--Select SegmentId
--From UserSegmentMap
--where SegmentType = 'Market'
--AND CustomerUserId = @AvirauserId 

--IF(@ProjectId = null)
--Begin
--SET @ProjectId = 
--(Select Top(1) SegmentId
--From UserSegmentMap
--where SegmentType = 'Project'
--AND CustomerUserId = @AvirauserId)
--End

--;with cteTIM as (
--Select tim.TrendId,tim.IndustryId
--from TrendIndustryMap tim
--Inner Join @IndustryIdList indlist
--On tim.IndustryId = indlist.ID
--Where tim.IsDeleted = 0)
--,cteTimeTag as (
--select 
--row_number() over(partition by ttg.trendid order by seq desc) as rnum,
--ttg.TimeTagId,
--ttg.TrendId,
--ttg.Impact 
--from TrendTimeTag ttg
--inner join TimeTag tt
-- on ttg.TimeTagId = tt.Id
-- where ttg.TrendId in(select trendid from cteTIM)
-- and ttg.isDeleted = 0
-- )
--Select  t.Id as TrendId,
--		t.TrendName,
--		t.ImportanceId,
--		t.ImpactDirectionId,
--		ttag.TimeTagId as Outlook,
--		ttag.Impact
--from Trend t
--left Join cteTimeTag ttag 
--On ttag.TrendId = t.Id
--and ttag.rnum = 1
--Where  t.IsDeleted = 0
--and exists (select 1 from cteTIM
--	where t.Id = cteTIM.TrendId)
--	--and ttag.TimeTagId = @OutlookId
--	and ttag.TimeTagId in (Select Id from @OutlookIdList)
--	and (not exists(select 1 from @ImportanceIdList) or t.ImportanceId in (select ID from @ImportanceIdList))
--	and (not exists(select 1 from @ImpactDirectionIdList) or t.ImpactDirectionId in (select ID from @ImpactDirectionIdList)) 
--Order By t.TrendName

--;with cteTMM as (
--Select tmm.TrendId,tmm.MarketId
--from TrendMarketMap tmm
--Inner Join @MarketIdList marlist
--On tmm.MarketId = marlist.ID
--Where tmm.IsDeleted = 0)
--,cteTimeTag as (
--select 
--row_number() over(partition by ttg.trendid order by seq desc) as rnum,
--ttg.TimeTagId,
--ttg.TrendId,
--ttg.Impact 
--from TrendTimeTag ttg
--inner join TimeTag tt
-- on ttg.TimeTagId = tt.Id
-- where ttg.TrendId in(select trendid from cteTMM)
-- and ttg.isDeleted = 0
-- )

--Select  t.Id as TrendId,
--		t.TrendName,
--		t.ImportanceId,
--		t.ImpactDirectionId,
--		ttag.TimeTagId	as Outlook,
--		ttag.Impact
--from Trend t
--Left Join cteTimeTag ttag 
--On ttag.TrendId = t.Id
--and ttag.rnum = 1
--Where t.IsDeleted = 0 
--and exists (select 1 from cteTMM 
--where t.Id = cteTMM.TrendId)
----and ttag.TimeTagId = @OutlookId
--	and ttag.TimeTagId in (Select Id from @OutlookIdList)
--	and (not exists(select 1 from @ImportanceIdList) or t.ImportanceId in (select ID from @ImportanceIdList))
--	and (not exists(select 1 from @ImpactDirectionIdList) or t.ImpactDirectionId in (select ID from @ImpactDirectionIdList))
--Order By t.TrendName

--;with cteTPM as (
--Select tpm.TrendId,tpm.ProjectId
--from TrendProjectMap tpm
----Inner Join @ProjectIdList prjlist
----On tpm.ProjectId = prjlist.ID
--Where tpm.IsDeleted = 0 
--and tpm.ProjectId = @ProjectId
--)
--,cteTimeTag as (
--select 
--row_number() over(partition by ttg.trendid order by seq desc) as rnum,
--ttg.TimeTagId,
--ttg.TrendId,
--ttg.Impact 
--from TrendTimeTag ttg
--inner join TimeTag tt
-- on ttg.TimeTagId = tt.Id
-- where ttg.TrendId in(select trendid from cteTPM)
-- and ttg.isDeleted = 0
-- )

--Select  t.Id as TrendId,
--		t.TrendName,
--		t.ImportanceId,
--		t.ImpactDirectionId,
--		ttag.TimeTagId	as Outlook,
--		ttag.Impact
--from Trend t
--Left Join cteTimeTag ttag 
--On ttag.TrendId = t.Id
--and ttag.rnum = 1
--Where t.IsDeleted = 0 
--and exists (select 1 from cteTPM 
--where t.Id = cteTPM.TrendId)
----and ttag.TimeTagId = @OutlookId
--	and ttag.TimeTagId in (Select Id from @OutlookIdList)
--	and (not exists(select 1 from @ImportanceIdList) or t.ImportanceId in (select ID from @ImportanceIdList))
--	and (not exists(select 1 from @ImpactDirectionIdList) or t.ImpactDirectionId in (select ID from @ImpactDirectionIdList))
--Order By t.TrendName
--End
END