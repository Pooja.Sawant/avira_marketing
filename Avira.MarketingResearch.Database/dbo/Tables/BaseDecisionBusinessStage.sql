﻿CREATE TABLE [dbo].[BaseDecisionBusinessStage] (
    [BaseDecisionId]  UNIQUEIDENTIFIER NOT NULL,
    [BusinessStageId] UNIQUEIDENTIFIER NULL,
    FOREIGN KEY ([BusinessStageId]) REFERENCES [dbo].[LookupCategory] ([Id]),
    CONSTRAINT [FK__BaseDecis__BaseD__5DC26471] FOREIGN KEY ([BaseDecisionId]) REFERENCES [dbo].[BaseDecision] ([Id])
);

