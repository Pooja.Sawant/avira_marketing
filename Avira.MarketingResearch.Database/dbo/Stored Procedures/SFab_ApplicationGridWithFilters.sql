﻿CREATE PROCEDURE [dbo].[SFab_ApplicationGridWithFilters]
	(@ApplicationIDList dbo.udtUniqueIdentifier READONLY,
	@ApplicationName nvarchar(256)='',
	@Description nvarchar(256)='',
	@includeDeleted bit = 0,
	@OrdbyByColumnName NVARCHAR(50) ='CreatedDate',
	@SortDirection INT = -1,
	@PageStart INT = 0,
	@PageSize INT = Null)
	AS
/*================================================================================
Procedure Name: SApp_GetApplicationbyID
Author: Adarsh
Create date: 03/16/2019
Description: Get list from Application table by Name or ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
03/16/2019	Adarsh			Initial Version
__________	____________	______________________________________________________
04/05/2019	Pooja			Paging Changes
11/05/2019	Praveen			Replace application table with Subsegment table
================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
if @includeDeleted is null set @includeDeleted = 0;
if len(@ApplicationName)>2 set @ApplicationName = '%'+ @ApplicationName + '%'
else set @ApplicationName = ''
if len(@Description)>2 set @Description = '%'+ @Description + '%';
else set @Description = '';
	if @PageSize is null or @PageSize =0
	set @PageSize = 10
	begin
	;WITH CTE_TBL AS (

	SELECT [Application].[Id],
		[Application].SubSegmentName as [ApplicationName],
		[Application].[Description],
		[Application].[IsDeleted],
		CASE
		WHEN @OrdbyByColumnName = 'ApplicationName' THEN [Application].SubSegmentName
		WHEN @OrdbyByColumnName = 'Description' THEN [Application].[Description]
		ELSE [Application].SubSegmentName
	END AS SortColumn
	
FROM [dbo].SubSegment [Application]
INNER JOIN Segment ON [Application].SegmentId = Segment.Id AND Segment.SegmentName = 'Applications'
where (NOT EXISTS (SELECT * FROM @ApplicationIDList) or  [Application].Id  in (select * from @ApplicationIDList))
	AND (@ApplicationName = '' or [Application].SubSegmentName like @ApplicationName)
	AND (@Description = '' or [Application].[Description] like @Description)
	AND (@includeDeleted = 1 or [Application].IsDeleted = 0)
	)

	SELECT 
		  CTE_TBL.*,
		  tCountResult.TotalItems
		FROM CTE_TBL
		CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult
		ORDER BY 
		  CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,
		  CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC 
		OFFSET @PageStart ROWS
		FETCH NEXT @PageSize ROWS ONLY;
		END
	END