﻿



CREATE procedure [dbo].[SApp_insertTrendCompanyGroupMap]      
(
@TrendId uniqueidentifier,   
@TrendCompanyGroupmap dbo.[udtGUIDGUIDMapValue] readonly,
@UserCreatedById uniqueidentifier
)      
as      
/*================================================================================      
Procedure Name: SApp_InsertTrendMarketMap      
Author: Sai      
Create date: 04/12/2019      
Description: Insert a record into TrendCompanyGroupMap table      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
04/18/2019 Sai Krishna   Initial Version    
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)  
    
--Validations      
--IF exists(select 1 from [dbo].[TrendMarketMap]       
--   where TrendId = @TrendId AND MarketId = @MarketId AND IsDeleted = 0)      
--begin       
--set @ErrorMsg = 'Market with Id "'+ CONVERT (nvarchar(30), @MarketId) + '" already exists.';    
--SELECT StoredProcedureName ='SApp_InsertTrendMarketMap',Message =@ErrorMsg,Success = 0;    
--RETURN;    
--end      
--End of Validations      
BEGIN TRY         
--mark records as delete which are not in list
--start hide by Pooja
--update [dbo].[TrendMarketMap]
--set IsDeleted = 1,
--UserDeletedById = @UserCreatedById,
--DeletedOn = GETDATE()
--from [dbo].[TrendCompanyGroupMap]
--where [TrendCompanyGroupMap].TrendId = @TrendId
--and not exists (select 1 from @TrendCompanyGroupmap map where map.id = [TrendCompanyGroupMap].CompanyGroupId)
-- hide pevious code by Pooja 290619
 --Update records for change
update [dbo].[TrendCompanyGroupMap]
set Impact = map.mapid,
EndUser = map.bitvalue,
IsDeleted = CASE WHEN map.mapid IS NULL THEN 1 ELSE IsDeleted END,
ModifiedOn = GETDATE(),
UserModifiedById = @UserCreatedById
from [dbo].[TrendCompanyGroupMap]
inner join @TrendCompanyGroupmap map 
on map.id = [TrendCompanyGroupMap].CompanyGroupId
where [TrendCompanyGroupMap].TrendId = @TrendId;

--Insert new records
insert into [dbo].[TrendCompanyGroupMap]([Id],      
        [TrendId],      
        [CompanyGroupId], 
		[Impact],
		[EndUser],
        [IsDeleted],      
        [UserCreatedById],      
        [CreatedOn])      
 select NEWID(),
   @TrendId,       
   map.ID as [CompanyGroupId],
   map.mapid as [Impact],     
   map.bitvalue,     
   0,      
   @UserCreatedById,       
   GETDATE()
 from @TrendCompanyGroupmap map
 where not exists (select 1 from [dbo].[TrendCompanyGroupMap] map1
					where map1.TrendId = @TrendId
					and map.ID = map1.CompanyGroupId);       
     
SELECT StoredProcedureName ='SApp_InsertTrendCompanyGroupMap',Message =@ErrorMsg,Success = 1;       
    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);      
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());      
    
 set @ErrorMsg = 'Error while insert a new TrendMarketMap, please contact Admin for more details.';    
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
       
END CATCH      
      
      
end