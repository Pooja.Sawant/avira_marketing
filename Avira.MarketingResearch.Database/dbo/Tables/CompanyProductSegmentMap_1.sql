﻿CREATE TABLE [dbo].[CompanyProductSegmentMap] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [CompanyProductId] UNIQUEIDENTIFIER NULL,
    [SegmentId]        UNIQUEIDENTIFIER NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_CompanyProductSegment_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CompanyProductSegment] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CompanyProductSegment_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyProductSegment_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyProductSegment_AviraUser2] FOREIGN KEY ([UserDeletedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyProductSegment_CompanyProduct] FOREIGN KEY ([CompanyProductId]) REFERENCES [dbo].[CompanyProduct] ([Id]),
    CONSTRAINT [FK_CompanyProductSegment_Segment] FOREIGN KEY ([SegmentId]) REFERENCES [dbo].[Segment] ([Id])
);

