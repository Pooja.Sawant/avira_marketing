﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SApp_Lookupstatusbystatusname]
	-- Add the parameters for the stored procedure here
	(@StatusType nvarchar(10))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Id,
StatusType,
StatusName FROM LookupStatus WHERE   StatusType= @StatusType ORDER BY StatusName 
END