﻿
CREATE procedure [dbo].[SFab_GetESMAnalysisbyID]
(@ProjectID uniqueidentifier=null,
@SegmentId uniqueidentifier=null,
@SubSegmentId uniqueidentifier=null,
@AviraUserID uniqueidentifier = null)
as
/*================================================================================
Procedure Name: SFab_GetESMAnalysisbyID
Author: Harshal 
Create date: 12/09/2019
Description: Get list from ESMAnalysis table by Filters
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
12/05/2019	Harshal			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT  ESMAnalysis.Id,ESMAnalysis.AnalysisText FROM ESMAnalysis 
Where ESMAnalysis.SegmentId=@SegmentId
AND exists (select 1 from  ESMSegmentMapping
      where ESMAnalysis.SegmentId = ESMSegmentMapping.SegmentId
	  AND (ESMAnalysis.SubSegmentId=@SubSegmentId or ESMAnalysis.SubSegmentId is null)
      AND ESMSegmentMapping.ProjectId = @ProjectID AND ESMSegmentMapping.IsDeleted=0 ) 
end