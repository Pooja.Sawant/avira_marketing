﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq; 
using System.Data;
using SQLDatabaseQueryContainer;
using Dapper;

namespace Avira.MarketingResearch.Repository.Implementations.Trend
{
    public class TrendKeyWordMapRepository : ITrendKeywordMapRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "KeyWord";
        public TrendKeyWordMapRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public Result<SpTransactionMessage> Create(TrendKeyWordMapInsertDbViewModel1 keyWordInsertDbViewModel)
        {
            DataTable userRoleListIdsTable = new DataTable();
            userRoleListIdsTable.Columns.Add("ID", typeof(Guid));
            userRoleListIdsTable.Columns.Add("Value", typeof(string));

            if (keyWordInsertDbViewModel.TrendNames.Count > 0)
            {
                foreach (var item in keyWordInsertDbViewModel.TrendNames)
                {
                    userRoleListIdsTable.Rows.Add(new Object[] { item.Id, item.TrendName });
                }
            }
            var spTransactionMessage = _dbHelper.ExecuteQuery(TrendKeywordMapQueries.StoreProcInsert,
                new
                {
                    @TrendKeyWordList = userRoleListIdsTable.AsTableValuedParameter("dbo.udtGUIDVarcharValue"),
                    @TrendId = keyWordInsertDbViewModel.TrendId,
                    @UserCreatedById = keyWordInsertDbViewModel.UserCreatedById,
                    @CreatedOn = keyWordInsertDbViewModel.CreatedOn
                }
                , CommandType.StoredProcedure);
            return spTransactionMessage;
        }

        public Result<SpTransactionMessage> Update(TrendKeyWordMapUpdateDbViewModel keyWordUpdateDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(TrendKeywordMapQueries.StoreProcUpdate,
               new
               {
                   @Id = keyWordUpdateDbViewModel.Id,
                   @TrendKeyWord = keyWordUpdateDbViewModel.TrendKeyWord,
                   @TrendId = keyWordUpdateDbViewModel.TrendId,
                   @IsDeleted = keyWordUpdateDbViewModel.IsDeleted,
                   @UserModifiedById = keyWordUpdateDbViewModel.UserModifiedById,
                   @DeletedOn = keyWordUpdateDbViewModel.DeletedOn,
                   @UserDeletedById = keyWordUpdateDbViewModel.UserDeletedById,
                   @ModifiedOn = keyWordUpdateDbViewModel.ModifiedOn
               }, CommandType.StoredProcedure);
            return spTransactionMessage;
        }

        public IEnumerable<TrendKeyWordMapViewModel> GetAll(Guid? guid, int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted)
        {
            throw new NotImplementedException();
        }

        //public IEnumerable<TrendViewModel> GetAllTrendNames()
        //{
        //    var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
        //    return dapper.Query<TrendViewModel>(TrendKeywordMapQueries.StoreProcSelectAllTrendNames
        //                    , null
        //                    , null, true, null, CommandType.StoredProcedure);
        //}

        public IEnumerable<TrendKeywordNames> GetAllTrendNames()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<TrendKeywordNames>(TrendKeywordMapQueries.StoreProcSelectAllTrendNames
                            , null
                            , null, true, null, CommandType.StoredProcedure);
        }


        public IEnumerable<TrendKeyWordMapViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted)
        {
            return GetAllKewwords(null, PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, includeDeleted);
        }
        private IEnumerable<TrendKeyWordMapViewModel> GetAllKewwords(Guid? guid, int PageStart = 0, int pageSize = 0, int SortDirection = -1, string OrdbyByColumnName = "", string Search = "", bool includeDeleted = false)
        {

            DataTable trendKeywordsIdsTable = new DataTable();
            trendKeywordsIdsTable.Columns.Add("ID", typeof(Guid));

            if (guid != null)
            {
                trendKeywordsIdsTable.Rows.Add(guid);
            }
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<TrendKeyWordMapViewModel>(TrendKeywordMapQueries.StoreProcSelectAll
                            , new { @TrendKeywordsIDList = trendKeywordsIdsTable.AsTableValuedParameter("dbo.udtUniqueIdentifier"), @includeDeleted = includeDeleted, @PageStart = PageStart, @pageSize = pageSize, @SortDirection = SortDirection, @OrdbyByColumnName = OrdbyByColumnName, @TrendKeyWord = Search }
                            , null, true, null, CommandType.StoredProcedure);
        }

        public IEnumerable<TrendKeyWordMapViewModel> GetById(Guid guid)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<TrendKeyWordMapViewModel>(TrendKeywordMapQueries.StoreProcSelectByTrendID,
               new { TrendId = guid }, null, true, null
               , CommandType.StoredProcedure);
        }

        public TrendKeywordNames GetTrendNameByID(Guid guid)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<TrendKeywordNames>(TrendKeywordMapQueries.StoreProcSelectTrendByTrendId,
                new { TrendId = guid }, null, true, null, CommandType.StoredProcedure).FirstOrDefault();
        }

    }
}
