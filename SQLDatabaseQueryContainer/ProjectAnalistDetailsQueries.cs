﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public static class ProjectAnalistDetailsQueries
    {
        public static readonly string StoreProcInsert = "SApp_insertProjectAnalistDetails";

        public static readonly string StoreProcUpdate = "SApp_UpdateProjectAnalistDetails";

        public static readonly string StoreProcSelectById = "[SApp_GetProjectAnalistDetailsbyID]";

        public static readonly string StoreProcSelectAll = "SApp_ProjectAnalistDetailsGridWithFilters";

      
    }
}
