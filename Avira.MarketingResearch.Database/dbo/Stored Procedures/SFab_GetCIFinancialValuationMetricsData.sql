﻿
--drop Procedure SFab_GetCIFinancialValuationMetricsData]

--execute [SFab_GetCIFinancialValuationMetricsData]  @CompanyID = '7cfa9e8e-d78d-41d9-be62-4e49fdc46fa6'

CREATE   Procedure [dbo].[SFab_GetCIFinancialValuationMetricsData] 
(
	@CompanyID uniqueidentifier 
)
As
/*================================================================================  
Procedure Name: [SFab_GetCIFinancialValuationMetricsTab]  
Author: Nitin 
Create date: 06-Nov-2019  
Description: Get CI Company Financial Analysis Valuation Metrics Tab Data
Change History  
Date         Developer     Reason  
____________ __________  ______________________________________________________  
06-Nov-2019  Nitin       Initial Version  
================================================================================*/  
BEGIN

	--Declare @CompanyID uniqueidentifier = '7c10786c-dfad-4d2d-a25f-4d69b13b9a2f'

	Declare @PriceUSD  decimal(19, 4)
	Declare @PriceInReportingCurrency decimal(19, 4)
	Declare @ReportingCurrencyCode nvarchar(20)
	Declare @ReportingUnitName nvarchar(512)
	Declare @PriceDate DateTime
	Declare @UnitId uniqueidentifier
	Declare @USDCurrencyId uniqueidentifier

	Select @USDCurrencyId = [dbo].[fun_GetCurrencyIDForUSD]()

	Select @UnitId = Id 
	From ValueConversion
	Where ValueName = 'Millions';
	
	Declare @Years Table (CompanyID uniqueidentifier, [Year] int)
	Declare @MaxYear int, @MinYear int

	Select @MaxYear = Max([Year]), @MinYear = Min([Year])
	From CompanyPLStatement
	Where CompanyId = @CompanyID;	
	
	If (@MaxYear Is Not Null And @MinYear Is Not Null)
	Begin
		WITH CTE
			 AS (SELECT @CompanyID As CompanyID, @MinYear As Year1
				 UNION ALL
				 SELECT @CompanyID As CompanyID, Year1 + 1 
				 FROM   CTE
				 WHERE  Year1 < @MaxYear)
		Insert Into @Years
		SELECT *
		FROM   CTE 
	End

	Declare @CICompanyFinancialValuationMetricsData Table(
		CompanyId	uniqueidentifier,
		[Year]	int,
		CashAndCashEquivalents	decimal (19, 4),
		MinorityInterest decimal (19, 4),
		TotalDebt decimal (19, 4),
		PriceUSD  decimal (19, 4),
		PriceInReportingCurrency decimal (19, 4),
		ReportingCurrencyCode nvarchar(20),
		PriceDate DateTime,
		WeightedAverageSharesOutstandingDiluted decimal (29, 4),
		MarketCapitalization decimal (19, 4),
		EnterpriseValue decimal (19, 4),
		Revenue decimal (19, 4),
		EBITDA decimal (19, 4),
		EBIT decimal (19, 4),
		NetIncome decimal (19, 4),
		EarningsPerShare decimal (19, 4),
		RevenueMultiple decimal (19, 4),
		EBITDAMultiple decimal (19, 4),
		EBITMultiple decimal (19, 4),
		PriceEarningsRatio decimal (19, 4),
		GrossProfit decimal (19,4)
		
		
	)

	Select Top 1 @PriceUSD = [dbo].[fun_UnitDeConversion](CPV.CurrencyUnitId, CPV.Price),
		@PriceInReportingCurrency = [dbo].[fun_UnitDeConversion](CPV.CurrencyUnitId, [dbo].[fun_CurrencyConversion] (CPV.Price, @USDCurrencyId, CPV.CurrencyId, null)), 
		--@PriceInReportingCurrency =[dbo].[fun_CurrencyConversion] (CPV.Price, @USDCurrencyId, CPV.CurrencyId, null), 
		@ReportingCurrencyCode = C.CurrencyCode,
		@PriceDate = CPV.[Date]
	From CompanyPriceVolume CPV 
		Inner Join Currency C 
			On CPV.CurrencyId = C.Id
		Inner Join ValueConversion VC
			On CPV.CurrencyUnitId = VC.Id
	Where CompanyId =  @CompanyID 
	Order By [Date] Desc
	print 'a'
	
	Insert Into @CICompanyFinancialValuationMetricsData (CompanyId, [Year], CashAndCashEquivalents, MinorityInterest, TotalDebt,GrossProfit)
	Select Y.CompanyId, Y.[Year], 
		[dbo].[fun_UnitDeConversion](@UnitId, Cast(CBS.CashAndCashEquivalents as decimal(19,2)) + Cast(CBS.ShortTermInvestments As decimal(19, 2))) As CashAndCashEquivalents,
		[dbo].[fun_UnitDeConversion](@UnitId, Cast(CBS.MinorityInterest as decimal(19, 2))) As MinorityInterest,
		[dbo].[fun_UnitDeConversion](@UnitId, Cast(CBS.ShortTermDebt as decimal(19, 2)) + Cast(CBS.LongTermDebt as decimal(19, 2))) As TotalDebt,
[dbo].[fun_UnitDeConversion](@UnitId, CPLS.GrossProfit) As GrossProfit
	From @Years Y Left Join CompanyBalanceSheet CBS On Y.[Year] = CBS.[Year] And CBS.CompanyId = @CompanyID	
	inner join  CompanyPLStatement CPLS On CBS.CompanyId= CPLS.CompanyId
	Order By Y.[Year]

	Update @CICompanyFinancialValuationMetricsData
	Set PriceUSD =  @PriceUSD, 
		PriceInReportingCurrency = @PriceInReportingCurrency, 
		ReportingCurrencyCode = @ReportingCurrencyCode, 
		PriceDate = @PriceDate
	Where [Year] = @MaxYear 
	
	Update @CICompanyFinancialValuationMetricsData Set
		WeightedAverageSharesOutstandingDiluted = CPLS.WeightedAverageSharesOutstandingDiluted,		
		Revenue = Case When CPLS.Revenue Is Null Then Null Else [dbo].[fun_UnitDeConversion](@UnitId, CPLS.Revenue) End,
		EBITDA = [dbo].[fun_UnitDeConversion](@UnitId, CPLS.EBITDA),
		EBIT = [dbo].[fun_UnitDeConversion](@UnitId, CPLS.EBIT),
		NetIncome = [dbo].[fun_UnitDeConversion](@UnitId, CPLS.NetIncome),
		EarningsPerShare = [dbo].[fun_UnitDeConversion](@UnitId, CPLS.DilutedEPS),
		[GrossProfit]=[dbo].[fun_UnitDeConversion](@UnitId, CPLS.GrossProfit)
	From @CICompanyFinancialValuationMetricsData T Inner Join CompanyPLStatement CPLS On T.CompanyId = CPLS.CompanyId And T.[Year] = CPLS.[Year]

	BEGIN TRY
		Update @CICompanyFinancialValuationMetricsData Set
			MarketCapitalization = WeightedAverageSharesOutstandingDiluted * @PriceUSD
	END TRY  
	BEGIN CATCH
		Update @CICompanyFinancialValuationMetricsData Set MarketCapitalization = 0
	END CATCH  

	Update @CICompanyFinancialValuationMetricsData
	Set EnterpriseValue = MarketCapitalization + TotalDebt + MinorityInterest - CashAndCashEquivalents

	Update @CICompanyFinancialValuationMetricsData
	Set RevenueMultiple = EnterpriseValue / NULLIF(Revenue, 0),
		EBITDAMultiple = EnterpriseValue / NULLIF(EBITDA, 0),
		EBITMultiple = EnterpriseValue / NULLIF(EBIT, 0),
		PriceEarningsRatio = @PriceUSD / NULLIF(EarningsPerShare, 0)
	
	Select top 7  CompanyID, [Year], CashAndCashEquivalents, MinorityInterest, TotalDebt, PriceUSD, PriceInReportingCurrency, ReportingCurrencyCode,
		PriceDate, WeightedAverageSharesOutstandingDiluted, MarketCapitalization, EnterpriseValue, Revenue, EBITDA, EBIT,GrossProfit,
		NetIncome, EarningsPerShare, RevenueMultiple, EBITDAMultiple, EBITMultiple, PriceEarningsRatio, GrossProfit / Revenue As ProfitMargin, EBIT / Revenue As OPeratingMargin
	From @CICompanyFinancialValuationMetricsData
	Order By [Year] Desc
END