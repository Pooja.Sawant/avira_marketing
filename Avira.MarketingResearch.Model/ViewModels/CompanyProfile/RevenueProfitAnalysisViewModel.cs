﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.CompanyProfile
{
    public class RevenueProfitAnalysisCurrencyViewModel
    {
        public Guid Id { get; set; }
        public string CurrencyName { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencySymbol { get; set; }
        public string Fractionalunit { get; set; }
        public string RowType
        {
            get
            {
                return "RevenueProfitAnalysisCurrency";
            }
        }
    }

    public class RevenueProfitAnalysisValueConversionViewModel
    {
        public Guid Id { get; set; }
        public string ValueName { get; set; }
        public decimal Conversion { get; set; }
        public string RowType
        {
            get
            {
                return "RevenueProfitAnalysisValueConversion";
            }
        }
    }

    public class RevenueProfitAnalysisRegionViewModel
    {
        public Guid Id { get; set; }
        public string RegionName { get; set; }
        public int RegionLevel { get; set; }
        public string RowType
        {
            get
            {
                return "RevenueProfitAnalysisRegion";
            }
        }
    }

    public class CompanyRevenueViewModel
    {
        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
        public Guid CurrencyId { get; set; }
        public Guid UnitId { get; set; }
        public decimal TotalRevenue { get; set; }
        public int Year { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public string RowType
        {
            get
            {
                return "CompanyRevenue";
            }
        }
    }

    public class CompanyProfitViewModel
    {
        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
        public Guid CurrencyId { get; set; }
        public Guid UnitId { get; set; }
        public decimal TotalProfit { get; set; }
        public int Year { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public string RowType
        {
            get
            {
                return "CompanyProfit";
            }
        }
    }

    public class CompanyGeographicSplitViewModel
    {
        public Guid Id { get; set; }
        public Guid CompanyRevenueId { get; set; }
        public Guid RegionId { get; set; }
        public decimal RegionValue { get; set; }
        public int Year { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public string RowType
        {
            get
            {
                return "CompanyGeographicSplit";
            }
        }
    }

    public class CompanyProfitGeographicSplitViewModel
    {
        public Guid Id { get; set; }
        public Guid CompanyProfitId { get; set; }
        public Guid RegionId { get; set; }
        public decimal RegionValue { get; set; }
        public int Year { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public string RowType
        {
            get
            {
                return "CompanyProfitGeographicSplit";
            }
        }
    }

    public class CompanySegmentationSplitViewModel
    {
        public Guid CompanyRevenueId { get; set; }
        public int Split { get; set; }
        public int Year { get; set; }
        public List<SegmentSplitViewModel> Segment { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public string RowType
        {
            get
            {
                return "CompanySegmentationSplit";
            }
        }
    }

    public class CompanyProfitSegmentationSplitViewModel
    {
        public Guid CompanyProfitId { get; set; }
        public int Split { get; set; }
        public int Year { get; set; }
        public List<SegmentSplitViewModel> Segment { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public string RowType
        {
            get
            {
                return "CompanyProfitSegmentationSplit";
            }
        }
    }

    public class SegmentSplitViewModel
    {
        public Guid Id { get; set; }
        public string Segmentation { get; set; }
        public decimal SegmentValue { get; set; }
        public string RowType
        {
            get
            {
                return "CompanySegmentationSplit";
            }
        }
    }

    public class MainRevenueProfitAnalysisInsertUpdateDbViewModel
    {
        public Guid CompanyId { get; set; }
        public Guid ProjectId { get; set; }
        public CompanyRevenueViewModel CompanyRevenueInsertUpdate { get; set; }
        public CompanyProfitViewModel CompanyProfitInsertUpdate { get; set; }
        public List<CompanyGeographicSplitViewModel> CompanyGeographicSplitInsertUpdate { get; set; }
        public List<CompanyProfitGeographicSplitViewModel> CompanyProfitGeographicSplitInsertUpdate { get; set; }
        public List<CompanySegmentationSplitViewModel> CompanySegmentationSplitInsertUpdate { get; set; }
        public List<CompanyProfitSegmentationSplitViewModel> CompanyProfitSegmentationSplitInsertUpdate { get; set; }
        public Guid AviraUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public CompanyViewModel CompanyModel { get; set; }
        public CompanyNoteViewModel CompanyNoteModel { get; set; }
    }
}


