﻿  
CREATE PROCEDURE [dbo].[SFab_GetMarketSizingData_MapChart]
(
 @SegmentID uniqueidentifier=null,
 @ProjectID uniqueidentifier=null,
 @SegmentIdList dbo.[udtUniqueIdentifier] readonly,
 @RegionIdList dbo.[udtUniqueIdentifier] readonly,
 @CountryIdList dbo.[udtUniqueIdentifier] readonly,
 @TimeTagId uniqueidentifier = null,
 @CurrencyId uniqueidentifier = null,
@CaptureJson bit = 0
)  
AS  
/*================================================================================  
Procedure Name: [SFab_GetMarketSizingData]  
Author: Swami Aluri  
Create date: 06/04/2019  
Description: Get list of Marketing sizing from table   
Change History  
Date        Developer     Reason  
__________  ____________  ______________________________________________________  
06/04/2019   Swami Aluri   Initial Version  
================================================================================*/  

BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  

	declare @USDCurrencyId uniqueidentifier	
	select @USDCurrencyId = Id from Currency
	where CurrencyName = 'United States dollar';
	If @CurrencyId is null 
	set @CurrencyId = @USDCurrencyId;
	declare @Today datetime = getdate();
	
	declare @RegionLevel tinyint;
	set @RegionLevel = 1;
	DECLARE @RegionList dbo.[udtUniqueIdentifier];

	IF not exists (select 1 from @RegionIdList)
	begin
	insert into @RegionList
	SELECT id FROM Region WHERE RegionName = 'Global'
	end
	else
	begin
	insert into @RegionList 
	select id from @RegionIdList;
	end

	while (@RegionLevel < = 3)
	begin
	set @RegionLevel = @RegionLevel +1;

	insert into @RegionList 
	select ID
	from Region
	where RegionLevel = @RegionLevel
	and exists (select 1 from @RegionList ParentRegion 
				where Region.ParentRegionId = ParentRegion.ID)

	end


	declare @CountryIdFilterList dbo.[udtUniqueIdentifier];

	if exists (select 1 from @CountryIdList)
	begin
	insert into @CountryIdFilterList
	select Id
	from country
	where exists(select 1 from @CountryIdList list
				inner join CountryRegionMap CRM
				on list.ID = CRM.CountryId
				inner join @RegionList RList
				on CRM.RegionId = RList.ID
				where country.id = CRM.CountryId
				);
	end
	else
	begin
	insert into @CountryIdFilterList
	select Id
	from country
	where exists(select 1 from CountryRegionMap CRM
				inner join @RegionList RList
				on CRM.RegionId = RList.ID
				where country.id = CRM.CountryId
				);
	end
    DECLARE @Years int;
    DECLARE @YearMin int;
    DECLARE @YearMax int;
    if @TimeTagId is not null
	begin
    SELECT @Years = DurationYears FROM TimeTag WHERE Id =@TimeTagId;
	end
	else
	begin
	SELECT @Years = DurationYears FROM TimeTag WHERE TimeTagName = 'LongTerm';
	end
    select @YearMin = BaseYear  from BaseYearMaster
    select @YearMax = @YearMin + @Years 

CREATE TABLE #tmp_MarketValue(
	[Value] [decimal](18, 4),
	[Volume] [decimal](19, 4),
	[GrowthRatePercentage] [decimal](19, 4),
	[TrendGrowthRatePercentage] [decimal](19, 4),
	[Amount] [decimal](18, 4),
	[Year] [int],
	[CountryId] [uniqueidentifier],
	[AvgSalePrice] [decimal](18, 4),
	[GraphOptions] [nvarchar](512),
	[SegmentId] [uniqueidentifier]);

declare @MarketSegmentList table ([SegmentId] [uniqueidentifier],
							SegmentName [nvarchar](256))
insert into @MarketSegmentList
select id as SegmentId,
		SubSegmentName as SegmentName
		from SubSegment
		where SegmentId = @SegmentID
		and (id IN (SELECT id FROM @SegmentIdList) OR not exists(select 1 from @SegmentIdList ))
		and IsDeleted = 0

insert into #tmp_MarketValue([Value], Volume, GrowthRatePercentage, TrendGrowthRatePercentage, Amount,
[Year], CountryId, AvgSalePrice, GraphOptions, SegmentId)
SELECT	   [dbo].[fun_CurrencyConversion](MV.[Value], @USDCurrencyId, @CurrencyId,@Today)   AS [Value], 
		   MV.Volume AS Volume, 
		  -- MV.GrowthRatePercentage AS GrowthRatePercentage, 
		  '1' AS GrowthRatePercentage,
		  -- MV.TrendGrowthRatePercentage AS TrendGrowthRatePercentage,
		  '1' AS TrendGrowthRatePercentage,
		   [dbo].[fun_CurrencyConversion](MV.value, @USDCurrencyId, @CurrencyId,@Today) AS AMOUNT,
		   MV.[Year] AS Year,
		  -- MV.RegionId,
		   MV.CountryId,
		   --[dbo].[fun_CurrencyConversion](MV.AvgSalePrice , @USDCurrencyId, @CurrencyId,@Today)AS AvgSalePrice,
		   100.00 AS AvgSalePrice,
		   'chart' AS GraphOptions,
		   MV.SubSegmentId as SegmentId
   FROM MarketValueSummary mv
   WHERE mv.Year between @YearMin and @YearMax 
   and mv.ProjectId = @ProjectID
   and MV.SegmentId = @SegmentID
   and MV.SubSegmentId in (SELECT list.SegmentId FROM @MarketSegmentList list)
   and ((MV.CountryId is null and MV.RegionId in( select ID from @RegionList) and not exists(select 1 from @CountryIdList))
	OR MV.CountryId in (select ID from @CountryIdFilterList))

if (@CaptureJson = 1)
begin
create table #Tmp_json
(JsonString nvarchar(max));
insert into #Tmp_json
select (SELECT --CTE_MV.SegmentId,
--segCTE.SegmentName,
CTE_MV.CountryId,
ISNULL(Country.CountryName, 'Other') as CountryName,
isnull(Country.CountryCode, 'Oth') as CountryCode,
isnull(Country.CountryUNCode, 'Oth') as CountryUNCode,
SUM(CTE_MV.[Value]) AS [Value],
SUM(CTE_MV.Volume) AS Volume, 
SUM(CTE_MV.GrowthRatePercentage) AS GrowthRatePercentage, --for testing purpose
 
SUM(CTE_MV.TrendGrowthRatePercentage) AS TrendGrowthRatePercentage,
SUM(CTE_MV.AMOUNT) AS AMOUNT,
--CTE_MV.[Year],
SUM(CTE_MV.AvgSalePrice) AS AvgSalePrice,
--STRING_AGG(CTE_MV.GraphOptions, ',') WITHIN GROUP (ORDER BY CTE_MV.GraphOptions) as GraphOptions
MAX(CTE_MV.GraphOptions) AS GraphOptions 
FROM #tmp_MarketValue CTE_MV 
left join Country
on CTE_MV.CountryId = Country.Id
--where CTE_MV.RegionId = @RegionId
where SegmentId in (select SegmentId from @MarketSegmentList)
GROUP BY  CTE_MV.CountryId,
Country.CountryName,
Country.CountryCode,
Country.CountryUNCode
ORDER BY Country.CountryCode
FOR JSON PATH);
select JsonString from #Tmp_json ;
drop table #Tmp_json;

end
else
begin
SELECT --CTE_MV.SegmentId,
--segCTE.SegmentName,
CTE_MV.CountryId,
ISNULL(Country.CountryName, 'Other') as CountryName,
isnull(Country.CountryCode, 'Oth') as CountryCode,
isnull(Country.CountryUNCode, 'Oth') as CountryUNCode,
SUM(CTE_MV.[Value]) AS [Value],
SUM(CTE_MV.Volume) AS Volume, 
SUM(CTE_MV.GrowthRatePercentage) AS GrowthRatePercentage, --for testing purpose
 
SUM(CTE_MV.TrendGrowthRatePercentage) AS TrendGrowthRatePercentage,
SUM(CTE_MV.AMOUNT) AS AMOUNT,
--CTE_MV.[Year],
SUM(CTE_MV.AvgSalePrice) AS AvgSalePrice,
--STRING_AGG(CTE_MV.GraphOptions, ',') WITHIN GROUP (ORDER BY CTE_MV.GraphOptions) as GraphOptions
MAX(CTE_MV.GraphOptions) AS GraphOptions 
FROM #tmp_MarketValue CTE_MV 
left join Country
on CTE_MV.CountryId = Country.Id
--where CTE_MV.RegionId = @RegionId
where SegmentId in (select SegmentId from @MarketSegmentList)
GROUP BY  CTE_MV.CountryId,
Country.CountryName,
Country.CountryCode,
Country.CountryUNCode;
--ORDER BY Country.CountryCode
--FOR JSON PATH
end
drop table #tmp_MarketValue;

END