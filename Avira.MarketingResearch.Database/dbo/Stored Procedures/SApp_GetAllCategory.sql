﻿

CREATE PROCEDURE [dbo].[SApp_GetAllCategory]
(
	@includeDeleted bit = 0
)
	AS
/*================================================================================
Procedure Name: [dbo].[SApp_GetAllCategory]
Author: Swami
Create date: 05/18/2019
Description: Get list Category table by Id
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
03/19/2019	Swami			Initial Version
================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT 
		Id as CategoryId
		,CategoryName
		,[Description]
		,CreatedOn
		,UserCreatedById
		,ModifiedOn
		,UserModifiedById
		,IsDeleted
		,DeletedOn
		,UserDeletedById
		
FROM [dbo].[Category]
where  ([Category].IsDeleted = 0)
END