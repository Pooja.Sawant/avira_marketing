﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public class ClientAndStrategyQueries
    {
        public static readonly string StoreProcInsert = "SApp_InsertUpdateClientAndStrategy"; 
        public static string StoreProcSelectStrategy = "SApp_GetStrategyByCompanyID";
        public static string StoreProcSelectclients = "SApp_GetClientByCompanyID";

        public static string StoreProcSelectAllImportance = "SApp_GetImportance";
        public static string StoreProcSelectAllImpactDirection = "SApp_GetImpactDirection";
        public static string StoreProcSelectAllCategories = "SApp_GetClientStrategyCategories";
        public static string StoreProcSelectAllApproach = "SApp_GetClientStrategyApproach";
        public static string StoreProcSelectAllIndustry = "SApp_GetAllIndustries";
    }
}
