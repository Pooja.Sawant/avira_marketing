﻿namespace SQLDatabaseQueryContainer
{
    public static class RegionQueries
    {
        public static readonly string StoreProcInsert = "SApp_InsertRegion";

        public static readonly string StoreProcUpdate = "SApp_UpdateRegion";

        public static string StoreProcSelectAll = "SApp_RegionGridWithFilters";

        public static string StoreProcCountryMapping = "SApp_GetCountrybyRegionID";

        public static string StoreProcSelectAllRegions = "SApp_GetRegionByTrendID";

        public static string StoreProcSelectAllCountries = "SApp_GetCountryByTrendID";
    }
}
