﻿CREATE TABLE [dbo].[CompanyProfitGeographicSplitMap] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [CompanyProfitId]  UNIQUEIDENTIFIER NOT NULL,
    [RegionId]         UNIQUEIDENTIFIER NOT NULL,
    [RegionValue]      DECIMAL (18, 4)  NULL,
    [Year]             INT              NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_CompanyProfitGeographicSplitMap_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    [CompanyId]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CompanyProfitGeographicSplitMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CompanyProfitGeographicSplitMap_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyProfitGeographicSplitMap_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyProfitGeographicSplitMap_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([Id]),
    CONSTRAINT [FK_CompanyProfitGeographicSplitMap_CompanyProfit] FOREIGN KEY ([CompanyProfitId]) REFERENCES [dbo].[CompanyProfit] ([Id]),
    CONSTRAINT [FK_CompanyProfitGeographicSplitMap_Region] FOREIGN KEY ([RegionId]) REFERENCES [dbo].[Region] ([Id])
);

