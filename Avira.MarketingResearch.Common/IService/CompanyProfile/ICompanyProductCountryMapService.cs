﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using static Avira.MarketingResearch.Model.ViewModels.CompanyProfile.CompanyProductCountryMapViewModel;

namespace Avira.MarketingResearch.Common.IService.CompanyProfile
{
    public interface ICompanyProductCountryMapService
    {
        IEnumerable<CompanyProductCountryMapViewModel> GetAll(Guid? Id);
        Result<SpTransactionMessage> InsertUpdate(CompanyProductViewMoel tfc, AviraUser currenUser);
    }
}
