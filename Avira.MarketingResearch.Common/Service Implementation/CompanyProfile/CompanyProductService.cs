﻿using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Utility;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace Avira.MarketingResearch.Common.Service_Implementation.CompanyProfile
{
    public class CompanyProductService : ICompanyProductService
    {
        private readonly ICompanyProductRepository _iCompanyProductRepository;
        private readonly ICompanyProductSegmentMapRepository _iCompanyProductSegmentMapRepository;
        private readonly ICompanyProductCategoryMapRepository _iCompanyProductCategoryMapRepository;
        private readonly ICompanyProductCountryMapRepository _iCompanyProductCountryMapRepository;
        private readonly ICompanyNoteRepository _iCompanyNoteRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;

        public CompanyProductService(IConfiguration configuration, IHostingEnvironment hostingEnvironment, ICompanyProductRepository iCompanyProductRepository,
               ICompanyProductSegmentMapRepository iCompanyProductSegmentMapRepository,
               ICompanyProductCategoryMapRepository iCompanyProductCategoryMapRepository,
               ICompanyProductCountryMapRepository iCompanyProductCountryMapRepository,
               ICompanyNoteRepository iCompanyNoteRepository
            )
        {
            _iCompanyProductRepository = iCompanyProductRepository;
            _iCompanyProductSegmentMapRepository = iCompanyProductSegmentMapRepository;
            _iCompanyProductCategoryMapRepository = iCompanyProductCategoryMapRepository;
            _iCompanyProductCountryMapRepository = iCompanyProductCountryMapRepository;
            _iCompanyNoteRepository = iCompanyNoteRepository;
            _hostingEnvironment = hostingEnvironment;
            _configuration = configuration;
        }

        public MainCompanyProductViewMoel GetAll(Guid Id)
        {
            MainCompanyProductViewMoel mainCompanyProductViewMoel = new MainCompanyProductViewMoel();
            List<CompanyProductViewMoel> companyProductViewMoel = _iCompanyProductRepository.GetAll(Id).ToList();
            for (int i = 0; i < companyProductViewMoel.Count; i++)
            {
                companyProductViewMoel[i].ProductCategoryId = _iCompanyProductCategoryMapRepository.GetAll(companyProductViewMoel[i].Id).Select(x => x.ProductCategoryId).ToArray();
                companyProductViewMoel[i].ProductSegmentId = _iCompanyProductSegmentMapRepository.GetAll(companyProductViewMoel[i].Id).Select(x => x.ProductSegmentId).ToArray();
                companyProductViewMoel[i].ProductCountryId = _iCompanyProductCountryMapRepository.GetAll(companyProductViewMoel[i].Id).Select(x => x.ProductCountryId).ToArray();
            }

            mainCompanyProductViewMoel.CompanyProductViewMoel = companyProductViewMoel??new List<CompanyProductViewMoel>();
            return mainCompanyProductViewMoel;
        }

        public Result<SpTransactionMessage> InsertUpdate(MainCompanyProductViewMoel obj, AviraUser currenUser)
        {
            Result<SpTransactionMessage> CompanyProductRep = null;
            List<CompanyProductViewMoel> companyProductViewMoel = obj.CompanyProductViewMoel;
            for(int i=0;i<companyProductViewMoel.Count;i++)
            {
                if (companyProductViewMoel[i].Id == Guid.Empty)
                {
                    companyProductViewMoel[i].Id = Guid.NewGuid();
                    companyProductViewMoel[i].CompanyId = obj.CompanyId;
                    companyProductViewMoel[i].ProjectId = obj.ProjectId;
                    companyProductViewMoel[i].UserCreatedById = currenUser.Id;
                    if (companyProductViewMoel[i].fileByte != null)
                    {
                        var path = _configuration.GetSection("AppUploadPath").GetSection("Company").GetSection("ProductImages").Value;
                        var fullPath = Path.Combine(_hostingEnvironment.ContentRootPath, path);
                        var newFileName = FileUpload.ByteArrayToFile(companyProductViewMoel[i].ImageActualName, companyProductViewMoel[i].fileByte, fullPath, companyProductViewMoel[i].fileExtension);
                        if(!string.IsNullOrEmpty(newFileName))
                        {
                            companyProductViewMoel[i].ImageActualName = newFileName;
                            companyProductViewMoel[i].ImageURL = path + newFileName;
                        }
                    }
                
                    CompanyProductRep = _iCompanyProductRepository.Insert(companyProductViewMoel[i]);
                }
                else
                {
                    companyProductViewMoel[i].UserModifiedById = currenUser.Id;
                    companyProductViewMoel[i].UserCreatedById = currenUser.Id;
                    if (companyProductViewMoel[i].fileByte != null)
                    {
                        var path = _configuration.GetSection("AppUploadPath").GetSection("Company").GetSection("ProductImages").Value;
                        var fullPath = Path.Combine(_hostingEnvironment.ContentRootPath, path);
                        var newFileName = FileUpload.ByteArrayToFile(companyProductViewMoel[i].ImageActualName, companyProductViewMoel[i].fileByte, fullPath, companyProductViewMoel[i].fileExtension);
                        if (!string.IsNullOrEmpty(newFileName))
                        {
                            companyProductViewMoel[i].ImageActualName = newFileName;
                            companyProductViewMoel[i].ImageURL = path + newFileName;
                        }
                    }
                    CompanyProductRep = _iCompanyProductRepository.Update(companyProductViewMoel[i]);
                }
                if (CompanyProductRep != null && CompanyProductRep.IsSuccess)
                {
                    _iCompanyProductSegmentMapRepository.InsertUpdate(companyProductViewMoel[i]);
                    _iCompanyProductCategoryMapRepository.InsertUpdate(companyProductViewMoel[i]);
                    _iCompanyProductCountryMapRepository.InsertUpdate(companyProductViewMoel[i]);

                    
                }
            }

            obj.CompanyNoteViewModel.Id = Guid.NewGuid();
            obj.CompanyNoteViewModel.UserCreatedById = currenUser.Id;
            obj.CompanyNoteViewModel.CompanyId = obj.CompanyId;
            obj.CompanyNoteViewModel.ProjectId = obj.ProjectId;
            _iCompanyNoteRepository.Insert(obj.CompanyNoteViewModel);

            return CompanyProductRep;
        }

    }
}
