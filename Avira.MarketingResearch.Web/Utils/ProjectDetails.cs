﻿using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Avira.MarketingResearch.Web.Utils
{
    public static class ProjectDetails
    {
        public static ProjectViewModel GetProjectDetails(IServiceRepository _serviceRepository, Guid Id)
        {

            var userId = AppHttpContext.Current.Session.GetComplexData("UserId");
            HttpResponseMessage responseProject = _serviceRepository.GetResponseByUserId("Project", Id, new Guid(userId),0, 0, -1, string.Empty, string.Empty);
            responseProject.EnsureSuccessStatusCode();
            List<ProjectViewModel> project = responseProject.Content.ReadAsAsync<List<ProjectViewModel>>().Result;
            foreach (var proj in project)
            {
                if (!string.IsNullOrEmpty(proj.MApproverApprover))
                {
                    var jApprover = JsonConvert.DeserializeObject<List<object>>(proj.MApproverApprover);
                    var definition = new[] { new { Id = "", ResourceName = "" } };
                    var customer = JsonConvert.DeserializeAnonymousType(proj.MApproverApprover, definition);
                    var name = customer.Select(x => x.ResourceName).ToList();
                    proj.Approver = (string)name[0];
                }
                if (!string.IsNullOrEmpty(proj.TrendsFormsAuthor))
                {
                    var jApprover = JsonConvert.DeserializeObject<List<object>>(proj.TrendsFormsAuthor);
                    var definition = new[] { new { Id = "", ResourceName = "" } };
                    var customer = JsonConvert.DeserializeAnonymousType(proj.TrendsFormsAuthor, definition);
                    var name = customer.Select(x => x.ResourceName).ToList();
                    proj.ProjectAuthorName = (string)name[0];
                }
                if (!string.IsNullOrEmpty(proj.TrendsFormsCoAuthor))
                {
                    var jApprover = JsonConvert.DeserializeObject<List<object>>(proj.TrendsFormsCoAuthor);
                    var definition = new[] { new { Id = "", ResourceName = "" } };
                    var customer = JsonConvert.DeserializeAnonymousType(proj.TrendsFormsCoAuthor, definition);
                    var name = customer.Select(x => x.ResourceName).ToList();
                    proj.ProjectCoAuthorName = (string)name[0];
                }
            }

            return project[0];

        }
    }
}
