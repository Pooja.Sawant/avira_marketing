﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations
{
    public class CompanyRepository : ICompanyRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "Company";
        public CompanyRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }


        public CompanyViewModel GetById(Guid guid)
        {
            CompanyViewModel companyViewModel = new CompanyViewModel();
            companyViewModel = GetAllCompany(guid).FirstOrDefault();
            return companyViewModel;
        }

        public IEnumerable<CompanyViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted)
        {
            return GetAllCompany(null, PageStart, pageSize, SortDirection, OrdbyByColumnName,Search,includeDeleted);
        }

        private IEnumerable<CompanyViewModel> GetAllCompany(Guid? guid, int PageStart = 0, int pageSize = 0, int SortDirection = -1, string OrdbyByColumnName = "", string Search = "", bool includeDeleted = false)
        {   
            DataTable companyIdsTable = new DataTable();
            companyIdsTable.Columns.Add("ID", typeof(Guid));

            if (guid != null)
            {
                companyIdsTable.Rows.Add(guid);

            }

            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<CompanyViewModel>(CompanyQueries.StoreProcSelectAll
                            , new { @CompanyIDList = companyIdsTable.AsTableValuedParameter("dbo.udtUniqueIdentifier"), @includeDeleted = includeDeleted, @PageStart = PageStart, @pageSize = pageSize, @SortDirection = SortDirection, @OrdbyByColumnName = OrdbyByColumnName, @CompanyName=Search }
                            , null, true, null, CommandType.StoredProcedure);
        }

        public Result<SpTransactionMessage> Create(CompanyInsertDbViewModel companyInsertDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(CompanyQueries.StoreProcInsert, companyInsertDbViewModel, CommandType.StoredProcedure);
            return spTransactionMessage;

        }

        public Result<SpTransactionMessage> Update(CompanyUpdateDbViewModel companyUpdateDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(CompanyQueries.StoreProcUpdate, companyUpdateDbViewModel, CommandType.StoredProcedure);
            return spTransactionMessage;
        }


    }
}
