﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyCashFlowStatementController : AviraBaseController
    {
        private readonly ICompanyCashFlowStatementService _iCompanyCashFlowStatementService;
        private ILoggerManager _logger;

        public CompanyCashFlowStatementController(
           ICompanyCashFlowStatementService iCompanyCashFlowStatementService,
            ILoggerManager logger)
        {
            _iCompanyCashFlowStatementService = iCompanyCashFlowStatementService;
            _logger = logger;

        }


        [HttpGet("{id}")]
        public ActionResult<IEnumerable<CompanyCashFlowStatementViewModel>> GetCompanyCashFlowStatement(Guid Id)
        {
            List<CompanyCashFlowStatementViewModel> res = null;
            AviraUser user = new AviraUser();
            try
            {
                res = _iCompanyCashFlowStatementService.GetCompanyCashFlowStatementData(Id, user).ToList();

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);

            }
            return res;
        }
    }
}
