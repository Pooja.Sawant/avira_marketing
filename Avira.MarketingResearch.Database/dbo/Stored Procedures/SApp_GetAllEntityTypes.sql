﻿Create procedure [dbo].[SApp_GetAllEntityTypes]

as
/*================================================================================
Procedure Name: [SApp_GetAllEntityTypes]
Author: Sai Krishna
Create date: 05/02/2019
Description: Get list from Region
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
05/06/2019	Sai Krishna		Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT [Id], 
		[EntityTypeName]
FROM [dbo].[EntityType]
where ([EntityType].IsDeleted = 0)  
end