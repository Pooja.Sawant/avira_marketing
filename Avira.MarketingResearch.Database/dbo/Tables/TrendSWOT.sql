﻿CREATE TABLE [dbo].[TrendSWOT] (
    [Id]                     UNIQUEIDENTIFIER NOT NULL,
    [TrendId]                UNIQUEIDENTIFIER NOT NULL,
    [StrengthtTypeId]        UNIQUEIDENTIFIER NOT NULL,
    [StrengthDescription]    NVARCHAR (1000)  NOT NULL,
    [WeaknessTypeId]         UNIQUEIDENTIFIER NOT NULL,
    [WeaknessDescription]    NVARCHAR (1000)  NOT NULL,
    [OpportunityTypeId]      UNIQUEIDENTIFIER NOT NULL,
    [OpportunityDescription] NVARCHAR (1000)  NOT NULL,
    [ThreatTypeId]           UNIQUEIDENTIFIER NOT NULL,
    [ThreatDescription]      NVARCHAR (1000)  NOT NULL,
    [CreatedOn]              DATETIME2 (7)    NULL,
    [UserCreatedById]        UNIQUEIDENTIFIER NULL,
    [ModifiedOn]             DATETIME2 (7)    NULL,
    [UserModifiedById]       UNIQUEIDENTIFIER NULL,
    [IsDeleted]              BIT              NOT NULL,
    [DeletedOn]              DATETIME2 (7)    NULL,
    [UserDeletedById]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_TrendSWOT] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TrendSWOT_SWOTType1] FOREIGN KEY ([StrengthtTypeId]) REFERENCES [dbo].[SWOTType] ([Id]),
    CONSTRAINT [FK_TrendSWOT_SWOTType2] FOREIGN KEY ([WeaknessTypeId]) REFERENCES [dbo].[SWOTType] ([Id]),
    CONSTRAINT [FK_TrendSWOT_SWOTType3] FOREIGN KEY ([OpportunityTypeId]) REFERENCES [dbo].[SWOTType] ([Id]),
    CONSTRAINT [FK_TrendSWOT_SWOTType4] FOREIGN KEY ([ThreatTypeId]) REFERENCES [dbo].[SWOTType] ([Id]),
    CONSTRAINT [FK_TrendSWOT_Trend] FOREIGN KEY ([TrendId]) REFERENCES [dbo].[Trend] ([Id])
);