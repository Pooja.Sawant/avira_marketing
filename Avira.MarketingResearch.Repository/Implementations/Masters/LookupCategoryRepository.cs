﻿using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces.Masters;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.Masters
{
    public class LookupCategoryRepository : ILookupCategoryRepository
    {
        protected IDbHelper _dbHelper;
        public LookupCategoryRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }
        public IEnumerable<CategoryViewModel> GetAll(string categoryType)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<CategoryViewModel>(LookupCategoryQueries.StoreProcGet
                            , new { @CategoryType = categoryType }
                            , null, true, null, CommandType.StoredProcedure);
        }
    }
}
