﻿CREATE TABLE [dbo].[Region] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [RegionName]       NVARCHAR (256)   CONSTRAINT [DF_Region_RegionName] DEFAULT ('') NOT NULL,
    [RegionLevel]      TINYINT          NOT NULL,
    [ParentRegionId]   UNIQUEIDENTIFIER NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_Region_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_Region] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Region_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_Region_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_Region_ParentRegionId] FOREIGN KEY ([ParentRegionId]) REFERENCES [dbo].[Region] ([Id])
);

