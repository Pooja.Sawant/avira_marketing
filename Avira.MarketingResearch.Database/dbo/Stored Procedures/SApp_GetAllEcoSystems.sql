﻿CREATE procedure [dbo].[SApp_GetAllEcoSystems]

as
/*================================================================================
Procedure Name: [SApp_GetAllEcoSystems]
Author: Sai Krishna
Create date: 08/05/2019
Description: Get list from EcoSystem(Segment)
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
08/05/2019	Sai Krishna		Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT [Id], 
		SegmentName as [EcoSystemName]
	FROM [dbo].Segment
where (Segment.[IsDeleted] = 0)  
end