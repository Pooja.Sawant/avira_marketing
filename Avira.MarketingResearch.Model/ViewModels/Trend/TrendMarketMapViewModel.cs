﻿using Avira.MarketingResearch.Model.ViewModels.Masters;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.Trend
{
    public class TrendMarketMapViewModel
    {       
        public Guid TrendId { get; set; }
        public Guid MarketId { get; set; }
        public Guid ImpactId { get; set; }
        public string MarketName { get; set; }
        public bool IsMapped { get; set; }
        public bool EndUser { get; set; }
        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "MarketMap";
            }
        }
    }

    public class MainTrendMarketMapInsertUpdateDbViewModel
    {
        public Guid TrendId { get; set; }
        public string MarketName { get; set; }
        public List<TrendMarketMapViewModel> TrendMarketMapInsertUpdate { get; set; }
        public Guid AviraUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }        
        public TrendViewModel TrendModel { get; set; }
        public TrendNoteViewModel TrendNoteModel { get; set; }
    }
    
}
