﻿CREATE TYPE [dbo].[udtGUIDCompanyProfitMultiValue] AS TABLE (
    [Id]          UNIQUEIDENTIFIER NOT NULL,
    [CompanyId]   UNIQUEIDENTIFIER NOT NULL,
    [CurrencyId]  UNIQUEIDENTIFIER NOT NULL,
    [UnitId]      UNIQUEIDENTIFIER NOT NULL,
    [TotalProfit] DECIMAL (18, 4)  NULL,
    [Year]        INT              NULL);

