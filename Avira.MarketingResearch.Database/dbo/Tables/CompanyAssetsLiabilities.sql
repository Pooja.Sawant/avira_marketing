﻿CREATE TABLE [dbo].[CompanyAssetsLiabilities] (
    [ID]                 UNIQUEIDENTIFIER NOT NULL,
    [CompanyFinancialID] UNIQUEIDENTIFIER NULL,
    [AssetsType]         NVARCHAR (100)   NULL,
    [Year]               INT              NULL,
    [ActualAmount]       DECIMAL (18)     NULL,
    [ConversionAmount]   DECIMAL (18)     NULL,
    [CreatedOn]          DATETIME         NULL,
    [UserCreatedById]    UNIQUEIDENTIFIER NULL,
    [ModifiedOn]         DATETIME         NULL,
    [UserModifiedById]   UNIQUEIDENTIFIER NULL,
    [IsDeleted]          BIT              NULL,
    [DeletedOn]          DATETIME         NULL,
    [UserDeletedById]    UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CompanyAssetsLiabilities] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_CompanyAssetsLiabilities_CompanyFinancialAnalysis] FOREIGN KEY ([CompanyFinancialID]) REFERENCES [dbo].[CompanyFinancialAnalysis] ([ID])
);

