﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyOrganizationController : Controller
    {
        private readonly ICompanyOrganizationService _companyCompanyOrganizationService;
        private readonly ICompanyOrganizationRepository _companyCompanyOrganizationRepository;
        private ILoggerManager _logger;

        public CompanyOrganizationController(
            ICompanyOrganizationService companyCompanyOrganizationService,
            ICompanyOrganizationRepository companyCompanyOrganizationRepository,
             ILoggerManager logger)
        {
            _companyCompanyOrganizationRepository = companyCompanyOrganizationRepository;
            _companyCompanyOrganizationService = companyCompanyOrganizationService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<CompanyOrganizationViewModel>> Get(Guid? Id)
        {
            List<CompanyOrganizationViewModel> res = null;
            try
            {
                res = _companyCompanyOrganizationService.GetOrganizationList(Id).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }
    }
}