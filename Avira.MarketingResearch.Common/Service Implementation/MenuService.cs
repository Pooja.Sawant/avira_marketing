﻿using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;

namespace Avira.MarketingResearch.Common.Service_Implementation
{
    public class MenuService : IMenuService
    {
        private readonly IMenuRepository _iMenuRepository;

        public MenuService(IMenuRepository iMenuRepository)
        {
            _iMenuRepository = iMenuRepository;
        }

        public Result<SpTransactionMessage> Create(MenuInsertDbViewModel tfc, AviraUser currentUser)
        {
            return _iMenuRepository.Create(tfc);
        }

        public IEnumerable<MenuViewModel> GetAll(Guid Id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<MenuViewModel> GetAll()
        {
            throw new NotImplementedException();
        }

        public Result<SpTransactionMessage> Update(MenuUpdateDbViewModel tfc, AviraUser currenUser)
        {
            return _iMenuRepository.Update(tfc);
        }
    }
}
