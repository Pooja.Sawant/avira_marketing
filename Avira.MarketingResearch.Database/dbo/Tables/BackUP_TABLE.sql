﻿CREATE TABLE [dbo].[BackUP_TABLE] (
    [id]                INT            IDENTITY (1, 1) NOT NULL,
    [timestamp]         DATETIME       NOT NULL,
    [level]             VARCHAR (100)  NOT NULL,
    [logger]            VARCHAR (1000) NOT NULL,
    [message]           VARCHAR (3600) NOT NULL,
    [userid]            NVARCHAR (100) NULL,
    [exception]         VARCHAR (3600) NULL,
    [stacktrace]        VARCHAR (3600) NULL,
    [AppEnviromentType] VARCHAR (250)  NULL,
    [SiteId]            SMALLINT       NULL
);

