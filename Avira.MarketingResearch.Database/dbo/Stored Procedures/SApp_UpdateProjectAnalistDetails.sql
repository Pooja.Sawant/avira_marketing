﻿

CREATE procedure [dbo].[SApp_UpdateProjectAnalistDetails] 
     
(@ProjectID uniqueidentifier, 
@AnalysisType nvarchar(100),
@AnalystList [dbo].[udtGUIDVarcharValue] readonly,
@UserModifiedById  uniqueidentifier
)  
as      
/*================================================================================      
Procedure Name: SApp_UpdateProjectAnalistDetails     
Author: Swami      
Create date: 04/10/2019      
Description: update a record in ProjectAnalistDetails table by ID. Also used for delete      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
04/10/2019 Swami   Initial Version     
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
declare @ErrorMsg NVARCHAR(2048);
--IF exists(select 1 from [dbo].ProjectAnalistDetails       
--   where [Id] !=  @Id  
--   AND ProjectID = @ProjectID 
--   AND AnalysisType = @AnalysisType 
--   AND  IsDeleted = 0)      
--begin      
--declare @ErrorMsg NVARCHAR(2048);      
--set @ErrorMsg = 'ProjectId with Name "'+ CONVERT(nvarchar(50), @ProjectID) + '" already exists.';      
  
--SELECT StoredProcedureName ='SApp_UpdateProjectAnalistDetails','Message' =@ErrorMsg,'Success' = 0;    
--RETURN;  
--end      
 
BEGIN TRY 
declare @AnalystType Varchar(50);
select top 1 @AnalystType = Value
from @AnalystList;
 
--Delete records which are not mapped     
	update [dbo].ProjectAnalistDetails      
	set DeletedOn = case when IsDeleted = 0 then getdate() else DeletedOn end,      
		UserDeletedById = case when IsDeleted = 0 then @UserModifiedById else UserDeletedById end,      
		IsDeleted = 1
	from [dbo].ProjectAnalistDetails
	where ProjectID = @ProjectID      
	AND AnalysisType = @AnalysisType
	AND AnalystType = @AnalystType
	AND NOT EXISTS (SELECT 1 FROM @AnalystList list 
					where list.[ID] = ProjectAnalistDetails.AnalystUserID
					--AND list.[Value] = ProjectAnalistDetails.AnalystType
					)
--Update users as active which are previously deleted.
	update [dbo].ProjectAnalistDetails      
	set DeletedOn = null,      
		UserDeletedById = null,      
		IsDeleted = 0,
		ModifiedOn = GETDATE(),
		UserModifiedById = @UserModifiedById
	from [dbo].ProjectAnalistDetails
	where ProjectID = @ProjectID      
	AND AnalysisType = @AnalysisType
	AND AnalystType = @AnalystType
	AND EXISTS (SELECT 1 FROM @AnalystList list 
					where list.[ID] = ProjectAnalistDetails.AnalystUserID
					--AND list.[Value] = ProjectAnalistDetails.AnalystType
					)
--insert new records
	insert into [dbo].ProjectAnalistDetails(
		ID,
		ProjectID,
		AnalysisType,
		AnalystType,
		AnalystUserID,
		CreatedOn,
		UserCreatedById,
	     IsDeleted)
	select NEWID(),
	@ProjectID,
	@AnalysisType,
	[Value],
	[ID],
	GETDATE(),
	@UserModifiedById,
	0
	from @AnalystList list
	where not exists (select 1 from [dbo].ProjectAnalistDetails
					  where ProjectAnalistDetails.AnalystType = @AnalystType
					AND list.[ID] = ProjectAnalistDetails.AnalystUserID
					and ProjectAnalistDetails.ProjectID = @ProjectID
					and ProjectAnalistDetails.AnalysisType=@AnalysisType)
					
     
SELECT StoredProcedureName ='SApp_UpdateProjectAnalistDetails',Message =@ErrorMsg,Success = 1;       
    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);      
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());   
  
set @ErrorMsg = 'Error while updating ProjectAnalistDetails, please contact Admin for more details.'        
   
 SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
END CATCH      
      
    
    
      
end