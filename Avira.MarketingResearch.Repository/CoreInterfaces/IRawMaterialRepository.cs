﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.Helpers;
using System.Collections.Generic;
using System;
using System.Data;

namespace Avira.MarketingResearch.Repository.CoreInterfaces
{
    public interface IRawMaterialRepository
    {
        RawMaterialViewModel GetById(Guid guid);
        IEnumerable<RawMaterialViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted);
        Result<SpTransactionMessage> Create(RawMaterialInsertDbViewModel viewModel);
        Result<SpTransactionMessage> Update(RawMaterialUpdateDbViewModel viewModel);
    }
}
