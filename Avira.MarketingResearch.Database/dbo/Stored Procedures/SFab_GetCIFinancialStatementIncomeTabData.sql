﻿
--drop Procedure SFab_GetCIFinancialStatementIncomeTabData

--execute [SFab_GetCIFinancialStatementIncomeTabData]  @CompanyID = '7cfa9e8e-d78d-41d9-be62-4e49fdc46fa6'

Create   Procedure [dbo].[SFab_GetCIFinancialStatementIncomeTabData] 
(
	@CompanyID uniqueidentifier = null
)
As
/*================================================================================  
Procedure Name: [SFab_GetCIFinancialStatementIncomeTabData]  
Author: Nitin 
Create date: 08-Nov-2019  
Description: Get CI Company Financial Statement Tab Data
Change History  
Date         Developer     Reason  
____________ __________  ______________________________________________________  
08-Nov-2019  Nitin       Initial Version  
================================================================================*/  
BEGIN

	--Declare @CompanyID uniqueidentifier = '97ad6d5a-56a8-4af7-961a-e220725c52f1'

	Declare @UnitId uniqueidentifier

	Declare @Years Table (CompanyID uniqueidentifier, [Year] int)
	Declare @MaxYear int, @MinYear int

	Select @MaxYear = Max([Year]), @MinYear = Min([Year])
	From CompanyPLStatement
	Where CompanyId = @CompanyID;	

	If (@MaxYear Is Not Null And @MinYear Is Not Null)
	Begin
		WITH CTE
			 AS (SELECT @CompanyID As CompanyID, @MinYear As Year1
				 UNION ALL
				 SELECT @CompanyID As CompanyID, Year1 + 1 
				 FROM   CTE
				 WHERE  Year1 < @MaxYear)
		Insert Into @Years
		SELECT *
		FROM   CTE 
	End
	
	--Select * From @Years Order by 1 desc;

	Declare @CICompanyFinancialStatementTabData Table(
		CompanyId uniqueidentifier,
		[Year] int,
		StatementType nvarchar(20),
		Revenue decimal (19, 4),
		CostsAndExpenses decimal (19, 4),	
		CostOfProduction decimal (19, 4),
		ResourceCost decimal (19, 4),
		GrossProfit decimal (19, 4),
		SalaryCost decimal (19, 4),
		CostOfSales decimal (19, 4),
		SellingExpenses decimal (19, 4),
		MarketingExpenses decimal (19, 4),
		AdministrativeExpenses decimal (19, 4),
		GeneralExpenses decimal (19, 4),
		SellingGeneralAndAdminExpenses decimal (19, 4),
		ResearchAndDevelopmentExpenses decimal (19, 4),
		Depreciation decimal (19, 4),
		Amortization decimal (19, 4),
		Restructuring decimal (19, 4),
		OtherIncomeOrDeductions decimal (19, 4),
		TotalOperatingExpenses decimal (19, 4),
		EBIT decimal (19, 4),
		EBITDA decimal (19, 4),
		InterestIncome decimal (19, 4),
		InterestExpense decimal (19, 4),
		EBT decimal (19, 4),
		Tax decimal (19, 4),
		NetIncomeFromContinuingOperations decimal (19, 4),
		DiscontinuedOperations decimal (19, 4),
		IncomeLossFromDiscontinuedOperations decimal (19, 4),
		GainOnDisposalOfDiscontinuedOperations decimal (19, 4),
		DiscontinuedOperationsNetOfTax decimal (19, 4),
		NetIncomeBeforeAllocationToNoncontrollingInterests decimal (19, 4),
		NonControllingInterests decimal (19, 4),
		NetIncome decimal (19, 4),
		BasicEPS_ decimal (19, 4),
		BasicEPSFromContinuingOperations decimal (19, 4),
		BasicEPSFromDiscontinuedOperations decimal (19, 4),
		BasicEPS decimal (19, 4),
		DilutedEPS_ decimal (19, 4),
		DilutedEPSFromContinuingOperations decimal (19, 4),
		DilutedEPSFromDiscontinuedOperations decimal (19, 4),
		DilutedEPS decimal (19, 4),
		WeightedAverageSharesOutstandingBasic decimal (19, 4),
		WeightedAverageSharesOutstandingDiluted decimal (19, 4)		
	)

	Select @UnitId = Id 
	From ValueConversion
	Where ValueName = 'Millions';

	Insert Into @CICompanyFinancialStatementTabData (CompanyId, [Year], StatementType,
		Revenue,		
		CostsAndExpenses,
		CostOfProduction,
		ResourceCost,
		GrossProfit,
		SalaryCost,
		CostOfSales,
		SellingExpenses,
		MarketingExpenses,
		AdministrativeExpenses,
		GeneralExpenses,
		SellingGeneralAndAdminExpenses,
		ResearchAndDevelopmentExpenses,
		Depreciation,
		Amortization,
		Restructuring,
		OtherIncomeOrDeductions,
		TotalOperatingExpenses,
		EBIT,
		EBITDA,
		InterestIncome,
		InterestExpense,
		EBT,
		Tax,
		NetIncomeFromContinuingOperations,
		DiscontinuedOperations,
		IncomeLossFromDiscontinuedOperations,
		GainOnDisposalOfDiscontinuedOperations,
		DiscontinuedOperationsNetOfTax,
		NetIncomeBeforeAllocationToNoncontrollingInterests,
		NonControllingInterests,
		NetIncome,
		BasicEPS_,
		BasicEPSFromContinuingOperations,
		BasicEPSFromDiscontinuedOperations,
		BasicEPS,
		DilutedEPS_,
		DilutedEPSFromContinuingOperations,
		DilutedEPSFromDiscontinuedOperations,
		DilutedEPS,
		WeightedAverageSharesOutstandingBasic,
		WeightedAverageSharesOutstandingDiluted
	)
	Select Y.CompanyId, Y.[Year], 'INCOME',
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.Revenue),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.CostsAndExpenses),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.CostOfProduction),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.ResourceCost),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.GrossProfit),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.SalaryCost),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.CostOfSales),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.SellingExpenses),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.MarketingExpenses),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.AdministrativeExpenses),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.GeneralExpenses),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.SellingGeneralAndAdminExpenses),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.ResearchAndDevelopmentExpenses),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.Depreciation),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.Amortization),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.Restructuring),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.OtherIncomeOrDeductions),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.TotalOperatingExpenses),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.EBIT),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.EBITDA),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.InterestIncome),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.InterestExpense),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.EBT),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.Tax),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.NetIncomeFromContinuingOperations),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.DiscontinuedOperations),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.IncomeLossFromDiscontinuedOperations),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.GainOnDisposalOfDiscontinuedOperations),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.DiscontinuedOperationsNetOfTax),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.NetIncomeBeforeAllocationToNoncontrollingInterests),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.NonControllingInterests),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.NetIncome),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.BasicEPS_),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.BasicEPSFromContinuingOperations),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.BasicEPSFromDiscontinuedOperations),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.BasicEPS),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.DilutedEPS_),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.DilutedEPSFromContinuingOperations),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.DilutedEPSFromDiscontinuedOperations),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.DilutedEPS),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.WeightedAverageSharesOutstandingBasic),
			[dbo].[fun_UnitDeConversion](@UnitId, CPLS.WeightedAverageSharesOutstandingDiluted)
	From @Years Y Left Join CompanyPLStatement CPLS On Y.[Year] = CPLS.[Year] And CPLS.CompanyId = @CompanyID
	Order By Y.[Year] 

	Select top 7 *
	From @CICompanyFinancialStatementTabData
	Order By [Year] Desc 
END