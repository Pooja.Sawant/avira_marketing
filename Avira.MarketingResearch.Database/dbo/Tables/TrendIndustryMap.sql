CREATE TABLE [dbo].[TrendIndustryMap] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [TrendId]          UNIQUEIDENTIFIER NOT NULL,
    [IndustryId]       UNIQUEIDENTIFIER NOT NULL,
    [Impact]           UNIQUEIDENTIFIER NULL,
    [EndUser]          BIT              NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_TrendIndustryMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TrendIndustryMap_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_TrendIndustryMap_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id])
);




GO

GO

GO

GO

GO

GO

GO
CREATE NONCLUSTERED INDEX [IX_TrendIndustryMap]
    ON [dbo].[TrendIndustryMap]([TrendId] ASC);

