﻿
CREATE PROCEDURE [dbo].[Proc_BackEnd_GetTrendDropdownData]
AS
/*================================================================================
Procedure Name: Proc_BackEnd_GetTrendDropdownData
Author: Nitin
Create date: 06/Dec/2019
Description: To update Trend download template with latest dropdown values
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
06/Dec/2019	Nitin			Initial Version
================================================================================*/
BEGIN

	Select TrendStatusName From TrendStatus Order By TrendStatusName

	Select ImportanceName From Importance Order By ImportanceName 

	Select ImpactDirectionName From ImpactDirection Order By ImpactDirectionName

	Select CompanyGroupName From CompanyGroup Order By CompanyGroupName

	Select ImpactTypeName From ImpactType Order By ImpactTypeName

	Select CompanyName From Company Order By CompanyName

	Select CountryName From Country Order By CountryName

	Select Distinct S.SegmentName As 'EcoSystem Segments' From ESMSegmentMapping CEPEM Inner Join Segment S On CEPEM.SegmentId = S.Id Order By S.SegmentName

	Select SS.SubSegmentName As 'Industry' From SubSegment SS Inner Join Segment S On SS.SegmentId = S.Id Where Upper(S.SegmentName) = 'END USER' Order By SS.SubSegmentName

	Select S.SegmentName As 'Segment Name' From Segment S Order By S.SegmentName

	Select SS.SubSegmentName As 'SubSegment Name' From SubSegment SS Order By SS.SubSegmentName

	Select MarketName From Market Order By MarketName

	Select RegionName From Region Order By RegionName

	Select ProjectName From Project Order By ProjectName

	Select TimeTagName From TimeTag Order By TimeTagName

	Select ValueName From ValueConversion Order By ValueName


END