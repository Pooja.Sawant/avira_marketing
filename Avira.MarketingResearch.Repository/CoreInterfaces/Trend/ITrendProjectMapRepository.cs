﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.Trend
{
    public interface ITrendProjectMapRepository
    {
        IEnumerable<TrendProjectMapViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search = "", bool isDeletedInclude = false, string id = "");

        Result<SpTransactionMessage> InsertUpdate(MainTrendProjectMapInsertUpdateDbViewModel trendCompanyGroupMapUpdateDbViewModel);
    }
}
