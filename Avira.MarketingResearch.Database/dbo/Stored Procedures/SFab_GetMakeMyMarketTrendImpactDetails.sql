﻿
  
CREATE PROCEDURE [dbo].[SFab_GetMakeMyMarketTrendImpactDetails]    
 AS  
/*================================================================================  
Procedure Name: [SFab_GetMakeMyMarketTrendImpactDetails]  
Author: Sai Krishna  
Create date: 06/07/2019  
Description: Get list of Trends and Trends releated data.
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
06/07/2019 Sai Krishna   Initial Version  
================================================================================*/  
  
BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
	
	;With cteCompanies as (
Select
TrendKeyCompanyMap.TrendId,
STRING_AGG(Company.CompanyName, ',') as CompanyName
from TrendKeyCompanyMap
Inner Join Company
On Company.Id = TrendKeyCompanyMap.CompanyId
Where Company.Isdeleted = 0
Group By TrendKeyCompanyMap.TrendId
),
cteTimeTags as (
Select
 TrendTimeTag.TrendId,
STRING_AGG(TimeTag.TimeTagName, ',') as TimeTagName
from TrendTimeTag
Inner Join TimeTag
On TimeTag.Id = TrendTimeTag.TimeTagId
Where TrendTimeTag.Isdeleted = 0
and TimeTag.TimeTagName in('ShortTerm','LongTerm','MidTerm')
Group By TrendTimeTag.TrendId
),
cteImpact as(
Select TTT.TrendId,
STRING_AGG(IT.ImpactTypeName, ',') as ImpactTypeName 
from TrendTimeTag TTT
Inner Join ImpactType IT
ON TTT.Impact = IT.Id
where TTT.IsDeleted = 0
Group By TTT.TrendId
),
cteESM as(
Select
TrendEcoSystemMap.TrendId,
STRING_AGG(Segment.SegmentName, ',') as EcoSystemName
from TrendEcoSystemMap
Inner Join Segment
On Segment.Id = TrendEcoSystemMap.EcoSystemId
Where Segment.Isdeleted = 0
Group By TrendEcoSystemMap.TrendId
),
cteIndustries as(
Select
TrendIndustryMap.TrendId,
STRING_AGG(Industry.IndustryName, ',') as IndustryName
from TrendIndustryMap
Inner Join Industry
On Industry.Id = TrendIndustryMap.IndustryId
Where Industry.Isdeleted = 0
Group By TrendIndustryMap.TrendId
)
Select t.Id as TrendId,
       t.TrendName,
	   cteImpact.ImpactTypeName,
	   ID.ImpactDirectionName as Nature,
	   cteCompanies.CompanyName,
	   cteTimeTags.TimeTagName,
	   cteESM.EcoSystemName,
	   cteIndustries.IndustryName
	 from Trend t
Inner Join ImpactDirection ID
On ID.Id = t.ImpactDirectionId
Inner Join cteTimeTags
On cteTimeTags.TrendId = t.Id
Inner Join cteCompanies 
On cteCompanies.TrendId = t.Id
Inner Join cteImpact
ON cteImpact.TrendId = t.Id
Inner Join cteESM
ON cteESM.TrendId = t.Id
Inner Join cteIndustries
On cteIndustries.TrendId = t.Id
Where t.IsDeleted = 0 
And t.TrendName = ISNULL(TrendName,Null)
Order By t.TrendName

END