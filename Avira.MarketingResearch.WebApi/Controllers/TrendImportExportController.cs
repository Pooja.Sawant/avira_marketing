﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.ImportExport;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrendImportExportController : AviraBaseController
    {
        private ILoggerManager _logger;
        public readonly ITrendImportExportRepository _trendImportExportRepository;
        public readonly ITrendImportExportService _trendImportExportService;

        public TrendImportExportController(
            ITrendImportExportService trendImportExportService,
            ITrendImportExportRepository trendImportExportRepository,
            ILoggerManager logger)
        {
            _trendImportExportRepository = trendImportExportRepository;
            _trendImportExportService = trendImportExportService;
            _logger = logger;
        }

        [HttpPost]
        public ActionResult Post(TrendImportInsertViewModel viewModel)
        {
            viewModel.UserCreatedById = CurrentUser.Id;
            ImportCommonResponseMainModel viewData = new ImportCommonResponseMainModel()
            {
                IsSuccess = false,
                Message = "Error while creating a new Trend, please contact Admin for more details."
            };

            try
            {

                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                IEnumerable<ImportCommonResponseModel> result = _trendImportExportService.Create(viewModel, CurrentUser);

                var res = result.Where(s => s.ErrorNotes != null).ToList();

                if (res.Count <= 0)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Trend Details successfully Imported";
                }
                else
                {
                    viewData.Message = "Import Trend have some errors";
                    viewData.IsSuccess = false;
                    //Export to Excel data.
                    viewData.ResponseModel = result;

                }
            }


            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);

        }

    }
}