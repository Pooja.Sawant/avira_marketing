﻿CREATE TYPE [dbo].[udtGUIDGUIDMapValue] AS TABLE (
    [ID]       UNIQUEIDENTIFIER NOT NULL,
    [MapId]    UNIQUEIDENTIFIER NOT NULL,
    [bitValue] BIT              NOT NULL);

