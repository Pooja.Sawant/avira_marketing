﻿
CREATE   Procedure [dbo].[SFab_InsertCompanyFinancialRelativeValuationInput] 
(
	@ProjectID uniqueidentifier,
	@CompanyID uniqueidentifier,	
	@BusinessObjective [nvarchar](512) NULL,
	@UserID uniqueidentifier
)
As
/*================================================================================  
Procedure Name: [SFab_InsertCompanyFinancialRelativeValuationInput]  
Author: Nitin 
Create date: 14-Nov-2019  
Description: Insert Company Financial Relative Valuation Input
Change History  
Date         Developer     Reason  
____________ __________  ______________________________________________________  
14-Nov-2019   Nitin       Initial Version  
================================================================================*/  
BEGIN  
SET NOCOUNT ON;  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
declare @ErrorMsg NVARCHAR(2048);

BEGIN TRY

	INSERT INTO [dbo].[CompanyFinancialRelativeValuationInput]
           ([Id], [CompanyId], [ProjectId], [BusinessObjective], [CreatedOn], [UserCreatedById], [ModifiedOn], [UserModifiedById])
    VALUES (NEWID(), @CompanyID, @ProjectID, @BusinessObjective, GETDATE(), @UserID, Null, Null)
	SELECT StoredProcedureName ='SFab_InsertCompanyFinancialRelativeValuationInput',Message =@ErrorMsg,Success = 1;     
END TRY
BEGIN CATCH
    -- Execute the error retrieval routine.
	DECLARE @ErrorNumber	int;
	DECLARE @ErrorSeverity	int;
	DECLARE @ErrorProcedure	varchar(100);
	DECLARE @ErrorLine	int;
	DECLARE @ErrorMessage	varchar(500);

	 SELECT @ErrorNumber = ERROR_NUMBER(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorProcedure = ERROR_PROCEDURE(),
        @ErrorLine = ERROR_LINE(),
        @ErrorMessage = ERROR_MESSAGE()

	insert into dbo.Errorlog(ErrorNumber,
							ErrorSeverity,
							ErrorProcedure,
							ErrorLine,
							ErrorMessage,
							ErrorDate)
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());

 set @ErrorMsg = 'Error while insert/Update Customer Company Interest, please contact Admin for more details.';    
 
SELECT Number = @ErrorNumber, 
		Severity =@ErrorSeverity,
		StoredProcedureName =@ErrorProcedure,
		LineNumber= @ErrorLine,
		Message =@ErrorMsg,
		Success = 0;     
END CATCH 
End