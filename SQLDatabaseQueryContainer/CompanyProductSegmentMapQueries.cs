﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public class CompanyProductSegmentMapQueries
    {
        public static readonly string StoreProcInsertUpdate = "SApp_InsertUpdateCompanyProductSegmentMap";

        public static string StoreProcSelectAll = "SApp_GetSegmentByCompanyProductID";
    }
}
