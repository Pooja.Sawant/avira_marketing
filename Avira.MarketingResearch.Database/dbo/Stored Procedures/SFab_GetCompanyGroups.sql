﻿




      
 CREATE PROCEDURE [dbo].[SFab_GetCompanyGroups]            
@AviraUserID uniqueidentifier = null
 AS            
/*================================================================================            
Procedure Name: SFab_GetCompanyGroups      
Author: Sai Krishna            
Create date: 16/07/2019            
Description: Get list from Company Group table            
Change History            
Date  Developer  Reason            
__________ ____________ ______________________________________________________            
16/07/2019  Sai Krishna   Initial Version  
================================================================================*/           
            
BEGIN            
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED         
Select Id as CompanyGroupId,
CompanyGroupName
From CompanyGroup 
Where IsDeleted = 0    order by CompanyGroupName asc    
END