﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.Models;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Avira.MarketingResearch.Web.Controllers
{
    [NoCache]
    [Authorization]
    public class IndustryController : BaseController
    {
        public readonly IServiceRepository _serviceRepository;
        private ILoggerManager _logger;
        public IndustryController(IServiceRepository serviceRepository, ILoggerManager logger)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
        }

        // GET: Industry
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LoadData(IndustryViewModel industry)
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            List<IndustryViewModel> data = null;
            int totalRecords = 0;
            try
            {
              
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                string search = Request.Form["search[value]"];

                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDir = Request.Form["order[0][dir]"].FirstOrDefault();
                var SortDirection = -1;
                if (sortColumnDir == "asc")
                {
                    SortDirection = 1;
                }
                else
                {
                    SortDirection = -1;
                }

                int skip = start != null ? Convert.ToInt32(start) : 0;
                int pageSize = length.ToString() != null ? Convert.ToInt32(length) : 0;
                int PageStart = start.ToString() != null ? Convert.ToInt32(start) : 0;
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();

                HttpResponseMessage response = _serviceRepository.GetResponse(contollerName, PageStart, pageSize, SortDirection, sortColumn, search);
                response.EnsureSuccessStatusCode();
                List<IndustryViewModel> industries = response.Content.ReadAsAsync<List<IndustryViewModel>>().Result;
                ViewBag.Title = "All Industry";
                data = industries;
                
                foreach (var item in industries)
                {
                    totalRecords = item.TotalItems;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data });

        }

        // GET: Industry/Create
        public ActionResult Create()
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.GetAllParentResponse(contollerName);
                List<IndustryViewModel> industryList = response.Content.ReadAsAsync<List<IndustryViewModel>>().Result;
                ViewBag.IndustryList = new SelectList(industryList, "Id", "IndustryName");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View();
        }

        [HttpPost]
        public IActionResult Create(IndustryInsertDbViewModel industry)
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.PostResponse(contollerName,industry);
                response.EnsureSuccessStatusCode();
                ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                HttpResponseMessage response1 = _serviceRepository.GetAllParentResponse(contollerName);
                response1.EnsureSuccessStatusCode();
                List<IndustryViewModel> industryList = response1.Content.ReadAsAsync<List<IndustryViewModel>>().Result;
                ViewBag.IndustryList = new SelectList(industryList, "Id", "IndustryName", industry.ParentId);
                if (viewModel != null)
                {
                    ViewBag.Message = viewModel.Message;
                }
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            //return RedirectToAction("Index");
            return View(industry);
        }

        [HttpGet]
        public IActionResult Edit(string id)
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.GetByIdResponse(contollerName, id);
                HttpResponseMessage response1 = _serviceRepository.GetAllParentResponse(contollerName);
                response.EnsureSuccessStatusCode();
                IndustryUpdateDbViewModel industry = response.Content.ReadAsAsync<IndustryUpdateDbViewModel>().Result;
                response1.EnsureSuccessStatusCode();
                List<IndustryViewModel> industryList = response1.Content.ReadAsAsync<List<IndustryViewModel>>().Result;
                ViewBag.IndustryList = new SelectList(industryList,"Id","IndustryName",industry.ParentId);
                return View(industry);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                ErrorViewModel err = new ErrorViewModel { RequestId = "1234" };
                //Log the exception 
                return View("Error", err);
            }
        }

        [HttpPost]
        public IActionResult Edit(IndustryUpdateDbViewModel industry)
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.PutResponse(contollerName, industry);
                response.EnsureSuccessStatusCode();
                ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                HttpResponseMessage response1 = _serviceRepository.GetAllParentResponse(contollerName);
                response1.EnsureSuccessStatusCode();
                List<IndustryViewModel> industryList = response1.Content.ReadAsAsync<List<IndustryViewModel>>().Result;
                ViewBag.IndustryList = new SelectList(industryList, "Id", "IndustryName", industry.ParentId);
                if (viewModel != null)
                {
                    ViewBag.Message = viewModel.Message;
                }
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            //return RedirectToAction("Index");
            return View(industry); 
        }

        [HttpGet]
        public IActionResult Delete(string id)
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.GetByIdResponse(contollerName, id);
                response.EnsureSuccessStatusCode();
                IndustryUpdateDbViewModel industry = response.Content.ReadAsAsync<IndustryUpdateDbViewModel>().Result;
                if (industry.ParentId.HasValue)
                {
                    HttpResponseMessage response1 = _serviceRepository.GetByIdResponse(contollerName, industry.ParentId.ToString()); ;
                    response1.EnsureSuccessStatusCode();
                    IndustryViewModel parentIndustry = response1.Content.ReadAsAsync<IndustryViewModel>().Result;
                    ViewBag.ParentName = parentIndustry.IndustryName;
                }
                else
                {
                    ViewBag.ParentName = string.Empty;
                }
                return View(industry);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                ErrorViewModel err = new ErrorViewModel { RequestId = "1234" };
                //Log the exception 
                return View("Error", err);
            }
        }

        [HttpPost]
        public IActionResult Delete(IndustryUpdateDbViewModel industry)
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.DeleteResponse(contollerName, industry);
                response.EnsureSuccessStatusCode();
                ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                if (viewModel != null)
                {
                    ViewBag.Message = viewModel.Message;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return RedirectToAction("Index");
        }

    }
}