﻿-- =============================================        
-- Author:  Laxmikant        
-- Create date: 05/06/2019        
-- Description: Get Company by CompanyId         
-- =============================================        
CREATE PROCEDURE [dbo].[SApp_GetClientByCompanyID]             
 @ComapanyID uniqueidentifier = null,         
 @includeDeleted bit = 0     
AS
BEGIN         
 SET NOCOUNT ON;          
declare @IndustrySegmentId uniqueidentifier;
select @IndustrySegmentId = Id from Segment
where SegmentName = 'End User';

 SELECT [CompanyClientsMapping].[Id]        
      ,[CompanyId]    
      ,[ClientName]        
      ,[ClientIndustry]      
   ,Industry.SubSegmentName As IndustryName        
      ,[ClientRemark]        
      ,[CompanyClientsMapping].[CreatedOn]        
      ,[CompanyClientsMapping].[UserCreatedById]        
      ,[CompanyClientsMapping].[ModifiedOn]        
      ,[CompanyClientsMapping].[UserModifiedById]        
      ,[CompanyClientsMapping].[IsDeleted]        
      ,[CompanyClientsMapping].[DeletedOn]        
      ,[CompanyClientsMapping].[UserDeletedById]        
  FROM [dbo].[CompanyClientsMapping]    
  LEFT JOIN [dbo].SubSegment Industry ON [CompanyClientsMapping].ClientIndustry = Industry.Id
  and Industry.SegmentId = @IndustrySegmentId
  WHERE ([CompanyClientsMapping].CompanyId = @ComapanyID)              
 AND (@includeDeleted = 0 or [CompanyClientsMapping].IsDeleted = 0)          
END