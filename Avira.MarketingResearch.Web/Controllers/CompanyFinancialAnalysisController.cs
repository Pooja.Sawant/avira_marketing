﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.Utils;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Avira.MarketingResearch.Web.Controllers
{
    public class CompanyFinancialAnalysisController : BaseController
    {
        private readonly IServiceRepository _serviceRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;
        //string contentRootPath = string.Empty;

        private ILoggerManager _logger;
        public CompanyFinancialAnalysisController(IServiceRepository serviceRepository, ILoggerManager logger, IHostingEnvironment hostingEnvironment, IConfiguration configuration)
        {
            _hostingEnvironment = hostingEnvironment;
            _serviceRepository = serviceRepository;
            _logger = logger;
            _configuration = configuration;
            //contentRootPath = _hostingEnvironment.ContentRootPath;
        }

        public IActionResult Index(string CompanyId, string ProjectId)
        {
            MainCompanyFinancialAnalysisInsertUpdateViewModel model = new MainCompanyFinancialAnalysisInsertUpdateViewModel();
            try
            {
                //model.companyFinancialAnalysisInsertUpdateViewModel.CompanyId = new Guid(companyId);
                var Contoller = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage currenciesResponse = _serviceRepository.GetAllParentResponse(Contoller);
                currenciesResponse.EnsureSuccessStatusCode();
                List<CurrencyViewModel> currencyModel = currenciesResponse.Content.ReadAsAsync<List<CurrencyViewModel>>().Result;
                ViewBag.CurrencyViewModel = new SelectList(currencyModel.OrderBy(e => e.CurrencyNameWithCode), "Id", "CurrencyNameWithCode");

                var categoryContoller = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage categoryResponse = _serviceRepository.GetAllCategoryURL(Contoller);
                categoryResponse.EnsureSuccessStatusCode();
                List<CategoryViewModel> categoryModel = categoryResponse.Content.ReadAsAsync<List<CategoryViewModel>>().Result;
                ViewBag.CategoryViewModel = new SelectList(categoryModel.OrderBy(e => e.CategoryName), "CategoryId", "CategoryName");

                HttpResponseMessage valueResponse = _serviceRepository.GetAllParentSegResponse(Contoller);
                valueResponse.EnsureSuccessStatusCode();
                List<Model.ViewModels.CompanyProfile.ValueConversionViewModel> valueConversionViewModel = valueResponse.Content.ReadAsAsync<List<Model.ViewModels.CompanyProfile.ValueConversionViewModel>>().Result;
                ViewBag.ValueConversionList = new SelectList(valueConversionViewModel, "ValueConversionId", "ValueName");


                HttpResponseMessage response = _serviceRepository.GetOtherFinancialByGuIdResponse(Contoller, new Guid(CompanyId));
                response.EnsureSuccessStatusCode();
                model = response.Content.ReadAsAsync<MainCompanyFinancialAnalysisInsertUpdateViewModel>().Result;
                model.ValueConversionId = model.companyFinancialAnalysisInsertUpdateViewModel.ValueConversionId;
                model.companyFinancialAnalysisInsertUpdateViewModel.CompanyId = new Guid(CompanyId);


                var data = model.baseYearModel.BaseYear;
                int leastYear = data - 5;
                List<TimePeriod> Period = new List<TimePeriod>();
                for (int i = leastYear + 1; i <= data; i++)
                {
                    TimePeriod p = new TimePeriod();
                    p.Period = i;
                    Period.Add(p);
                }
                ViewBag.BaseYearsFrom = new SelectList(Period.OrderByDescending(e => e.Period).ToList(), "Period", "Text");

                List<CompanyTransactionsInsertUpdateViewModel> transModelList = new List<CompanyTransactionsInsertUpdateViewModel>();
                if (model.companyTransactionsInsertUpdateViewModel == null || model.companyTransactionsInsertUpdateViewModel.Count == 0)
                {
                    for (int i = 1; i <= 3; i++)
                    {
                        CompanyTransactionsInsertUpdateViewModel transModel = new CompanyTransactionsInsertUpdateViewModel();
                        transModel.Rationale = "";
                        transModel.OtherParty = "";
                        transModel.Date = DateTime.Now;
                        transModel.Value = Convert.ToDecimal(0);
                        transModelList.Add(transModel);
                    }
                    model.companyTransactionsInsertUpdateViewModel = transModelList;
                }
                else if (model.companyTransactionsInsertUpdateViewModel.Count > 1 || model.companyTransactionsInsertUpdateViewModel.Count < 3)
                {
                    if (model.companyTransactionsInsertUpdateViewModel.Count == 1)
                    {
                        for (int i = 1; i <= 2; i++)
                        {
                            CompanyTransactionsInsertUpdateViewModel transModel = new CompanyTransactionsInsertUpdateViewModel();
                            transModel.Rationale = "";
                            transModel.OtherParty = "";
                            transModel.Date = DateTime.Now;
                            transModel.Value = Convert.ToDecimal(0);
                            model.companyTransactionsInsertUpdateViewModel.Add(transModel);
                        }
                        //model.companyTransactionsInsertUpdateViewModel = transModelList;
                    }
                    else if (model.companyTransactionsInsertUpdateViewModel.Count == 2)
                    {
                        CompanyTransactionsInsertUpdateViewModel transModel = new CompanyTransactionsInsertUpdateViewModel();
                        transModel.Rationale = "";
                        transModel.OtherParty = "";
                        transModel.Date = DateTime.Now;
                        transModel.Value = Convert.ToDecimal(0);
                        model.companyTransactionsInsertUpdateViewModel.Add(transModel);
                    }
                }
                model.companyTransactionsInsertUpdateViewModel.OrderBy(e => e.CreatedOn);
                if (model.expensesBreakdownViewModel.Count > 0 || model.expensesBreakdownViewModel != null)
                    if (model.expensesBreakdownViewModel.Count <= 4)
                        model.RowCount = 4;
                    else
                        model.RowCount = model.expensesBreakdownViewModel.Count;
                else
                    model.RowCount = 4;

                HttpResponseMessage CompanyResponse = _serviceRepository.GetByIdResponse("Company", CompanyId);
                CompanyViewModel companyViewModel = CompanyResponse.Content.ReadAsAsync<CompanyViewModel>().Result;
                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, new Guid(ProjectId));
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.CompanyId = CompanyId;
                    model.ProjectId = new Guid(ProjectId);
                    ViewBag.CompanyName = companyViewModel.CompanyName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult Post(MainCompanyFinancialAnalysisInsertUpdateViewModel model, IFormCollection collection, string actionName)
        {
            CompanyFinancialAnalysisInsertUpdateModel mainModel = new CompanyFinancialAnalysisInsertUpdateModel();
            //mainModel.ValueConversionId = Guid.Parse(collection["ValueConversionId"]);
            try
            {
                List<ExpensesBreakdownViewModel> expenseList = new List<ExpensesBreakdownViewModel>();
                ExpensesBreakdownViewModel expensesModel = new ExpensesBreakdownViewModel();
                expensesModel.ExpensesType = "Total Expenses";
                expensesModel.BaseYearMinus_1 = Convert.ToDecimal(collection["TotalExpensesB1"].ToString() != "" ? collection["TotalExpensesB1"].ToString() : null);
                expensesModel.BaseYearMinus_2 = Convert.ToDecimal(collection["TotalExpensesB2"].ToString() != "" ? collection["TotalExpensesB2"].ToString() : null);
                expensesModel.BaseYearMinus_3 = Convert.ToDecimal(collection["TotalExpensesB3"].ToString() != "" ? collection["TotalExpensesB3"].ToString() : null);
                expensesModel.BaseYearMinus_4 = Convert.ToDecimal(collection["TotalExpensesB4"].ToString() != "" ? collection["TotalExpensesB4"].ToString() : null);
                expensesModel.BaseYearMinus_5 = Convert.ToDecimal(collection["TotalExpensesB5"].ToString() != "" ? collection["TotalExpensesB5"].ToString() : null);
                expenseList.Add(expensesModel);

                ExpensesBreakdownViewModel expensesModel1 = new ExpensesBreakdownViewModel();
                expensesModel1.ExpensesType = "Expense-01";
                expensesModel1.BaseYearMinus_1 = Convert.ToDecimal(collection["Expense1B1"].ToString() != "" ? collection["Expense1B1"].ToString() : null);
                expensesModel1.BaseYearMinus_2 = Convert.ToDecimal(collection["Expense1B2"].ToString() != "" ? collection["Expense1B2"].ToString() : null);
                expensesModel1.BaseYearMinus_3 = Convert.ToDecimal(collection["Expense1B3"].ToString() != "" ? collection["Expense1B3"].ToString() : null);
                expensesModel1.BaseYearMinus_4 = Convert.ToDecimal(collection["Expense1B4"].ToString() != "" ? collection["Expense1B4"].ToString() : null);
                expensesModel1.BaseYearMinus_5 = Convert.ToDecimal(collection["Expense1B5"].ToString() != "" ? collection["Expense1B5"].ToString() : null);
                expenseList.Add(expensesModel1);

                ExpensesBreakdownViewModel expensesModel2 = new ExpensesBreakdownViewModel();
                expensesModel2.ExpensesType = "Expense-02";
                expensesModel2.BaseYearMinus_1 = Convert.ToDecimal(collection["Expense2B1"].ToString() != "" && collection["Expense2B1"].ToString() != "" ? collection["Expense2B1"].ToString() : null);
                expensesModel2.BaseYearMinus_2 = Convert.ToDecimal(collection["Expense2B2"].ToString() != "" ? collection["Expense2B2"].ToString() : null);
                expensesModel2.BaseYearMinus_3 = Convert.ToDecimal(collection["Expense2B3"].ToString() != "" ? collection["Expense2B3"].ToString() : null);
                expensesModel2.BaseYearMinus_4 = Convert.ToDecimal(collection["Expense2B4"].ToString() != "" ? collection["Expense2B4"].ToString() : null);
                expensesModel2.BaseYearMinus_5 = Convert.ToDecimal(collection["Expense2B5"].ToString() != "" ? collection["Expense2B5"].ToString() : null);
                expenseList.Add(expensesModel2);

                ExpensesBreakdownViewModel expensesModel3 = new ExpensesBreakdownViewModel();
                expensesModel3.ExpensesType = "Expense-03";
                expensesModel3.BaseYearMinus_1 = Convert.ToDecimal(collection["Expense3B1"].ToString() != "" ? collection["Expense3B1"].ToString() : null);
                expensesModel3.BaseYearMinus_2 = Convert.ToDecimal(collection["Expense3B2"].ToString() != "" ? collection["Expense3B2"].ToString() : null);
                expensesModel3.BaseYearMinus_3 = Convert.ToDecimal(collection["Expense3B3"].ToString() != "" ? collection["Expense3B3"].ToString() : null);
                expensesModel3.BaseYearMinus_4 = Convert.ToDecimal(collection["Expense3B4"].ToString() != "" ? collection["Expense3B4"].ToString() : null);
                expensesModel3.BaseYearMinus_5 = Convert.ToDecimal(collection["Expense3B5"].ToString() != "" ? collection["Expense3B5"].ToString() : null);
                expenseList.Add(expensesModel3);

                ExpensesBreakdownViewModel expensesModel4 = new ExpensesBreakdownViewModel();
                expensesModel4.ExpensesType = "Expense-04";
                expensesModel4.BaseYearMinus_1 = Convert.ToDecimal(collection["Expense4B1"].ToString() != "" ? collection["Expense4B1"].ToString() : null);
                expensesModel4.BaseYearMinus_2 = Convert.ToDecimal(collection["Expense4B2"].ToString() != "" ? collection["Expense4B2"].ToString() : null);
                expensesModel4.BaseYearMinus_3 = Convert.ToDecimal(collection["Expense4B3"].ToString() != "" ? collection["Expense4B3"].ToString() : null);
                expensesModel4.BaseYearMinus_4 = Convert.ToDecimal(collection["Expense4B4"].ToString() != "" ? collection["Expense4B4"].ToString() : null);
                expensesModel4.BaseYearMinus_5 = Convert.ToDecimal(collection["Expense4B5"].ToString() != "" ? collection["Expense4B5"].ToString() : null);
                expenseList.Add(expensesModel4);

                ExpensesBreakdownViewModel expensesModel5 = new ExpensesBreakdownViewModel();
                expensesModel5.ExpensesType = "Expense-05";
                expensesModel5.BaseYearMinus_1 = Convert.ToDecimal(collection["Expense5B1"].ToString() != "" ? collection["Expense5B1"].ToString() : null);
                expensesModel5.BaseYearMinus_2 = Convert.ToDecimal(collection["Expense5B2"].ToString() != "" ? collection["Expense5B2"].ToString() : null);
                expensesModel5.BaseYearMinus_3 = Convert.ToDecimal(collection["Expense5B3"].ToString() != "" ? collection["Expense5B3"].ToString() : null);
                expensesModel5.BaseYearMinus_4 = Convert.ToDecimal(collection["Expense5B4"].ToString() != "" ? collection["Expense5B4"].ToString() : null);
                expensesModel5.BaseYearMinus_5 = Convert.ToDecimal(collection["Expense5B5"].ToString() != "" ? collection["Expense5B5"].ToString() : null);
                expenseList.Add(expensesModel5);

                ExpensesBreakdownViewModel expensesModel6 = new ExpensesBreakdownViewModel();
                expensesModel6.ExpensesType = "Expense-06";
                expensesModel6.BaseYearMinus_1 = Convert.ToDecimal(collection["Expense6B1"].ToString() != "" ? collection["Expense6B1"].ToString() : null);
                expensesModel6.BaseYearMinus_2 = Convert.ToDecimal(collection["Expense6B2"].ToString() != "" ? collection["Expense6B2"].ToString() : null);
                expensesModel6.BaseYearMinus_3 = Convert.ToDecimal(collection["Expense6B3"].ToString() != "" ? collection["Expense6B3"].ToString() : null);
                expensesModel6.BaseYearMinus_4 = Convert.ToDecimal(collection["Expense6B4"].ToString() != "" ? collection["Expense6B4"].ToString() : null);
                expensesModel6.BaseYearMinus_5 = Convert.ToDecimal(collection["Expense6B5"].ToString() != "" ? collection["Expense6B5"].ToString() : null);
                expenseList.Add(expensesModel6);

                ExpensesBreakdownViewModel expensesModel7 = new ExpensesBreakdownViewModel();
                expensesModel7.ExpensesType = "Expense-07";
                expensesModel7.BaseYearMinus_1 = Convert.ToDecimal(collection["Expense7B1"].ToString() != "" ? collection["Expense7B1"].ToString() : null);
                expensesModel7.BaseYearMinus_2 = Convert.ToDecimal(collection["Expense7B2"].ToString() != "" ? collection["Expense7B2"].ToString() : null);
                expensesModel7.BaseYearMinus_3 = Convert.ToDecimal(collection["Expense7B3"].ToString() != "" ? collection["Expense7B3"].ToString() : null);
                expensesModel7.BaseYearMinus_4 = Convert.ToDecimal(collection["Expense7B4"].ToString() != "" ? collection["Expense7B4"].ToString() : null);
                expensesModel7.BaseYearMinus_5 = Convert.ToDecimal(collection["Expense7B5"].ToString() != "" ? collection["Expense7B5"].ToString() : null);
                expenseList.Add(expensesModel7);

                List<AssetsLiabilitiesViewModel> assetsLiabilitiesList = new List<AssetsLiabilitiesViewModel>();
                AssetsLiabilitiesViewModel cashflowModel = new AssetsLiabilitiesViewModel();
                cashflowModel.AssetsType = "Cash";
                cashflowModel.BaseYearMinus_1 = Convert.ToDecimal(collection["CashB1"].ToString() != "" ? collection["CashB1"].ToString() : null);
                cashflowModel.BaseYearMinus_2 = Convert.ToDecimal(collection["CashB2"].ToString() != "" ? collection["CashB2"].ToString() : null);
                cashflowModel.BaseYearMinus_3 = Convert.ToDecimal(collection["CashB3"].ToString() != "" ? collection["CashB3"].ToString() : null);
                cashflowModel.BaseYearMinus_4 = Convert.ToDecimal(collection["CashB4"].ToString() != "" ? collection["CashB4"].ToString() : null);
                cashflowModel.BaseYearMinus_5 = Convert.ToDecimal(collection["CashB5"].ToString() != "" ? collection["CashB5"].ToString() : null);
                assetsLiabilitiesList.Add(cashflowModel);

                AssetsLiabilitiesViewModel CashEquivalents = new AssetsLiabilitiesViewModel();
                CashEquivalents.AssetsType = "Cash Equivalents";
                CashEquivalents.BaseYearMinus_1 = Convert.ToDecimal(collection["CashEquiB1"].ToString() != "" ? collection["CashEquiB1"].ToString() : null);
                CashEquivalents.BaseYearMinus_2 = Convert.ToDecimal(collection["CashEquiB2"].ToString() != "" ? collection["CashEquiB2"].ToString() : null);
                CashEquivalents.BaseYearMinus_3 = Convert.ToDecimal(collection["CashEquiB3"].ToString() != "" ? collection["CashEquiB3"].ToString() : null);
                CashEquivalents.BaseYearMinus_4 = Convert.ToDecimal(collection["CashEquiB4"].ToString() != "" ? collection["CashEquiB4"].ToString() : null);
                CashEquivalents.BaseYearMinus_5 = Convert.ToDecimal(collection["CashEquiB5"].ToString() != "" ? collection["CashEquiB5"].ToString() : null);
                assetsLiabilitiesList.Add(CashEquivalents);

                AssetsLiabilitiesViewModel DebtShortTerm = new AssetsLiabilitiesViewModel();
                DebtShortTerm.AssetsType = "Debt-Short Term";
                DebtShortTerm.BaseYearMinus_1 = Convert.ToDecimal(collection["ShortTermB1"].ToString() != "" ? collection["ShortTermB1"].ToString() : null);
                DebtShortTerm.BaseYearMinus_2 = Convert.ToDecimal(collection["ShortTermB2"].ToString() != "" ? collection["ShortTermB2"].ToString() : null);
                DebtShortTerm.BaseYearMinus_3 = Convert.ToDecimal(collection["ShortTermB3"].ToString() != "" ? collection["ShortTermB3"].ToString() : null);
                DebtShortTerm.BaseYearMinus_4 = Convert.ToDecimal(collection["ShortTermB4"].ToString() != "" ? collection["ShortTermB4"].ToString() : null);
                DebtShortTerm.BaseYearMinus_5 = Convert.ToDecimal(collection["ShortTermB5"].ToString() != "" ? collection["ShortTermB5"].ToString() : null);
                assetsLiabilitiesList.Add(DebtShortTerm);

                AssetsLiabilitiesViewModel DebtLongTerm = new AssetsLiabilitiesViewModel();
                DebtLongTerm.AssetsType = "Debt-Long Term";
                DebtLongTerm.BaseYearMinus_1 = Convert.ToDecimal(collection["LongTermB1"].ToString() != "" ? collection["LongTermB1"].ToString() : null);
                DebtLongTerm.BaseYearMinus_2 = Convert.ToDecimal(collection["LongTermB2"].ToString() != "" ? collection["LongTermB2"].ToString() : null);
                DebtLongTerm.BaseYearMinus_3 = Convert.ToDecimal(collection["LongTermB3"].ToString() != "" ? collection["LongTermB3"].ToString() : null);
                DebtLongTerm.BaseYearMinus_4 = Convert.ToDecimal(collection["LongTermB4"].ToString() != "" ? collection["LongTermB4"].ToString() : null);
                DebtLongTerm.BaseYearMinus_5 = Convert.ToDecimal(collection["LongTermB5"].ToString() != "" ? collection["LongTermB5"].ToString() : null);
                assetsLiabilitiesList.Add(DebtLongTerm);

                AssetsLiabilitiesViewModel Minority = new AssetsLiabilitiesViewModel();
                Minority.AssetsType = "Minority Interest";
                Minority.BaseYearMinus_1 = Convert.ToDecimal(collection["MinorityB1"].ToString() != "" ? collection["MinorityB1"].ToString() : null);
                Minority.BaseYearMinus_2 = Convert.ToDecimal(collection["MinorityB2"].ToString() != "" ? collection["MinorityB2"].ToString() : null);
                Minority.BaseYearMinus_3 = Convert.ToDecimal(collection["MinorityB3"].ToString() != "" ? collection["MinorityB3"].ToString() : null);
                Minority.BaseYearMinus_4 = Convert.ToDecimal(collection["MinorityB4"].ToString() != "" ? collection["MinorityB4"].ToString() : null);
                Minority.BaseYearMinus_5 = Convert.ToDecimal(collection["MinorityB5"].ToString() != "" ? collection["MinorityB5"].ToString() : null);
                assetsLiabilitiesList.Add(Minority);

                List<CashFlowsViewModel> cashflowList = new List<CashFlowsViewModel>();
                CashFlowsViewModel cash = new CashFlowsViewModel();
                cash.CashFlowType = "Opening Balance";
                cash.BaseYearMinus_1 = Convert.ToDecimal(collection["OpeningBalB1"].ToString() != "" ? collection["OpeningBalB1"].ToString() : null);
                cash.BaseYearMinus_2 = Convert.ToDecimal(collection["OpeningBalB2"].ToString() != "" ? collection["OpeningBalB2"].ToString() : null);
                cash.BaseYearMinus_3 = Convert.ToDecimal(collection["OpeningBalB3"].ToString() != "" ? collection["OpeningBalB3"].ToString() : null);
                cash.BaseYearMinus_4 = Convert.ToDecimal(collection["OpeningBalB4"].ToString() != "" ? collection["OpeningBalB4"].ToString() : null);
                cash.BaseYearMinus_5 = Convert.ToDecimal(collection["OpeningBalB5"].ToString() != "" ? collection["OpeningBalB5"].ToString() : null);
                cashflowList.Add(cash);

                CashFlowsViewModel Operating = new CashFlowsViewModel();
                Operating.CashFlowType = "From Operating Activities";
                Operating.BaseYearMinus_1 = Convert.ToDecimal(collection["OperatingActB1"].ToString() != "" ? collection["OperatingActB1"].ToString() : null);
                Operating.BaseYearMinus_2 = Convert.ToDecimal(collection["OperatingActB2"].ToString() != "" ? collection["OperatingActB2"].ToString() : null);
                Operating.BaseYearMinus_3 = Convert.ToDecimal(collection["OperatingActB3"].ToString() != "" ? collection["OperatingActB3"].ToString() : null);
                Operating.BaseYearMinus_4 = Convert.ToDecimal(collection["OperatingActB4"].ToString() != "" ? collection["OperatingActB4"].ToString() : null);
                Operating.BaseYearMinus_5 = Convert.ToDecimal(collection["OperatingActB5"].ToString() != "" ? collection["OperatingActB5"].ToString() : null);
                cashflowList.Add(Operating);

                CashFlowsViewModel Financing = new CashFlowsViewModel();
                Financing.CashFlowType = "From Financing Activities";
                Financing.BaseYearMinus_1 = Convert.ToDecimal(collection["FinancingActB1"].ToString() != "" ? collection["FinancingActB1"].ToString() : null);
                Financing.BaseYearMinus_2 = Convert.ToDecimal(collection["FinancingActB2"].ToString() != "" ? collection["FinancingActB2"].ToString() : null);
                Financing.BaseYearMinus_3 = Convert.ToDecimal(collection["FinancingActB3"].ToString() != "" ? collection["FinancingActB3"].ToString() : null);
                Financing.BaseYearMinus_4 = Convert.ToDecimal(collection["FinancingActB4"].ToString() != "" ? collection["FinancingActB4"].ToString() : null);
                Financing.BaseYearMinus_5 = Convert.ToDecimal(collection["FinancingActB5"].ToString() != "" ? collection["FinancingActB5"].ToString() : null);
                cashflowList.Add(Financing);

                CashFlowsViewModel Investing = new CashFlowsViewModel();
                Investing.CashFlowType = "From Investing Activities";
                Investing.BaseYearMinus_1 = Convert.ToDecimal(collection["InvestingActB1"].ToString() != "" ? collection["InvestingActB1"].ToString() : null);
                Investing.BaseYearMinus_2 = Convert.ToDecimal(collection["InvestingActB2"].ToString() != "" ? collection["InvestingActB2"].ToString() : null);
                Investing.BaseYearMinus_3 = Convert.ToDecimal(collection["InvestingActB3"].ToString() != "" ? collection["InvestingActB3"].ToString() : null);
                Investing.BaseYearMinus_4 = Convert.ToDecimal(collection["InvestingActB4"].ToString() != "" ? collection["InvestingActB4"].ToString() : null);
                Investing.BaseYearMinus_5 = Convert.ToDecimal(collection["InvestingActB5"].ToString() != "" ? collection["InvestingActB5"].ToString() : null);
                cashflowList.Add(Investing);

                CashFlowsViewModel ClosingBalance = new CashFlowsViewModel();
                ClosingBalance.CashFlowType = "Closing Balance";
                ClosingBalance.BaseYearMinus_1 = Convert.ToDecimal(collection["ClosingBalB1"].ToString() != "" ? collection["ClosingBalB1"].ToString() : null);
                ClosingBalance.BaseYearMinus_2 = Convert.ToDecimal(collection["ClosingBalB2"].ToString() != "" ? collection["ClosingBalB2"].ToString() : null);
                ClosingBalance.BaseYearMinus_3 = Convert.ToDecimal(collection["ClosingBalB3"].ToString() != "" ? collection["ClosingBalB3"].ToString() : null);
                ClosingBalance.BaseYearMinus_4 = Convert.ToDecimal(collection["ClosingBalB4"].ToString() != "" ? collection["ClosingBalB4"].ToString() : null);
                ClosingBalance.BaseYearMinus_5 = Convert.ToDecimal(collection["ClosingBalB5"].ToString() != "" ? collection["ClosingBalB5"].ToString() : null);
                cashflowList.Add(ClosingBalance);

                //List<CompanyTransactionsViewModel> transactionsList = new List<CompanyTransactionsViewModel>();
                //CompanyTransactionsViewModel transaction1 = new CompanyTransactionsViewModel();
                //transaction1.Value = Convert.ToDecimal(collection["Value1"].ToString() != "" ? collection["Value1"].ToString() : null);
                //transaction1.OtherParty = collection["OtherParty1"].ToString() != "" ? collection["OtherParty1"].ToString() : null;
                //transaction1.Date = Convert.ToDateTime(collection["Date1"].ToString() != "" ? collection["Date1"].ToString() : null);
                //transaction1.Rationale = collection["Rationale1"].ToString() != "" ? collection["Rationale1"].ToString() : null;
                //transactionsList.Add(transaction1);

                //CompanyTransactionsViewModel transaction2 = new CompanyTransactionsViewModel();
                //transaction2.Value = Convert.ToDecimal(collection["Value2"].ToString() != "" ? collection["Value2"].ToString() : null);
                //transaction2.OtherParty = collection["OtherParty2"].ToString() != "" ? collection["OtherParty2"].ToString() : null;
                //transaction2.Date = Convert.ToDateTime(collection["Date2"].ToString() != "" ? collection["Date2"].ToString() : null);
                //transaction2.Rationale = collection["Rationale2"].ToString() != "" ? collection["Rationale2"].ToString() : null;
                //transactionsList.Add(transaction2);

                //CompanyTransactionsViewModel transaction3 = new CompanyTransactionsViewModel();
                //transaction3.Value = Convert.ToDecimal(collection["Value3"].ToString() != "" ? collection["Value3"].ToString() : null);
                //transaction3.OtherParty = collection["OtherParty3"].ToString() != "" ? collection["OtherParty3"].ToString() : null;
                //transaction3.Date = Convert.ToDateTime(collection["Date3"].ToString() != "" ? collection["Date3"].ToString() : null);
                //transaction3.Rationale = collection["Rationale3"].ToString() != "" ? collection["Rationale3"].ToString() : null;
                //transactionsList.Add(transaction3);
                mainModel.assetsLiabilitiesViewModels = assetsLiabilitiesList;
                mainModel.cashFlowsViewModel = cashflowList;
                mainModel.expensesBreakdownViewModel = expenseList;
                mainModel.companyTransactionsInsertUpdateViewModel = model.companyTransactionsInsertUpdateViewModel;
                mainModel.CurrencyId = model.companyFinancialAnalysisInsertUpdateViewModel.CurrencyId;
                mainModel.CompanyId = model.companyFinancialAnalysisInsertUpdateViewModel.CompanyId;
                mainModel.AuthorNotes = model.companyFinancialAnalysisInsertUpdateViewModel.AuthorNotes;
                mainModel.ApproverNotes = model.companyFinancialAnalysisInsertUpdateViewModel.ApproverNotes;
                mainModel.ValueConversionId = model.ValueConversionId;
                // mainModel.companyFinancialAnalysisInsertUpdateViewModel = model.companyFinancialAnalysisInsertUpdateViewModel;

                //HttpResponseMessage responseTrendNote = _serviceRepository.GetByIdResponse("TrendNote", model.companyFinancialAnalysisInsertUpdateViewModel.CompanyId.ToString(), "Fundamentals");
                //responseTrendNote.EnsureSuccessStatusCode();
                //model.trendNoteViewModel = responseTrendNote.Content.ReadAsAsync<TrendNoteViewModel>().Result;

                //model.trendNoteViewModel.TrendType = "Value";
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                if (actionName.Equals("Save"))
                {
                    if (model != null)
                    {
                        //model.trendNoteViewModel.TrendStatusName = "Draft";
                        //testMainCompanyFinancialAnalysisInsertUpdateViewModel
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, mainModel);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            //ViewBag.Message = viewModel.Message;
                            return RedirectToAction("Index", new { CompanyId = model.companyFinancialAnalysisInsertUpdateViewModel.CompanyId.ToString(), ProjectId = model.ProjectId });
                        }
                    }

                }
                else if (actionName.Equals("Submit"))
                {
                    if (model != null)
                    {
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            //ViewBag.Message = viewModel.Message;
                            return RedirectToAction("Index", new { CompanyId = model.companyFinancialAnalysisInsertUpdateViewModel.CompanyId.ToString(), ProjectId = model.ProjectId });
                        }
                    }
                }

                else if (actionName.Equals("Rejected"))
                {
                    HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, mainModel);
                    response.EnsureSuccessStatusCode();
                    ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                    if (viewModel != null)
                    {
                        //ViewBag.Message = viewModel.Message;
                        return RedirectToAction("Index", new { CompanyId = mainModel.CompanyId.ToString(), ProjectId = model.ProjectId });
                    }
                }

                else if (actionName.Equals("Next"))
                    return RedirectToAction("Index", "RevenueProfitAnalysis", new { CompanyId = mainModel.CompanyId.ToString(), ProjectId = model.ProjectId });
                else if (actionName.Equals("Previous"))
                    return RedirectToAction("Index", "CompanyShareVolume", new { CompanyId = mainModel.CompanyId.ToString(), ProjectId = model.ProjectId });
                else if (actionName.Equals("Cancel"))
                    return RedirectToAction("Index", "#", new { CompanyId = mainModel.CompanyId.ToString(), ProjectId = model.ProjectId });


            }

            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Ok();
        }
    }
}