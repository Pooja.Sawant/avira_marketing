﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Web.Utils;
//using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.Web.Controllers
{
    [NoCache]
    [Authorization]
    public class TrendTimeTagController : BaseController
    {
        private readonly IServiceRepository _serviceRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;
        string contentRootPath = string.Empty;
        private ILoggerManager _logger;

        public TrendTimeTagController(IServiceRepository serviceRepository, ILoggerManager logger, IHostingEnvironment hostingEnvironment, IConfiguration configuration)
        {
            _hostingEnvironment = hostingEnvironment;
            _serviceRepository = serviceRepository;
            _logger = logger;
            _configuration = configuration;
            contentRootPath = _hostingEnvironment.ContentRootPath;
        }

        [HttpGet]
        public ActionResult Index(string id, string currentFilter, string search, int PageStart, string Direction, int totalRecords, int currentpage)
        {
            MainTrendTimeTagInsertUpdateDbViewModel model = new MainTrendTimeTagInsertUpdateDbViewModel();
            try
            {
                SetRoleList();
                ViewBag.Message = string.Empty;
                model.TrendId = new Guid(id);
                model.TimeTagName = search;
                //int PageStart = 0;
                int pagesize = Common.TrendPagingSize;
                int SortDirection = 1;

                if (Direction == "Next" && (PageStart == 0 || PageStart < (totalRecords - pagesize)))
                {
                    PageStart = PageStart + pagesize;
                }
                else if (Direction == "Previous")
                {
                    if (PageStart >= pagesize)
                        PageStart = PageStart - pagesize;
                    else
                    {
                        PageStart = 0;
                    }
                }
                if (string.IsNullOrEmpty(Direction) && currentpage > 0)
                {
                    //totalRecords


                    PageStart = currentpage * pagesize;

                    if (PageStart >= totalRecords)
                    {
                        PageStart = (totalRecords - pagesize) + 1;
                    }
                    else
                    {
                        PageStart = 0;
                    }

                }
                else
                {
                    //PageStart = PageStart ?? 0 + pagesize;
                }

                //------Get Static master data for Impact           
                HttpResponseMessage responseImpactType = _serviceRepository.GetResponse("ImpactType");
                responseImpactType.EnsureSuccessStatusCode();
                ViewBag.ImpactList = responseImpactType.Content.ReadAsAsync<List<ImpactTypeViewModel>>().Result;


                HttpResponseMessage responseTrend = _serviceRepository.GetByIdResponse("Trend", id);
                responseTrend.EnsureSuccessStatusCode();
                TrendViewModel trend = responseTrend.Content.ReadAsAsync<TrendViewModel>().Result;
                if (trend != null)
                    model.TrendModel = trend;

                //------Get trend note by trend Id
                HttpResponseMessage responseTrendNote = _serviceRepository.GetByIdResponse("TrendNote", id, "Time Tag", "id");
                responseTrendNote.EnsureSuccessStatusCode();
                var tnm = responseTrendNote.Content.ReadAsAsync<TrendNoteViewModel>().Result;
                model.TrendNoteModel = tnm ?? new TrendNoteViewModel();
                model.TrendModel.TrendStatusName = model.TrendNoteModel.TrendStatusName;
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.GetResponse(contollerName, model.TrendId, PageStart, pagesize, SortDirection, string.Empty, search);
                response.EnsureSuccessStatusCode();
                List<TrendTimeTagViewModel> trendTimeTagMap = response.Content.ReadAsAsync<List<TrendTimeTagViewModel>>().Result;
                model.TrendTimeTagInsertUpdate = trendTimeTagMap;
                if (trendTimeTagMap != null && trendTimeTagMap.Count > 0)
                {
                    totalRecords = trendTimeTagMap.FirstOrDefault().TotalItems;
                    ViewBag.totalPage = (int)Math.Ceiling((double)trendTimeTagMap.Count / pagesize);
                }

                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, model.TrendModel.ProjectID);
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                }

                ViewBag.totalRecords = totalRecords;
                ViewBag.PageStart = PageStart;
                ViewBag.currentpage = currentpage;
                ViewBag.Id = model.TrendId;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(model);
        }
        private void SetRoleList()
        {
            try
            {
                var roles = HttpContext.Session.GetString("userRoles");
                var userRoles = JsonConvert.DeserializeObject<List<RoleViewModel>>(roles);
                List<string> RoleListItems = new List<string>();
                foreach (var item in userRoles)
                    RoleListItems.Add(item.UserRoleName);
                ViewBag.UserRoleList = RoleListItems;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        } 
        [HttpPost]
        public IActionResult Index(MainTrendTimeTagInsertUpdateDbViewModel model, string actionName)
        {
            SetRoleList();
            ViewBag.Message = string.Empty;
            bool bResult = false;
            try
            {
                model.TrendNoteModel.TrendType = "Time Tag";
                if (actionName.Equals("save"))
                {

                    bResult = validate(model);
                    if (bResult)
                    {
                        model.TrendNoteModel.TrendStatusName = "Draft";
                        var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                        HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, model);
                        response.EnsureSuccessStatusCode();
                        ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            //ViewBag.Message = viewModel.Message;
                            return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.TimeTagName });
                        }
                    }

                }
                else if (actionName.Equals("Submitted"))
                {

                    bResult = validate(model);
                    if (bResult)
                    {
                        model.TrendNoteModel.TrendStatusName = "Submitted";
                        var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                        HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, model);
                        response.EnsureSuccessStatusCode();
                        ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            //ViewBag.Message = viewModel.Message;
                            return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.TimeTagName });
                        }
                    }

                }
                else if (actionName.Equals("Rejected"))
                {

                    bResult = validate(model);
                    if (bResult)
                    {
                        model.TrendNoteModel.TrendStatusName = "Rejected";
                        var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                        HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, model);
                        response.EnsureSuccessStatusCode();
                        ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            //ViewBag.Message = viewModel.Message;
                            return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.TimeTagName });
                        }
                    }

                }
                else if (actionName.Equals("Approve"))
                {

                    bResult = validate(model);
                    if (bResult)
                    {
                        model.TrendNoteModel.TrendStatusName = "Approved";
                        var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                        HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, model);
                        response.EnsureSuccessStatusCode();
                        ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            //ViewBag.Message = viewModel.Message;
                            return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.TimeTagName });
                        }
                    }

                }
                else if (actionName.Equals("search"))
                {
                    return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.TimeTagName });
                }
                else if (actionName.Equals("Next"))
                    return RedirectToAction("Index", "TrendEcoSystemMap", new { id = model.TrendId.ToString() });
                else if (actionName.Equals("Previous"))
                    return RedirectToAction("Index", "TrendCompanyGroupMap", new { id = model.TrendId.ToString() });
                else if (actionName.Equals("Cancel"))
                {
                    return RedirectToAction("Index", "TrendTimeTag", new { id = model.TrendId.ToString() });
                }
                HttpResponseMessage responseImpactType = _serviceRepository.GetResponse("ImpactType");
                responseImpactType.EnsureSuccessStatusCode();
                ViewBag.ImpactList = responseImpactType.Content.ReadAsAsync<List<ImpactTypeViewModel>>().Result;

                HttpResponseMessage responseTrend = _serviceRepository.GetByIdResponse("Trend", model.TrendId.ToString());

                TrendViewModel trend = responseTrend.Content.ReadAsAsync<TrendViewModel>().Result;
                model.TrendModel = trend ?? new TrendViewModel();
                HttpResponseMessage responseTrendNote = _serviceRepository.GetByIdResponse("TrendNote", model.TrendId.ToString(), "Company Group", "id");
                responseTrendNote.EnsureSuccessStatusCode();
                var tnm = responseTrendNote.Content.ReadAsAsync<TrendNoteViewModel>().Result;
                model.TrendNoteModel = tnm ?? new TrendNoteViewModel();
                model.TrendModel.TrendStatusName = model.TrendNoteModel.TrendStatusName;

                ViewBag.Id = model.TrendId;
                View(model);

            }
             
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return View(model);
        }

        /// <summary>
        /// Method to validate Form
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private bool validate(MainTrendTimeTagInsertUpdateDbViewModel model)
        {
            if (model.TrendTimeTagInsertUpdate != null && model.TrendTimeTagInsertUpdate.Count(x => x.ImpactId != Guid.Empty) < 1)
            {
                ViewBag.Message = "Please select atlease one TimeTag for the trend time tag mapping.";
                return false;
            }
            else if (model.TrendTimeTagInsertUpdate != null && model.TrendTimeTagInsertUpdate.Count(x => (x.ImpactId != Guid.Empty && x.EndUser)) < 1)
            {
                ViewBag.Message = "Please select atleast one end user for the trend mapping.";
                return false;
            }
            //else if (model.TrendTimeTagInsertUpdate != null && model.TrendTimeTagInsertUpdate.Where(x => x.ImpactId != Guid.Empty).Select(x => x.ImpactId).Distinct().Count() == 1)
            //{
            //    ViewBag.Message = "Minimum 1 Direct and Minimum 1 Indirect to be selected.";
            //    return false;
            //}
            else if (model.TrendTimeTagInsertUpdate != null && model.TrendTimeTagInsertUpdate.Any(x => x.ImpactId == Guid.Empty && x.EndUser))
            {
                ViewBag.Message = "Please select direct/indirect impact value.";
                return false;
            }
            if (model.TrendNoteModel.AuthorRemark == null || model.TrendNoteModel.AuthorRemark == "" || model.TrendNoteModel.AuthorRemark == string.Empty)
            {
                ViewBag.Message = "Please enter Author Remark!!";
                return false;
            }
            else
            {
                ViewBag.Message = string.Empty;
                return true;
            }
        }

    }  
}