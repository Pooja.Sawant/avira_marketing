﻿CREATE TABLE [dbo].[StagingQualitativeRegionMapping] (
    [Id]          UNIQUEIDENTIFIER NOT NULL,
    [ImportId]    UNIQUEIDENTIFIER NULL,
    [RowNo]       INT              NULL,
    [MarketName]  NVARCHAR (255)   NULL,
    [RegionName]  NVARCHAR (255)   NULL,
    [CountryName] NVARCHAR (255)   NULL,
    [MarketId]    UNIQUEIDENTIFIER NULL,
    [RegionId]    UNIQUEIDENTIFIER NULL,
    [CountryId]   UNIQUEIDENTIFIER NULL,
    [ProjectId]   UNIQUEIDENTIFIER NULL,
    [ErrorNotes]  NVARCHAR (1000)  NULL,
    CONSTRAINT [PK_StagingQualitativeRegionMapping] PRIMARY KEY CLUSTERED ([Id] ASC)
);



