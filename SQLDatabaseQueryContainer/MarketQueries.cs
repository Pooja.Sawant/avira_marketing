﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public static class MarketQueries
    {
        public static readonly string StoreProcInsert = "SApp_insertMarket";

        public static readonly string StoreProcUpdate = "SApp_UpdateMarket";

        public static string StoreProcSelectAll = "SApp_MarketGridWithFilters";

        public static string StoreProcSelectAllParent = "SApp_GetAllParentMarket";
    }
}
