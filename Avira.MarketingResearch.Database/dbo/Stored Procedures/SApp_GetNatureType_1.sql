﻿  
  
CREATE PROCEDURE [dbo].[SApp_GetNatureType]  

AS  
/*================================================================================  
Procedure Name: [SApp_GetNatureType]  
Author: Gopi  
Create date: 05/02/2019  
Description: Get list of NatureTypes from table   
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
05/02/2019 Sai Krishna   Initial Version  
================================================================================*/  
  
BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
 SELECT [Nature].[Id],  
  [Nature].[NatureTypeName]   
FROM [dbo].[Nature]  
where ([Nature].IsDeleted = 0)   
 END