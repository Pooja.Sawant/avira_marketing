﻿CREATE TYPE [dbo].[udtGUIDCompanyClientsMultiValue] AS TABLE (
    [Id]             UNIQUEIDENTIFIER NOT NULL,
    [CompanyId]      UNIQUEIDENTIFIER NOT NULL,
    [ClientName]     NVARCHAR (256)   NULL,
    [ClientIndustry] UNIQUEIDENTIFIER NULL,
    [ClientRemark]   NVARCHAR (512)   NULL);

