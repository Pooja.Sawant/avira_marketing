﻿CREATE TABLE [dbo].[MarketGraphData] (
    [ID]              UNIQUEIDENTIFIER NOT NULL,
    [ProjectID]       UNIQUEIDENTIFIER NOT NULL,
    [SegmentID]       UNIQUEIDENTIFIER NOT NULL,
    [MetricType]      NVARCHAR (100)   NOT NULL,
    [BarChartJson]    NVARCHAR (MAX)   NULL,
    [MapJson]         NVARCHAR (MAX)   NULL,
    [TableJson]       NVARCHAR (MAX)   NULL,
    [CreatedOn]       DATETIME2 (7)    NULL,
    [UserCreatedById] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_MarketGraphData] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_MarketGraphData_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_MarketGraphData_Project] FOREIGN KEY ([ProjectID]) REFERENCES [dbo].[Project] ([ID]),
    CONSTRAINT [FK_MarketGraphData_Segment] FOREIGN KEY ([SegmentID]) REFERENCES [dbo].[Segment] ([Id])
);

