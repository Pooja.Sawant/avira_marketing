﻿          
          
CREATE procedure [dbo].[SApp_insertupdateCompanyProfitGeographicSplit]        
(              
@RegionList dbo.udtGUIDGeographicSplitMultiValue READONLY,    
@CompanyId uniqueidentifier,          
@CompanyProfitId uniqueidentifier,        
@Year int,    
@UserCreatedById uniqueidentifier          
--@CreatedOn Datetime          
)              
as              
/*================================================================================              
Procedure Name: SApp_insertupdateCompanyProfitGeographicSplit        
Change History              
Date  Developer  Reason              
__________ ____________ ______________________________________________________              
05/06/2019 Gopi   Initial Version              
================================================================================*/              
BEGIN              
SET NOCOUNT ON;              
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;            
declare @ErrorMsg NVARCHAR(2048)          
        
BEGIN TRY                 
                 
--DELETE FROM [DBO].[CompanyProfitGeographicSplitMap]       
--WHERE [CompanyProfitId] = @CompanyProfitId    
--AND NOT EXISTS(SELECT 1 FROM @RegionList LIST     
--             WHERE LIST.RegionId = [CompanyProfitGeographicSplitMap].[RegionId])    
declare @UnitId uniqueidentifier;

select @UnitId = UnitId
from CompanyProfit  
where id = @CompanyProfitId;

DELETE FROM [DBO].[CompanyProfitGeographicSplitMap]       
WHERE [CompanyId] = @CompanyId      
    
        
INSERT INTO [dbo].[CompanyProfitGeographicSplitMap]([Id],      
  [CompanyProfitId],      
  [CompanyId],              
  [RegionId],    
  [RegionValue],    
  [Year],    
        [IsDeleted],              
        [UserCreatedById],              
        [CreatedOn])              
 SELECT NEWID(),       
 @CompanyProfitId,    
 @CompanyId,           
   RegionId,    
  dbo.fun_UnitConversion(@UnitId,RegionValue) as RegionValue,    
   @Year,    
   0,              
   @UserCreatedById,               
   GETDATE()       
   from @RegionList list        
   where not exists (select 1 from [CompanyProfitGeographicSplitMap]         
    where list.RegionId = [CompanyProfitGeographicSplitMap].[RegionId])        
        
SELECT StoredProcedureName ='SApp_insertupdateCompanyProfitGeographicSplit',Message =@ErrorMsg,Success = 1;               
            
END TRY              
BEGIN CATCH              
    -- Execute the error retrieval routine.              
 DECLARE @ErrorNumber int;              
 DECLARE @ErrorSeverity int;              
 DECLARE @ErrorProcedure varchar(100);              
 DECLARE @ErrorLine int;              
 DECLARE @ErrorMessage varchar(500);              
              
  SELECT @ErrorNumber = ERROR_NUMBER(),              
        @ErrorSeverity = ERROR_SEVERITY(),              
        @ErrorProcedure = ERROR_PROCEDURE(),              
        @ErrorLine = ERROR_LINE(),              
        @ErrorMessage = ERROR_MESSAGE()              
              
 insert into dbo.Errorlog(ErrorNumber,              
       ErrorSeverity,              
       ErrorProcedure,              
       ErrorLine,              
       ErrorMessage,              
       ErrorDate)              
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());              
            
 set @ErrorMsg = 'Error while creating a new CompanyProfitGeographicSplit, please contact Admin for more details.';              
          
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine            
    ,Message =@ErrorMsg,Success = 0;               
               
END CATCH              
              
              
end