﻿
----drop Procedure SFab_GetCIPriceInformationTableData

----execute [SFab_GetCIPriceInformationTableData]  @CompanyID = 'ab8f9f92-fae8-4700-a90f-2a7bcab36cce'

CREATE   Procedure [dbo].[SFab_GetCIPriceInformationTableData] 
(
	@CompanyID uniqueidentifier
)
As
/*================================================================================  
Procedure Name: [SFab_GetCIPriceInformationTableData]  
Author: Nitin 
Create date: 14-Nov-2019  
Description: Get CI Price Information Tab Data
Change History  
Date         Developer     Reason  
____________ __________  ______________________________________________________  
12-Nov-2019  Nitin       Initial Version  
================================================================================*/  
BEGIN

	--Declare @CompanyID uniqueidentifier =  '9BC77007-8949-41DF-8214-FE3E8EFB572F'

	Declare @UnitId uniqueidentifier
	Declare @USDCurrencyId uniqueidentifier
	Declare @LastUpdatePriceUSD decimal (19, 4),
			@LastUpdatePriceInReportingCurrency decimal (19, 4),
			@PercentageOf52WeekHigh decimal (19, 4),
			@LastTradedVolumn decimal (19, 4),
			@LastTradedDate DateTime,
			@Price52WeekHighInReportingCurrency decimal (19, 4),
			@Price52WeekLowInReportingCurrency decimal (19, 4),
			@Last3MonthsAverageTradingVolume decimal (19, 4),
			@Last6MonthsAverageTradingVolume decimal (19, 4),
			@Last12MonthsAverageTradingVolume decimal (19, 4),
			@SimpleMovingAverage50Days decimal (19, 4),
			@SimpleMovingAverage100Days decimal (19, 4),
			@SimpleMovingAverage200Days decimal (19, 4),
			@ReportingCurrencyCode nvarchar(20),
			@VolumnUnitName nvarchar(20)

	Select @USDCurrencyId = [dbo].[fun_GetCurrencyIDForUSD]()	

	Declare @CICompanyRatioAnalysisTabData Table(
		CompanyId uniqueidentifier,
		LastUpdatePriceUSD decimal (19, 4),
		LastUpdatePriceInReportingCurrency decimal (19, 4),
		ReportingCurrencyCode nvarchar(20),
		PercentageOf52WeekHigh decimal (19, 4),
		LastTradedVolumn decimal (19, 4),
		VolumnUnitName nvarchar(20),
		TradedDate DateTime,
		Price52WeekHighInReportingCurrency decimal (19, 4),
		Price52WeekLowInReportingCurrency decimal (19, 4),
		Last3MonthsAverageTradingVolume decimal (19, 4),
		Last6MonthsAverageTradingVolume decimal (19, 4),
		Last12MonthsAverageTradingVolume decimal (19, 4),
		SimpleMovingAverage50Days decimal (19, 4),
		SimpleMovingAverage100Days decimal (19, 4),
		SimpleMovingAverage200Days decimal (19, 4)
	)
	Select @UnitId = Id 
	From ValueConversion
	Where ValueName = 'Millions';
	
	Select Top 1 --@CompanyID, 
			@LastUpdatePriceUSD = [dbo].[fun_UnitDeConversion](CPV.CurrencyUnitId, CPV.Price),
			@LastUpdatePriceInReportingCurrency = [dbo].[fun_UnitDeConversion](CPV.CurrencyUnitId, [dbo].[fun_CurrencyConversion] (CPV.Price, @USDCurrencyId, CPV.CurrencyId, null)),
			@LastTradedVolumn = [dbo].[fun_UnitDeConversion](CPV.VolumeUnitId, CPV.Volume),
			@LastTradedDate = CPV.[Date], 
			@ReportingCurrencyCode = C.CurrencyCode,
			@VolumnUnitName = VC.ValueName
	From CompanyPriceVolume CPV 
		Inner Join Currency C On CPV.CurrencyId = C.Id
		Inner Join ValueConversion VC On CPV.VolumeUnitId = VC.Id
	Where CompanyId = @CompanyID 
	Order By CPV.[Date] Desc
	print 1
	Select @Price52WeekHighInReportingCurrency = Max([dbo].[fun_UnitDeConversion](CurrencyUnitId, Price)), 
		   @Price52WeekLowInReportingCurrency = Min([dbo].[fun_UnitDeConversion](CurrencyUnitId, Price))
	From CompanyPriceVolume
	Where CompanyId = @CompanyID And [Date] Between DateAdd(DAY, -1, DATEADD(YEAR, -1, @LastTradedDate)) And @LastTradedDate

	Select @Last3MonthsAverageTradingVolume = AVG([dbo].[fun_UnitDeConversion](VolumeUnitId, Volume))
	From CompanyPriceVolume
	Where CompanyId = @CompanyID And [Date] Between DateAdd(DAY, -89, @LastTradedDate) And @LastTradedDate
	
	Select @Last6MonthsAverageTradingVolume = AVG([dbo].[fun_UnitDeConversion](VolumeUnitId, Volume))
	From CompanyPriceVolume
	Where CompanyId = @CompanyID And [Date] Between DateAdd(DAY, -179, @LastTradedDate) And @LastTradedDate

	Select @Last12MonthsAverageTradingVolume = AVG([dbo].[fun_UnitDeConversion](VolumeUnitId, Volume))
	From CompanyPriceVolume
	Where CompanyId = @CompanyID And [Date] Between DateAdd(DAY, -365, @LastTradedDate) And @LastTradedDate
	
	Select @SimpleMovingAverage50Days = AVG([dbo].[fun_UnitDeConversion](CurrencyUnitId, [dbo].[fun_CurrencyConversion] (Price, @USDCurrencyId, CurrencyId, null)))
	From CompanyPriceVolume
	Where CompanyId = @CompanyID And [Date] Between DateAdd(DAY, -49, @LastTradedDate) And @LastTradedDate
	
	Select @SimpleMovingAverage100Days = AVG([dbo].[fun_UnitDeConversion](CurrencyUnitId, [dbo].[fun_CurrencyConversion] (Price, @USDCurrencyId, CurrencyId, null)))
	From CompanyPriceVolume
	Where CompanyId = @CompanyID And [Date] Between DateAdd(DAY, -99, @LastTradedDate) And @LastTradedDate

	Select @SimpleMovingAverage200Days = AVG([dbo].[fun_UnitDeConversion](CurrencyUnitId, [dbo].[fun_CurrencyConversion] (Price, @USDCurrencyId, CurrencyId, null)))
	From CompanyPriceVolume
	Where CompanyId = @CompanyID And [Date] Between DateAdd(DAY, -199, @LastTradedDate) And @LastTradedDate

	Insert Into @CICompanyRatioAnalysisTabData 
		(CompanyId, 
		LastUpdatePriceUSD, 
		LastUpdatePriceInReportingCurrency,
		ReportingCurrencyCode,
		LastTradedVolumn, 
		VolumnUnitName,
		TradedDate,
		Price52WeekHighInReportingCurrency,
		Price52WeekLowInReportingCurrency,
		PercentageOf52WeekHigh,
		Last3MonthsAverageTradingVolume,
		Last6MonthsAverageTradingVolume,
		Last12MonthsAverageTradingVolume,
		SimpleMovingAverage50Days,
		SimpleMovingAverage100Days,
		SimpleMovingAverage200Days)
	Values (@CompanyID, 
		@LastUpdatePriceUSD, 
		@LastUpdatePriceInReportingCurrency,
		@ReportingCurrencyCode,
		@LastTradedVolumn, 
		@VolumnUnitName,
		@LastTradedDate, 
		@Price52WeekHighInReportingCurrency, 
		@Price52WeekLowInReportingCurrency,
		(@LastUpdatePriceInReportingCurrency / Nullif(@Price52WeekHighInReportingCurrency, 0)) * 100.00,
		@Last3MonthsAverageTradingVolume,
		@Last6MonthsAverageTradingVolume,
		@Last12MonthsAverageTradingVolume,
		@SimpleMovingAverage50Days,
		@SimpleMovingAverage100Days,
		@SimpleMovingAverage200Days)

	Select CompanyId, 
		LastUpdatePriceUSD, 
		LastUpdatePriceInReportingCurrency,
		ReportingCurrencyCode,
		LastTradedVolumn, 
		VolumnUnitName,
		TradedDate,
		Price52WeekHighInReportingCurrency,
		Price52WeekLowInReportingCurrency,
		PercentageOf52WeekHigh,
		Last3MonthsAverageTradingVolume,
		Last6MonthsAverageTradingVolume,
		Last12MonthsAverageTradingVolume,
		SimpleMovingAverage50Days,
		SimpleMovingAverage100Days,
		SimpleMovingAverage200Days
	From @CICompanyRatioAnalysisTabData
	Order By TradedDate Desc 
END