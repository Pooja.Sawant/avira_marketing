﻿


CREATE procedure [dbo].[SFab_insertUpdateUserQueries]    
( 
@Id uniqueidentifier, 
@SupportCategoryId uniqueidentifier=null, 
@SupportSubCategoryId uniqueidentifier=null,
@UserMessage nvarchar(3000)=null , 
@UserId uniqueidentifier=null,
@ContactChanel nvarchar(50)=null,
@UserCreatedById uniqueidentifier=null,
@AccountOwner uniqueidentifier=null,
@StatusName nvarchar(50)=null,
@ActionPoint nvarchar(3000)=null ,
@OwnerRemarks nvarchar(3000)=null ,
@LastActionOn Datetime=null
)    
as    
/*================================================================================    
Procedure Name: SApp_insertUpdateUserQueries   
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
08/21/2019 Harshal   Initial Version    
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; 
declare @StatusID uniqueidentifier;
SET @StatusID =(SELECT Id FROM [dbo].[UserQueriesStatus] WHERE StatusName=@StatusName);   
declare @ErrorMsg NVARCHAR(2048)
  
BEGIN TRY       
If Exists (SELECT * FROM UserQueriesStatus Where Id=@Id )  
Begin
UPDATE [dbo].[UserQueries] SET
       StatusID=@StatusID,
	   AccountOwner=@AccountOwner,
	   ActionPoint=@ActionPoint,
	   OwnerRemarks=@OwnerRemarks,
       LastActionOn=@LastActionOn 
END  
ELSE
Begin  
insert into [dbo].[UserQueries](
        [Id],    
        [SupportCategoryId],    
        [SupportSubCategoryId], 
		[UserMessage],
		[UserId],
		[ContactChanel], 
		[StatusID],  
        [UserCreatedById],    
        [CreatedOn])    
 values(@Id,     
   @SupportCategoryId,     
   @SupportSubCategoryId, 
   @UserMessage,
   @UserId,
   @ContactChanel,
   @StatusID,    
   @UserCreatedById,     
   GETDATE());    
    

SELECT StoredProcedureName ='SApp_insertUpdateUserQueries',Message =@ErrorMsg,Success = 1;     
END    
END TRY    
BEGIN CATCH    
    -- Execute the error retrieval routine.    
 DECLARE @ErrorNumber int;    
 DECLARE @ErrorSeverity int;    
 DECLARE @ErrorProcedure varchar(100);    
 DECLARE @ErrorLine int;    
 DECLARE @ErrorMessage varchar(500);    
    
  SELECT @ErrorNumber = ERROR_NUMBER(),    
        @ErrorSeverity = ERROR_SEVERITY(),    
        @ErrorProcedure = ERROR_PROCEDURE(),    
        @ErrorLine = ERROR_LINE(),    
        @ErrorMessage = ERROR_MESSAGE()    
    
 insert into dbo.Errorlog(ErrorNumber,    
       ErrorSeverity,    
       ErrorProcedure,    
       ErrorLine,    
       ErrorMessage,    
       ErrorDate)    
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());    
  
 set @ErrorMsg = 'Error while creating/Update a User Query, please contact Admin for more details.';    
--THROW 50000,@ErrorMsg,1;      
--return(1)  
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;     
     
END CATCH    
    
    
end