﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.Trend
{
    public class TrendPreviewViewModel
    {
        public string TrendType { get; set; }
        public string Keywords { get; set; }
        public DateTime? KeywordsSubmittedDate { get; set; }
        public DateTime? KeywordsApprovedDate { get; set; }
        public bool KeywordsIsApproval { get; set; }
        public string Industry { get; set; }
        public DateTime? IndustrySubmittedDate { get; set; }
        public DateTime? IndustryApprovedDate { get; set; }
        public bool IndustryIsApproval { get; set; }
        public string Market { get; set; }
        public DateTime? MarketSubmittedDate { get; set; }
        public DateTime? MarketApprovedDate { get; set; }
        public bool MarketIsApproval { get; set; }
        public string Project { get; set; }
        public DateTime? ProjectSubmittedDate { get; set; }
        public DateTime? ProjectApprovedDate { get; set; }
        public bool ProjectIsApproval { get; set; }
        public string Geography { get; set; }
        public DateTime? GeographySubmittedDate { get; set; }
        public DateTime? GeographyApprovedDate { get; set; }
        public bool GeographyIsApproval { get; set; }
        public string CompanyGroups { get; set; }
        public DateTime? CompanyGroupsSubmittedDate { get; set; }
        public DateTime? CompanyGroupsApprovedDate { get; set; }
        public bool CompanyGroupsIsApproval { get; set; }
        public string Timetag { get; set; }
        public DateTime? TimetagSubmittedDate { get; set; }
        public DateTime? TimetagApprovedDate { get; set; }
        public bool TimetagIsApproval { get; set; }
        public string EcoSystem { get; set; }
        public DateTime? EcoSystemSubmittedDate { get; set; }
        public DateTime? EcoSystemApprovedDate { get; set; }
        public bool EcoSystemIsApproval { get; set; }
        public string KeyCompanies { get; set; }
        public DateTime? KeyCompaniesSubmittedDate { get; set; }
        public DateTime? KeyCompaniesApprovedDate { get; set; }
        public bool KeyCompaniesIsApproval { get; set; }
    }

    public class MainTrendPreviewViewModel
    {
        public Guid TrendId { get; set; }
        public TrendPreviewViewModel TrendPreviewModel { get; set; }
        public TrendViewModel TrendModel { get; set; }
        public List<TrendValuePreviewViewModel> TrendValueViewModel { get; set; }
    }
}
