﻿using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.CompanyProfile
{
    public class CompanySegmentInformationService : ICompanySegmentInformationService
    {
        private readonly ICompanySegmentInformationRepository _companySegmentInformationRepository;
        private readonly ICompanyNoteRepository _iCompanyNoteRepository;

        public CompanySegmentInformationService(ICompanySegmentInformationRepository companySegmentInformationRepository, ICompanyNoteRepository CompanyNoteRepository)
        {
            _companySegmentInformationRepository = companySegmentInformationRepository;
            _iCompanyNoteRepository = CompanyNoteRepository;
        }

        public IEnumerable<CompanySegmentInformationViewModel> GetCompanySegmentInformationData(Guid CompanyId, AviraUser user)
        {
            var res = _companySegmentInformationRepository.GetCompanySegmentInformationData(CompanyId, user);
            return res;
        }
    }
}
