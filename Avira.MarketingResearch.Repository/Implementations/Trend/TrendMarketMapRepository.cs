﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.Trend
{
    public class TrendMarketMapRepository : ITrendMarketMapRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "TrendMarketMapping";

        public TrendMarketMapRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }
        /// <summary>
        /// To get all market with mapped/unmapped status by trend id
        /// </summary>
        /// <param name="TrendIdint"></param>
        /// <param name="PageStart"></param>
        /// <param name="pageSize"></param>
        /// <param name="SortDirection"></param>
        /// <param name="OrdbyByColumnName"></param>
        /// <param name="Search"></param>
        /// <param name="isDeletedInclude"></param>
        /// <returns></returns>
        public IEnumerable<TrendMarketMapViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search="", bool isDeletedInclude = false, string id = "")
        {
            
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<TrendMarketMapViewModel>(TrendMarketMapQueries.StoreProcSelectAll
                            , new {@MarketName = Search, @TrendID = id, @includeDeleted = false, @OrdbyByColumnName = @OrdbyByColumnName, @PageStart = PageStart, @pageSize = pageSize, @SortDirection = SortDirection }
                            , null, true, null, CommandType.StoredProcedure);
        }
      /// <summary>
      /// 
      /// </summary>
      /// <param name="trendMarketMapInsertDbViewModel"></param>
      /// <returns></returns>
        public Result<SpTransactionMessage> InsertUpdate(MainTrendMarketMapInsertUpdateDbViewModel trendMarketMapInsertDbViewModel)
        {

            DataTable trendMarketMapTable = new DataTable();
            trendMarketMapTable.Columns.Add("ID", typeof(Guid));
            trendMarketMapTable.Columns.Add("MapId", typeof(Guid));
            trendMarketMapTable.Columns.Add("bitValue", typeof(bool));
            if (trendMarketMapInsertDbViewModel.TrendMarketMapInsertUpdate != null &trendMarketMapInsertDbViewModel.TrendMarketMapInsertUpdate.Count > 0)
            {
                foreach (var item in trendMarketMapInsertDbViewModel.TrendMarketMapInsertUpdate)//.Where(x => x.ImpactId != Guid.Empty)
                {                    
                    trendMarketMapTable.Rows.Add(new Object[] { item.MarketId, item.ImpactId, item.EndUser });
                }
            }


            var spTransactionMessage = _dbHelper.ExecuteQuery(TrendMarketMapQueries.StoreProcInsertUpdate,
               new
               {
                   @TrendId = trendMarketMapInsertDbViewModel.TrendId,
                   @TrendMarketmap = trendMarketMapTable,
                   @AviraUserId = trendMarketMapInsertDbViewModel.AviraUserId
               }
                    , CommandType.StoredProcedure);
            return spTransactionMessage;
        }
       
    }
}
