﻿CREATE TABLE [dbo].[StagingQualitativeSegmentMapping] (
    [Id]             UNIQUEIDENTIFIER NOT NULL,
    [ImportId]       UNIQUEIDENTIFIER NULL,
    [RowNo]          INT              NULL,
    [MarketName]     NVARCHAR (255)   NULL,
    [SegmentName]    NVARCHAR (255)   NULL,
    [SubSegmentName] NVARCHAR (255)   NULL,
    [MarketId]       UNIQUEIDENTIFIER NULL,
    [SegmentId]      UNIQUEIDENTIFIER NULL,
    [SubSegmentId]   UNIQUEIDENTIFIER NULL,
    [ProjectId]      UNIQUEIDENTIFIER NULL,
    [ErrorNotes]     NVARCHAR (1000)  NULL,
    CONSTRAINT [PK_StagingQualitativeSegmentMapping] PRIMARY KEY CLUSTERED ([Id] ASC)
);



