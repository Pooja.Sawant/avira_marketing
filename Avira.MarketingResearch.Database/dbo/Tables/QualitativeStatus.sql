﻿CREATE TABLE [dbo].[QualitativeStatus] (
    [Id]                    UNIQUEIDENTIFIER NOT NULL,
    [QualitativeStatusName] NVARCHAR (256)   NOT NULL,
    [Description]           NVARCHAR (256)   NULL,
    [CreatedOn]             DATETIME2 (7)    NULL,
    [UserCreatedById]       UNIQUEIDENTIFIER NULL,
    [ModifiedOn]            DATETIME2 (7)    NULL,
    [UserModifiedById]      UNIQUEIDENTIFIER NULL,
    [IsDeleted]             BIT              CONSTRAINT [DF_QualitativeStatus_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]             DATETIME2 (7)    NULL,
    [UserDeletedById]       UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_QualitativeStatus] PRIMARY KEY CLUSTERED ([Id] ASC)
);

