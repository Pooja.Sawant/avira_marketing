﻿CREATE TABLE [dbo].[MessageStatus] (
    [Id]          UNIQUEIDENTIFIER NOT NULL,
    [StatusName]  NVARCHAR (50)    NOT NULL,
    [Description] NVARCHAR (200)   NULL,
    CONSTRAINT [PK_MessageStatus] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_MessageStatus_MessageStatus] FOREIGN KEY ([Id]) REFERENCES [dbo].[MessageStatus] ([Id])
);



