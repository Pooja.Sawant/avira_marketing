﻿


CREATE procedure [dbo].[SFab_GetQualitativeMarketsbyID]
(@ProjectID uniqueidentifier=null,
@includeDeleted bit = 0,
@AviraUserID uniqueidentifier = null)
as
/*================================================================================
Procedure Name: SFab_GetQualitativeDescriptionDatabyID
Author: Harshal 
Create date: 09/04/2019
Description: Get list from QualitativeDescription table by Filters
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
09/04/2019	Harshal			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if @includeDeleted is null set @includeDeleted = 0

SELECT distinct
       [MarketName]
      ,[QualitativeDescription].[MarketId]
	  
FROM [dbo].[QualitativeDescription]
INNER JOIN Market Market ON Market.Id=QualitativeDescription.MarketId
where (@ProjectId is null or QualitativeDescription.ProjectId = @ProjectID)
and (@includeDeleted = 1 or QualitativeDescription.IsDeleted = 0)

end