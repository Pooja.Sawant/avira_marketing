﻿CREATE TABLE [dbo].[ModuleType] (
    [Id]                 UNIQUEIDENTIFIER NOT NULL,
    [ModuleName]         NVARCHAR (100)   NOT NULL,
    [DisplayName]        NVARCHAR (100)   NULL,
    [ParentModuleTypeId] UNIQUEIDENTIFIER NULL,
    [CreatedOn]          DATETIME2 (7)    NULL,
    [UserDeletedById]    UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_ModuleType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

