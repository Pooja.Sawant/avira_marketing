﻿using Avira.MarketingResearch.Model.CustomValidation;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.CompanyProfile
{
    public class CompanyFundamentalViewModel
    {
        public Guid Id { get; set; }
        public string NatureTypeName { get; set; }
        public string RowType
        {
            get
            {
                return "CompanyFundamental";
            }
        }
    }

    public class CompanyRegionViewModel
    {
        public Guid RegionId { get; set; }
        public string RegionName { get; set; }
        public int RegionLevel { get; set; }
        public string RowType
        {
            get
            {
                return "CompanyRegion";
            }
        }
    }

    public class CompanyEntityViewModel
    {
        public Guid Id { get; set; }
        public string EntityTypeName { get; set; }
        public string RowType
        {
            get
            {
                return "CompanyEntity";
            }
        }
    }

    public class CompanyManagerViewModel
    {
        public Guid? ManagerId { get; set; }
        public string ManagerName { get; set; }
        public string RowType
        {
            get
            {
                return "CompanyEntity";
            }
        }
    }

    public class CompanyNatureViewModel
    {
        public Guid? NatureId { get; set; }
        public string NatureType { get; set; }
        public string RowType
        {
            get
            {
                return "CompanyNature";
            }
        }
    }

    public class CompanySubsidaryViewModel
    {
        public Guid Id { get; set; }
        public string CompanyName { get; set; }
        public string CompanySubsidaryName { get; set; }
        public string CompanySubsidaryLocation { get; set; }
        public bool? IsHeadQuarters { get; set; }
        public string RowType
        {
            get
            {
                return "CompanySubsidary";
            }
        }
    }

    public class CompanyDetailsViewModel
    {
        public Guid CompanyId { get; set; }
        public Guid ProjectId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyCode { get; set; }
        public string DisplayName { get; set; }
        public string LogoURL { get; set; }
        public Guid NatureId { get; set; }
        public string ParentCompanyName { get; set; }
        public string CompanyLocation { get; set; }
        public bool? IsHeadQuarters { get; set; }
        public string Telephone { get; set; }
        public string Telephone2 { get; set; }
        public string Telephone3 { get; set; }
        public string Email { get; set; }
        public string Email2 { get; set; }
        public string Email3 { get; set; }
        public string Website { get; set; }
        public int YearOfEstb { get; set; }
        public Guid CompanyStageId { get; set; }
        public string RegionId { get; set; }
        public int NoOfEmployees { get; set; }
        public Guid Id { get; set; }
        public string KeyEmployeeName { get; set; }
        public string KeyEmployeeDesignation { get; set; }
        public string KeyEmployeeEmailId { get; set; }
        public string KeyEmployeeComments { get; set; }
        public Guid BoardOfDirectorsId { get; set; }
        public string CompanyBoardOfDirecorsName { get; set; }
        public string CompanyBoardOfDirecorsOtherCompanyName { get; set; }
        public string CompanyBoardOfDirecorsOtherCompanyDesignation { get; set; }
        public Guid DesignationId { get; set; }
        public bool IsDirector { get; set; }
        public Guid? ManagerId { get; set; }
        public int RankNumber { get; set; }
        public string RankRationale { get; set; }
        public string ShareHoldingName { get; set; }
        public Guid EntityTypeId { get; set; }
        public decimal ShareHoldingPercentage { get; set; }

        public string RowType
        {
            get
            {
                return "CompanyDetails";
            }
        }
    }

    public class AllCompaniesListViewModel
    {
        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
        public Guid? ParentId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyLocation { get; set; }
        public bool? IsHeadQuarters { get; set; }
        public string RowType
        {
            get
            {
                return "AllCompaniesList";
            }
        }
    }

    public class AllCompanyStageListViewModel
    {
        public Guid Id { get; set; }
        public string CompanyStageName { get; set; }
        public string RowType
        {
            get
            {
                return "AllCompanyStageList";
            }
        }
    }

    public class CompanyDetailsInsertUpdateViewModel
    {
        public Guid CompanyId { get; set; }
        public Guid ProjectId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyCode { get; set; }
        public string DisplayName { get; set; }
        public string LogoURL { get; set; }
        public Guid NatureId { get; set; }
        public List<CompanySubsidaryDetails> SubsidaryCompanyDetails { get; set; }
        public string ParentCompanyName { get; set; }
        public Guid? ParentId { get; set; }
        public string CompanyLocation { get; set; }
        public bool? IsHeadQuarters { get; set; }
        public string Telephone { get; set; }
        public string Telephone2 { get; set; }
        public string Telephone3 { get; set; }
        public string Email { get; set; }
        public string Email2 { get; set; }
        public string Email3 { get; set; }
        public string Website { get; set; }
        public int YearOfEstb { get; set; }
        public string CompanyStageId { get; set; }
        public Guid[] RegionIdArrays { get; set; }
        public List<Guid> RegionId { get; set; }
        public List<RegionsDbViewModel> RegionsDbModelList { get; set; }
        public CompanyRegionViewModel RegoinModelList { get; set; }
        public int NoOfEmployees { get; set; }
        public List<KeyEmployees> KeyEmployee { get; set; }
        public List<BoardOfDirectors> BoardOfDirector { get; set; }
        public int RankNumber { get; set; }
        public string RankRationale { get; set; }
        public List<ShareHolding> ShareHoldingPattern { get; set; }
        public string ImageActualName { get; set; }
        public string ImageDisplayName { get; set; }
        public string ImageURL { get; set; }
        public string fileExtension { get; set; }
        public byte[] fileByte { get; set; }
        [ValidateImageAttribute]
        public IFormFile File { get; set; }
        public string RowType
        {
            get
            {
                return "CompanyDetails";
            }
        }
        public bool IsNewCompany { get; set; }
    }

    public class CompanyDetailsListViewModel
    {
        public Guid CompanyId { get; set; }
        public Guid ParentId { get; set; }
        public Guid ProjectId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyCode { get; set; }
        public string DisplayName { get; set; }
        public string LogoURL { get; set; }
        public string ImageActualName { get; set; }
        public string ImageDisplayName { get; set; }
        public Guid NatureId { get; set; }
        public string ParentCompanyName { get; set; }
        public string CompanyLocation { get; set; }
        public bool? IsHeadQuarters { get; set; }
        public string Telephone { get; set; }
        public string Telephone2 { get; set; }
        public string Telephone3 { get; set; }
        public string Email { get; set; }
        public string Email2 { get; set; }
        public string Email3 { get; set; }
        public string Website { get; set; }
        public int YearOfEstb { get; set; }
        public Guid CompanyStageId { get; set; }
        public int NoOfEmployees { get; set; }
        public Guid Id { get; set; }
        public int RankNumber { get; set; }
        public string RankRationale { get; set; }

        public string RowType
        {
            get
            {
                return "CompanyDetails";
            }
        }
    }

    public class CompanySubsidaryDetails
    {
        public Guid Id { get; set; }

        public Guid CompanySubsidaryId { get; set; }

        public string SubsidaryCompanyName { get; set; }

        public string SubsidaryCompanyLocation { get; set; }

        public bool? IsHeadQuarters { get; set; }
    }

    public class RegionsDbViewModel
    {
        public Guid RegionId { get; set; }
    }

    public class RegionsViewModel
    {
        public List<Guid> RegionId { get; set; }
    }

    public class KeyEmployees
    {
        public Guid Id { get; set; }

        public string KeyEmployeeName { get; set; }

        public string ManagerName { get; set; }

        public string KeyEmployeeDesignation { get; set; }

        public string KeyEmployeeEmailId { get; set; }

        public string KeyEmployeeComments { get; set; }

        public Guid DesignationId { get; set; }

        public bool IsDirector { get; set; }

        public Guid? ManagerId { get; set; }

        public string CompanyBoardOfDirecorsOtherCompanyName { get; set; }

        public string CompanyBoardOfDirecorsOtherCompanyDesignation { get; set; }
        public string RowType
        {
            get
            {
                return "KeyEmployees";
            }
        }
    }

    public class BoardOfDirectors
    {
        public Guid Id { get; set; }

        public Guid BoardOfDirectorsId { get; set; }

        public string CompanyBoardOfDirecorsName { get; set; }

        public string KeyEmployeeName { get; set; }

        public string ManagerName { get; set; }

        public string KeyEmployeeDesignation { get; set; }

        public string KeyEmployeeEmailId { get; set; }

        public string CompanyBoardOfDirecorsOtherCompanyName { get; set; }

        public string CompanyBoardOfDirecorsOtherCompanyDesignation { get; set; }

        public Guid DesignationId { get; set; }

        public bool IsDirector { get; set; }

        public Guid? ManagerId { get; set; }

        public string KeyEmployeeComments { get; set; }
    }

    public class ShareHolding
    {
        public Guid Id { get; set; }

        public string ShareHoldingName { get; set; }

        public Guid EntityTypeId { get; set; }

        public decimal ShareHoldingPercentage { get; set; }
    }

    public class CompanyProfileDbViewModel
    {
        public List<CompanyDetailsListViewModel> compList { get; set; }

        public List<CompanySubsidaryDetails> subcompList { get; set; }

        public List<RegionsDbViewModel> regionlist { get; set; }

        public List<KeyEmployees> keyemplist { get; set; }

        public List<BoardOfDirectors> boardofdirlist { get; set; }

        public List<ShareHolding> shareholdinglist { get; set; }

    }

    public class DesignationViewModel
    {
        public Guid DesignationId { get; set; }

        public string Designation { get; set; }

        public string Description { get; set; }
    }
}
