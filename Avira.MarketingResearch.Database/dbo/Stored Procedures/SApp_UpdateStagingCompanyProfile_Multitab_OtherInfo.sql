﻿CREATE procedure [dbo].[SApp_UpdateStagingCompanyProfile_Multitab_OtherInfo]
        
as        
/*================================================================================        
Procedure Name: SApp_UpdateStagingCompanyProfile_Multitab       
Author: Sharath        
Create date: 08/22/2019        
Description: to update Company Profiles 
Change History        
Date  Developer  Reason        
__________ ____________ ______________________________________________________        
08/22/2019 Praveen   Initial Version        
03/12/2019 Nitin	 Company Profile ESM Segment and Subsegment mapping changes
================================================================================*/        
BEGIN        
SET NOCOUNT ON;        
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;        
declare @ErrorMsg NVARCHAR(2048) 
declare @EndUserSegmentId uniqueidentifier;
select @EndUserSegmentId = Id from Segment
where SegmentName = 'End User';         
        
BEGIN TRY 
declare @CompanyId uniqueidentifier

select @CompanyId = company.Id from company
INNER JOIN StagingCompanyProfileFundamentals ON company.CompanyName = StagingCompanyProfileFundamentals.CompanyName

--Fundamental verification
update staging
set ErrorNotes = isnull(ErrorNotes,'') + case when not exists (select 1 from company where company.CompanyName = staging.CompanyName) then 'Company does not exists, ' else '' end
+ case when LEN(staging.Description)>0 then 
               case when LEN(staging.Description)>512 then 'Maximum 512 Characters are allowed for Description, 'else '' end
  else '' end
--+ case when LEN(staging.Headquarter)>0 then 
--               case when not exists(select 1 from Country where Country.CountryName = staging.Headquarter) then 'Invalid Head-Quarter Country Name, 'else '' end
--else '' end
--+ case when LEN(staging.Nature) = 0 OR ISNULL(staging.Nature,'') = '' then 'Nature is mandatory, ' else '' end
--+ case when not exists (select 1 from Nature where Nature.NatureTypeName = staging.Nature) then 'Invalid Nature, ' else '' end
+ case when LEN(staging.CompanyStage) = 0 OR ISNULL(staging.CompanyStage,'') = '' then 'CompanyStage is mandatory, ' else '' end
+ case when not exists (select 1 from CompanyStage where CompanyStage.CompanyStageName = staging.CompanyStage) then 'Invalid Company Stage, ' else '' end
+ case when LEN(staging.Website)>0 then 
               case when LEN(staging.Website)>256 then 'Maximum 256 Characters are allowed for Website, 'else '' end
  else '' end  
+ case when LEN(staging.CompanyRank_Rationale)>0 then 
               case when LEN(staging.CompanyRank_Rationale)>512 then 'Maximum 512 Characters are allowed for CompanyRank Rationale, 'else '' end
  else '' end  
--+ case when isnumeric(Foundedyear) = 0 then 'Invalid Founded year, ' else '' end
--+ case when isnumeric(NumberOfEmployee) = 0 then 'Invalid NumberOfEmployee, ' else '' end
--+ case when isdate(AsonDate) = 0 then 'Invalid AsonDate, ' else '' end
+ case when LEN(staging.Currency1)>0 then 
               case when not exists(select 1 from Currency where Currency.CurrencyCode = staging.Currency1) then 'Invalid Currency, 'else '' end
else '' end
+ case when LEN(staging.PhoneNumber1)>0 then 
               case when LEN(staging.PhoneNumber1)>15 then 'Maximum 15 Characters are allowed for PhoneNumber1, 'else '' end
  else '' end
+ case when LEN(staging.PhoneNumber2)>0 then 
               case when LEN(staging.PhoneNumber2)>15 then 'Maximum 15 Characters are allowed for PhoneNumber2, 'else '' end
  else '' end
+ case when LEN(staging.PhoneNumber3)>0 then 
               case when LEN(staging.PhoneNumber3)>15 then 'Maximum 15 Characters are allowed for PhoneNumber3, 'else '' end
  else '' end
--+ case when exists (select 1 from dbo.FUN_STRING_TOKENIZER(GeographicPresence,',') geo
--					left join Country 
--					on geo.value = Country.CountryName
--					left join Region
--					on geo.value = Region.RegionName 
--					where Country.ID is null and Region.ID is null) then 'Invalid GeographicPresence, ' else '' end
,CurrencyId = (select top 1 id from Currency where Currency.Currencycode = staging.Currency1)
,NatureId = (select top 1 id from Nature where Nature.NatureTypeName = staging.Nature)
,CompanyStageId = (select top 1 id from CompanyStage where CompanyStage.CompanyStageName = staging.CompanyStage),
CompanyId = @CompanyId
from [StagingCompanyProfileFundamentals] staging

Declare @NatureIdIsPublic bit = 0
Select @NatureIdIsPublic = (Select 1
From Nature N Inner Join [StagingCompanyProfileFundamentals] SCPF On SCPF.NatureId = N.Id
Where N.NatureTypeName = 'Public') -- Public Nature

--Share Holding Pattern verification
update staging
set ErrorNotes = isnull(ErrorNotes,'') --+ case when not exists (select 1 from EntityType where EntityType.EntityTypeName = staging.Entity) then 'Invalid EntityTypeName, ' else '' end
+ case when LEN(staging.Name)>0 then 
               case when LEN(staging.Name)>256 then 'Maximum 256 Characters are allowed for Name, 'else '' end
  else '' end
+ case when LEN(staging.Entity)>0 then 
               case when not exists(select 1 from EntityType where EntityType.EntityTypeName = staging.Entity) then 'Invalid Entity Type, 'else '' end
else '' end
,EntityTypeId = (select top 1 id from EntityType where EntityType.EntityTypeName = staging.Entity)
, CompanyId = @CompanyId
from [StagingCompanyProfileShareHolding] staging

----Subsidiaries Verification
update staging
set 
ErrorNotes = isnull(ErrorNotes,'') + case when exists (select 1 from company 
														   where company.CompanyName = staging.CompanySubsdiaryName And ParentId <> @CompanyId) 
										then 'CompanySubsdiaryName Already Exists for some different company, ' else '' end
+ case when LEN(staging.CompanySubsdiaryLocation)>0 then 
              case when not exists(select 1 from Country where Country.CountryName = staging.CompanySubsdiaryLocation) then 'Invalid Company Subsdiary Location Country Name, 'else '' end
else '' end	
+ case when LEN(staging.CompanySubsdiaryLocation)>0 then 
              case when LEN(staging.CompanySubsdiaryLocation)>256 then 'Maximum 256 Characters are allowed for CompanySubsdiaryLocation, 'else '' end
else '' end	
+ Case When Exists(Select 1 From StagingCompanyProfileSubsidiaries Group By StagingCompanyProfileSubsidiaries.CompanySubsdiaryName Having Count(*) > 1) Then 'Duplicate Company Subsdiary found,' else '' end,
CompanyId = @CompanyId
from [StagingCompanyProfileSubsidiaries] staging

--KeyEmployees Verification
update staging
set
ErrorNotes = isnull(ErrorNotes,'') + 
case when LEN(staging.KeyEmployeeName)>0 then 
              case when LEN(staging.KeyEmployeeName)>256 then 'Maximum 256 Characters are allowed for KeyEmployeeName, 'else '' end
else '' end	
+case when LEN(staging.KeyEmployeeDesignation)>0 then 
              case when LEN(staging.KeyEmployeeDesignation)>256 then 'Maximum 256 Characters are allowed for KeyEmployeeDesignation, 'else '' end
else '' end	
+case when LEN(staging.KeyEmployeeEmail)>0 then 
              case when LEN(staging.KeyEmployeeEmail)>254 then 'Maximum 254 Characters are allowed for KeyEmployeeEmail, 'else '' end
else '' end	
+case when LEN(staging.ReportingManager)>0 then 
              case when LEN(staging.ReportingManager)>256 then 'Maximum 256 Characters are allowed for ReportingManager, 'else '' end
else '' end	
, CompanyId = @CompanyId
from [StagingCompanyKeyEmployees] staging;

--BoD Verification
update staging
set ErrorNotes = isnull(ErrorNotes,'') + 
case when LEN(staging.BoardofDirectorName)>0 then 
              case when LEN(staging.BoardofDirectorName)>256 then 'Maximum 256 Characters are allowed for BoardofDirectorName, 'else '' end
else '' end	
+case when LEN(staging.BoardofDirectorOtherCompanyDesignation)>0 then 
              case when LEN(staging.BoardofDirectorOtherCompanyDesignation)>256 then 'Maximum 256 Characters are allowed for BoardofDirectorOtherCompanyDesignation, 'else '' end
else '' end	
+case when LEN(staging.BoardofDirectorEmail)>0 then 
              case when LEN(staging.BoardofDirectorEmail)>254 then 'Maximum 254 Characters are allowed for BoardofDirectorEmail, 'else '' end
else '' end	
+case when LEN(staging.BoardofDirectorOtherCompany)>0 then 
              case when LEN(staging.BoardofDirectorOtherCompany)>256 then 'Maximum 256 Characters are allowed for BoardofDirectorOtherCompany, 'else '' end
else '' end	
,CompanyId = @CompanyId
from [StagingCompanyBoardOfDirectors] staging;

--Product Verification
update staging
set ErrorNotes = isnull(ErrorNotes,'') + case when exists (select 1 from dbo.FUN_STRING_TOKENIZER(staging.TargetedGeogrpahy,',') geo
					left join Country 
					on geo.value = Country.CountryName
					left join Region
					on geo.value = Region.RegionName 
					where (Country.ID is null and Region.ID is null)) then 'Invalid GeographicPresence, ' else '' end
+ case when IsNull(staging.Status, '') = '' Then 'Status is mandatory, ' Else '' End
+ case when IsNull(staging.ProductServiceName, '') = '' Then 'Product/Service Name is mandatory, ' Else '' End
+ case when LEN(staging.ProductServiceName)>256 then 'Maximum 256 Characters are allowed for Product Service Name, 'else '' end
+case when LEN(staging.Patents)>0 then 
              case when LEN(staging.Patents)>512 then 'Maximum 512 Characters are allowed for Patents, 'else '' end
else '' end	
+case when LEN(staging.DecriptionRemarks)>0 then 
              case when LEN(staging.DecriptionRemarks)>512 then 'Maximum 512 Characters are allowed for DecriptionRemarks, 'else '' end
else '' end
+ case when not exists (select 1 from ProductStatus where ProductStatus.ProductStatusName = staging.Status) then 'Invalid Product Status Name, ' else '' end
+ case when not exists (select 1 from Currency where Currency.Currencycode = staging.Currency) and staging.Revenue !=0 then 'Invalid Currency, ' else '' end
+ case when not exists (select 1 from Segment where Segment.SegmentName = staging.Segment) then 'Invalid Segment, ' else '' end
+ case when not exists (select 1 from Valueconversion where Valueconversion.ValueName = staging.Units) and staging.Revenue !=0 then 'Invalid Units, ' else '' end
+ case when not exists (select 1 from ProductCategory where ProductCategory.ProductCategoryName = staging.ProductCategory) then 'Invalid ProductCategory, ' else '' end
+ case when not exists (select 1 from Project where Project.ProjectName = staging.Market) and staging.Market!='' then 'Invalid Market/Project Name, ' else '' end
, CurrencyId = (select top 1 id from Currency where Currency.Currencycode = staging.Currency)
, SegmentId = (select top 1 id from Segment where Segment.SegmentName = staging.Segment)
, ValueConversionId = (select top 1 id from ValueConversion where ValueConversion.ValueName = staging.Units)
, ProductCategoryId = (select top 1 id from ProductCategory where ProductCategory.ProductCategoryName = staging.ProductCategory)
, ProjectId = (select top 1 id from Project where Project.ProjectName = staging.Market) 
, CompanyId = @CompanyId
from [StagingCompanyProduct] staging;

--Company Strategy Verification
update staging
set ErrorNotes = isnull(ErrorNotes,'') + 
+ case when staging.Date Is Null Then 'Date is mandatory in Client & Strategies, ' Else '' End
+ case when IsNull(staging.OrganicInorganic, '') = '' Then 'OrganicInorganic is mandatory in Client & Strategies, ' Else '' End
+ case when IsNull(staging.Strategy, '') = '' Then 'Strategy is mandatory in Client & Strategies, ' Else '' End
+ case when IsNull(staging.Category, '') = '' Then 'Category is mandatory in Client & Strategies, ' Else '' End
+ case when not exists (select 1 from Approach where Approach.ApproachName = staging.OrganicInorganic) then 'Invalid Approach Name, ' else '' end
+ case when not exists (select 1 from Category where Category.CategoryName = staging.Category) then 'Invalid Category Name, ' else '' end
+ case when not exists (select 1 from Importance where Importance.ImportanceName = staging.Importance Or IsNull(staging.Importance, '') = ''  )  then 'Invalid Importance Name, ' else '' end
+ case when not exists (select 1 from ImpactDirection where ImpactDirection.ImpactDirectionName = staging.Impact) and isnull(staging.Impact,'') != '' then 'Invalid Impact, ' else '' end
+case when LEN(staging.Remarks)>0 then 
              case when LEN(staging.Remarks)>512 then 'Maximum 512 Characters are allowed for Remark, 'else '' end
else '' end
, ApproachId = (select top 1 id from Approach where Approach.ApproachName = staging.OrganicInorganic) 
, CategoryId = (select top 1 id from Category where Category.CategoryName = staging.Category)
, ImportanceId = (select top 1 id from Importance where Importance.ImportanceName = staging.Importance) 
, ImpactId = (select top 1 id from ImpactDirection where ImpactDirection.ImpactDirectionName = staging.Impact)
, CompanyId = @CompanyId
from StagingCompanyStrategy staging;

--Company Clients Verification
update staging
set ErrorNotes = isnull(ErrorNotes,'')
	+ case when IsNull(staging.ClientName, '') = '' Then 'ClientName is mandatory in Client & Strategies, ' Else '' End
	+ case when LEN(staging.ClientName)>256 then 'Maximum 256 Characters are allowed for ClientName, 'else '' end
	+ Case When IsNull(Staging.ClientName, '') <> '' And 
										   Exists(Select 1 
													From dbo.FUN_STRING_TOKENIZER(Staging.ClientName,',') CN
													Where IsNull(CN.Value, '') <> ''
													Group By CN.Value
													Having Count(*) > 1															
											) Then 'Duplicate ClientName found,' else '' end
   + Case When IsNull(Staging.ClientName, '') <> '' And 
										   Exists(Select 1 
													From dbo.FUN_STRING_TOKENIZER(Staging.ClientName,',') CN
													Where IsNull(CN.Value, '') = ''
											) Then 'Do not put space between comma,' else '' end
   --+ case when IsNull(Staging.ClientName, '') <> '' And Exists(Select 1 
			--										   From dbo.FUN_STRING_TOKENIZER(Staging.ClientName,',') CN
			--												Inner Join CompanyClientsMapping CCM On
			--													CN.Value = CCM.ClientName
			--												AND CCM.CompanyId = @CompanyId														
			--										   ) Then 'ClientName name is already mapped to Company,'
			--						Else '' End
	+ case when IsNull(staging.ClientIndustry, '') = '' Then 'ClientIndustry is mandatory in Client & Strategies, ' Else '' End	
	+ case when not exists (select 1 from SubSegment where SubSegmentName = staging.ClientIndustry AND SegmentId = @EndUserSegmentId) then 'Invalid ClientIndustry, ' else '' end
	+ case when LEN(staging.Remarks)>0 then 
              case when LEN(staging.Remarks)>512 then 'Maximum 512 Characters are allowed for Remarks, 'else '' end
else '' end
	,CompanyId = @CompanyId
from [StagingCompanyClients] staging;

--Company News Verification
update staging
set ErrorNotes = isnull(ErrorNotes,'') 
+ case when staging.NewsDate is Null Then 'NewsDate is mandatory, ' Else '' End
+ case when IsNull(staging.News,'') = '' Then 'News is mandatory, ' Else '' End
+ case when IsNull(staging.NewsCategory,'') = '' Then 'NewsCategory is mandatory, ' Else '' End
+ case when not exists (select 1 from NewsCategory where NewsCategory.NewsCategoryName = staging.NewsCategory) then 'Invalid News Category Name, ' else '' end
+ case when not exists (select 1 from Importance where Importance.ImportanceName = staging.Importance Or IsNull(staging.Importance, '') = ''  ) then 'Invalid Importance Name, ' else '' end
+ case when not exists (select 1 from ImpactDirection where ImpactDirection.ImpactDirectionName = staging.Impact Or IsNull(staging.Impact, '') = ''  ) then 'Invalid Impact, ' else '' end
+ case when LEN(staging.Remarks)>0 then 
              case when LEN(staging.Remarks)>128 then 'Maximum 128 Characters are allowed for Remarks, 'else '' end
else '' end
+ case when LEN(staging.OtherParticipants)>0 then 
              case when LEN(staging.OtherParticipants)>128 then 'Maximum 128 Characters are allowed for OtherParticipants, 'else '' end
else '' end
, CategoryId = (select top 1 id from NewsCategory where NewsCategory.NewsCategoryName = staging.NewsCategory)
, ImportanceId = (select top 1 id from Importance where Importance.ImportanceName = staging.Importance) 
, ImpactId = (select top 1 id from ImpactDirection where ImpactDirection.ImpactDirectionName = staging.Impact)
, CompanyId = @CompanyId
from [StagingCompanyNews] staging;

--Company ESM Verification
update staging
set ErrorNotes = isnull(ErrorNotes,'') 
+ case when IsNull(staging.ESMPresence, '') = '' Then 'ESMPresence is mandatory, ' Else '' End
+ case when IsNull(staging.SectorPresence,'') = '' Then 'SectorPresence is mandatory, ' Else '' End
+ case when IsNull(staging.KeyCompetitor,'') = '' Then 'KeyCompetitor is mandatory, ' Else '' End
+ case when LEN(staging.KeyCompetitor)>100 then 'Maximum 100 Characters are allowed for OtherParticipants, 'else '' end
+ Case When IsNull(Staging.KeyCompetitor, '') <> '' And 
										   Exists(Select 1 
													From dbo.FUN_STRING_TOKENIZER(Staging.KeyCompetitor,',') KC
													Where IsNull(KC.Value, '') <> ''
													Group By KC.Value
													Having Count(*) > 1															
											) Then 'Duplicate KeyCompetitor found,' else '' end
--+ case when IsNull(Staging.KeyCompetitor, '') <> '' And Exists(Select 1 
--													   From dbo.FUN_STRING_TOKENIZER(Staging.KeyCompetitor,',') KC
--															Inner Join CompanyEcosystemPresenceKeyCompetitersMap CEPK On
--																KC.Value = CEPK.KeyCompetiters
--															AND CEPK.CompanyId = @CompanyId														
--													   ) Then 'KeyCompetitor name is already mapped to Company,'
--									Else '' End
+ case when exists (select 1 from dbo.FUN_STRING_TOKENIZER(ESMPresence,',') ESM
					left join Segment
					on ESM.value = Segment.SegmentName
					where Segment.ID is null) then 'Invalid ESM Presence, ' else '' end
+ case when exists (select 1 from dbo.FUN_STRING_TOKENIZER(SectorPresence,',') SP
					left join 					
					SubSegment on SP.value = SubSegment.SubSegmentName and SubSegment.IsDeleted = 0
					INNER JOIN Segment ON Segment.Id = SubSegment.SegmentId	
					where Segment.SegmentName = 'End User' AND  SubSegment.ID is null) then 'Invalid Sector Presence, ' else '' end
, CompanyId = @CompanyId
from [StagingCompanyEcosystemPresence] staging;

--Company Profile ESM Segment and Subsegment mapping
Update Staging
Set ErrorNotes = IsNull(ErrorNotes, '') 
	  + Case When IsNull(Staging.SegmentName, '') = '' Then 'Segment name is mandatory,' Else '' End
	  + Case When Len(Staging.SegmentName) > 255 Then 'Segment name exceed max length(255),' Else '' End 
	  + Case When Exists (SELECT 1 FROM [StagingCompanyEcosystemSegmentMap] 
							Where IsNull(SegmentName, '') <> '' And SegmentName = Staging.SegmentName Group By ImportId, SegmentName Having Count(SegmentName) > 1) 
			Then 'Duplicate segment found,' Else '' End
	  + Case When IsNull(Staging.SegmentName, '') != '' And Not Exists(Select 1 From Segment Where Segment.SegmentName = Staging.SegmentName) Then 'Invalid Segment Name,' Else '' End
	  + Case When IsNull(Staging.SegmentName, '') != '' And Not Exists(Select 1 
							   From Segment 
									Inner Join QualitativeSegmentMapping QSM On
										Segment.SegmentName = Staging.SegmentName And
										Segment.Id = QSM.SegmentId) Then 'Segment name is not mapped to Qualitative Segments,' Else '' End
	  + Case When IsNull(Staging.SubsegmentName, '') <> '' And 
			Exists(Select 1 
					From dbo.FUN_STRING_TOKENIZER(Staging.SubsegmentName,',') SP 
									Left Join SubSegment SS On SP.Value = SS.SubSegmentName Where IsNull(SP.Value, '') <> '' And SS.SubSegmentName Is NUll 
					) Then 'Invalid subsegment name,' 
							--+ (Select SP.Value + ',' 
							--	From dbo.FUN_STRING_TOKENIZER(Staging.SubsegmentName,',') SP 
							--		Left Join SubSegment SS On SP.Value = SS.SubSegmentName Where SS.SubSegmentName Is NUll FOR XML PATH('')) + ',' 
						Else '' End
	   + Case When IsNull(Staging.SubsegmentName, '') <> '' And 
			Exists(Select 1 
					From dbo.FUN_STRING_TOKENIZER(Staging.SubsegmentName,',') SP 
									Where IsNull(SP.Value, '') <> '' And Len(SP.Value) > 255
					) Then 'SubSegment name exceed max length(255),' 
							--+ (Select SP.Value + ',' 
							--	From dbo.FUN_STRING_TOKENIZER(Staging.SubsegmentName,',') SP 
							--		Left Join SubSegment SS On SP.Value = SS.SubSegmentName Where SS.SubSegmentName Is NUll FOR XML PATH('')) + ',' 
						Else '' End
	  --+ Case When IsNull(Staging.SubsegmentName, '') <> '' And (Select Count(1)
			--						   From dbo.FUN_STRING_TOKENIZER(Staging.SubsegmentName,',') SS 
			--						   Where IsNull(SS.Value, '') <> '' ) > 15 Then 'Maximum 15 subsegments can be tagged to every segment,' 
			--				Else '' End 
	  + Case When IsNull(Staging.SubsegmentName, '') <> '' And 
										   Exists(Select 1 
													From dbo.FUN_STRING_TOKENIZER(Staging.SubsegmentName,',') SP
													Where IsNull(SP.Value, '') <> ''
													Group By SP.Value
													Having Count(*) > 1															
											) Then 'Duplicate subsegment found,' 
													--(Select SP.Value 
													--From dbo.FUN_STRING_TOKENIZER(Staging.SubsegmentName,',') SP
													--Where IsNull(SP.Value, '') <> ''
													--Group By SP.Value
													--Having Count(*) > 1) + ','
								Else '' End	
	  + Case When IsNull(Staging.SegmentName, '') != '' And IsNull(Staging.SubsegmentName, '') <> '' And Not Exists(Select 1 
													   From dbo.FUN_STRING_TOKENIZER(Staging.SubsegmentName,',') SP 
															Inner Join SubSegment SS On
																SP.Value = SS.SubsegmentName
															Inner Join Segment S On 
																SS.SegmentId = S.Id And
																S.SegmentName = Staging.SegmentName														
													   ) Then 'Subsegment name is not mapped to segment,'
									Else '' End
	  + Case When IsNull(Staging.SegmentName, '') != '' And IsNull(Staging.SubsegmentName, '') <> '' And Not Exists(Select 1 
															   From dbo.FUN_STRING_TOKENIZER(Staging.SubsegmentName,',') SP 
																	Inner Join SubSegment SS On
																		SP.Value = SS.SubsegmentName
																	Inner Join Segment S On 
																		SS.SegmentId = S.Id And
																		S.SegmentName = Staging.SegmentName
																	Inner Join QualitativeSegmentMapping QSM On
																		QSM.SegmentId = S.Id And
																		QSM.SubSegmentId = SS.Id
															   ) Then 'Subsegment name is not mapped to Qualitative SubSegments,' 
										Else '' End 

	--+ Case When Len(Staging.SegmentName) > 255 Then 'Segment name exceed max length(255),' Else '' End
	--+ Case When Not Exists(Select 1 From Segment Where Segment.SegmentName = Staging.SegmentName) Then 'Invalid Segment Name.' Else '' End
	+ Case When Exists (SELECT 1 FROM [StagingCompanyEcosystemSegmentMap] 
						Where IsNull(SegmentName, '') <> '' Group By ImportId Having Count(SegmentName) > 10) 
		Then 'Maximum 10 Segments allowed,' Else'' End
	, CompanyId = @CompanyId
From StagingCompanyEcosystemSegmentMap staging



--Company SWOT Verification
Update Staging
set ErrorNotes = isnull(ErrorNotes,'') 
+ case when IsNull(staging.AnalysisPoint, '') = '' Then 'AnalysisPoint is mandatory, ' Else '' End
+ case when LEN(staging.AnalysisPoint)>256 then 'Maximum 256 Characters are allowed for AnalysisPoint, 'else '' end
+ case when IsNull(staging.Market,'') = '' Then 'Market is mandatory, ' Else '' End
+ case when IsNull(staging.Category,'') = '' Then 'Category is mandatory, ' Else '' End
+ case when IsNull(staging.SWOTType,'') = '' Then 'SWOTType is mandatory, ' Else '' End
+ case when not exists (select 1 from Project where Project.ProjectName = staging.Market) then 'Invalid Market/Project Name, ' else '' end
+ case when not exists (select 1 from SWOTType where SWOTType.SWOTTypeName = staging.SWOTType) then 'Invalid SWOT Type Name, ' else '' end
+ case when not exists (select 1 from Category where Category.CategoryName = staging.Category) then 'Invalid Category Name, ' else '' end
, SWOTTypeId = (select top 1 id from [SWOTType] where [SWOTType].SWOTTypeName = staging.SWOTType)
, CategoryId = (select top 1 id from Category where Category.CategoryName = staging.Category)
, ProjectId = (select top 1 id from Project where Project.ProjectName = staging.Market) 
, CompanyId = @CompanyId
from [StagingCompanySWOT] staging;

--Company Trends Verification
update staging
set ErrorNotes = isnull(ErrorNotes,'') + 
+ case when LEN(staging.Trends)>0 then 
              case when LEN(staging.Trends)>900 then 'Maximum 900 Characters are allowed for Trends, 'else '' end
else '' end
+ case when LEN(staging.Department)>0 then 
              case when LEN(staging.Department)>256 then 'Maximum 256 Characters are allowed for Department, 'else '' end
else '' end
+ case when not exists (select 1 from Category where Category.CategoryName = staging.Category Or IsNull(staging.Category,'') = '') then 'Invalid Category Name, ' else '' end
+ case when not exists (select 1 from Importance where Importance.ImportanceName = staging.Importance) and staging.Importance!='' then 'Invalid Importance Name, ' else '' end
+ case when not exists (select 1 from ImpactDirection where ImpactDirection.ImpactDirectionName = staging.Impact) and staging.Impact!='' then 'Invalid Impact, ' else '' end
+ case when not exists (select 1 from Project where Project.ProjectName = staging.Market) and staging.Market!='' then 'Invalid Market/Project Name, ' else '' end
, ImportanceId = (select top 1 id from Importance where Importance.ImportanceName = staging.Importance) 
, ImpactId = (select top 1 id from ImpactDirection where ImpactDirection.ImpactDirectionName = staging.Impact)
, ProjectId = (select top 1 id from Project where Project.ProjectName = staging.Market) 
, CompanyId = @CompanyId
from [StagingCompanyTrends] staging;

--Company AnalystView Verification
update staging
set ErrorNotes = isnull(ErrorNotes,'') 
+ case when IsNull(staging.Market, '') = '' Then 'Market is mandatory, ' Else '' End
+ case when IsNull(staging.AnalystView,'') = '' Then 'AnalystView is mandatory, ' Else '' End
+ case when not exists (select 1 from Category where Category.CategoryName = staging.Category Or IsNull(staging.Category,'') = '') then 'Invalid Category Name, ' else '' end
+ case when not exists (select 1 from Importance where Importance.ImportanceName = staging.Importance Or IsNull(staging.Importance, '') = ''  )  then 'Invalid Importance Name, ' else '' end
+ case when not exists (select 1 from Project where Project.ProjectName = staging.Market) then 'Invalid Market/Project Name, ' else '' end
, ProjectId = (select top 1 id from Project where Project.ProjectName = staging.Market) 
, CategoryId = (select top 1 id from Category where Category.CategoryName = staging.Category)
, ImportanceId = (select top 1 id from Importance where Importance.ImportanceName = staging.Importance) 
, CompanyId = @CompanyId
from [StagingCompanyAnalystView] staging;

-- Company Price Volume
update staging
set ErrorNotes = isnull(ErrorNotes,'') 
+ case when staging.[Date] is null Then 'Invalid Date format/Date is missing' else '' end
+ case when (staging.Price is null and staging.[Date] is not null ) Then 'Price missing for imported date, ' else '' end
+ case when not exists (select 1 from Currency where Currency.Currencycode = staging.Currency) and staging.Price !=0 then 'Invalid Currency, ' else '' end
+ case when not exists (select 1 from Valueconversion where Valueconversion.ValueName = staging.CurrencyUnit) and staging.Price !=0 then 'Invalid Units, ' else '' end
+ case when not exists (select 1 from Valueconversion where Valueconversion.ValueName = staging.VolumeUnit) and staging.Volume !=0 then 'Invalid Units, ' else '' end
, CurrencyId = (select top 1 id from Currency where Currency.Currencycode = staging.Currency)
, CurrencyUnitId = (select top 1 id from ValueConversion where ValueConversion.ValueName = staging.CurrencyUnit)
, VolumeUnitId = (select top 1 id from ValueConversion where ValueConversion.ValueName = staging.VolumeUnit)
, CompanyId = @CompanyId
from [StagingCompanyPriceVolume] staging;

IF( EXISTS(SELECT 1 FROM StagingCompanyAnalystView WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingCompanyBoardOfDirectors WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingCompanyClients WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingCompanyEcosystemPresence WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingCompanyKeyEmployees WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingCompanyNews WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingCompanyProduct WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingCompanyProfileFundamentals WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingCompanyProfileShareHolding WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingCompanyProfileSubsidiaries WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingCompanyStrategy WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingCompanySWOT WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingCompanyTrends WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingCompanyPriceVolume WHERE ErrorNotes != '')
OR Exists(Select 1 From StagingCompanyEcosystemSegmentMap Where ErrorNotes != '') 

)    
BEGIN   


	Select 'Fundamentals' as Module, RowNo, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingCompanyProfile_Multitab' AS StoredProcedureName   FROM StagingCompanyProfileFundamentals WHERE ErrorNotes != '' UNION ALL
	Select 'Share Holding Pattern' as Module, RowNo, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingCompanyProfile_Multitab' AS StoredProcedureName   FROM StagingCompanyProfileShareHolding WHERE ErrorNotes != '' UNION ALL
	Select 'Subsidiaries' as Module, RowNo, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingCompanyProfile_Multitab' AS StoredProcedureName   FROM StagingCompanyProfileSubsidiaries WHERE ErrorNotes != '' UNION ALL
	Select 'Key Employees' as Module, RowNo, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingCompanyProfile_Multitab' AS StoredProcedureName   FROM StagingCompanyKeyEmployees WHERE ErrorNotes != '' UNION ALL
	Select 'Board Of Directors' as Module, RowNo, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingCompanyProfile_Multitab' AS StoredProcedureName   FROM StagingCompanyBoardOfDirectors WHERE ErrorNotes != '' UNION ALL
	Select 'Product' as Module, RowNo, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingCompanyProfile_Multitab' AS StoredProcedureName   FROM StagingCompanyProduct WHERE ErrorNotes != '' UNION ALL
	Select 'Client' as Module, RowNo, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingCompanyProfile_Multitab' AS StoredProcedureName   FROM StagingCompanyClients WHERE ErrorNotes != '' UNION ALL
	Select 'Strategies' as Module, RowNo, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingCompanyProfile_Multitab' AS StoredProcedureName   FROM StagingCompanyStrategy WHERE ErrorNotes != '' UNION ALL
	Select 'News' as Module, RowNo, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingCompanyProfile_Multitab' AS StoredProcedureName   FROM StagingCompanyNews WHERE ErrorNotes != '' UNION ALL
	Select 'SWOT' as Module, RowNo, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingCompanyProfile_Multitab' AS StoredProcedureName   FROM StagingCompanySWOT WHERE ErrorNotes != '' UNION ALL
	Select 'Ecosystem' as Module, RowNo, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingCompanyProfile_Multitab' AS StoredProcedureName   FROM StagingCompanyEcosystemPresence WHERE ErrorNotes != '' UNION ALL
	Select 'Trends' as Module, RowNo, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingCompanyProfile_Multitab' AS StoredProcedureName   FROM StagingCompanyTrends WHERE ErrorNotes != '' UNION ALL
	Select 'Analyst Views' as Module, RowNo, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingCompanyProfile_Multitab' AS StoredProcedureName   FROM StagingCompanyAnalystView WHERE ErrorNotes != '' UNION ALL
	Select 'Price Volume' as Module, RowNo, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingCompanyProfile_Multitab' AS StoredProcedureName   FROM StagingCompanyPriceVolume WHERE ErrorNotes != '' UNION ALL
	Select 'ESM Segment' as Module, RowNo, ErrorNotes , CAST(0 AS BIT) AS Success, 'SApp_UpdateStagingCompanyProfile_Multitab' AS StoredProcedureName   FROM StagingCompanyEcosystemSegmentMap WHERE ErrorNotes != '' Order by 1,2

	DELETE FROM ImportData where Id in (select ImportId from StagingCompanyProfileFundamentals sm);    
	DELETE FROM StagingCompanyProfileFundamentals;
	DELETE FROM StagingCompanyProfileShareHolding;
	DELETE FROM StagingCompanyProfileSubsidiaries;
	DELETE FROM StagingCompanyKeyEmployees;
	DELETE FROM StagingCompanyBoardOfDirectors;
	DELETE FROM StagingCompanyProduct;
	DELETE FROM StagingCompanyClients;
	DELETE FROM StagingCompanyStrategy;
	DELETE FROM StagingCompanyNews;
	DELETE FROM StagingCompanySWOT;
	DELETE FROM StagingCompanyEcosystemPresence;
	DELETE FROM StagingCompanyTrends;
	DELETE FROM StagingCompanyAnalystView;
	DELETE FROM StagingCompanyPriceVolume;
	Delete From [StagingCompanyEcosystemSegmentMap]
END  
ELSE  
Begin
	
	Exec [dbo].[SApp_insertAllCompanyProfileFromStagging_MultiTab_OtherInfo];
	
	DELETE FROM StagingCompanyProfileFundamentals;
	DELETE FROM StagingCompanyProfileShareHolding;
	DELETE FROM StagingCompanyProfileSubsidiaries;
	DELETE FROM StagingCompanyKeyEmployees;
	DELETE FROM StagingCompanyBoardOfDirectors;
	DELETE FROM StagingCompanyProduct;
	DELETE FROM StagingCompanyClients;
	DELETE FROM StagingCompanyStrategy;
	DELETE FROM StagingCompanyNews;
	DELETE FROM StagingCompanySWOT;
	DELETE FROM StagingCompanyEcosystemPresence;
	DELETE FROM StagingCompanyTrends;
	DELETE FROM StagingCompanyAnalystView;
	DELETE FROM StagingCompanyPriceVolume;
	Delete From [StagingCompanyEcosystemSegmentMap]

End 

END TRY              
BEGIN CATCH              
    --Execute the error retrieval routine.              
 DECLARE @ErrorNumber int;              
 DECLARE @ErrorSeverity int;              
 DECLARE @ErrorProcedure varchar(100);              
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);              
              
  SELECT @ErrorNumber = ERROR_NUMBER(),              
        @ErrorSeverity = ERROR_SEVERITY(),              
        @ErrorProcedure = ERROR_PROCEDURE(),              
        @ErrorLine = ERROR_LINE(),              
        @ErrorMessage = ERROR_MESSAGE()              
              
 insert into dbo.Errorlog(ErrorNumber,              
       ErrorSeverity,              
       ErrorProcedure,              
       ErrorLine,              
       ErrorMessage,              
       ErrorDate)              
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());              
            
 set @ErrorMsg = 'Error while updating SApp_UpdateStagingCompanyProfile_Multitab_OtherInfo, please contact Admin for more details.';            
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine            
,Message =@ErrorMsg,Success = 0;               
               
END CATCH              
        
        
end