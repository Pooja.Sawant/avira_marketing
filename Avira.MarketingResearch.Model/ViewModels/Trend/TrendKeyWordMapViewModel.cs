﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.Trend
{
    public class TrendKeyWordMapViewModel
    {
        public string TrendKeyWord { get; set; }
        public Guid Id { get; set; }
        public Guid TrendId { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public int TotalItems { get; set; }
        public string TrendName { get; set; }
        public string ApproverRemark { get; set; }
        public string AuthorRemark { get; set; }
        public Guid TrendStatusID { get; set; }
        public string TrendStatusName { get; set; }
        public List<TrendKeyWordMapInsertDbViewModel> InsertModel { get; set; }
        public TrendNoteViewModel TrendNoteModel { get; set; }
        public Guid TrendNoteId { get; set; }

        //
        public TrendViewModel TrendModel { get; set; }
        //

        public string RowType
        {
            get
            {
                return "Keywords";
            }
        }
    }

    public class TrendKeyWordMapInsertDbViewModel
    {
        [Required]
        public string TrendKeyWord { get; set; }
        public Guid Id { get; set; }
        [Required(ErrorMessage = "Trend Name is Required")]
        public Guid TrendId { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public List<TrendKeywordNames> TrendNames { get; set; }
    }

    public class TrendKeywordNames
    {
        public Guid Id { get; set; }
        [Required]
        public string TrendName { get; set; }
        public bool? IsDeleted { get; set; }
    }

    public class TrendKeyWordMapUpdateDbViewModel
    {
        [Required]
        public string TrendKeyWord { get; set; }
        public Guid Id { get; set; }
        public Guid TrendId { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
    }

    public class TrendKeyWordMapInsertDbViewModel1
    {
        //[Required]
        public List<TrendKeywordNames> TrendKeyWord { get; set; }
        public List<Guid> Id { get; set; }
        [Required(ErrorMessage = "Trend Name is Required")]
        public Guid TrendId { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public List<TrendKeywordNames> TrendNames { get; set; }
        public List<string> TrendN { get; set; }
        public Guid TrendStatusID { get; set; }
        public string TrendStatusName { get; set; }
        public string ApproverRemark { get; set; }
        public string AuthorRemark { get; set; }
        public TrendNoteViewModel TrendNoteModel { get; set; }
    }
}
