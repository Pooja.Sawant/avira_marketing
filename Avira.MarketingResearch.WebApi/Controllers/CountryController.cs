﻿using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Utility;
using Avira.MarketingResearch.WebApi;
using CacheManager.Core;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountryController : AviraBaseController
    {
        public readonly ICountryRepository _countryRepository;
        public readonly ICountryService _countryService;
        private ILoggerManager _logger;
        public CountryController(
            ICountryService countryService,
            ICountryRepository countryRepository,
            ICacheManager<IEnumerable<CountryViewModel>> cache, ILoggerManager logger)
        {
            _countryRepository = countryRepository;
            _countryService = countryService;
            _logger = logger;
        }
        //public IActionResult Index()
        //{
        //    return View();
        //}

        [HttpGet]
        public ActionResult<IEnumerable<CountryViewModel>> Get(bool includeDeleted = false)
        {
            List<CountryViewModel> res = null;
            try
            {
                 res = _countryService.GetAll(includeDeleted).ToList();
        }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet("{id}")]
        public ActionResult<CountryViewModel> Get(Guid id)
        {
            CountryViewModel res = null;
            try
            {
                res = _countryService.GetById(id);
        }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }
    }
}