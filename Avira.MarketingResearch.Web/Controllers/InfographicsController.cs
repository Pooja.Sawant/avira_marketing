﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;

namespace Avira.MarketingResearch.Web.Controllers
{
    [NoCache]
    [Authorization]
    public class InfographicsController : BaseController
    {    
        public readonly IServiceRepository _serviceRepository;
        private ILoggerManager _logger;
        private readonly IConfiguration _configuration;

        public InfographicsController(IServiceRepository serviceRepository, ILoggerManager logger, IConfiguration configuration)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
            _configuration = configuration;
        }

        [HttpGet]
        public IActionResult Index(Guid ProjectId, Guid CategoryId)
        {
            InitPage(ProjectId, CategoryId);
            ViewBag.ProjectId = ProjectId;
            ViewBag.CategoryId = CategoryId;
            return View();
        }

        public ActionResult LoadData(InfographicsViewModel infographic)
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            List<InfographicsViewModel> data = null;
            int totalRecords = 0;
            try
            {
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                string search = Request.Form["search[value]"];

                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDir = Request.Form["order[0][dir]"].FirstOrDefault();
                var SortDirection = -1;
                if (sortColumnDir == "asc")
                {
                    SortDirection = 1;
                }
                else
                {
                    SortDirection = -1;
                }

                int skip = start != null ? Convert.ToInt32(start) : 0;
                int pageSize = length.ToString() != null ? Convert.ToInt32(length) : 0;
                int PageStart = start.ToString() != null ? Convert.ToInt32(start) : 0;
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();

                HttpResponseMessage response = _serviceRepository.GetResponse(contollerName, PageStart, pageSize, SortDirection, sortColumn, search, infographic.ProjectId??Guid.Empty, infographic.LookupCategoryId ?? Guid.Empty);
                response.EnsureSuccessStatusCode();
                List<InfographicsViewModel> markets = response.Content.ReadAsAsync<List<InfographicsViewModel>>().Result;
                ViewBag.Title = "All Infographics";
                data = markets;

                foreach (var item in markets)
                {
                    totalRecords = item.TotalItems;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data });

        }

        [HttpGet]
        public IActionResult CreateUpdate(Guid Id,Guid ProjectId,Guid? CategoryId)
        {
            InfographicsViewModel model = new InfographicsViewModel();
            if (Id != Guid.Empty)
            {
                HttpResponseMessage responseSave = _serviceRepository.GetResponseWithMethodName("Infographics", "GetURLById", Id.ToString());
                responseSave.EnsureSuccessStatusCode();
                model = responseSave.Content.ReadAsAsync<InfographicsViewModel>().Result;
                
            }
            InitPage(ProjectId, CategoryId);
            ViewBag.ProjectId = ProjectId;
            ViewBag.CategoryId = CategoryId;
            return View(model);
        }

        [HttpPost]
        public IActionResult CreateUpdate(InfographicsViewModel model)
        {
            var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();

            var loginId = HttpContext.Session.GetString("UserId");
            model.UserCreatedById =Guid.Parse(loginId);
            if (ModelState.IsValid)
            {
                if (model.ImageFile != null && model.ImageFile.Length > 0)
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        model.ImageFile.CopyToAsync(memoryStream);
                        model.FileByte = memoryStream.ToArray();
                    }
                    model.ImageActualName = model.ImageFile.FileName;

                }
                model.ImageFile = null;

                HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                responseSave.EnsureSuccessStatusCode();
                ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;

                if (viewModel != null && viewModel.IsSuccess)
                {
                    ModelState.Clear();
                    ViewBag.Message = viewModel.Message;
                   // model = new InfographicsViewModel();
                }
                else
                {
                    ViewBag.Message = viewModel != null ? viewModel.Message : "An error occured.";
                }

            }
            InitPage(model.ProjectId, model.LookupCategoryId);
            ViewBag.ProjectId = model.ProjectId;
            ViewBag.CategoryId = model.LookupCategoryId;
            //model.Id = Guid.Empty;
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid Id)
        {
            IViewModel resModel = new ViewData();

            if (Id != Guid.Empty)
            {
                HttpResponseMessage responseSave = _serviceRepository.GetResponseWithMethodName("Infographics", "DeleteURL", Id.ToString());
                responseSave.EnsureSuccessStatusCode();
                resModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
            }
            //InitPage();
            return Json("Ok");
        }

        [HttpPost]
        public IActionResult BulkDelete(InfographicsViewModel viewmodel)
        {
            IViewModel resModel = new ViewData(); 
            if(viewmodel.SelectIdList!=null)
            {
                Guid Id = Guid.Empty;
                for(int i=0;i< viewmodel.SelectIdList.Count();i++)
                {
                    Id = viewmodel.SelectIdList[i];
                    HttpResponseMessage responseSave = _serviceRepository.GetResponseWithMethodName("Infographics", "DeleteURL", Id.ToString());
                    responseSave.EnsureSuccessStatusCode();
                    resModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                }
            }
            //InitPage();
            return Json("Ok");
        }

        private void InitPage(Guid? ProjectId, Guid? CategoryId)
        {
            HttpResponseMessage StatusResponse = _serviceRepository.GetResponseWithMethodName("LookupCategory", "LookupCategoryURL", "Infographics");
            var categoryList = StatusResponse.Content.ReadAsAsync<List<CategoryViewModel>>().Result;
            if (CategoryId != Guid.Empty && CategoryId!=null)
            {
                ViewBag.CategoryList = new SelectList(categoryList, "Id", "CategoryName", CategoryId);
            }
            else
            {
                ViewBag.CategoryList = new SelectList(categoryList, "Id", "CategoryName");
            }

            HttpResponseMessage response = _serviceRepository.GetResponseWithMethodName("Project", "GetAllProjectURL");
            response.EnsureSuccessStatusCode();
            var projects = response.Content.ReadAsAsync<List<ProjectViewModel>>().Result;
            if (ProjectId != Guid.Empty && ProjectId != null)
            {
                ViewBag.ProjectList = new SelectList(projects, "Id", "ProjectName",ProjectId);
            }
            else
            {
                ViewBag.ProjectList = new SelectList(projects, "Id", "ProjectName");
            }
        }
    }
}