﻿
  
CREATE PROCEDURE [dbo].[SApp_GetConversionValues]    
 AS  
/*================================================================================  
Procedure Name: [SApp_GetConversionValues]  
Author: Swami  
Create date: 04/26/2019  
Description: Get list of conversion values from table  
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
04/26/2019 Swami   Initial Version  
================================================================================*/  
  
BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
	 
	SELECT Id as ValueConversionId, Conversion, ValueName FROM ValueConversion
	WHERE IsDeleted = 0 

END