﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.Trend
{
    public interface ITrendTimeTagService
    {
        IEnumerable<TrendTimeTagViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false, string id = "");

        Result<SpTransactionMessage> InsertUpdate(MainTrendTimeTagInsertUpdateDbViewModel tfc, AviraUser currenUser);
    }
}
