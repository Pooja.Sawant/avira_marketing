﻿

CREATE PROCEDURE [dbo].[SApp_GetAllParentIndustry]
	(
		@includeDeleted bit = 0
	)
	AS
/*================================================================================
Procedure Name: [SApp_GetAllParentIndustry]
Author: Harshal
Create date: 03/19/2019
Description: Get list from Industry table by ParentId
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
03/19/2019	Harshal			Initial Version
================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
declare @SegmentId uniqueidentifier;
select @SegmentId = Id from Segment
where SegmentName = 'End User';

	SELECT [SubSegment].[Id],
		[SubSegment].[SubSegmentName] as IndustryName,
		[SubSegment].[Description],
		[SubSegment].[ParentId],
		[SubSegment].[IsDeleted]
		
FROM [dbo].[SubSegment]
where ([SubSegment].ParentId is NULL OR [SubSegment].ParentId in (Select Id FROM SubSegment))
	AND [SubSegment].SegmentId = @SegmentId
	AND ([SubSegment].IsDeleted = 0)
	
	END