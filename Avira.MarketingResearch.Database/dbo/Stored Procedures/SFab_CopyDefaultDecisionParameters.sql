﻿

CREATE PROCEDURE [dbo].[SFab_CopyDefaultDecisionParameters]
(		
	@DecisionId uniqueidentifier = null,
	@UserID uniqueidentifier = null
)
AS

--declare @DecisionId uniqueidentifier = 'A22EC61E-8E75-479D-B3DF-3173D4EB3DE7',
--	@UserID uniqueidentifier = null

/*================================================================================
Procedure Name: [dbo].[SFab_CopyDefaultDecisionParameters]
Author: Nitin
Create date: 14/01/2020
Description: Copy all default decision parameters to Decision table
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
14/01/2020	Nitin			Initial Version
13-Feb-2020 Nitin			CR - Default rating changed to 1
================================================================================*/

BEGIN	

	Declare @DecisionParameterCount int
	Declare @BaseDecisionParameterCount int

	Select @DecisionParameterCount = Count(*) From DecisionParameter Where DecisionId = @DecisionId	
	Select @BaseDecisionParameterCount = Count(*) From BaseDecisionParameter

	Declare @DefaultWeightage decimal(18, 2) = 0

	Declare @RatingId1 uniqueidentifier
	Select @RatingId1 = Id from LookupCategory Where CategoryType = 'DMDecisionParameterRating' And CategoryName = '1'

	If (@DecisionParameterCount = 0)
	Begin

		Set @DefaultWeightage = 100.00 / @BaseDecisionParameterCount
	End	
	Insert Into DecisionParameter (DecisionId, BaseDecisionParameterId, Description, CategoryId, RatingId, Weightage) 		
	Select @DecisionId, BDP.Id As BaseDecisionParameterId, BDP.[Description], LC.Id, @RatingId1, @DefaultWeightage
	From BaseDecisionParameter BDP 
		Inner Join LookupCategory LC On 
			BDP.CategoryId = LC.Id And
			LC.CategoryType = 'DMDecisionParameterCategory'
	Where BDP.Id Not In 
		(Select BaseDecisionParameterId From DecisionParameter Where DecisionId = @DecisionId)
	
END