﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : AviraBaseController
    {
        public readonly IRoleService _roleService;
        private ILoggerManager _logger;
        public RoleController(
            IRoleService roleService, ILoggerManager logger)
        {
            _roleService = roleService;
            _logger = logger;
        }
        [HttpPost]
        public ActionResult Post([FromBody]RoleInsertDbViewModel roleViewModel)
        {

            roleViewModel.CreatedOn = System.DateTime.UtcNow;
            roleViewModel.UserCreatedById = CurrentUser.Id;
            roleViewModel.UserRoleID = Guid.NewGuid();


            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new Role, please contact Admin for more details."
            };
            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                var result = _roleService.Create(roleViewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "User roll name " + roleViewModel.UserRoleName + " is created successfully.";
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Ok(viewData); ;
        }

        [HttpPut]
        public ActionResult Post([FromBody]RoleUpdateViewModel roleViewModel)
        {

            roleViewModel.CreatedOn = System.DateTime.UtcNow;
            roleViewModel.UserCreatedById = CurrentUser.Id;


            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while updating a Role, please contact Admin for more details."
            };
            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                var result = _roleService.Put(roleViewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "User roll name " + roleViewModel.UserRoleName + " is updated successfully.";
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Ok(viewData); ;
        }

        [Route("DeleteSegment")]
        [HttpPut]
        public ActionResult DeleteSegment([FromBody] RoleUpdateViewModel viewModel)
        {

            viewModel.UserDeletedById = CurrentUser.Id;
            viewModel.IsDeleted = true;


            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while deleting a Uesr Role, please contact Admin for more details."
            };
            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                Result<SpTransactionMessage> result = _roleService.Update(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;

                    viewData.Message = "User Role Name " + viewModel.UserRoleName + " is deleted.";

                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Ok(viewData);
        }

        [HttpGet]
        [Route("GetAllPermissions")]
        public ActionResult<IEnumerable<RoleViewModel>> Get()
        {
            List<RoleViewModel> res = null;
            try
            {
                res = _roleService.GetAllPermissions().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet]
        public ActionResult<IEnumerable<RoleViewModel>> Get(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false)
        {
            List<RoleViewModel> res = null;
            try
            {
                res = _roleService.Get(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet("{id}")]
        public IEnumerable<RoleEditViewModel> Get(Guid id)
        {
            IEnumerable<RoleEditViewModel> roleViewModel = null;
            try
            {
                roleViewModel = _roleService.GetById(id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return roleViewModel;
        }
    }
}