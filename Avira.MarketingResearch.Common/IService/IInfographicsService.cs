﻿using Avira.MarketingResearch.Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService
{
    public interface IInfographicsService
    {
        InfographicsViewModel GetById(Guid guid);
        IEnumerable<InfographicsViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, Guid? projectId = null, Guid? lookupCategoryId = null);
        IViewModel CreateUpdate(InfographicsViewModel viewModel);
        IViewModel Delete(Guid Id);
    }

}