﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.Utils;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Avira.MarketingResearch.Web.Controllers
{
    [Authorize]
    [NoCache]
    public class EcoSystemPresenceController : BaseController
    {
        public readonly IServiceRepository _serviceRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;
        string contentRootPath = string.Empty;
        private ILoggerManager _logger;

        public EcoSystemPresenceController(IServiceRepository serviceRepository, ILoggerManager logger, IHostingEnvironment hostingEnvironment, IConfiguration configuration)
        {
            _hostingEnvironment = hostingEnvironment;
            _serviceRepository = serviceRepository;
            _logger = logger;
            _configuration = configuration;
            contentRootPath = _hostingEnvironment.ContentRootPath;
        }

        public IActionResult Index(string companyId, string projectId)
        {
            SetRoleList();
            EcoSystemPresenceDbViewModel ecoSystemPresenceDbViewModel = new EcoSystemPresenceDbViewModel();
            try
            {
                ecoSystemPresenceDbViewModel.CompanyId = new Guid(companyId);
                ecoSystemPresenceDbViewModel.ProjectId = new Guid(projectId);
                var ContollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage EcoSystemPresenceMapResponse = _serviceRepository.GetEcoSystemPresenceMapByCompanyId(ContollerName, ecoSystemPresenceDbViewModel.CompanyId);
                ecoSystemPresenceDbViewModel.EcoSystemPresences = EcoSystemPresenceMapResponse.Content.ReadAsAsync<List<EcoSystemPresenceMapViewModel>>().Result;

                HttpResponseMessage SectorPresenceMapResponse = _serviceRepository.GetSectorPresenceMapByCompanyId(ContollerName, ecoSystemPresenceDbViewModel.CompanyId);
                ecoSystemPresenceDbViewModel.SectorPresences = SectorPresenceMapResponse.Content.ReadAsAsync<List<SectorPresenceMapViewModel>>().Result;

                HttpResponseMessage PresenceKeyCompResponse = _serviceRepository.GetEcoSystemPresenceKeyCompByCompanyId(ContollerName, ecoSystemPresenceDbViewModel.CompanyId);
                ecoSystemPresenceDbViewModel.KeyCompetiters = PresenceKeyCompResponse.Content.ReadAsAsync<List<EcoSystemPresenceKeyCompViewModel>>().Result;

                if (ecoSystemPresenceDbViewModel.KeyCompetiters.Count < 5)
                {
                    for (int i = ecoSystemPresenceDbViewModel.KeyCompetiters.Count; i < 5; i++)
                    {
                        EcoSystemPresenceKeyCompViewModel objempty = new EcoSystemPresenceKeyCompViewModel();
                        ecoSystemPresenceDbViewModel.KeyCompetiters.Add(objempty);
                    }
                }

                HttpResponseMessage CompanyNoteResponse = _serviceRepository.GetCompanyNoteByIdResponse("CompanyNote", companyId, "EcoSystemPresence", null);
                CompanyNoteViewModel companyNoteViewModel = CompanyNoteResponse.Content.ReadAsAsync<CompanyNoteViewModel>().Result;
                ecoSystemPresenceDbViewModel.CompanyNoteModel = companyNoteViewModel ?? new CompanyNoteViewModel();

                HttpResponseMessage CompanyResponse = _serviceRepository.GetByIdResponse("Company", companyId);
                CompanyViewModel companyViewModel = CompanyResponse.Content.ReadAsAsync<CompanyViewModel>().Result;
                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, new Guid(projectId));
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                    ViewBag.StatusName = ecoSystemPresenceDbViewModel.CompanyNoteModel.StatusName;
                }

                ViewBag.CompanyName = companyViewModel.CompanyName;
                ViewBag.CompanyId = ecoSystemPresenceDbViewModel.CompanyId;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(ecoSystemPresenceDbViewModel);
        }

        private void SetRoleList()
        {
            try
            {
                var roles = HttpContext.Session.GetString("userRoles");
                var userRoles = JsonConvert.DeserializeObject<List<RoleViewModel>>(roles);
                List<string> RoleListItems = new List<string>();
                foreach (var item in userRoles)
                    RoleListItems.Add(item.UserRoleName);
                ViewBag.UserRoleList = RoleListItems;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult Index(EcoSystemPresenceDbViewModel model, string actionName)
        {
            SetRoleList();
            //ViewBag.Message = string.Empty;
            try
            {

                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                if (actionName.Equals("Save"))
                {
                    model.CompanyNoteModel.CompanyType = "EcoSystemPresence";
                    if (model.EcoSystemPresences != null && model.EcoSystemPresences.Count(x => (x.Ischecked)) < 1)
                    {
                        ViewBag.Message = "Please select atleast one Presence for the mapping.";
                    }
                    else if (model.SectorPresences != null && model.SectorPresences.Count(x => (x.Ischecked)) < 1)
                    {
                        ViewBag.Message = "Please select atleast one Sector for the mapping.";
                    }
                    else if (model.KeyCompetiters != null && model.KeyCompetiters.Count (x=>(x.KeyCompetiters!=null))<1)
                    {
                        ViewBag.Message = "Please select atleast one KeyCompetiters for the Company mapping.";
                    }
                    else if(String.IsNullOrEmpty(model.CompanyNoteModel.AuthorRemark))
                    {
                        ViewBag.Message = "Please add Author Remark.";
                    }
                    else
                    {
                        model.CompanyNoteModel.CompanyStatusName = "Draft";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            return RedirectToAction("Index", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                        }
                    }
                }
                else if (actionName.Equals("Submitted"))
                {
                    model.CompanyNoteModel.CompanyType = "EcoSystemPresence";
                    if (model.EcoSystemPresences != null && model.EcoSystemPresences.Count(x => (x.Ischecked)) < 1)
                    {
                        ViewBag.Message = "Please select atleast one Presence for the mapping.";
                    }
                    else if (model.SectorPresences != null && model.SectorPresences.Count(x => (x.Ischecked)) < 1)
                    {
                        ViewBag.Message = "Please select atleast one Sector for the mapping.";
                    }
                    else if (model.KeyCompetiters != null && model.KeyCompetiters.Count(x => (x.KeyCompetiters != null)) < 1)
                    {
                        ViewBag.Message = "Please select atleast one KeyCompetiters for the Company mapping.";
                    }
                    else if (String.IsNullOrEmpty(model.CompanyNoteModel.AuthorRemark))
                    {
                        ViewBag.Message = "Please add Author Remark.";
                    }
                    else
                    {
                        model.CompanyNoteModel.CompanyStatusName = "Submitted";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            return RedirectToAction("Index", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                        }
                    }

                }
                else if (actionName.Equals("Reject"))
                {
                    if (model.EcoSystemPresences != null && model.EcoSystemPresences.Count(x => (x.Ischecked)) < 1)
                    {
                        ViewBag.Message = "Please select atleast one Presence for the mapping.";
                    }
                    else if (model.SectorPresences != null && model.SectorPresences.Count(x => (x.Ischecked)) < 1)
                    {
                        ViewBag.Message = "Please select atleast one Sector for the mapping.";
                    }
                    else if (model.KeyCompetiters != null && model.KeyCompetiters.Count(x => (x.KeyCompetiters != null)) < 1)
                    {
                        ViewBag.Message = "Please select atleast one KeyCompetiters for the Company mapping.";
                    }
                    else if (String.IsNullOrEmpty(model.CompanyNoteModel.AuthorRemark))
                    {
                        ViewBag.Message = "Please add Author Remark.";
                    }
                    else
                    {
                        model.CompanyNoteModel.CompanyType = "EcoSystemPresence";
                        model.CompanyNoteModel.CompanyStatusName = "Rejected";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            return RedirectToAction("Index", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                        }
                    }
                }
                else if (actionName.Equals("Approve"))
                {
                    if (model.EcoSystemPresences != null && model.EcoSystemPresences.Count(x => (x.Ischecked)) < 1)
                    {
                        ViewBag.Message = "Please select atleast one Presence for the mapping.";
                    }
                    else if (model.SectorPresences != null && model.SectorPresences.Count(x => (x.Ischecked)) < 1)
                    {
                        ViewBag.Message = "Please select atleast one Sector for the mapping.";
                    }
                    else if (model.KeyCompetiters != null && model.KeyCompetiters.Count(x => (x.KeyCompetiters != null)) < 1)
                    {
                        ViewBag.Message = "Please select atleast one KeyCompetiters for the Company mapping.";
                    }
                    else if (String.IsNullOrEmpty(model.CompanyNoteModel.AuthorRemark))
                    {
                        ViewBag.Message = "Please add Author Remark.";
                    }
                    else
                    {
                        model.CompanyNoteModel.CompanyType = "EcoSystemPresence";
                        model.CompanyNoteModel.CompanyStatusName = "Approved";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            return RedirectToAction("Index", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                        }
                    }
                }

                else if (actionName.Equals("Next"))
                    return RedirectToAction("Index", "CompanySWOT", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                else if (actionName.Equals("Previous"))
                    return RedirectToAction("Index", "News", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                else if (actionName.Equals("Cancel"))
                    return RedirectToAction("Index", "#", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                else if (actionName.Equals("AddRow"))
                {
                    EcoSystemPresenceKeyCompViewModel objempty = new EcoSystemPresenceKeyCompViewModel();
                    model.KeyCompetiters.Add(objempty);
                    model.CompanyNoteModel = model.CompanyNoteModel ?? new CompanyNoteViewModel();
                }
                HttpResponseMessage CompanyNoteResponse = _serviceRepository.GetCompanyNoteByIdResponse("CompanyNote", model.CompanyId.ToString(), "EcoSystemPresence", null);
                CompanyNoteViewModel companyNoteViewModel = CompanyNoteResponse.Content.ReadAsAsync<CompanyNoteViewModel>().Result;
                companyNoteViewModel = companyNoteViewModel ?? new CompanyNoteViewModel();
                HttpResponseMessage CompanyResponse = _serviceRepository.GetByIdResponse("Company", model.CompanyId.ToString());
                CompanyViewModel companyViewModel = CompanyResponse.Content.ReadAsAsync<CompanyViewModel>().Result;
                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, model.ProjectId);
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                    ViewBag.StatusName = companyNoteViewModel.StatusName ?? string.Empty;
                }

                ViewBag.CompanyName = companyViewModel.CompanyName;
                ViewBag.CompanyId = model.CompanyId;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(model);
        }

    }
}