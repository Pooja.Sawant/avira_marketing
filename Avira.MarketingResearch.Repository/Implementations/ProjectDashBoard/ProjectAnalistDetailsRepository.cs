﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Repository.CoreInterfaces.ProjectDashBoard;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.ProjectDashBoard
{
    public class ProjectAnalistDetailsRepository : IProjectAnalistDetailsRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "ProjectAnalistDetails";
        public ProjectAnalistDetailsRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }


        public ProjectAnalistDetailsViewModel GetById(Guid guid)
        {
            ProjectAnalistDetailsViewModel projectAnalistDetailsViewModel = new ProjectAnalistDetailsViewModel();
            projectAnalistDetailsViewModel = GetAllProjectAnalistDetails(guid).FirstOrDefault();
            return projectAnalistDetailsViewModel;
        }

        public IEnumerable<ProjectAnalistDetailsViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted)
        {
            return GetAllProjectAnalistDetails(null, PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, includeDeleted);
        }

        private IEnumerable<ProjectAnalistDetailsViewModel> GetAllProjectAnalistDetails(Guid? guid, int PageStart = 0, int pageSize = 0, int SortDirection = -1, string OrdbyByColumnName = "", string Search = "", bool includeDeleted = false)
        {

            DataTable projectAnalistDetailsIdsTable = new DataTable();
            projectAnalistDetailsIdsTable.Columns.Add("ID", typeof(Guid));

            if (guid != null)
            {
                projectAnalistDetailsIdsTable.Rows.Add(guid);
            }
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<ProjectAnalistDetailsViewModel>(ProjectAnalistDetailsQueries.StoreProcSelectAll
                            , new { @ProjectAnalistDetailsIDList = projectAnalistDetailsIdsTable.AsTableValuedParameter("dbo.udtUniqueIdentifier"), @includeDeleted = includeDeleted, @PageStart = PageStart, @pageSize = pageSize, @SortDirection = SortDirection, @OrdbyByColumnName = OrdbyByColumnName, @AnalysisType = Search }
                            , null, true, null, CommandType.StoredProcedure);
        }

        public Result<SpTransactionMessage> Create(ProjectAnalistDetailsInsertDbViewModel projectAnalistDetailsInsertDbViewModel)
        {
             Result<SpTransactionMessage> innerResponse=null;
            if (projectAnalistDetailsInsertDbViewModel.AnalysisType != "Approver" && projectAnalistDetailsInsertDbViewModel.AnalysisType != "Co-Approver")
            {
                projectAnalistDetailsInsertDbViewModel.AnalystType = "Author";
                projectAnalistDetailsInsertDbViewModel.UserIDs = new[] { projectAnalistDetailsInsertDbViewModel.AuthorUserID };
                innerResponse = Create_New(projectAnalistDetailsInsertDbViewModel);

                if (projectAnalistDetailsInsertDbViewModel.CoAuthorUserID != null)
                {
                    projectAnalistDetailsInsertDbViewModel.AnalystType = "Co-Author";
                    projectAnalistDetailsInsertDbViewModel.UserIDs = projectAnalistDetailsInsertDbViewModel.CoAuthorUserID.ToArray();
                    innerResponse = Create_New(projectAnalistDetailsInsertDbViewModel);
                }
            }
           
            if (projectAnalistDetailsInsertDbViewModel.AnalysisType == "Approver")
            {
                projectAnalistDetailsInsertDbViewModel.AnalystType = "Approver";
                projectAnalistDetailsInsertDbViewModel.UserIDs = new[] { projectAnalistDetailsInsertDbViewModel.ApproverUserId };
                innerResponse = Create_New(projectAnalistDetailsInsertDbViewModel);

                //projectAnalistDetailsInsertDbViewModel.AnalystType = "Co-Approver";
                //projectAnalistDetailsInsertDbViewModel.UserIDs = projectAnalistDetailsInsertDbViewModel.CoApproverUserID.ToArray();
                //innerResponse = Create_New(projectAnalistDetailsInsertDbViewModel);
            }
            if (projectAnalistDetailsInsertDbViewModel.AnalysisType == "Co-Approver")
            {
                if (projectAnalistDetailsInsertDbViewModel.CoApproverUserID != null)
                {
                    projectAnalistDetailsInsertDbViewModel.AnalystType = "Co-Approver";
                    projectAnalistDetailsInsertDbViewModel.UserIDs = projectAnalistDetailsInsertDbViewModel.CoApproverUserID.ToArray();
                    innerResponse = Create_New(projectAnalistDetailsInsertDbViewModel);
                }

                //projectAnalistDetailsInsertDbViewModel.AnalystType = "Approver";
                //projectAnalistDetailsInsertDbViewModel.UserIDs = new[] { projectAnalistDetailsInsertDbViewModel.ApproverUserId };
                //innerResponse = Create_New(projectAnalistDetailsInsertDbViewModel);
            }
   
            return innerResponse;
           
        }

        private Result<SpTransactionMessage> Create_New(ProjectAnalistDetailsInsertDbViewModel projectAnalistDetailsInsertDbViewModel)
        {
            DataTable AnalystUserIDLists = new DataTable();
            AnalystUserIDLists.Columns.Add("ID", typeof(Guid));
            if (projectAnalistDetailsInsertDbViewModel.UserIDs != null)
            {
                for (int i = 0; i < projectAnalistDetailsInsertDbViewModel.UserIDs.Count(); i++)
                {
                    AnalystUserIDLists.Rows.Add(new Object[] { projectAnalistDetailsInsertDbViewModel.UserIDs[i] });
                }
            }

            var spTransactionMessage = _dbHelper.ExecuteQuery(ProjectAnalistDetailsQueries.StoreProcInsert,
                 new
                 {                     
                     @ProjectID = projectAnalistDetailsInsertDbViewModel.ProjectID,
                     @AnalysisType= projectAnalistDetailsInsertDbViewModel.AnalysisType,
                     @AnalystType= projectAnalistDetailsInsertDbViewModel.AnalystType,
                     @AnalystUserIDList = AnalystUserIDLists,
                     @UserCreatedById = projectAnalistDetailsInsertDbViewModel.UserCreatedById
                 },
                CommandType.StoredProcedure);
            return spTransactionMessage;
           
        }

        public Result<SpTransactionMessage> Update(ProjectAnalistDetailsUpdateDbViewModel projectAnalistDetailsUpdateDbViewModel)
        {
          
             Result<SpTransactionMessage> innerResponse=null;
            if (projectAnalistDetailsUpdateDbViewModel.AnalysisType != "Approver" && projectAnalistDetailsUpdateDbViewModel.AnalysisType != "Co-Approver")
            {
             
                
                var dictAnalystList = projectAnalistDetailsUpdateDbViewModel.AuthorUserIDs.Select((value, index) => new { value, index })
                      .ToDictionary(pair => pair.value, pair => "Author");
                innerResponse = Update_New(projectAnalistDetailsUpdateDbViewModel, dictAnalystList);
                if (projectAnalistDetailsUpdateDbViewModel.CoAuthorUserIDs != null)
                {
                    dictAnalystList = projectAnalistDetailsUpdateDbViewModel.CoAuthorUserIDs.Select((value, index) => new { value, index })
                            .ToDictionary(pair => pair.value, pair => "Co-Author");
                    innerResponse = Update_New(projectAnalistDetailsUpdateDbViewModel, dictAnalystList);
                }
            }
           
            if (projectAnalistDetailsUpdateDbViewModel.AnalysisType == "Approver")
            {
             
                var dictAnalystList = projectAnalistDetailsUpdateDbViewModel.AuthorUserIDs.Select((value, index) => new { value, index })
                        .ToDictionary(pair => pair.value, pair => "Approver");
                innerResponse = Update_New(projectAnalistDetailsUpdateDbViewModel, dictAnalystList);
              


            }
            if (projectAnalistDetailsUpdateDbViewModel.AnalysisType == "Co-Approver")
            {

                if (projectAnalistDetailsUpdateDbViewModel.CoAuthorUserIDs != null)
                {
                    var dictAnalystList = projectAnalistDetailsUpdateDbViewModel.CoAuthorUserIDs.Select((value, index) => new { value, index })
                           .ToDictionary(pair => pair.value, pair => "Co-Approver");
                    innerResponse = Update_New(projectAnalistDetailsUpdateDbViewModel, dictAnalystList);
                }
             

                //dictAnalystList = projectAnalistDetailsUpdateDbViewModel.AuthorUserIDs.Select((value, index) => new { value, index })
                //       .ToDictionary(pair => pair.value, pair => "Approver");
                //innerResponse = Update_New(projectAnalistDetailsUpdateDbViewModel, dictAnalystList);
            }
   
            return innerResponse;
        }

        private Result<SpTransactionMessage> Update_New(ProjectAnalistDetailsUpdateDbViewModel projectAnalistDetailsUpdateDbViewModel, Dictionary<Guid, string> analysisType)
        {
            DataTable AnalystUserIDLists = new DataTable();
            AnalystUserIDLists.Columns.Add("ID", typeof(Guid));
            AnalystUserIDLists.Columns.Add("Value", typeof(string)); 
            if (analysisType != null)
            {
                foreach (var at in analysisType)
                {
                    AnalystUserIDLists.Rows.Add(new Object[] { at.Key, at.Value });
                }
            }

            var spTransactionMessage = _dbHelper.ExecuteQuery(ProjectAnalistDetailsQueries.StoreProcUpdate,
                 new
                 {
                     @ProjectID = projectAnalistDetailsUpdateDbViewModel.ProjectID,
                     @AnalysisType = projectAnalistDetailsUpdateDbViewModel.AnalysisType,                 
                     @AnalystList = AnalystUserIDLists,
                     @UserModifiedById = projectAnalistDetailsUpdateDbViewModel.UserModifiedById
                 },
                CommandType.StoredProcedure);
            return spTransactionMessage;

        }
    }
}
