﻿CREATE TABLE [dbo].[QualitativeTableTab] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [ProjectId]        UNIQUEIDENTIFIER NULL,
    [Rows]             INT              NULL,
    [Columns]          INT              NULL,
    [TableName]        NVARCHAR (50)    NULL,
    [TableMetadata]    NVARCHAR (512)   NULL,
    [TableData]        NVARCHAR (512)   NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_QualitativeTableTab] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_QualitativeTableTab_AviraUser] FOREIGN KEY ([UserDeletedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_QualitativeTableTab_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_QualitativeTableTab_QualitativeTableTab] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id])
);

