﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyOtherInfoController : AviraBaseController
    {
        private readonly ICompanyOtherInfoService _companyOtherInfoService;
        private readonly ICompanyOtherInfoRepository _companyOtherInfoRepository;
        private ILoggerManager _logger;
        public CompanyOtherInfoController(
            ICompanyOtherInfoService companyOtherInfoService,
            ICompanyOtherInfoRepository companyOtherInfoRepository,
             ILoggerManager logger)
        {
            _companyOtherInfoRepository = companyOtherInfoRepository;
            _companyOtherInfoService = companyOtherInfoService;
            _logger = logger;

        }

        [HttpGet]
        public ActionResult<IEnumerable<CompanyOtherInfoInsertUpdateViewModel>> Get(Guid? id)
        {
            List<CompanyOtherInfoInsertUpdateViewModel> res = null;
            try
            {
                 res = _companyOtherInfoService.GetAll(id).ToList();
        }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpPost]
        public ActionResult Post([FromBody] MainCompanyOtherInfoInsertUpdateViewModel viewModel)
        {


            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new Company Other Info, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                var result = _companyOtherInfoService.InsertUpdate(viewModel, CurrentUser);
                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Company Other Info is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }

    }
}
    