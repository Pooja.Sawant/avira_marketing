﻿CREATE TABLE [dbo].[QualitativeRegionMapping] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [ProjectId]        UNIQUEIDENTIFIER NOT NULL,
    [MarketId]         UNIQUEIDENTIFIER NULL,
    [RegionId]         UNIQUEIDENTIFIER NOT NULL,
    [CountryId]        UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_QualitativeRegionMapping] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_QualitativeRegionMapping_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_QualitativeRegionMapping_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_QualitativeRegionMapping_Country] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Country] ([Id]),
    CONSTRAINT [FK_QualitativeRegionMapping_Market] FOREIGN KEY ([MarketId]) REFERENCES [dbo].[Market] ([Id]),
    CONSTRAINT [FK_QualitativeRegionMapping_Project] FOREIGN KEY ([ProjectId]) REFERENCES [dbo].[Project] ([ID]),
    CONSTRAINT [FK_QualitativeRegionMapping_Region] FOREIGN KEY ([RegionId]) REFERENCES [dbo].[Region] ([Id])
);

