﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.CompanyProfile
{
    public interface ICompanyClientAndStrategyService
    {
        IEnumerable<CompanyStrategyViewModel> GetstrategyById(Guid? guid);
        IEnumerable<CompanyclientViewModel> GetclientById(Guid? guid);
        Result<SpTransactionMessage> InsertUpdate(CompanyClientStrategyInsertUpdateDbViewModel viewModel, AviraUser currenUser);

        IEnumerable<CompanyClientStrategyImportanceViewModel> GetAllImportance();
        IEnumerable<CompanyClientStrategyDirectionViewModel> GetAllImpactDirection();
        IEnumerable<CompanyClientStrategyCategoryViewModel> GetAllCategories();
        IEnumerable<CompanyClientStrategyApproachViewModel> GetAllApproach();
        IEnumerable<CompanyClientStrategyIndustryViewModel> GetAllIndustries();

    }
}
