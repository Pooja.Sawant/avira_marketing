﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.CompanyProfile
{
    public class CompanyShareVolumeModel
    {
        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
        public int Period { get; set; }
        public Guid CurrencyId { get; set; }
        public Guid UserCreatedById { get; set; }
        public string CurrencyNameAndCode { get; set; }
        public DateTime BaseDate { get; set; }
        public int Date { get; set; }
        public decimal Price { get; set; }
        public decimal Volume { get; set; }
        public bool IsDeleted { get; set; }
        public Guid AviraUserId { get; set; }
    }

    public class CompanyShareImageModel
    {
        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
        public Guid ProjectId { get; set; }
        public Guid? ParentCompanyId { get; set; }
        public byte[] fileByte { get; set; }
        public string fileExtension { get; set; }
        public string ImageActualName { get; set; }
        public string ImageDisplayName { get; set; }
        public string ImageURL { get; set; }
        public IFormFile ImageFile { get; set; }
        public int Year { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public Guid? AviraUserId { get; set; }
        public string CompanyStatusName { get; set; }
    }

    public class MainCompanyShareVolModel
    {
        public List<CompanyShareVolumeModel> CompanyShareVolumeDataModel { get; set; }
        public CompanyShareImageModel CompanyShareImageModel { get; set; }
        public CompanyNoteViewModel CompanyNoteViewModel { get; set; }
    }
}
