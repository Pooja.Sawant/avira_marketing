﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.Utils;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Avira.MarketingResearch.Web.Controllers
{
    [Authorize]
    [NoCache]
    public class CompanyClientAndStrategyController : BaseController
    {
        public readonly IServiceRepository _serviceRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;
        string contentRootPath = string.Empty;
        private ILoggerManager _logger;

        public CompanyClientAndStrategyController(IServiceRepository serviceRepository, ILoggerManager logger, IHostingEnvironment hostingEnvironment, IConfiguration configuration)
        {
            _hostingEnvironment = hostingEnvironment;
            _serviceRepository = serviceRepository;
            _logger = logger;
            _configuration = configuration;
            contentRootPath = _hostingEnvironment.ContentRootPath;
        }

        public IActionResult Index(string CompanyId,string ProjectId)
        {
            ViewBag.Message = string.Empty; 
            SetRoleList();
            BindDropdowns();
            CompanyClientStrategyInsertUpdateDbViewModel companyClientStrategyInsertUpdateDbViewModel = new CompanyClientStrategyInsertUpdateDbViewModel();
            try
            {
                companyClientStrategyInsertUpdateDbViewModel.CompanyId = new Guid(CompanyId);
                companyClientStrategyInsertUpdateDbViewModel.ProjectId = new Guid(ProjectId);
                var ContollerName = this.ControllerContext.RouteData.Values["controller"].ToString();

                // Get all strategy mapped to the Company
                HttpResponseMessage responseStrategy = _serviceRepository.GetAllStrategyByCompanyIdSegmentResponse(ContollerName, CompanyId);
                responseStrategy.EnsureSuccessStatusCode();
                List<CompanyStrategyViewModel> companyStrategyViewModel = responseStrategy.Content.ReadAsAsync<List<CompanyStrategyViewModel>>().Result;
                if (companyStrategyViewModel.Count == 0)
                {
                    CompanyStrategyViewModel objempty = new CompanyStrategyViewModel();
                    objempty.Date = DateTime.Now;
                    companyStrategyViewModel.Add(objempty);
                }

                // Get all Clients mapped to the Company
                HttpResponseMessage responseclients = _serviceRepository.GetAllclientByCompanyIdSegmentResponse(ContollerName, CompanyId);
                responseclients.EnsureSuccessStatusCode();
                List<CompanyclientViewModel> companyclientViewModel = responseclients.Content.ReadAsAsync<List<CompanyclientViewModel>>().Result;
                if (companyclientViewModel.Count == 0)
                {
                    CompanyclientViewModel objempty = new CompanyclientViewModel();
                    companyclientViewModel.Add(objempty);
                }

                // assig  strategy list to Model
                companyClientStrategyInsertUpdateDbViewModel.CompanyStrategyViewModel = companyStrategyViewModel;

                // assig  client list to Model
                companyClientStrategyInsertUpdateDbViewModel.CompanyclientViewModel = companyclientViewModel;

                HttpResponseMessage CompanyNoteResponse = _serviceRepository.GetCompanyNoteByIdResponse("CompanyNote", CompanyId, "CompanyClientAndStrategy", null);
                CompanyNoteViewModel companyNoteViewModel = CompanyNoteResponse.Content.ReadAsAsync<CompanyNoteViewModel>().Result;
                companyClientStrategyInsertUpdateDbViewModel.CompanyNoteViewModel = companyNoteViewModel ?? new CompanyNoteViewModel();

                HttpResponseMessage CompanyResponse = _serviceRepository.GetByIdResponse("Company", CompanyId);
                CompanyViewModel companyViewModel = CompanyResponse.Content.ReadAsAsync<CompanyViewModel>().Result;
                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, new Guid(ProjectId));
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                    ViewBag.StatusName = companyClientStrategyInsertUpdateDbViewModel.CompanyNoteViewModel.StatusName;
                }
                ViewBag.CompanyName = companyViewModel.CompanyName;
                ViewBag.CompanyId = companyClientStrategyInsertUpdateDbViewModel.CompanyId;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(companyClientStrategyInsertUpdateDbViewModel); 
        }

        [HttpPost]
        public IActionResult Index(CompanyClientStrategyInsertUpdateDbViewModel model, string actionName)
        { 
            ViewBag.Message = string.Empty;
            bool bResult = false;
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                model.CompanyNoteViewModel.CompanyType = "CompanyClientAndStrategy";
                if (actionName.Equals("Save"))
                {
                    bResult = validate(model);
                    if (bResult)
                    {
                        model.CompanyNoteViewModel.CompanyStatusName = "Draft";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            return RedirectToAction("Index", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                        }
                    }
                }
                else if (actionName.Equals("Submitted"))
                {
                    bResult = validate(model);
                    if (bResult)
                    {
                        model.CompanyNoteViewModel.CompanyStatusName = "Submitted";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            return RedirectToAction("Index", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                        }
                    }

                }
                else if (actionName.Equals("Reject"))
                {
                    bResult = validate(model);
                    if (bResult)
                    {
                        model.CompanyNoteViewModel.CompanyType = "CompanyClientAndStrategy";
                        model.CompanyNoteViewModel.CompanyStatusName = "Rejected";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            return RedirectToAction("Index", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                        }
                    }
                }
                else if (actionName.Equals("Approve"))
                {
                    bResult = validate(model);
                    if (bResult)
                    {
                        model.CompanyNoteViewModel.CompanyType = "CompanyClientAndStrategy";
                        model.CompanyNoteViewModel.CompanyStatusName = "Approved";
                        HttpResponseMessage responseSave = _serviceRepository.PostResponse(contollerName, model);
                        responseSave.EnsureSuccessStatusCode();
                        ViewData viewModel = responseSave.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            return RedirectToAction("Index", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                        }
                    }
                }

                else if (actionName.Equals("Next"))
                    return RedirectToAction("Index", "News", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                else if (actionName.Equals("Previous"))
                    return RedirectToAction("Index", "CompanyShareVolume", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                else if (actionName.Equals("Cancel"))
                    return RedirectToAction("Index", "#", new { CompanyId = model.CompanyId.ToString(), ProjectId = model.ProjectId.ToString() });
                else if(actionName.Equals("AddRowStrategy"))
                {
                    CompanyStrategyViewModel objempty = new CompanyStrategyViewModel();
                    model.CompanyStrategyViewModel.Add(objempty);
                    model.CompanyNoteViewModel = model.CompanyNoteViewModel ?? new CompanyNoteViewModel();
                }
                else if(actionName.Equals("AddRowclient"))
                {
                    CompanyclientViewModel objempty = new CompanyclientViewModel();
                    model.CompanyclientViewModel.Add(objempty);
                    model.CompanyNoteViewModel = model.CompanyNoteViewModel ?? new CompanyNoteViewModel();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            SetRoleList();
            BindDropdowns();
            HttpResponseMessage CompanyNoteResponse = _serviceRepository.GetCompanyNoteByIdResponse("CompanyNote", model.CompanyId.ToString(), "CompanyClientAndStrategy", null);
            CompanyNoteViewModel companyNoteViewModel = CompanyNoteResponse.Content.ReadAsAsync<CompanyNoteViewModel>().Result;
            companyNoteViewModel = companyNoteViewModel ?? new CompanyNoteViewModel();
            HttpResponseMessage CompanyResponse = _serviceRepository.GetByIdResponse("Company", model.CompanyId.ToString());
            CompanyViewModel companyViewModel = CompanyResponse.Content.ReadAsAsync<CompanyViewModel>().Result;
            ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, model.ProjectId);
            if (project != null)
            {
                ViewBag.ProjectId = project.Id;
                ViewBag.ProjectName = project.ProjectName;
                ViewBag.ProjectCode = project.ProjectCode;
                ViewBag.Approver = project.Approver;
                ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                ViewBag.StatusName = companyNoteViewModel.StatusName??string.Empty;
            }
            ViewBag.CompanyName = companyViewModel.CompanyName;
            ViewBag.CompanyId = model.CompanyId;
            return View(model); 
        }

        private void BindDropdowns()
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage responseImpactType = _serviceRepository.GetAllImpactDirectionByCompanyIdResponse(contollerName);
                responseImpactType.EnsureSuccessStatusCode();
                ViewBag.ImpactList = responseImpactType.Content.ReadAsAsync<List<CompanyClientStrategyDirectionViewModel>>().Result;

                HttpResponseMessage responseImportance = _serviceRepository.GetAllImportanceByCompanyIdResponse(contollerName);
                responseImportance.EnsureSuccessStatusCode();
                ViewBag.ImportanceList = responseImportance.Content.ReadAsAsync<List<CompanyClientStrategyImportanceViewModel>>().Result;

                HttpResponseMessage responseApproach = _serviceRepository.GetAllApproachByCompanyIdSegmentResponse(contollerName);
                responseApproach.EnsureSuccessStatusCode();
                ViewBag.ApproachList = responseApproach.Content.ReadAsAsync<List<CompanyClientStrategyApproachViewModel>>().Result;

                HttpResponseMessage responseCategories = _serviceRepository.GetAllCategoriesByCompanyIdSegmentResponse(contollerName);
                responseCategories.EnsureSuccessStatusCode();
                ViewBag.CategoryList = responseCategories.Content.ReadAsAsync<List<CompanyClientStrategyCategoryViewModel>>().Result;

                HttpResponseMessage responseIndustry = _serviceRepository.GetAllIndustryByCompanyIdSegmentResponse(contollerName);
                responseIndustry.EnsureSuccessStatusCode();
                ViewBag.IndustryList = responseIndustry.Content.ReadAsAsync<List<CompanyClientStrategyIndustryViewModel>>().Result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

        } 
        private void SetRoleList() 
        {
            try
            {
                var roles = HttpContext.Session.GetString("userRoles");
                var userRoles = JsonConvert.DeserializeObject<List<RoleViewModel>>(roles);
                List<string> RoleListItems = new List<string>();
                foreach (var item in userRoles)
                    RoleListItems.Add(item.UserRoleName);
                ViewBag.UserRoleList = RoleListItems;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        /// <summary>
        /// Method to validate Form
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private bool validate(CompanyClientStrategyInsertUpdateDbViewModel model)
        {
            if (model.CompanyStrategyViewModel.Count < 2)
            {
                ViewBag.Message = "Please fill atleast two set of Company strategy mapping";
                return false;
            }
            if (model.CompanyclientViewModel.Count < 2)
            {
                ViewBag.Message = "Please fill atleast two set of Company Client mapping";
                return false;
            }
            if (model.CompanyNoteViewModel.AuthorRemark == null)
            {
                ViewBag.Message = "Please fill Author Notes.";
                return false;
            }
            else
            {
                ViewBag.Message = string.Empty;
                return true;
            }
        }

    }
}