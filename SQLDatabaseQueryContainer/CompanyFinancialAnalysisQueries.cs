﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public class CompanyFinancialAnalysisQueries
    {
        public static readonly string StoreProcInsertUpdate = "SApp_insertCompanyFinancialAnalysis";       
        public static string StoreProcSelectAll = "SApp_GetCompanyFinancialAnalysisByCompanyID";
        public static string GetAllValueConversion = "SApp_GetConversionValues";
        public static string StoreProcGetAllCurrencies = "SApp_GetAllCurrenciesWithCode";
        public static string StoreProcGetAllCategories = "SApp_GetAllCategory";

    }
}
