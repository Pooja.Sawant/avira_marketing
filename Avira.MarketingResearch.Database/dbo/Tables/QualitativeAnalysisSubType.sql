﻿CREATE TABLE [dbo].[QualitativeAnalysisSubType] (
    [Id]                             UNIQUEIDENTIFIER NOT NULL,
    [QualitativeAnalysisSubTypeName] NVARCHAR (256)   CONSTRAINT [DF_QualitativeAnalysisSubType_QualitativeAnalysisSubTypeName] DEFAULT ('') NOT NULL,
    [Description]                    NVARCHAR (256)   NULL,
    [CreatedOn]                      DATETIME2 (7)    NULL,
    [UserCreatedById]                UNIQUEIDENTIFIER NULL,
    [ModifiedOn]                     DATETIME2 (7)    NULL,
    [UserModifiedById]               UNIQUEIDENTIFIER NULL,
    [IsDeleted]                      BIT              CONSTRAINT [DF_QualitativeAnalysisSubType_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]                      DATETIME2 (7)    NULL,
    [UserDeletedById]                UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_QualitativeAnalysisSubType] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_QualitativeAnalysisSubType_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_QualitativeAnalysisSubType_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id])
);

