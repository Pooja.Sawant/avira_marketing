﻿

/*================================================================================      
Procedure Name: [SApp_GetNewsByCompanyID]   
Author: Harshal      
Create date: 05/08/2019      
Description: Get Records froms News By CompanyId      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
05/08/2019 Harshal   Initial Version     
================================================================================*/  
CREATE PROCEDURE [dbo].[SApp_GetNewsByCompanyID]     
	@CompanyID uniqueidentifier ,
	@ProjectID uniqueidentifier , 
	@includeDeleted bit = 0     
AS
BEGIN 
	SET NOCOUNT ON;  
	SELECT [News].[Id]
	  ,[Date]
	  ,[News]
      ,[CompanyId]
	  ,[ProjectID]
      ,[ImportanceId] 
	  ,Importance.ImportanceName as ImportanceName
      ,[ImpactDirectionId] 
	  ,ImpactType.ImpactDirectionName as ImpactName
	  ,NewsCategoryId
	  ,NewsCategory.NewsCategoryName
	  ,[News].[OtherParticipants]
	  ,[News].[Remarks]
      ,[News].[CreatedOn]
      ,[News].[UserCreatedById]
      ,[News].[ModifiedOn]
      ,[News].[UserModifiedById]
      ,[News].[IsDeleted]
      ,[News].[DeletedOn]
      ,[News].[UserDeletedById]
  FROM [dbo].[News]
  INNER JOIN [dbo].[Importance] Importance ON [News].ImportanceId = Importance.Id
  INNER JOIN [dbo].[ImpactDirection] ImpactType ON [News].ImpactDirectionId = ImpactType.Id
  INNER JOIN [dbo].[NewsCategory] NewsCategory ON [News].NewsCategoryId = NewsCategory.Id
  WHERE ([News].CompanyId = @CompanyID --and [News].ProjectID = @ProjectID
  )      
 AND (@includeDeleted = 0 or [News].IsDeleted = 1)  
END