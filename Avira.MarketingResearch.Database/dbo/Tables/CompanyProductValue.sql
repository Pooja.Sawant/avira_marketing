﻿CREATE TABLE [dbo].[CompanyProductValue] (
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [CompanyProductId]  UNIQUEIDENTIFIER NULL,
    [Year]              INT              NULL,
    [CurrencyId]        UNIQUEIDENTIFIER NULL,
    [ValueConversionId] UNIQUEIDENTIFIER NULL,
    [Amount]            DECIMAL (18, 2)  NULL,
    CONSTRAINT [PK_CompanyProductValue] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CompanyProductValue_CompanyProduct] FOREIGN KEY ([CompanyProductId]) REFERENCES [dbo].[CompanyProduct] ([Id]),
    CONSTRAINT [FK_CompanyProductValue_Currency] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[Currency] ([Id]),
    CONSTRAINT [FK_CompanyProductValue_ValueConversion] FOREIGN KEY ([ValueConversionId]) REFERENCES [dbo].[ValueConversion] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_CompanyProductValue]
    ON [dbo].[CompanyProductValue]([Id] ASC);

