﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.ImportExport;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImportRequestController : AviraBaseController
    {
        
        public readonly IImportRequestService _iImportRequestService;
        private ILoggerManager _logger;

        public ImportRequestController(
            IImportRequestService iImportRequestService,
            ILoggerManager logger)
        {
            _iImportRequestService = iImportRequestService;
            _logger = logger;
        }

        [HttpGet("{id}")]
        public ActionResult<ImportRequestViewModel> GetImportRequest(Guid Id)
        {
            ImportRequestViewModel ImportRequest = new ImportRequestViewModel();
            try
            {
                ImportRequest = _iImportRequestService.GetRequest(Id);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);

            }
            return ImportRequest;
        }

        [Route("GetAllByFilters")]
        public ActionResult<IEnumerable<ImportRequestViewModel>> GetAllByFilters(ImportRequestViewModel viewModel)
        {
            List<ImportRequestViewModel> res = null;
            try
            {
                viewModel.UserImportedById = CurrentUser.Id;
                res = _iImportRequestService.GetAll(viewModel).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }


        [HttpPost]
        public ActionResult Post([FromBody] ImportRequestViewModel viewModel)
        {
            viewModel.ImportedOn = System.DateTime.Now;
            viewModel.UserImportedById = CurrentUser.Id;

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new Import Request, please contact Admin for more details."
            };

            try
            {

                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                var result = _iImportRequestService.Create(viewModel);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Import request is created successfully.Please click on Import History button to check status of imported file"; 
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }


        [HttpPut]
        public ActionResult Put([FromBody] ImportRequestViewModel viewModel)
        {
            viewModel.ModifiedOn = System.DateTime.UtcNow;
            viewModel.UserModifiedById = CurrentUser.Id;
            
            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while updating Import request, please contact Admin for more details."
            };
            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                Result<SpTransactionMessage> result = _iImportRequestService.Update(viewModel);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Import request is updated successfully.";
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }

        
        [Route("GetImportTemplate")]
        public ActionResult<ImportRequestViewModel> GetImportTemplate(string Id)
        {
            ImportRequestViewModel ImportRequest = new ImportRequestViewModel();
            try
            {
                ImportRequest = _iImportRequestService.GetImportTemplate(Id);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);

            }
            return ImportRequest;
        }

    }
}