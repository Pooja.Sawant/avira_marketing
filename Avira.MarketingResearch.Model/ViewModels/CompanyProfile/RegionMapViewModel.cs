﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.CompanyProfile
{
    public class RegionMapViewModel
    {
        public Guid Id { get; set; }
        public string RegionName { get; set; }
        public Guid ParentRegionId { get; set; }
        public int RegionLevel { get; set; }
        //public List<CountryViewModel> CountryList { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "Region";
            }
        }
    }
}
