﻿using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Syncfusion.XlsIO;
using Syncfusion.Drawing;


namespace Avira.MarketingResearch.Web.ImportExportUtility
{
    public class ImportMarketSizing : Controller, IExcelImport
    {
        public readonly IServiceRepository _serviceRepository;
        private ILoggerManager _logger;
        public ImportMarketSizing(IServiceRepository serviceRepository, ILoggerManager logger)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
        }

        public ImportResponse Import(IFormFile formFile)
        {
            var res = new ImportResponse();
            var list = new List<MarketSizingImportViewModel>();
            var inputModel = new MarketSizingImportInsertViewModel();
            using (var stream = new MemoryStream())
            {
                formFile.CopyTo(stream);

                using (var package = new ExcelPackage(stream))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                    //ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
                    //ExcelWorksheet worksheet = package.Workbook.Worksheets[2];
                    if (worksheet.Dimension == null)
                    {
                        res.ResponseMessage = "Please upload proper excel file";
                        res.TotalSuccessRecords = 0;
                        res.TotalFailedRecords = 0;
                        res.ResponseStatus = "Failed";
                        return res;
                    }
                    var rowCount = worksheet.Dimension.Rows;

                    for (int row = 7; row <= rowCount; row++)
                    {
                        list.Add(new MarketSizingImportViewModel
                        {
                            Project = worksheet.Cells[row, 1].Value?.ToString().Trim(),
                            Market = worksheet.Cells[row, 2].Value?.ToString().Trim(),
                            Region = worksheet.Cells[row, 3].Value?.ToString().Trim(),
                            Country = worksheet.Cells[row, 4].Value?.ToString().Trim(),
                            Currency = worksheet.Cells[row, 5].Value?.ToString().Trim(),
                            Value = worksheet.Cells[row, 6].Value?.ToString().Trim(),
                            ASPUnit = worksheet.Cells[row, 7].Value?.ToString().Trim(),
                            VolumeUnit = worksheet.Cells[row, 8].Value?.ToString().Trim(),
                            //RawMaterial = worksheet.Cells[row, 9].Value?.ToString().Trim(),
                            //Component = worksheet.Cells[row, 10].Value?.ToString().Trim(),
                            //Processes = worksheet.Cells[row, 11].Value?.ToString().Trim(),
                            //CompanyProduct = worksheet.Cells[row, 12].Value?.ToString().Trim(),
                            //DeviceAndSolution = worksheet.Cells[row, 13].Value?.ToString().Trim(),
                            //Size = worksheet.Cells[row, 14].Value?.ToString().Trim(),
                            //Financing = worksheet.Cells[row, 15].Value?.ToString().Trim(),
                            //Logistics = worksheet.Cells[row, 16].Value?.ToString().Trim(),
                            //Distribution = worksheet.Cells[row, 17].Value?.ToString().Trim(),
                            //Units = worksheet.Cells[row, 18].Value?.ToString().Trim(),
                            //Dimensions = worksheet.Cells[row, 19].Value?.ToString().Trim(),
                            //CommunicationProtocol = worksheet.Cells[row, 20].Value?.ToString().Trim(),
                            //CommunicationTechnology = worksheet.Cells[row, 21].Value?.ToString().Trim(),
                            //PriceRange = worksheet.Cells[row, 22].Value?.ToString().Trim(),
                            //MountingType = worksheet.Cells[row, 23].Value?.ToString().Trim(),
                            //PowerSource = worksheet.Cells[row, 24].Value?.ToString().Trim(),
                            //Equiptment = worksheet.Cells[row, 25].Value?.ToString().Trim(),
                            //PartsandAccessories = worksheet.Cells[row, 26].Value?.ToString().Trim(),
                            //Rating = worksheet.Cells[row, 27].Value?.ToString().Trim(),
                            //Class = worksheet.Cells[row, 28].Value?.ToString().Trim(),
                            //From1 = worksheet.Cells[row, 29].Value?.ToString().Trim(),
                            //SalesChannels = worksheet.Cells[row, 30].Value?.ToString().Trim(),
                            //Technology = worksheet.Cells[row, 31].Value?.ToString().Trim(),
                            //Sensors = worksheet.Cells[row, 32].Value?.ToString().Trim(),
                            //Application = worksheet.Cells[row, 33].Value?.ToString().Trim(),
                            //EndUser = worksheet.Cells[row, 34].Value?.ToString().Trim(),

                            ASP_2018 = worksheet.Cells[row, 35].Value?.ToString().Trim(),
                            Value_2018 = worksheet.Cells[row, 36].Value?.ToString().Trim(),
                            Volume_2018 = worksheet.Cells[row, 37].Value?.ToString().Trim(),
                            OranicGrowth_2018 = worksheet.Cells[row, 38].Value?.ToString().Trim(),
                            TrendsGrowth_2018 = worksheet.Cells[row, 39].Value?.ToString().Trim(),
                            Source1_2018 = worksheet.Cells[row, 40].Value?.ToString().Trim(),
                            Source2_2018 = worksheet.Cells[row, 41].Value?.ToString().Trim(),
                            Source3_2018 = worksheet.Cells[row, 42].Value?.ToString().Trim(),
                            Source4_2018 = worksheet.Cells[row, 43].Value?.ToString().Trim(),
                            Source5_2018 = worksheet.Cells[row, 44].Value?.ToString().Trim(),
                            Rationale_2018 = worksheet.Cells[row, 45].Value?.ToString().Trim(),

                            ASP_2019 = worksheet.Cells[row, 46].Value?.ToString().Trim(),
                            Value_2019 = worksheet.Cells[row, 47].Value?.ToString().Trim(),
                            Volume_2019 = worksheet.Cells[row, 48].Value?.ToString().Trim(),
                            OranicGrowth_2019 = worksheet.Cells[row, 49].Value?.ToString().Trim(),
                            TrendsGrowth_2019 = worksheet.Cells[row, 50].Value?.ToString().Trim(),
                            Source1_2019 = worksheet.Cells[row, 51].Value?.ToString().Trim(),
                            Source2_2019 = worksheet.Cells[row, 52].Value?.ToString().Trim(),
                            Source3_2019 = worksheet.Cells[row, 53].Value?.ToString().Trim(),
                            Source4_2019 = worksheet.Cells[row, 54].Value?.ToString().Trim(),
                            Source5_2019 = worksheet.Cells[row, 55].Value?.ToString().Trim(),
                            Rationale_2019 = worksheet.Cells[row, 56].Value?.ToString().Trim(),

                            ASP_2020 = worksheet.Cells[row, 57].Value?.ToString().Trim(),
                            Value_2020 = worksheet.Cells[row, 58].Value?.ToString().Trim(),
                            Volume_2020 = worksheet.Cells[row, 59].Value?.ToString().Trim(),
                            OranicGrowth_2020 = worksheet.Cells[row, 60].Value?.ToString().Trim(),
                            TrendsGrowth_2020 = worksheet.Cells[row, 61].Value?.ToString().Trim(),
                            Source1_2020 = worksheet.Cells[row, 62].Value?.ToString().Trim(),
                            Source2_2020 = worksheet.Cells[row, 63].Value?.ToString().Trim(),
                            Source3_2020 = worksheet.Cells[row, 64].Value?.ToString().Trim(),
                            Source4_2020 = worksheet.Cells[row, 65].Value?.ToString().Trim(),
                            Source5_2020 = worksheet.Cells[row, 66].Value?.ToString().Trim(),
                            Rationale_2020 = worksheet.Cells[row, 67].Value?.ToString().Trim(),

                            ASP_2021 = worksheet.Cells[row, 68].Value?.ToString().Trim(),
                            Value_2021 = worksheet.Cells[row, 69].Value?.ToString().Trim(),
                            Volume_2021 = worksheet.Cells[row, 70].Value?.ToString().Trim(),
                            OranicGrowth_2021 = worksheet.Cells[row, 71].Value?.ToString().Trim(),
                            TrendsGrowth_2021 = worksheet.Cells[row, 72].Value?.ToString().Trim(),
                            Source1_2021 = worksheet.Cells[row, 73].Value?.ToString().Trim(),
                            Source2_2021 = worksheet.Cells[row, 74].Value?.ToString().Trim(),
                            Source3_2021 = worksheet.Cells[row, 75].Value?.ToString().Trim(),
                            Source4_2021 = worksheet.Cells[row, 76].Value?.ToString().Trim(),
                            Source5_2021 = worksheet.Cells[row, 77].Value?.ToString().Trim(),
                            Rationale_2021 = worksheet.Cells[row, 78].Value?.ToString().Trim(),

                            ASP_2022 = worksheet.Cells[row, 79].Value?.ToString().Trim(),
                            Value_2022 = worksheet.Cells[row, 80].Value?.ToString().Trim(),
                            Volume_2022 = worksheet.Cells[row, 81].Value?.ToString().Trim(),
                            OranicGrowth_2022 = worksheet.Cells[row, 82].Value?.ToString().Trim(),
                            TrendsGrowth_2022 = worksheet.Cells[row, 83].Value?.ToString().Trim(),
                            Source1_2022 = worksheet.Cells[row, 84].Value?.ToString().Trim(),
                            Source2_2022 = worksheet.Cells[row, 85].Value?.ToString().Trim(),
                            Source3_2022 = worksheet.Cells[row, 86].Value?.ToString().Trim(),
                            Source4_2022 = worksheet.Cells[row, 87].Value?.ToString().Trim(),
                            Source5_2022 = worksheet.Cells[row, 88].Value?.ToString().Trim(),
                            Rationale_2022 = worksheet.Cells[row, 89].Value?.ToString().Trim(),

                            ASP_2023 = worksheet.Cells[row, 90].Value?.ToString().Trim(),
                            Value_2023 = worksheet.Cells[row, 91].Value?.ToString().Trim(),
                            Volume_2023 = worksheet.Cells[row, 92].Value?.ToString().Trim(),
                            OranicGrowth_2023 = worksheet.Cells[row, 93].Value?.ToString().Trim(),
                            TrendsGrowth_2023 = worksheet.Cells[row, 94].Value?.ToString().Trim(),
                            Source1_2023 = worksheet.Cells[row, 95].Value?.ToString().Trim(),
                            Source2_2023 = worksheet.Cells[row, 96].Value?.ToString().Trim(),
                            Source3_2023 = worksheet.Cells[row, 97].Value?.ToString().Trim(),
                            Source4_2023 = worksheet.Cells[row, 98].Value?.ToString().Trim(),
                            Source5_2023 = worksheet.Cells[row, 99].Value?.ToString().Trim(),
                            Rationale_2023 = worksheet.Cells[row, 100].Value?.ToString().Trim(),

                            ASP_2024 = worksheet.Cells[row, 101].Value?.ToString().Trim(),
                            Value_2024 = worksheet.Cells[row, 102].Value?.ToString().Trim(),
                            Volume_2024 = worksheet.Cells[row, 103].Value?.ToString().Trim(),
                            OranicGrowth_2024 = worksheet.Cells[row, 104].Value?.ToString().Trim(),
                            TrendsGrowth_2024 = worksheet.Cells[row, 105].Value?.ToString().Trim(),
                            Source1_2024 = worksheet.Cells[row, 106].Value?.ToString().Trim(),
                            Source2_2024 = worksheet.Cells[row, 107].Value?.ToString().Trim(),
                            Source3_2024 = worksheet.Cells[row, 108].Value?.ToString().Trim(),
                            Source4_2024 = worksheet.Cells[row, 109].Value?.ToString().Trim(),
                            Source5_2024 = worksheet.Cells[row, 110].Value?.ToString().Trim(),
                            Rationale_2024 = worksheet.Cells[row, 111].Value?.ToString().Trim(),

                            ASP_2025 = worksheet.Cells[row, 112].Value?.ToString().Trim(),
                            Value_2025 = worksheet.Cells[row, 113].Value?.ToString().Trim(),
                            Volume_2025 = worksheet.Cells[row, 114].Value?.ToString().Trim(),
                            OranicGrowth_2025 = worksheet.Cells[row, 115].Value?.ToString().Trim(),
                            TrendsGrowth_2025 = worksheet.Cells[row, 116].Value?.ToString().Trim(),
                            Source1_2025 = worksheet.Cells[row, 117].Value?.ToString().Trim(),
                            Source2_2025 = worksheet.Cells[row, 118].Value?.ToString().Trim(),
                            Source3_2025 = worksheet.Cells[row, 119].Value?.ToString().Trim(),
                            Source4_2025 = worksheet.Cells[row, 120].Value?.ToString().Trim(),
                            Source5_2025 = worksheet.Cells[row, 121].Value?.ToString().Trim(),
                            Rationale_2025 = worksheet.Cells[row, 122].Value?.ToString().Trim(),

                            ASP_2026 = worksheet.Cells[row, 123].Value?.ToString().Trim(),
                            Value_2026 = worksheet.Cells[row, 124].Value?.ToString().Trim(),
                            Volume_2026 = worksheet.Cells[row, 125].Value?.ToString().Trim(),
                            OranicGrowth_2026 = worksheet.Cells[row, 126].Value?.ToString().Trim(),
                            TrendsGrowth_2026 = worksheet.Cells[row, 127].Value?.ToString().Trim(),
                            Source1_2026 = worksheet.Cells[row, 128].Value?.ToString().Trim(),
                            Source2_2026 = worksheet.Cells[row, 129].Value?.ToString().Trim(),
                            Source3_2026 = worksheet.Cells[row, 130].Value?.ToString().Trim(),
                            Source4_2026 = worksheet.Cells[row, 131].Value?.ToString().Trim(),
                            Source5_2026 = worksheet.Cells[row, 132].Value?.ToString().Trim(),
                            Rationale_2026 = worksheet.Cells[row, 133].Value?.ToString().Trim(),

                            ASP_2027 = worksheet.Cells[row, 134].Value?.ToString().Trim(),
                            Value_2027 = worksheet.Cells[row, 135].Value?.ToString().Trim(),
                            Volume_2027 = worksheet.Cells[row, 136].Value?.ToString().Trim(),
                            OranicGrowth_2027 = worksheet.Cells[row, 137].Value?.ToString().Trim(),
                            TrendsGrowth_2027 = worksheet.Cells[row, 138].Value?.ToString().Trim(),
                            Source1_2027 = worksheet.Cells[row, 139].Value?.ToString().Trim(),
                            Source2_2027 = worksheet.Cells[row, 140].Value?.ToString().Trim(),
                            Source3_2027 = worksheet.Cells[row, 141].Value?.ToString().Trim(),
                            Source4_2027 = worksheet.Cells[row, 142].Value?.ToString().Trim(),
                            Source5_2027 = worksheet.Cells[row, 143].Value?.ToString().Trim(),
                            Rationale_2027 = worksheet.Cells[row, 144].Value?.ToString().Trim(),

                            ASP_2028 = worksheet.Cells[row, 145].Value?.ToString().Trim(),
                            Value_2028 = worksheet.Cells[row, 146].Value?.ToString().Trim(),
                            Volume_2028 = worksheet.Cells[row, 147].Value?.ToString().Trim(),
                            OranicGrowth_2028 = worksheet.Cells[row, 148].Value?.ToString().Trim(),
                            TrendsGrowth_2028 = worksheet.Cells[row, 149].Value?.ToString().Trim(),
                            Source1_2028 = worksheet.Cells[row, 150].Value?.ToString().Trim(),
                            Source2_2028 = worksheet.Cells[row, 151].Value?.ToString().Trim(),
                            Source3_2028 = worksheet.Cells[row, 152].Value?.ToString().Trim(),
                            Source4_2028 = worksheet.Cells[row, 153].Value?.ToString().Trim(),
                            Source5_2028 = worksheet.Cells[row, 154].Value?.ToString().Trim(),
                            Rationale_2028 = worksheet.Cells[row, 155].Value?.ToString().Trim(),

                            CAGR_3Years = worksheet.Cells[row, 156].Value?.ToString().Trim(),
                            CAGR_5Years = worksheet.Cells[row, 157].Value?.ToString().Trim(),
                            Total_CAGR = worksheet.Cells[row, 158].Value?.ToString().Trim()
                        });
                    }
                }
            }

            //--------Call api to post data
            //list
            inputModel.MarketSizingsImportViewModel = list;
            inputModel.TemplateName = "MarketSizing Import Template";
            inputModel.ImportFileName = formFile.FileName;
            try
            {
                HttpResponseMessage response = _serviceRepository.PostResponse("MarketSizingImportExport", inputModel);
                response.EnsureSuccessStatusCode();
                ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;

                res.ResponseStatus = viewModel.IsSuccess ? "Success" : "Failed";
                res.ResponseMessage = viewModel.Message;
                //res.TotalRecords = inputModel.MarketSizingsImportViewModel.Count;
                //res.TotalSuccessRecords = 80;
                //res.TotalFailedRecords = 20;
                //res.GetMarketSizeErrorList = viewModel.Data;
                res.TotalRecords = inputModel.MarketSizingsImportViewModel.Count();
                if (viewModel.Data == null)
                {
                    res.TotalSuccessRecords = (inputModel.MarketSizingsImportViewModel.Count());
                    res.TotalFailedRecords = 0;
                }
                else
                {
                    res.TotalSuccessRecords = (inputModel.MarketSizingsImportViewModel.Count()) - (viewModel.Data.Count());
                    res.TotalFailedRecords = viewModel.Data.Count();
                }
                res.GetMarketSizeErrorList = viewModel.Data;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }
        private static byte[] GetBytes(ExcelPackage file)
        {
            using (var stream = new MemoryStream())
            {
                file.SaveAs(stream);
                return stream.ToArray();
            }
        }
    }

    //internal class ExcelFile
    //{
    //}
}
