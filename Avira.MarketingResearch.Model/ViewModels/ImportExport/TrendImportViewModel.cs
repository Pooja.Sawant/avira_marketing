﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.ImportExport
{
    public class TrendImportViewModel
    {
        public int RowNo { get; set; }
        public string Project { get; set; }
        public string TrendName { get; set; }
        public string Description { get; set; }
        public string TrendStatus { get; set; }
        public string Importance { get; set; }
        public string ImpactDirection { get; set; }
        public string TrendValue { get; set; }
        public string Category { get; set; }
        public string CompanyGroup { get; set; }
        public string CompanyGroupImpact { get; set; }
        public string CompanyGroupEndUser { get; set; }
        public string Company { get; set; }
        public string Country { get; set; }
        public string CountryImpact { get; set; }
        public string CountryEndUser { get; set; }
        public string EcoSystem { get; set; }
        public string EcoSystemSubSegment { get; set; }
        public string EcoSystemImpact { get; set; }
        public string EcoSystemEndUser { get; set; }
        public string TrendKeyWord { get; set; }
        public string Industry { get; set; }
        public string IndustryImpact { get; set; }
        public string IndustryEndUser { get; set; }
        public string Market { get; set; }
        public string Region { get; set; }
        public string RegionImpact { get; set; }
        public string RegionEndUser { get; set; }
        public string MarketImpact { get; set; }
        public string MarketEndUser { get; set; }
        public string Project1 { get; set; }
        public string ProjectImpact { get; set; }
        public string ProjectEndUser { get; set; }
        public string StrengthtType { get; set; }
        public string StrengthDescription { get; set; }
        public string WeaknessType { get; set; }
        public string WeaknessDescription { get; set; }
        public string OpportunityType { get; set; }
        public string OpportunityDescription { get; set; }
        public string ThreatType { get; set; }
        public string ThreatDescription { get; set; }
        public string TimeTag { get; set; }
        public string TimeTagImpact { get; set; }
        public string TimeTagEndUser { get; set; }
        public string ValueConversion { get; set; }
        public string Rationale_2018 { get; set; }
        public string Value_2018 { get; set; }
        public string Rationale_2019 { get; set; }
        public string Value_2019 { get; set; }
        public string Rationale_2020 { get; set; }
        public string Value_2020 { get; set; }
        public string Rationale_2021 { get; set; }
        public string Value_2021 { get; set; }
        public string Rationale_2022 { get; set; }
        public string Value_2022 { get; set; }
        public string Rationale_2023 { get; set; }
        public string Value_2023 { get; set; }
        public string Rationale_2024 { get; set; }
        public string Value_2024 { get; set; }
        public string Rationale_2025 { get; set; }
        public string Value_2025 { get; set; }
        public string Rationale_2026 { get; set; }
        public string Value_2026 { get; set; }
        public string Rationale_2027 { get; set; }
        public string Value_2027 { get; set; }
        public string Rationale_2028 { get; set; }
        public string Value_2028 { get; set; }
        public string Rationale_2029 { get; set; }
        public string Value_2029 { get; set; }
        public string Rationale_2030 { get; set; }
        public string Value_2030 { get; set; }
        public string Rationale_2031 { get; set; }
        public string Value_2031 { get; set; }
        public string Rationale_2032 { get; set; }
        public string Value_2032 { get; set; }
        public string CompanyGroup_ApproverRemark { get; set; }
        public string CompanyGroup_AuthorRemark { get; set; }
        public string CompanyGroup_TrendStatus { get; set; }
        public string EcoSystem_ApproverRemark { get; set; }
        public string EcoSystem_AuthorRemark { get; set; }
        public string EcoSystem_TrendStatus { get; set; }
        public string Industry_ApproverRemark { get; set; }
        public string Industry_AuthorRemark { get; set; }
        public string Industry_TrendStatus { get; set; }
        public string KeyCompany_ApproverRemark { get; set; }
        public string KeyCompany_AuthorRemark { get; set; }
        public string KeyCompany_TrendStatus { get; set; }
        public string keyword_ApproverRemark { get; set; }
        public string keyword_AuthorRemark { get; set; }
        public string keyword_TrendStatus { get; set; }
        public string Market_ApproverRemark { get; set; }
        public string Market_AuthorRemark { get; set; }
        public string Market_TrendStatus { get; set; }
        public string Project_ApproverRemark { get; set; }
        public string Project_AuthorRemark { get; set; }
        public string Project_TrendStatus { get; set; }
        public string Region_ApproverRemark { get; set; }
        public string Region_AuthorRemark { get; set; }
        public string Region_TrendStatus { get; set; }
        public string TimeTag_ApproverRemark { get; set; }
        public string TimeTag_AuthorRemark { get; set; }
        public string TimeTag_TrendStatus { get; set; }
        public string Value_ApproverRemark { get; set; }
        public string Value_AuthorRemark { get; set; }
        public string Value_TrendStatus { get; set; }

        public string Trend_ApproverRemark { get; set; }
        public string Trend_AuthorRemark { get; set; }

        public string Message { get; set; }
        public bool Success { get; set; }
        public string ErrorNotes { get; set; }
        //public string ErrorNotesAuthorRemark { get; set; }
        //public string ErrorNotesCompanyGroup { get; set; }
        //public string ErrorNotesEcoSystem { get; set; }
        //public string ErrorNotesIndustry { get; set; }
        //public string ErrorNotesKeyCompany { get; set; }
        //public string ErrorNotesKeyword { get; set; }
        //public string ErrorNotesMarket { get; set; }
        //public string ErrorNotesProject { get; set; }
        //public string ErrorNotesProject1 { get; set; }
        //public string ErrorNotesRegion { get; set; }
        //public string ErrorNotesTimeTag { get; set; }
        //public string ErrorNotesTrend { get; set; }
        //public string ErrorNotesValueConversion { get; set; }
    }

    public class ProjectKeyWordImportViewModel
    {
        public int RowNo { get; set; }
        public string Project { get; set; }
        public string KeyWord { get; set; }
        public string Message { get; set; }
        public bool Success { get; set; }
        public string ErrorNotes { get; set; }
    }
    public class TrendImportInsertViewModel
    {
        public string TemplateName { get; set; }
        public string ImportFileName { get; set; }
        public string TrendJson { get; set; }
        public string ProjectKeywordJson { get; set; }
        public Guid UserCreatedById { get; set; }
        public List<TrendImportViewModel> TrendsImportViewModel { get; set; }
        public List<ProjectKeyWordImportViewModel> ProjectKeyWordImportViewModel { get; set; }
    }
}