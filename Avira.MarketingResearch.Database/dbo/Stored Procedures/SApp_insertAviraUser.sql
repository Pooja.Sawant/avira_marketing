﻿
  
CREATE procedure [dbo].[SApp_insertAviraUser]  
(@AviraUserID uniqueidentifier,  
@FirstName nvarchar(128),  
@LastName nvarchar(128),  
@UserName nvarchar(100),  
@DisplayName nvarchar(256),  
@EmailAddress nvarchar(256),  
@PhoneNumber nvarchar(128),  
@UserTypeId tinyint,  
@PassWord nvarchar(1000),  
@CustomerId uniqueidentifier,  
@IsEverLoggedIn bit,  
@LocationID uniqueidentifier,  
@Salt nvarchar(256),  
@UserCreatedById uniqueidentifier,  
@CreatedOn Datetime,  
@IsDeleted bit,
@DateofJoining Datetime)  
as  
/*================================================================================  
Procedure Name: SApp_insertAviraUser  
Author: Adarsh  
Create date: 03/16/2019  
Description: Insert a record into AviraUser table  
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
03/16/2019 Adarsh   Initial Version  
================================================================================*/  
BEGIN  
SET NOCOUNT ON;  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
declare @ErrorMsg NVARCHAR(2048);  
  

--Validations
IF CHARINDEX('@', @UserName) > 0 AND NOT EXISTS(SELECT value FROM STRING_SPLIT('inforgrowth.com,12thwonder.com', ',') WHERE value = REPLACE(@UserName, LEFT(@UserName, CHARINDEX('@', @UserName)),'' ) ) 
begin  
set @ErrorMsg = 'You can create user only with inforgrowth.com and 12thwonder.com domain for user name '  
SELECT StoredProcedureName ='SApp_insertAviraUser',Message =@ErrorMsg,Success = 0;    
RETURN;    
end

IF CHARINDEX('@', @EmailAddress) > 0 AND NOT EXISTS(SELECT value FROM STRING_SPLIT('inforgrowth.com,12thwonder.com', ',') WHERE value = REPLACE(@EmailAddress, LEFT(@EmailAddress, CHARINDEX('@', @EmailAddress)),'' ) ) 
begin  
set @ErrorMsg = 'You can create user only with inforgrowth.com and 12thwonder.com domain for email id '  
SELECT StoredProcedureName ='SApp_insertAviraUser',Message =@ErrorMsg,Success = 0;    
RETURN;    
end
  
IF exists(select 1 from [dbo].[AviraUser]   
   where EmailAddress = @EmailAddress AND IsDeleted = 0)  
begin  
set @ErrorMsg = 'User with email "'+ @EmailAddress + '" already exists.';  
SELECT StoredProcedureName ='SApp_insertAviraUser',Message =@ErrorMsg,Success = 0;    
RETURN;    
end 
IF exists(select 1 from [dbo].[AviraUser]   
   where UserName = @UserName AND IsDeleted = 0)  
begin  
set @ErrorMsg = 'User Name "'+ @UserName + '" already exists.';  
SELECT StoredProcedureName ='SApp_insertAviraUser',Message =@ErrorMsg,Success = 0;    
RETURN;    
end  
--End of Validations  
BEGIN TRY  
  
INSERT INTO [dbo].[AviraUser]  
           ([Id]  
           ,[FirstName]  
           ,[LastName]  
           ,[UserName]  
           ,[DisplayName]  
           ,[EmailAddress]  
           ,[PhoneNumber]  
           ,[UserTypeId]  
           ,[PassWord]  
           ,[CustomerId]  
           ,[IsEverLoggedIn]  
           ,[LocationID]  
           ,[Salt]  
           ,[CreatedOn]  
           ,[UserCreatedById]  
           ,[IsDeleted]--  
		   ,[DateofJoining])
     VALUES  
           (@AviraUserID,  
     @FirstName,  
     @LastName,  
           @UserName,  
     @DisplayName,  
     @EmailAddress,  
     @PhoneNumber,  
     @UserTypeId,  
     @PassWord,  
     @CustomerId,  
     0,  
     @LocationID,  
     @Salt,  
     @CreatedOn,  
     @UserCreatedById,  
     0,
	 @DateofJoining)  
   
--select @AviraUserID;  
--return(0)  
SELECT StoredProcedureName ='SApp_insertAviraUser',Message = @ErrorMsg,Success = 1;  
END TRY  
BEGIN CATCH  
    -- Execute the error retrieval routine.  
 DECLARE @ErrorNumber int;  
 DECLARE @ErrorSeverity int;  
 DECLARE @ErrorProcedure varchar(100);  
 DECLARE @ErrorLine int;  
 DECLARE @ErrorMessage varchar(500);  
  
  SELECT @ErrorNumber = ERROR_NUMBER(),  
        @ErrorSeverity = ERROR_SEVERITY(),  
        @ErrorProcedure = ERROR_PROCEDURE(),  
        @ErrorLine = ERROR_LINE(),  
        @ErrorMessage = ERROR_MESSAGE()  
  
 insert into dbo.Errorlog(ErrorNumber,  
       ErrorSeverity,  
       ErrorProcedure,  
       ErrorLine,  
       ErrorMessage,  
       ErrorDate)  
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,@CreatedOn);  
  
 set @ErrorMsg = 'Error while creating a new AviraUser, please contact Admin for more details.';  
  
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;  
  
END CATCH  
  
  
end