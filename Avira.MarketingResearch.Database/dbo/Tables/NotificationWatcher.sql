﻿CREATE TABLE [dbo].[NotificationWatcher] (
    [NotificationWatcherID] UNIQUEIDENTIFIER NOT NULL,
    [ActionName]            VARCHAR (1000)   NOT NULL,
    [IsBroadCosted]         BIT              CONSTRAINT [DF_NotificationWatcher_IsPublic] DEFAULT ((0)) NOT NULL,
    [AssignedById]          UNIQUEIDENTIFIER NULL,
    [AssignedToId]          UNIQUEIDENTIFIER NULL,
    [Description]           NVARCHAR (500)   NULL,
    [LowId]                 UNIQUEIDENTIFIER NULL,
    [HighId]                UNIQUEIDENTIFIER NULL,
    [CreatedOn]             DATETIME         CONSTRAINT [DF_NotificationWatcher_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [UserCreatedById]       UNIQUEIDENTIFIER NULL,
    [ModifiedOn]            DATETIME         NULL,
    [UserModifiedById]      UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_NotificationWatcher] PRIMARY KEY CLUSTERED ([NotificationWatcherID] ASC),
    CONSTRAINT [FK_NotificationWatcher_AviraUser] FOREIGN KEY ([AssignedToId]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_NotificationWatcher_AviraUser1] FOREIGN KEY ([AssignedById]) REFERENCES [dbo].[AviraUser] ([Id])
);

