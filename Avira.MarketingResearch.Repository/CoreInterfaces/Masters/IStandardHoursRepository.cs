﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.Helpers;
using System.Collections.Generic;
using System;
using System.Data;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.Masters
{
    public interface IStandardHoursRepository
    {
        StandardHoursViewModel GetById(Guid guid);
        IEnumerable<StandardHoursViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted);
        Result<SpTransactionMessage> Update(StandardHoursUpdateDbViewModel viewModel);
    }
}
