﻿CREATE PROCEDURE [dbo].[SFab_GetDecisionAlternative]    
(  
	@DecisionId UNIQUEIDENTIFIER,
	@DecisionAlternativeId UNIQUEIDENTIFIER,
	@AviraUserId UNIQUEIDENTIFIER
)
As    

/*================================================================================    
Procedure Name: SFab_GetDecisionAlternative   
Author: Praveen    
Create date: 07-Jan-2020
Description: To return Decision Alternative by DecisionAlternativeId of the user
Change History    
Date  Developer  Reason  
__________ ____________ ______________________________________________________    
07-Jan-2020  Praveen   Initial Version    
================================================================================*/    
BEGIN    
------------------

	--Required Tables: Decision, DecisionAlternative,LookupStatus and LookupCategory
	--Notes: Get alternative by tbe decisionId of the user and validate records by userId
	--Expected Columns : DecisionAlternative.Id, Decision.Description, Decision.ResourceAssigned, Decision.DecisionDeadine, DecisionAlternative.Status, DecisionAlternative.Description, DecisionAlternative.Priority, DecisionAlternative.ResourceAssigned 
	SELECT ''
		
END