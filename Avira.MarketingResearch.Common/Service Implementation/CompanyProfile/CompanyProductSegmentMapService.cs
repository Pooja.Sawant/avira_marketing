﻿using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.CompanyProfile
{
    public class CompanyProductSegmentMapService : ICompanyProductSegmentMapService
    {
        private readonly ICompanyProductSegmentMapRepository _companyProductSegmentMapRepository;

        public CompanyProductSegmentMapService(ICompanyProductSegmentMapRepository companyProductSegmentMapRepository)
        {
            _companyProductSegmentMapRepository = companyProductSegmentMapRepository;
        }
        public IEnumerable<CompanyProductSegmentMapViewModel> GetAll(Guid? Id)
        {
            return _companyProductSegmentMapRepository.GetAll(Id);
        }

        public Result<SpTransactionMessage> InsertUpdate(CompanyProductViewMoel tfc, AviraUser currenUser)
        {
            var res = _companyProductSegmentMapRepository.InsertUpdate(tfc);
            return res;
        }
    }
}
