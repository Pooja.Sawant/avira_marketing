﻿

CREATE PROCEDURE [dbo].[SFab_GetAllCountryByRegionId]
(@RegionIdList dbo.[udtUniqueIdentifier] readonly,
@UserId uniqueidentifier =  null)
	AS
/*================================================================================
Procedure Name: [dbo].[SFab_GetAllCountryByRegionId]
Author: Pooja Sawant
Create date: 07/01/2019
Description: Get list from country table by RegionId
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
07/01/2019	Pooja S			Initial Version
================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SELECT  Country.Id, Country.CountryName, Country.CountryUNCode as CountryCode
FROM Country 
where Country.IsDeleted=0
AND (
exists (select 1 from @RegionIdList Rlist
			INNER JOIN CountryRegionMap 
			on Rlist.ID = CountryRegionMap.RegionId
			where Country.Id = CountryRegionMap.CountryId
			and CountryRegionMap.IsDeleted=0) 
or not exists (select 1 from @RegionIdList)) 
order by Country.CountryName asc
END