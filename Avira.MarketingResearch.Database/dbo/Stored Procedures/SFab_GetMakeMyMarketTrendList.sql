﻿CREATE PROCEDURE [dbo].[SFab_GetMakeMyMarketTrendList]
AS
BEGIN
Declare @ProjectId uniqueidentifier;
Set @ProjectId = 'B1466961-AB58-4855-ABCB-12E92F392DC6'

;With cteTimeTags as (
Select ROW_NUMBER() over(partition by T.id order by TT.seq desc) as rnum,
T.ID as TrendId,
TT.Id as TimeTagId,
TT.TimeTagName
from Trend T
inner join TrendTimeTag TTT
on T.Id = TTT.TrendId
Inner Join TimeTag TT
On TT.Id = TTT.TimeTagId
Where TTT.Isdeleted = 0
and T.ProjectID = @ProjectId
and TT.TimeTagName in('ShortTerm','LongTerm','MidTerm')
)

Select P.Id as ProjectId,
		P.ProjectName,
	   t.Id as  TrendId,
       t.TrendName,
	   t.ImportanceId,
	   imp.ImportanceName,
	   t.ImpactDirectionId,
	   IDir.ImpactDirectionName as Nature,
	   TT.TimeTagId,
	   TT.TimeTagName as Outlook,
	   (select string_agg(Company.CompanyName, ',') from Company 
		where exists(select 1 from TrendKeyCompanyMap TCM
					where Company.Id = TCM.CompanyId
					and TCM.TrendId = T.Id)) as CompanyName,
		(select string_agg(Segment.SegmentName, ',') from Segment 
		where exists(select 1 from TrendEcoSystemMap TEM
					where Segment.Id = TEM.EcoSystemId
					and TEM.TrendId = T.Id)) as EcoSystemName,
	   (select string_agg(Industry.IndustryName, ',') from Industry 
		where exists(select 1 from TrendIndustryMap TIM
					where Industry.Id = TIM.IndustryId
					and TIM.TrendId = T.Id)) as IndustryName,
	   t.TrendValue as IndustryViewPoint
from Trend T
inner join Project P
on T.ProjectID = p.Id
inner join Importance imp
on t.ImportanceId = imp.Id
inner join ImpactDirection IDir
on T.ImpactDirectionId = IDir.Id
inner join cteTimeTags TT
on TT.TrendId = T.Id
and tt.rnum = 1
	
END