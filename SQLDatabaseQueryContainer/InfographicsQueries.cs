﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public static class InfographicsQueries
    {
        public static string StoreProcInsertUpdate = "SApp_insertUpdateInfographics";
        public static string StoreProcInsertDelete = "SApp_GetInfographicsDelete";
        public static string StoreProcSelectAll = "SApp_InfographicsGridWithFilters";
    }
}
