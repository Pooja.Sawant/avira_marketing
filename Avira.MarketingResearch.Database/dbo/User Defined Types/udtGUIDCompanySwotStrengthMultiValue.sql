﻿CREATE TYPE [dbo].[udtGUIDCompanySwotStrengthMultiValue] AS TABLE (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [CompanyId]        UNIQUEIDENTIFIER NULL,
    [StrenghtCategory] UNIQUEIDENTIFIER NULL,
    [Description]      NVARCHAR (512)   NULL);

