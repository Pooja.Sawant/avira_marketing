﻿  
  
CREATE PROCEDURE [dbo].[SApp_GetAllTrendNames]  
 (  
  @includeDeleted bit = 0  
 )  
 AS  
/*================================================================================  
Procedure Name: [SApp_GetAllTrendNames]  
Author: Gopi  
Create date: 04/17/2019  
Description: Get list of TrendNames from table   
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
04/17/2019 Gopi   Initial Version  
================================================================================*/  
  
BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
  SELECT [Trend].[Id],        
  [Trend].[TrendName],       
  [Trend].[IsDeleted]
  FROM [dbo].[Trend] 
  WHERE ([Trend].IsDeleted = 0)

 END