﻿
CREATE procedure [dbo].[SFab_GetCompanyComparisonFundamentalDetailsbyProjectID]
(
@SegmentID uniqueidentifier=null,
@SegmentList dbo.[udtUniqueIdentifier] readonly,
--@RegionId uniqueidentifier = null,
@RegionIdList dbo.[udtUniqueIdentifier] readonly,
@CountryIdList dbo.[udtUniqueIdentifier] readonly,
--@CompanystageId UniqueIdentifier=null,
@CompanystageId [dbo].[udtUniqueIdentifier] READONLY ,
@StratergyList [dbo].[udtUniqueIdentifier] READONLY,
@Years nvarchar(100),
@ProjectId uniqueidentifier,
@CompanyIdList dbo.[udtUniqueIdentifier] readonly,
@AviraUserID uniqueidentifier = null
)
as
/*================================================================================
Procedure Name: [SFab_GetCompanyComparisonFundamentalDetailsbyProjectID]
Author: Harshal
Create date: 07/29/2019
Description: Get list from company table by project ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
07/29/2019	Harshal	    Initial Version
10/11/2019  Pooja        changes in sp column
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ProjectApprovedStatusId uniqueidentifier
select @ProjectApprovedStatusId = ID from ProjectStatus
where ProjectStatusName = 'Completed';

declare @RegionList dbo.[udtUniqueIdentifier];
declare @YearList table (Year int);
insert into @YearList
select value
from string_split(@Years,',')

insert into @RegionList
select Id
from Region
where id in(select ID from @RegionIdList)
or exists (select 1
			from CountryRegionMap CRM
			inner join @CountryIdList Loc
			on CRM.CountryId = Loc.ID
			and Region.id = CRM.RegionId)
			;

--Fundamentals
	 select comp.Id as CompanyId,
	 comp.CompanyName,
	 comp.CompanyCode,
	 comp.DisplayName,
	 comp.Description,
	 comp.NatureId,
	 Nature.NatureTypeName as NatureTypeName,
	 STUFF((SELECT top 5 '|'+ CompanyP.CompanyName  FROM Company CompanyP  
      WHERE CompanyP.ParentId=comp.Id  ORDER BY CompanyP.CompanyName   
   FOR XML PATH('')), 1, 1, '') AS CompanySubsidiaryName,
	 comp.ParentId,
	 comp.ParentCompanyName,
	 comp.CompanyLocation,
	case when  comp.IsHeadQuarters=1 then comp.CompanyLocation else '' end AS CompanyHeadQuarters,
	 comp.YearOfEstb,
	 comp.CompanyStageId,
	 CompanyStage.CompanyStageName,
	 STUFF((SELECT ', '+ Region.RegionName  FROM CompanyRegionMap  
   INNER JOIN Region ON Region.Id=CompanyRegionMap.RegionId  
      WHERE CompanyRegionMap.CompanyId=comp.Id  
   FOR XML PATH('')), 1, 1, '') As GeographicPresence,
	 Comp.NoOfEmployees,
	 Comp.RankNumber,
	 STUFF(
      COALESCE(', ' + RTRIM(Telephone),     '') 
    + COALESCE(', ' + RTRIM(Telephone2), '') 
    + COALESCE(', ' + RTRIM(Telephone3),  '')
    , 1, 2, '') as PhoneNo,
	STUFF(
      COALESCE(', ' + RTRIM(Email), '') 
    + COALESCE(', ' + RTRIM(Email2), '') 
    + COALESCE(', ' + RTRIM(Email3),  '')
    , 1, 2, '') as MailId,
	comp.Website,
	STUFF(( SELECT ', '+ CompanyKeyEmployeesMap.KeyEmployeeName +''+Designation.Designation  
		FROM CompanyKeyEmployeesMap 
		INNER JOIN Designation ON Designation.Id =CompanyKeyEmployeesMap.DesignationId
		WHERE CompanyKeyEmployeesMap.CompanyId=comp.Id  and IsDirector=1
		FOR XML PATH('')), 1, 1, '') As BoD,
	'' As CEO,
	'' AS CxO_2,
	'' AS CxO_3,
	STUFF(( SELECT distinct ', '+ Segment.SegmentName 
		FROM CompanyEcosystemPresenceESMMap 
		INNER JOIN Segment ON Segment.Id =CompanyEcosystemPresenceESMMap.ESMId
		WHERE CompanyEcosystemPresenceESMMap.CompanyId=comp.Id  
		FOR XML PATH('')), 1, 1, '') As ESM_Pre,
	'' AS LMNA,
	'' As LFUNDRaise,
	STUFF(( SELECT '|'+ CompanyEcosystemPresenceKeyCompetitersMap.KeyCompetiters 
		FROM CompanyEcosystemPresenceKeyCompetitersMap  
		WHERE CompanyEcosystemPresenceKeyCompetitersMap.CompanyId=comp.Id  
		FOR XML PATH('')), 1, 1, '') As KeyClients
	 --comp.ActualImageName as ImageActualName
	from Company comp
	inner join Nature
	on comp.NatureId = Nature.Id
	inner join CompanyStage
	on comp.CompanyStageId = CompanyStage.Id
	where comp.IsDeleted = 0 
	and exists (select 1 from CompanyProjectMap cpmap
				inner join project proj
				on cpmap.ProjectId = proj.ID
				where comp.Id = cpmap.CompanyId 
				and proj.ProjectStatusID = @ProjectApprovedStatusId
	and cpmap.ProjectId = @ProjectId)
	and (not exists(select 1 from @CompanyIdList) or comp.Id in (select ID from @CompanyIdList))
	and (not exists (select * from @RegionList) 
		or exists(select 1 from CompanyRegionMap CRM
				  inner join @RegionList Rlist
				  on CRM.RegionId = Rlist.ID))
	--and (@CompanystageId is null or comp.CompanyStageId = @CompanystageId)
	and (not exists (select * from @CompanystageId) 
		or exists(select 1 from [CompanyStage] CST
				inner join  @CompanystageId CSTAList
				on CST.Id = CSTAList.ID
				where comp.CompanyStageId = CSTAList.ID))
	and (not exists (select * from @StratergyList) 
		or exists(select 1 from [CompanyStrategyMapping] CSM
				inner join  @StratergyList CSlist
				on CSM.Category = CSlist.ID
				where comp.Id = CSM.CompanyId))
	--and (not exists(select 1 from @SegmentList)
	--	or exists(select 1 from CompanyProduct CP
	--				inner join CompanyProductSegmentMap CPSM
	--				on cp.Id=cpsm.CompanyProductId 
	--				inner join @SegmentList list
	--				on list.ID = CPSM.SegmentId
	--				where cp.CompanyId = comp.Id))
	and (not exists(select 1 from @SegmentList)
        or exists(SELECT 1 FROM CompanyEcosystemPresenceESMMap CPSM                   
                    inner join @SegmentList list
                    on list.ID = CPSM.ESMId
                    where CPSM.CompanyId = comp.Id))
	and (@Years is null or @Years = '' 
		or exists(select 1 from CompanyRevenue
					inner join @YearList Ylist
					on Ylist.Year = CompanyRevenue.Year
					and CompanyRevenue.CompanyId = comp.Id));

end