﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.ImportExport
{
    public class TrendImportExportRepository : ITrendImportExportRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "TrendImportExport";

        public TrendImportExportRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public List<ImportCommonResponseModel> Create(TrendImportInsertViewModel trendImportInsertViewModel)
        {
            var spTransactionMessage = new List<TrendImportViewModel>();
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);

             spTransactionMessage = dapper.Query<TrendImportViewModel>(ImportExportQueries.StoreProcInsert, new
            {
                @TemplateName = trendImportInsertViewModel.TemplateName,
                @ImportFileName = trendImportInsertViewModel.ImportFileName,
                @jsonTrend = trendImportInsertViewModel.TrendJson,
                @JsonProjectKeyword=trendImportInsertViewModel.ProjectKeywordJson,
                @UserCreatedById = trendImportInsertViewModel.UserCreatedById
            }, null, true, 300, CommandType.StoredProcedure) as List<TrendImportViewModel>;

            var spTransactionMessage1 = new List<ImportCommonResponseModel>();
            if (spTransactionMessage!=null)
            {
                //var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
                spTransactionMessage1 = dapper.Query<ImportCommonResponseModel>(ImportExportQueries.StoreProcUpdateTrend
                            , null
                            , null, true, 300, CommandType.StoredProcedure) as List<ImportCommonResponseModel>;

            }

            return spTransactionMessage1;
        }
    }
}
