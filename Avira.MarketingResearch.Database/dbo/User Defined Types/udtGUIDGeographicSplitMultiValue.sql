﻿CREATE TYPE [dbo].[udtGUIDGeographicSplitMultiValue] AS TABLE (
    [Id]          UNIQUEIDENTIFIER NOT NULL,
    [RegionId]    UNIQUEIDENTIFIER NOT NULL,
    [RegionValue] DECIMAL (19, 4)  NULL);

