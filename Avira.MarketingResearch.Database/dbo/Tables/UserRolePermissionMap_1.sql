﻿CREATE TABLE [dbo].[UserRolePermissionMap] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [UserRoleId]       UNIQUEIDENTIFIER NOT NULL,
    [PermissionId]     UNIQUEIDENTIFIER NOT NULL,
    [PermissionLevel]  TINYINT          NOT NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_UserRolePermissionMap_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_UserRolePermissionMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_UserRolePermissionMap_CreatedUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_UserRolePermissionMap_DeletedUser] FOREIGN KEY ([UserDeletedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_UserRolePermissionMap_ModifiedUser] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_UserRolePermissionMap_PermissionId] FOREIGN KEY ([PermissionId]) REFERENCES [dbo].[Permission] ([Id]),
    CONSTRAINT [FK_UserRolePermissionMap_UserRoleId] FOREIGN KEY ([UserRoleId]) REFERENCES [dbo].[UserRole] ([Id]),
    CONSTRAINT [IX_UserRolePermissionMap] UNIQUE NONCLUSTERED ([PermissionId] ASC, [UserRoleId] ASC)
);



