﻿using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.Trend
{
    public class TrendViewModel
    {
        public Guid Id { get; set; }
        public string TrendName { get; set; }
        public string Description { get; set; }
        public Guid ProjectID { get; set; }
        public string ProjectName { get; set; }
        public string ProjectCode { get; set; }
        public string ProjectAuthorName { get; set; }
        public string ProjectApproverName { get; set; }
        public string ProjectCoAuthorName { get; set; }
        public string ApproverRemark { get; set; }
        public string AuthorRemark { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid TrendStatusID { get; set; }
        public string TrendStatusName { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "Trends";
            }
        }
    }

    public class TrendInsertDbViewModel
    {
        public Guid Id { get; set; }
        [Required]
        [Display(Name = "Trend Name")]
        public string TrendName { get; set; }
        public string Description { get; set; }
        public string ApproverRemark { get; set; }
        [Required]
        [Display(Name = "Author Remark")]
        public string AuthorRemark { get; set; }
        public Guid ProjectID { get; set; }
        public Guid TrendStatusID { get; set; }
        public string TrendStatusName { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
    }

    public class TrendUpdateDbViewModel
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        [Display(Name = "Trend Name")]
        public string TrendName { get; set; }
        public string Description { get; set; }
        public string ApproverRemark { get; set; }
        [Required]
        [Display(Name = "Author Remark")]
        public string AuthorRemark { get; set; }
        public Guid ProjectID { get; set; }
        public Guid TrendStatusID { get; set; }
        public string TrendStatusName { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public List<TrendNoteViewModel> StatusList { get; set; }
    }
}
