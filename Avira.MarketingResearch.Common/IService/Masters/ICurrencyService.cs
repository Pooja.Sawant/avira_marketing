﻿using System;
using System.Collections.Generic;
using System.Text;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Model;
using Avira.MarketingResearch.Model.ViewModels.Masters;

namespace Avira.MarketingResearch.Common.IService.Masters
{
    public interface ICurrencyService
    {
        IEnumerable<CurrencyViewModel> GetAll(Guid Id);
        IEnumerable<CurrencyViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false);
        CurrencyViewModel GetById(Guid guid);
    }
}
