﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.QualitativeInfo;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.QualitativeInfo
{
    public interface IQualitativeNoteRepository
    {
        QualitativeNoteViewModel GetById(Guid Id, Guid projectId);
        Result<SpTransactionMessage> Insert(QualitativeNoteViewModel viewModel);
    }
}
