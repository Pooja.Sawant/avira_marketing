﻿using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.Utils;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace Avira.MarketingResearch.Web.Controllers
{
    [NoCache]
    [Authorization]
    public class TrendRegionMapController : BaseController
    {
        private readonly IServiceRepository _serviceRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;
        string contentRootPath = string.Empty;

        private ILoggerManager _logger;
        public TrendRegionMapController(IServiceRepository serviceRepository, ILoggerManager logger, IHostingEnvironment hostingEnvironment, IConfiguration configuration)
        {
            _hostingEnvironment = hostingEnvironment;
            _serviceRepository = serviceRepository;
            _logger = logger;
            _configuration = configuration;
            contentRootPath = _hostingEnvironment.ContentRootPath;
        }

        public ActionResult Index(string id)
        {
            SetRoleList();
            MainTrendRegionMapInsertUpdateDbViewModel model = new MainTrendRegionMapInsertUpdateDbViewModel();
            try
            {
                model.TrendId = new Guid(id); ;
                int PageStart = 0;
                int pageSize = Common.TrendPagingSize;
                int SortDirection = 1;

                //------Get master data for Impact           
                HttpResponseMessage responseImpactType = _serviceRepository.GetResponse("ImpactType");
                responseImpactType.EnsureSuccessStatusCode();
                ViewBag.ImpactList = responseImpactType.Content.ReadAsAsync<List<ImpactTypeViewModel>>().Result;

                HttpResponseMessage responseTrend = _serviceRepository.GetByIdResponse("Trend", id);
                responseTrend.EnsureSuccessStatusCode();
                TrendViewModel trend = responseTrend.Content.ReadAsAsync<TrendViewModel>().Result;
                model.TrendModel = trend;

                //------Get trend note by trend Id           
                HttpResponseMessage responseTrendNote = _serviceRepository.GetByIdResponse("TrendNote", id, "Region", "id");
                responseTrendNote.EnsureSuccessStatusCode();
                var tnm = responseTrendNote.Content.ReadAsAsync<TrendNoteViewModel>().Result;
                model.TrendNoteModel = tnm ?? new TrendNoteViewModel();

                model.TrendModel.TrendStatusName = model.TrendNoteModel.TrendStatusName;

                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.GetResponse(contollerName, PageStart, pageSize, SortDirection, string.Empty, id);
                response.EnsureSuccessStatusCode();
                List<TrendRegionMapViewModel> regionCountryMap = response.Content.ReadAsAsync<List<TrendRegionMapViewModel>>().Result;
                //model.TrendRegionMapInsertUpdate = regionCountryMap;

                //------Converting normal list to tile---------------
                model.TrendRegionMapInsertUpdateTile = Common.splitList(regionCountryMap, 2);
                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, model.TrendModel.ProjectID);
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                }
                ViewBag.Id = model.TrendId;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(model);
        }
        private void SetRoleList()
        {
            try
            {
                var roles = HttpContext.Session.GetString("userRoles");
                var userRoles = JsonConvert.DeserializeObject<List<RoleViewModel>>(roles);
                List<string> RoleListItems = new List<string>();
                foreach (var item in userRoles)
                    RoleListItems.Add(item.UserRoleName);
                ViewBag.UserRoleList = RoleListItems;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult Index(MainTrendRegionMapInsertUpdateDbViewModel model, string actionName)
        {
            bool bResult = false;
            SetRoleList();
            try
            {
                //------Start Converting Tile to normal list---------------
                model.TrendNoteModel.TrendType = "Region";
                if (model.TrendRegionMapInsertUpdateTile != null && model.TrendRegionMapInsertUpdateTile.Count > 0)
                {
                    model.TrendRegionMapInsertUpdate = new List<TrendRegionMapViewModel>();
                    foreach (var tm in model.TrendRegionMapInsertUpdateTile)
                    {
                        if (tm != null)
                            model.TrendRegionMapInsertUpdate.AddRange(tm);
                    }
                }
                //------End Converting Tile to normal list---------------


                if (actionName.Equals("save"))
                {
                    bResult = validate(model);
                    if (bResult)
                    {
                        model.TrendNoteModel.TrendStatusName = "Draft";
                        var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                        HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, model);
                        response.EnsureSuccessStatusCode();
                        ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            //ViewBag.Message = viewModel.Message;
                            return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.RegionName });
                        }
                    }
                }
                else if (actionName.Equals("Submitted"))
                {
                    bResult = validate(model);
                    if (bResult)
                    {
                        model.TrendNoteModel.TrendStatusName = "Submitted";
                        var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                        HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, model);
                        response.EnsureSuccessStatusCode();
                        ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            //ViewBag.Message = viewModel.Message;
                            return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.RegionName });
                        }
                    }
                }
                else if (actionName.Equals("Rejected"))
                {
                    bResult = validate(model);
                    if (bResult)
                    {
                        model.TrendNoteModel.TrendStatusName = "Rejected";
                        var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                        HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, model);
                        response.EnsureSuccessStatusCode();
                        ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            //ViewBag.Message = viewModel.Message;
                            return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.RegionName });
                        }
                    }
                }
                else if (actionName.Equals("Approve"))
                {
                    bResult = validate(model);
                    if (bResult)
                    {
                        model.TrendNoteModel.TrendStatusName = "Approved";
                        var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                        HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, model);
                        response.EnsureSuccessStatusCode();
                        ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                        if (viewModel != null)
                        {
                            //ViewBag.Message = viewModel.Message;
                            return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.RegionName });
                        }
                    }
                }
                else if (actionName.Equals("Next"))
                {
                    return RedirectToAction("Index", "TrendCompanyGroupMap", new { id = model.TrendId.ToString() });
                }
                else if (actionName.Equals("Previous"))
                {
                    return RedirectToAction("Index", "TrendProjectMap", new { id = model.TrendId.ToString() });
                }
                else if (actionName.Equals("Cancel"))
                {
                    return RedirectToAction("Index", "TrendRegionMap", new { id = model.TrendId.ToString() });
                }

                int PageStart = 0;
                int pageSize = 0;
                int SortDirection = 1;
                //------Get master data for Impact           
                HttpResponseMessage responseImpactType = _serviceRepository.GetResponse("ImpactType");
                responseImpactType.EnsureSuccessStatusCode();
                ViewBag.ImpactList = responseImpactType.Content.ReadAsAsync<List<ImpactTypeViewModel>>().Result;

                HttpResponseMessage responseTrend = _serviceRepository.GetByIdResponse("Trend", model.TrendId.ToString());
                responseTrend.EnsureSuccessStatusCode();
                TrendViewModel trend = responseTrend.Content.ReadAsAsync<TrendViewModel>().Result;
                model.TrendModel = trend;

                //------Get trend note by trend Id           
                HttpResponseMessage responseTrendNote = _serviceRepository.GetByIdResponse("TrendNote", model.TrendId.ToString(), "Region");
                responseTrendNote.EnsureSuccessStatusCode();
                var tnm = responseTrendNote.Content.ReadAsAsync<TrendNoteViewModel>().Result;
                model.TrendNoteModel = tnm ?? new TrendNoteViewModel();
                model.TrendModel.TrendStatusName = model.TrendNoteModel.TrendStatusName;


                var contoller = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage responseTwo = _serviceRepository.GetResponse(contoller, PageStart, pageSize, SortDirection, string.Empty, model.TrendId.ToString());
                responseTwo.EnsureSuccessStatusCode();
                List<TrendRegionMapViewModel> regionCountryMap = responseTwo.Content.ReadAsAsync<List<TrendRegionMapViewModel>>().Result;
                //model.TrendRegionMapInsertUpdate = regionCountryMap;

                //------Converting normal list to tile---------------
                model.TrendRegionMapInsertUpdateTile = Common.splitList(regionCountryMap, 2);

                //model.TrendRegionMapInsertUpdateTile = Common.splitList(model.TrendRegionMapInsertUpdate, 2);

                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, model.TrendModel.ProjectID);
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                }
                ViewBag.Id = model.TrendId;

                //HttpResponseMessage responsemp = _serviceRepository.GetResponse(contollerName, PageStart, pageSize, SortDirection, string.Empty, model.TrendId.ToString());
                //responsemp.EnsureSuccessStatusCode();
                //List<TrendRegionMapViewModel> trendRegionMap = responsemp.Content.ReadAsAsync<List<TrendRegionMapViewModel>>().Result;
                //model.TrendRegionMapInsertUpdate = trendRegionMap;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(model);
        }

        /// <summary>
        /// Method to validate Form
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private bool validate(MainTrendRegionMapInsertUpdateDbViewModel model)
        {
            int i = 0;
            int count = 1;
            int flag = 0;
            do
            {
                if (count > 0)
                {
                    if (model.TrendNoteModel.AuthorRemark == null || model.TrendNoteModel.AuthorRemark == "" || model.TrendNoteModel.AuthorRemark == string.Empty)
                    {
                        ViewBag.Message = "Please select Author Notes.";
                        flag++;
                        break;
                    }
                }

                i++;
                if (i > count - 1)
                    break;

            } while (true);
            if (flag > 0)
                return false;
            else
                return true;

        }

    }
}