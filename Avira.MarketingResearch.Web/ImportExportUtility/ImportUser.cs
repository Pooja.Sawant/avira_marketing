﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Web.Controllers;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Avira.MarketingResearch.Web.ImportExportUtility
{
    public class ImportUser : IExcelImport
    {
        public ImportResponse Import(IFormFile formFile)
        {
            var res = new ImportResponse();
            var list = new List<UserInfo>();

            using (var stream = new MemoryStream())
            {
                formFile.CopyTo(stream);

                using (var package = new ExcelPackage(stream))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                    var rowCount = worksheet.Dimension.Rows;

                    for (int row = 2; row <= rowCount; row++)
                    {
                        list.Add(new UserInfo
                        {
                            UserName = worksheet.Cells[row, 1].Value.ToString().Trim(),
                            Age = int.Parse(worksheet.Cells[row, 2].Value.ToString().Trim()),
                        });
                    }
                }
            }

            //--------Call api to post data
            //list

            res.ResponseStatus = "Success";
            res.ResponseMessage = "Partialy uploaded, please see the list";
            res.TotalRecords = 100;
            res.TotalSuccessRecords = 80;            
            res.TotalFailedRecords = 20;

            return res;
        }
    }
}
