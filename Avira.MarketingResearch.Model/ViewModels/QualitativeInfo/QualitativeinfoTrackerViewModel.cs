﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.QualitativeInfo
{
    public class QualitativeInfoTrackerViewModel
    {
        public Guid Id { get; set; }
        public Guid ProjectId { get; set; }
        public string TableName { get; set; }
        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "QualitativeInfoTracker";
            }
        }
    }
}
