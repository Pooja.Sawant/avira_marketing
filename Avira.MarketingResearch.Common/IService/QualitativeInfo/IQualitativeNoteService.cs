﻿using Avira.MarketingResearch.Model.ViewModels.QualitativeInfo;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.QualitativeInfo
{
    public interface IQualitativeNoteService
    {
         QualitativeNoteViewModel GetById(Guid Id, Guid ProjectId);
    }
}
