﻿

CREATE PROCEDURE [dbo].[SFab_GetTrendsByProjectId]
(
@ProjectID uniqueidentifier=null,
@ImportanceType nvarchar(256),
@AviraUserID uniqueidentifier = null
)
AS
/*================================================================================
Procedure Name:[SFab_GetTrendsByProjectId]
Author: Harshal
Create date: 06/05/2020
Description: Get All Trends of Project with its importance
Change History
Date		Developer		Reason
================================================================================
06-05-20    Harshal       Initial
---------------------------------------------------------------------------------*/
BEGIN
	declare @ImportanceIdList [dbo].[udtUniqueIdentifier];
	declare @TrendApprovedStatusId uniqueidentifier;

	select @TrendApprovedStatusId = Id
	from TrendStatus
	where TrendStatusName = 'Approved';

	insert into @ImportanceIdList
	Select Id
	From Importance
	Where (GroupName = @ImportanceType)

	Select t.Id,
		   t.TrendName,
		   t.TrendValue as Amount,
		   i.ImpactDirectionName,
		   ttg.TimeTagName
		   From Trend t
INNER JOIN ImpactDirection i ON t.ImpactDirectionId=i.Id
INNER JOIN Importance imp ON t.ImportanceId=imp.Id
INNER JOIN TimeTag ttg ON ttg.Id=(SELECT TOP 1 TimeTagId FROM TrendTimeTag Where TrendTimeTag.TrendId=t.Id Order by Seq desc)
where t.IsDeleted = 0 
AND (t.ProjectID=@ProjectID AND t.TrendStatusID=@TrendApprovedStatusId )--'29C5FF4B-D602-4943-865E-45581C1BCCAF'
AND t.ImportanceId in (SELECT Id From @ImportanceIdList )
END