﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : AviraBaseController
    {
        private ILoggerManager _logger;
        public readonly IUserService _userService;
        public AccountController(
            IUserService userService, ILoggerManager logger)
        {
            _userService = userService;
            _logger = logger;
        }

        [HttpPost]
        public ActionResult Post([FromBody]AviraUserLogin aviraUserLogin)
        {
            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error Occured, please contact Admin for more details."
            };
            try
            {
                var userDetails = _userService.GetUserLogin(aviraUserLogin.EmailAddress);

            //Checks the User is Active or Not.
            if(userDetails != null)
            { 
            if (userDetails.IsUserLocked == true)
                {
                    viewData.Message = "Your Email Address is Locked. Please contact Admin.";
                    return Ok(viewData);
                }
            }

            //Checks the Email Address is registred or not.
            if (userDetails == null)
            {
                viewData.Message = "Kindly enter correct email Address and Password Or Email address is not registred";
                return Ok(viewData);
            }
            else
            {
                //Decrypts the password and verifies the user.
                byte[] array = Encoding.ASCII.GetBytes("5");
                bool verify = PasswordHash.VerifyHash(aviraUserLogin.PassWord, "SHA512", userDetails.PassWord, array);

                if (verify)
                {
                    //Updates the user when success.
                    _userService.UpdateUserLogin(aviraUserLogin.EmailAddress, true);
                    viewData.Message = "User loggged in successfully";
                    viewData.IsSuccess = true;
                    return Ok(viewData);
                }
                else
                {
                    //if user gets enterd 3 times wrong password then user will be blocked.
                    userDetails = _userService.UpdateUserLogin(aviraUserLogin.EmailAddress, false);
                    int incorrectPwdCount = userDetails.ConcurrentLoginFailureCount;
                    if (incorrectPwdCount == 1)
                    {
                        viewData.Message = "Password is incorrect. You have 2 chances left.";
                    }
                    else if (incorrectPwdCount == 2)
                    {
                        viewData.Message = "Password is incorrect. You have 1 chance left.";
                    }
                    else
                    {
                        viewData.Message = "Password is incorrect. Your Email Address is Locked. Please contact Admin.";
                    }
                    return Ok(viewData);
                }
            }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Ok(viewData);
        }

        [HttpGet("{email}")]
        public ActionResult<AviraUser> Get(string email)
        {
            AviraUser res = null;
            try
            {
                res = _userService.GetByEmail(email);
            if (res == null)
            {
                return NotFound();
            }

        }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return res;
        }

        [HttpGet]
        [Route("GetByAviraUserId")]
        public ActionResult<AviraUser> GetByAviraUserId(string userId)
        {
            AviraUser aviraUser = null;
            try
            {
                aviraUser = _userService.GetByUserId(userId);
            if (aviraUser == null)
            {
                return NotFound();
            }
        }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return aviraUser;
        }

        //[HttpGet("{AviraUserId}")]
        //[Route("GetAllUserRoles")]
        public ActionResult<IEnumerable<RoleViewModel>> Get(Guid aviraUserId)
        {
            List<RoleViewModel> res = null;
            try
            {
                res = _userService.GetRolesByAviraUserId(aviraUserId).ToList();
        }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet]
        [Route("GetPermissionsByUserRoleId")]
        public ActionResult<PermissionsByUserRoleIdViewModel> GetPermissionsByUserRoleId(Guid id)
        {
            PermissionsByUserRoleIdViewModel res = null;
            try
            {
                res = _userService.GetPermissionsByUserRoleId(id);
        }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }
    }
}