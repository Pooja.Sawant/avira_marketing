﻿
CREATE   Procedure [dbo].[SFab_GetCIDebtProfileTab] 
(
	@CompanyID uniqueidentifier = null
)
As
/*================================================================================  
Procedure Name: [SFab_GetCIDebtProfileTab]  
Author: Harshal 
Create date: 12-Nov-2019  
Description: Get CI Company Financial Analysis Debt Profile Tab Data
Change History  
Date         Developer     Reason  
____________ __________  ______________________________________________________  
12-Nov-2019   Harshal       Initial Version  
================================================================================*/  
BEGIN
	
	Declare @UnitId uniqueidentifier
	Declare @USDCurrencyId uniqueidentifier

	Select @USDCurrencyId = [dbo].[fun_GetCurrencyIDForUSD]()

	Select @UnitId = Id 
	From ValueConversion
	Where ValueName = 'Millions';
	
	Declare @Years Table (CompanyID uniqueidentifier, [Year] int)
	Declare @MaxYear int, @MinYear int

	Select @MaxYear = Max([Year]), @MinYear = Min([Year])
	From CompanyPLStatement
	Where CompanyId = @CompanyID;	
	
	If (@MaxYear Is Not Null And @MinYear Is Not Null)
	Begin
		WITH CTE
			 AS (SELECT @CompanyID As CompanyID, @MinYear As Year1
				 UNION ALL
				 SELECT @CompanyID As CompanyID, Year1 + 1 
				 FROM   CTE
				 WHERE  Year1 < @MaxYear)
		Insert Into @Years
		SELECT *
		FROM   CTE 
	End

	Declare @CICompanyFinancialDebtProfileData Table(
		CompanyId	uniqueidentifier,
		[Year]	int,
		ShortTermDebt decimal (19, 2),
		LongTermDebt decimal (19, 2),
		TotalDebt decimal (19, 2),
		CashAndCashEquivalents	decimal (19, 2),
		ShortTermInvestments decimal (19, 2),
		NetDebt decimal (19, 2),
		InterestExpense decimal (19, 2),
		EBITDA decimal (19, 2),
		EBIT decimal (19, 2),
		TotalDebt_EBITDA decimal (19, 2),
		InterestCoverageRatio decimal (19, 2),
		DebtServiceCoverageRatio decimal (19, 2)
	)


	
	Insert Into @CICompanyFinancialDebtProfileData (CompanyId, [Year],ShortTermDebt,LongTermDebt,TotalDebt, CashAndCashEquivalents,ShortTermInvestments)
	Select Y.CompanyId, Y.[Year], 
	        [dbo].[fun_UnitDeConversion](@UnitId, Cast(CBS.ShortTermDebt as decimal(19, 1))) As ShortTermDebt,
		    [dbo].[fun_UnitDeConversion](@UnitId, Cast(CBS.LongTermDebt as decimal(19, 1))) As LongTermDebt,
			[dbo].[fun_UnitDeConversion](@UnitId, Cast(CBS.ShortTermDebt as decimal(19,1)) + Cast(CBS.LongTermDebt As decimal(19, 1))) As TotalDebt,
			[dbo].[fun_UnitDeConversion](@UnitId, Cast(CBS.CashAndCashEquivalents as decimal(19, 1))) As CashAndCashEquivalents,
			[dbo].[fun_UnitDeConversion](@UnitId, Cast(CBS.ShortTermInvestments as decimal(19, 1))) As ShortTermInvestments
	From @Years Y Left Join CompanyBalanceSheet CBS On Y.[Year] = CBS.[Year] And CBS.CompanyId = @CompanyID	
	Order By Y.[Year]

	Update @CICompanyFinancialDebtProfileData
	Set NetDebt = TotalDebt -(CashAndCashEquivalents + ShortTermInvestments)


	Update @CICompanyFinancialDebtProfileData Set
		EBITDA = [dbo].[fun_UnitDeConversion](@UnitId, CPLS.EBITDA),
		EBIT = [dbo].[fun_UnitDeConversion](@UnitId, CPLS.EBIT),
		InterestExpense = [dbo].[fun_UnitDeConversion](@UnitId, CPLS.InterestExpense)
	From @CICompanyFinancialDebtProfileData T Inner Join CompanyPLStatement CPLS On T.CompanyId = CPLS.CompanyId And T.[Year] = CPLS.[Year]


	Update @CICompanyFinancialDebtProfileData
	Set TotalDebt_EBITDA = TotalDebt / NULLIF(EBITDA, 0),
		InterestCoverageRatio = EBIT/ NULLIF(InterestExpense, 0),
		DebtServiceCoverageRatio = EBIT / NULLIF(TotalDebt,0)
		
	
	Select top 7  CompanyID, [Year],ShortTermDebt,LongTermDebt,TotalDebt, CashAndCashEquivalents,ShortTermInvestments,NetDebt,InterestExpense, EBITDA, EBIT,TotalDebt_EBITDA,InterestCoverageRatio,DebtServiceCoverageRatio
	From @CICompanyFinancialDebtProfileData
	Order By [Year] Desc
END