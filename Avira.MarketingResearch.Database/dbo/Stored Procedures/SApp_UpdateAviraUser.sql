﻿CREATE procedure [dbo].[SApp_UpdateAviraUser]    
(@AviraUserID uniqueidentifier,
@FirstName nvarchar(128),
@LastName nvarchar(128),
@UserName nvarchar(100),
@DisplayName nvarchar(256),
@EmailAddress nvarchar(256),
@PhoneNumber nvarchar(128),
@UserTypeId tinyint,
@PassWord nvarchar(1000),
@CustomerId uniqueidentifier,
@UserRoleId uniqueidentifier,
@Salt nvarchar(256),
@IsDeleted bit=0,  
@IsUserLocked bit = 0,
@UserModifiedById  uniqueidentifier,  
@DeletedOn datetime = null,  
@UserDeletedById uniqueidentifier = null,  
@ModifiedOn Datetime   
)
as    
/*================================================================================    
Procedure Name: SApp_UpdateAviraUser    
Author: Sharath    
Create date: 02/19/2019    
Description: update a record in AviraUser table by ID. Also used for delete    
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
02/19/2019 Sharath   Initial Version  
------------------------------------------------------------------------------  
03/18/2019 Nimishekh 1) All the paramter of Model should be decalre as input parameter   
						with default value is null if those parameter is not mandetory.      
					 2) Return should select with all the value of standard Model that we are using in Code  
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
  
IF exists(select 1 from [dbo].[AviraUser]     
   where [Id] !=  @AviraUserID    
   and  UserName = @UserName AND IsDeleted = 0)    
begin    
declare @ErrorMsg NVARCHAR(2048);    
set @ErrorMsg = 'AviraUser with Name "'+ @UserName + '" already exists.';    
--SELECT 50000,@ErrorMsg,1;   
--return(1);   
SELECT StoredProcedureName ='SApp_UpdateAviraUser','Message' =@ErrorMsg,'Success' = 0;  
RETURN;
end    
  
BEGIN TRY     
--update record    

UPDATE [dbo].[AviraUser]
   SET [FirstName] = @FirstName,
		[LastName] = @LastName,
		[UserName] = @UserName,
		[DisplayName] = @DisplayName,
		[EmailAddress] = @EmailAddress,
		[PhoneNumber] = @PhoneNumber,
		[UserTypeId] = @UserTypeId,
		[PassWord] = @PassWord,
		[CustomerId] = @CustomerId,
		[UserRoleId] = @UserRoleId,
		[Salt] = @Salt,
		[ModifiedOn] = getdate(), 
		DeletedOn = case when IsDeleted = 0 and @IsDeleted = 1 then getdate() else DeletedOn end,
		UserDeletedById = case when IsDeleted = 0 and @IsDeleted = 1 then @UserModifiedById else UserDeletedById end,
		IsDeleted = isnull(@IsDeleted, IsDeleted),
		IsUserLocked = @IsUserLocked,
		[UserModifiedById] = @UserModifiedById    
 WHERE [Id] = @AviraUserID
 
SELECT StoredProcedureName ='SApp_UpdateAviraUser',Message =@ErrorMsg,Success = 1;     
  
END TRY    
BEGIN CATCH    
    -- Execute the error retrieval routine.    
 DECLARE @ErrorNumber int;    
 DECLARE @ErrorSeverity int;    
 DECLARE @ErrorProcedure varchar(100);    
 DECLARE @ErrorLine int;    
 DECLARE @ErrorMessage varchar(500);    
    
  SELECT @ErrorNumber = ERROR_NUMBER(),    
        @ErrorSeverity = ERROR_SEVERITY(),    
        @ErrorProcedure = ERROR_PROCEDURE(),    
        @ErrorLine = ERROR_LINE(),    
        @ErrorMessage = ERROR_MESSAGE()    
    
 insert into dbo.Errorlog(ErrorNumber,    
       ErrorSeverity,    
       ErrorProcedure,    
       ErrorLine,    
       ErrorMessage,    
       ErrorDate)    
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE()); 

set @ErrorMsg = 'Error while updating AviraUser, please contact Admin for more details.' 		   
 --set @ErrorMsg = 'Unspecified Error';    
--THROW 50000,@ErrorMsg,1;    
 --return(1)  
 SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;     
END CATCH    
    
  
  
    
end