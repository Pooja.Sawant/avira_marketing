﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.ImportExport;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StrategyInstanceImportExportController : AviraBaseController
    {
        private ILoggerManager _logger;
        public readonly IStrategyInstanceImportExportService _strategyInstanceImportExportService;

        public StrategyInstanceImportExportController(IStrategyInstanceImportExportService strategyInstanceImportExportService, ILoggerManager logger)
        {
            _strategyInstanceImportExportService = strategyInstanceImportExportService;
            _logger = logger;
        }

        [HttpPost]
        public ActionResult Post([FromBody] StrategyIntanceImportModel viewModel)
        {
            ImportCommonResponseMainModel viewData = new ImportCommonResponseMainModel()
            {
                IsSuccess = false,
                Message = "Error while creating a new Strategy Instance, please contact Admin for more details."
            };
            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                var result = _strategyInstanceImportExportService.Create(viewModel);

                var res = result.Where(s => s.ErrorNotes != null).ToList();

                if (res.Count <= 0)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Import Strategy Instance successfully.";
                }
                else
                {
                    viewData.Message = "Import Strategy Instance have some errors";
                    //Export to Excel data.
                    //foreach (var item in result.ToList())
                    //{
                    //    item.ErrorNotes = item.ErrorNotes.Replace(",", " ");
                    //}
                    viewData.ResponseModel = result;

                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);

                viewData = new ImportCommonResponseMainModel()
                {
                    IsSuccess = false,
                    Message = "Error while creating a new Strategy Instance. Error Details: " + ex.Message + ". please contact Admin for more details."
                };
            }
            return Ok(viewData);
        }
    }
}