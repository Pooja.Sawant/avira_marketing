﻿
CREATE PROCEDURE [dbo].[SFab_InsertDownloadHistory]
(
@ModuleId	uniqueidentifier,
@ProjectId	uniqueidentifier,
@FileName   nvarchar(256),
@UserId	uniqueidentifier,
@DownloadDate datetime2(7),
@StatusName	nvarchar(256)
)
As   

/*================================================================================    
Procedure Name: SFab_InsertDownloadHistory   
Author: Harshal Ghotkar    
Create date: 27-April-2020
Description: insert In DownloadHistory by user
Change History    
Date  Developer  Reason  
__________ ____________ ______________________________________________________    
27-April-20  Harshal   Initial Version    
================================================================================*/    
BEGIN  

SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
Declare @ErrorMsg NVARCHAR(2048)     
 BEGIN TRY 
 Declare @StatusId uniqueidentifier 
 Select @StatusId = Id From [LookupStatus] 
	Where [StatusType] = 'ImportStatus'  And StatusName = @StatusName;
		
		
INSERT INTO [dbo].[DownloadHistory]
([Id],
[ModuleId],
[ProjectId],	
[FileName], 
[UserId],	
[DownloadDate], 
[StatusId]
)
VALUES(
NEWID(),
@ModuleId,
@ProjectId,
@FileName,
@UserId,
GETDATE(),
@StatusId
)
SELECT StoredProcedureName ='SFab_InsertDownloadHistory',Message =@ErrorMsg,Success = 1; 
END TRY      
BEGIN CATCH      
  exec SApp_LogErrorInfo
  set @ErrorMsg = 'Error while insert in DownloadHistory, please contact Admin for more details.';    
SELECT StoredProcedureName ='SFab_InsertDownloadHistory',Message =@ErrorMsg,Success = 0;  
END CATCH   
		
END