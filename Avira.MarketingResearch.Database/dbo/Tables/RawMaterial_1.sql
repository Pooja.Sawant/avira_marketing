﻿CREATE TABLE [dbo].[RawMaterial] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [RawMaterialName]  NVARCHAR (256)   CONSTRAINT [DF_RawMaterial_RawMaterialName] DEFAULT ('') NOT NULL,
    [Description]      NVARCHAR (256)   NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_RawMaterial_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_RawMaterial] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_RawMaterial_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_RawMaterial_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id])
);

