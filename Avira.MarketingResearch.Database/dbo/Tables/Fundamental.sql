﻿CREATE TABLE [dbo].[Fundamental] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [FundamentalName]  NVARCHAR (256)   CONSTRAINT [DF_Fundamental_FundamentalName] DEFAULT ('') NOT NULL,
    [Description]      NVARCHAR (256)   NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_Fundamental_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_Fundamental] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Fundamental_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_Fundamental_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id])
);


GO

GO

GO

GO

GO

GO

GO