﻿using Avira.KeyCompanyingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrendKeyCompanyMapController : AviraBaseController
    {
        private readonly ITrendKeyCompanyMapService _trendKeyCompanyMapService;
        private ILoggerManager _logger;
        public TrendKeyCompanyMapController(
            ITrendKeyCompanyMapService trendKeyCompanyMapService,
             ILoggerManager logger)
        {
            _trendKeyCompanyMapService = trendKeyCompanyMapService;
            _logger = logger;

        }

        [HttpGet]
        public ActionResult<IEnumerable<TrendKeyCompanyMapViewModel>> Get(int PageStart = 0, int pageSize = 0, int SortDirection = 1, string OrdbyByColumnName = "CreatedDate", string Search = "", bool isDeletedInclude = false, string id = "")
        {
            List<TrendKeyCompanyMapViewModel> res = null;
            try
            {
                res = _trendKeyCompanyMapService.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude, id).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        //[HttpPost]
        //public ActionResult Post([FromBody] MainTrendKeyCompanyMapInsertUpdateDbViewModel viewModel)
        //{
        //    viewModel.CreatedOn = System.DateTime.UtcNow;
        //    viewModel.AviraUserId = CurrentUser.Id;

        //    IViewModel viewData = new ViewData()
        //    {
        //        IsSuccess = false,
        //        Message = "Error while creating a new TrendKeyCompany, please contact Admin for more details."
        //    };

        //    try
        //    {
        //        if (!ModelState.IsValid)
        //        {
        //            viewData.Message = string.Join("; ", ModelState.Values
        //            .SelectMany(x => x.Errors)
        //            .Select(x => x.ErrorMessage));
        //            return Ok(viewData);
        //        }
        //        var result = _trendKeyCompanyMapService.InsertUpdate(viewModel, CurrentUser);
        //        if (result != null && result.IsSuccess)
        //        {
        //            viewData.IsSuccess = true;
        //            viewData.Message = "TrendKeyCompany is created successfully."; ;
        //        }
        //        else
        //        {
        //            viewData.Message = result.Error;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError(ex.Message);
        //    }

        //    return Ok(viewData);
        //}


        [HttpPost]        
        public ActionResult Post([FromBody] MainTrendKeyCompanyMapInsertUpdateDbViewModel viewModel)
        {
            viewModel.CreatedOn = System.DateTime.UtcNow;
            viewModel.AviraUserId = CurrentUser.Id;

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new TrendKeyCompany, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                var result = _trendKeyCompanyMapService.Insert(viewModel, CurrentUser);
                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "TrendKeyCompany is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }

        [HttpPut]
        [Route("Delete")]
        public ActionResult Delete([FromBody] MainTrendKeyCompanyMapInsertUpdateDbViewModel viewModel)
        {
            viewModel.CreatedOn = System.DateTime.UtcNow;
            viewModel.AviraUserId = CurrentUser.Id;

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new TrendKeyCompany, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                var result = _trendKeyCompanyMapService.Delete(viewModel, CurrentUser);
                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "TrendKeyCompany is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }

    }
}