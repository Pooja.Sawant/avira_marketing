﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyNoteController : AviraBaseController
    {
        private ILoggerManager _logger;
        public readonly ICompanyNoteService _companyNoteService;

        public CompanyNoteController(ICompanyNoteService companyNoteService, ILoggerManager logger)
        {
            _companyNoteService = companyNoteService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<CompanyNoteViewModel> Get(Guid companyId, string type, Guid? ProjectId = null)
        {
            CompanyNoteViewModel trendNote = null;
            try
            {
                trendNote = _companyNoteService.GetById(companyId, type, ProjectId);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return trendNote;
        }
    }
}