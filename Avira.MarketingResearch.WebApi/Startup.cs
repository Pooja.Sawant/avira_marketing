﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Utility;
using Avira.MarketingResearch.WebApi;
using CacheManager.Core;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using NLog;
using NLog.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;

namespace Avira.MarketingResearch
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {

            Configuration = configuration;
            LogManager.LoadConfiguration(String.Concat(Directory.GetCurrentDirectory(), "/nlog.config"));

        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            DependencyConfig.ConfigureServices(services);

            services.AddCors(options =>
            {
                options.AddPolicy("AnyOrigin",
                builder => builder.AllowAnyOrigin());
            });

            var cacheConfig = CacheManager.Core.ConfigurationBuilder.BuildConfiguration(settings =>
            {
                settings.WithUpdateMode(CacheUpdateMode.Up)
               .WithDictionaryHandle()
                .WithExpiration(ExpirationMode.Absolute, TimeSpan.FromMinutes(5));

            });
            services.AddSingleton<ICacheManager<IEnumerable<MenuViewModel>>>(t => CacheFactory.FromConfiguration<IEnumerable<MenuViewModel>>(cacheConfig));
            services.AddSingleton<ICacheManager<IEnumerable<RegionViewModel>>>(t => CacheFactory.FromConfiguration<IEnumerable<RegionViewModel>>(cacheConfig));
            services.AddSingleton<ICacheManager<IEnumerable<CountryViewModel>>>(t => CacheFactory.FromConfiguration<IEnumerable<CountryViewModel>>(cacheConfig));
            services.AddSingleton<ILoggerManager, LoggerManager>();
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            GlobalDiagnosticsContext.Set("configDir", @"D:\logs\authorizing-api-internallog.txt");
            GlobalDiagnosticsContext.Set("ConnectionStrings", Configuration.GetConnectionString("NLogDb"));

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
           Path.Combine(Directory.GetCurrentDirectory(), "Uploads")),
                RequestPath = "/Uploads"
            });
            loggerFactory.AddNLog();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseMvc();           

            app.Run(async (context) =>
            {
                await context.Response.WriteAsync("MVC didn't find anything!");
            });
        }
    }
}
