﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport
{
    public interface ICompanyProfileImportExportRepository
    {
        //Result<SpTransactionMessage> Create(CompanyProfileImportInsertViewModel viewModel);
        List<ImportCommonResponseModel> Create(CompanyProfileImportInsertViewModel viewModel);
    }
}
