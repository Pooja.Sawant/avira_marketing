﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.ImportExport
{
    public class MarketSizingImportExportRepository : IMarketSizingImportExportRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "MarketSizingImportExport";

        public MarketSizingImportExportRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public bool CreateStagging(MarketSizingImportInsertViewModel viewModel)
        {
          
            var spTransactionMessage = _dbHelper.ExecuteQuery(ImportExportQueries.StoreProcInsertMarketSizing, new
            {
                @TemplateName = viewModel.TemplateName,
                @ImportFileName = viewModel.ImportFileName,
                @jsonTrend = viewModel.TrendJson,
                @ImportId = viewModel.ImportId,
                @UserCreatedById = viewModel.UserCreatedById
            }, CommandType.StoredProcedure);
            
            return spTransactionMessage != null? spTransactionMessage.IsSuccess : false;
        }

        public bool DeleteMSStagging(MarketSizingImportInsertViewModel viewModel)
        {

            var spTransactionMessage = _dbHelper.ExecuteQuery(ImportExportQueries.StoreProcDeleteStagingMarketSizing, new
            {
                @ImportId = viewModel.ImportId,
                @UserCreatedById = viewModel.UserCreatedById
            }, CommandType.StoredProcedure);

            return spTransactionMessage != null ? spTransactionMessage.IsSuccess : false;
        }

        public List<MarketSizingImportViewModel> Create(MarketSizingImportInsertViewModel viewModel)
        {

            //var spTransactionMessage = _dbHelper.ExecuteQuery(ImportExportQueries.StoreProcInsertMarketSizing, new
            //{
            //    @TemplateName = viewModel.TemplateName,
            //    @ImportFileName = viewModel.ImportFileName,
            //    @jsonTrend = viewModel.TrendJson,
            //    @ImportId = viewModel.ImportId,
            //    @UserCreatedById = viewModel.UserCreatedById
            //}, CommandType.StoredProcedure);

            var spTransactionMessage1 = new List<MarketSizingImportViewModel>();
            //if (spTransactionMessage.IsSuccess == true)
            //{
                var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
                spTransactionMessage1 = dapper.Query<MarketSizingImportViewModel>(ImportExportQueries.StoreProcUpdateMarketSizing
                            , null
                            , null, true, 0, CommandType.StoredProcedure) as List<MarketSizingImportViewModel>;

            //}
            return spTransactionMessage1;
        }


    }
}
