﻿CREATE FUNCTION [dbo].[Fn_GetAlphaNumSortText](
@String nvarchar(2000) )
returns 
nvarchar(2000) as 
Begin
declare @ReturnValue nvarchar(2000) = '';
declare @PostFix nvarchar(2000) = '';
declare @strtable table (Prefix nvarchar(2000),
						LPADNumber nvarchar(2000),
						PostFix nvarchar(2000),
						seq int)
if @String is null or @String = '' or patindex('%[0-9]%', @String) = 0
begin
set @ReturnValue = @String
end
else
begin
;with C as
  (
    select cast(substring(S.Value, S1.Pos, S2.L) as int) as Number
           ,stuff(s.Value, 1, S1.Pos + S2.L-1, '') as Value
		   ,case when S1.Pos> 1 then cast(substring(S.Value, 0, S1.Pos) as nvarchar(2000)) else '' end
		   as Prefix
		   ,case when S2.L<10 then REPLICATE('0', 10-S2.L) + substring(S.Value, S1.Pos, S2.L) else substring(S.Value, S1.Pos, S2.L) end 
		   as lpadNumber
		  ,S2.L,
		  1 as seq
    from (select @String+' ') as S(Value)
      cross apply (select patindex('%[0-9]%', S.Value)) as S1(Pos)
      cross apply (select patindex('%[^0-9]%', stuff(S.Value, 1, S1.Pos, ''))) as S2(L)
    union all
    select cast(substring(S.Value, S1.Pos, S2.L) as int)
           ,stuff(S.Value, 1, S1.Pos + S2.L-1, '')
		   ,case when S1.Pos> 1 then cast(substring(S.Value, 0, S1.Pos) as nvarchar(2000)) else '' end
		   ,case when S2.L<10 then REPLICATE('0', 10-S2.L) + substring(S.Value, S1.Pos, S2.L) else substring(S.Value, S1.Pos, S2.L) end 
		   as lpadNumber
		   ,S2.L,
		   seq+1 
    from C as S
      cross apply (select patindex('%[0-9]%', S.Value)) as S1(Pos)
      cross apply (select patindex('%[^0-9]%', stuff(S.Value, 1, S1.Pos, ''))) as S2(L)
    where patindex('%[0-9]%', S.Value) > 0
  )
  insert into @strtable
  select Prefix, LPADNumber, Value, seq
  FROM C
  order by seq

  select @ReturnValue = @ReturnValue + Prefix + LPADNumber,
  @PostFix = PostFix
  from @strtable
  order by seq;

  select @ReturnValue = @ReturnValue + @PostFix;
  end
  return (@ReturnValue);

 end