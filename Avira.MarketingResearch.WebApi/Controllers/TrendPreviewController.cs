﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrendPreviewController : AviraBaseController
    {
        private ILoggerManager _logger;
        public readonly ITrendPreviewService _trendPreviewService;

        public TrendPreviewController(ITrendPreviewService trendPreviewService, ILoggerManager logger)
        {
            _trendPreviewService = trendPreviewService;
            _logger = logger;
        }

        [HttpGet("{id}")]
        public ActionResult<TrendPreviewViewModel> Get(Guid id)
        {
            TrendPreviewViewModel trendPreview = null;
            try
            {
                trendPreview = _trendPreviewService.GetById(id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return trendPreview;
        }
    }
}