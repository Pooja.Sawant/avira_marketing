﻿CREATE PROCEDURE [dbo].[SFab_GetTopESMSegments] (@ProjectID uniqueidentifier ,@SegmentId  uniqueidentifier = NULL,
@AviraUserID uniqueidentifier = NULL)  
AS  
/*================================================================================  
Procedure Name: SFab_GetTopESMSegments  
Author: Praveen   
Create date: 12/17/2019  
Description: Get top 4 segment based on the sub segment larger count from ESMSegmentMapping table 
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
12/17/2019 Praveen   Initial Version  
================================================================================*/  
BEGIN  
  SET NOCOUNT ON;  
  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
  SET @SegmentId = ISNULL(@SegmentId, '00000000-0000-0000-0000-000000000000')
 
  SELECT TOP 7 Segment.Id, Segment.[SegmentName], COUNT(ESM.Id) AS NoSubSegment FROM Segment
  INNER JOIN [dbo].[ESMSegmentMapping] ESM ON ESM.ProjectId = @ProjectID AND Segment.Id = ESM.SegmentId
  WHERE  @SegmentId = CASE WHEN @SegmentId = NULL or @SegmentId = '00000000-0000-0000-0000-000000000000' THEN @SegmentId ELSE Segment.Id END 
 --AND EXISTS(SELECT 1 FROM [dbo].[ESMSegmentMapping] ESM WHERE ESM.ProjectId = @ProjectID AND Segment.Id = ESM.SegmentId)
 GROUP BY Segment.Id, Segment.[SegmentName]
  ORDER BY COUNT(ESM.Id) desc , SegmentName  
  
END