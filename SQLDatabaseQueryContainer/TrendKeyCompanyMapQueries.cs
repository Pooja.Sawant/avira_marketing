﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public class TrendKeyCompanyMapQueries
    {
        public static readonly string StoreProcInsertUpdate = "SApp_InsertUpdateTrendKeyCompanyMap";
        public static readonly string StoreProcInsert = "SApp_InsertTrendKeyCompanyMap";
        public static readonly string StoreProcDelete = "SApp_DeleteTrendKeyCompanyMap";
        public static string StoreProcSelectAll = "SApp_GetTrendKeyCompanyMapByTrendID";
    }
}
