﻿using Avira.MarketingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.Trend
{
    public class TrendPreviewService : ITrendPreviewService
    {
        private readonly ITrendPreviewRepository _iTrendPreviewRepository;

        public TrendPreviewService(ITrendPreviewRepository iTrendPreviewRepository)
        {
            _iTrendPreviewRepository = iTrendPreviewRepository;
        }

        public TrendPreviewViewModel GetById(Guid id)
        {
            return _iTrendPreviewRepository.GetById(id);
        }
    }
}
