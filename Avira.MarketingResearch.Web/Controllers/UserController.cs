﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;

namespace Avira.MarketingResearch.Web.Controllers
{
    [Authorization]
    public class UserController : BaseController
    {
        public readonly IServiceRepository _serviceRepository;
        private ILoggerManager _logger;
        public UserController(IServiceRepository serviceRepository, ILoggerManager logger)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
        }

        // GET: User

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Loaddata()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            int totalRecords = 0;
            List<GetAllAviraUsers> data = null;
            try
            {
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                string search = Request.Form["search[value]"];
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();

                var sortColumnDir = Request.Form["order[0][dir]"].FirstOrDefault();

                var SortDirection = -1;
                if (sortColumnDir == "asc")
                {
                    SortDirection = 1;
                }
                else
                {
                    SortDirection = -1;
                }

                int skip = start != null ? Convert.ToInt32(start) : 0;
                int pageSize = length.ToString() != null ? Convert.ToInt32(length) : 0;
                int PageStart = start.ToString() != null ? Convert.ToInt32(start) : 0;
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.GetResponse(contollerName, PageStart, pageSize, SortDirection, sortColumn, search);

                response.EnsureSuccessStatusCode();
                List<GetAllAviraUsers> users = response.Content.ReadAsAsync<List<GetAllAviraUsers>>().Result;

                ViewBag.Title = "All Users";
                data = users;
            
                foreach (var item in users)
                {
                    totalRecords = item.TotalItems;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data });

        }

        public IActionResult Details(int id)
        {
            return View();
        }

        // GET: User/Create
        [Authorize]
        public IActionResult Create()
        {
            try
            {
                ViewBag.data = HttpContext.Session.GetString("DisplayName");
                var roles = HttpContext.Session.GetString("userRoles");
                var userRoles = JsonConvert.DeserializeObject<List<RoleViewModel>>(roles);
                if (userRoles[0].UserRoleName == "Admin" || userRoles[0].UserRoleName == "Approver" || userRoles[0].UserRoleName == "Co-Approver")
                {
                    var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                    HttpResponseMessage roleResponse = _serviceRepository.GetAllParentResponse(contollerName);
                    List<RoleViewModel> roleList = roleResponse.Content.ReadAsAsync<List<RoleViewModel>>().Result;
                    HttpResponseMessage locationResponse = _serviceRepository.GetSubResponse(contollerName);
                    List<LocationViewModel> locationList = locationResponse.Content.ReadAsAsync<List<LocationViewModel>>().Result;
                    ViewBag.RoleList = new SelectList(roleList, "Id", "UserRoleName");
                    ViewBag.LocationList = new SelectList(locationList, "Id", "LocationName");
                    //--
                    ViewData["RoleList"] = new SelectList(roleList, "Id", "UserRoleName", "---select---");
                    //--
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View();
        }

        // POST: User/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(AviraUserInsertRequest aviraUserInsertRequest)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var ulist = new List<UserRoleList>();
                    for (int i = 0; i < aviraUserInsertRequest.UserRoleIDList.Count; i++)
                    {
                        if (aviraUserInsertRequest.UserRoleIDList[i] == aviraUserInsertRequest.UserRoleIDList[0])
                            ulist.Add(new UserRoleList { UserRoleID = aviraUserInsertRequest.UserRoleIDList[i], IsPrimary = true });
                        else
                            ulist.Add(new UserRoleList { UserRoleID = aviraUserInsertRequest.UserRoleIDList[i], IsPrimary = false });
                    }

                    var userReq = new AviraUserInsertDbViewModel()
                    {
                        FirstName = aviraUserInsertRequest.FirstName,
                        LastName = aviraUserInsertRequest.LastName,
                        DisplayName = aviraUserInsertRequest.DisplayName,
                        EmailAddress = aviraUserInsertRequest.EmailAddress,
                        Username = aviraUserInsertRequest.Username,
                        PassWord = aviraUserInsertRequest.PassWord,
                        //UserTypeId = aviraUserInsertRequest.UserTypeId,
                        UserTypeId = 1,
                        AviraUserID = aviraUserInsertRequest.AviraUserID,
                        CustomerId = aviraUserInsertRequest.CustomerId,
                        DateofJoining = aviraUserInsertRequest.DateofJoining,
                        CreatedOn = aviraUserInsertRequest.CreatedOn,
                        IsDeleted = aviraUserInsertRequest.IsDeleted,
                        IsEverLoggedIn = aviraUserInsertRequest.IsEverLoggedIn,
                        PhoneNumber = aviraUserInsertRequest.PhoneNumber,
                        salt = aviraUserInsertRequest.salt,
                        UserCreatedById = aviraUserInsertRequest.UserCreatedById,
                        LocationID = aviraUserInsertRequest.LocationID,
                        UserRolelist = ulist
                    };
                    var controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                    HttpResponseMessage response = _serviceRepository.PostResponse(controllerName, userReq);
                    response.EnsureSuccessStatusCode();
                    ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                    if (viewModel != null)
                    {
                        ViewBag.Message = viewModel.Message;
                        if (viewModel.IsSuccess)
                        {
                            aviraUserInsertRequest = new AviraUserInsertRequest();
                            ModelState.Clear();
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                }
            }
            LoadData();
            aviraUserInsertRequest.UserRoleIDList = null;
            return View(aviraUserInsertRequest);
        }

        public void LoadData()
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage roleResponse = _serviceRepository.GetAllParentResponse(contollerName);
                List<RoleViewModel> roleList = roleResponse.Content.ReadAsAsync<List<RoleViewModel>>().Result;
                HttpResponseMessage locationResponse = _serviceRepository.GetSubResponse(contollerName);
                List<LocationViewModel> locationList = locationResponse.Content.ReadAsAsync<List<LocationViewModel>>().Result;
                ViewBag.RoleList = new SelectList(roleList, "Id", "UserRoleName");
                //ViewData["UserRoleId"] = new SelectList(roleList, "Id", "UserRoleName");
                //ViewData["AuthorID"] = new SelectList(_context.Authors, "AuthorID", "Author");
                ViewBag.LocationList = new SelectList(locationList, "Id", "LocationName");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        // GET: User/Edit/5
        public IActionResult Edit(int id)
        {
            return View();
        }

        // POST: User/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Delete/5
        public IActionResult Delete(int id)
        {
            return View();
        }

        // POST: User/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}