﻿

CREATE procedure [dbo].[SApp_insertAllCompanyProfileFromStagging]
@AviraUserId uniqueidentifier =null
as    
/*================================================================================    
Procedure Name: [SApp_insertAllCompanyProfileFromStagging]    
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
19/06/2019 Sharath   Initial Version    
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    
declare @ErrorMsg NVARCHAR(2048)
declare @CompanyId	UNIQUEIDENTIFIER;

BEGIN TRY       
BEGIN TRANSACTION
select @CompanyId = CompanyId	
from [StagingCompanyProfileFundamentals];

--Fundamentals
Insert Into Company(
			Id,
			CompanyName,
			CompanyCode,
			NatureId,
			Telephone,
			Telephone2,
			Telephone3,
			Website,
			YearOfEstb,
			CompanyStageId,
			NoOfEmployees,
			CompanyLocation,
			IsHeadQuarters,
			CreatedOn,
			UserCreatedById,
			IsDeleted,
			RankRationale,
			RankNumber,
			EntityTypeId
)
Select CPFStaging.CompanyId,
	   CPFStaging.CompanyName,
	   --CPFStaging.CompanyCode, commented by praveen
	   dbo.fun_NextCompanyCode(),
	   CPFStaging.NatureId,
	   CPFStaging.PhoneNumber1,
	   CPFStaging.PhoneNumber2,
	   CPFStaging.PhoneNumber3,
	   CPFStaging.Website,
	   CPFStaging.Foundedyear,
	   CPFStaging.CompanyStageId,
	    CPFStaging.NumberOfEmployee,
		CPFStaging.Headquarter,
		1 as IsHeadQuarters,
		 GETDATE(),
	   @AviraUserId,
	   0,
	   CPFStaging.CompanyRank_Rationale,
		CPFStaging.CompanyRank_Rank,
		null as EntityTypeId
From [StagingCompanyProfileFundamentals] CPFStaging;

--Share Holding Pattern
insert into [ShareHoldingPatternMap](
Id,
ShareHoldingName,
CompanyId,
EntityTypeId,
ShareHoldingPercentage,
CreatedOn,
UserCreatedById,
IsDeleted)
select newid(),
csh.Name,
csh.CompanyId,
csh.EntityTypeId,
csh.Holding,
getdate(),
@AviraUserId,
0
from [StagingCompanyProfileShareHolding] csh;

--Subsidiaries
insert into Company(Id,
CompanyCode,
CompanyName,
ParentCompanyYearOfEstb,
ProfiledCompanyName,
ParentId,
RankNumber,
RankRationale,
CompanyLocation,
IsHeadQuarters,
IsDeleted,
CreatedOn,
UserCreatedById)
SELECT NEWID(),
--CompanyCode,Commented by praveen
dbo.fun_NextCompanyCode(),
CPSStaging.CompanySubsdiaryName,
Company.YearOfEstb as ParentCompanyYearOfEstb,
Company.CompanyName as ParentCompanyName,
Company.Id as ParentId,
Company.RankNumber as CompanyRank_Rank,
Company.RankRationale as CompanyRank_Rationale,
CPSStaging.CompanySubsdiaryLocation,
0 as IsHeadQuarters,
0,
getdate(),
@AviraUserId
from [StagingCompanyProfileSubsidiaries] CPSStaging
inner join Company
on CPSStaging.CompanyId = Company.Id;


--Import for Organization 
with CTED as (select KeyEmployeeDesignation as Designation
			FROM [StagingCompanyKeyEmployees]
			WHERE KeyEmployeeDesignation != ''
			UNION 
			select BoardofDirectorOtherCompanyDesignation
			FROM [StagingCompanyBoardOfDirectors]
			WHERE BoardofDirectorOtherCompanyDesignation != ''
			)
insert into Designation
(Id,
Designation,
Description,
CreatedOn,
UserCreatedById,
IsVisible,
IsDeleted)
select distinct 
NEWID(),
CTED.Designation,
CTED.Designation,
getdate(),
@AviraUserId,
1,
0
from CTED
where Designation != ''
and not exists (select 1 from Designation d1
				where d1.Designation = CTED.Designation
				and d1.IsDeleted =0);

--Key Employees
Insert Into CompanyKeyEmployeesMap(
			Id,
			KeyEmployeeName,
			DesignationId,
			KeyEmployeeEmailId,
			KeyEmployeeComments,
			IsDirector,
			CompanyId,
			CreatedOn,
			UserCreatedById,
			IsDeleted)
Select NEWID(),
	   cgkemp.KeyEmployeeName,
	   D.Id as DesignationId,
	   cgkemp.KeyEmployeeEmail,
	   '' as KeyEmployeeComments,
	   0 as IsDirector,
	   cgkemp.CompanyId,
		 GETDATE(),
	   @AviraUserId,
	   0
From [StagingCompanyKeyEmployees] cgkemp
inner join Designation D
on cgkemp.KeyEmployeeDesignation = D.Designation;

;with CTE_Kemp as (SELECT KeyEmployeeName,
						KeyEmployeeEmail,
						D.Id as MGRDesignationID,
						D.Designation as MGRDesignation
					FROM [StagingCompanyKeyEmployees] cgkemp
					INNER JOIN Designation D
					ON cgkemp.ReportingManager = D.Designation
					)
update ckemp
set ManagerId = (select top 1 ID from CompanyKeyEmployeesMap mgr where mgr.DesignationId = CTE_Kemp.MGRDesignationID)
from CompanyKeyEmployeesMap ckemp
inner join CTE_Kemp
ON ckemp.KeyEmployeeName = CTE_Kemp.KeyEmployeeName
where ckemp.CompanyId = @CompanyId;

--BoDs
Insert Into CompanyKeyEmployeesMap(
			Id,
			KeyEmployeeName,
			DesignationId,
			KeyEmployeeEmailId,
			CompanyBoardOfDirecorsOtherCompanyName,
			CompanyBoardOfDirecorsOtherCompanyDesignation,
			IsDirector,
			CompanyId,
			CreatedOn,
			UserCreatedById,
			IsDeleted)
Select NEWID(),
	   cgkemp.BoardofDirectorName,
	   D.Id as DesignationId,
	   cgkemp.BoardofDirectorEmail,
	   cgkemp.BoardofDirectorOtherCompany,
	   cgkemp.BoardofDirectorOtherCompanyDesignation,
		1 as IsDirector,
		cgkemp.companyId,
		 GETDATE(),
	   @AviraUserId,
	   0
From [StagingCompanyBoardOfDirectors] cgkemp
inner join Designation D
on cgkemp.BoardofDirectorOtherCompanyDesignation = D.Designation;

--Product
;with CTE_Prod as(select ROW_NUMBER() over (partition by ProductServiceName order by id) as rnum,
				 *
				 from [StagingCompanyProduct])
insert into CompanyProduct(
Id,
ProductName,
LaunchDate,
Description,
Patents,
StatusId,
CompanyId,
ProjectId,
CreatedOn,
UserCreatedById,
IsDeleted)
select newid(),
ProductServiceName,
LaunchDate,
DecriptionRemarks,
Patents,
case when st.Status is null or st.Status = '' then (select top 1 id from ProductStatus where ProductStatus.ProductStatusName = 'Marketed')
	 else (select top 1 id from ProductStatus where ProductStatus.ProductStatusName =  st.Status) 
end as StatusId,
CompanyId,
null as ProjectId,
getdate(),
@AviraUserId,
0 as isDeleted
from CTE_Prod st
where rnum = 1;

INSERT INTO [dbo].[CompanyProductCategoryMap]
           ([Id]
           ,[CompanyProductId]
           ,[ProductCategoryId]
           ,[IsDeleted]
		   ,[CreatedOn]
           ,[UserCreatedById])
select NEWID(),
cp.Id,
scp.ProductCategoryId,
0 as [IsDeleted],
getdate(),
@AviraUserId
from CompanyProduct CP
inner join [StagingCompanyProduct] SCP
on SCP.ProductServiceName = cp.ProductName
where cp.CompanyId = @CompanyId;

INSERT INTO [dbo].[CompanyProductSegmentMap]
           ([Id]
           ,[CompanyProductId]
           ,[SegmentId]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
select NEWID(),
cp.Id,
scp.SegmentId,
getdate(),
@AviraUserId,
0 as [IsDeleted]
from CompanyProduct CP
inner join [StagingCompanyProduct] SCP
on SCP.ProductServiceName = cp.ProductName
where cp.CompanyId = @CompanyId;

;with CTE_SCP as (select ProductServiceName, GEO.Value as RegionName
		from [StagingCompanyProduct] SCP
		cross apply dbo.FUN_STRING_TOKENIZER(scp.TargetedGeogrpahy, ',') GEO ),
CTE_PRMap as (select ProductServiceName, Region.Id as RegionID, CRM.CountryId
			from CTE_SCP
			inner join Region
			on CTE_SCP.RegionName = Region.RegionName
			inner join CountryRegionMap CRM
			on CRM.RegionId = Region.Id)
INSERT INTO [dbo].[CompanyProductCountryMap]
           ([Id]
           ,[CompanyProductId]
           ,[CountryId]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
select distinct NewID(),
CP.Id,
CTE_PRMap.CountryId,
getdate(),
@AviraUserId,
0 as [IsDeleted]
from CompanyProduct CP
inner join CTE_PRMap
on CTE_PRMap.ProductServiceName = cp.ProductName
where cp.CompanyId = @CompanyId;

INSERT INTO [dbo].[CompanyProductValue]
           ([Id]
           ,[CompanyProductId]
           ,[Year]
           ,[CurrencyId]
           ,[ValueConversionId]
           ,[Amount])
select NEWID(),
CP.Id,
YEAR(getdate()),
SCP.CurrencyId,
SCP.ValueConversionId,
SCP.Revenue
from CompanyProduct CP
inner join [StagingCompanyProduct] SCP
on SCP.ProductServiceName = cp.ProductName
where cp.CompanyId = @CompanyId
and SCP.revenue is not null;

declare @SegmentId uniqueidentifier;
select @SegmentId = Id from Segment
where SegmentName = 'End User';

--Client 
INSERT INTO [dbo].[CompanyClientsMapping]
           ([Id]
           ,[CompanyId]
           ,[ClientName]
           ,[ClientIndustry]
           ,[ClientRemark]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted]
           )
select NEWID(),
       SCC.CompanyId,
       SCC.ClientName,
       (select top 1 ID from Subsegment ind where ind.SubSegmentName = SCC.ClientIndustry and ind.SegmentId = @SegmentId)ClientIndustryID,
       SCC.Remarks as  ClientRemark,
		getdate(),
		@AviraUserId as UserCreatedById,
0 as IsDeleted
FROM StagingCompanyClients SCC;

--Strategies
INSERT INTO [dbo].[CompanyStrategyMapping]
           ([Id]
           ,[CompanyId]
           ,[Date]
           ,[Approach]
           ,[Strategy]
           ,[Category]
           ,[Importance]
		   ,Impact
           ,[Remark]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
select newid(),
       Strategy.CompanyId as CompanyId,
       Strategy.Date,
       Strategy.ApproachId,
	   Strategy.Strategy,
       Strategy.CategoryId,
       Strategy.ImportanceId,
	   Strategy.ImpactId,
       Strategy.Remarks,
	   getdate() as CreatedOn,
       @AviraUserId as UserCreatedById,
       0 as IsDeleted
from StagingCompanyStrategy Strategy;

--News
INSERT INTO [dbo].[News]
           ([Id]
           ,[Date]
           ,[News]
           ,[NewsCategoryId]
           ,[ImportanceId]
           ,[ImpactDirectionId]
           ,[OtherParticipants]
           ,[Remarks]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted]
           ,[CompanyID])
     select
           newid(),
           News.NewsDate,
           News.News,
           News.CategoryId,
           News.ImportanceId, 
           News.ImpactId, 
           News.OtherParticipants, 
           News.Remarks,
		   getdate() as CreatedOn,
		   @AviraUserId as UserCreatedById,
		   0 as IsDeleted,
           News.CompanyID
from [StagingCompanyNews] News

--Ecosystem
INSERT INTO [dbo].[CompanyEcosystemPresenceESMMap]
           ([Id]
           ,[CompanyId]
           ,[IsChecked]
           ,[ESMId]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
select NEWID(),
    SCESM.CompanyId,
    1 AS IsChecked,
    Segment.ID as  ESMId,
    GETDATE(),
    @AviraUserId,
    0 as IsDeleted
FROM [StagingCompanyEcosystemPresence] SCESM
cross apply dbo.FUN_STRING_TOKENIZER(SCESM.ESMPresence, ',') ESMP
inner join Segment
on ESMP.Value = Segment.SegmentName;

INSERT INTO [dbo].[CompanyEcosystemPresenceSectorMap]
           ([Id]
           ,[CompanyId]
           ,[IsChecked]
           ,[SectorId]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted]
           )
select NEWID(),
    SCESM.CompanyId,
    1 AS IsChecked,
    Industry.ID as  [SectorId],
    GETDATE(),
    @AviraUserId,
    0 as IsDeleted
FROM [StagingCompanyEcosystemPresence] SCESM
cross apply dbo.FUN_STRING_TOKENIZER(SCESM.SectorPresence, ',') ESMP
inner join SubSegment Industry
on ESMP.Value = Industry.SubSegmentName
and Industry.SegmentId = @SegmentId;

INSERT INTO [dbo].[CompanyEcosystemPresenceKeyCompetitersMap]
           ([Id]
           ,[CompanyId]
           ,[KeyCompetiters]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
select NEWID(),
    SCESM.CompanyId,
    ESMP.Value,
    GETDATE(),
    @AviraUserId,
    0 as IsDeleted
FROM [StagingCompanyEcosystemPresence] SCESM
CROSS APPLY dbo.FUN_STRING_TOKENIZER(SCESM.ESMPresence,',') ESMP

--SWOT
INSERT INTO [dbo].[CompanySWOTMap]
           ([Id]
           ,[CompanyId]
		   ,[ProjectId]
           ,[CategoryId]
           ,[SWOTTypeId]
           ,[SWOTDescription]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
select NEWID(),
    SWOT.CompanyId,
	SWOT.ProjectId,
    SWOT.CategoryId,
    SWOT.SWOTTypeId,
     SWOT.AnalysisPoint as SWOTDescription,
    getdate() as CreatedOn,
    @AviraUserId,
	0 as IsDeleted
FROM [StagingCompanySWOT] SWOT;

--Trends
INSERT INTO [dbo].[CompanyTrendsMap]
           ([Id]
           ,[TrendName]
           ,[Department]
           ,[ImportanceId]
           ,[ImpactId]
           ,[CompanyId]
           ,[ProjectId]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted]
           )
SELECT NEWID(),
SCT.Trends,
SCT.[Department],
SCT.ImportanceId,
SCT.ImpactId,
SCT.CompanyId,
SCT.ProjectId,
getdate() as [CreatedOn],
@AviraUserId,
0 as [IsDeleted]
FROM [StagingCompanyTrends] SCT;

--Analyst Views
 INSERT INTO [dbo].[CompanyAnalystView]
           ([Id]
           ,[CompanyId]
           ,[ProjectId]
           ,[AnalystView]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
select NEWID(),
    SCAV.CompanyId,
	SCAV.ProjectId,
    SCAV.AnalystView,
    getdate() as CreatedOn,
    @AviraUserId,
	0 as IsDeleted
	FROM StagingCompanyAnalystView SCAV;

;with CTEProj as ( select projectID from StagingCompanyAnalystView union 
select projectID from StagingCompanySWOT union 
select projectID from StagingCompanyTrends)
INSERT INTO [dbo].[CompanyProjectMap]
           ([Id]
           ,[CompanyId]
           ,[ProjectId]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
select newid(),
@CompanyId,
CTEProj.ProjectId,
GETDATE(),
@AviraUserId,
0 as [IsDeleted]
from CTEProj
where ProjectId is not null;

declare @CompanyProfileStatusId uniqueidentifier;
select @CompanyProfileStatusId = ID
from CompanyProfileStatus cps
where cps.CompanyStatusName = 'Approved'

INSERT INTO [dbo].[CompanyNote]
           ([Id]
           ,[CompanyId]
           ,[ProjectId]
           ,[CompanyType]
           ,[AuthorRemark]
           ,[ApproverRemark]
           ,[CompanyStatusID]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
select NEWID(), @CompanyId, null as [ProjectId], 'CompanyOtherInfo' as [CompanyType], 'From Import' as [AuthorRemark], 'From Import' as [ApproverRemark], @CompanyProfileStatusId as CompanyStatusId, GETDATE(), @AviraUserId, 0 as [IsDeleted] union all
select NEWID(), @CompanyId, null as [ProjectId], 'CompanyShare' as [CompanyType], 'From Import' as [AuthorRemark], 'From Import' as [ApproverRemark], @CompanyProfileStatusId as CompanyStatusId, GETDATE(), @AviraUserId, 0 as [IsDeleted] union all
select NEWID(), @CompanyId, null as [ProjectId], 'CompanyClientAndStrategy' as [CompanyType], 'From Import' as [AuthorRemark], 'From Import' as [ApproverRemark], @CompanyProfileStatusId as CompanyStatusId, GETDATE(), @AviraUserId, 0 as [IsDeleted] union all
select NEWID(), @CompanyId, null as [ProjectId], 'CompanyNews' as [CompanyType], 'From Import' as [AuthorRemark], 'From Import' as [ApproverRemark], @CompanyProfileStatusId as CompanyStatusId, GETDATE(), @AviraUserId, 0 as [IsDeleted] union all
select NEWID(), @CompanyId, null as [ProjectId], 'CompanyProduct' as [CompanyType], 'From Import' as [AuthorRemark], 'From Import' as [ApproverRemark], @CompanyProfileStatusId as CompanyStatusId, GETDATE(), @AviraUserId, 0 as [IsDeleted] union all
select NEWID(), @CompanyId, null as [ProjectId], 'EcoSystemPresence' as [CompanyType], 'From Import' as [AuthorRemark], 'From Import' as [ApproverRemark], @CompanyProfileStatusId as CompanyStatusId, GETDATE(), @AviraUserId, 0 as [IsDeleted];

with CTEProj as ( select 'CompanyAnalystView' as [CompanyType], ProjectID from StagingCompanyAnalystView union 
select 'CompanySWOT' as [CompanyType], ProjectID from StagingCompanySWOT union 
select 'CompanyTrends' as [CompanyType], ProjectID from StagingCompanyTrends)
INSERT INTO [dbo].[CompanyNote]
           ([Id]
           ,[CompanyId]
           ,[ProjectId]
           ,[CompanyType]
           ,[AuthorRemark]
           ,[ApproverRemark]
           ,[CompanyStatusID]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])
select NEWID(), @CompanyId, [ProjectId], [CompanyType], 'From Import' as [AuthorRemark], 'From Import' as [ApproverRemark], @CompanyProfileStatusId as CompanyStatusId, GETDATE(), @AviraUserId, 0 as [IsDeleted] 
from CTEProj;

SELECT StoredProcedureName ='[SApp_insertAllCompanyProfileFromStagging]',Message =@ErrorMsg,Success = 1;     
COMMIT TRANSACTION
END TRY    
BEGIN CATCH    
 if @@TRANCOUNT > 0 
 ROLLBACK TRANSACTION;
 
    -- Execute the error retrieval routine.    
 DECLARE @ErrorNumber int;    
 DECLARE @ErrorSeverity int;    
 DECLARE @ErrorProcedure varchar(100);    
 DECLARE @ErrorLine int;    
 DECLARE @ErrorMessage varchar(500);    
    
  SELECT @ErrorNumber = ERROR_NUMBER(),    
        @ErrorSeverity = ERROR_SEVERITY(),    
        @ErrorProcedure = ERROR_PROCEDURE(),    
        @ErrorLine = ERROR_LINE(),    
        @ErrorMessage = ERROR_MESSAGE()    
    
 insert into dbo.Errorlog(ErrorNumber,    
       ErrorSeverity,    
       ErrorProcedure,    
       ErrorLine,    
       ErrorMessage,    
       ErrorDate)    
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());    
  
 set @ErrorMsg = 'Error while creating a new Company Profile, please contact Admin for more details.';    
--THROW 50000,@ErrorMsg,1;      
--return(1)  
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;     
     
END CATCH    
    
    
end