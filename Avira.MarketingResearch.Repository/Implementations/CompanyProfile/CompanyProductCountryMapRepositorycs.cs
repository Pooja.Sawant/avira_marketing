﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using static Avira.MarketingResearch.Model.ViewModels.CompanyProfile.CompanyProductCountryMapViewModel;

namespace Avira.MarketingResearch.Repository.Implementations.CompanyProfile
{
    public class CompanyProductCountryMapRepositorycs: ICompanyProductCountryMapRepository
    {
        protected IDbHelper _dbHelper;

        public CompanyProductCountryMapRepositorycs(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }
        public IEnumerable<CompanyProductCountryMapViewModel> GetAll(Guid? Id)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<CompanyProductCountryMapViewModel>(CompanyProductCountryMapQueries.StoreProcSelectAll
                            , new { @ComapanyProductID = Id, @includeDeleted = false }
                            , null, true, null, CommandType.StoredProcedure);
        }

        public Result<SpTransactionMessage> InsertUpdate(CompanyProductViewMoel companyProductViewMoel)
        {

            DataTable companyProductCountryMapTable = new DataTable();
            companyProductCountryMapTable.Columns.Add("ID", typeof(Guid));
            if (companyProductViewMoel != null)
            {
                for (int k = 0; k < companyProductViewMoel.ProductCountryId.Length; k++)
                {
                    companyProductCountryMapTable.Rows.Add(companyProductViewMoel.ProductCountryId[k]);
                }
            }
            var spTransactionMessage = _dbHelper.ExecuteQuery(CompanyProductCountryMapQueries.StoreProcInsertUpdate,
               new
               {
                   @CompanyProductId = companyProductViewMoel.Id,
                   @CompanyProductCountryMap = companyProductCountryMapTable,
                   @AviraUserId = companyProductViewMoel.UserCreatedById
               }
                    , CommandType.StoredProcedure);
            return spTransactionMessage;
        }

    }
}
