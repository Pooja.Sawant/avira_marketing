﻿CREATE PROCEDURE [dbo].[SFab_GetStrategyByCompanyID]             
 @ComapanyID uniqueidentifier = null, 
 @ImportanceList nvarchar(100) ,
 @CategoryIdlist dbo.udtUniqueIdentifier ReadOnly,        
 @includeDeleted bit = 0,
 @AviraUserID uniqueidentifier = null             
AS        
BEGIN         
 SET NOCOUNT ON;          
 SELECT [CompanyStrategyMapping].[Id]        
      ,[CompanyId] 
	  ,[Company].CompanyName
	  ,[Company].[RankNumber]       
      ,[Date]        
      ,[Approach] as ApproachId        
   ,Approach.ApproachName as ApproachName        
      ,[Strategy]        
      ,[CompanyStrategyMapping].[Category] as CategoryId        
   ,Category.CategoryName as CategoryName        
      ,[Importance] as ImportanceId        
   ,Importance.GroupName as ImportanceName        
      ,[Impact] as ImpactId        
   ,ImpactType.ImpactTypeName as ImpactName        
      ,[Remark]     
  FROM [dbo].[CompanyStrategyMapping]   
  INNER JOIN [Company] [Company] ON [Company].id=[CompanyStrategyMapping].[CompanyId]     
  LEFT JOIN [dbo].[Category] Category  ON [CompanyStrategyMapping].Category = [Category].Id        
  LEFT JOIN [dbo].[Importance] Importance ON [CompanyStrategyMapping].Importance = Importance.Id        
  LEFT JOIN [dbo].[ImpactType] ImpactType ON [CompanyStrategyMapping].Impact = ImpactType.Id        
  LEFT JOIN [dbo].[Approach] Approach ON [CompanyStrategyMapping].Approach = Approach.Id   
  WHERE ([CompanyStrategyMapping].CompanyId = @ComapanyID)
  AND (not exists(select value from STRING_SPLIT(@ImportanceList,',')) or Importance.GroupName IN (select value from STRING_SPLIT(@ImportanceList,','))) --(CASE WHEN (Importance.ImportanceName = 'VeryHigh' OR Importance.ImportanceName = 'High' ) THEN 'High' WHEN (Importance.ImportanceName = 'VeryLow' OR Importance.ImportanceName = 'Low' ) THEN 'Low' ELSE Importance.ImportanceName END) in (select imp.ImportanceName from @ImportanceIDList Im JOIN [dbo].[Importance] imp ON Im.ID = imp.Id))
  AND (not exists(select 1 from @CategoryIdlist) or [CompanyStrategyMapping].Category in (select ID from @CategoryIdlist))
 AND (@includeDeleted = 0 or [CompanyStrategyMapping].IsDeleted = 0)
 Order By Importance.Sequence desc
END