﻿
CREATE PROCEDURE [dbo].[SApp_EcoSystemGridWithFilters]
	(@EcoSystemIDList dbo.udtUniqueIdentifier READONLY,
	@EcoSystemName nvarchar(256)='',
	@Description nvarchar(256)='',
	@includeDeleted bit = 0,
	@OrdbyByColumnName NVARCHAR(50) ='CreatedDate',
	@SortDirection INT = -1,
	@PageStart INT = 0,
	@PageSize INT = null)
	AS
/*================================================================================
Procedure Name: SApp_EcoSystemGridWithFilters
Author: Harshal
Create date: 04/02/2019
Description: Get list from EcoSystem(Segment) table by Name or ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
04/02/2019	Harshal			Initial Version
__________	____________	______________________________________________________
04/22/2019	Pooja			For Client side paging comment pagesize and null init
================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
if @includeDeleted is null set @includeDeleted = 0;
if len(@EcoSystemName)>2 set @EcoSystemName = '%'+ @EcoSystemName + '%'
else set @EcoSystemName = ''
if len(@Description)>2 set @Description = '%'+ @Description + '%';
else set @Description = '';
	if @PageSize is null or @PageSize =0
	set @PageSize = 10
	begin
	;WITH CTE_TBL AS (

	SELECT Segment.[Id],
		Segment.SegmentName as [EcoSystemName],
		Segment.[Description],
		Segment.[IsDeleted],
		CASE
		WHEN @OrdbyByColumnName = 'EcoSystemName' THEN Segment.SegmentName
		WHEN @OrdbyByColumnName = 'Description' THEN Segment.[Description]
		ELSE Segment.SegmentName
	END AS SortColumn

FROM [dbo].Segment
where (NOT EXISTS (SELECT * FROM @EcoSystemIDList) or  Segment.Id  in (select * from @EcoSystemIDList))
	AND (@EcoSystemName = '' or Segment.SegmentName like @EcoSystemName)
	AND (@Description = '' or Segment.[Description] like @Description)
	AND (@includeDeleted = 1 or Segment.IsDeleted = 0)
	)

	SELECT 
		  CTE_TBL.*,
		  tCountResult.TotalItems
		FROM CTE_TBL
		CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult
		ORDER BY 
		  CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,
		  CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC 
		OFFSET @PageStart ROWS
		FETCH NEXT @PageSize ROWS ONLY;
		END
	END