﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using static Avira.MarketingResearch.Model.ViewModels.ComponentViewModel;

namespace Avira.MarketingResearch.Repository.Implementations
{
   public class ComponentRepository:IComponentRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "Component";
        public ComponentRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public ComponentViewModel GetById(Guid guid)
        {
            ComponentViewModel rawMaterialViewModel = new ComponentViewModel();
            rawMaterialViewModel = GetAllComponent(guid).FirstOrDefault();
            return rawMaterialViewModel;
        }

        public IEnumerable<ComponentViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted)
        {
            return GetAllComponent(null, PageStart, pageSize, SortDirection, OrdbyByColumnName,Search, includeDeleted);
        }
        public IEnumerable<ComponentViewModel> GetAllComponent(Guid? guid, int PageStart = 0, int pageSize = 0, int SortDirection = -1, string OrdbyByColumnName = "", string Search = "", bool includeDeleted = false)
        {
            DataTable componentIdsTable = new DataTable();
            componentIdsTable.Columns.Add("ID", typeof(Guid));

            if (guid != null)
            {
                componentIdsTable.Rows.Add(guid);

            }
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<ComponentViewModel>(ComponentQueries.StoreProcGetAll,
                 new { @ComponentIDList = componentIdsTable.AsTableValuedParameter("dbo.udtUniqueIdentifier"), @includeDeleted = includeDeleted, @PageStart = PageStart, @pageSize = pageSize, @SortDirection = SortDirection, @OrdbyByColumnName = OrdbyByColumnName, @ComponentName=Search },
                null, true, null, CommandType.StoredProcedure).Where(x => x.ComponentName != null);

        }

        public Result<SpTransactionMessage> Create(ComponentInsertDbViewModel componentDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(ComponentQueries.StoreProcInsert,
                componentDbViewModel,
                CommandType.StoredProcedure);
            return spTransactionMessage;

        }

        public Result<SpTransactionMessage> Update(ComponentUpdateDbViewModel componentDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(ComponentQueries.StoreProcUpdate,
                componentDbViewModel,
                CommandType.StoredProcedure);
            return spTransactionMessage;
        }
    }
}
