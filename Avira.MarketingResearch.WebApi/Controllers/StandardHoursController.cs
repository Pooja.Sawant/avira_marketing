﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.Masters;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces.Masters;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StandardHoursController : AviraBaseController
    {
        private ILoggerManager _logger;
        public readonly IStandardHoursRepository _standardHoursRepository;
        public readonly IStandardHoursService _standardHoursService;
        public StandardHoursController(
            IStandardHoursService standardHoursService,
            IStandardHoursRepository standardHoursRepository,
            ILoggerManager logger)
        {
            _standardHoursRepository = standardHoursRepository;
            _standardHoursService = standardHoursService;
            _logger = logger;
        }
        [HttpGet]
        public ActionResult<IEnumerable<StandardHoursViewModel>> Get(int PageStart = 0, int pageSize = 0, int SortDirection = 1, string OrdbyByColumnName = "CreatedDate", string Search = "", bool isDeletedInclude = false)
        {
            List<StandardHoursViewModel> res = null;
            try
            {
                res = _standardHoursService.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }


        [HttpGet("{id}")]
        public ActionResult<StandardHoursViewModel> Get(Guid id)
        {
            StandardHoursViewModel standardHours = null;
            try
            {
                standardHours = _standardHoursService.GetById(id);
                if (standardHours == null)
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return standardHours;
        }


        [HttpPut]
        public ActionResult Put([FromBody] StandardHoursUpdateDbViewModel viewModel)
        {

            viewModel.ModifiedOn = System.DateTime.UtcNow;
            viewModel.UserModifiedById = CurrentUser.Id;


            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while updating a  Standard Hours, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                Result<SpTransactionMessage> result = _standardHoursService.Update(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Standard Hours name " + viewModel.StandardHoursName + " is updated successfully.";

                }
                else
                {
                    viewData.Message = result.Error;
                }
            }

            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }

    }
}