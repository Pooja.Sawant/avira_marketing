﻿using Avira.MarketingResearch.Models.Entities;
using Avira.MarketingResearch.Models.Response;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Avira.MarketingResearch.Service.CoreInterfaces
{
    public interface IRawMaterialService
    {
        Task<RawMaterialResponse> GetAsync(Guid Id);
        Task<RawMaterialResponse> GetAllAsync();    
        Task AddAsync(RawMaterial rawMaterial);
        Task UpdateAsync(RawMaterial rawMaterial);
        void Add(RawMaterial rawMaterial);
        void DeleteAsync(RawMaterial rawMaterial);
    }
}
