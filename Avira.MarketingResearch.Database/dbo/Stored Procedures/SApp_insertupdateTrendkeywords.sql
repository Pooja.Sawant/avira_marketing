﻿    
    
CREATE procedure [dbo].[SApp_insertupdateTrendkeywords]  
(        
@TrendKeyWordList dbo.udtGUIDVarcharValue READONLY,        
@TrendId uniqueidentifier,     
@UserCreatedById uniqueidentifier,      
@CreatedOn Datetime    
)        
as        
/*================================================================================        
Procedure Name: SApp_insertupdateTrendkeywords  
Change History        
Date  Developer  Reason        
__________ ____________ ______________________________________________________        
04/16/2019 Gopi   Initial Version        
================================================================================*/        
BEGIN        
SET NOCOUNT ON;        
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)    
  
--Validations        
--IF exists(select 1 from [dbo].[TrendKeywords]         
--    AND IsDeleted = 0)        
--begin           
--set @ErrorMsg = 'TrendKeyWord with Name "'+ @TrendKeyWordList + '" already exists.';           
--SELECT StoredProcedureName ='SApp_insertupdateTrendkeywords',Message =@ErrorMsg,Success = 0;      
--RETURN;      
--end        
--End of Validations        
BEGIN TRY           
           
delete [dbo].[TrendKeywords]  
where [TrendId] = @TrendId  
and not exists (select 1 from @TrendKeyWordList list  
    where list.Value = [TrendKeywords].[TrendKeyWord])  
  
INSERT INTO [dbo].[TrendKeywords]([Id],        
        [TrendKeyWord],        
  [TrendId],    
        [IsDeleted],        
        [UserCreatedById],        
        [CreatedOn])        
 SELECT NEWID(),         
    Value,         
   @TrendId,    
   0,        
   @UserCreatedById,         
   @CreatedOn  
   from @TrendKeyWordList  list  
   where not exists (select 1 from [TrendKeywords]   
    where list.Value = [TrendKeywords].[TrendKeyWord])  
  
SELECT StoredProcedureName ='SApp_insertupdateTrendkeywords',Message =@ErrorMsg,Success = 1;         
      
END TRY        
BEGIN CATCH        
    -- Execute the error retrieval routine.        
 DECLARE @ErrorNumber int;        
 DECLARE @ErrorSeverity int;        
 DECLARE @ErrorProcedure varchar(100);        
 DECLARE @ErrorLine int;        
 DECLARE @ErrorMessage varchar(500);        
        
  SELECT @ErrorNumber = ERROR_NUMBER(),        
        @ErrorSeverity = ERROR_SEVERITY(),        
        @ErrorProcedure = ERROR_PROCEDURE(),        
        @ErrorLine = ERROR_LINE(),        
        @ErrorMessage = ERROR_MESSAGE()        
        
 insert into dbo.Errorlog(ErrorNumber,        
       ErrorSeverity,        
       ErrorProcedure,        
       ErrorLine,        
       ErrorMessage,        
       ErrorDate)        
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,@CreatedOn);        
      
 set @ErrorMsg = 'Error while creating a new Trend, please contact Admin for more details.';        
    
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine      
    ,Message =@ErrorMsg,Success = 0;         
         
END CATCH        
        
        
end