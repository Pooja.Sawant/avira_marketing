﻿using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Avira.MarketingResearch.Web.ImportExportUtility
{
    public class ImportQualitativeAnalysis : IExcelImport
    {
        public readonly IServiceRepository _serviceRepository;
        private ILoggerManager _logger;
        public ImportQualitativeAnalysis(IServiceRepository serviceRepository, ILoggerManager logger)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
        }

        public ImportResponse Import(IFormFile formFile)
        {
            var res = new ImportResponse();
            var inputModel = new QualitativeAnalysisImportModel();
            StringBuilder sb;
            using (var stream = new MemoryStream())
            {
                formFile.CopyTo(stream);

                using (var package = new ExcelPackage(stream))
                {
                    try
                    {
                        if (package.Workbook.Worksheets["News"] != null)
                        {
                            inputModel.NewsList = GetNews(package.Workbook.Worksheets["News"]);
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'News' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'News' worksheet is missing.");
                            return res;
                        }
                        if (package.Workbook.Worksheets["Analysis"] != null)
                        {
                            inputModel.AnalysisList = GetAnalysis(package.Workbook.Worksheets["Analysis"]);
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'Analysis' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'Analysis' worksheet is missing.");
                            return res;
                        }
                        if (package.Workbook.Worksheets["Description"] != null)
                        {
                            inputModel.DescriptionList = GetDescription(package.Workbook.Worksheets["Description"]);
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'Description' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'Description' worksheet is missing.");
                            return res;
                        }
                        if (package.Workbook.Worksheets["Quotes"] != null)
                        {
                            inputModel.QuoteList = GetQuotes(package.Workbook.Worksheets["Quotes"]);
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'Quotes' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'Quotes' worksheet is missing.");
                            return res;
                        }
                        if (package.Workbook.Worksheets["Segments"] != null)
                        {
                            inputModel.SegmentsList = GetSegment(package.Workbook.Worksheets["Segments"]);
                            if (inputModel.SegmentsList.Count() == 0)
                            {
                                res.ResponseStatus = "Error";
                                res.ResponseMessage = "Segments Mapping sheet is mandatory.";
                                res.TotalFailedRecords = 1;
                                _logger.LogError("Segments Mapping sheet is mandatory.");
                                return res;
                            }
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'Segments' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'Segments' worksheet is missing.");
                            return res;
                        }
                        if (package.Workbook.Worksheets["Region"] != null)
                        {
                            inputModel.RegionList = GetRegion(package.Workbook.Worksheets["Region"]);
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'Region' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'Region' worksheet is missing.");
                            return res;
                        }
                        if (package.Workbook.Worksheets["Ecosystem Segment Mapping"] != null)
                        {
                            inputModel.ESMSegmentList = GetESMSegment(package.Workbook.Worksheets["Ecosystem Segment Mapping"],out sb);
                            if (sb.Length != 0)
                            {
                                res.ResponseStatus = "Error";
                                res.ResponseMessage = sb.ToString();
                                res.TotalFailedRecords = 1;
                                _logger.LogError(sb.ToString());
                                return res;

                            }
                            if (inputModel.ESMSegmentList.Count()==0 )
                            {
                                res.ResponseStatus = "Error";
                                res.ResponseMessage = "Ecosystem segment Mapping sheet is mandatory.";
                                res.TotalFailedRecords = 1;
                                _logger.LogError("Ecosystem segment Mapping sheet is mandatory.");
                                return res;
                            }
                            if (inputModel.ESMSegmentList !=null && inputModel.ESMSegmentList.Select(x => x.SegmentName).Distinct().Count() > 10)
                            {
                                res.ResponseStatus = "Error";
                                res.ResponseMessage = "More than 10 segments are not allowed in Ecosystem segment Mapping sheet.";
                                res.TotalFailedRecords = 1;
                                _logger.LogError("More than 10 segments are not allowed in Ecosystem segment Mapping sheet.");
                                return res;
                            }
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'Ecosystem segment mapping' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'Ecosystem segment mapping' worksheet is missing.");
                            return res;
                        }
                        if (package.Workbook.Worksheets["Analyst Views"] != null)
                        {
                            inputModel.ESMAnalysisList = GetESMAnalysis(package.Workbook.Worksheets["Analyst Views"]);
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'Analyst Views' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'Analyst Views' worksheet is missing.");
                            return res;
                        }
                    }
                    catch (Exception ex)
                    {
                        res.ResponseStatus = "Error";
                        res.ResponseMessage = "Error while reading data. Error Message: " + ex.Message + ". Please check input excel. More details logged in error log.";
                        res.TotalFailedRecords = 1;
                        _logger.LogError(ex.Message);
                        return res;
                    }
                }
            }

            //--------Call api to post data            
            inputModel.TemplateName = "Qualitative Analysis Import Template";
            inputModel.ImportFileName = formFile.FileName;

            try
            {

                HttpResponseMessage response = _serviceRepository.PostResponse("QualitativeAnalysisImportExport", inputModel);
                response.EnsureSuccessStatusCode();
                ImportCommonResponseMainModel viewModel = response.Content.ReadAsAsync<ImportCommonResponseMainModel>().Result;

                res.ResponseStatus = viewModel.IsSuccess ? "Success" : "Failed";
                res.ResponseMessage = viewModel.Message;
                res.TotalRecords = inputModel.NewsList.Count();
                if (viewModel.ResponseModel == null)
                {
                    res.ResponseStatus = "Success";
                    res.TotalSuccessRecords = 1;
                    res.TotalFailedRecords = 0;
                }
                else
                {
                    res.ResponseMessage = "Please check downloaded file for inserted record";
                    //res.TotalSuccessRecords = ((inputModel.NewsList == null ? 0 : inputModel.NewsList.Count())) - (viewModel.NewsList.Count());
                    res.TotalFailedRecords = viewModel.ResponseModel.Count();
                }
                res.ResponseModel = viewModel.ResponseModel;
            }
            catch (Exception ex)
            {
                res.ResponseStatus = "Error";
                res.ResponseMessage = "Error while reading data. Error Message: " + ex.Message + ". Please check input excel. More details logged in error log.";
                res.TotalFailedRecords = 1;
                _logger.LogError(ex.Message);
            }
            
            return res;
        }


        private List<QualitativeNewsImportModel> GetNews(ExcelWorksheet worksheet)
        {
            List<QualitativeNewsImportModel> CPnews = new List<QualitativeNewsImportModel>();

            var rowCount = worksheet.Dimension.Rows;

            
            int colIndexNewsImpact = GetColIndex(worksheet, "NewsImpact");
            int colIndexNewsImportance = GetColIndex(worksheet, "NewsImportance");
            int colIndexMarket_Name = GetColIndex(worksheet, "Market_Name");
            int colIndexSubmarket_Name = GetColIndex(worksheet, "Submarket_Name");
            int colIndexNews_Category = GetColIndex(worksheet, "News_Category");
            int colIndexNews_Date = GetColIndex(worksheet, "News_Date");
            int colIndexNewsText = GetColIndex(worksheet, "NewsText");

            for (int row = 7; row <= rowCount; row++)
            {
                QualitativeNewsImportModel CompanyNews = null;
                var cn = worksheet.Cells[row, 3].Value?.ToString().Trim();

                if (!string.IsNullOrEmpty(cn))
                {
                    CompanyNews = new QualitativeNewsImportModel
                    {
                        RowNo = row,
                        MarketName = worksheet.Cells[row, colIndexMarket_Name].Value?.ToString().Trim(),
                        SubMarketName = worksheet.Cells[row, colIndexSubmarket_Name].Value?.ToString().Trim(),
                        NewsCategory = worksheet.Cells[row, colIndexNews_Category].Value?.ToString().Trim(),
                        NewsDate = Convert.ToDateTime(worksheet.Cells[row, colIndexNews_Date].Value?.ToString().Trim()),
                        News = worksheet.Cells[row, colIndexNewsText].Value?.ToString().Trim(),
                        Impact = worksheet.Cells[row, colIndexNewsImpact].Value?.ToString().Trim(),
                        Importance = worksheet.Cells[row, colIndexNewsImportance].Value?.ToString().Trim()
                    };
                    CPnews.Add(CompanyNews);
                }
            }
            return CPnews;
        }

        private List<AnalysisImportModel> GetAnalysis(ExcelWorksheet worksheet)
        {
            List<AnalysisImportModel> CPAnalysis = new List<AnalysisImportModel>();

            var rowCount = worksheet.Dimension.Rows;

           
            int colIndexMarket_Name = GetColIndex(worksheet, "Market_Name");
            int colIndexSubmarket_Name = GetColIndex(worksheet, "Submarket_Name");
            int colIndexAnalysis_Category = GetColIndex(worksheet, "Analysis_Category");
            int colIndexAnalysis_subcategory = GetColIndex(worksheet, "Analysis_subcategory");
            int colIndexAnalysisText = GetColIndex(worksheet, "AnalysisText");
            int colIndexAnalysisImpact = GetColIndex(worksheet, "AnalysisImpact");
            int colIndexAnalysisImportance = GetColIndex(worksheet, "AnalysisImportance");

            for (int row = 7; row <= rowCount; row++)
            {
                AnalysisImportModel CompanyNews = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

                if (!string.IsNullOrEmpty(cn))
                {
                    CompanyNews = new AnalysisImportModel
                    {
                        RowNo = row,
                        MarketName = worksheet.Cells[row, colIndexMarket_Name].Value?.ToString().Trim(),
                        SubMarketName = worksheet.Cells[row, colIndexSubmarket_Name].Value?.ToString().Trim(),
                        Analysis_Category = worksheet.Cells[row, colIndexAnalysis_Category].Value?.ToString().Trim(),
                        Analysis_subcategory = worksheet.Cells[row, colIndexAnalysis_subcategory].Value?.ToString().Trim(),
                        AnalysisText = worksheet.Cells[row, colIndexAnalysisText].Value?.ToString().Trim(),
                        Impact = worksheet.Cells[row, colIndexAnalysisImpact].Value?.ToString().Trim(),
                        Importance = worksheet.Cells[row, colIndexAnalysisImportance].Value?.ToString().Trim()

                    };
                    CPAnalysis.Add(CompanyNews);
                }
            }
            return CPAnalysis;
        }

        private List<QualitativeDescriptionImportModel> GetDescription(ExcelWorksheet worksheet)
        {
            List<QualitativeDescriptionImportModel> Descriptions = new List<QualitativeDescriptionImportModel>();

            var rowCount = worksheet.Dimension.Rows;

            int colIndexMarket_Name = GetColIndex(worksheet, "Market_Name");
            int colIndexSubmarket_Name = GetColIndex(worksheet, "Submarket_Name");
            int colIndexDescription_Category = GetColIndex(worksheet, "Description_Category");
            int colIndexDescription_Subcategory = GetColIndex(worksheet, "Description_Subcategory");
            int colIndexDescriptionText = GetColIndex(worksheet, "DescriptionText");
          
            for (int row = 7; row <= rowCount; row++)
            {
                QualitativeDescriptionImportModel CompanyNews = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

                if (!string.IsNullOrEmpty(cn))
                {
                    CompanyNews = new QualitativeDescriptionImportModel
                    {
                        RowNo = row,
                        MarketName = worksheet.Cells[row, colIndexMarket_Name].Value?.ToString().Trim(),
                        SubMarketName = worksheet.Cells[row, colIndexSubmarket_Name].Value?.ToString().Trim(),
                        Description_Category = worksheet.Cells[row, colIndexDescription_Category].Value?.ToString().Trim(),
                        Description_Subcategory = worksheet.Cells[row, colIndexDescription_Subcategory].Value?.ToString().Trim(),
                        DescriptionText = worksheet.Cells[row, colIndexDescriptionText].Value?.ToString().Trim()
                      
                    };
                    Descriptions.Add(CompanyNews);
                }
            }
            return Descriptions;
        }

        private List<QualitativeQuoteImportModel> GetQuotes(ExcelWorksheet worksheet)
        {
            List<QualitativeQuoteImportModel> Descriptions = new List<QualitativeQuoteImportModel>();

            var rowCount = worksheet.Dimension.Rows;
            int colIndexMarket_Name = GetColIndex(worksheet, "Market_Name");
            int colIndexSubmarket_Name = GetColIndex(worksheet, "Submarket_Name");
            int colIndexResourceName = GetColIndex(worksheet, "ResourceName");
            int colIndexResourceCompany = GetColIndex(worksheet, "ResourceCompany");
            int colIndexResourceDesignation = GetColIndex(worksheet, "ResourceDesignation");
            int colIndexDate_of_quote = GetColIndex(worksheet, "Date_of_quote");
            int colIndexQuote = GetColIndex(worksheet, "Quote");
            int colOther_Remarks = GetColIndex(worksheet, "Other_Remarks");

            for (int row = 7; row <= rowCount; row++)
            {
                QualitativeQuoteImportModel CompanyNews = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

                if (!string.IsNullOrEmpty(cn))
                {
                    CompanyNews = new QualitativeQuoteImportModel
                    {
                        RowNo = row,
                        MarketName = worksheet.Cells[row, colIndexMarket_Name].Value?.ToString().Trim(),
                        SubMarketName = worksheet.Cells[row, colIndexSubmarket_Name].Value?.ToString().Trim(),
                        ResourceName = worksheet.Cells[row, colIndexResourceName].Value?.ToString().Trim(),
                        ResourceCompany = worksheet.Cells[row, colIndexResourceCompany].Value?.ToString().Trim(),
                        ResourceDesignation = worksheet.Cells[row, colIndexResourceDesignation].Value?.ToString().Trim(),
                        DateOfQuote = Convert.ToDateTime(worksheet.Cells[row, colIndexDate_of_quote].Value?.ToString().Trim()),
                        Quote = worksheet.Cells[row, colIndexQuote].Value?.ToString().Trim(),
                        OtherRemarks = worksheet.Cells[row, colOther_Remarks].Value?.ToString().Trim()
                    };
                    Descriptions.Add(CompanyNews);
                }
            }
            return Descriptions;
        }

        private List<QualitativeQuoteImportModel> GetDescriptions(ExcelWorksheet worksheet)
        {
            List<QualitativeQuoteImportModel> Descriptions = new List<QualitativeQuoteImportModel>();

            var rowCount = worksheet.Dimension.Rows;

           
            int colIndexMarket_Name = GetColIndex(worksheet, "Market_Name");
            int colIndexSubmarket_Name = GetColIndex(worksheet, "Submarket_Name");
            int colIndexResourceName = GetColIndex(worksheet, "ResourceName");
            int colIndexResourceCompany = GetColIndex(worksheet, "ResourceCompany");
            int colIndexResourceDesignation = GetColIndex(worksheet, "ResourceDesignation");
            int colIndexDate_of_quote = GetColIndex(worksheet, "Date_of_quote");
            int colIndexQuote = GetColIndex(worksheet, "Quote");
            int colOther_Remarks = GetColIndex(worksheet, "Other_Remarks");

            for (int row = 7; row <= rowCount; row++)
            {
                QualitativeQuoteImportModel CompanyNews = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

                if (!string.IsNullOrEmpty(cn))
                {
                    CompanyNews = new QualitativeQuoteImportModel
                    {
                        RowNo = row,
                        MarketName = worksheet.Cells[row, colIndexMarket_Name].Value?.ToString().Trim(),
                        SubMarketName = worksheet.Cells[row, colIndexSubmarket_Name].Value?.ToString().Trim(),
                        ResourceName = worksheet.Cells[row, colIndexResourceName].Value?.ToString().Trim(),
                        ResourceCompany = worksheet.Cells[row, colIndexResourceCompany].Value?.ToString().Trim(),
                        ResourceDesignation = worksheet.Cells[row, colIndexResourceDesignation].Value?.ToString().Trim(),
                        DateOfQuote = Convert.ToDateTime(worksheet.Cells[row, colIndexDate_of_quote].Value?.ToString().Trim()),
                        Quote = worksheet.Cells[row, colIndexQuote].Value?.ToString().Trim(),
                        OtherRemarks = worksheet.Cells[row, colOther_Remarks].Value?.ToString().Trim()
                    };
                    Descriptions.Add(CompanyNews);
                }
            }
            return Descriptions;
        }

        private List<QualitativeRegionsImportModel> GetRegion(ExcelWorksheet worksheet)
        {
            List<QualitativeRegionsImportModel> Region = new List<QualitativeRegionsImportModel>();

            var rowCount = worksheet.Dimension.Rows;

            int colIndexMarket = GetColIndex(worksheet, "Market");
            int colIndexRegionName = GetColIndex(worksheet, "RegionName");
            int colIndexCountries = GetColIndex(worksheet, "Countries");
           
            for (int row = 7; row <= rowCount; row++)
            {
                QualitativeRegionsImportModel CompanyNews = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

                if (!string.IsNullOrEmpty(cn))
                {
                    CompanyNews = new QualitativeRegionsImportModel
                    {
                        RowNo = row,
                        MarketName = worksheet.Cells[row, colIndexMarket].Value?.ToString().Trim(),
                        RegionName = worksheet.Cells[row, colIndexRegionName].Value?.ToString().Trim(),
                        CountryName = worksheet.Cells[row, colIndexCountries].Value?.ToString().Trim()
                    };
                    Region.Add(CompanyNews);
                }
            }
            return Region;
        }

        private List<QualitativeSegmentsImportModel> GetSegment(ExcelWorksheet worksheet)
        {
            List<QualitativeSegmentsImportModel> objSegments = new List<QualitativeSegmentsImportModel>();

            var rowCount = worksheet.Dimension.Rows;
            int colIndexMarket = GetColIndex(worksheet, "Market Name");
            int colIndexSegmentName = GetColIndex(worksheet, "SegmentName");
            int colIndexSubSegmentName = GetColIndex(worksheet, "SubSegmentName");
            int colIndexParentName = GetColIndex(worksheet, "ParentName");

            for (int row = 7; row <= rowCount; row++)
            {
                QualitativeSegmentsImportModel Segments = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

                //if (!string.IsNullOrEmpty(cn))
                //{
                    Segments = new QualitativeSegmentsImportModel
                    {
                        RowNo = row,
                        MarketName = worksheet.Cells[row, colIndexMarket].Value?.ToString().Trim(),
                        SegmentName = worksheet.Cells[row, colIndexSegmentName].Value?.ToString().Trim(),
                        SubSegmentName = worksheet.Cells[row, colIndexSubSegmentName].Value?.ToString().Trim(),
                        ParentName = worksheet.Cells[row, colIndexParentName].Value?.ToString().Trim()
                    };
                    objSegments.Add(Segments);
                //}
            }
            return objSegments;

            #region Old Code Before 3rd Level Segments
            //var segmentList = GetSegmentList(worksheet, 6);

            //int colIndexRawMaterial = GetColIndex(worksheet, "Raw materials");
            //int colIndexComponent = GetColIndex(worksheet, "Components");
            //int colIndexProduct = GetColIndex(worksheet, "Products & Parts");
            //int colIndexDevices = GetColIndex(worksheet, "Devices");
            //int colIndexTechnology = GetColIndex(worksheet, "Technology");
            //int colIndexApplications = GetColIndex(worksheet, "Applications");
            //int colIndexEndUsers = GetColIndex(worksheet, "End User");
            //int colIndexServices = GetColIndex(worksheet, "Services");
            //int colIndexDeployment = GetColIndex(worksheet, "Deployment");
            //int colIndexSoftware = GetColIndex(worksheet, "Software");
            //int colIndexProtocols = GetColIndex(worksheet, "Protocols");
            //int colIndexPricing = GetColIndex(worksheet, "Pricing");
            //int colIndexDimensions = GetColIndex(worksheet, "Dimensions");
            //int colIndexType = GetColIndex(worksheet, "Type");
            //int colIndexSources = GetColIndex(worksheet, "Sources");
            //int colIndexEquipment = GetColIndex(worksheet, "Equipment");
            //int colIndexPartsAndAccessories = GetColIndex(worksheet, "Parts and Accessories");
            //int colIndexRating = GetColIndex(worksheet, "Rating");
            //int colIndexClass = GetColIndex(worksheet, "Class");
            //int colIndexForm = GetColIndex(worksheet, "Form");
            //int colIndexChannel = GetColIndex(worksheet, "Channel");
            //int colIndexSize = GetColIndex(worksheet, "Size");
            //int colIndexOther1 = GetColIndex(worksheet, "Other1");
            //int colIndexOther2 = GetColIndex(worksheet, "Other2");
            //int colIndexOther3 = GetColIndex(worksheet, "Other3");
            //int colIndexOther4 = GetColIndex(worksheet, "Other4");
            //int colIndexOther5 = GetColIndex(worksheet, "Other5");

            //for (int row = 7; row <= rowCount; row++)
            //{
            //    var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

            //    if (!string.IsNullOrEmpty(cn))
            //    {
            //        if (segmentList != null)
            //        {
            //            for (var i = 0; i < segmentList.Count(); i++)
            //            {
            //                int colIndex = GetColIndex(worksheet, segmentList[i]);
            //                GetSegmentColumnwise(worksheet, objSegments, colIndex, row, cn);
            //            }
            //        }
            //        //GetSegmentColumnwise(worksheet, objSegments, colIndexRawMaterial, row, cn);
            //        //GetSegmentColumnwise(worksheet, objSegments, colIndexComponent, row, cn);
            //        //GetSegmentColumnwise(worksheet, objSegments, colIndexProduct, row, cn);
            //        //GetSegmentColumnwise(worksheet, objSegments, colIndexDevices, row, cn);
            //        //GetSegmentColumnwise(worksheet, objSegments, colIndexTechnology, row, cn);
            //        //GetSegmentColumnwise(worksheet, objSegments, colIndexApplications, row, cn);
            //        //GetSegmentColumnwise(worksheet, objSegments, colIndexEndUsers, row, cn);
            //        //GetSegmentColumnwise(worksheet, objSegments, colIndexServices, row, cn);
            //        //GetSegmentColumnwise(worksheet, objSegments, colIndexDeployment, row, cn);
            //        //GetSegmentColumnwise(worksheet, objSegments, colIndexSoftware, row, cn);
            //        //GetSegmentColumnwise(worksheet, objSegments, colIndexProtocols, row, cn);
            //        //GetSegmentColumnwise(worksheet, objSegments, colIndexPricing, row, cn);
            //        //GetSegmentColumnwise(worksheet, objSegments, colIndexDimensions, row, cn);
            //        //GetSegmentColumnwise(worksheet, objSegments, colIndexType, row, cn);
            //        //GetSegmentColumnwise(worksheet, objSegments, colIndexSources, row, cn);
            //        //GetSegmentColumnwise(worksheet, objSegments, colIndexEquipment, row, cn);
            //        //GetSegmentColumnwise(worksheet, objSegments, colIndexPartsAndAccessories, row, cn);
            //        //GetSegmentColumnwise(worksheet, objSegments, colIndexRating, row, cn);
            //        //GetSegmentColumnwise(worksheet, objSegments, colIndexClass, row, cn);
            //        //GetSegmentColumnwise(worksheet, objSegments, colIndexForm, row, cn);
            //        //GetSegmentColumnwise(worksheet, objSegments, colIndexChannel, row, cn);
            //        //GetSegmentColumnwise(worksheet, objSegments, colIndexSize, row, cn);
            //        //GetSegmentColumnwise(worksheet, objSegments, colIndexOther1, row, cn);
            //        //GetSegmentColumnwise(worksheet, objSegments, colIndexOther2, row, cn);
            //        //GetSegmentColumnwise(worksheet, objSegments, colIndexOther3, row, cn);
            //        //GetSegmentColumnwise(worksheet, objSegments, colIndexOther4, row, cn);
            //        //GetSegmentColumnwise(worksheet, objSegments, colIndexOther5, row, cn);

            //    }
            //}
            //return objSegments;
            #endregion
        }

        private static void GetSegmentColumnwise(ExcelWorksheet worksheet, List<QualitativeSegmentsImportModel> objSegments, int colIndex, int row, string cn)
        {
            var subSegmentName = worksheet.Cells[row, colIndex].Value?.ToString().Trim();
            if (!string.IsNullOrEmpty(subSegmentName))
            {
                var segment = new QualitativeSegmentsImportModel
                {
                    RowNo = row,
                    MarketName = worksheet.Cells[row, 1].Value?.ToString().Trim(),
                    SegmentName = worksheet.Cells[6, colIndex].Value?.ToString().Trim(),
                    SubSegmentName = subSegmentName
                };
                objSegments.Add(segment);
            }
        }

        private List<string> GetSegmentList(ExcelWorksheet worksheet, int rowIndex)
        {
            List<string> YearList = new List<string>();
            var columnCount = worksheet.Dimension.Columns;
            for (int colIndex = 2; colIndex <= columnCount; colIndex++)
            {
                if (worksheet.Cells[rowIndex, colIndex].Value != null)
                {
                    YearList.Add(worksheet.Cells[rowIndex, colIndex].Value.ToString());
                }
            }
            return YearList;
        }


        private int GetColIndex(ExcelWorksheet worksheet, string colName)
        {
            int colIndex = worksheet.Cells["6:6"].First(c => c.Value.ToString() == colName).Start.Column;

            return colIndex;
        }
        private static byte[] GetBytes(ExcelPackage file)
        {
            using (var stream = new MemoryStream())
            {
                file.SaveAs(stream);
                return stream.ToArray();
            }
        }

        private List<ESMAnalysisImportModel> GetESMAnalysis(ExcelWorksheet worksheet)
        {
            List<ESMAnalysisImportModel> ESMAnalysisList = new List<ESMAnalysisImportModel>();

            var rowCount = worksheet.Dimension.Rows;


            int colIndexMarket_Name = GetColIndex(worksheet, "Market_Name");
            int colIndexSegment_Name = GetColIndex(worksheet, "Segment_Name");
            int colIndexSubsegment_Name = GetColIndex(worksheet, "Subsegment_Name");
            int colIndexAnalysis_Category = GetColIndex(worksheet, "Analysis_Category");
            int colIndexAnalysisText = GetColIndex(worksheet, "AnalysisText");
            int colIndexAnalysisImpact = GetColIndex(worksheet, "AnalysisImpact");
            int colIndexAnalysisImportance = GetColIndex(worksheet, "AnalysisImportance");

            for (int row = 7; row <= rowCount; row++)
            {
                ESMAnalysisImportModel ESMAnalysis = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

                //if (!string.IsNullOrEmpty(cn))
                //{
                    ESMAnalysis = new ESMAnalysisImportModel
                    {
                        RowNo = row,
                        MarketName = worksheet.Cells[row, colIndexMarket_Name].Value?.ToString().Trim(),
                        SegmentName = worksheet.Cells[row, colIndexSegment_Name].Value?.ToString().Trim(),
                        SubSegmentName = worksheet.Cells[row, colIndexSubsegment_Name].Value?.ToString().Trim(),
                        Analysis_Category = worksheet.Cells[row, colIndexAnalysis_Category].Value?.ToString().Trim(),
                        AnalysisText = worksheet.Cells[row, colIndexAnalysisText].Value?.ToString().Trim(),
                        Impact = worksheet.Cells[row, colIndexAnalysisImpact].Value?.ToString().Trim(),
                        Importance = worksheet.Cells[row, colIndexAnalysisImportance].Value?.ToString().Trim()

                    };
                    ESMAnalysisList.Add(ESMAnalysis);
               // }
            }
            return ESMAnalysisList;
        }

        private List<ESMSegmentsImportModel> GetESMSegment(ExcelWorksheet worksheet,out StringBuilder sb)
        {
            sb = new StringBuilder();
            List<ESMSegmentsImportModel> objESMSegments = new List<ESMSegmentsImportModel>();

            var rowCount = worksheet.Dimension.Rows;
            int colIndexMarket = GetColIndex(worksheet, "Market Name");
            int colIndexSegmentName = GetColIndex(worksheet, "SegmentName");
            int colIndexSubSegmentName = GetColIndex(worksheet, "SubSegmentName");
            int colIndexParentName = GetColIndex(worksheet, "ParentName");

            for (int row = 7; row <= rowCount; row++)
            {
                ESMSegmentsImportModel ESMSegments = null;
                var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

               // if (!string.IsNullOrEmpty(cn))
                //{
                    ESMSegments = new ESMSegmentsImportModel
                    {
                        RowNo = row,
                        MarketName = worksheet.Cells[row, colIndexMarket].Value?.ToString().Trim(),
                        SegmentName= worksheet.Cells[row, colIndexSegmentName].Value?.ToString().Trim(),
                        SubSegmentName = worksheet.Cells[row, colIndexSubSegmentName].Value?.ToString().Trim(),
                        ParentName = worksheet.Cells[row, colIndexParentName].Value?.ToString().Trim()
                    };
                    objESMSegments.Add(ESMSegments);
                //}
            }
            return objESMSegments;

            #region Old Code Before 3rd Level Segment

            //sb = new StringBuilder();
            //List<ESMSegmentsImportModel> objESMSegments = new List<ESMSegmentsImportModel>();
            //var rowCount = worksheet.Dimension.Rows;
            //var ESMsegmentList = GetSegmentList(worksheet, 6);
            //for (int row = 7; row <= rowCount; row++)
            //{
            //    var cn = worksheet.Cells[row, 1].Value?.ToString().Trim();

            //    //if (!string.IsNullOrEmpty(cn))
            //    //{
            //        if (ESMsegmentList != null)
            //        {
            //            for (var i = 0; i < ESMsegmentList.Count(); i++)
            //            {
            //                int colIndex = GetColIndex(worksheet, ESMsegmentList[i]);
            //                GetESMSegmentColumnwise(worksheet, objESMSegments, colIndex, row, cn);
            //            }
            //        }
            //    //}
            //    if (!string.IsNullOrEmpty(cn))
            //    {
            //        var result = objESMSegments.Where(t => t.RowNo == row);
            //        if (result == null || result.Count() == 0)
            //        {
            //            sb.Append("Minimum 1 segment is required for rowno " + row + ".\n");
            //        }
            //    }
            //    else
            //    {
            //        var result = objESMSegments.Where(t => t.RowNo == row);
            //        if (result == null || result.Count() == 0)
            //        {
            //            sb.Append("Project Name/Market Name is mandatory in Ecosytem Segment Mapping sheet rowno " + row + ".\n");
            //        }
            //    }

            //}
            //return objESMSegments;
            #endregion
        }

        private static void GetESMSegmentColumnwise(ExcelWorksheet worksheet, List<ESMSegmentsImportModel> objSegments, int colIndex, int row, string cn)
        {
            var subSegmentName = worksheet.Cells[row, colIndex].Value?.ToString().Trim();
            if (!string.IsNullOrEmpty(subSegmentName))
            {
                var segment = new ESMSegmentsImportModel
                {
                    RowNo = row,
                    MarketName = worksheet.Cells[row, 1].Value?.ToString().Trim(),
                    SegmentName = worksheet.Cells[6, colIndex].Value?.ToString().Trim(),
                    SubSegmentName = subSegmentName
                };
                objSegments.Add(segment);
            }
        }

    }
}
