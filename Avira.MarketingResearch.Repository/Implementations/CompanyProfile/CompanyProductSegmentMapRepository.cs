﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.CompanyProfile
{
    public class CompanyProductSegmentMapRepository: ICompanyProductSegmentMapRepository
    {
        protected IDbHelper _dbHelper;

        public CompanyProductSegmentMapRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public IEnumerable<CompanyProductSegmentMapViewModel> GetAll(Guid? Id)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<CompanyProductSegmentMapViewModel>(CompanyProductSegmentMapQueries.StoreProcSelectAll
                            , new { @ComapanyProductID = Id, @includeDeleted = false}
                            , null, true, null, CommandType.StoredProcedure); 
        }

        public Result<SpTransactionMessage> InsertUpdate(CompanyProductViewMoel companyProductViewMoel)
        {

            DataTable companyProductSegmentMapTable = new DataTable();
            companyProductSegmentMapTable.Columns.Add("ID", typeof(Guid));
            if (companyProductViewMoel != null)
            {
                for (int k = 0; k < companyProductViewMoel.ProductSegmentId.Length; k++)
                {
                    companyProductSegmentMapTable.Rows.Add(companyProductViewMoel.ProductSegmentId[k]);
                }
            }
            var spTransactionMessage = _dbHelper.ExecuteQuery(CompanyProductSegmentMapQueries.StoreProcInsertUpdate,
               new
               {
                   @CompanyProductId = companyProductViewMoel.Id,
                   @CompanyProductSegmentMap = companyProductSegmentMapTable.AsTableValuedParameter("dbo.udtUniqueIdentifier"),
                   @AviraUserId = companyProductViewMoel.UserCreatedById
               }
                    , CommandType.StoredProcedure);
            return spTransactionMessage;
        }
    }
}

