﻿CREATE procedure [dbo].[SApp_GetCompanyDetailsbyProjectID]
(
	@CompanyId uniqueidentifier,
	@ProjectId uniqueidentifier
)
as
/*================================================================================
Procedure Name: [SApp_GetCompanyDetailsbyProjectID]
Author: Pooja
Create date: 05/06/2019
Description: Get list from company table by project ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
05/06/2019	Sai Krishna	    Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	 select comp.Id as CompanyId,
	 comp.CompanyName,
	 comp.CompanyCode,
	 comp.DisplayName,
	 comp.LogoURL,
	 comp.NatureId,
	 comp.ParentCompanyName,
	 comp.ParentId,
	 comp.CompanyLocation,
	 comp.IsHeadQuarters,
	 comp.Telephone,
	 comp.Telephone2,
	 comp.Telephone3,
	 comp.Email,
	 comp.Email2,
	 comp.Email3,
	 comp.Website,
	 comp.YearOfEstb,
	 comp.CompanyStageId,
	 comp.NoOfEmployees,
	 comp.RankNumber,
	 comp.RankRationale,
	 comp.DisplayName,
	 comp.LogoURL,
	 comp.ActualImageName as ImageActualName
	from Company comp
	LEFT JOIN CompanyProjectMap cpmap ON cpmap.CompanyId = comp.Id AND  cpmap.ProjectId = @ProjectId AND comp.IsDeleted = 0
	--and exists (select 1 from CompanyProjectMap cpmap
	--where comp.Id = cpmap.CompanyId 
	----and cpmap.ProjectId = @ProjectId 
	--and comp.IsDeleted = 0 );
where comp.IsDeleted = 0 and comp.Id = @CompanyId
	--Subsidary Companies
	SELECT [Id] as CompanySubsidaryId,
		[CompanyName] as SubsidaryCompanyName,
		[CompanyLocation] as SubsidaryCompanyLocation,		
		[IsHeadQuarters]
		FROM [dbo].[Company]
		where [Company].ParentId = @CompanyID 
		and [Company].[IsDeleted] = 0
		and [Company].IsHeadQuarters = 0


	--Regions
	select 
	crm.RegionId
	from CompanyRegionMap crm
	where crm.CompanyId = @CompanyId
	and crm.IsDeleted = 0;
--Key employees
	select 
	ckem.Id,
	 ckem.KeyEmployeeName,
	 ckem.DesignationId,
	 ckem.[ManagerId],
	 mgr.KeyEmployeeName as ManagerName,
	 Designation.Designation as KeyEmployeeDesignation,
	 ckem.KeyEmployeeEmailId,
	 ckem.KeyEmployeeComments
	from CompanyKeyEmployeesMap ckem
	inner join Designation
	on Designation.Id = ckem.DesignationId
	left join CompanyKeyEmployeesMap mgr
	on ckem.[ManagerId] = mgr.id
	where ckem.CompanyId = @CompanyId
	and ckem.[IsDirector] = 0 
	and ckem.IsDeleted = 0;

--Directors
	select 
	 cbdm.Id ,
	 cbdm.DesignationId,
	 cbdm.[ManagerId],
	 cbdm.KeyEmployeeName,
	 cbdm.CompanyBoardOfDirecorsOtherCompanyName,
	 cbdm.CompanyBoardOfDirecorsOtherCompanyDesignation,
	 cbdm.KeyEmployeeEmailId,
	 cbdm.[ManagerId],
	 mgr.KeyEmployeeName as ManagerName
	from CompanyKeyEmployeesMap cbdm
	inner join Designation
	on Designation.Id = cbdm.DesignationId
	left join CompanyKeyEmployeesMap mgr
	on cbdm.[ManagerId] = mgr.id
	where cbdm.CompanyId = @CompanyId
	and cbdm.[IsDirector] = 1
	and cbdm.IsDeleted = 0;

--Share Holding
	select 
	cshpm.Id,
	cshpm.ShareHoldingName,
	 cshpm.EntityTypeId,
	 cshpm.ShareHoldingPercentage
	from ShareHoldingPatternMap cshpm
	where cshpm.CompanyId = @CompanyId
end