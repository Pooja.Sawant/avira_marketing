﻿
CREATE procedure [dbo].[SFab_EcoSystemPresenceCompetitersByCompanyID] 
(@CompanyID uniqueidentifier,
@includeDeleted bit = 0,
@UserId uniqueidentifier = null)
as
/*================================================================================
Procedure Name: SFab_EcoSystemPresenceCompetitersByCompanyID
Author: Jagan
Create date: 26/06/2019
Description: Get list from CompanyEcosystemPresenceKeyCompetitersMap table by CompanyID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
26/06/2019	JAGAN			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT [Id],
	   [KeyCompetiters],
	   [CompanyId]
FROM [dbo].[CompanyEcosystemPresenceKeyCompetitersMap]
where (CompanyId = @CompanyID)
and ( IsDeleted = 0)

end