﻿CREATE TABLE [dbo].[CompanyCashFlows] (
    [ID]                 UNIQUEIDENTIFIER NOT NULL,
    [CompanyFinancialID] UNIQUEIDENTIFIER NOT NULL,
    [CashFlowType]       NVARCHAR (100)   NULL,
    [Year]               INT              NULL,
    [ActualAmount]       DECIMAL (18, 4)  NULL,
    [ConversionAmount]   DECIMAL (18, 4)  NULL,
    [CreatedOn]          DATETIME         NULL,
    [UserCreatedById]    UNIQUEIDENTIFIER NULL,
    [ModifiedOn]         DATETIME         NULL,
    [UserModifiedById]   UNIQUEIDENTIFIER NULL,
    [IsDeleted]          BIT              NULL,
    [DeletedOn]          DATETIME         NULL,
    [UserDeletedById]    UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CompanyCashFlows] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_CompanyCashFlows_CompanyFinancialAnalysis] FOREIGN KEY ([CompanyFinancialID]) REFERENCES [dbo].[CompanyFinancialAnalysis] ([ID])
);

