﻿

CREATE procedure [dbo].[SFab_GetQualitativeAnalysisDatabyID]
(@ProjectID uniqueidentifier=null,
@QualitativeAnalysisTypeId uniqueidentifier=null,
@includeDeleted bit = 0,
@AviraUserID uniqueidentifier = null)
as
/*================================================================================
Procedure Name: SFab_GetQualitativeAnalysisDatabyID
Author: Harshal 
Create date: 09/04/2019
Description: Get list from QualitativeAnalysis table by Filters
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
09/04/2019	Harshal			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if @includeDeleted is null set @includeDeleted = 0

SELECT [QualitativeAnalysis].[Id]
      ,[QualitativeAnalysis].[ProjectId]
	  ,[ProjectName]
      ,[QualitativeAnalysis].[MarketId]
	  ,[MarketName]
	  ,[QualitativeAnalysisTypeName]
      ,[QualitativeAnalysisTypeId]
	  ,[QualitativeAnalysisSubTypeName]
      ,[QualitativeAnalysisSubTypeId]
      ,[ImportanceId]
	  ,[GroupName] As[ImportanceName]
      ,[ImpactId]
	  ,[ImpactDirectionName]
      ,[AnalysisText]
		
FROM [dbo].[QualitativeAnalysis]
INNER JOIN Project Project ON Project.ID=QualitativeAnalysis.ProjectId
LEFT JOIN Market Market ON Market.Id=QualitativeAnalysis.MarketId
INNER JOIN  QualitativeAnalysisType QT ON QT.ID=QualitativeAnalysis.QualitativeAnalysisTypeId
LEFT JOIN  QualitativeAnalysisSubType QST ON QST.ID=QualitativeAnalysis.QualitativeAnalysisSubTypeId
LEFT JOIN  Importance Importance ON Importance.ID=QualitativeAnalysis.ImportanceId
LEFT JOIN  ImpactDirection ImpactDirection ON ImpactDirection.ID=QualitativeAnalysis.ImpactId

where (@ProjectId is null or QualitativeAnalysis.ProjectId = @ProjectID)
AND (@QualitativeAnalysisTypeId is null or QualitativeAnalysis.QualitativeAnalysisTypeId=@QualitativeAnalysisTypeId)
and (@includeDeleted = 1 or QualitativeAnalysis.IsDeleted = 0)
order by 

CASE WHEN  QualitativeAnalysisTypeName = 'PEST' THEN 
	CASE WHEN QualitativeAnalysisSubTypeName LIKE 'P%' THEN 1
	 WHEN QualitativeAnalysisSubTypeName LIKE 'E%' THEN 2
	 WHEN QualitativeAnalysisSubTypeName LIKE 'S%' THEN 3
	 WHEN QualitativeAnalysisSubTypeName LIKE 'T%' THEN 4 
	ELSE 0 END 
 ELSE 0 END,

 CASE WHEN  QualitativeAnalysisTypeName = 'DROC' THEN 
	CASE 
	 WHEN QualitativeAnalysisSubTypeName LIKE 'D%' THEN 1
	 WHEN QualitativeAnalysisSubTypeName LIKE 'R%' THEN 2
	 WHEN QualitativeAnalysisSubTypeName LIKE 'O%' THEN 3
	 WHEN QualitativeAnalysisSubTypeName LIKE 'C%' THEN 4
	
	ELSE 0 END 
 ELSE 0 END,

  CASE WHEN  QualitativeAnalysisTypeName = 'SWOT Analysis' THEN 
	CASE 
	 WHEN QualitativeAnalysisSubTypeName LIKE 'S%' THEN 1
	 WHEN QualitativeAnalysisSubTypeName LIKE 'W%' THEN 2
	 WHEN QualitativeAnalysisSubTypeName LIKE 'O%' THEN 3
	 WHEN QualitativeAnalysisSubTypeName LIKE 'T%' THEN 4
	
	ELSE 0 END 
 ELSE 0 END,

CASE WHEN Importance.Sequence < 5 then 1
WHEN Importance.Sequence < 5 then 1
WHEN Importance.Sequence >= 5 AND Importance.Sequence < 9 then 2
WHEN Importance.Sequence >= 9 then 3 ELSE 0 END
 desc
 ,
 QualitativeAnalysisSubTypeName, AnalysisText


end