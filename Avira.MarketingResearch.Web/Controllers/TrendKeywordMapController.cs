﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.Models;
using Avira.MarketingResearch.Web.Utils;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;

namespace Avira.MarketingResearch.Web.Controllers
{
    [NoCache]
    [Authorization]
    public class TrendKeywordMapController : BaseController
    {
        public readonly IServiceRepository _serviceRepository;
        private ILoggerManager _logger;

        public TrendKeywordMapController(IServiceRepository serviceRepository, ILoggerManager logger)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
        }

        public IActionResult Index()
        {
            SetRoleList();
            return View();
        }

        public ActionResult GetTrendKeywords(string id)
        {
            SetRoleList();
            //ViewBag.trendName = trendName;
            ViewBag.trendId = id;
            try
            {
                Guid Id = new Guid(id);
                ViewBag.Id = Id;
                //model.Id = trendId;
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.GetTrendByIdResponse(contollerName, Id);
                response.EnsureSuccessStatusCode();
                List<TrendKeyWordMapViewModel> trend = response.Content.ReadAsAsync<List<TrendKeyWordMapViewModel>>().Result;
                //string name = trend.Distinct().Select(tn => tn.TrendName).First();
                //ViewBag.TrendName = name;
                //ViewBag.Trends = trend;

                if(trend.Count!=0)
                {
                    string name = trend.Distinct().Select(tn => tn.TrendName).First();
                    ViewBag.TrendName = name;
                    ViewBag.Trends = trend;
                }
                else
                {
                    HttpResponseMessage trendResponse = _serviceRepository.GetTrendNameByIdResponse(contollerName, Id);
                    response.EnsureSuccessStatusCode();
                    TrendKeywordNames trendName = trendResponse.Content.ReadAsAsync<TrendKeywordNames>().Result;
                    ViewBag.TrendName = trendName.TrendName;
                }

                //---Get TrendName & ProjectName
                HttpResponseMessage responseTrend = _serviceRepository.GetByIdResponse("Trend", id);

                TrendViewModel trendProjectName = responseTrend.Content.ReadAsAsync<TrendViewModel>().Result;
                if(trend.Count != 0)
                {
                    trend.First().TrendModel = trendProjectName ?? new TrendViewModel();
                    ViewBag.ProjectId = trendProjectName.ProjectID;
                }
                else
                {
                    TrendViewModel trendModel = trendProjectName ?? new TrendViewModel();
                    HttpContext.Session.SetComplexData("Test", trendModel);
                    ViewBag.ProjectId = trendModel.ProjectID;
                }
                //

                //------Get trend note by trend Id     
                TrendKeyWordMapViewModel model = new TrendKeyWordMapViewModel();
                HttpResponseMessage responseTrendNote = _serviceRepository.GetByIdResponse("TrendNote", id, "keyword", "id");
                responseTrendNote.EnsureSuccessStatusCode();
                var tnm = responseTrendNote.Content.ReadAsAsync<TrendNoteViewModel>().Result;
                model.TrendNoteModel = tnm ?? new TrendNoteViewModel();
                model.TrendModel = responseTrend.Content.ReadAsAsync<TrendViewModel>().Result ?? new TrendViewModel();
                if (trend != null && trend.Count > 0)
                {
                    trend.First().TrendModel.TrendStatusName = model.TrendNoteModel.TrendStatusName;
                }
                
                ViewBag.NoteApprover = model.TrendNoteModel.ApproverRemark;
                ViewBag.Author = model.TrendNoteModel.AuthorRemark;
                ViewBag.TrendNoteID = model.TrendNoteModel.Id;
                HttpContext.Session.SetString("TrendNoteID", model.TrendNoteModel.Id.ToString());

                //Project Details//
                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, ViewBag.ProjectId);
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                }
                //
                return View(trend);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                ErrorViewModel err = new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier };
                return View("Error", err);
            }
        }

        [HttpPost]
        public IActionResult GetTrendKeywords(IFormCollection form, TrendKeyWordMapInsertDbViewModel1 model, string actionName)
        {
            var trendId = Request.Form["Id"];
            string trendName = string.Empty;
            var tNoteId = ViewBag.FormName = HttpContext.Session.GetString("TrendNoteID");
            Guid trendNoteId = new Guid(tNoteId);
            ViewBag.Message = string.Empty;
            bool bResult = false;
            if (ModelState.IsValid)
            {
                try
                {
                    model.TrendNoteModel = new TrendNoteViewModel { TrendType = "keyword", ApproverRemark = model.ApproverRemark, AuthorRemark = model.AuthorRemark, Id = trendNoteId };

                    if (actionName.Equals("Submitted"))
                    {
                        bResult = validate(model);
                        if (bResult)
                        {
                            model.TrendNoteModel.TrendStatusName = "Submitted";
                            trendName = Request.Form["TrendName"];
                            List<TrendKeywordNames> lst = new List<TrendKeywordNames>();
                            foreach (var item in Request.Form["TrendKeyWord"])
                            {
                                lst.Add(new TrendKeywordNames()
                                {
                                    TrendName = item
                                });
                            }
                            var userReq = new TrendKeyWordMapInsertDbViewModel1()
                            {
                                TrendId = new Guid(Request.Form["Id"]),
                                TrendNames = lst,
                                UserCreatedById = model.UserCreatedById,
                                CreatedOn = model.CreatedOn,
                                TrendStatusName = model.TrendStatusName,
                                ApproverRemark = model.ApproverRemark,
                                AuthorRemark = model.AuthorRemark,
                                TrendNoteModel = model.TrendNoteModel,
                            };
                            var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                            HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, userReq);
                            response.EnsureSuccessStatusCode();
                            ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                            if (viewModel != null)
                            {
                                ViewBag.Message = viewModel.Message;
                            }
                        }
                    }
                    else if (actionName.Equals("Approve"))
                    {
                        bResult = validate(model);
                        if (bResult)
                        {
                            model.TrendNoteModel.TrendStatusName = "Approved";
                            trendName = Request.Form["TrendName"];
                            List<TrendKeywordNames> lst = new List<TrendKeywordNames>();
                            foreach (var item in Request.Form["TrendKeyWord"])
                            {
                                lst.Add(new TrendKeywordNames()
                                {
                                    TrendName = item
                                });
                            }
                            var userReq = new TrendKeyWordMapInsertDbViewModel1()
                            {
                                TrendId = new Guid(Request.Form["Id"]),
                                TrendNames = lst,
                                UserCreatedById = model.UserCreatedById,
                                CreatedOn = model.CreatedOn,
                                TrendStatusName = model.TrendStatusName,
                                ApproverRemark = model.ApproverRemark,
                                AuthorRemark = model.AuthorRemark,
                                TrendNoteModel = model.TrendNoteModel
                            };
                            var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                            HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, userReq);
                            response.EnsureSuccessStatusCode();
                            ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                            if (viewModel != null)
                            {
                                ViewBag.Message = viewModel.Message;
                            }
                        }
                    }
                    else if (actionName.Equals("Rejected"))
                    {
                        bResult = validate(model);
                        if (bResult)
                        {
                            model.TrendNoteModel.TrendStatusName = "Rejected";
                            trendName = Request.Form["TrendName"];
                            List<TrendKeywordNames> lst = new List<TrendKeywordNames>();
                            foreach (var item in Request.Form["TrendKeyWord"])
                            {
                                lst.Add(new TrendKeywordNames()
                                {
                                    TrendName = item
                                });
                            }
                            var userReq = new TrendKeyWordMapInsertDbViewModel1()
                            {
                                TrendId = new Guid(Request.Form["Id"]),
                                TrendNames = lst,
                                UserCreatedById = model.UserCreatedById,
                                CreatedOn = model.CreatedOn,
                                TrendStatusName = model.TrendStatusName,
                                ApproverRemark = model.ApproverRemark,
                                AuthorRemark = model.AuthorRemark,
                                TrendNoteModel = model.TrendNoteModel
                            };
                            var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                            HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, userReq);
                            response.EnsureSuccessStatusCode();
                            ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                            if (viewModel != null)
                            {
                                ViewBag.Message = viewModel.Message;
                            }
                        }
                    }
                    else if(actionName.Equals("save"))
                    {
                        bResult = validate(model);
                        if (bResult)
                        {
                            model.TrendNoteModel.TrendStatusName = "Draft";
                            trendName = Request.Form["TrendName"];
                            List<TrendKeywordNames> lst = new List<TrendKeywordNames>();
                            foreach (var item in Request.Form["TrendKeyWord"])
                            {
                                lst.Add(new TrendKeywordNames()
                                {
                                    TrendName = item
                                });
                            }
                            var userReq = new TrendKeyWordMapInsertDbViewModel1()
                            {
                                TrendId = new Guid(Request.Form["Id"]),
                                TrendNames = lst,
                                UserCreatedById = model.UserCreatedById,
                                CreatedOn = model.CreatedOn,
                                TrendStatusName = model.TrendStatusName,
                                ApproverRemark = model.ApproverRemark,
                                AuthorRemark = model.AuthorRemark,
                                TrendNoteModel = model.TrendNoteModel
                            };
                            var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                            HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, userReq);
                            response.EnsureSuccessStatusCode();
                            ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                            if (viewModel != null)
                            {
                                ViewBag.Message = viewModel.Message;
                            }
                        }
                    }

                    else if (actionName.Equals("search"))
                    {
                        return RedirectToAction("Index", new { id = model.TrendId.ToString(), search = model.TrendKeyWord });
                    }
                    else if (actionName.Equals("Next"))
                        return RedirectToAction("Index", "TrendIndustryMap", new { id = trendId });
                    else if (actionName.Equals("Previous"))
                        return RedirectToAction("Edit", "Trend", new { id = trendId });
                    else if (actionName.Equals("Cancel"))
                    {
                        return RedirectToAction("GetTrendKeywords", "TrendKeywordMap", new { TrendName = trendName });
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                }
            }
            return RedirectToAction("GetTrendKeywords", "TrendKeywordMap", new { TrendName = trendName });
        }
        public ActionResult LoadData(TrendKeyWordMapViewModel keyword)
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            int totalRecords = 0;
            List<TrendKeyWordMapViewModel> data = null;
            try
            {
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                string search = Request.Form["search[value]"];

                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDir = Request.Form["order[0][dir]"].FirstOrDefault();
                var SortDirection = -1;
                if (sortColumnDir == "asc")
                {
                    SortDirection = 1;
                }
                else
                {
                    SortDirection = -1;
                }

                int skip = start != null ? Convert.ToInt32(start) : 0;
                int pageSize = length.ToString() != null ? Convert.ToInt32(length) : 0;
                int PageStart = start.ToString() != null ? Convert.ToInt32(start) : 0;
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();

                HttpResponseMessage response = _serviceRepository.GetResponse(contollerName, PageStart, pageSize, SortDirection, sortColumn, search);
                response.EnsureSuccessStatusCode();
                List<TrendKeyWordMapViewModel> trendKeywords = response.Content.ReadAsAsync<List<TrendKeyWordMapViewModel>>().Result;
                ViewBag.Title = "All Keywords";
                data = trendKeywords;
             
                foreach (var item in trendKeywords)
                {
                    totalRecords = item.TotalItems;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data });

        }

        public IActionResult Create()
        {
            try
            {
                SetRoleList();
                LoadDdlTrendNames();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View();
        }

        [HttpPost]
        public ActionResult Create(TrendKeyWordMapInsertDbViewModel trendKeyWordMap)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                    HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, trendKeyWordMap);
                    response.EnsureSuccessStatusCode();
                    ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                    if (viewModel != null)
                    {
                        ViewBag.Message = viewModel.Message;
                        trendKeyWordMap.TrendKeyWord = "";
                        trendKeyWordMap.TrendNames = null;
                        trendKeyWordMap.TrendId = new Guid();
                        ModelState.Clear();
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                }
            }
            LoadDdlTrendNames();
            return View(trendKeyWordMap);
        }

        private void LoadDdlTrendNames()
        {
            try
            {
                TrendKeyWordMapInsertDbViewModel model = new TrendKeyWordMapInsertDbViewModel();
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.GetAllParentResponse(contollerName);
                List<TrendKeywordNames> trendNamesList = response.Content.ReadAsAsync<List<TrendKeywordNames>>().Result;
                ViewBag.TrendNamesList = new SelectList(trendNamesList, "Id", "TrendName");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        [HttpGet]
        public IActionResult Delete(string id)
        {
            SetRoleList();
            try
            {
                LoadDdlTrendNames();
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.GetByIdResponse(contollerName, id);
                response.EnsureSuccessStatusCode();
                TrendKeyWordMapUpdateDbViewModel trendKeyword = response.Content.ReadAsAsync<TrendKeyWordMapUpdateDbViewModel>().Result;
                return View(trendKeyword);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                ErrorViewModel err = new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier };
                return View("Error", err);
            }
        }

        [HttpPost]
        public ActionResult Delete(TrendKeyWordMapUpdateDbViewModel test)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                    HttpResponseMessage response = _serviceRepository.DeleteResponse(contollerName, test);
                    response.EnsureSuccessStatusCode();
                    ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                    if (viewModel != null)
                    {
                        ViewBag.Message = viewModel.Message;
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                }
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Edit(string id)
        {
            SetRoleList();
            try
            {
                LoadDdlTrendNames();
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.GetByIdResponse(contollerName, id);
                response.EnsureSuccessStatusCode();
                TrendKeyWordMapUpdateDbViewModel trendtKeywordMapUpdate = response.Content.ReadAsAsync<TrendKeyWordMapUpdateDbViewModel>().Result;
                return View(trendtKeywordMapUpdate);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                ErrorViewModel err = new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier };
                return View("Error", err);
            }
        }

        [HttpPost]
        public IActionResult Edit(TrendKeyWordMapUpdateDbViewModel trendKeywordUpdate)
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.PutResponse(contollerName, trendKeywordUpdate);
                ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                if (viewModel != null)
                {
                    ViewBag.Message = viewModel.Message;
                    trendKeywordUpdate.TrendKeyWord = "";
                }
                response.EnsureSuccessStatusCode();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            LoadDdlTrendNames();
            return View(trendKeywordUpdate);
        }

        private void SetRoleList()
        {
            try
            {
                var roles = HttpContext.Session.GetString("userRoles");
                var userRoles = JsonConvert.DeserializeObject<List<RoleViewModel>>(roles);
                List<string> RoleListItems = new List<string>();
                foreach (var item in userRoles)
                    RoleListItems.Add(item.UserRoleName);
                ViewBag.UserRoleList = RoleListItems;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        /// <summary>
        /// Method to validate Form
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private bool validate(TrendKeyWordMapInsertDbViewModel1 model)
        { 
            if (model.TrendNoteModel.AuthorRemark == null || model.TrendNoteModel.AuthorRemark == "" || model.TrendNoteModel.AuthorRemark == string.Empty)
            {
                ViewBag.Message = "Please enter Author Remark!!";
                return false;
            }
            else
            {
                ViewBag.Message = string.Empty;
                return true;
            }
        }
    }
}