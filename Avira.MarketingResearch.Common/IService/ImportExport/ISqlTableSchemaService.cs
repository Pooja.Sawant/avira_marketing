﻿using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.ImportExport
{
    public interface ISqlTableSchemaService
    {
        DataTable GetTableSchema(SchemaModel model);
    }
}
