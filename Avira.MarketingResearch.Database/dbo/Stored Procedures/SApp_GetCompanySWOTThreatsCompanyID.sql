﻿-- =============================================            
-- Author:  Laxmikant            
-- Create date: 05/15/2019            
-- Description: Get Company swot Threats by CompanyId             
-- =============================================            
CREATE PROCEDURE [dbo].[SApp_GetCompanySWOTThreatsCompanyID]                 
 @ComapanyID uniqueidentifier = null,             
 @includeDeleted bit = 0                 
AS            
BEGIN             
 SET NOCOUNT ON;              
 SELECT[CompanySwotThreatsMap].[Id]            
      ,[CompanySwotThreatsMap].[CompanyId]              
      ,[CompanySwotThreatsMap].[CategoryName]          
      ,[CompanySwotThreatsMap].[Description]            
      ,[CompanySwotThreatsMap].[CreatedOn]            
      ,[CompanySwotThreatsMap].[UserCreatedById]            
      ,[CompanySwotThreatsMap].[ModifiedOn]            
      ,[CompanySwotThreatsMap].[UserModifiedById]            
      ,[CompanySwotThreatsMap].[IsDeleted]            
      ,[CompanySwotThreatsMap].[DeletedOn]            
      ,[CompanySwotThreatsMap].[UserDeletedById]            
  FROM [dbo].[CompanySwotThreatsMap]        
  LEFT JOIN [dbo].[Category] Category ON [CompanySwotThreatsMap].CategoryName = Category.Id          
  WHERE ([CompanySwotThreatsMap].CompanyId = @ComapanyID)                  
 AND (@includeDeleted = 0 or [CompanySwotThreatsMap].IsDeleted = 0)              
END