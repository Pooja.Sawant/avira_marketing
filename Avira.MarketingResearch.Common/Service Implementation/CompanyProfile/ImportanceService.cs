﻿using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.CompanyProfile
{
    public class ImportanceService : IImportanceService
    {
        private readonly IImportanceRepository _iImportanceRepository;

        public ImportanceService(IImportanceRepository iImportanceRepository)
        {
            _iImportanceRepository = iImportanceRepository;
        }
        public IEnumerable<ImportanceViewModel> GetAll()
        {
            return _iImportanceRepository.GetAll();
        }
    }
}
