﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile
{
    public interface ICompanyNoteRepository
    {
        CompanyNoteViewModel GetById(Guid companyId,string trendType, Guid? projectId);
        Result<SpTransactionMessage> Insert(CompanyNoteViewModel viewModel);
    }
}
