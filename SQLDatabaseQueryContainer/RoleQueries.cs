﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public static class RoleQueries
    {
        public static readonly string StoreProcInsert = "SApp_insertAviraUserRole";

        public static readonly string StoreProcSelectById = "SApp_GetUserRolesById";

        public static readonly string StoreProcSelectAll = "SApp_UserRolesGridWithFilters";

        public static readonly string StroreProcInsertPermissionmap = "SApp_insertRolePermissionMap";

        public static readonly string StroreProcDeleteRolePermission = "SApp_DeleteUserRolesandPermissionsByRoleId";
    }
}
