﻿CREATE TABLE [dbo].[CompanyProductCategoryMap] (
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [CompanyProductId]  UNIQUEIDENTIFIER NULL,
    [ProductCategoryId] UNIQUEIDENTIFIER NULL,
    [CreatedOn]         DATETIME2 (7)    NULL,
    [UserCreatedById]   UNIQUEIDENTIFIER NULL,
    [ModifiedOn]        DATETIME2 (7)    NULL,
    [UserModifiedById]  UNIQUEIDENTIFIER NULL,
    [IsDeleted]         BIT              CONSTRAINT [DF_CompanyProductCategoryMap_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]         DATETIME2 (7)    NULL,
    [UserDeletedById]   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CompanyProductCategoryMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CompanyProductCategoryMap_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyProductCategoryMap_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyProductCategoryMap_AviraUser2] FOREIGN KEY ([UserDeletedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyProductCategoryMap_CompanyProduct] FOREIGN KEY ([CompanyProductId]) REFERENCES [dbo].[CompanyProduct] ([Id]),
    CONSTRAINT [FK_CompanyProductCategoryMap_ProductCategory] FOREIGN KEY ([ProductCategoryId]) REFERENCES [dbo].[ProductCategory] ([Id])
);

