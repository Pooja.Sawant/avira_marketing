﻿
CREATE PROCEDURE [dbo].[SApp_StandardHoursGridWithFilters]
	(@StandardHoursIDList dbo.udtUniqueIdentifier READONLY,
	@StandardHoursName nvarchar(256)='',
	@StandardHoursValue nvarchar(256)='',
	@includeDeleted bit = NULL,
	@OrdbyByColumnName NVARCHAR(50) ='CreatedDate',
	@SortDirection INT = -1,
	@PageStart INT = 0,
	@PageSize INT = null)
	AS
/*================================================================================
Procedure Name: SApp_StandardHoursGridWithFilters
Author: Harshal
Create date: 04/02/2019
StandardHoursValue: Get list from StandardHours table by Name or ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
04/02/2019	Adarsh			Initial Version
================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
if @includeDeleted is null set @includeDeleted = 0;
if len(@StandardHoursName)>2 set @StandardHoursName = '%'+ @StandardHoursName + '%'
else set @StandardHoursName = ''
if len(@StandardHoursValue)>2 set @StandardHoursValue = '%'+ @StandardHoursValue + '%';
else set @StandardHoursValue = '';
	if @PageSize is null or @PageSize =0
	set @PageSize = 10
	begin
	;WITH CTE_TBL AS (

	SELECT [StandardHours].[Id],
		[StandardHours].[StandardHoursName],
		[StandardHours].[StandardHoursValue],
		[StandardHours].[IsDeleted],
		CASE
		WHEN @OrdbyByColumnName = 'StandardHoursName' THEN [StandardHours].StandardHoursName
		WHEN @OrdbyByColumnName = 'StandardHoursValue' THEN [StandardHours].[StandardHoursValue]
		ELSE [StandardHours].[StandardHoursName]
	END AS SortColumn

FROM [dbo].[StandardHours]
where (NOT EXISTS (SELECT * FROM @StandardHoursIDList) or  [StandardHours].Id  in (select * from @StandardHoursIDList))
	AND (@StandardHoursName = '' or [StandardHours].[StandardHoursName] like @StandardHoursName)
	AND (@StandardHoursValue = '' or [StandardHours].[StandardHoursValue] like @StandardHoursValue)
	AND (@includeDeleted = 1 or [StandardHours].IsDeleted = 0)
	)

	SELECT 
		  CTE_TBL.*,
		  tCountResult.TotalItems
		FROM CTE_TBL
		CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult
		ORDER BY 
		  CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,
		  CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC 
		OFFSET @PageStart ROWS
		FETCH NEXT @PageSize ROWS ONLY;
		END
	END