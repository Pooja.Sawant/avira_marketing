﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations
{
    public class IndustryRepository : IIndustryRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "Industry";
        public IndustryRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public IndustryViewModel GetById(Guid guid)
        {
            IndustryViewModel industryViewModel = new IndustryViewModel();
            industryViewModel = GetAllIndustry(guid).FirstOrDefault();
            return industryViewModel;
        }

        public IEnumerable<IndustryViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted)
        {
            return GetAllIndustry(null, PageStart, pageSize, SortDirection, OrdbyByColumnName,Search, includeDeleted);
        }

        public IEnumerable<IndustryViewModel> GetAllParentIndustry()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<IndustryViewModel>(IndustryQueries.StoreProcSelectAllParent
                            , null
                            , null, true, null, CommandType.StoredProcedure);
        }

        public IEnumerable<IndustryViewModel> GetAllIndustry(Guid? guid, int PageStart = 0, int pageSize = 0, int SortDirection = -1, string OrdbyByColumnName = "", string Search = "", bool includeDeleted=false)
        {
            DataTable industryIdsTable = new DataTable();
            industryIdsTable.Columns.Add("ID", typeof(Guid));

            if (guid != null)
            {
                industryIdsTable.Rows.Add(guid);

            }

            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<IndustryViewModel>(IndustryQueries.StoreProcSelectAll
                            , new { @IndustryIDList = industryIdsTable.AsTableValuedParameter("dbo.udtUniqueIdentifier"), @includeDeleted = includeDeleted, @PageStart = PageStart, @pageSize = pageSize, @SortDirection = SortDirection, @OrdbyByColumnName = OrdbyByColumnName, @IndustryName=Search }
                            , null, true, null, CommandType.StoredProcedure);
        }

        public Result<SpTransactionMessage> Create(IndustryInsertDbViewModel industryInsertDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(IndustryQueries.StoreProcInsert, industryInsertDbViewModel, CommandType.StoredProcedure);
            return spTransactionMessage;

        }

        public Result<SpTransactionMessage> Update(IndustryUpdateDbViewModel industryUpdateDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(IndustryQueries.StoreProcUpdate, industryUpdateDbViewModel, CommandType.StoredProcedure);
            return spTransactionMessage;
        }
    }
}
