﻿CREATE TABLE [dbo].[Country] (
    [Id]                   UNIQUEIDENTIFIER NOT NULL,
    [CountryUNCode]        NVARCHAR (6)     NULL,
    [CountryCode]          NVARCHAR (3)     NOT NULL,
    [CountryName]          NVARCHAR (256)   CONSTRAINT [DF_Country_CountryName] DEFAULT ('') NOT NULL,
    [STDCode]              INT              NULL,
    [SecondaryCountryName] NVARCHAR (256)   NULL,
    [CurrencyId]           UNIQUEIDENTIFIER NULL,
    [CreatedOn]            DATETIME2 (7)    NULL,
    [UserCreatedById]      UNIQUEIDENTIFIER NULL,
    [ModifiedOn]           DATETIME2 (7)    NULL,
    [UserModifiedById]     UNIQUEIDENTIFIER NULL,
    [IsDeleted]            BIT              CONSTRAINT [DF_Country_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]            DATETIME2 (7)    NULL,
    [UserDeletedById]      UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Country_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_Country_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id])
);



