﻿CREATE procedure [dbo].[SApp_GetUserDetailsByUserID]      
(@AviraUserId uniqueidentifier)      
as      
/*================================================================================      
Procedure Name: [[SApp_GetUserDetailsByUserID]]      
Author: Gopi      
Create date: 06/13/2019      
Description: Get userdetails based on userid from avirauser table           
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
06/13/2019 Gopi   Initial Version     
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
      
--SELECT        ur.UserRoleName, au.Id, au.FirstName, au.LastName, au.UserName, au.DisplayName, au.EmailAddress, au.PhoneNumber, au.UserTypeId, au.PassWord, au.CustomerId, au.IsEverLoggedIn, au.LastLoggedOn, au.UserRoleId, 
--                         au.Salt, au.CreatedOn, au.UserCreatedById, au.ModifiedOn, au.UserModifiedById, au.IsDeleted, au.DeletedOn, au.UserDeletedById, au.ConcurrentLoginFailureCount, au.IsUserLocked, au.DateofJoining, au.LocationID, 
--                         au.ResetPasswordLinkCreatedOn, au.ResetPasswordSalt, CustomerRequest.Company, CustomerRequest.AddressLine1, CustomerRequest.State
--FROM            AviraUser AS au INNER JOIN
--                         UserRole AS ur ON au.UserRoleId = ur.Id LEFT  JOIN
--                         CustomerRequest ON au.EmailAddress = CustomerRequest.Email  
SELECT        ur.UserRoleName, au.Id, au.FirstName, au.LastName, au.UserName, au.DisplayName, au.EmailAddress, au.PhoneNumber, au.UserTypeId, au.PassWord, au.CustomerId, au.IsEverLoggedIn, au.LastLoggedOn, au.UserRoleId, 
                         au.Salt, au.CreatedOn, au.UserCreatedById, au.ModifiedOn, au.UserModifiedById, au.IsDeleted, au.DeletedOn, au.UserDeletedById, au.ConcurrentLoginFailureCount, au.IsUserLocked, au.DateofJoining, au.LocationID, 
                         au.ResetPasswordLinkCreatedOn, au.ResetPasswordSalt, Location.LocationName, Location.Address1 as Address, Location.State, Customer.CustomerName as CompanyName
FROM            AviraUser AS au INNER JOIN
                         UserRole AS ur ON au.UserRoleId = ur.Id INNER JOIN
                         Location ON au.LocationID = Location.Id left JOIN
                         Customer ON au.CustomerId = Customer.Id
WHERE au.Id = @AviraUserId   
AND au.IsDeleted = 0  
END