﻿CREATE procedure [dbo].[SFab_GetCompanyBoardOfDirectorsByCompanyID]
(	
	@CompanyId uniqueidentifier,
	@UserId uniqueidentifier
)      
as      
/*================================================================================      
Procedure Name: [SFab_GetCompanyBoardOfDirectorsByCompanyID] 
Author: Nitin      
Create date: 31-Mar-2020      
Description: Get ShareHoldingPatternMap      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
31-Mar-2020 Nitin   Initial Version    
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
    
Declare @ErrorMsg NVARCHAR(2048)  
    
BEGIN TRY         
  
	Select Emp.Id, Emp.KeyEmployeeName, Emp.DesignationId, D.Designation As KeyEmployeeDesignation, Emp.KeyEmployeeEmailId, Emp.CompanyBoardOfDirecorsOtherCompanyName
	From CompanyKeyEmployeesMap Emp 
		Inner Join Designation D On 
			Emp.DesignationId = D.Id
	Where Emp.CompanyId = @CompanyId And Emp.IsDirector = 1
     
	SELECT StoredProcedureName ='SFab_GetCompanyBoardOfDirectorsByCompanyID',Message =@ErrorMsg,Success = 1;       
    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);      
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());      
    
 set @ErrorMsg = 'Error while insert a new SFab_GetCompanyBoardOfDirectorsByCompanyID, please contact Admin for more details.';    
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
       
END CATCH      
      
      
end