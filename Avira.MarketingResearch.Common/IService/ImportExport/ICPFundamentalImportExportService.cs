﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.ImportExport
{
    public interface ICPFundamentalImportExportService
    {
        List<ImportCommonResponseModel> Create(CompanyFundamentalsViewModel viewModel, AviraUser currentUser);
    }
}
