﻿CREATE TABLE [dbo].[MarketValueYearMap] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [MarketId]         UNIQUEIDENTIFIER NOT NULL,
    [Year]             INT              NULL,
    [ASP]              DECIMAL (18)     NULL,
    [Value]            DECIMAL (18)     NULL,
    [Volume]           DECIMAL (18)     NULL,
    [OranicGrowth]     DECIMAL (18)     NULL,
    [TrendsGrowth]     DECIMAL (18)     NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_MarketValueYearMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_MarketValueYearMap_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_MarketValueYearMap_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_MarketValueYearMap_Market] FOREIGN KEY ([MarketId]) REFERENCES [dbo].[Market] ([Id])
);

