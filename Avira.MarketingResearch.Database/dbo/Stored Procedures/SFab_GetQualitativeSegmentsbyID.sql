﻿
CREATE procedure [dbo].[SFab_GetQualitativeSegmentsbyID]
(@ProjectID uniqueidentifier=null,
@includeDeleted bit = 0,
@AviraUserID uniqueidentifier = null)
as
/*================================================================================
Procedure Name: SFab_GetQualitativeSegmentsbyID
Author: Harshal 
Create date: 09/04/2019
Description: Get list from QualitativeSegmentMapping table by Filters
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
09/04/2019	Harshal			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if @includeDeleted is null set @includeDeleted = 0

SELECT distinct
       [SegmentName]
      ,MSP.SegmentId
	  
FROM [dbo].[QualitativeSegmentMapping] MSP
INNER JOIN Segment Segment ON Segment.Id=MSP.SegmentId
where (@ProjectId is null or MSP.ProjectId = @ProjectID)
and (@includeDeleted = 1 or MSP.IsDeleted = 0) order by SegmentName

end