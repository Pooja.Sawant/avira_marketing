﻿
CREATE procedure [dbo].[SApp_insertCompanyKeyEmpMap]      
(
	@CompanyId uniqueidentifier,
	@KeyEmpListmap dbo.[udtGUIDKeyEmpMultiValue] readonly,	
	@UserCreatedById uniqueidentifier
)      
as      
/*================================================================================      
Procedure Name: [SApp_insertCompanyKeyEmpMap]      
Author: Sai Krishna      
Create date: 05/06/2019      
Description: Insert a record into CompanyKeyEmployeesMap table      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
05/06/2019 Sai Krishna   Initial Version    
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)  
    
      
--BEGIN TRY         

--Delete From [CompanyKeyEmployeesMap] 
--Where [CompanyKeyEmployeesMap].CompanyId = @CompanyId
--and IsDirector = 0;
--Update [CompanyKeyEmployeesMap]
--Set [KeyEmployeeName] = keymap.[KeyEmployeeName],
--	DesignationId = keymap.DesignationId,
--	[KeyEmployeeEmailId] = keymap.[KeyEmployeeEmailId],
--	[KeyEmployeeComments] = keymap.[KeyEmployeeComments],
--	[CompanyId] = @CompanyId,
--	ManagerId = keymap.ManagerId,
--	[UserModifiedById] =  @UserCreatedById,
--	[ModifiedOn] = GETDATE()
--	From CompanyKeyEmployeesMap
--	Inner Join @KeyEmpListmap keymap
--	on CompanyKeyEmployeesMap.CompanyId = @CompanyId
--	and CompanyKeyEmployeesMap.Id = keymap.ID
--	and CompanyKeyEmployeesMap.IsDirector = 0
--	Where IsDeleted = 0
--where Exists (select 1 from [CompanyKeyEmployeesMap]
--					where [CompanyKeyEmployeesMap].CompanyId = @CompanyId
--					and [CompanyKeyEmployeesMap].Id in(Select Id from @KeyEmpListmap) and [CompanyKeyEmployeesMap].IsDirector = 0)

--mark records as delete which are not in list
--update [dbo].CompanyKeyEmployeesMap
--set IsDeleted = 1,
--UserDeletedById = @UserCreatedById,
--DeletedOn = GETDATE()
--from [dbo].CompanyKeyEmployeesMap
--where CompanyKeyEmployeesMap.CompanyId = @CompanyId
--and not exists (select 1 from @KeyEmpListmap map where map.id = CompanyKeyEmployeesMap.Id  and CompanyKeyEmployeesMap.IsDirector = 0)


 --Update records for change
update [dbo].CompanyKeyEmployeesMap
Set [KeyEmployeeName] = keymap.[KeyEmployeeName],
	DesignationId = keymap.DesignationId,
	[KeyEmployeeEmailId] = keymap.[KeyEmployeeEmailId],
	[KeyEmployeeComments] = keymap.[KeyEmployeeComments],
	[CompanyId] = @CompanyId,
	ManagerId = keymap.ManagerId,
	[UserModifiedById] =  @UserCreatedById,
	[ModifiedOn] = GETDATE()
	From CompanyKeyEmployeesMap
	Inner Join @KeyEmpListmap keymap
	on CompanyKeyEmployeesMap.Id = keymap.ID
	--and CompanyKeyEmployeesMap.IsDirector = 0
	Where CompanyKeyEmployeesMap.CompanyId = @CompanyId
	and exists (select 1 from @KeyEmpListmap map where map.id = CompanyKeyEmployeesMap.Id  and CompanyKeyEmployeesMap.IsDirector = 0)

--Insert new records
insert into [dbo].[CompanyKeyEmployeesMap](
		[Id],      
        [KeyEmployeeName], 
		DesignationId,
		[KeyEmployeeEmailId],
		[KeyEmployeeComments],
		IsDirector,
		[CompanyId],
		ManagerId,
        [IsDeleted],      
        [UserCreatedById],      
        [CreatedOn])      
 select NEWID(),
   map1.KeyEmployeeName,       
   map1.[DesignationID],
   map1.KeyEmployeeEmailId,
   map1.KeyEmployeeComments,
   0,
   @CompanyId,
   map1.[ManagerID],
   0,      
   @UserCreatedById,       
   GETDATE()
 from @KeyEmpListmap map1
 where not exists (select 1 from [dbo].[CompanyKeyEmployeesMap] map2
					where map2.CompanyId = @CompanyId
					and map1.Id = map2.Id and map2.IsDirector = 0);       
     
SELECT StoredProcedureName ='SApp_insertCompanyKeyEmpMap',Message =@ErrorMsg,Success = 1;       
    
--END TRY      
--BEGIN CATCH      
--    -- Execute the error retrieval routine.      
-- DECLARE @ErrorNumber int;      
-- DECLARE @ErrorSeverity int;      
-- DECLARE @ErrorProcedure varchar(100);      
-- DECLARE @ErrorLine int;      
-- DECLARE @ErrorMessage varchar(500);      
      
--  SELECT @ErrorNumber = ERROR_NUMBER(),      
--        @ErrorSeverity = ERROR_SEVERITY(),      
--        @ErrorProcedure = ERROR_PROCEDURE(),      
--        @ErrorLine = ERROR_LINE(),      
--        @ErrorMessage = ERROR_MESSAGE()      
      
-- insert into dbo.Errorlog(ErrorNumber,      
--       ErrorSeverity,      
--       ErrorProcedure,      
--       ErrorLine,      
--       ErrorMessage,      
--       ErrorDate)      
--        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());      
    
-- set @ErrorMsg = 'Error while insert a new Company Key Employees, please contact Admin for more details.';    
--SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
--    ,Message =@ErrorMsg,Success = 0;       
       
--END CATCH      
      
      
end