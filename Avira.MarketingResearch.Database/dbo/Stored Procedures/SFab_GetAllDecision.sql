﻿CREATE PROCEDURE [dbo].[SFab_GetAllDecision]    
(  
	@BusinessStageIdList [dbo].[udtUniqueIdentifier] READONLY,--Optional record 0 ... list
	@BusinessFunctionIdList [dbo].[udtUniqueIdentifier] READONLY,--Optional record 0 ... list
	@BusinessRoleIdList [dbo].[udtUniqueIdentifier] READONLY,--Optional record 0 ... list
	@DecisionTypeIdList [dbo].[udtUniqueIdentifier] READONLY,--Optional record 0 ... list
	@StatusIdList [dbo].[udtUniqueIdentifier] READONLY,--Optional record 0 ... list
	@AssingedTo NVARCHAR(512) = null,--Optional
	@ScoreJsonList NVARCHAR(512) = null,--Optional
	@AviraUserID UNIQUEIDENTIFIER
)
As    

-----------------------Test Data of Json Parameter
--DECLARE @ScoreJsonList NVARCHAR(MAX);
--SET @ScoreJsonList = N'[
--  {"FromScore": 1, "ToScore": 2},
--  {"FromScore": 4, "ToScore": 5}
--]';

--SELECT *
--FROM OPENJSON(@ScoreJsonList)
--  WITH (
--    FromScore FLOAT '$.FromScore',
--    ToScore FLOAT '$.ToScore'
--  );
---------------------------------------------


/*================================================================================    
Procedure Name: SFab_GetAllDecision   
Author: Praveen    
Create date: 07-Jan-2020
Description: To return all Decision of the user
Change History    
Date  Developer  Reason  
__________ ____________ ______________________________________________________    
07-Jan-2020  Praveen   Initial Version 
14-Jan-2020  Harshal   Added Query Statement
14-Jan-2020  Praveen   Modified the condition for filters
================================================================================*/    
BEGIN    
------------------

	--Required Tables: Decision,LookupStatus and LookupCategory
	--Notes: N/A
	--Case 1: if @BusinessStageIdList is not null then it should return data based on the AviraUserID and @BusinessStageIdList for particular user
	--Case 2: if @BusinessStageIdList is null then it should return data based on the AviraUserID level for particular user

	--Case 3: if @BusinessFunctionIdList is not null then it should return data based on the AviraUserID and @BusinessFunctionIdList for particular user
	--Case 4: if @BusinessFunctionIdList is null then it should return data based on the AviraUserID level for particular user

	--Case 5: if @BusinessRoleIdList is not null then it should return data based on the AviraUserID and @BusinessRoleIdList for particular user
	--Case 6: if @BusinessRoleIdList is null then it should return data based on the AviraUserID level for particular user

	--Case 7: if @DecisionTypeIdList is not null then it should return data based on the AviraUserID and @DecisionTypeIdList for particular user
	--Case 8: if @DecisionTypeIdList is null then it should return data based on the AviraUserID level for particular user

	--Case 9: if @StatusIdList is not null then it should return data based on the AviraUserID and @StatusIdList for particular user
	--Case 10: if @StatusIdList is null then it should return data based on the AviraUserID level for particular user

	--Case 11: if @AssingedTo is not null then it should return data based on the AviraUserID and @AssingedTo for particular user
	--Case 12: if @AssingedTo is null then it should return data based on the AviraUserID level for particular user

	--Case 13: if @ScoreJsonList is not null then it should return data based on the AviraUserID and @ScoreJsonList for particular user
	--Case 14: if @ScoreJsonList is null then it should return data based on the AviraUserID level for particular user

	--Expected Columns :Id, Status, Description, AssingedTo, Score 
	declare @AllRoleId uniqueidentifier 
	select @AllRoleId = ID from LookupCategory
	where CategoryName = 'All Roles' and CategoryType='DMBusinessRole';

	declare @RoleIdList dbo.[udtUniqueIdentifier];
	DECLARE @totalRecords int = isnull((select count(1) from @BusinessRoleIdList), 0)

	Insert Into @RoleIdList
	select Id
	from @BusinessRoleIdList
	
	if(exists(select 1 from @BusinessRoleIdList))
	  Begin 
	     if(not exists (select 1 from @BusinessRoleIdList where ID = @AllRoleId))
	       Begin
		    if(@totalRecords =1 AND exists (select 1 from @BusinessRoleIdList where ID='00000000-0000-0000-0000-000000000000'))
		      Begin
	              SET @totalRecords=1
			  End
            else
			    Insert Into @RoleIdList values(@AllRoleId)
	       End
	  end 
	declare @DecisionStatusId uniqueidentifier
	
	select @DecisionStatusId = ID from LookupStatus
	where StatusName = 'Decision made' and StatusType='DMDecisionStatus';

	SELECT null As DecisionId,BaseDecision.Description As Description,'' As AssignTo,BaseDecision.Score As Score,BaseDecision.StatusId,LookupStatus.StatusName,BaseDecision.Id As BaseDecisionId,
	Case when (BaseDecision.StatusId = @DecisionStatusId) then 'true' else 'false' end as IsReadonly
    FROM BaseDecision
	INNER JOIN LookupStatus ON BaseDecision.StatusId=LookupStatus.Id
    LEFT JOIN BaseDecisionFunction ON BaseDecision.Id=BaseDecisionFunction.BaseDecisionId
    LEFT JOIN BaseDecisionBusinessRole ON BaseDecision.Id=BaseDecisionBusinessRole.BaseDecisionId
    LEFT JOIN BaseDecisionBusinessStage ON BaseDecision.Id=BaseDecisionBusinessStage.BaseDecisionId
	WHERE BaseDecision.Id NOT IN (SELECT Decision.BaseDecisionId FROM Decision WHERE Decision.UserId=@AviraUserID AND Decision.BaseDecisionId is not null)
	AND (NOT exists(select 1 from @BusinessStageIdList) or ISNULL(BaseDecisionBusinessStage.BusinessStageId,'00000000-0000-0000-0000-000000000000') In (Select Id From @BusinessStageIdList))
	AND (NOT exists(select 1 from @BusinessFunctionIdList) or BaseDecisionFunction.BusinessFunctionId In (Select Id From @BusinessFunctionIdList))
	AND (NOT exists(select 1 from @RoleIdList) or ISNULL(BaseDecisionBusinessRole.BusinessRoleId,'00000000-0000-0000-0000-000000000000') In (Select Id From @RoleIdList)) 
	AND (NOT exists(select 1 from @DecisionTypeIdList) or ISNULl(BaseDecision.DecisionTypeId,'00000000-0000-0000-0000-000000000000') In (Select  Id From @DecisionTypeIdList))
	AND (NOT exists(select 1 from @StatusIdList) or BaseDecision.StatusId In (Select Id From @StatusIdList))
	--Order By BaseDecision.Score desc,BaseDecision.Description
UNION
    SELECT Decision.Id As DecisionId,Decision.Description As Description,Decision.ResourceAssigned As AssignTo,Decision.Score As Score,Decision.StatusId,LookupStatus.StatusName,Decision.BaseDecisionId As BaseDecisionId,
	Case when (Decision.StatusId = @DecisionStatusId) then 'true' else 'false' end as IsReadonly
    FROM Decision
	INNER JOIN LookupStatus ON Decision.StatusId=LookupStatus.Id
    LEFT JOIN DecisionFunction ON Decision.Id=DecisionFunction.DecisionId
    LEFT JOIN DecisionBusinessRole ON Decision.Id=DecisionBusinessRole.DecisionId
    LEFT JOIN DecisionBusinessStage ON Decision.Id=DecisionBusinessStage.DecisionId
	WHERE Decision.UserId=@AviraUserID
	--AND (@AssingedTo is null or Decision.ResourceAssigned like '%'+@AssingedTo+'%') 
	AND (Not exists(select 1 from @BusinessStageIdList) or ISNULL(DecisionBusinessStage.BusinessStageId,'00000000-0000-0000-0000-000000000000') In (Select Id From @BusinessStageIdList))
	AND (NOT exists(select 1 from @BusinessFunctionIdList) or ISNULL(DecisionFunction.BusinessFunctionId,'00000000-0000-0000-0000-000000000000') In (Select Id From @BusinessFunctionIdList))
	AND (NOT exists(select 1 from @RoleIdList) or ISNULL(DecisionBusinessRole.BusinessRoleId,'00000000-0000-0000-0000-000000000000') In (Select Id From @RoleIdList)) 
	AND (NOT exists(select 1 from @DecisionTypeIdList) or ISNULL(Decision.DecisionTypeId,'00000000-0000-0000-0000-000000000000') In (Select Id From @DecisionTypeIdList))
	AND (NOT exists(select 1 from @StatusIdList) or Decision.StatusId In (Select Id From @StatusIdList))
	Order By Score desc,Description
END