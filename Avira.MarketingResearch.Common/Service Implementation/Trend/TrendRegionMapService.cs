﻿using Avira.MarketingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.Trend
{
    public class TrendRegionMapService : ITrendRegionMapService
    {
        private readonly ITrendRegionMapRepository _trendRegionRepository;
        private readonly ITrendNoteRepository _iTrendNoteRepository;

        public TrendRegionMapService(ITrendRegionMapRepository trendEcoSystemMapRepository, ITrendNoteRepository iTrendNoteRepository)
        {
            _trendRegionRepository = trendEcoSystemMapRepository;
            _iTrendNoteRepository = iTrendNoteRepository;
        }
        /// <summary>
        /// To get all Region with mapped/unmapped status by trend id
        /// </summary>
        /// <param name="TrendIdint"></param>
        /// <param name="PageStart"></param>
        /// <param name="pageSize"></param>
        /// <param name="SortDirection"></param>
        /// <param name="OrdbyByColumnName"></param>
        /// <param name="Search"></param>
        /// <param name="isDeletedInclude"></param>
        /// <returns></returns>
        public IEnumerable<TrendRegionMapViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false)
        {
            return _trendRegionRepository.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude = false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tfc"></param>
        /// <param name="currenUser"></param>
        /// <returns></returns>
        public Result<SpTransactionMessage> InsertUpdate(MainTrendRegionMapInsertUpdateDbViewModel tfc, AviraUser currenUser)
        {
            var res = _trendRegionRepository.InsertUpdate(tfc);
            if (res.IsSuccess == true)
            {
                tfc.TrendNoteModel.Id = Guid.NewGuid();
                tfc.TrendNoteModel.UserCreatedById = currenUser.Id;
                tfc.TrendNoteModel.TrendId = tfc.TrendId;
                _iTrendNoteRepository.Insert(tfc.TrendNoteModel);
            }
            //tfc.TrendNoteModel.Id = Guid.NewGuid();
            //tfc.TrendNoteModel.UserCreatedById = tfc.AviraUserId;
            //tfc.TrendNoteModel.TrendId = tfc.TrendId;
            //_iTrendNoteRepository.Insert(tfc.TrendNoteModel);

            return res;
        }
    }
}
