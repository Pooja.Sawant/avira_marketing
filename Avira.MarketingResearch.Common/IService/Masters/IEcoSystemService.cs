﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;

namespace Avira.MarketingResearch.Common.IService.Masters
{
    public interface IEcoSystemService
    {
        IEnumerable<EcoSystemViewModel> GetAll(Guid Id);
        IEnumerable<EcoSystemViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false);
        EcoSystemViewModel GetById(Guid guid);


        Result<SpTransactionMessage> Create(EcoSystemInsertDbViewModel tfc, AviraUser currentUser);
        Result<SpTransactionMessage> Update(EcoSystemUpdateDbViewModel tfc, AviraUser currenUser);

    }
}
