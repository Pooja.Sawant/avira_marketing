﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile
{
    public interface ICompanyClientsStrategyRepository
    {
        IEnumerable<CompanyStrategyViewModel> GetstrategyById(Guid? guid);
        IEnumerable<CompanyclientViewModel> GetclientById(Guid? guid);
        Result<SpTransactionMessage> InsertUpdate(CompanyClientStrategyInsertUpdateDbViewModel viewModel, AviraUser aviraUser);

        IEnumerable<CompanyClientStrategyImportanceViewModel> GetAllImportance();
        IEnumerable<CompanyClientStrategyDirectionViewModel> GetAllImpactDirection();
        IEnumerable<CompanyClientStrategyCategoryViewModel> GetAllCategory();
        IEnumerable<CompanyClientStrategyApproachViewModel> GetAllApproach();
        IEnumerable<CompanyClientStrategyIndustryViewModel> GetAllIndustries();
    }
}
