﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.ImportExport
{
    public interface IMarketSizingImportExportService
    {
        List<MarketSizingImportViewModel> Create(MarketSizingImportInsertViewModel viewModel, AviraUser currentUser);
        bool CreateStagging(MarketSizingImportInsertViewModel viewModel);
        bool DeleteMSStagging(MarketSizingImportInsertViewModel viewModel);
    }
}
