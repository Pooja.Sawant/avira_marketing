﻿

      
 CREATE PROCEDURE [dbo].[SApp_GetEcoSystemPresenceByComapnyID]            
 (@CompanyID uniqueidentifier = null)  
 AS            
/*================================================================================            
Procedure Name:[SApp_GetEcoSystemPresenceByComapnyID]       
Author: Harshal            
Create date: 05/20/2019            
Description: Get list from EcoSystem table by companyId            
Change History            
Date  Developer  Reason            
__________ ____________ ______________________________________________________            
04/22/2019 Harshal   Initial Version 
================================================================================*/           
            
BEGIN            
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED         

begin        
      
 SELECT         
   [EcoSystem].[Id] AS [EcoSystemId],            
   [EcoSystem].[EcoSystemName],           
   [EcoSystem].IsDeleted,        
   CompanyEcosystemPresenceESMMap.isChecked,      
   CompanyEcosystemPresenceESMMap.CompanyId,      
   CompanyEcosystemPresenceESMMap.Id ,      
  cast (case when exists(select 1 from dbo.CompanyEcosystemPresenceESMMap         
     where CompanyEcosystemPresenceESMMap.CompanyId = @CompanyID        
     and CompanyEcosystemPresenceESMMap.IsDeleted = 0        
     and CompanyEcosystemPresenceESMMap.ESMId = EcoSystem.Id) then 1 else 0 end as bit) as IsMapped       
  
        
FROM [dbo].EcoSystem      
LEFT JOIN dbo.CompanyEcosystemPresenceESMMap       
ON EcoSystem.Id = CompanyEcosystemPresenceESMMap.ESMId
and CompanyEcosystemPresenceESMMap.CompanyId = @CompanyID
and CompanyEcosystemPresenceESMMap.IsDeleted = 0
where ( EcoSystem.IsDeleted = 0)        
END       
        
END