﻿using Avira.MarketingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.Trend
{
    public class TrendService : ITrendService
    {
        private readonly ITrendRepository _iTrendRepository;

        public TrendService(ITrendRepository iIndustryRepository)
        {
            _iTrendRepository = iIndustryRepository;
        }

        public IEnumerable<TrendViewModel> GetAll(Guid ProjectId,int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false)
        {
            return _iTrendRepository.GetAll(ProjectId,PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude);
        }

        public TrendViewModel GetById(Guid guid)
        {
            return _iTrendRepository.GetById(guid);
        }

        public Result<SpTransactionMessage> Create(TrendInsertDbViewModel tfc, AviraUser currentUser)
        {
            return _iTrendRepository.Create(tfc);
        }

        public Result<SpTransactionMessage> Update(TrendUpdateDbViewModel tfc, AviraUser currenUser)
        {
            return _iTrendRepository.Update(tfc);
        }
    }
}
