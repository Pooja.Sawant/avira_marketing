﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LookupCategoryController : AviraBaseController
    {
        private ILoggerManager _logger;
        public readonly ILookupCategoryService _iLookupCategoryService;

        public LookupCategoryController(ILookupCategoryService iLookupCategoryService, ILoggerManager logger)
        {
            _iLookupCategoryService = iLookupCategoryService;
            _logger = logger;
        }
        
        [HttpGet]
        [Route("GetAll")]
        public ActionResult<IEnumerable<CategoryViewModel>> GetAll(string Id)
        {
            List<CategoryViewModel> res = null;
            try
            {
                res = _iLookupCategoryService.GetAll(Id).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }
    }
}