﻿CREATE PROCEDURE [dbo].[SFab_GetCrossSegmentSelectionByProjectMarketId]    
(    
   @ProjectId UniqueIdentifier=null,    
   --@MarketId UniqueIdentifier=null,    
   @SegmentList1 dbo.[udtVarcharValue] readonly,    
   @SegmentList2 dbo.[udtVarcharValue] readonly,    
   @SegmentList3 dbo.[udtVarcharValue] readonly, 
   @MetricList dbo.[udtVarcharValue] readonly, 
   @SegmentName1 nvarchar(100),    
   @SegmentName2 nvarchar(100),    
   @SegmentName3 nvarchar(100)    
   --@SegmentColumn1 nvarchar(100) = null,    
   --@SegmentColumn2 nvarchar(100) = null,    
   --@SegmentColumn3 nvarchar(100) = null    
)    
as    
/*================================================================================    
Procedure Name: [SFab_GetCrossSegmentSelectionByProjectMarketId]    
Author: Gopi Krishna    
Create date: 05/07/2019    
Description: Get list from CrossSegmentSelection by ProjectId and MarketId    
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
05/07/2019 Gopi Krishna  Initial Version    
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    
declare @RawMaterialIdlist dbo.udtGUIDVarcharValue;    
declare @ApplicationIdlist dbo.udtGUIDVarcharValue;    
declare @IndustryIdlist dbo.udtGUIDVarcharValue;    
declare @RegionIdlist dbo.udtGUIDVarcharValue;    
declare @CountryIdlist dbo.udtGUIDVarcharValue;    
declare @CompanyProductIdlist dbo.udtGUIDVarcharValue;    
declare @MetricListLocal dbo.[udtVarcharValue]
insert into @RawMaterialIdlist    
select RM.id, rm.RawMaterialName from @SegmentList1 list    
inner join RawMaterial RM    
on list.Value = rm.RawMaterialName    
where @SegmentName1 = 'Raw Material'    
union all    
select RM.id, rm.RawMaterialName from @SegmentList2 list    
inner join RawMaterial RM    
on list.Value = rm.RawMaterialName    
where @SegmentName2 = 'Raw Material'    
union all    
select RM.id, rm.RawMaterialName from @SegmentList3 list    
inner join RawMaterial RM    
on list.Value = rm.RawMaterialName    
where @SegmentName3 = 'Raw Material'    
    
insert into @ApplicationIdlist    
select App.id, App.ApplicationName from @SegmentList1 list    
inner join Application App    
on list.Value = App.ApplicationName    
where @SegmentName1 = 'Application'    
union all    
select App.id, App.ApplicationName from @SegmentList2 list    
inner join Application App    
on list.Value = App.ApplicationName    
where @SegmentName2 = 'Application'    
union all    
select App.id, App.ApplicationName from @SegmentList3 list    
inner join Application App    
on list.Value = App.ApplicationName    
where @SegmentName3 = 'Application'    
    
insert into @CompanyProductIdlist    
select CP.id, CP.ProductName from @SegmentList1 list    
inner join CompanyProduct CP    
on list.Value = CP.ProductName    
where @SegmentName1 = 'Product'    
union all    
select CP.id, CP.ProductName from @SegmentList2 list    
inner join CompanyProduct CP    
on list.Value = CP.ProductName    
where @SegmentName2 = 'Product'    
union all    
select CP.id, CP.ProductName from @SegmentList3 list    
inner join CompanyProduct CP    
on list.Value = CP.ProductName    
where @SegmentName3 = 'Product'    
    
insert into @IndustryIdlist    
select IND.id, IND.IndustryName from @SegmentList1 list    
inner join Industry IND    
on list.Value = IND.IndustryName    
where @SegmentName1 = 'End Use Industry'    
union all    
select IND.id, IND.IndustryName from @SegmentList2 list    
inner join Industry IND    
on list.Value = IND.IndustryName    
where @SegmentName2 = 'End Use Industry'    
union all    
select IND.id, IND.IndustryName from @SegmentList3 list    
inner join Industry IND    
on list.Value = IND.IndustryName    
where @SegmentName3 = 'End Use Industry'    
    
;with RList as (select Value from @SegmentList1    
  where @SegmentName1 = 'Region'    
  union all    
  select Value from @SegmentList2    
  where @SegmentName2 = 'Region'    
  union all    
  select Value from @SegmentList3    
  where @SegmentName3 = 'Region')    
insert into @CountryIdlist    
select C.id, C.CountryName    
from country C    
where exists (select 1 from CountryRegionMap CRM    
    inner join Region R    
    on CRM.RegionId = r.Id    
    inner join RList    
    on r.RegionName = RList.Value    
    where C.Id = CRM.CountryId);    
    
insert into @RegionIdlist    
select R.Id, r.RegionName    
from Region R    
where exists (select 1 from CountryRegionMap CRM    
    inner join @CountryIdlist Clist    
    on CRM.CountryId = Clist.ID    
    where r.Id = CRM.RegionId)    
    
if not exists(select 1 from @MetricList)
begin
insert into @MetricListLocal
select 'Revenue'
end
else
begin
insert into @MetricListLocal
select Value from @MetricList
end


declare @SelectColumnList nvarchar(4000)    
set @SelectColumnList = 'mv.[Year], '+    
case     
when @SegmentName1 = 'Raw Material' then 'RMlist.Value,'    
when @SegmentName1 = 'Application' then 'Alist.Value,'    
when @SegmentName1 = 'Product' then 'Clist.Value,'    
when @SegmentName1 = 'End Use Industry' then 'ilist.Value,'    
when @SegmentName1 = 'Region' then 'Rglist.Value,'    
else '' end    
+    
case     
when @SegmentName2 = 'Raw Material' then 'RMlist.Value,'    
when @SegmentName2 = 'Application' then 'Alist.Value,'    
when @SegmentName2 = 'Product' then 'Clist.Value,'    
when @SegmentName2 = 'End Use Industry' then 'ilist.Value,'    
when @SegmentName2 = 'Region' then 'Rglist.Value,'    
else '' end    
+    
case     
when @SegmentName3 = 'Raw Material' then 'RMlist.Value,'    
when @SegmentName3 = 'Application' then 'Alist.Value,'    
when @SegmentName3 = 'Product' then 'Clist.Value,'    
when @SegmentName3 = 'End Use Industry' then 'ilist.Value,'    
when @SegmentName3 = 'Region' then 'Rglist.Value,'    
else '' end;    
    
Declare @SQL nvarchar(4000);    
declare @Param nvarchar(4000);    
declare @temptablename nvarchar(100);    
set @temptablename = '[TMP_'+ cast(newid() as varchar(100)) + ']';    
set @Param = '@ProjectId UniqueIdentifier,     
@RawMaterialIdlist dbo.udtGUIDVarcharValue readonly,     
@ApplicationIdlist dbo.udtGUIDVarcharValue readonly,     
@IndustryIdlist dbo.udtGUIDVarcharValue readonly,    
@RegionIdlist dbo.udtGUIDVarcharValue readonly,    
@CompanyProductIdlist dbo.udtGUIDVarcharValue readonly'    
set @SQL = 'CREATE TABLE '+ @temptablename +' (Metric nvarchar(20),[Year] int,'  
		+ case when @SegmentName1 ! = '' then QUOTENAME(@SegmentName1) + ' nvarchar(256),' else '' end  
        + case when @SegmentName2 ! = '' then QUOTENAME(@SegmentName2) + ' nvarchar(256),' else '' end  
		+ case when @SegmentName3 ! = '' then QUOTENAME(@SegmentName3) + ' nvarchar(256),' else '' end  
      + '[Value] decimal(19,4))';    
exec sp_executesql @SQL;  
  
 declare @SQL_2 nvarchar(4000);
 set @SQL_2 = ' from MarketValue mv '+ char(13)+ char(10);    
    
IF (@SegmentName1 = 'Raw Material' or @SegmentName2 = 'Raw Material' or @SegmentName3 = 'Raw Material')    
set @SQL_2 = @SQL_2 + ' INNER JOIN @RawMaterialIdlist RMList on mv.RawMaterialId = RMList.ID ' + char(13)+ char(10);    
    
IF (@SegmentName1 = 'Application' or @SegmentName2 = 'Application' or @SegmentName3 = 'Application')    
set @SQL_2 = @SQL_2 + ' INNER JOIN @ApplicationIdlist Alist on mv.ApplicationId = Alist.ID ' + char(13)+ char(10);    
    
IF (@SegmentName1 = 'Product' or @SegmentName2 = 'Product' or @SegmentName3 = 'Product')    
set @SQL_2 = @SQL_2 + ' INNER join @CompanyProductIdlist Clist on mv.CompanyProductId = Clist.ID ' + char(13)+ char(10);    
    
IF (@SegmentName1 = 'End Use Industry' or @SegmentName2 = 'End Use Industry' or @SegmentName3 = 'End Use Industry')    
set @SQL_2 = @SQL_2 + ' INNER join @IndustryIdlist Ilist on mv.IndustryId = Ilist.id ' + char(13)+ char(10);    
    
IF (@SegmentName1 = 'Region' or @SegmentName2 = 'Region' or @SegmentName3 = 'Region')    
set @SQL_2 = @SQL_2 + ' INNER join @RegionIdlist Rglist on mv.RegionId = Rglist.id ' + char(13)+ char(10);    
      
set @SQL_2 = @SQL_2 + ' Where mv.ProjectId = @ProjectId Group By '+ left(@SelectColumnList, len(@SelectColumnList)-1);    

DECLARE Metric_cursor CURSOR FOR   
	SELECT value 
	FROM @MetricListLocal
	declare @Metric nvarchar(20);

	OPEN Metric_cursor  
	FETCH NEXT FROM Metric_cursor INTO @Metric  
  
	WHILE @@FETCH_STATUS = 0  
	BEGIN  
  
	set @SQL = 'insert into '+@temptablename+ ' Select '''+ @Metric + ''' as Metric, '+ @SelectColumnList + 
	case when @Metric = 'Revenue' then 'SUM(mv.Value) as Value '
		 when @Metric = 'Volume' then 'SUM(mv.Volume) as Value '
		 when @Metric = 'ASP' then 'SUM(mv.Value)/SUM(mv.Volume) as Value '
		 when @Metric = 'YoY' then 'Avg(GrowthRatePercentage) as Value '
	end
	   
	set @SQL = @SQL + @SQL_2;

	exec sp_executesql @SQL, @Param,     
	@ProjectId = @ProjectId,    
	@RawMaterialIdlist = @RawMaterialIdlist,    
	@ApplicationIdlist = @ApplicationIdlist,    
	@IndustryIdlist = @IndustryIdlist,    
	@RegionIdlist = @RegionIdlist,    
	@CompanyProductIdlist = @CompanyProductIdlist;   

	FETCH NEXT FROM Metric_cursor INTO @Metric  
	END  
  
	CLOSE Metric_cursor  
	DEALLOCATE Metric_cursor  
declare @YearColumnList nvarchar(200);    
    
select @YearColumnList= STRING_AGG(QUOTENAME([Year]), ',') WITHIN GROUP (ORDER BY [Year])    
FROM (SELECT [Year] FROM MarketValue mv      
  WHERE ProjectId = @ProjectId    
  GROUP BY [Year]) AS Y;    
    
set @SelectColumnList =  
 case when @SegmentName1 ! = '' then QUOTENAME(@SegmentName1) + ',' else '' end  
+ case when @SegmentName2 ! = '' then QUOTENAME(@SegmentName2) + ',' else '' end  
+ case when @SegmentName3 ! = '' then QUOTENAME(@SegmentName3) + ',' else '' end  

set @sql = 'SELECT '+ @SelectColumnList + ' Metric, ' + @YearColumnList     
+ ' FROM     
(SELECT ' + @SelectColumnList +'Metric, [Year], [Value] FROM '+@temptablename+ ') p      
PIVOT (      
SUM([Value])      
FOR [Year] IN      
( ' + @YearColumnList + ')      
) AS pvt 
order by '+ @SelectColumnList + ' Metric    
FOR JSON PATH ;'    

print '@YearColumnList-->'
print @YearColumnList

print '@SelectColumnList-->'
print @SelectColumnList
--select @Sql =
--'SELECT '+ @SelectColumnList + ' Metric, ' + @YearColumnList + ' FROM 
--(select * from
--(SELECT Metric, ' + @SelectColumnList + ' [Year], [Value] FROM ' + @temptablename +') p      
--PIVOT (      
--SUM([Value])      
--FOR [Year] IN      
--(' + @YearColumnList + ')      
--) AS pvt) Metric
--on ' + @SQL_2 + ' 
--FOR JSON auto ;'  

exec sp_executesql @sql;    
    
set @SQL = 'DROP table ' + @temptablename +';'    
    
exec sp_executesql @sql;    
    
END