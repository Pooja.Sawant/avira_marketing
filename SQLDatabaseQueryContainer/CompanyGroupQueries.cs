﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public class CompanyGroupQueries
    {
        public static readonly string StoreProcInsert = "SApp_insertCompanyGroup";

        public static readonly string StoreProcUpdate = "SApp_UpdateCompanyGroup";

        public static string StoreProcSelectAll = "SApp_CompanyGroupGridWithFilters";
    }
}
