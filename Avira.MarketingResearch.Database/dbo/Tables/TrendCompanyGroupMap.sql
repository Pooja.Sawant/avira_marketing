﻿CREATE TABLE [dbo].[TrendCompanyGroupMap] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [TrendId]          UNIQUEIDENTIFIER NOT NULL,
    [CompanyGroupId]   UNIQUEIDENTIFIER NOT NULL,
    [Impact]           UNIQUEIDENTIFIER NULL,
    [EndUser]          BIT              NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_TrendCompanyGroupMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TrendCompanyGroupMap_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_TrendCompanyGroupMap_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_TrendCompanyGroupMap_CompanyGroup] FOREIGN KEY ([CompanyGroupId]) REFERENCES [dbo].[CompanyGroup] ([Id])
);


GO

GO

GO

GO

GO

GO

GO