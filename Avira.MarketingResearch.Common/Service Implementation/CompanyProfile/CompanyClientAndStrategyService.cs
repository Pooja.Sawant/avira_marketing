﻿using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.CompanyProfile
{
    public class CompanyClientAndStrategyService : ICompanyClientAndStrategyService
    {
        private readonly ICompanyClientsStrategyRepository _companyClientsStrategyRepository;
        private readonly ICompanyNoteRepository _iCompanyNoteRepository;
        public CompanyClientAndStrategyService(ICompanyClientsStrategyRepository CompanyClientsStrategyRepository, ICompanyNoteRepository iCompanyNoteRepository)
        {
            _companyClientsStrategyRepository = CompanyClientsStrategyRepository;
            _iCompanyNoteRepository = iCompanyNoteRepository;
        }
        
        public Result<SpTransactionMessage> InsertUpdate(CompanyClientStrategyInsertUpdateDbViewModel viewModel, AviraUser currenUser)
        {
            //var res = _companyClientsStrategyRepository.InsertUpdate(viewModel, currenUser);
            //return res;

            var res = _companyClientsStrategyRepository.InsertUpdate(viewModel, currenUser);
            viewModel.CompanyNoteViewModel.Id = Guid.NewGuid();
            viewModel.CompanyNoteViewModel.UserCreatedById = viewModel.AviraUserId;
            viewModel.CompanyNoteViewModel.CompanyId = viewModel.CompanyId;
            _iCompanyNoteRepository.Insert(viewModel.CompanyNoteViewModel);

            return res;
        }

        public IEnumerable<CompanyClientStrategyApproachViewModel> GetAllApproach()
        {
            return _companyClientsStrategyRepository.GetAllApproach();
        }

        public IEnumerable<CompanyClientStrategyCategoryViewModel> GetAllCategories()
        {
            return _companyClientsStrategyRepository.GetAllCategory();
        }

        public IEnumerable<CompanyClientStrategyDirectionViewModel> GetAllImpactDirection()
        {
            return _companyClientsStrategyRepository.GetAllImpactDirection();
        }

        public IEnumerable<CompanyClientStrategyImportanceViewModel> GetAllImportance()
        {
            return _companyClientsStrategyRepository.GetAllImportance();
        }

        public IEnumerable<CompanyClientStrategyIndustryViewModel> GetAllIndustries()
        {
            return _companyClientsStrategyRepository.GetAllIndustries();
        }

        public IEnumerable<CompanyStrategyViewModel> GetstrategyById(Guid? guid)
        {
            return _companyClientsStrategyRepository.GetstrategyById(guid);
        }

        public IEnumerable<CompanyclientViewModel> GetclientById(Guid? guid)
        {
            return _companyClientsStrategyRepository.GetclientById(guid);
        }
    }
}
