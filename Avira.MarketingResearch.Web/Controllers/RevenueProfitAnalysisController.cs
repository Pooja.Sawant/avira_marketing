﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.Utils;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Avira.MarketingResearch.Web.Controllers
{
    [Authorize]
    [NoCache]
    public class RevenueProfitAnalysisController : BaseController
    {
        public readonly IServiceRepository _serviceRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;
        string contentRootPath = string.Empty;
        private ILoggerManager _logger;

        public RevenueProfitAnalysisController(IServiceRepository serviceRepository, ILoggerManager logger, IHostingEnvironment hostingEnvironment, IConfiguration configuration)
        {
            _hostingEnvironment = hostingEnvironment;
            _serviceRepository = serviceRepository;
            _logger = logger;
            _configuration = configuration;
            contentRootPath = _hostingEnvironment.ContentRootPath;
        }
        public IActionResult Index(string CompanyId,string ProjectId)
        {
            SetRoleList();
            BindDropdowns(CompanyId);
            ViewBag.PageName = this.ControllerContext.RouteData.Values["controller"].ToString();
            MainRevenueProfitAnalysisInsertUpdateDbViewModel model = new MainRevenueProfitAnalysisInsertUpdateDbViewModel();
            try
            {
                model.CompanyId = new Guid(CompanyId);
                model.ProjectId = new Guid(ProjectId);

                HttpResponseMessage CompanyResponse = _serviceRepository.GetByIdResponse("Company", CompanyId);
                CompanyViewModel companyViewModel = CompanyResponse.Content.ReadAsAsync<CompanyViewModel>().Result;
                ProjectViewModel project = ProjectDetails.GetProjectDetails(_serviceRepository, new Guid(ProjectId));
                if (project != null)
                {
                    ViewBag.ProjectId = project.Id;
                    ViewBag.ProjectName = project.ProjectName;
                    ViewBag.ProjectCode = project.ProjectCode;
                    ViewBag.Approver = project.Approver;
                    ViewBag.ProjectAuthorName = project.ProjectAuthorName;
                    ViewBag.ProjectCoAuthorName = project.ProjectCoAuthorName;
                }
                ViewBag.CompanyName = companyViewModel.CompanyName;
                ViewBag.CompanyId = model.CompanyId;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return View(model);
        }

        private void BindDropdowns(string Id)
        {
            try
            {
                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();

                HttpResponseMessage responseCurrencies = _serviceRepository.GetAllCurrencies(contollerName);
                responseCurrencies.EnsureSuccessStatusCode();
                List<RevenueProfitAnalysisCurrencyViewModel> currencyModel = responseCurrencies.Content.ReadAsAsync<List<RevenueProfitAnalysisCurrencyViewModel>>().Result;
                ViewBag.CurrencyViewModel = new SelectList(currencyModel.OrderBy(e => e.CurrencyCode), "Id", "CurrencyCode");

                HttpResponseMessage responseRegions = _serviceRepository.GetAllRegions(contollerName);
                responseRegions.EnsureSuccessStatusCode();
                ViewBag.RegionsList = responseRegions.Content.ReadAsAsync<List<RevenueProfitAnalysisRegionViewModel>>().Result;

                HttpResponseMessage responseValueConversion = _serviceRepository.GetAllValueConversion(contollerName);
                responseValueConversion.EnsureSuccessStatusCode();
                ViewBag.ValueConversionList = responseValueConversion.Content.ReadAsAsync<List<RevenueProfitAnalysisValueConversionViewModel>>().Result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

        }

        private void SetRoleList()
        {
            try
            {
                var roles = HttpContext.Session.GetString("userRoles");
                var userRoles = JsonConvert.DeserializeObject<List<RoleViewModel>>(roles);
                List<string> RoleListItems = new List<string>();
                foreach (var item in userRoles)
                    RoleListItems.Add(item.UserRoleName);
                ViewBag.UserRoleList = RoleListItems;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }
    }
}