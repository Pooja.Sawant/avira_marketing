﻿CREATE TABLE [dbo].[MakeMyMarkets] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [UserId]           UNIQUEIDENTIFIER NOT NULL,
    [ProjectID]        UNIQUEIDENTIFIER NOT NULL,
    [Name]             VARCHAR (50)     NOT NULL,
    [Description]      VARCHAR (500)    NULL,
    [SequenceNo]       INT              NOT NULL,
    [CreatedOn]        DATETIME2 (7)    NOT NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NOT NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_MakeMyMarkets_ID] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__MakeMyMar__Proje__03006CC6] FOREIGN KEY ([ProjectID]) REFERENCES [dbo].[Project] ([ID]),
    CONSTRAINT [FK__MakeMyMar__UserC__03F490FF] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK__MakeMyMar__UserD__05DCD971] FOREIGN KEY ([UserDeletedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK__MakeMyMar__UserI__020C488D] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK__MakeMyMar__UserM__04E8B538] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_MakeMyMarkets_Name]
    ON [dbo].[MakeMyMarkets]([Name] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MakeMyMarkets_ProjectID]
    ON [dbo].[MakeMyMarkets]([ProjectID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MakeMyMarkets_UserID]
    ON [dbo].[MakeMyMarkets]([UserId] ASC);

