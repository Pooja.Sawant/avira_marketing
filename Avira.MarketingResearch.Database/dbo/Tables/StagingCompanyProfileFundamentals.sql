﻿CREATE TABLE [dbo].[StagingCompanyProfileFundamentals] (
    [Id]                    UNIQUEIDENTIFIER CONSTRAINT [DF_StagingCompanyProfileFundamentals_Id] DEFAULT (newid()) NOT NULL,
    [ImportId]              UNIQUEIDENTIFIER NULL,
    [RowNo]                 INT              CONSTRAINT [DF_StagingCompanyProfileFundamentals_RowNo] DEFAULT ((0)) NOT NULL,
    [CompanyName]           NVARCHAR (257)   NULL,
    [Description]           NVARCHAR (512)   NULL,
    [Headquarter]           NVARCHAR (255)   NULL,
    [Foundedyear]           INT              NULL,
    [Nature]                NVARCHAR (50)    NULL,
    [CompanyStage]          NVARCHAR (50)    NULL,
    [NumberOfEmployee]      INT              NULL,
    [AsonDate]              DATETIME         NULL,
    [Website]               NVARCHAR (255)   NULL,
    [CompanyRank_Rank]      TINYINT          NULL,
    [CompanyRank_Rationale] NVARCHAR (512)   NULL,
    [Currency1]             NVARCHAR (3)     NULL,
    [PhoneNumber1]          NVARCHAR (15)    NULL,
    [PhoneNumber2]          NVARCHAR (15)    NULL,
    [PhoneNumber3]          NVARCHAR (15)    NULL,
    [GeographicPresence]    NVARCHAR (512)   NULL,
    [CurrencyId]            UNIQUEIDENTIFIER NULL,
    [NatureId]              UNIQUEIDENTIFIER NULL,
    [CompanyStageId]        UNIQUEIDENTIFIER NULL,
    [CompanyId]             UNIQUEIDENTIFIER NULL,
    [ErrorNotes]            NVARCHAR (1000)  NULL,
    CONSTRAINT [PK_StagingCompanyProfileFundamentals] PRIMARY KEY CLUSTERED ([Id] ASC)
);









