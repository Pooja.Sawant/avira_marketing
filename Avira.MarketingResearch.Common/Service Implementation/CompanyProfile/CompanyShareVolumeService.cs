﻿using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Avira.MarketingResearch.Common.Service_Implementation.CompanyProfile
{
    public class CompanyShareVolumeService : ICompanyShareVolumeService
    {
        private readonly ICompanyShareVolumeRepository _companyShareVolumeRepository;
        private readonly IConfiguration _configuration;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly ICompanyNoteRepository _iCompanyNoteRepository;

        public CompanyShareVolumeService(ICompanyShareVolumeRepository companyShareVolumeRepository, IConfiguration configuration, IHostingEnvironment hostingEnvironment, ICompanyNoteRepository iCompanyNoteRepository)
        {
            _companyShareVolumeRepository = companyShareVolumeRepository;
            _configuration = configuration;
            _hostingEnvironment = hostingEnvironment;
            _iCompanyNoteRepository = iCompanyNoteRepository;
        }

        public IEnumerable<CurrencyViewModel> GetAllCurrencies()
        {
            return _companyShareVolumeRepository.GetAllCurrencies();
        }

        public Task<MainCompanyShareVolModel> GetShareVolume(string Id)
        {
            return _companyShareVolumeRepository.GetShareVolume(Id);
        }

        public Result<SpTransactionMessage> InsertUpdate(CompanyShareVolumeListModel csv, AviraUser currenUser)
        {
            var res = _companyShareVolumeRepository.InsertUpdate(csv);

            csv.CompanyNoteViewModel.Id = Guid.NewGuid();
            csv.CompanyNoteViewModel.UserCreatedById = currenUser.Id;
            csv.CompanyNoteViewModel.CompanyId = csv.CompanyId;
            _iCompanyNoteRepository.Insert(csv.CompanyNoteViewModel);

            return res;
        }

        public Result<SpTransactionMessage> SaveImageInServer(MainCompanyShareVolModel Model, AviraUser currenUser)
        {
            Result<SpTransactionMessage> CompanyShareVolRep = null;
            //CompanyShareImageModel companyShareImageModel = new CompanyShareImageModel();

            //if (companyShareImageModel.Id == Guid.Empty)
            //    Model.CompanyShareImageModel.Id = Guid.NewGuid();

            //Model.CompanyShareImageModel.UserCreatedById = currenUser.Id;

            if (Model.CompanyShareImageModel.Id==Guid.Empty)
            {
                Model.CompanyShareImageModel.UserCreatedById = currenUser.Id;
                Model.CompanyShareImageModel.Id = Guid.NewGuid();

                if (Model.CompanyShareImageModel.fileByte != null)
                {
                    var path = _configuration.GetSection("AppUploadPath").GetSection("Company").GetSection("GraphsImages").Value;
                    var fullPath = Path.Combine(_hostingEnvironment.ContentRootPath, path);
                    var newFileName = FileUpload.ByteArrayToFile(Model.CompanyShareImageModel.ImageDisplayName, Model.CompanyShareImageModel.fileByte, fullPath, Model.CompanyShareImageModel.fileExtension);
                    if (!string.IsNullOrEmpty(newFileName))
                    {
                        Model.CompanyShareImageModel.ImageActualName = newFileName;
                        Model.CompanyShareImageModel.ImageURL = path + newFileName;
                    }
                }
            }
            else
            {
                Model.CompanyShareImageModel.UserCreatedById = currenUser.Id;
                if (Model.CompanyShareImageModel.fileByte != null)
                {
                    var path = _configuration.GetSection("AppUploadPath").GetSection("Company").GetSection("GraphsImages").Value;
                    var fullPath = Path.Combine(_hostingEnvironment.ContentRootPath, path);
                    var newFileName = FileUpload.ByteArrayToFile(Model.CompanyShareImageModel.ImageDisplayName, Model.CompanyShareImageModel.fileByte, fullPath, Model.CompanyShareImageModel.fileExtension);
                    if (!string.IsNullOrEmpty(newFileName))
                    {
                        Model.CompanyShareImageModel.ImageActualName = newFileName;
                        Model.CompanyShareImageModel.ImageURL = path + newFileName;
                    }
                }
            }

            CompanyShareVolRep = _companyShareVolumeRepository.SaveImageInServer(Model, currenUser);

            if (Model.CompanyNoteViewModel != null)
            {
                Model.CompanyNoteViewModel.Id = Guid.NewGuid();
                Model.CompanyNoteViewModel.UserCreatedById = currenUser.Id;
                Model.CompanyNoteViewModel.CompanyId = Model.CompanyShareImageModel.CompanyId;
            }
            else
            {
                CompanyNoteViewModel companyNoteViewModel = new CompanyNoteViewModel();
                companyNoteViewModel.Id = Guid.NewGuid();
                companyNoteViewModel.UserCreatedById = currenUser.Id;
                companyNoteViewModel.CompanyId = Model.CompanyShareImageModel.CompanyId;
                Model.CompanyNoteViewModel = companyNoteViewModel;
            }

            _iCompanyNoteRepository.Insert(Model.CompanyNoteViewModel);

            return CompanyShareVolRep;
        }
    }
}
