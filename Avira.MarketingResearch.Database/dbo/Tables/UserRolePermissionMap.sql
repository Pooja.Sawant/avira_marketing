﻿CREATE TABLE [dbo].[UserRolePermissionMap](
	[Id] [uniqueidentifier] NOT NULL,
	[UserRoleId] [uniqueidentifier] NOT NULL,
	[PermissionId] [uniqueidentifier] NOT NULL,
	[PermissionLevel] [tinyint] NOT NULL,
	[CreatedOn] [datetime2](7) NULL,
	[UserCreatedById] [uniqueidentifier] NULL,
	[ModifiedOn] [datetime2](7) NULL,
	[UserModifiedById] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeletedOn] [datetime2](7) NULL,
	[UserDeletedById] [uniqueidentifier] NULL,
 CONSTRAINT [PK_UserRolePermissionMap] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserRolePermissionMap] ADD  CONSTRAINT [DF_UserRolePermissionMap_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[UserRolePermissionMap]  WITH CHECK ADD  CONSTRAINT [FK_UserRolePermissionMap_CreatedUser] FOREIGN KEY([UserCreatedById])
REFERENCES [dbo].[AviraUser] ([Id])
GO
ALTER TABLE [dbo].[UserRolePermissionMap] CHECK CONSTRAINT [FK_UserRolePermissionMap_CreatedUser]
GO
ALTER TABLE [dbo].[UserRolePermissionMap]  WITH CHECK ADD  CONSTRAINT [FK_UserRolePermissionMap_DeletedUser] FOREIGN KEY([UserDeletedById])
REFERENCES [dbo].[AviraUser] ([Id])
GO
ALTER TABLE [dbo].[UserRolePermissionMap] CHECK CONSTRAINT [FK_UserRolePermissionMap_DeletedUser]
GO
ALTER TABLE [dbo].[UserRolePermissionMap]  WITH CHECK ADD  CONSTRAINT [FK_UserRolePermissionMap_ModifiedUser] FOREIGN KEY([UserModifiedById])
REFERENCES [dbo].[AviraUser] ([Id])
GO
ALTER TABLE [dbo].[UserRolePermissionMap] CHECK CONSTRAINT [FK_UserRolePermissionMap_ModifiedUser]
GO
ALTER TABLE [dbo].[UserRolePermissionMap]  WITH CHECK ADD  CONSTRAINT [FK_UserRolePermissionMap_PermissionId] FOREIGN KEY([PermissionId])
REFERENCES [dbo].[Permission] ([Id])
GO
ALTER TABLE [dbo].[UserRolePermissionMap] CHECK CONSTRAINT [FK_UserRolePermissionMap_PermissionId]
GO
ALTER TABLE [dbo].[UserRolePermissionMap]  WITH CHECK ADD  CONSTRAINT [FK_UserRolePermissionMap_UserRoleId] FOREIGN KEY([UserRoleId])
REFERENCES [dbo].[UserRole] ([Id])
GO
ALTER TABLE [dbo].[UserRolePermissionMap] CHECK CONSTRAINT [FK_UserRolePermissionMap_UserRoleId]
GO
