﻿using Avira.MarketingResearch.Model.ViewModels.Masters;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.Trend
{
    public class TrendCompanyGroupMapViewModel
    {
        public Guid TrendId { get; set; }
        public Guid CompanyGroupId { get; set; }
        public Guid ImpactId { get; set; }
        public string CompanyGroupName { get; set; }
        public bool IsMapped { get; set; }
        public bool EndUser { get; set; }
        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "CompanyGroupMap";
            }
        }
    }

    public class MainTrendCompanyGroupMapInsertUpdateDbViewModel
    {
        public Guid TrendId { get; set; }
        public string CompanyGroupName { get; set; }
        public List<TrendCompanyGroupMapViewModel> TrendCompanyGroupMapInsertUpdate { get; set; }
        public Guid UserCreatedById { get; set; }
        public TrendNoteInsertUpdateDbViewModel TrendNote { get; set; }
        public TrendViewModel TrendModel { get; set; }
        public TrendNoteViewModel TrendNoteModel { get; set; }
        public bool IsDeleted { get; set; }
    }
}
