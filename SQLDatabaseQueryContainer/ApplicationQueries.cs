﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public static class ApplicationQueries
    {
        public static readonly string StoreProcInsert = "SApp_insertApplication";

        public static readonly string StoreProcUpdate = "SApp_UpdateApplication";

        public static readonly string StoreProcSelectById = "[SApp_GetApplicationbyID]";

        public static string StoreProcGetAll = "SApp_ApplicationGridWithFilters";
    }
}
