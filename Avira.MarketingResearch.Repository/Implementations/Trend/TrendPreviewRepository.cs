﻿using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.Trend
{
    public class TrendPreviewRepository : ITrendPreviewRepository
    {
        protected IDbHelper _dbHelper;

        public TrendPreviewRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public TrendPreviewViewModel GetById(Guid guid)
        {
            TrendPreviewViewModel trendPreviewViewModel = new TrendPreviewViewModel();
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<TrendPreviewViewModel>(TrendPreviewQueries.StoreProcSelectAll
                            , new { @TrendID = guid}
                            , null, true, null, CommandType.StoredProcedure).FirstOrDefault();
        }
    }
}
