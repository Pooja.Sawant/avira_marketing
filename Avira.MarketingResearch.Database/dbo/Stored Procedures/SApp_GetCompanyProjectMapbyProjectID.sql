﻿
 CREATE PROCEDURE [dbo].[SApp_GetCompanyProjectMapbyProjectID]        
 (@ProjectID uniqueidentifier = null,    
 @includeDeleted bit = 0,    
 @OrdbyByColumnName NVARCHAR(50) ='CompanyName',    
 @SortDirection INT = -1,    
 @PageStart INT = 0,  
 @Search NVARCHAR(512)='',    
 @PageSize INT = null)        
 AS        
/*================================================================================        
Procedure Name: [SApp_GetCompanyProjectMapbyProjectID]  
Author: Harshal        
Create date: 05/31/2019        
Description: Get list from CompanyProjectMap table by ProjectID        
Change History        
Date  Developer  Reason        
__________ ____________ ______________________________________________________        
05/31/2019 Harshal   Initial Version        
================================================================================*/       
        
BEGIN        
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED     
if @includeDeleted is null set @includeDeleted = 0;  
if len(@Search)>2 set @Search = '%'+ @Search + '%'  
else set @Search = ''  
if @PageSize is null or @PageSize =0    
set @PageSize = 10    
     
begin    
;WITH CTE_TBL AS (    
 SELECT     
  [Company].[Id] As CompanyId,        
  [Company].CompanyName, 
  [CompanyProjectMap].Id,
  [CompanyProjectMap].ProjectId,      
  [CompanyProjectMap].IsDeleted,    
 CASE    
  WHEN @OrdbyByColumnName = 'CompanyName' THEN [Company].CompanyName  
  ELSE [Company].CompanyName     
 END AS SortColumn    
    
FROM [dbo].[CompanyProjectMap]   
INNER JOIN [dbo].[Company] ON [Company].ID=[CompanyProjectMap].CompanyId   
where [CompanyProjectMap] .ProjectID=@ProjectID    AND
      (@Search = '' or [Company].[CompanyName] like @Search)  
      AND (@includeDeleted = 1 or [CompanyProjectMap].IsDeleted = 0)    
)    
    
 SELECT     
    CTE_TBL.*,    
    tCountResult.TotalItems    
  FROM CTE_TBL    
  CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult    
  ORDER BY     
    CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,    
    CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC     
  OFFSET @PageStart ROWS    
  FETCH NEXT @PageSize ROWS ONLY;    
  END    
    
END