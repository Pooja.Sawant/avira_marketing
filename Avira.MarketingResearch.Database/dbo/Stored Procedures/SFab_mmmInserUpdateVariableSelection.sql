﻿CREATE procedure [dbo].[SFab_mmmInserUpdateVariableSelection]  
(
	@UserId	uniqueidentifier,
	@MarketID uniqueidentifier,
	@ProjectId uniqueidentifier,
	@JsonVariables NVARCHAR(MAX)
	)  
as  
/*================================================================================  
Procedure Name: SFab_mmmInserUpdateVariableSelection  
Author: Pooja Sawant  
Create date: 12/06/2020  
Description: Inseret or (Update)delete a record from MMM_VariableSelection table by conditions  
Change History  
Date       Developer    Reason  
__________ ____________ ______________________________________________________  
12/06/2020 Pooja Sawant Initial Version  
================================================================================*/  
BEGIN  
SET NOCOUNT ON;  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  
declare @ErrorMsg NVARCHAR(2048);
declare @SelectionType varchar(50) ;
declare @PageLevel varchar(100) ;
BEGIN TRY
set @PageLevel = (select top(1) PageLevel  from  OPENJSON(@JsonVariables)
    WITH ([PageLevel] [nvarchar](100) ) as jsonvariable)
set @SelectionType= case when @PageLevel ='SegmentCustomSelection' or @PageLevel ='SubSegmentCustomSelection' then @PageLevel else 'Variable' end


	--DELETE MVS FROM MMM_VariableSelection MVS	
	--LEFT JOIN OPENJSON(@JsonVariables) WITH([SelectionId] [uniqueidentifier])SV ON SV.SelectionId = MVS.SelectionId AND MVS.SelectionType = @SelectionType
	--WHERE MVS.UserId=@UserId and MVS.ProjectID=@ProjectId and MVS.MarketID =@MarketID AND MVS.Id IS NULL
	
	DELETE MVS FROM MMM_VariableSelection MVS	
	LEFT JOIN OPENJSON(@JsonVariables) WITH([SelectionId] [uniqueidentifier])SV ON SV.SelectionId = MVS.SelectionId 
	WHERE MVS.UserId=@UserId and MVS.ProjectID=@ProjectId and MVS.MarketID =@MarketID 
	 AND MVS.SelectionType = @SelectionType and SV.SelectionId is null


	--DELETE MVS FROM MMM_VariableSelection MVS	
	--	WHERE NOT EXISTS 
	--	(
	--		SELECT 1  FROM  OPENJSON(@JsonVariables) WITH ([SelectionId] UNIQUEIDENTIFIER ) as jsonvariable WHERE jsonvariable.SelectionId = MVS.SelectionId
	--	)
	--	AND MVS.SelectionType = @SelectionType and MVS.UserId=@UserId and MVS.ProjectID=@ProjectId and MVS.MarketID =@MarketID
	
	-----Insert 
  INSERT INTO [dbo].[MMM_VariableSelection]
           ([Id]
           ,[UserId]
           ,[ProjectID]
           ,[MarketID]
           ,[SelectionId]
           ,[SelectionType]
           ,[CreatedOn]
           ,[UserCreatedById]
           )
     select
            NEWID() 
           ,@UserId
           ,@ProjectId
           ,@MarketID
           ,SelectionId
           ,@SelectionType
           ,GETDATE()
           ,@UserId
           FROM OPENJSON(@JsonVariables)
    WITH ([SelectionId]	[uniqueidentifier]) as jsonvariables 
	WHERE NOT EXISTS(
	SELECT 1 FROM MMM_VariableSelection MVS WHERE
	 jsonvariables.[SelectionId] = MVS.SelectionId AND MVS.SelectionType = @SelectionType
	 AND MVS.SelectionType = @SelectionType and MVS.UserId=@UserId and MVS.ProjectID=@ProjectId and MVS.MarketID =@MarketID
	)

SELECT StoredProcedureName ='SFab_mmmInserUpdateVariableSelection',Message =@ErrorMsg,Success = 1;     
END TRY

BEGIN CATCH
    -- Execute the error retrieval routine.
	DECLARE @ErrorNumber	int;
	DECLARE @ErrorSeverity	int;
	DECLARE @ErrorProcedure	varchar(100);
	DECLARE @ErrorLine	int;
	DECLARE @ErrorMessage	varchar(500);

	 SELECT @ErrorNumber = ERROR_NUMBER(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorProcedure = ERROR_PROCEDURE(),
        @ErrorLine = ERROR_LINE(),
        @ErrorMessage = ERROR_MESSAGE()

	insert into dbo.Errorlog(ErrorNumber,
							ErrorSeverity,
							ErrorProcedure,
							ErrorLine,
							ErrorMessage,
							ErrorDate)
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());

 set @ErrorMsg = 'Error while insert/Update MMM_VariableSelection, please contact Admin for more details.';    
 
SELECT Number = @ErrorNumber, 
		Severity =@ErrorSeverity,
		StoredProcedureName =@ErrorProcedure,
		LineNumber= @ErrorLine,
		Message =@ErrorMsg,
		Success = 0;     
END CATCH 
end