﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public static class ImportExportQueries
    {
        public static readonly string StoreProcInsert = "SStagin_InsertImportTrend";
        public static readonly string StoreProcInsertMarketSizing = "SStagin_InsertImportMarketSizing";
        public static readonly string StoreProcDeleteStagingMarketSizing = "SStagin_InsertImportMarketSizing_Delete";
        public static readonly string StoreProcUpdateMarketSizing = "SApp_UpdateStagingMarketSize";
        public static readonly string StoreProcInsertCompanyProfile = "SStaging_InsertImportCompanyProfiles_Multitab";
        public static readonly string StoreProcInsertCompanyBasicInfo = "SStaging_InsertImportCompanyProfiles_BasicInfo";
        public static readonly string StoreProcUpdateCompanyBasicInfo = "SApp_UpdateStagingCompanyProfile_BasicInfo";
        //public static readonly string StoreProcUpdateCompanyProfile = "SApp_UpdateStagingCompanyProfile_Multitab";
        public static readonly string StoreProcUpdateCompanyProfile = "SApp_UpdateStagingCompanyProfile_Multitab_OtherInfo";
        public static readonly string StoreProcUpdateTrend = "SApp_UpdateStagingTrend";
        public static readonly string StoreProcInsertCPFinancial = "SStagin_InsertImportCompanyProfilesFinancial";
        public static readonly string StoreProcUpdateCPFinancial = "SApp_UpdateStagingCompanyProfilesFinancial";
        public static readonly string StoreProcInsertQualitativeAnalysis = "SStaging_InsertImportQualitativeAnalysis";
        public static readonly string StoreProcUpdateQualitativeAnalysis = "SApp_UpdateStagingQualitativeAnalysis";
        public static readonly string StoreProcInsertReportGeneration = "SStagin_InsertImportStagingDownloads";
        public static readonly string StoreProcUpdateReportGeneration = "SApp_UpdateStagingDownloads";
        public static readonly string storeProcInsertStrategyInstance = "SStaging_InsertImportStrategyInstance";
        public static readonly string storeProcUpdateStrategyInstance = "SApp_UpdateStagingStrategyInstance";
        public static readonly string StoreProcInsertCompanyFundamentals = "SStaging_InsertImportCompanyFundamentals";
        public static readonly string StoreProcUpdateCompanyFundamentals = "SApp_UpdateStagingCompanyFundamentals";
        public static readonly string StoreProcInsertCompanyBulkImport = "SStaging_InsertCompanyBulkImport";
        public static readonly string StoreProcUpdateCompanyBulkImport = "SApp_UpdateStagingCompanyBulkImport";
    }
}
