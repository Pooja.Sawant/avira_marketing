﻿CREATE PROCEDURE [dbo].[SApp_GetImportRequest]  
(
@AviraUserID uniqueidentifier = null,
@TemplateName nvarchar(400),
@StatusName	nvarchar(200)
)  
 AS  
/*================================================================================  
Procedure Name: [SApp_GetImportRequest]  
Author: Praveen  
Create date: 10/17/2019  
Description: Get import data list based on the type and status
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
10/17/2019   Praveen   Initial Version  
================================================================================*/  
  
BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
	
SELECT [ImportData].[Id], TemplateName, LookupStatus.StatusName, ImportFileName,ImportFilePath,ImportedOn,ModifiedOn FROM [dbo].[ImportData]  
INNER JOIN LookupStatus ON LookupStatus.Id=ImportData.ImportStatusId  
where  TemplateName = @TemplateName AND LookupStatus.StatusName = @StatusName
Order By Case When ModifiedOn is null then CreatedOn else ModifiedOn end

END