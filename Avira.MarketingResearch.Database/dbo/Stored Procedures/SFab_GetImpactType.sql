﻿CREATE PROCEDURE [dbo].[SFab_GetImpactType]
(
	@AviraUserID uniqueidentifier = null,  
	@AviraUserRoleId uniqueidentifier = null 
)
AS
BEGIN
	SELECT Id,ImpactTypeName from ImpactType
END