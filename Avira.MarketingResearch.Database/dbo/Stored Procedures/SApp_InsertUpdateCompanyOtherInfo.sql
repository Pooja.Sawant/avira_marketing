﻿    
    
CREATE procedure [dbo].[SApp_InsertUpdateCompanyOtherInfo]    
(    
@CompanyId uniqueidentifier,     
@TableInfoName nvarchar(100)=null,    
@Description nvarchar(4000)=null,    
@CompanyStatusId uniqueidentifier=null,    
@UserCreatedById uniqueidentifier,    
@jsonOtherInfo NVARCHAR(MAX)=null    
)    
as    
/*================================================================================    
Procedure Name: SApp_InsertUpdateCompanyOtherInfo    
Author: Pooja Sawant    
Create date: 05/07/2019    
Description: Insert a record into Companyotherinfo table    
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
05/07/2019 Pooja   Initial Version    
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    
declare @ErrorMsg NVARCHAR(2048);    
    
Begin try    
if @CompanyStatusId is null    
set @CompanyStatusId=(select Id from CompanyProfileStatus    
where CompanyStatusName = 'Draft')    
    
--For start CompanyOtherInfo Table     
update [dbo].[CompanyOtherInfo]    
set 
	Description =jsonOtherInfo.Description, 
    TableInfoName = jsonOtherInfo.TableInfoName,
	IsDeleted = 0,    
	ModifiedOn = GETDATE(),    
	UserModifiedById = @UserCreatedById    
from [dbo].[CompanyOtherInfo]    
inner join OPENJSON(@jsonOtherInfo)    
    WITH (      
  [Id] [uniqueidentifier],      
  [TableInfoName] [nvarchar](100),    
  [Description] [nvarchar](4000)     
  ) as jsonOtherInfo on jsonOtherInfo.Id=CompanyOtherInfo.ID     
where [CompanyOtherInfo].CompanyId = @CompanyId AND jsonOtherInfo.Id = [CompanyOtherInfo].ID;    
    
--Insert new records    
insert into [dbo].[CompanyOtherInfo]([Id],          
  [CompanyId],          
  [TableInfoName],     
  [Description],    
  [CompanyStatusId],      
  [CreatedOn],    
  [UserCreatedById],    
        [DeletedOn])    
                  
 select Id,    
  @CompanyId,          
  TableInfoName,     
  Description,    
  @CompanyStatusId,      
  getdate(),    
  @UserCreatedById,    
        0    
 FROM OPENJSON(@jsonOtherInfo)    
    WITH (      
  [Id] [uniqueidentifier] ,     
  [CompanyId] [uniqueidentifier] ,      
  [TableInfoName] [nvarchar](100) ,    
  [Description] [nvarchar](4000)     
      
   ) as jsonOtherInfo    
    where not exists (select 1 from [dbo].[CompanyOtherInfo] compassetp    
     where compassetp.CompanyId = @CompanyId    
     and jsonOtherInfo.Id = compassetp.ID);       
--For end CompanyOtherInfo Table     
    
    
    
                
SELECT StoredProcedureName ='SApp_InsertUpdateCompanyOtherInfo',Message =@ErrorMsg,Success = 1;           
        
END TRY          
BEGIN CATCH          
    -- Execute the error retrieval routine.          
 DECLARE @ErrorNumber int;          
 DECLARE @ErrorSeverity int;          
 DECLARE @ErrorProcedure varchar(100);          
 DECLARE @ErrorLine int;          
 DECLARE @ErrorMessage varchar(500);          
          
  SELECT @ErrorNumber = ERROR_NUMBER(),          
        @ErrorSeverity = ERROR_SEVERITY(),          
        @ErrorProcedure = ERROR_PROCEDURE(),          
        @ErrorLine = ERROR_LINE(),          
        @ErrorMessage = ERROR_MESSAGE()          
          
 insert into dbo.Errorlog(ErrorNumber,          
       ErrorSeverity,          
       ErrorProcedure,          
       ErrorLine,          
       ErrorMessage,          
       ErrorDate)          
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());          
        
 set @ErrorMsg = 'Error while insert a new Company other info, please contact Admin for more details.';        
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine        
    ,Message =@ErrorMsg,Success = 0;           
           
END CATCH          
          
          
end