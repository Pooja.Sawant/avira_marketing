﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Avira.MarketingResearch.Model.ViewModels.Masters
{
    public class MarketViewModel
    {
        public string MarketName { get; set; }
        public string Description { get; set; }
        public Guid? ParentId { get; set; }
        public string ParentMarketName { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public Guid Id { get; set; }
        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "Market";
            }
        }

    }

    public class MarketInsertDbViewModel
    {
        [Required]
        [Display(Name = "Market Name")]
        public string MarketName { get; set; }
        public string Description { get; set; }
        public Guid? ParentId { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public Guid Id { get; set; }
    }

    public class MarketUpdateDbViewModel
    {
        [Required]
        [Display(Name = "Market Name")]
        public string MarketName { get; set; }
        public string Description { get; set; }
        public Guid? ParentId { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool IsDeleted { get; set; } = false;
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        [Required]
        public Guid Id { get; set; }
    }
}
