﻿CREATE PROCEDURE [dbo].[proc_BackEnd_Form_Create]
(
@FormName NVARCHAR(MAX)
)
AS
/*================================================================================
Procedure Name: proc_BackEnd_Form_Create
Author: Praveen
Create date: 07/02/2019
Description: To insert Form from backend
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
07/02/2019	Praveen			Initial Version
================================================================================*/
BEGIN

	INSERT INTO Form (Id ,FormName)
	VALUES (NEWID() ,@FormName)

END