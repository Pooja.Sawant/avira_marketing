﻿CREATE PROCEDURE [dbo].[SFab_UpdateDecisionParameterRating]
(
	@DecisionParameterId uniqueidentifier,
	@BaseDecisionParameterId uniqueidentifier ,
	@DecisionId	uniqueidentifier,
	@RatingId uniqueidentifier,
	@UserId	uniqueidentifier
)
As   

--Declare @DecisionParameterId uniqueidentifier = null,
--	@BaseDecisionParameterId uniqueidentifier = '7981a348-78fc-408d-a4ba-ac50aa64d6dc',
--	@DecisionId	uniqueidentifier = '4fbb31aa-0365-4d3b-8540-112e500339f3',
--	@RatingId uniqueidentifier = '89bf4481-2c6e-4ca7-90bd-4d73e567f35a',
--	@UserId	uniqueidentifier = '1570416b-443a-4833-aa8f-941e159dd90f'

--Select @DecisionParameterId='00000000-0000-0000-0000-000000000000',
--@BaseDecisionParameterId='7981a348-78fc-408d-a4ba-ac50aa64d6dc',
--@DecisionId='4FBB31AA-0365-4D3B-8540-112E500339F3',@RatingId='89bf4481-2c6e-4ca7-90bd-4d73e567f35a',@UserId='1570416B-443A-4833-AA8F-941E159DD90F'


	
/*================================================================================    
Procedure Name: SFab_UpdateDecisionParameterRating   
Author: Praveen    
Create date: 07-Jan-2020
Description: Update decision parameter rating
Change History    
Date  Developer  Reason  
__________ ____________ ______________________________________________________    
07-Jan-2020  Praveen   Initial Version    
================================================================================*/    
BEGIN    
Declare @ErrorMsg NVARCHAR(2048);

BEGIN TRY
Begin
------------------

	
	--Required Tables: DecisionParameter 
	--Notes: DecisionID parameter is check whether this parameter belongs to logged-in user or not
	--Case 1: N/A
	--Expected Result :SELECT StoredProcedureName ='SFab_UpdateDecisionParameterRating',Message = @ErrorMsg,Success = 1/0;
	Declare @DecisionStatusDecisionMadeId uniqueidentifier
	Select @DecisionStatusDecisionMadeId = ID From LookupStatus
	Where StatusName = 'Decision made' And StatusType = 'DMDecisionStatus'
	
	If Exists (Select ID from Decision Where Id = @DecisionId and UserId= @UserID and StatusId=@DecisionStatusDecisionMadeId)
        Begin
        	set @ErrorMsg = 'Can not edit any existing details since this decision has been made.';   
        		SELECT StoredProcedureName ='[SFab_UpdateDecisionParameterRating]','Message' =@ErrorMsg,'Success' = 0;  
        end
	else
	begin 
	
	Execute SFab_CopyDecisionFromBase @InDecisionId = @DecisionId, @UserId = @UserId, @CopyDecisionParametersFromBase = 1, @OutDecisionId = @DecisionId OUTPUT
	
	If (@DecisionParameterId Is Null Or @DecisionParameterId = '00000000-0000-0000-0000-000000000000')
	Begin

		Select @DecisionParameterId = Id From DecisionParameter 
		Where BaseDecisionParameterId = @BaseDecisionParameterId And 
			DecisionId = @DecisionId		
	End

	Update DecisionParameter
	Set RatingId = @RatingId, ModifiedOn = GetDate()	
	Where Id = @DecisionParameterId
		
	SELECT StoredProcedureName ='SFab_UpdateDecisionParameterRating',Message =@ErrorMsg,Success = 1;     
End
end
END TRY
BEGIN CATCH  
    EXEC SApp_LogErrorInfo
    set @ErrorMsg = 'Error while Update DecisionParameterRating, please contact Admin for more details.';    
    SELECT StoredProcedureName ='SFab_UpdateDecisionParameterRating',Message =@ErrorMsg,Success = 0;     
END CATCH 
END