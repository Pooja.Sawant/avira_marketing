﻿CREATE TABLE [dbo].[StagingQualitativeNews] (
    [Id]            UNIQUEIDENTIFIER NOT NULL,
    [ImportId]      UNIQUEIDENTIFIER NULL,
    [RowNo]         INT              NULL,
    [MarketName]    NVARCHAR (255)   NOT NULL,
    [SubMarketName] NVARCHAR (255)   NULL,
    [NewsDate]      DATETIME         NULL,
    [News]          NVARCHAR (1000)  NULL,
    [NewsCategory]  NVARCHAR (255)   NULL,
    [Importance]    NVARCHAR (255)   NULL,
    [Impact]        NVARCHAR (255)   NULL,
    [MarketId]      UNIQUEIDENTIFIER NULL,
    [SubMarketId]   UNIQUEIDENTIFIER NULL,
    [ProjectId]     UNIQUEIDENTIFIER NULL,
    [CategoryId]    UNIQUEIDENTIFIER NULL,
    [ImportanceId]  UNIQUEIDENTIFIER NULL,
    [ImpactId]      UNIQUEIDENTIFIER NULL,
    [ErrorNotes]    NVARCHAR (1000)  NULL,
    CONSTRAINT [PK_StagingQualitativeNews] PRIMARY KEY CLUSTERED ([Id] ASC)
);



