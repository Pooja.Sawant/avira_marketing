﻿
      
 CREATE PROCEDURE [dbo].[SApp_GetProductCategoryByCompanyProductID]            
 (@ComapanyProductID uniqueidentifier = null, 
  @includeDeleted bit = 0     
 )            
 AS            
/*================================================================================            
Procedure Name: SApp_GetCompanyProductCategoryByCompanyProductID      
Author: Harshal            
Create date: 05/02/2019            
Description: Get list from ProductCategory table by CompanyProductID            
Change History            
Date  Developer  Reason            
__________ ____________ ______________________________________________________            
05/02/2019 Harshal   Initial Version 
================================================================================*/           
            
BEGIN            
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED         
begin        
        
 SELECT         
   [ProductCategory].[Id] AS [ProductCategoryId],            
   [ProductCategory].[ProductCategoryName],           
   [ProductCategory].[Description],        
   [ProductCategory].IsDeleted,        
   CompanyProductCategoryMap.Id as CompanyProductCategoryMapId      
   --cast (case when exists(select 1 from dbo.CompanyProductCategoryMap         
   --  where CompanyProductCategoryMap.CompanyProductId = @ComapanyProductID        
   --  and CompanyProductCategoryMap.IsDeleted = 0        
   --  and CompanyProductCategoryMap.ProductCategoryId = ProductCategory.Id) then 1 else 0 end as bit) as IsMapped        
       
FROM [dbo].[ProductCategory]      
LEFT JOIN dbo.CompanyProductCategoryMap       
ON ProductCategory.Id = CompanyProductCategoryMap.ProductCategoryId
and CompanyProductCategoryMap.CompanyProductId = @ComapanyProductID 
and CompanyProductCategoryMap.IsDeleted = 0
where  (@includeDeleted = 1 or ProductCategory.IsDeleted = 0)        
      
  END        
       
END