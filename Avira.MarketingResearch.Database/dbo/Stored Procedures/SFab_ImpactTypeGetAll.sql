﻿CREATE PROCEDURE [dbo].[SFab_ImpactTypeGetAll]
@AviraUserID uniqueidentifier = null
 AS      
/*================================================================================      
Procedure Name: SFab_ImpactTypeGetAll      
Author: Jagan      
Create date: 29/05/2019      
Description: Get list from Impact Type      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
29/05/2019 Jagan   Initial Version      
================================================================================*/      
      
BEGIN      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED      

SELECT Id, ImpactTypeName FROM ImpactType WHERE IsDeleted  = 0
      
 END