﻿using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Implementations;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation
{
    public class InfographicsService : IInfographicsService
    {
        private readonly IInfographicsRepository _iInfographicsRepository;
        private readonly IConfiguration _configuration;
        private ILoggerManager _logger;
        private readonly IHostingEnvironment _hostingEnvironment;

        public InfographicsService(IInfographicsRepository iInfographicsRepository, ILoggerManager logger, IConfiguration configuration, IHostingEnvironment hostingEnvironment)
        {
            _iInfographicsRepository = iInfographicsRepository;
            _logger = logger;
            _hostingEnvironment = hostingEnvironment;
            _configuration = configuration;
        }

        public IViewModel CreateUpdate(InfographicsViewModel viewModel)
        {
            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while uploading a new Infographics, please contact Admin for more details."
            };

            try
            {

                var isSuccess = (viewModel.FileByte != null && viewModel.FileByte.Length > 0) ? UploadFile(viewModel) : (viewModel.Id == Guid.Empty? false: true);
                if (isSuccess)
                {
                    var result = _iInfographicsRepository.CreateUpdate(viewModel);
                    if (result != null && result.IsSuccess)
                    {
                        viewData.IsSuccess = true;
                        viewData.Message = "Infographics upload successfully.";
                    }
                    else
                    {
                        viewData.IsSuccess = false;
                        viewData.Message = result.Error;
                    }
                }
                else
                {
                    var result = _iInfographicsRepository.CreateUpdate(viewModel);
                    if (result != null && result.IsSuccess)
                    {
                        viewData.IsSuccess = true;
                        viewData.Message = "Infographics upload successfully.";
                        //viewData.Message = message contains Guid;
                    }
                    else
                    {
                        viewData.IsSuccess = false;
                        viewData.Message = result.Error;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return viewData;
        }


        public bool UploadFile(InfographicsViewModel viewModel)
        {
            bool isCopied = false;
            try
            {

                //1 check if the file length is greater than 0 bytes 


                var fileExt = Path.GetExtension(viewModel.ImageActualName);
                var newFileName = string.IsNullOrEmpty(viewModel.ImageName) ? Guid.NewGuid().ToString() + fileExt : viewModel.ImageName;
                var path = _configuration.GetSection("AppUploadPath").GetSection("Infographics").GetSection("Images").Value;
                var fullPath = Path.Combine(_hostingEnvironment.ContentRootPath, path, newFileName);
                viewModel.ImagePath = path.ToString();
                viewModel.ImageName = newFileName.ToString();
                //2 Get the extension of the file

                //3 check the file extension as png
                //if (extension == ".png" || extension == ".jpg")
                //{

                isCopied = FileUpload.ByteArrayToFile(viewModel.FileByte, fullPath);


                //}
                //else
                //{
                //    throw new Exception("File must be either .png or .JPG");
                //}


            }
            catch (Exception ex)
            {
                //throw ex;
            }
            return isCopied;
        }

        public InfographicsViewModel GetById(Guid guid)
        {

            var resModel = (guid == Guid.Empty ? null : _iInfographicsRepository.GetById(guid));

            if(resModel != null)
            {                
                var path = _configuration.GetSection("AppUploadPath").GetSection("Infographics").GetSection("Images").Value;
                if (!string.IsNullOrEmpty(resModel.ImageName))
                {
                    var fullPath = Path.Combine(_hostingEnvironment.ContentRootPath, path, resModel.ImageName);
                    resModel.ImagePath = FileOperations.GetImageBase64Data(fullPath);
                }
            }

            return resModel;
        }

        public IEnumerable<InfographicsViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, Guid? projectId = null, Guid? lookupCategoryId = null)
        {
            var resModel = _iInfographicsRepository.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, projectId, lookupCategoryId);
            var path = _configuration.GetSection("AppUploadPath").GetSection("Infographics").GetSection("Images").Value;
            foreach (var item in resModel)
            {
                if (!string.IsNullOrEmpty(item.ImageName))
                {
                    var fullPath = Path.Combine(_hostingEnvironment.ContentRootPath, path, item.ImageName);
                    item.ImagePath = FileOperations.GetImageBase64Data(fullPath);
                }
            }
            return resModel;
        }

        public IViewModel Delete(Guid Id)
        {
            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while deleteing Infographics, please contact Admin for more details."
            };

            try
            {

                var resModel = Id== Guid.Empty? null : _iInfographicsRepository.GetById(Id);
                if (resModel != null)
                {
                    var resFileDelete = true;
                    if (!string.IsNullOrEmpty(resModel.ImageName))
                    {
                        var path = _configuration.GetSection("AppUploadPath").GetSection("Infographics").GetSection("Images").Value;
                        var fullPath = Path.Combine(_hostingEnvironment.ContentRootPath, resModel.ImagePath, resModel.ImageName);

                        resFileDelete = FileUpload.DeleteFile(fullPath);
                    }
                    if (resFileDelete)
                    {
                        var result = _iInfographicsRepository.Delete(Id);
                        if (result != null && result.IsSuccess)
                        {
                            viewData.IsSuccess = true;
                            viewData.Message = "Infographics is deleted successfully.";
                        }
                        else
                        {
                            viewData.Message = result.Error;
                        }
                    }
                    else
                    {

                        viewData.IsSuccess = false;
                        viewData.Message = "Error while deleteing Infographics, please contact Admin for more details.";
                    }
                }
                else
                {
                    viewData.IsSuccess = false;
                    viewData.Message = "There is no records to delete.";
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return viewData;
        }
    }
}
