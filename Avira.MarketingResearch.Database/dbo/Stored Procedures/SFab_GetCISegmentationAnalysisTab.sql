﻿

CREATE   Procedure [dbo].[SFab_GetCISegmentationAnalysisTab] 
(
	@CompanyID uniqueidentifier 
)
As
/*================================================================================  
Procedure Name: [SFab_GetCISegmentationAnalysisTab]  
Author: Harshal 
Create date: 18-Nov-2019  
Description: Get CI Company Financial Analysis Segmentation Analysis Tab Data
Change History  
Date         Developer     Reason  
____________ __________  ______________________________________________________  
18-Nov-2019   Harshal       Initial Version  
================================================================================*/  
BEGIN
	
	Declare @UnitId uniqueidentifier
	Declare @USDCurrencyId uniqueidentifier

	Select @USDCurrencyId = [dbo].[fun_GetCurrencyIDForUSD]()

	Select @UnitId = Id 
	From ValueConversion
	Where ValueName = 'Millions';
	
	Declare @Years Table (CompanyID uniqueidentifier, [Year] int)
	Declare @MaxYear int, @MinYear int

	Select @MaxYear = Max([Year]), @MinYear = Min([Year])
	From CompanySegmentInformation
	Where CompanyId = @CompanyID;	
	
	If (@MaxYear Is Not Null And @MinYear Is Not Null)
	Begin
		WITH CTE
			 AS (SELECT @CompanyID As CompanyID, @MinYear As Year1
				 UNION ALL
				 SELECT @CompanyID As CompanyID, Year1 + 1 
				 FROM   CTE
				 WHERE  Year1 < @MaxYear)
		Insert Into @Years
		SELECT *
		FROM   CTE 
	End

	Declare @CICompanyFinancialSegmentationAnalysisData Table(
		CompanyId	uniqueidentifier,
		[Year]	int,
		[Type] nvarchar(255),
		[Name] nvarchar(255),
		[Value] decimal (19, 2)
	)

	Insert Into @CICompanyFinancialSegmentationAnalysisData (CompanyId, [Year],[Type],[Name],[Value])
	Select Y.CompanyId, Y.[Year],CBI.Type,CBI.Name, 
		[dbo].[fun_UnitDeConversion](@UnitId, Cast(CBI.Value as decimal(19, 2))) As [Value]
	From @Years Y Left Join CompanySegmentInformation CBI On Y.[Year] = CBI.[Year] And CBI.CompanyId = @CompanyID	
	Order By Y.[Year]

	
	Select CompanyId, [Year],[Type],[Name],[Value]
	From @CICompanyFinancialSegmentationAnalysisData
	Order By [Year],[Value] Desc
END