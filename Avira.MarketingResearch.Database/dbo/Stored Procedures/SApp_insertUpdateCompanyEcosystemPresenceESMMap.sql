﻿
CREATE procedure [dbo].[SApp_insertUpdateCompanyEcosystemPresenceESMMap]
(
	@CompanyId uniqueidentifier,
	@EsmIdList dbo.udtGUIDBitValue READONLY,
	@UserCreatedById uniqueidentifier,  
	@CreatedOn Datetime
)
as
/*================================================================================
Procedure Name: [SApp_insertUpdateCompanyEcosystemPresenceESMMap]
Author: Sai Krishna
Create date: 08/05/2019
Description: Insert a record into CompanyEcosystemPresenceESMMap table
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
08/05/2019	Sai Krishna		Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ErrorMsg NVARCHAR(2048);

BEGIN TRY

delete [dbo].[CompanyEcosystemPresenceESMMap]
where [CompanyId] = @CompanyId 

INSERT INTO [dbo].[CompanyEcosystemPresenceESMMap]
           ([Id]
           ,[CompanyId]
           ,[ESMId]
		   ,[IsChecked]
		   ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted]
           )
   select NEWID(),
   @CompanyId,
   esm.ID,
   esm.bitValue,
   @CreatedOn,
   @UserCreatedById,
   0
   from @EsmIdList esm

SELECT StoredProcedureName ='SApp_insertUpdateCompanyEcosystemPresenceESMMap',Message =@ErrorMsg,Success = 1;  
END TRY
BEGIN CATCH
    -- Execute the error retrieval routine.
	DECLARE @ErrorNumber	int;
	DECLARE @ErrorSeverity	int;
	DECLARE @ErrorProcedure	varchar(100);
	DECLARE @ErrorLine	int;
	DECLARE @ErrorMessage	varchar(500);

	 SELECT @ErrorNumber = ERROR_NUMBER(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorProcedure = ERROR_PROCEDURE(),
        @ErrorLine = ERROR_LINE(),
        @ErrorMessage = ERROR_MESSAGE()

	insert into dbo.Errorlog(ErrorNumber,
							ErrorSeverity,
							ErrorProcedure,
							ErrorLine,
							ErrorMessage,
							ErrorDate)
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,@CreatedOn);

	set @ErrorMsg = 'Error, please contact Admin for more details.';

SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;

END CATCH


end