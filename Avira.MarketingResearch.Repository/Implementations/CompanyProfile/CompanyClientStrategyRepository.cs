﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.CompanyProfile
{ 
    public class CompanyClientStrategyRepository : ICompanyClientsStrategyRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "CompanyClientStrategy";

        public CompanyClientStrategyRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        } 
        
        public IEnumerable<CompanyClientStrategyImportanceViewModel> GetAllImportance()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<CompanyClientStrategyImportanceViewModel>(ClientAndStrategyQueries.StoreProcSelectAllImportance
                            , null
                            , null, true, null, CommandType.StoredProcedure);
        }

        public IEnumerable<CompanyClientStrategyDirectionViewModel> GetAllImpactDirection()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<CompanyClientStrategyDirectionViewModel>(ClientAndStrategyQueries.StoreProcSelectAllImpactDirection
                            , null
                            , null, true, null, CommandType.StoredProcedure);
        }

        public IEnumerable<CompanyClientStrategyCategoryViewModel> GetAllCategory()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<CompanyClientStrategyCategoryViewModel>(ClientAndStrategyQueries.StoreProcSelectAllCategories
                            , null
                            , null, true, null, CommandType.StoredProcedure);
        }

        public IEnumerable<CompanyClientStrategyApproachViewModel> GetAllApproach()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<CompanyClientStrategyApproachViewModel>(ClientAndStrategyQueries.StoreProcSelectAllApproach
                            , null
                            , null, true, null, CommandType.StoredProcedure);
        }

        public IEnumerable<CompanyClientStrategyIndustryViewModel> GetAllIndustries()
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<CompanyClientStrategyIndustryViewModel>(ClientAndStrategyQueries.StoreProcSelectAllIndustry
                            , null
                            , null, true, null, CommandType.StoredProcedure);
        }

        public IEnumerable<CompanyStrategyViewModel> GetstrategyById(Guid? guid)
        {
            var dapperStrategy = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapperStrategy.Query<CompanyStrategyViewModel>(ClientAndStrategyQueries.StoreProcSelectStrategy
                            , new { @ComapanyID = guid }
                            , null, true, null, CommandType.StoredProcedure);
        }

        public IEnumerable<CompanyclientViewModel> GetclientById(Guid? guid)
        {
            var dapperclient = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapperclient.Query<CompanyclientViewModel>(ClientAndStrategyQueries.StoreProcSelectclients
                            , new { @ComapanyID = guid }
                            , null, true, null, CommandType.StoredProcedure);

        }

        public Result<SpTransactionMessage> InsertUpdate(CompanyClientStrategyInsertUpdateDbViewModel viewModel, AviraUser aviraUser)
        {
            DataTable CompanystrategyTable = new DataTable();
            CompanystrategyTable.Columns.Add("Id", typeof(Guid));
            CompanystrategyTable.Columns.Add("CompanyId", typeof(Guid));
            CompanystrategyTable.Columns.Add("Date", typeof(DateTime));
            CompanystrategyTable.Columns.Add("Approach", typeof(Guid));
            CompanystrategyTable.Columns.Add("Strategy", typeof(string));
            CompanystrategyTable.Columns.Add("Category", typeof(Guid));
            CompanystrategyTable.Columns.Add("Importance", typeof(Guid));
            CompanystrategyTable.Columns.Add("Impact", typeof(Guid));
            CompanystrategyTable.Columns.Add("Remark", typeof(string));
            if (viewModel.CompanyStrategyViewModel != null & viewModel.CompanyStrategyViewModel.Count > 0)
            {
                foreach (var item in viewModel.CompanyStrategyViewModel)
                {
                    if (item.Id == Guid.Empty)
                    {
                        item.Id = Guid.NewGuid();
                    }
                    item.CompanyId = viewModel.CompanyId;
                    CompanystrategyTable.Rows.Add(new Object[] { item.Id, item.CompanyId, item.Date, item.ApproachId, item.Strategy, item.CategoryId, item.ImportanceId, item.ImpactId,item.Remark });
                }
            }

            DataTable CompanyClientTable = new DataTable();
            CompanyClientTable.Columns.Add("Id", typeof(Guid));
            CompanyClientTable.Columns.Add("CompanyId", typeof(Guid));
            CompanyClientTable.Columns.Add("ClientName", typeof(string));
            CompanyClientTable.Columns.Add("ClientIndustry", typeof(Guid));
            CompanyClientTable.Columns.Add("ClientRemark", typeof(string));
            
            if (viewModel.CompanyclientViewModel != null & viewModel.CompanyclientViewModel.Count > 0)
            {
                foreach (var item in viewModel.CompanyclientViewModel)
                {
                    if (item.Id == Guid.Empty)
                    {
                        item.Id = Guid.NewGuid();
                    }
                    item.CompanyId = viewModel.CompanyId;
                    CompanyClientTable.Rows.Add(new Object[] { item.Id, item.CompanyId, item.ClientName, item.ClientIndustry, item.ClientRemark});
                }
            }


            var spTransactionMessage = _dbHelper.ExecuteQuery(ClientAndStrategyQueries.StoreProcInsert,
               new
               {
                   @CompanyStrategy = CompanystrategyTable,
                   @CompanyClients = CompanyClientTable,
                   @CompanyId = viewModel.CompanyId,
                   @UserCreatedById = viewModel.AviraUserId
               }
                    , CommandType.StoredProcedure);
            return spTransactionMessage;
        }
    }
}
