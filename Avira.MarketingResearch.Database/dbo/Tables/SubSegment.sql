﻿CREATE TABLE [dbo].[SubSegment] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [SegmentId]        UNIQUEIDENTIFIER NOT NULL,
    [SubSegmentName]   NVARCHAR (256)   CONSTRAINT [DF_SubSegment_SubSegmentName] DEFAULT ('') NOT NULL,
    [Description]      NVARCHAR (256)   NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_SubSegment_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    [ParentId]         UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_SubSegment] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_SubSegment_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_SubSegment_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_SubSegment_Segment] FOREIGN KEY ([SegmentId]) REFERENCES [dbo].[Segment] ([Id]),
    CONSTRAINT [FK_SubSegment_SubSegment] FOREIGN KEY ([ParentId]) REFERENCES [dbo].[SubSegment] ([Id]),
    CONSTRAINT [IX_SubSegment] UNIQUE NONCLUSTERED ([SubSegmentName] ASC, [SegmentId] ASC)
);





