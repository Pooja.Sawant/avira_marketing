﻿CREATE TABLE [dbo].[CompanyProductCountryMap] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [CompanyProductId] UNIQUEIDENTIFIER NULL,
    [CountryId]        UNIQUEIDENTIFIER NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_CompanyProductCountryMap_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CompanyProductCountryMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CompanyProductCountryMap_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyProductCountryMap_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyProductCountryMap_AviraUser2] FOREIGN KEY ([UserDeletedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyProductCountryMap_Country] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Country] ([Id])
);

