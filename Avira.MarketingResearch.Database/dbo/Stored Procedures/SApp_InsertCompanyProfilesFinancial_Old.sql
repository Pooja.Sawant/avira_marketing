﻿                  
CREATE PROCEDURE [dbo].[SApp_InsertCompanyProfilesFinancial_Old]                
AS                
/*================================================================================                    
Procedure Name: SApp_InsertCompanyProfilesFinancial                   
Change History                    
Date  Developer  Reason                    
__________ ____________ ______________________________________________________                    
07/25/2019 Gopi   Initial Version                    
================================================================================*/                 
            
            
declare @ErrorMsg NVARCHAR(2048),@CreatedOn Datetime                
BEGIN                   
BEGIN TRY    
       
--Delete OldMapping Records.  
Delete from [dbo].[CompanyProfilesFinancial]    
Where [CompanyProfilesFinancial].[CompanyId] in     
(select SCPF.CompanyId  
from StagingCompanyProfilesFinancial SCPF)  
         
SELECT StoredProcedureName ='SApp_InsertCompanyProfilesFinancial',Message =@ErrorMsg,Success = 1;                   
END TRY                    
BEGIN CATCH                    
    -- Execute the error retrieval routine.                    
 DECLARE @ErrorNumber int;                    
 DECLARE @ErrorSeverity int;                    
 DECLARE @ErrorProcedure varchar(100);                    
 DECLARE @ErrorLine int;                    
 DECLARE @ErrorMessage varchar(500);                    
                    
  SELECT @ErrorNumber = ERROR_NUMBER(),                    
        @ErrorSeverity = ERROR_SEVERITY(),                    
        @ErrorProcedure = ERROR_PROCEDURE(),                    
        @ErrorLine = ERROR_LINE(),                    
        @ErrorMessage = ERROR_MESSAGE()                    
                    
 insert into dbo.Errorlog(ErrorNumber,                    
       ErrorSeverity,                    
       ErrorProcedure,                    
       ErrorLine,                    
       ErrorMessage,                    
       ErrorDate)                    
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,@CreatedOn);                    
                  
 set @ErrorMsg = 'Error while inserting CompanyProfilesFinancial, please contact Admin for more details.';                    
--THROW 50000,@ErrorMsg,1;                      
--return(1)                  
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine                  
    ,Message =@ErrorMsg,Success = 0;                     
                     
END CATCH                     
                
                
END