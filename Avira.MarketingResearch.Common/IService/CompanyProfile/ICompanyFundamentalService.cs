﻿using System;
using System.Collections.Generic;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using System.Text;
using System.Threading.Tasks;

namespace Avira.MarketingResearch.Common.IService.CompanyProfile
{
    public interface ICompanyFundamentalService
    {
        IEnumerable<CompanyFundamentalViewModel> GetAll();
        IEnumerable<CompanyRegionViewModel> GetAllRegion();
        IEnumerable<CompanyEntityViewModel> GetAllEntities();
        IEnumerable<CompanyManagerViewModel> ManagersList(string companyId);
        IEnumerable<CompanyNatureViewModel> NaturesList();
        IEnumerable<CompanySubsidaryViewModel> GetCompanySubsidaries(Guid Id);
        //IEnumerable<CompanyDetailsViewModel> GetCompanyDetails(Guid CompanyId, Guid ProjectId);

        Task<CompanyDetailsInsertUpdateViewModel> GetCompanyDetails(Guid CompanyId, Guid ProjectId);

        IEnumerable<AllCompaniesListViewModel> GetAllCompaniesList();

        IEnumerable<DesignationViewModel> AllDesignations();

        IEnumerable<AllCompanyStageListViewModel> GetAllCompanyStageList();

        Result<SpTransactionMessage> InsertUpdate(CompanyDetailsInsertUpdateViewModel tfc, AviraUser currenUser);
    }

}
