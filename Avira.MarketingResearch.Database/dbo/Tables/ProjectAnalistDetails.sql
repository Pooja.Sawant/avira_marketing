﻿CREATE TABLE [dbo].[ProjectAnalistDetails] (
    [ID]               UNIQUEIDENTIFIER NOT NULL,
    [ProjectID]        UNIQUEIDENTIFIER NOT NULL,
    [AnalysisType]     NVARCHAR (100)   NULL,
    [AnalystType]      NVARCHAR (100)   NULL,
    [AnalystUserID]    UNIQUEIDENTIFIER NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_ProjectAnalistDetails] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK__ProjectAn__Proje__405A880E] FOREIGN KEY ([ProjectID]) REFERENCES [dbo].[Project] ([ID]),
    CONSTRAINT [FK_ProjectAnalistDetails_AnalistUser] FOREIGN KEY ([AnalystUserID]) REFERENCES [dbo].[AviraUser] ([Id])
);


GO

GO

GO

GO

GO