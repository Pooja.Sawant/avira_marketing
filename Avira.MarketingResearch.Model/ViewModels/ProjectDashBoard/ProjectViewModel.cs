﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard
{
    public class ProjectViewModel
    {
        //public string ProjectCode { get; set; }
        //public string ProjectName { get; set; }
        //public string Remark { get; set; }
        //public DateTime StartDate { get; set; }
        //public DateTime EndDate { get; set; }
        //public int MinTrends { get; set; }
        //public int MinCompanyProfiles { get; set; }
        //public int MinPrimaries { get; set; }
        public decimal Complition { get; set; }
        public decimal Efficiency { get; set; }
        public decimal Approved { get; set; }
        public Guid ProjectStatusID { get; set; }
        //public string ProjectStatusName { get; set; }
        public string AnalysisType { get; set; }
        public string ProjectAuthorName { get; set; }
        public string ProjectApproverName { get; set; }
        public string ProjectCoAuthorName { get; set; }
        //public string MApproverApprover { get; set; }
        //public string MCoApproverCoApprover { get; set; }
        //public string TrendsFormsAuthor { get; set; }
        //public string TrendsFormsCoAuthor { get; set; }
        public string Approver { get; set; }
        public string CoApprover { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        //public Guid Id { get; set; }
        public int TotalItems { get; set; }   
        public string RowType
        {
            get
            {
                return "Projects";
            }
        }

        public Guid Id { get; set; }
        public string ProjectName { get; set; }
        public string ProjectCode { get; set; }
        public string Remark { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int MinTrends { get; set; }
        public int MinCompanyProfiles { get; set; }
        public int MinPrimaries { get; set; }
        public string CompanyProfilesAuthor { get; set; }
        public string CompanyProfilesCoAuthor { get; set; }
        public string MarketSizingAuthor { get; set; }
        public string MarketSizingCoAuthor { get; set; }
        public string PrimariesInfoAuthor { get; set; }
        public string PrimariesInfoCoAuthor { get; set; }
        public string QuantativeinfoAuthor { get; set; }
        public string QuantativeinfoCoAuthor { get; set; }
        public string TrendsFormsAuthor { get; set; }
        public string TrendsFormsCoAuthor { get; set; }
        public string TrendsFormsApprover { get; set; }
        public string TrendsFormsCoApprover { get; set; }
        public string MApproverApprover { get; set; }
        public string MApproverCoApprover { get; set; }
        public string MCoApproverApprover { get; set; }
        public string MCoApproverCoApprover { get; set; }
        public string ProjectStatusName { get; set; }
    }
    public class MainProjectDBViewModel
    {
        public ProjectViewModel projectViewModel { get; set; }
        public ProjectAnalistDetailsViewModel projectAnalistDetailsViewModel { get; set; }
        public ProjectSegmentMappingViewModel projectSegmentMappingViewModel { get; set; }
    }
    public class ProjectInsertDbViewModel
    {
        [Required]
        [Display(Name = "Project Name")]
        public string ProjectName { get; set; }     
        public string Remark { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int MinTrends { get; set; }
        public int MinCompanyProfiles { get; set; }
        public int MinPrimaries { get; set; }
        public Guid AuthorUserID { get; set; }       
        public Guid ProjectID { get; set; }

        
    }
    public class MainProjectInsertDBViewModel
    {
        public ProjectInsertDbViewModel projectInsertDbViewModel { get; set; }
        public ProjectAnalistDetailsInsertDbViewModel projectAnalistDetailsInsertTrendDbViewModel { get; set; }
        public ProjectAnalistDetailsInsertDbViewModel projectAnalistDetailsInsertCompnyDbViewModel { get; set; }
        public ProjectAnalistDetailsInsertDbViewModel projectAnalistDetailsInsertMarketDbViewModel { get; set; }
        public ProjectAnalistDetailsInsertDbViewModel projectAnalistDetailsInsertQuantDbViewModel { get; set; }
        public ProjectAnalistDetailsInsertDbViewModel projectAnalistDetailsInsertPrimarDbViewModel { get; set; }
        public ProjectAnalistDetailsInsertDbViewModel projectAnalistDetailsInsertApproverDbViewModel { get; set; }
        public ProjectAnalistDetailsInsertDbViewModel projectAnalistDetailsInsertCoApproDbViewModel { get; set; }

        public ProjectSegmentMappingInsertDbViewModel projectSegmentMappingInsertInduDbViewModel { get; set; }
        public ProjectSegmentMappingInsertDbViewModel projectSegmentMappingInsertSubInduDbViewModel { get; set; }
        public ProjectSegmentMappingInsertDbViewModel projectSegmentMappingInsertMarketDbViewModel { get; set; }
        public ProjectSegmentMappingInsertDbViewModel projectSegmentMappingInsertSubMarkDbViewModel { get; set; }
    }

    public class ProjectUpdateDbViewModel
    {
        [Required]
        [Display(Name = "Project Name")]
        public string ProjectName { get; set; }
        public string ProjectCode { get; set; }
        public string Remark { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int MinTrends { get; set; }
        public int MinCompanyProfiles { get; set; }
        public int MinPrimaries { get; set; }
        public string ProjectStatusName { get; set; }
        public string ProjectStatusChange { get; set; }

        [Required]
        public Guid ProjectID { get; set; }
    }
    public class MainProjectUpdateDBViewModel
    {
        public ProjectUpdateDbViewModel projectUpdateDbViewModel { get; set; }
        public ProjectAnalistDetailsUpdateDbViewModel projectAnalistDetailsUpdateTrendDbViewModel { get; set; }
        public ProjectAnalistDetailsUpdateDbViewModel projectAnalistDetailsUpdateCompanyDbViewModel { get; set; }
        public ProjectAnalistDetailsUpdateDbViewModel projectAnalistDetailsUpdateMarketDbViewModel { get; set; }
        public ProjectAnalistDetailsUpdateDbViewModel projectAnalistDetailsUpdateQuantDbViewModel { get; set; }
        public ProjectAnalistDetailsUpdateDbViewModel projectAnalistDetailsUpdatePrimiDbViewModel { get; set; }
        public ProjectAnalistDetailsUpdateDbViewModel projectAnalistDetailsUpdateApproverDbViewModel { get; set; }
        public ProjectAnalistDetailsUpdateDbViewModel projectAnalistDetailsUpdateCoApproverDbViewModel { get; set; }
        public ProjectSegmentMappingUpdateDbViewModel projectSegmentMappingUpdateDbViewModel { get; set; }
    }
    public class DisplayProjectDBViewModel
    {
        public Guid Id { get; set; }
        public string ProjectName { get; set; }
        public string ProjectCode { get; set; }
        public string Remark { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int MinTrends { get; set; }
        public int MinCompanyProfiles { get; set; }
        public int MinPrimaries { get; set; }
        public string CompanyProfilesAuthor { get; set; }
        public string CompanyProfilesCoAuthor { get; set; }
        public string MarketSizingAuthor { get; set; }
        public string MarketSizingCoAuthor { get; set; }
        public string PrimariesInfoAuthor { get; set; }
        public string PrimariesInfoCoAuthor { get; set; }
        public string QuantativeinfoAuthor { get; set; }
        public string QuantativeinfoCoAuthor { get; set; }
        public string TrendsFormsAuthor { get; set; }
        public string TrendsFormsCoAuthor { get; set; }
        public string TrendsFormsApprover { get; set; }
        public string TrendsFormsCoApprover { get; set; }
        public string MApproverApprover { get; set; }
        public string MApproverCoApprover { get; set; }
        public string MCoApproverApprover { get; set; }
        public string MCoApproverCoApprover { get; set; }
        public string ProjectStatusName { get; set; }

    }
    public class JsopStringClass
    {
        public string Id { get; set; }
        public string ResourceName { get; set; }
    }

    public class SegmentnameViewModel
    {
        public string Markets { get; set; }
        public string SubMarkets { get; set; }
        public string BroadIndustries { get; set; }
        public string Industries { get; set; }
    }
}
