﻿CREATE procedure [dbo].[SApp_GetCustomerbyID]
(@CustomerID uniqueidentifier,
@includeDeleted bit = 0)
as
/*================================================================================
Procedure Name: SApp_GetCustomerbyID
Author: Sharath
Create date: 02/19/2019
Description: Get list from Customer table by Name or ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
02/19/2019	Sharath			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if @includeDeleted is null set @includeDeleted = 0

SELECT [Id],
		[CustomerName],
		[DisplayName]
FROM [dbo].[Customer]
where (@CustomerID is null or ID = @CustomerID)
and (@includeDeleted = 1 or IsDeleted = 0)



end