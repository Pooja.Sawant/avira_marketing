﻿CREATE TABLE [dbo].[CompanyFinancialAnalysis] (
    [ID]               UNIQUEIDENTIFIER NOT NULL,
    [CompanyId]        UNIQUEIDENTIFIER NULL,
    [CurrencyId]       UNIQUEIDENTIFIER NULL,
    [UnitId]           UNIQUEIDENTIFIER NULL,
    [CompanyStatusId]  UNIQUEIDENTIFIER NULL,
    [AuthorNotes]      NVARCHAR (2000)  NULL,
    [ApproverNotes]    NVARCHAR (2000)  NULL,
    [CreatedOn]        DATETIME         NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME         NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NULL,
    [DeletedOn]        DATETIME         NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CompanyFinancialAnalysis] PRIMARY KEY NONCLUSTERED ([ID] ASC),
    CONSTRAINT [FK_CompanyFinancialAnalysis_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([Id]),
    CONSTRAINT [FK_CompanyFinancialAnalysis_Currency] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[Currency] ([Id]),
    CONSTRAINT [FK_CompanyFinancialAnalysis_ValueConversion] FOREIGN KEY ([UnitId]) REFERENCES [dbo].[ValueConversion] ([Id])
);

