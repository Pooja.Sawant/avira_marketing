﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.Trend
{
    public class TrendIndustryMapRepository : ITrendIndustryMapRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "TrendIndustryMapping";

        public TrendIndustryMapRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }  

        public IEnumerable<TrendIndustryMapViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted, string id = "")
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<TrendIndustryMapViewModel>(TrendIndustryMappingQueries.StoreProcSelectById
                            , new { @IndustryName = Search, @TrendID = id, @includeDeleted = false, @OrdbyByColumnName = @OrdbyByColumnName, @PageStart = PageStart, @pageSize = pageSize, @SortDirection = SortDirection }
                            , null, true, null, CommandType.StoredProcedure);
        }  

        public Result<SpTransactionMessage> Create(TrendIndustryMapInsertDbViewModel trendIndustryMapInsertDbViewModel)
        {
            DataTable trendIndustryMapTable = new DataTable();
            trendIndustryMapTable.Columns.Add("ID", typeof(Guid));
            trendIndustryMapTable.Columns.Add("MapId", typeof(Guid));
            trendIndustryMapTable.Columns.Add("bitValue", typeof(bool));
            //if (trendIndustryMapInsertDbViewModel.TrendIndustryMapViewModel != null & trendIndustryMapInsertDbViewModel.TrendIndustryMapViewModel.Count > 0)
            //{
            //    foreach (var item in trendIndustryMapInsertDbViewModel.TrendIndustryMapViewModel.Where(x => x.ImpactId != Guid.Empty))
            //    {
            //        trendIndustryMapTable.Rows.Add(new Object[] { item.IndustryId, item.ImpactId, item.IsMapped });
            //    }
            //}
            if (trendIndustryMapInsertDbViewModel.TrendIndustryMapViewModel != null & trendIndustryMapInsertDbViewModel.TrendIndustryMapViewModel.Count > 0)
            {
                foreach (var item in trendIndustryMapInsertDbViewModel.TrendIndustryMapViewModel)//.Where(x => x.ImpactId != Guid.Empty)
                {
                    trendIndustryMapTable.Rows.Add(new Object[] { item.IndustryId, item.ImpactId, item.EndUser });
                }
            }

            var spTransactionMessage = _dbHelper.ExecuteQuery(TrendIndustryMappingQueries.StoreProcInsert,
               new
               {
                   @TrendId = trendIndustryMapInsertDbViewModel.TrendId,
                   @TrendIndustrymap = trendIndustryMapTable,
                   @AviraUserId = trendIndustryMapInsertDbViewModel.AviraUserId
               }
                    , CommandType.StoredProcedure);
            return spTransactionMessage;
        } 
    }
}
