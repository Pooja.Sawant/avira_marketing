﻿CREATE procedure [dbo].[SApp_DeleteApplicationbyID]
(@ApplicationID uniqueidentifier,
@AviraUserId uniqueidentifier)
as
/*================================================================================
Procedure Name: SApp_DeleteApplicationbyID
Author: Sharath
Create date: 02/19/2019
Description: soft delete a record from Application table by ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
02/19/2019	Sharath			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @SegmentId uniqueidentifier;
select @SegmentId = Id from Segment
where SegmentName = 'Applications';

UPDATE [dbo].SubSegment
   SET [IsDeleted] = 1,
   [DeletedOn] = getdate(),
   [UserDeletedById] = @AviraUserId
where ID = @ApplicationID
and SegmentId = @SegmentId
and IsDeleted = 0 -- no need to delete record which is already deleted

end