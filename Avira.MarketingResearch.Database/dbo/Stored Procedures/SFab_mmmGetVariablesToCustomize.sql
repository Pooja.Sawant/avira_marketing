﻿
CREATE PROCEDURE [dbo].[SFab_mmmGetVariablesToCustomize]
(
   @Id uniqueidentifier,
   @UserId	uniqueidentifier,
   @MarketID uniqueidentifier,
   @ProjectId uniqueidentifier
)
As   

/*================================================================================    
Procedure Name: SFab_mmmGetVariablesToCustomize   
Author: Nagi   
Create date: 08-06-2020
Description: gets the variable customozations
__________ ____________ ______________________________________________________    
08-June-20  Nagi   Intial draft version    
================================================================================*/    
BEGIN  


	SET nocount ON; 
	SET TRANSACTION isolation level READ uncommitted;  

	declare @RecordCount as INT;
	select @RecordCount = COUNT(*) from MMM_EditMarket_SaveSettings where UserId = @UserId and ProjectID = @ProjectId and MarketID = @MarketID;

	if (@RecordCount = 0)
	BEGIN

	INSERT INTO MMM_EditMarket_SaveSettings (Id, UserId, ProjectID, MarketID, SectionName, PageLevel, Type, VariableValue, CreatedOn, UserCreatedById)
     VALUES (newid(), @UserId, @ProjectId, @MarketID, 'Value', (select Id from MMM_PageLevels p where P.Value = 'MainSelection'), 'Variable', 0, GETDATE(), @UserId);

	INSERT INTO MMM_EditMarket_SaveSettings (Id, UserId, ProjectID, MarketID, SectionName, PageLevel, Type, VariableValue, CreatedOn, UserCreatedById)
     VALUES (newid(), @UserId, @ProjectId, @MarketID, 'Growth%', (select Id from MMM_PageLevels p where P.Value = 'MainSelection'), 'Variable', 0, GETDATE(), @UserId);

	INSERT INTO MMM_EditMarket_SaveSettings (Id, UserId, ProjectID, MarketID, SectionName, PageLevel, Type, VariableValue, CreatedOn, UserCreatedById)
     VALUES (newid(), @UserId, @ProjectId, @MarketID, 'ASP', (select Id from MMM_PageLevels p where P.Value = 'MainSelection'), 'Variable', 0, GETDATE(), @UserId);

	INSERT INTO MMM_EditMarket_SaveSettings (Id, UserId, ProjectID, MarketID, SectionName, PageLevel, Type, VariableValue, CreatedOn, UserCreatedById)
     VALUES (newid(), @UserId, @ProjectId, @MarketID, 'Volume', (select Id from MMM_PageLevels p where P.Value = 'MainSelection'), 'Variable', 0, GETDATE(), @UserId);

	INSERT INTO MMM_EditMarket_SaveSettings (Id, UserId, ProjectID, MarketID, SectionName, PageLevel, Type, VariableValue, CreatedOn, UserCreatedById)
     VALUES (newid(), @UserId, @ProjectId, @MarketID, 'Trend Impact', (select Id from MMM_PageLevels p where P.Value = 'MainSelection'), 'Variable', 0, GETDATE(), @UserId);

	INSERT INTO MMM_EditMarket_SaveSettings (Id, UserId, ProjectID, MarketID, SectionName, PageLevel, Type, VariableValue, CreatedOn, UserCreatedById)
     VALUES (newid(), @UserId, @ProjectId, @MarketID, 'Broad Level', (select Id from MMM_PageLevels p where P.Value = 'MainSelection'), 'Level', 0, GETDATE(), @UserId);

	INSERT INTO MMM_EditMarket_SaveSettings (Id, UserId, ProjectID, MarketID, SectionName, PageLevel, Type, VariableValue, CreatedOn, UserCreatedById)
     VALUES (newid(), @UserId, @ProjectId, @MarketID, 'Segment Level', (select Id from MMM_PageLevels p where P.Value = 'MainSelection'), 'Level', 0, GETDATE(), @UserId);

	 --Insert the segement details of the project
	MERGE mmm_editmarket_savesettings AS T  
    USING (select S.SegmentName, Q.ProjectID from Segment S, QualitativeSegmentMapping Q where  Q.SegmentID = S.Id and Q.ProjectID = @ProjectId) AS S 
    ON (T.ProjectID = S.ProjectID AND T.UserId = @UserId AND T.MarketID = @MarketID)  
    WHEN NOT MATCHED THEN  
        INSERT (Id, UserId, ProjectID, MarketID, SectionName, PageLevel, Type, VariableValue, CreatedOn, UserCreatedById)  
        VALUES (newid(), @UserId, @ProjectId, @MarketID, S.SegmentName, (select Id from MMM_PageLevels p where P.Value = 'BroadCustomSelection'), 'BroadVariable', 0, GETDATE(), @UserId);

		--insert the subsegent details of the project
	MERGE mmm_editmarket_savesettings AS T  
    USING (select S.SegmentName, q.ProjectID, SS.SubSegmentName from Segment S, QualitativeSegmentMapping Q, SubSegment SS where  q.SegmentID = S.Id and SS.id = q.SubSegmentId and Q.ProjectID = @ProjectId) AS S 
    ON (T.ProjectID = S.ProjectID AND T.UserId = @UserId AND T.MarketID = @MarketID and T.SectionName = s.SegmentName)  
    WHEN NOT MATCHED THEN  
        INSERT (Id, UserId, ProjectID, MarketID, SectionName, PageLevel, Type, VariableValue, CreatedOn, UserCreatedById)  
        VALUES (newid(), @UserId, @ProjectId, @MarketID, S.SubSegmentName, (select Id from MMM_PageLevels p where P.Value = 'SegmentCustomSelection'), S.SegmentName, 0, GETDATE(), @UserId);


	INSERT INTO MMM_EditMarket_SaveSettings (Id, UserId, ProjectID, MarketID, SectionName, PageLevel, Type, VariableValue, CreatedOn, UserCreatedById)
		VALUES (newid(), @UserId, @ProjectId, @MarketID, 'Value', (select Id from MMM_PageLevels p where P.Value = 'ConstantSelection'), 'ConstantCustomizeLabel', 0, GETDATE(), @UserId);

	INSERT INTO MMM_EditMarket_SaveSettings (Id, UserId, ProjectID, MarketID, SectionName, PageLevel, Type, VariableValue, CreatedOn, UserCreatedById)
		VALUES (newid(), @UserId, @ProjectId, @MarketID, 'Growth%', (select Id from MMM_PageLevels p where P.Value = 'ConstantSelection'), 'ConstantCustomizeLabel', 0, GETDATE(), @UserId);

	INSERT INTO MMM_EditMarket_SaveSettings (Id, UserId, ProjectID, MarketID, SectionName, PageLevel, Type, VariableValue, CreatedOn, UserCreatedById)
		VALUES (newid(), @UserId, @ProjectId, @MarketID, 'ASP', (select Id from MMM_PageLevels p where P.Value = 'ConstantSelection'), 'ConstantCustomizeLabel', 0, GETDATE(), @UserId);

	INSERT INTO MMM_EditMarket_SaveSettings (Id, UserId, ProjectID, MarketID, SectionName, PageLevel, Type, VariableValue, CreatedOn, UserCreatedById)
		VALUES (newid(), @UserId, @ProjectId, @MarketID, 'Volume', (select Id from MMM_PageLevels p where P.Value = 'ConstantSelection'), 'ConstantCustomizeLabel', 0, GETDATE(), @UserId);

	INSERT INTO MMM_EditMarket_SaveSettings (Id, UserId, ProjectID, MarketID, SectionName, PageLevel, Type, VariableValue, CreatedOn, UserCreatedById)
		VALUES (newid(), @UserId, @ProjectId, @MarketID, 'Trend Impact', (select Id from MMM_PageLevels p where P.Value = 'ConstantSelection'), 'ConstantCustomizeLabel', 0, GETDATE(), @UserId);


	INSERT INTO MMM_EditMarket_SaveSettings (Id, UserId, ProjectID, MarketID, SectionName, PageLevel, Type, VariableValue, CreatedOn, UserCreatedById)
		VALUES (newid(), @UserId, @ProjectId, @MarketID, 'Change Market Size', (select Id from MMM_PageLevels p where P.Value = 'ConstantSelection'), 'ConstantOverAllMarketLabel', 0, GETDATE(), @UserId);

	INSERT INTO MMM_EditMarket_SaveSettings (Id, UserId, ProjectID, MarketID, SectionName, PageLevel, Type, VariableValue, CreatedOn, UserCreatedById)
		VALUES (newid(), @UserId, @ProjectId, @MarketID, 'Keep Market Size Same', (select Id from MMM_PageLevels p where P.Value = 'ConstantSelection'), 'ConstantOverAllMarketLabel', 0, GETDATE(), @UserId);


	INSERT INTO MMM_EditMarket_SaveSettings (Id, UserId, ProjectID, MarketID, SectionName, PageLevel, Type, VariableValue, CreatedOn, UserCreatedById)
		VALUES (newid(), @UserId, @ProjectId, @MarketID, 'Proportionate Change', (select Id from MMM_PageLevels p where P.Value = 'ConstantSelection'), 'ConstantOtherBroadLabel', 0, GETDATE(), @UserId);

	INSERT INTO MMM_EditMarket_SaveSettings (Id, UserId, ProjectID, MarketID, SectionName, PageLevel, Type, VariableValue, CreatedOn, UserCreatedById)
		VALUES (newid(), @UserId, @ProjectId, @MarketID, 'Value', (select Id from MMM_PageLevels p where P.Value = 'ConstantSelection'), 'ConstantOtherSegmentLabel', 0, GETDATE(), @UserId);

	INSERT INTO MMM_EditMarket_SaveSettings (Id, UserId, ProjectID, MarketID, SectionName, PageLevel, Type, VariableValue, CreatedOn, UserCreatedById)
		VALUES (newid(), @UserId, @ProjectId, @MarketID, 'Growth%', (select Id from MMM_PageLevels p where P.Value = 'ConstantSelection'), 'ConstantOtherSegmentLabel', 0, GETDATE(), @UserId);

	INSERT INTO MMM_EditMarket_SaveSettings (Id, UserId, ProjectID, MarketID, SectionName, PageLevel, Type, VariableValue, CreatedOn, UserCreatedById)
		VALUES (newid(), @UserId, @ProjectId, @MarketID, 'ASP', (select Id from MMM_PageLevels p where P.Value = 'ConstantSelection'), 'ConstantOtherSegmentLabel', 0, GETDATE(), @UserId);

	INSERT INTO MMM_EditMarket_SaveSettings (Id, UserId, ProjectID, MarketID, SectionName, PageLevel, Type, VariableValue, CreatedOn, UserCreatedById)
		VALUES (newid(), @UserId, @ProjectId, @MarketID, 'Volume', (select Id from MMM_PageLevels p where P.Value = 'ConstantSelection'), 'ConstantOtherSegmentLabel', 0, GETDATE(), @UserId);

	INSERT INTO MMM_EditMarket_SaveSettings (Id, UserId, ProjectID, MarketID, SectionName, PageLevel, Type, VariableValue, CreatedOn, UserCreatedById)
		VALUES (newid(), @UserId, @ProjectId, @MarketID, 'Trend Impact', (select Id from MMM_PageLevels p where P.Value = 'ConstantSelection'), 'ConstantOtherSegmentLabel', 0, GETDATE(), @UserId);
	END

	SELECT id, 
			userid, 
			projectid, 
			MarketID, 
			pagelevel, 
			type, 
			variablevalue, 
			sectionname 
	FROM   mmm_editmarket_savesettings 
	WHERE  userid = @UserId 
			AND MarketID = @MarketID 
			AND projectid = @ProjectId 
			AND ( @Id IS NULL 
					OR id = @Id );										
		
END