﻿using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.CompanyProfile
{
    public class SegmentService : ISegmentService
    {
        private readonly ISegmentRepository _iSegmentRepository;

        public SegmentService(ISegmentRepository iSegmentRepository)
        {
            _iSegmentRepository = iSegmentRepository;
        }

        public IEnumerable<SegmentViewModel> GetAll()
        {
           return _iSegmentRepository.GetAll();
        }
    }
}
