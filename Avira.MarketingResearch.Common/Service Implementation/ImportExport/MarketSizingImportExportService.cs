﻿using Avira.MarketingResearch.Common.IService.ImportExport;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport;
using Avira.MarketingResearch.Repository.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.ImportExport
{
    public class MarketSizingImportExportService : IMarketSizingImportExportService
    {
        private readonly IMarketSizingImportExportRepository _marketSizingImportExportRepository;

        public MarketSizingImportExportService(IMarketSizingImportExportRepository buildLevelRepository)
        {
            _marketSizingImportExportRepository = buildLevelRepository;
        }

        public List<MarketSizingImportViewModel> Create(MarketSizingImportInsertViewModel viewModel, AviraUser currentUser)
        {
            //viewModel.TrendJson = JsonConvert.SerializeObject(viewModel.MarketSizingsImportViewModel);

            
            var marketImportResponse = _marketSizingImportExportRepository.Create(viewModel);
            return marketImportResponse;
        }

        public bool CreateStagging(MarketSizingImportInsertViewModel viewModel)
        {
            var marketImportResponse = _marketSizingImportExportRepository.CreateStagging(viewModel);
            return marketImportResponse;
        }

        public bool DeleteMSStagging(MarketSizingImportInsertViewModel viewModel)
        {
            var marketImportResponse = _marketSizingImportExportRepository.DeleteMSStagging(viewModel);
            return marketImportResponse;
        }
    }
}
