﻿CREATE PROCEDURE [dbo].[SFab_GetAllIndustriesViewPoint]
(
	@AviraUserID uniqueidentifier = null,  
	@AviraUserRoleId uniqueidentifier = null,
	@TrendId uniqueidentifier 
)
AS
/*================================================================================
Procedure Name: [SFab_GetAllIndustriesViewPoint]
Author: Sai Krishna
Create date: 15/05/2019
Description: Get list from Trends by project Id, Market Id, Indiustry Id
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
15/05/2019	Jagan		Initial Version
================================================================================*/
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	declare @TrendApprovedStatusId uniqueidentifier;

	select @TrendApprovedStatusId = Id
	from TrendStatus
	where TrendStatusName = 'Approved';

	--SELECT YEAR, AVG(Amount) AMOUNT FROM TrendValueMap 
	--if 2 decimal value then use below query
	SELECT YEAR, cast(SUM(Amount)  AS DECIMAL(9,4))  AMOUNT FROM TrendValueMap 
	WHERE TrendId =@TrendId and IsDeleted = 0
	and exists(select 1 from trend where TrendValueMap.TrendId = Trend.Id and  Trend.TrendStatusID = @TrendApprovedStatusId)
	GROUP BY YEAR
END