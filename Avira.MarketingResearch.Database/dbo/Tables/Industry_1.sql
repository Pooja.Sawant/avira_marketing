﻿CREATE TABLE [dbo].[Industry] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [IndustryName]     NVARCHAR (256)   CONSTRAINT [DF_Industry_IndustryName] DEFAULT ('') NOT NULL,
    [Description]      NVARCHAR (256)   NULL,
    [ParentId]         UNIQUEIDENTIFIER NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_Industry_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_Industry] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Industry_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_Industry_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_Industry_ParentId] FOREIGN KEY ([ParentId]) REFERENCES [dbo].[Industry] ([Id])
);

