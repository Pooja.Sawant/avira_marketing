﻿CREATE TABLE [dbo].[CompanyKeyEmployeesMap] (
    [Id]                     UNIQUEIDENTIFIER NOT NULL,
    [KeyEmployeeName]        NVARCHAR (256)   NULL,
    [KeyEmployeeDesignation] NVARCHAR (100)   NULL,
    [KeyEmployeeEmailId]     NVARCHAR (256)   NULL,
    [KeyEmployeeComments]    NVARCHAR (MAX)   NULL,
    [CreatedOn]              DATETIME2 (7)    NULL,
    [UserCreatedById]        UNIQUEIDENTIFIER NULL,
    [ModifiedOn]             DATETIME2 (7)    NULL,
    [UserModifiedById]       UNIQUEIDENTIFIER NULL,
    [IsDeleted]              BIT              CONSTRAINT [DF_CompanyKeyEmployeesMap_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]              DATETIME2 (7)    NULL,
    [UserDeletedById]        UNIQUEIDENTIFIER NULL,
    [CompanyId]              UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [Pk_CompanyKeyEmployeesMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CompanyKeyEmployeesMap_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyKeyEmployeesMap_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyKeyEmployeesMap_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([Id])
);

