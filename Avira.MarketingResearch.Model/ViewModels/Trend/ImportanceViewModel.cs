﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.Trend
{
    public class ImportanceViewModel
    {
        public Guid ImportanceId { get; set; }
        public string ImportanceName { get; set; }
        public string ImportanceDescription { get; set; }
    }
}
