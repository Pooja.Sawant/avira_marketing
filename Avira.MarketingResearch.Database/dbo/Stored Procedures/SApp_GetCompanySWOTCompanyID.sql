﻿
 
-- =============================================            
-- Author:  Harshal            
-- Create date: 06/05/2019            
-- Description: Get CompanySWOTMap by CompanyId             
-- =============================================            
CREATE PROCEDURE [dbo].[SApp_GetCompanySWOTCompanyID]                 
 @ComapanyID uniqueidentifier,
 @ProjectID uniqueidentifier,             
 @includeDeleted bit = 0                 
AS            
BEGIN             
 SET NOCOUNT ON;              
 SELECT[CompanySWOTMap].[Id]            
      ,[CompanySWOTMap].[CompanyId] 
	  ,[CompanySWOTMap].[ProjectId]              
      ,[CompanySWOTMap].[CategoryId]   
      ,[CompanySWOTMap].[SWOTDescription]
	  ,[CompanySWOTMap].[SWOTTypeId]
	  ,[SWOTType].SWOTTypeName            
      ,[CompanySWOTMap].[CreatedOn]            
      ,[CompanySWOTMap].[UserCreatedById]            
      ,[CompanySWOTMap].[ModifiedOn]            
      ,[CompanySWOTMap].[UserModifiedById]            
      ,[CompanySWOTMap].[IsDeleted]            
      ,[CompanySWOTMap].[DeletedOn]            
      ,[CompanySWOTMap].[UserDeletedById]            
  FROM [dbo].[CompanySWOTMap]        
  LEFT JOIN [dbo].[SWOTType] SWOTType ON [CompanySWOTMap].SWOTTypeId = SWOTType.Id          
  WHERE ([CompanySWOTMap].CompanyId = @ComapanyID and [CompanySWOTMap].ProjectId = @ProjectID )                  
 AND (@includeDeleted = 0 or [CompanySWOTMap].IsDeleted = 0)              
END