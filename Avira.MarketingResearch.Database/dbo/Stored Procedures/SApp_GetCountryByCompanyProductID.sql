﻿

      
 CREATE PROCEDURE [dbo].[SApp_GetCountryByCompanyProductID]            
 (@ComapanyProductID uniqueidentifier = null, 
  @includeDeleted bit = 0     
 )            
 AS            
/*================================================================================            
Procedure Name: SApp_GetCountryByCompanyProductID     
Author: Harshal            
Create date: 05/02/2019            
Description: Get list from country table by CompanyProductID            
Change History            
Date  Developer  Reason            
__________ ____________ ______________________________________________________            
05/02/2019 Harshal   Initial Version 
================================================================================*/           
            
BEGIN            
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED         
begin        
        
 SELECT         
   [Country].[Id] AS [CountryId],            
   [Country].[CountryName],           
   [Country].IsDeleted,        
   CompanyProductCountryMap.Id as CompanyProductCountryMapId      
   --cast (case when exists(select 1 from dbo.CompanyProductCountryMap         
   --  where CompanyProductCountryMap.CompanyProductId = @ComapanyProductID        
   --  and CompanyProductCountryMap.IsDeleted = 0        
   --  and CompanyProductCountryMap.CountryId = Country.Id) then 1 else 0 end as bit) as IsMapped        
       
FROM [dbo].[Country]      
LEFT JOIN dbo.CompanyProductCountryMap       
ON Country.Id = CompanyProductCountryMap.CountryId
and CompanyProductCountryMap.CompanyProductId = @ComapanyProductID 
and CompanyProductCountryMap.IsDeleted = 0
where  (@includeDeleted = 1 or Country.IsDeleted = 0)        
      
  END        
       
END