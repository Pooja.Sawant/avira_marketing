﻿CREATE TABLE [dbo].[DeviceAndSolution] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [Name]             NVARCHAR (256)   CONSTRAINT [DF_DeviceAndSolution_Name] DEFAULT ('') NOT NULL,
    [IsProduct]        BIT              CONSTRAINT [DF_DeviceAndSolution_isProduct] DEFAULT ('') NOT NULL,
    [IsSoftware]       BIT              CONSTRAINT [DF_DeviceAndSolution_isSoftware] DEFAULT ('') NOT NULL,
    [Description]      NVARCHAR (256)   NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_DeviceAndSolution_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_DeviceAndSolution] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_DeviceAndSolution_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_DeviceAndSolution_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id])
);

