﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.LogService;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Avira.MarketingResearch.Web.Controllers
{
    [CustomExceptionFilter()]
    [Authorization]
    public class BaseController : Controller
    {
        //this the base controller of all controllers
        //public override void OnActionExecuting(ActionExecutingContext context)
        //{
        //    //check Session here
        //    if (context.HttpContext.Session.Id==string.Empty)
        //    {
        //        context.Result = new RedirectResult("~/Account/Login"); //Session expired so redirecting to login page.
        //    }
        //}
    }
}