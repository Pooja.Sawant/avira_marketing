﻿


CREATE procedure [dbo].[SApp_InsertUpdateCompanyProductCountryMap]      
(
@CompanyProductId uniqueidentifier,   
@CompanyProductCountryMap dbo.[udtGUIDGUIDMapValue] readonly,
@AviraUserId uniqueidentifier
)      
as      
/*================================================================================      
Procedure Name: SApp_InsertUpdateCompanyProductCountryMap      
Author: Harshal      
Create date: 04/22/2019      
Description: Insert a record into CompanyProductCountryMap table      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
04/22/2019 Harshal   Initial Version    
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)  
    
BEGIN TRY         
--mark records as delete which are not in list
update [dbo].[CompanyProductCountryMap]
set IsDeleted = 1,
UserDeletedById = @AviraUserId,
DeletedOn = GETDATE()
from [dbo].[CompanyProductCountryMap]
where [CompanyProductCountryMap].CompanyProductId = @CompanyProductId
and not exists (select 1 from @CompanyProductCountryMap map where map.id = [CompanyProductCountryMap].CountryId)

 --Update records for change
update [dbo].[CompanyProductCountryMap]
set CountryId = map.mapid,
ModifiedOn = GETDATE(),
UserModifiedById = @AviraUserId
from [dbo].[CompanyProductCountryMap]
inner join @CompanyProductCountryMap map 
on map.id = [CompanyProductCountryMap].CountryId
where [CompanyProductCountryMap].CompanyProductId = @CompanyProductId;

--Insert new records
insert into [dbo].[CompanyProductCountryMap]([Id],      
        [CompanyProductId],      
        [CountryId], 
        [IsDeleted],      
        [UserCreatedById],      
        [CreatedOn])      
 select NEWID(),
   @CompanyProductId,       
   map.ID as [CountryId],
   0,      
   @AviraUserId,       
   GETDATE()
 from @CompanyProductCountryMap map
 where not exists (select 1 from [dbo].[CompanyProductCountryMap] map1
					where map1.CompanyProductId = @CompanyProductId
					and map.ID = map1.CountryId);       
     
SELECT StoredProcedureName ='SApp_InsertUpdateCompanyProductCountryMap',Message =@ErrorMsg,Success = 1;       
    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar;      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar;      
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());      
    
 set @ErrorMsg = 'Error while insert a new CompanyProductCountryMap, please contact Admin for more details.';    
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
       
END CATCH      
      
      
end