﻿CREATE TABLE [dbo].[DecisionBusinessRole] (
    [DecisionId]     UNIQUEIDENTIFIER NOT NULL,
    [BusinessRoleId] UNIQUEIDENTIFIER NULL,
    FOREIGN KEY ([BusinessRoleId]) REFERENCES [dbo].[LookupCategory] ([Id]),
    CONSTRAINT [FK__DecisionB__Decis__74A5C9C9] FOREIGN KEY ([DecisionId]) REFERENCES [dbo].[Decision] ([Id]),
    CONSTRAINT [Decision_BusinessRole] UNIQUE NONCLUSTERED ([DecisionId] ASC, [BusinessRoleId] ASC)
);

