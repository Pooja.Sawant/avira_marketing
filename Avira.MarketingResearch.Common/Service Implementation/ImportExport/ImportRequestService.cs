﻿using Avira.MarketingResearch.Common.IService.ImportExport;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport;
using Avira.MarketingResearch.Repository.Helpers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Avira.MarketingResearch.Utility;

namespace Avira.MarketingResearch.Common.Service_Implementation.ImportExport
{
    public class ImportRequestService : IImportRequestService
    {
        private readonly IImportRequestRepository _importRequestRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;




        public ImportRequestService(IImportRequestRepository importRequestRepository)
        {
            _importRequestRepository = importRequestRepository;            
        }

        public ImportRequestService(IImportRequestRepository importRequestRepository,IConfiguration configuration, IHostingEnvironment hostingEnvironment)
        {
            _importRequestRepository = importRequestRepository;
            _hostingEnvironment = hostingEnvironment;
            _configuration = configuration;

            

        }

        public Result<SpTransactionMessage> Create(ImportRequestViewModel tfc)
        {
            Result<SpTransactionMessage> ImportRequestRep = null;
            var path = _configuration.GetSection("AppUploadPath").GetSection("Import").GetSection("MSImport").Value;
            var fullPath = Path.Combine(_hostingEnvironment.ContentRootPath, path);
            var newFileName = FileUpload.ByteArrayToExceFile(tfc.fileByte, fullPath, ".xlsx");
            if (!string.IsNullOrEmpty(newFileName))
            {
                tfc.ImportFileName = newFileName;
                tfc.ImportFilePath = path;
                ImportRequestRep= _importRequestRepository.Create(tfc);
            }
            return ImportRequestRep;
        }

        public IEnumerable<ImportRequestViewModel> GetAll(ImportRequestViewModel viewModel)
        {
            return _importRequestRepository.GetAll(viewModel);
        }

        public Result<SpTransactionMessage> Update(ImportRequestViewModel tfc)
        {
            return _importRequestRepository.Update(tfc);
        }

        public IEnumerable<ImportRequestViewModel> GetImportRequest(string templateName, string statusName)
        {
            var importRequests = _importRequestRepository.GetImportRequest(templateName, statusName);
            return importRequests;
        }

        public ImportRequestViewModel GetRequest(Guid Id)
        {
            var importRequest = _importRequestRepository.GetRequest(Id);

            if (importRequest !=null)
            { 
                if (!string.IsNullOrEmpty(importRequest.ImportExceptionFilePath))
                {
                    importRequest.fileByte = FileUpload.FileTobyteArray(importRequest.ImportExceptionFilePath);
                }
                else
                {
                    importRequest.fileByte = null;
                }
            }
            return importRequest;
        }

        public ImportRequestViewModel GetImportTemplate(string importType)
        {
            ImportRequestViewModel importRequest = new ImportRequestViewModel();
            if (!string.IsNullOrEmpty(importType))
            {
              string excelTemplatePath = _configuration.GetSection("AppTemplatePath").GetSection("ExcelTemplate").Value;

                switch (importType)
                {
                    case "Trend":
                        {
                            importRequest.ImportFileName = _configuration.GetSection("AppTemplateName").GetSection("TrendTemplate").Value;
                            var fullPath = Path.Combine(_hostingEnvironment.ContentRootPath, excelTemplatePath, importRequest.ImportFileName);
                            importRequest.fileByte = FileUpload.FileTobyteArray(fullPath);
                            break;
                        }

                    case "Company Basic Info": // statement sequence
                        {
                            importRequest.ImportFileName = _configuration.GetSection("AppTemplateName").GetSection("CompanyBasicInfoTemplate").Value;
                            var fullPath = Path.Combine(_hostingEnvironment.ContentRootPath, excelTemplatePath, importRequest.ImportFileName);
                            importRequest.fileByte = FileUpload.FileTobyteArray(fullPath);

                            break;
                        }
                    case "CP_Fundamentals": // statement sequence
                        {
                            importRequest.ImportFileName = _configuration.GetSection("AppTemplateName").GetSection("CP_Fundamentals").Value;
                            var fullPath = Path.Combine(_hostingEnvironment.ContentRootPath, excelTemplatePath, importRequest.ImportFileName);
                            importRequest.fileByte = FileUpload.FileTobyteArray(fullPath);
                            break;
                        }
                    case "Company Profile": // statement sequence
                        {
                            importRequest.ImportFileName = _configuration.GetSection("AppTemplateName").GetSection("CompanyProfileTemplate").Value;
                            var fullPath = Path.Combine(_hostingEnvironment.ContentRootPath, excelTemplatePath, importRequest.ImportFileName);
                            importRequest.fileByte = FileUpload.FileTobyteArray(fullPath);
                            break;
                        }
                    case "CompanyBulkImport": // statement sequence
                        {
                            importRequest.ImportFileName = _configuration.GetSection("AppTemplateName").GetSection("CompanyBulkImportTemplate").Value;
                            var fullPath = Path.Combine(_hostingEnvironment.ContentRootPath, excelTemplatePath, importRequest.ImportFileName);
                            importRequest.fileByte = FileUpload.FileTobyteArray(fullPath);
                            break;
                        }
                    case "MarketSizing Import Template": // statement sequence
                        {
                            importRequest.ImportFileName = _configuration.GetSection("AppTemplateName").GetSection("MarketSizingTemplate").Value;
                            var fullPath = Path.Combine(_hostingEnvironment.ContentRootPath, excelTemplatePath, importRequest.ImportFileName);
                            importRequest.fileByte = FileUpload.FileTobyteArray(fullPath);
                            break;
                        }
                    case "CP Financial": // statement sequence
                        {
                            importRequest.ImportFileName = _configuration.GetSection("AppTemplateName").GetSection("CPFinancialTemplate").Value;
                            var fullPath = Path.Combine(_hostingEnvironment.ContentRootPath, excelTemplatePath, importRequest.ImportFileName);
                            importRequest.fileByte = FileUpload.FileTobyteArray(fullPath);
                            break;
                        }
                    case "QualitativeAnalysis": // statement sequence
                        {
                            importRequest.ImportFileName = _configuration.GetSection("AppTemplateName").GetSection("QualitativeAnalysis").Value; ;
                            var fullPath = Path.Combine(_hostingEnvironment.ContentRootPath, excelTemplatePath, importRequest.ImportFileName);
                            importRequest.fileByte = FileUpload.FileTobyteArray(fullPath);
                            break;
                        }
                    case "Report Generation": 
                        {
                            importRequest.ImportFileName = _configuration.GetSection("AppTemplateName").GetSection("ReportGeneration").Value; ;
                            var fullPath = Path.Combine(_hostingEnvironment.ContentRootPath, excelTemplatePath, importRequest.ImportFileName);
                            importRequest.fileByte = FileUpload.FileTobyteArray(fullPath);
                            break;
                        }
                    case "Strategy Instance":
                        {
                            importRequest.ImportFileName = _configuration.GetSection("AppTemplateName").GetSection("StrategyInstance").Value; ;
                            var fullPath = Path.Combine(_hostingEnvironment.ContentRootPath, excelTemplatePath, importRequest.ImportFileName);
                            importRequest.fileByte = FileUpload.FileTobyteArray(fullPath);
                            break;
                        }
                    default:
                        {
                            importRequest.ImportFileName = "NoTemplate.xlsx";
                            var fullPath = Path.Combine(_hostingEnvironment.ContentRootPath, excelTemplatePath, importRequest.ImportFileName);
                            //importRequest.fileByte = FileUpload.FileTobyteArray(fullPath);

                        }  // default statement sequence
                        break;
                }

            }
            else
            {

            }
            return importRequest;
        }
    }
}
