﻿CREATE procedure [dbo].[SFab_GetAllProjectByUserId]
(@UserId uniqueidentifier=null,
@includeDeleted bit = 0)
as
/*================================================================================
Procedure Name: SFab_GetAllProjectByUserId 
Author: Pooja
Create date: 05/24/2019
Description: Get list from project table by Avirauser id
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
05/24/2019	Pooja			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ProjectApprovedStatusId uniqueidentifier
select @ProjectApprovedStatusId = ID from ProjectStatus
where ProjectStatusName = 'Completed';

if @includeDeleted is null set @includeDeleted = 0

--comment by pooja as discuss with sharath on 04-07-19
--SELECT  distinct      UserSegmentMap.SegmentType, Project.ProjectName, Project.ID, AviraUser.Id as Userid
--FROM            UserSegmentMap INNER JOIN
--                         AviraUser ON UserSegmentMap.CustomerUserId = AviraUser.Id INNER JOIN
--                         Project ON UserSegmentMap.SegmentId = Project.ID

--WHERE        (UserSegmentMap.SegmentType = 'Project') 
--and UserSegmentMap.IsDeleted=0 and AviraUser.IsDeleted=0
--and AviraUser.Id=@UserId and Project.IsDeleted=0
--order by Project.ProjectName asc
--comment end

--select id,ProjectName from project where IsDeleted=0 order by Project.ProjectName asc

--declare @CustomerId    uniqueidentifier = '8B411CFC-DBC2-4EDA-A34B-25931E9FDE9A';

 

--select top 1 SegmentId as ProjectId from SubscriptionSegmentMap SSM
--where SegmentType = 'Project'
--and exists (select 1 from CustomerSubscriptionMap CSP
--            where CSP.CustomerId = @CustomerId
--            and ssm.SubscriptionId = csp.SubscriptionId)
--order by CreatedOn

declare @CustomerId    uniqueidentifier ;
declare @usertype    int ;
	SELECT @CustomerId = CustomerId FROM AviraUser
	WHERE Id = @UserId 
SELECT @usertype = UserTypeId FROM AviraUser
	WHERE Id = @UserId 
 if(@usertype = 2)
 begin
SELECT         Project.ID, Project.ProjectName
FROM            SubscriptionSegmentMap AS SSM 
INNER JOIN Project ON SSM.SegmentId = Project.ID and ssm.IsDeleted=0
WHERE 
project.ProjectStatusID = @ProjectApprovedStatusId
AND 
(SSM.SegmentType = 'Project' and Project.IsDeleted=0) 
AND EXISTS (SELECT        1 AS Expr1
            FROM            CustomerSubscriptionMap AS CSP
            WHERE        (CustomerId = @CustomerId) 
			AND (SSM.SubscriptionId = SubscriptionId))

ORDER BY SSM.CreatedOn
end
else
begin
	select * from Project 
	where project.ProjectStatusID = @ProjectApprovedStatusId
	AND IsDeleted=0
end
end