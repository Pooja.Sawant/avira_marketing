﻿CREATE TABLE [dbo].[AviraUserCustomerSubscriptionMap] (
    [Id]                        UNIQUEIDENTIFIER NOT NULL,
    [AviraUserId]               UNIQUEIDENTIFIER NOT NULL,
    [CustomerId]                UNIQUEIDENTIFIER NOT NULL,
    [SubscriptionId]            UNIQUEIDENTIFIER NOT NULL,
    [CustomerSubscriptionMapId] UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]                 DATETIME2 (7)    NULL,
    [UserCreatedById]           UNIQUEIDENTIFIER NULL,
    [ModifiedOn]                DATETIME2 (7)    NULL,
    [UserModifiedById]          UNIQUEIDENTIFIER NULL,
    [IsDeleted]                 BIT              CONSTRAINT [DF_AviraUserCustomerSubscriptionMap_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]                 DATETIME2 (7)    NULL,
    [UserDeletedById]           UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_AviraUserCustomerSubscriptionMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_AviraUserCustomerSubscriptionMap_CreatedUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_AviraUserCustomerSubscriptionMap_CustomerId] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([Id]),
    CONSTRAINT [FK_AviraUserCustomerSubscriptionMap_CustomerSubscriptionMapId] FOREIGN KEY ([CustomerSubscriptionMapId]) REFERENCES [dbo].[CustomerSubscriptionMap] ([Id]),
    CONSTRAINT [FK_AviraUserCustomerSubscriptionMap_DeletedUser] FOREIGN KEY ([UserDeletedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_AviraUserCustomerSubscriptionMap_ModifiedUser] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_AviraUserCustomerSubscriptionMap_SubscriptionId] FOREIGN KEY ([SubscriptionId]) REFERENCES [dbo].[Subscription] ([Id])
);



