﻿CREATE TABLE [dbo].[CompanySWOTMap] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [CompanyId]        UNIQUEIDENTIFIER NOT NULL,
    [ProjectId]        UNIQUEIDENTIFIER NULL,
    [CategoryId]       UNIQUEIDENTIFIER NOT NULL,
    [SWOTTypeId]       UNIQUEIDENTIFIER NOT NULL,
    [SWOTDescription]  NVARCHAR (1024)  NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CompanySWOTMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CompanySWOTMap_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([Id]),
    CONSTRAINT [FK_CompanySWOTMap_SWOTType] FOREIGN KEY ([SWOTTypeId]) REFERENCES [dbo].[SWOTType] ([Id])
);

