﻿CREATE TABLE [dbo].[CompanyProfileStatus] (
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [CompanyStatusName] NVARCHAR (256)   NOT NULL,
    [Description]       NVARCHAR (256)   NULL,
    [CreatedOn]         DATETIME2 (7)    NULL,
    [UserCreatedById]   UNIQUEIDENTIFIER NULL,
    [ModifiedOn]        DATETIME2 (7)    NULL,
    [UserModifiedById]  UNIQUEIDENTIFIER NULL,
    [IsDeleted]         BIT              CONSTRAINT [DF_ComanyProfileStatus_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]         DATETIME2 (7)    NULL,
    [UserDeletedById]   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_ComanyProfileStatus] PRIMARY KEY CLUSTERED ([Id] ASC)
);

