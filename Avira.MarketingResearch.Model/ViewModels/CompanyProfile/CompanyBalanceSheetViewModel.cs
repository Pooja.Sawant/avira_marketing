﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.CompanyProfile
{
    public class CompanyBalanceSheetViewModel
    {
        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
        public Guid CurrencyId { get; set; }
        public Guid ValueConversionId { get; set; }
        public string CompanyName { get; set; }
        public string Currency { get; set; }
        public string Units { get; set; }
        public int Year { get; set; }
        public decimal Assets_ { get; set; }
        public decimal CashAndCashEquivalents { get; set; }
        public decimal ShortTermInvestments { get; set; }
        public decimal AccountsReceivables { get; set; }
        public decimal Inventories { get; set; }
        public decimal CurrentTaxAssets { get; set; }
        public decimal OtherCurrentAssets { get; set; }
        public decimal AssetsHeldForSale { get; set; }
        public decimal TotalCurrentAssets { get; set; }
        public decimal LongTermInvestments { get; set; }
        public decimal PropertyPlantAndEquipment { get; set; }
        public decimal IntangibleAssets { get; set; }
        public decimal Goodwill { get; set; }
        public decimal DeferredTaxAssets { get; set; }
        public decimal OtherNoncurrentAssets { get; set; }
        public decimal TotalAssets { get; set; }
        public decimal LiabilitiesAndEquity_ { get; set; }
        public decimal ShortTermDebt { get; set; }
        public decimal TradeAccountsPayable { get; set; }
        public decimal DividendsPayable { get; set; }
        public decimal IncomeTaxesPayable { get; set; }
        public decimal AccruedExpenses { get; set; }
        public decimal DeferredRevenuesShortTerm { get; set; }
        public decimal OtherCurrentLiabilities { get; set; }
        public decimal LiabilitiesHeldForSale { get; set; }
        public decimal TotalCurrentLiabilities { get; set; }
        public decimal LongTermDebt { get; set; }
        public decimal PensionBenefitObligations { get; set; }
        public decimal PostretirementBenefitObligations { get; set; }
        public decimal DeferredTaxLiabilities { get; set; }
        public decimal OtherTaxesPayable { get; set; }
        public decimal DeferredRevenuesLongTerm { get; set; }
        public decimal OtherNoncurrentLliabilities { get; set; }
        public decimal TotalLiabilities { get; set; }
        public decimal CommitmentsAndContingencies_ { get; set; }
        public decimal PreferredStock { get; set; }
        public decimal CommonStock { get; set; }
        public decimal Debentures { get; set; }
        public decimal AdditionalPaidInCapital { get; set; }
        public decimal TresuryStock { get; set; }
        public decimal RetainedEarnings { get; set; }
        public decimal AccumulatedOtherComprehensiveLoss { get; set; }
        public decimal TotalShareholdersEquity { get; set; }
        public decimal MinorityInterest { get; set; }
        public decimal TotalEquity { get; set; }
        public decimal TotalLiabilitiesAndEquity { get; set; }
        public DateTime? CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid UserModifiedById { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid UserDeletedById { get; set; }
    }

    public class MainCompanyBalanceSheetViewModel
    {
        public Guid ProjectId { get; set; }
        public Guid CompanyId { get; set; }
        public List<CompanyBalanceSheetViewModel> CompanyBalanceSheetViewModelList { get; set; }
        public CompanyNoteViewModel CompanyNoteViewModel { get; set; }
        public List<int> YearList { get; set; }
    }
}
