﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.CompanyProfile
{
    public class CompanyCashFlowStatementViewModel
    {
        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
        public Guid CurrencyId { get; set; }
        public Guid ValueConversionId { get; set; }
        public string CompanyName { get; set; }
        public string Currency { get; set; }
        public string Units { get; set; }
        public int Year { get; set; }
        public decimal OperatingActivities_ { get; set; }
        public decimal AdjustmentsToOperatingActivities_ { get; set; }
        public decimal DepreciationAndAmortization { get; set; }
        public decimal Impairments { get; set; }
        public decimal LossOnSaleOfAssets { get; set; }
        public decimal OtherLossesOrGains { get; set; }
        public decimal DeferredTaxes { get; set; }
        public decimal ShareBasedCompensationExpense { get; set; }
        public decimal BenefitPlanContributions { get; set; }
        public decimal OtherAdjustmentsNet { get; set; }
        public decimal OtherChangesInAssetsAndLiabilitiesNetOfAcquisitionsAndDistitures_ { get; set; }
        public decimal AccountsReceivable { get; set; }
        public decimal OtherAssets { get; set; }
        public decimal AccountsPayable { get; set; }
        public decimal OtherLiabilities { get; set; }
        public decimal OtherTaxAccountsNet { get; set; }
        public decimal NetCashProvidedByOrUsedInOperatingActivities { get; set; }
        public decimal InvestingActivities_ { get; set; }
        public decimal PurchasesOfPropertyPlantAndEquipment { get; set; }
        public decimal PurchasesOfShortTermInvestments { get; set; }
        public decimal SaleOfShortTermInvestments { get; set; }
        public decimal RedemptionsOfShortTermInvestments { get; set; }
        public decimal PurchasesOfLongTermInvestments { get; set; }
        public decimal SaleOfLongTermInvestments { get; set; }
        public decimal BusinessAcquisitions { get; set; }
        public decimal AcquisitionsOfIntangibleAssets { get; set; }
        public decimal OtherInvestingActivitiesNet { get; set; }
        public decimal NetCashProvidedByOrUsedInInvestingActivities { get; set; }
        public decimal FinancingActivities_ { get; set; }
        public decimal ProceedsFromShortTermDebt { get; set; }
        public decimal PrincipalPaymentsOnShortTermDebt { get; set; }
        public decimal NetPaymentsonOrProceedsFromShortTermBorrowingsWithOrigilMaturitiesOfThreeMonthsOrLess { get; set; }
        public decimal IssuanceOfLongTermDebt { get; set; }
        public decimal PrincipalPaymentsOnLongTermDebt { get; set; }
        public decimal PurchasesOfCommonStock { get; set; }
        public decimal CommonStockRepurchased { get; set; }
        public decimal CashDividendsPaid { get; set; }
        public decimal ProceedsFromExerciseOfStockOptions { get; set; }
        public decimal OtherFinancingActivitiesNet { get; set; }
        public decimal CapitalExpenditure { get; set; }
        public decimal NetCashProvidedByOrUsedInFinancingActivities { get; set; }
        public decimal ForeignExchangeImpact { get; set; }
        public decimal DecreaseInCashAndCashEquivalents { get; set; }
        public decimal OpeningBalanceOfCashAndCashEquivalents { get; set; }
        public decimal ClosingBalanceOfCashAndCashEquivalents { get; set; }
        public decimal CashPaidReceivedDuringThePeriodFor_ { get; set; }
        public decimal IncomeTaxes { get; set; }
        public decimal InterestRateHedges { get; set; }
        public DateTime? CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid UserModifiedById { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid UserDeletedById { get; set; }
    }

    public class MainCompanyCashFlowStatementViewModel
    {
        public Guid ProjectId { get; set; }
        public Guid CompanyId { get; set; }
        public List<CompanyCashFlowStatementViewModel> CompanyCashFlowStatementViewModelList { get; set; }
        public CompanyNoteViewModel CompanyNoteViewModel { get; set; }
        public List<int> YearList { get; set; }
    }
}
