﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.CompanyProfile
{
    public class CompanyAnalystViewRepository: ICompanyAnalystViewRepository
    {
        protected IDbHelper _dbHelper;

        public CompanyAnalystViewRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }
        public CompanyAnalystViewModel GetById(Guid companyId, Guid projectId)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<CompanyAnalystViewModel>(CompanyAnalystViewQueries.StoreProcSelectById
                            , new { @CompanyID = companyId, @ProjectID = projectId, @includeDeleted = false }
                            , null, true, null, CommandType.StoredProcedure).FirstOrDefault();
        }

        public Result<SpTransactionMessage> InsertUpdate(MainCompanyAnalystViewModel CompanyAnalystViewMoel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(CompanyAnalystViewQueries.StoreProcInsert,
               new
               {
                   @CompanyId = CompanyAnalystViewMoel.CompanyId,
                   @ProjectId = CompanyAnalystViewMoel.ProjectId,
                   @ID = CompanyAnalystViewMoel.CompanyAnalystViewModel.Id,
                   @CompanyAnalystViewName = CompanyAnalystViewMoel.CompanyAnalystViewModel.AnalystView,
                   @IsDeleted= false,
                   @AviraUserId = CompanyAnalystViewMoel.UserCreatedById
               }
                    , CommandType.StoredProcedure);
            return spTransactionMessage;
        }

    }
}
