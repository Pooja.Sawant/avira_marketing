﻿

 CREATE PROCEDURE [dbo].[SFab_GetCompanyListbyProjectId]        
 (@ProjectID uniqueidentifier = null, 
  @includeDeleted bit = 0,
  @AviraUserID uniqueidentifier = null   
 )        
 AS        
/*================================================================================        
Procedure Name: [SFab_GetCompanyListbyProjectId]  
Author: Harshal        
Create date: 07/25/2019        
Description: Get list from CompanyProjectMap table by ProjectID       
Change History        
Date  Developer  Reason        
__________ ____________ ______________________________________________________        
07/25/2019 Harshal   Initial Version        
================================================================================*/       
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ProjectApprovedStatusId uniqueidentifier
select @ProjectApprovedStatusId = ID from ProjectStatus
where ProjectStatusName = 'Completed';
declare @UnitsId uniqueidentifier	
select @UnitsId = Id from ValueConversion
where ValueName = 'Millions';
DECLARE @BaseYear INT = 0;
select @BaseYear = BaseYear from BaseYearMaster
--if @includeDeleted is null set @includeDeleted = 0

SELECT  [CompanyProjectMap].CompanyId,
        [CompanyName],
		[Description],
		[RankNumber],
        CompanyLocation,
	    Nature.NatureTypeName,
		(SELECT  [dbo].[fun_UnitDeConversion](@UnitsId,Revenue)
        From CompanyPLStatement 
		where  CompanyPLStatement.CompanyId=CompanyProjectMap.CompanyId AND CompanyPLStatement.Year=@BaseYear
        ) TotalRevenue,
		--CompanyRevenue.TotalRevenue,
		[CompanyCode],
		[Project].Id As ProjectId,
		[Project].ProjectName,
		(SELECT STUFF( (SELECT ',' + RTRIM(regionname)
                              FROM region INNER JOIN 
	CompanyRegionMap ON  Region.Id=CompanyRegionMap.RegionId
	AND CompanyRegionMap.CompanyId=CompanyProjectMap.CompanyId

               FOR XML PATH('')
          ),1,1,'')) As GeographicPresence
	
FROM [dbo].[CompanyProjectMap]
INNER JOIN [dbo].[Company] ON [Company].Id=[CompanyProjectMap].CompanyId
INNER JOIN [dbo].[Project] ON [Project].ID=[CompanyProjectMap].ProjectId
--left JOIN  CompanyRevenue ON CompanyRevenue.CompanyId=Company.Id
left  join Nature ON Nature.Id=Company.NatureId
--INNER JOIN CompanyRegionMap ON CompanyRegionMap.CompanyId=CompanyProjectMap.CompanyId
--INNER JOIN Region ON Region.Id=CompanyRegionMap.RegionId
WHERE ([CompanyProjectMap].ProjectId=@ProjectID)
AND (@includeDeleted = 1 or [Company].IsDeleted = 0)
AND [Project].ProjectStatusID = @ProjectApprovedStatusId;
--Order by [Company].CompanyName
end