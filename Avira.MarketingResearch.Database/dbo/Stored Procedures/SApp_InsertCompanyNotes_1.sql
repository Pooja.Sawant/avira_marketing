﻿/*================================================================================
-- Author:  Laxmikant  
-- Create date: 04/15/2019  
-- Description: Insert /  Update Company Notes 
Date  Developer  Reason          
__________ ____________ ______________________________________________________          
04/15/2019  Laxmikant   Initial Version  
-------------------------------------------------------------------------------
04/26/2019  Harshal     Added ModificationON and UserModifiedById in Update Part   
-------------------------------------------------------------------------------
06/04/2019  Praveen     Manage Project ID to for company-project notes   
================================================================================*/      
  

CREATE PROCEDURE [dbo].[SApp_InsertCompanyNotes]   
 -- Add the parameters for the stored procedure here  
 @Id uniqueidentifier,  
 @CompanyId uniqueidentifier,
 @ProjectId uniqueidentifier=null,
 @CompanyType NCHAR(512),  
 @AuthorRemark NVARCHAR(512) = null,  
 @ApproverRemark NVARCHAR(512) = null,  
 @CompanyStatusName VARCHAR(512),    
 @AviraUserId uniqueidentifier   
AS   
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
 Declare @CompanyNoteStatusID uniqueidentifier  
 --Declare @ExistingAuthorRemark NVARCHAR(512)  
 --Declare @ExistingApproverRemark NVARCHAR(512)  
 declare @ErrorMsg NVARCHAR(2048) 
BEGIN TRY       
 SELECT @CompanyNoteStatusID = (SELECT  Id FROM CompanyProfileStatus where CompanyStatusName = @CompanyStatusName  )
 --SELECT @ExistingApproverRemark = ApproverRemark FROM Company Where id =  @CompanyId  
 --SELECT @ExistingAuthorRemark = AuthorRemark FROM Company Where id =  @CompanyId   
  

  IF(@ProjectId IS NULL)
  BEGIN

 if exists (select CompanyId from [CompanyNote] where (CompanyId = @CompanyId and CompanyType = @CompanyType) 
 --and (@ProjectId is null or ProjectId=@ProjectId)
 ) 
  begin  
   update [dbo].[CompanyNote]  
   set AuthorRemark =  @AuthorRemark, ApproverRemark = @ApproverRemark, CompanyStatusID = @CompanyNoteStatusID,ModifiedOn=GETDATE(),UserModifiedById=@AviraUserId WHERE CompanyId = @CompanyId and CompanyType = @CompanyType   
  end  
 else  
  begin  
   if NOT exists (select CompanyId from [CompanyNote] where CompanyId = @CompanyId and CompanyType = @CompanyType)  
    begin  
     INSERT INTO [dbo].[CompanyNote]  
     (  
       [Id], [CompanyId],[ProjectId], [CompanyType], [AuthorRemark], [ApproverRemark], [CompanyStatusID], [CreatedOn],[UserCreatedById], IsDeleted  
     )      
     VALUES (@Id, @CompanyId,@ProjectId, @CompanyType, @AuthorRemark, @ApproverRemark, @CompanyNoteStatusID, getdate(), @AviraUserId, 0);   
    end  
  end  

  END
  ELSE	
  BEGIN


  if exists (select CompanyId from [CompanyNote] where (CompanyId = @CompanyId AND ProjectId = @ProjectId and CompanyType = @CompanyType) 
 --and (@ProjectId is null or ProjectId=@ProjectId)
 ) 
  begin  
   update [dbo].[CompanyNote]  
   set AuthorRemark =  @AuthorRemark, ApproverRemark = @ApproverRemark, CompanyStatusID = @CompanyNoteStatusID,ModifiedOn=GETDATE(),UserModifiedById=@AviraUserId 
   WHERE CompanyId = @CompanyId AND ProjectId = @ProjectId and CompanyType = @CompanyType   
  end  
 else  
  begin  
   if NOT exists (select CompanyId from [CompanyNote] where CompanyId = @CompanyId AND ProjectId = @ProjectId and CompanyType = @CompanyType)  
    begin  
     INSERT INTO [dbo].[CompanyNote]  
     (  
       [Id], [CompanyId],[ProjectId], [CompanyType], [AuthorRemark], [ApproverRemark], [CompanyStatusID], [CreatedOn],[UserCreatedById], IsDeleted  
     )      
     VALUES (@Id, @CompanyId,@ProjectId, @CompanyType, @AuthorRemark, @ApproverRemark, @CompanyNoteStatusID, getdate(), @AviraUserId, 0);   
    end  
  end  

  END
  SELECT StoredProcedureName ='SApp_InsertCompanyNotes',Message =@ErrorMsg,Success = 1;    
END TRY   
BEGIN CATCH        
    -- Execute the error retrieval routine.        
 DECLARE @ErrorNumber int;        
 DECLARE @ErrorSeverity int;        
 DECLARE @ErrorProcedure varchar(50);        
 DECLARE @ErrorLine int;        
 DECLARE @ErrorMessage varchar(500);        
        
  SELECT @ErrorNumber = ERROR_NUMBER(),        
        @ErrorSeverity = ERROR_SEVERITY(),        
        @ErrorProcedure = ERROR_PROCEDURE(),        
        @ErrorLine = ERROR_LINE(),        
        @ErrorMessage = ERROR_MESSAGE()        
        
 insert into dbo.Errorlog(ErrorNumber,        
       ErrorSeverity,        
       ErrorProcedure,        
       ErrorLine,        
       ErrorMessage,        
       ErrorDate)        
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());        
      
 set @ErrorMsg = 'Error while insert a new CompanyMarketMap, please contact Admin for more details.';      
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine      
    ,Message =@ErrorMsg,Success = 0;         
         
END CATCH