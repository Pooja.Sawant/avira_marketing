﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SFab_InsertDownloadsExportData_del] 
	-- Add the parameters for the stored procedure here
   (@ProjectId uniqueidentifier,
	@ModuleTypeId uniqueidentifier,
	@USerId uniqueidentifier
	)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

INSERT INTO [dbo].[DownloadsExportData]
             ([Id]
           ,[ProjectId]
           ,[ModuleTypeId]
           ,[Header]
           ,[Footer]
           ,[ReportTitle]
           ,[CoverPageImageLink]
           ,[CoverPageImageName]
           ,[Overview]
           ,[KeyPoints]
           ,[Methodology]
           ,[MethodologyImageLink]
           ,[ContactUsAnalystName]
           ,[ContactUsOfficeContactNo]
           ,[ContactUsAnalystEmailId]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted]
           ,[DeletedOn]
           ,[UserDeletedById])
  select newid(),
          @ProjectId,
		  @ModuleTypeId,
		  Header,
		  Footer,
		  ReportTitle,
		  CoverPageImageLink,
          CoverPageImageName,
		  Overview,
		  KeyPoints,
		  Methodology,
		  MethodologyImageLink,
          ContactUsAnalystName,
          ContactUsOfficeContactNo,
          ContactUsAnalystEmailId,
		  GetDate(),
		  @UserId,
		  0,
		  GetDate(),
		  @UserId
		 	   From DownloadsExportData Inner Join ModuleType
			 ON moduletype.id = @ModuleTypeId 
			 Where ProjectId = @ProjectId 
END