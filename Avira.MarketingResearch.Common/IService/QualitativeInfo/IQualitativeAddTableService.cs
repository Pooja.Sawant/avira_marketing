﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.QualitativeInfo;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.QualitativeInfo
{
    public interface IQualitativeAddTableService
    {        
        IEnumerable<QualitativeAddTableViewModel> GetAll(Guid Id);
        Result<SpTransactionMessage> InsertUpdate(QualitativeGenerateTableCRUDViewModel viewModel);
    }
}
