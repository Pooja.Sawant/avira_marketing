﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport
{
    public interface IImportRequestRepository
    {
        ImportRequestViewModel GetRequest(Guid Id);
        IEnumerable<ImportRequestViewModel> GetAll(ImportRequestViewModel tfc);
        Result<SpTransactionMessage> Create(ImportRequestViewModel tfc);
        Result<SpTransactionMessage> Update(ImportRequestViewModel tfc);
        IEnumerable<ImportRequestViewModel> GetImportRequest(string templateName, string statusName);
    }
}
