﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces.Masters;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Avira.MarketingResearch.Repository.Implementations.Masters
{
   public class StandardHoursRepository:IStandardHoursRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "StandardHours";
        public StandardHoursRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }
        public StandardHoursViewModel GetById(Guid guid)
        {
            StandardHoursViewModel statndardHoursViewModel = new StandardHoursViewModel();
            statndardHoursViewModel = GetAllStandardHours(guid).FirstOrDefault();
            return statndardHoursViewModel;
        }

        public IEnumerable<StandardHoursViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted)
        {
            return GetAllStandardHours(null, PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, includeDeleted);
        }

        private IEnumerable<StandardHoursViewModel> GetAllStandardHours(Guid? guid, int PageStart = 0, int pageSize = 0, int SortDirection = -1, string OrdbyByColumnName = "", string Search = "", bool includeDeleted = false)
        {

            DataTable standarHoursIdsTable = new DataTable();
            standarHoursIdsTable.Columns.Add("ID", typeof(Guid));

            if (guid != null)
            {
                standarHoursIdsTable.Rows.Add(guid);
            }
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<StandardHoursViewModel>(StandardHoursQueries.StoreProcSelectAll
                            , new { @StandardHoursIDList = standarHoursIdsTable.AsTableValuedParameter("dbo.udtUniqueIdentifier"), @includeDeleted = includeDeleted, @PageStart = PageStart, @pageSize = pageSize, @SortDirection = SortDirection, @OrdbyByColumnName = OrdbyByColumnName, @StandardHoursName = Search }
                            , null, true, null, CommandType.StoredProcedure);
        }


        public Result<SpTransactionMessage> Update(StandardHoursUpdateDbViewModel standardHoursUpdateDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(StandardHoursQueries.StoreProcUpdate, standardHoursUpdateDbViewModel, CommandType.StoredProcedure);
            return spTransactionMessage;
        }
    }
}
