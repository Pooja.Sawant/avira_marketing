﻿
CREATE procedure [dbo].[SFab_insertSubscription]      
(
	@AviraUserId uniqueidentifier,
	@CustomerId uniqueidentifier,
	@SubscriptionId uniqueidentifier,
	@SubscriptionName nvarchar(512),      
	@Description nvarchar(512) = null,  
	@StartDate datetime = null,  
	@EndDate datetime = null
	  
)      
as      
/*================================================================================      
Procedure Name: [SFab_insertSubscription]      
Author: Sai Krishna      
Create date: 23/05/2019      
Description: Insert a record into Subscription Table 
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
23/05/2019 Sai Krishna   Initial Version      
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)  

IF exists(select 1 
from [dbo].[Subscription] sub
Inner Join [dbo].[CustomerSubscriptionMap] csm
On csm.CustomerId = @CustomerId
and sub.Id = csm.SubscriptionId
)
begin
set @ErrorMsg = 'Subscription "'+ @SubscriptionName + '" already exists.';
SELECT StoredProcedureName ='SFab_insertSubscription','Message' =@ErrorMsg,'Success' = 0;  
RETURN;
end
      
BEGIN TRY         
         
insert into [dbo].[Subscription]([Id],      
        [SubscriptionName],      
        [Description],  
		[StartDate],
		[EndDate],      
        [IsDeleted],      
        [UserCreatedById],      
        [CreatedOn])      
 values(
   @SubscriptionId,       
   @SubscriptionName,       
   @Description,  
   @StartDate,    
   @EndDate,   
   0,      
   @AviraUserId,       
   GETDATE());      
  

--select @RegionID;      
--return(0)      
SELECT StoredProcedureName ='SApp_insertSubscription',Message = @ErrorMsg,Success = 1;       
    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);      
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());      
    
 set @ErrorMsg = 'Error while creating a new Region, please contact Admin for more details.';      
--THROW 50000,@ErrorMsg,1;        
--return(1)    
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
       
END CATCH      
      
      
end