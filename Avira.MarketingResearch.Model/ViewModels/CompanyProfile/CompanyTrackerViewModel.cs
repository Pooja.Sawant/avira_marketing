﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.CompanyProfile
{
    public class CompanyTrackerViewModel
    {
        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
        public string CompanyName { get; set; }
        public Guid ProjectId { get; set; }
        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "CompanyTracker";
            }
        }
    }

    public class ProjectInformationViewModel
    {
        public Guid ProjectId { get; set; }
        public int CompaniesProfiledTargetCompletedCount { get; set; }
        public double CompaniesProfiledTargetCompletedPct { get; set; }
        public int CompaniesProfiledTargetApprovedCount { get; set; }
        public double CompaniesProfiledTargetApprovedPct { get; set; }
        public int CompaniesProfiledActualCompletedCount { get; set; }
        public double CompaniesProfiledActualCompletedPct { get; set; }
        public int CompaniesProfiledActualApprovedCount { get; set; }
        public double CompaniesProfiledActualApprovedPct { get; set; }
        public int FundamentalsCompletedCount { get; set; }
        public double FundamentalsCompletedPct { get; set; }
        public int FundamentalsApprovedCount { get; set; }
        public double FundamentalsApprovedPct { get; set; }
        public int OrgStructureCompletedCount { get; set; }
        public double OrgStructureCompletedPct { get; set; }
        public int OrgStructureApprovedCount { get; set; }
        public double OrgStructureApprovedPct { get; set; }
        public int PrductsServicesCompletedCount { get; set; }
        public double PrductsServicesCompletedPct { get; set; }
        public int PrductsServicesApprovedCount { get; set; }
        public double PrductsServicesApprovedPct { get; set; }
        public int RevenueProfitAnalysisCompletedCount { get; set; }
        public double RevenueProfitAnalysisCompletedPct { get; set; }
        public int RevenueProfitAnalysisApprovedCount { get; set; }
        public double RevenueProfitAnalysisApprovedPct { get; set; }
        public int OtherFinancialAnalysisCompletedCount { get; set; }
        public double OtherFinancialAnalysisCompletedPct { get; set; }
        public int OtherFinancialAnalysisApprovedCount { get; set; }
        public double OtherFinancialAnalysisApprovedPct { get; set; }
        public int ShareVolumeGraphCompletedCount { get; set; }
        public double ShareVolumeGraphCompletedPct { get; set; }
        public int ShareVolumeGraphApprovedCount { get; set; }
        public double ShareVolumeGraphApprovedPct { get; set; }
        public int DebtLongTermCompletedCount { get; set; }
        public double DebtLongTermCompletedPct { get; set; }
        public int DebtLongTermApprovedCount { get; set; }
        public double DebtLongTermApprovedPct { get; set; }
        public int ClientStrategiesCompletedCount { get; set; }
        public double ClientStrategiesCompletedPct { get; set; }
        public int ClientStrategiesApprovedCount { get; set; }
        public double ClientStrategiesApprovedPct { get; set; }
        public int OpeningBalanceCompletedCount { get; set; }
        public double OpeningBalanceCompletedPct { get; set; }
        public int OpeningBalanceApprovedCount { get; set; }
        public double OpeningBalanceApprovedPct { get; set; }
        public int FromOperatingActivitiesCompletedCount { get; set; }
        public double FromOperatingActivitiesCompletedPct { get; set; }
        public int FromOperatingActivitiesApprovedCount { get; set; }
        public double FromOperatingActivitiesApprovedPct { get; set; }
        public int FromFinancingActivitiesCompletedCount { get; set; }
        public double FromFinancingActivitiesCompletedPct { get; set; }
        public int FromFinancingActivitiesApprovedCount { get; set; }
        public double FromFinancingActivitiesApprovedPct { get; set; }
        public int FromInvestingActivitiesCompletedCount { get; set; }
        public double FromInvestingActivitiesCompletedPct { get; set; }
        public int FromInvestingActivitiesApprovedCount { get; set; }
        public double FromInvestingActivitiesApprovedPct { get; set; }
        public int ClosingBalanceCompletedCount { get; set; }
        public double ClosingBalanceCompletedPct { get; set; }
        public int ClosingBalanceApprovedCount { get; set; }
        public double ClosingBalanceApprovedPct { get; set; }
    }

    public class MainCompanyTrackerViewModel
    {
        public Guid ProjectId { get; set; }
        public List<CompanyTrackerViewModel> CompanyTrackerList { get; set; }
        public ProjectInformationViewModel ProjectInformationViewModel { get; set; }
    }
}
