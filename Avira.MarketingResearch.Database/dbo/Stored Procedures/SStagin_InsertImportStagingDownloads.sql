﻿  
CREATE PROCEDURE [dbo].[SStagin_InsertImportStagingDownloads]
@TemplateName NVARCHAR(200)='',
@ImportFileName NVARCHAR(200)='',
@JsonReportGeneration NVARCHAR(MAX),
@UserCreatedById UNIQUEIDENTIFIER=null	
AS
/*================================================================================    
Procedure Name: SStagin_InsertImportDownloads    
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
12/03/2020 Raju   Initial Version    
================================================================================*/ 
DECLARE @ImportId UNIQUEIDENTIFIER =NEWID()
declare @ErrorMsg NVARCHAR(2048),@CreatedOn Datetime
BEGIN
	SET @CreatedOn = GETDATE()
	BEGIN TRY 
	Delete from ImportData where Id in (select Distinct ImportId from StagingDownloadsExportData); 
    DELETE from StagingDownloadsExportData;
	  
	INSERT INTO ImportData
	(Id               , 
TemplateName		  ,
ImportFileName		  ,
ImportedOn			  ,
UserImportedById)
VALUES(@ImportId, @TemplateName, @ImportFileName,@CreatedOn, @UserCreatedById)
---------------------------------------------------
INSERT INTO StagingDownloadsExportData(
 Id
,RowNo
,ImportId
,ProjectName
,ModuleName
,Header
,Footer
,ReportTitle
,CoverPageImageLink
,CoverPageImageName
,Overview
,KeyPoints
,Methodology
,MethodologyImageLink
,MethodologyImageName
,ContactUsAnalystName
,ContactUsOfficeContactNo
,ContactUsAnalystEmailId
,CreatedOn
,UserCreatedById
,ErrorNotes)

SELECT
      newid()
      ,RowNo
      ,@ImportId
      ,ProjectName
      ,ModuleName
      ,Header
      ,Footer
      ,ReportTitle
      ,CoverPageImageLink
	  ,CoverPageImageName
      ,Overview
      ,KeyPoints
      ,Methodology
      ,MethodologyImageLink
	  ,MethodologyImageName
      ,ContactUsAnalystName
      ,ContactUsOfficeContactNo
      ,ContactUsAnalystEmailId
      ,CreatedOn
      ,UserCreatedById
      ,ErrorNotes
    FROM OPENJSON(@jsonReportGeneration)
    WITH (   
	       RowNo int ,
	       [ProjectName] [nvarchar](256),
	       [ModuleName] [nvarchar](100) ,
	       [Header] [nvarchar](150) ,
	       [Footer] [nvarchar](150),
	       [ReportTitle] [nvarchar](250),
	       [CoverPageImageLink] [nvarchar](250),
	       [CoverPageImageName] [nvarchar](100) ,
	       [Overview] [nvarchar](3050),
	       [KeyPoints] [nvarchar](3050) ,
	       [Methodology] [nvarchar](3050) ,
	       [MethodologyImageLink] [nvarchar](250) ,
		   [MethodologyImageName] [nvarchar](100) ,
	       [ContactUsAnalystName] [nvarchar](150) ,
	       [ContactUsOfficeContactNo] [nvarchar](20) ,
	       [ContactUsAnalystEmailId] [nvarchar](254),
	       [CreatedOn] [datetime2](7),
	       [UserCreatedById] [uniqueidentifier] ,
	       [ErrorNotes] [nvarchar](max)
         ) AS JsonReportGeneration
 

 SELECT StoredProcedureName ='SStagin_InsertImportDownloads',Message =@ErrorMsg,Success = 1;   
END TRY    
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 exec SApp_LogErrorInfo
 set @ErrorMsg = 'Error while insert a Downloads, please contact Admin for more details.';    
SELECT StoredProcedureName ='SStagin_InsertImportDownloads',Message =@ErrorMsg,Success = 0;
END CATCH

END