﻿CREATE TABLE [dbo].[StagingESMAnalysis] (
    [Id]                        UNIQUEIDENTIFIER NOT NULL,
    [ImportId]                  UNIQUEIDENTIFIER NULL,
    [RowNo]                     INT              NULL,
    [MarketName]                NVARCHAR (255)   NULL,
    [SegmentName]               NVARCHAR (255)   NULL,
    [SubSegmentName]            NVARCHAR (260)   NULL,
    [AnalysisCategory]          NVARCHAR (255)   NULL,
    [AnalysisText]              NVARCHAR (MAX)   NULL,
    [Importance]                NVARCHAR (255)   NULL,
    [Impact]                    NVARCHAR (255)   NULL,
    [MarketId]                  UNIQUEIDENTIFIER NULL,
    [SegmentId]                 UNIQUEIDENTIFIER NULL,
    [SubSegmentId]              UNIQUEIDENTIFIER NULL,
    [ProjectId]                 UNIQUEIDENTIFIER NULL,
    [QualitativeAnalysisTypeId] UNIQUEIDENTIFIER NULL,
    [ImportanceId]              UNIQUEIDENTIFIER NULL,
    [ImpactId]                  UNIQUEIDENTIFIER NULL,
    [ErrorNotes]                NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_StagingESMAnalysis] PRIMARY KEY CLUSTERED ([Id] ASC)
);

