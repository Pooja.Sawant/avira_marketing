﻿CREATE procedure [dbo].[SFab_GetCompanyComparisonListbyProjectID]
(
@SegmentID uniqueidentifier=null,
@SegmentList [dbo].[udtUniqueIdentifier] READONLY ,
@RegionIdList [dbo].[udtUniqueIdentifier] READONLY,
@CountryIdList dbo.[udtUniqueIdentifier] readonly,
--@CompanystageId UniqueIdentifier=null,
@CompanystageId [dbo].[udtUniqueIdentifier] READONLY ,
@StratergyList [dbo].[udtUniqueIdentifier] READONLY,
@Years nvarchar(100)=null,
@ProjectId uniqueidentifier,
@CompanyIdList dbo.[udtUniqueIdentifier] readonly
)
as
/*================================================================================
Procedure Name: [SFab_GetCompanyComparisonListbyProjectID]
Author: Sai
Create date: 07/29/2019
Description: Get list from company table by project ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
07/29/2019	Sai Krishna	    Initial Version
10/21/2019	Pooja	        segmentlist para change
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @RegionList dbo.[udtUniqueIdentifier];
declare @YearList table (Year int);
insert into @YearList
select value
from string_split(@Years,',')

---------------

DECLARE @SegmentListAll [dbo].[udtUniqueIdentifier]

insert into @SegmentListAll(ID)
SELECT ID FROM @SegmentList

if not exists(select 1 from @SegmentListAll)
begin

insert into @SegmentListAll(ID)
SELECT distinct
      MSP.SegmentId 
FROM [dbo].[QualitativeSegmentMapping] MSP  
INNER JOIN Segment Segment ON Segment.Id=MSP.SegmentId  
where (@ProjectId is null or MSP.ProjectId = @ProjectID)  
and  MSP.IsDeleted = 0 

end

--------------


insert into @RegionList
select Id
from Region
where id in (select id from @RegionIdList)
or exists (select 1
			from CountryRegionMap CRM
			inner join @RegionIdList Loc
			on CRM.CountryId = Loc.ID
			and Region.id = CRM.RegionId)
			;


	select comp.Id,
	 comp.CompanyName
	from Company comp
	inner join Nature
	on comp.NatureId = Nature.Id
	LEFT join CompanyStage
	on comp.CompanyStageId = CompanyStage.Id
	where comp.IsDeleted = 0 
	and exists (select 1 from CompanyProjectMap cpmap
	where comp.Id = cpmap.CompanyId 
	and cpmap.ProjectId = @ProjectId
	--and (cpmap.ProjectId = @ProjectId or 
	
	--( (not exists (select * from @StratergyList) 
	--	or exists(select 1 from [CompanyStrategyMapping] CSM
	--			inner join  @StratergyList CSlist
	--			on CSM.Category = CSlist.ID
	--			where comp.Id = CSM.CompanyId))
	--and (not exists(select 1 from @SegmentListAll)
	--	or exists(SELECT 1 FROM CompanyEcosystemPresenceESMMap CPSM					
	--				inner join @SegmentListAll list
	--				on list.ID = CPSM.ESMId
	--				where CPSM.CompanyId = comp.Id)
	--				))
	--	 )
	and cpmap.IsDeleted = 0 )
	and (not exists (select * from @RegionIdList) 
		or exists(select 1 from CompanyRegionMap CRM
				  inner join @RegionList Rlist
				  on CRM.RegionId = Rlist.ID))
	--and (@CompanystageId is null or  comp.CompanyStageId = @CompanystageId)
	and (not exists (select * from @CompanystageId) 
		or exists(select 1 from [CompanyStage] CST
				inner join  @CompanystageId CSTAList
				on CST.Id = CSTAList.ID
				where comp.CompanyStageId = CSTAList.ID))
	
	and (@Years is null or @Years = '' 
		or exists(select 1 from CompanyRevenue
					inner join @YearList Ylist
					on Ylist.Year = CompanyRevenue.Year
					and CompanyRevenue.CompanyId = comp.Id))
					ORDER BY comp.CompanyName;
end