﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using static Avira.MarketingResearch.Model.ViewModels.ApplicationViewModel;

namespace Avira.MarketingResearch.Repository.Implementations
{
    public class ApplicationRepository:IApplicationRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "Application";
        public ApplicationRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public ApplicationViewModel GetById(Guid guid)
        {
            ApplicationViewModel applicationViewModel = new ApplicationViewModel();
            applicationViewModel = GetAllApplication(guid).FirstOrDefault();
            return applicationViewModel;
        }

        public IEnumerable<ApplicationViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted)
        {
            return GetAllApplication(null, PageStart, pageSize, SortDirection, OrdbyByColumnName,Search, includeDeleted);
        }

        private IEnumerable<ApplicationViewModel> GetAllApplication(Guid? guid, int PageStart = 0, int pageSize = 0, int SortDirection = -1, string OrdbyByColumnName = "", string Search = "", bool includeDeleted = false)
        {
            DataTable applicationIdsTable = new DataTable();
            applicationIdsTable.Columns.Add("ID", typeof(Guid));

            if (guid != null)
            {
                applicationIdsTable.Rows.Add(guid);

            }

            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<ApplicationViewModel>(ApplicationQueries.StoreProcGetAll
                            , new { @ApplicationIDList = applicationIdsTable.AsTableValuedParameter("dbo.udtUniqueIdentifier"), @includeDeleted = includeDeleted, @PageStart = PageStart, @pageSize = pageSize, @SortDirection = SortDirection, @OrdbyByColumnName = OrdbyByColumnName, @ApplicationName=Search }
                            , null, true, null, CommandType.StoredProcedure);
        }


        public Result<SpTransactionMessage> Create(ApplicationInsertDbViewModel applicationDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(ApplicationQueries.StoreProcInsert,
                applicationDbViewModel,
                CommandType.StoredProcedure);
            return spTransactionMessage;

        }

        public Result<SpTransactionMessage> Update(ApplicationUpdateDbViewModel applicationDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(ApplicationQueries.StoreProcUpdate,
               applicationDbViewModel,
                CommandType.StoredProcedure);
            return spTransactionMessage;
        }
    }
}
