﻿CREATE PROCEDURE [dbo].[SApp_LogErrorInfo]
	-- Add the parameters for the stored procedure here

AS
/*================================================================================      
Procedure Name:[SApp_LogErrorInfo]     
Author:Raju      
Create date: 023-Jan-2020  
Description:  
Change History      
Date  Developer  Reason    
__________ ____________ ______________________________________________________      

23-JAN-2020  Raju      
================================================================================*/      
declare @ErrorNumber int ,
 @ErrorSeverity int ,  
 @ErrorProcedure varchar(100),
 @ErrorLine int ,
 @ErrorMessage varchar(500)
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;     
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
--	-- Insert statements for procedure here
--	insert into  dbo.Errorlog(
--	   ErrorNumber,  
--       ErrorSeverity,  
--       ErrorProcedure,  
--       ErrorLine,  
--       ErrorMessage,  
--       ErrorDate) 
--values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());     




insert into Errorlog(
ErrorNumber
,ErrorSeverity
,ErrorProcedure
,ErrorLine
,ErrorDate
,ErrorMessage)
 
 SELECT  
          ERROR_NUMBER() AS Number
        ,ERROR_SEVERITY() AS Severity
        ,ERROR_PROCEDURE() AS StoredProcedureName
        ,ERROR_LINE () AS LineNumber
        ,getdate() AS Date
        ,ERROR_MESSAGE() AS Message ;

		END


		--select * from Errorlog
		--where ErrorProcedure='SApp_InsertMarketValue'