﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.CompanyProfile
{
    public class CompanyStructureModel
    {
        public Guid Id { get; set; }
        public string CompanyName { get; set; }
        public string Description { get; set; }
        public Guid ParentId { get; set; }
        public string Generation { get; set; }
        public string RowType
        {
            get
            {
                return "CompanyStructure";
            }
        }
    }
}
