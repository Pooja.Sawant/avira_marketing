﻿--sp_helptext SApp_InsertUpdateTrendIndustryMap

  
  
  
CREATE procedure [dbo].[SApp_InsertUpdateTrendIndustryMap]        
(  
@TrendId uniqueidentifier,     
@TrendIndustrymap dbo.[udtGUIDGUIDMapValue] readonly,  
@AviraUserId uniqueidentifier  
)        
as        
/*================================================================================        
Procedure Name: SApp_InsertTrendIndustryMap        
Author: Swami        
Create date: 04/12/2019        
Description: Insert a record into TrendIndustryMap table        
Change History        
Date  Developer  Reason        
__________ ____________ ______________________________________________________        
04/12/2019 Swami   Initial Version      
================================================================================*/        
BEGIN        
SET NOCOUNT ON;        
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;        
declare @ErrorMsg NVARCHAR(2048)    
      
--Validations        
--IF exists(select 1 from [dbo].[TrendIndustryMap]         
--   where TrendId = @TrendId AND IndustryId = @IndustryId AND IsDeleted = 0)        
--begin         
--set @ErrorMsg = 'Industry with Id "'+ CONVERT (nvarchar(30), @IndustryId) + '" already exists.';      
--SELECT StoredProcedureName ='SApp_InsertTrendIndustryMap',Message =@ErrorMsg,Success = 0;      
--RETURN;      
--end        
--End of Validations        
BEGIN TRY           
--mark records as delete which are not in list  
--update [dbo].[TrendIndustryMap]  
--set IsDeleted = 1,  
--UserDeletedById = @AviraUserId,  
--DeletedOn = GETDATE()  
--from [dbo].[TrendIndustryMap]  
--where [TrendIndustryMap].TrendId = @TrendId  
--and not exists (select 1 from @TrendIndustrymap map where map.id = [TrendIndustryMap].IndustryId)  
  
 --Update records for change  
update [dbo].[TrendIndustryMap]  
set Impact = map.mapid,  
EndUser = map.bitvalue,  
ModifiedOn = GETDATE(),  
UserModifiedById = @AviraUserId  
from [dbo].[TrendIndustryMap]  
inner join @TrendIndustrymap map   
on map.id = [TrendIndustryMap].IndustryId  
where [TrendIndustryMap].TrendId = @TrendId;  
  
--Insert new records  
insert into [dbo].[TrendIndustryMap]([Id],        
        [TrendId],        
        [IndustryId],   
  [Impact],  
  [EndUser],  
        [IsDeleted],        
        [UserCreatedById],        
        [CreatedOn])        
 select NEWID(),  
   @TrendId,         
   map.ID as [IndustryId],  
   map.mapid as [Impact],       
   map.bitvalue,       
   0,        
   @AviraUserId,         
   GETDATE()  
 from @TrendIndustrymap map  
 where not exists (select 1 from [dbo].[TrendIndustryMap] map1  
     where map1.TrendId = @TrendId  
     and map.ID = map1.IndustryId);         
       
SELECT StoredProcedureName ='SApp_InsertTrendIndustryMap',Message =@ErrorMsg,Success = 1;         
      
END TRY        
BEGIN CATCH        
    -- Execute the error retrieval routine.        
 DECLARE @ErrorNumber int;        
 DECLARE @ErrorSeverity int;        
 DECLARE @ErrorProcedure varchar(100);        
 DECLARE @ErrorLine int;        
 DECLARE @ErrorMessage varchar(500);        
        
  SELECT @ErrorNumber = ERROR_NUMBER(),        
        @ErrorSeverity = ERROR_SEVERITY(),        
        @ErrorProcedure = ERROR_PROCEDURE(),        
        @ErrorLine = ERROR_LINE(),        
        @ErrorMessage = ERROR_MESSAGE()        
        
 insert into dbo.Errorlog(ErrorNumber,        
       ErrorSeverity,        
       ErrorProcedure,        
       ErrorLine,        
       ErrorMessage,        
       ErrorDate)        
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());        
      
 set @ErrorMsg = 'Error while insert a new TrendIndustryMap, please contact Admin for more details.';      
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine      
    ,Message =@ErrorMsg,Success = 0;         
         
END CATCH        
        
        
end