﻿using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using static Avira.MarketingResearch.Model.ViewModels.ComponentViewModel;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ComponentController : AviraBaseController
    {
        public readonly IComponentRepository _componentRepository;
        public readonly IComponentService _componentService;
        private ILoggerManager _logger;
        public ComponentController(
            IComponentService componentService,
            IComponentRepository componentRepository,
            ILoggerManager logger)
        {
            _componentRepository = componentRepository;
            _componentService = componentService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ComponentViewModel>> Get(int PageStart = 0, int pageSize = 0, int SortDirection = 1, string OrdbyByColumnName = "CreatedDate", string Search = "", bool isDeletedInclude = false)
        {
            List<ComponentViewModel> res = null;
            try
            {
                res = _componentService.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName,Search, isDeletedInclude).ToList();
        }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }


        [HttpGet("{id}")]
        public ActionResult<ComponentViewModel> Get(Guid id)
        {
            ComponentViewModel component = null;
            try
            {
                 component = _componentService.GetById(id);
            if (component == null)
            {
                return NotFound();
            }
        }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return component;
        }

        [HttpPost]
        public ActionResult Post([FromBody] ComponentInsertDbViewModel viewModel)
        {
            viewModel.CreatedOn = System.DateTime.UtcNow;
            viewModel.UserCreatedById = CurrentUser.Id;
            viewModel.Id = Guid.NewGuid();

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new component, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                var result = _componentService.Create(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Component name " + viewModel.ComponentName + " is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }


        [HttpPut]
        public ActionResult Put([FromBody] ComponentUpdateDbViewModel viewModel)
        {

            viewModel.ModifiedOn = System.DateTime.UtcNow;
            viewModel.UserModifiedById = CurrentUser.Id;

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while updating a new component, please contact Admin for more details."
            };
            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                Result<SpTransactionMessage> result = _componentService.Update(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Component name " + viewModel.ComponentName + " is updated successfully.";

                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }

        [Route("DeleteSegment")]
        [HttpPut]
        public ActionResult Delete([FromBody] ComponentUpdateDbViewModel viewModel)
        {

            viewModel.DeletedOn = System.DateTime.UtcNow;
            viewModel.UserDeletedById = CurrentUser.Id;
            viewModel.IsDeleted = true;

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while deleting a new component, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                Result<SpTransactionMessage> result = _componentService.Update(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;

                    viewData.Message = "Compoenent name " + viewModel.ComponentName + " is deleted.";

                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }
    }
}

