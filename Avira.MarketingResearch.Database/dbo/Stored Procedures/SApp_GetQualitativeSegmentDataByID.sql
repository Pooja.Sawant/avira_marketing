﻿

CREATE procedure [dbo].[SApp_GetQualitativeSegmentDataByID]    
(  
@ProjectId uniqueidentifier,
@SegmentId uniqueidentifier)    
as    
/*================================================================================    
Procedure Name: SApp_GetQualitativeSegmentDataByID   
Author: Harshal    
Create date: 09/06/2019    
Description: Get data from respective segement Table  
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
09/06/2019  Harshal   Initial Version    
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
   	  Select subsegment.Id As SubsegId,
      SubSegmentName As SegName,
      subsegment.Description As SegDespcription
      FROM subsegment
      WHERE subsegment.isDeleted=0
      AND subsegment.SegmentId=@SegmentId
      and exists (select 1 from  QualitativeSegmentMapping
      where  subsegment.SegmentId = QualitativeSegmentMapping.SegmentId
      and QualitativeSegmentMapping.SubSegmentId = subsegment.Id
      and QualitativeSegmentMapping.ProjectId = @ProjectId)
      order by  SegName
END