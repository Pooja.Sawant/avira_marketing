﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile
{
    public interface ICompanyTrendsMapRepository
    {
        IEnumerable<CompanyTrendsMapViewModel> GetAll(Guid companyId);
        Result<SpTransactionMessage> InsertUpdate(MainCompanyTrendsMapInsertUpdateDbViewModel CompanyTrendsMapInsertUpdateDbViewModel);
    }
}
