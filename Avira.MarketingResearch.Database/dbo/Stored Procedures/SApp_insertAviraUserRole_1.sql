﻿
CREATE procedure [dbo].[SApp_insertAviraUserRole]
(@ID uniqueidentifier,
@UserRoleName nvarchar(256),
@UserType tinyint,
@CreatedOn Datetime,
@UserCreatedById uniqueidentifier
)
as
/*================================================================================
Procedure Name: SApp_insertAviraUserRole
Author: Gopi
Create date: 04/01/2019
Description: Insert a record into UserRole table
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
04/01/2019	Gopi			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ErrorMsg NVARCHAR(2048);

--Validations
IF exists(select 1 from [dbo].[UserRole] 
		 where UserRoleName = @UserRoleName AND IsDeleted = 0)
begin
set @ErrorMsg = 'User with RoleName "'+ @UserRoleName + '" already exists.';
SELECT StoredProcedureName ='SApp_insertAviraUserRole',Message =@ErrorMsg,Success = 0;  
RETURN;  
end
--End of Validations
BEGIN TRY

INSERT INTO [dbo].[UserRole]
           ([Id]
           ,[UserRoleName]
           ,[UserType]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted])--
     VALUES
           (@ID,
           @UserRoleName,
		   @UserType,
		   @CreatedOn,
		   @UserCreatedById,
		   0)
 
 select @ID, StoredProcedureName ='SApp_insertAviraUserRole',Success = 1;
return;
END TRY
BEGIN CATCH
    -- Execute the error retrieval routine.
	DECLARE @ErrorNumber	int;
	DECLARE @ErrorSeverity	int;
	DECLARE @ErrorProcedure	varchar;
	DECLARE @ErrorLine	int;
	DECLARE @ErrorMessage	varchar;

	 SELECT @ErrorNumber = ERROR_NUMBER(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorProcedure = ERROR_PROCEDURE(),
        @ErrorLine = ERROR_LINE(),
        @ErrorMessage = ERROR_MESSAGE()

	insert into dbo.Errorlog(ErrorNumber,
							ErrorSeverity,
							ErrorProcedure,
							ErrorLine,
							ErrorMessage,
							ErrorDate)
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,@CreatedOn);

	set @ErrorMsg = 'Error while creating a new AviraUserRole, please contact Admin for more details.';

SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;

END CATCH


end