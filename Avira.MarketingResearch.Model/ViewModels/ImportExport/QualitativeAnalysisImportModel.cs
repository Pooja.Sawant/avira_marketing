﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.ImportExport
{
    public class QualitativeAnalysisImportModel
    {
        public string TemplateName { get; set; }
        public string ImportFileName { get; set; }
        public string JsonNewsList { get; set; }
        public string JsonAnalysisList { get; set; }
        public string JsonDescriptionList { get; set; }
        public string JsonQuoteList { get; set; }
        public string JsonSegmentsList { get; set; }
        public string JsonRegionList { get; set; }
        public string JsonESMSegmentList { get; set; }
        public string JsonESMAnalysisList { get; set; }

        public IEnumerable<QualitativeNewsImportModel> NewsList { get; set; }
        public IEnumerable<AnalysisImportModel> AnalysisList { get; set; }
        public IEnumerable<QualitativeDescriptionImportModel> DescriptionList { get; set; }
        public IEnumerable<QualitativeQuoteImportModel> QuoteList { get; set; }
        public IEnumerable<QualitativeSegmentsImportModel> SegmentsList { get; set; }
        public IEnumerable<QualitativeRegionsImportModel> RegionList { get; set; }
        public IEnumerable<ESMSegmentsImportModel> ESMSegmentList { get; set; }
        public IEnumerable<ESMAnalysisImportModel> ESMAnalysisList { get; set; }
        public Guid UserCreatedById { get; set; }
    }

    public class QualitativeNewsImportModel
    {
        public int RowNo { get; set; }
        public string MarketName { get; set; }
        public string SubMarketName { get; set; }
        public DateTime? NewsDate { get; set; }
        public string News { get; set; }
        public string NewsCategory { get; set; }
        public string Category { get; set; }
        public string Importance { get; set; }
        public string Impact { get; set; }
    }

    public class AnalysisImportModel
    {
        public int RowNo { get; set; }
        public string MarketName { get; set; }
        public string SubMarketName { get; set; }
        public string Analysis_Category { get; set; }
        public string Analysis_subcategory { get; set; }
        public string AnalysisText { get; set; }
        public string Importance { get; set; }
        public string Impact { get; set; }
    }

    public class QualitativeDescriptionImportModel
    {
        public int RowNo { get; set; }
        public string MarketName { get; set; }
        public string SubMarketName { get; set; }
        public string Description_Category { get; set; }
        public string Description_Subcategory { get; set; }
        public string DescriptionText { get; set; }
    }

    public class QualitativeQuoteImportModel
    {
        public int RowNo { get; set; }
        public string MarketName { get; set; }
        public string SubMarketName { get; set; }
        public string ResourceName { get; set; }
        public string ResourceCompany { get; set; }
        public string ResourceDesignation { get; set; }
        public DateTime? DateOfQuote { get; set; }
        public string Quote { get; set; }
        public string OtherRemarks { get; set; }
    }

    public class QualitativeSegmentsImportModel
    {
        public int RowNo { get; set; }
        public string MarketName { get; set; }
        public string SegmentName { get; set; }
        public string SubSegmentName { get; set; }
        public string ParentName { get; set; }
    }

    public class QualitativeRegionsImportModel
    {
        public int RowNo { get; set; }
        public string MarketName { get; set; }
        public string RegionName { get; set; }
        public string CountryName { get; set; }
    }

    public class ESMSegmentsImportModel
    {
        public int RowNo { get; set; }
        public string MarketName { get; set; }
        public string SegmentName { get; set; }
        public string SubSegmentName { get; set; }
        public string ParentName { get; set; }
    }

    public class ESMAnalysisImportModel
    {
        public int RowNo { get; set; }
        public string MarketName { get; set; }
        public string SegmentName { get; set; }
        public string SubSegmentName { get; set; }
        public string Analysis_Category { get; set; }
        public string AnalysisText { get; set; }
        public string Importance { get; set; }
        public string Impact { get; set; }
    }

}

