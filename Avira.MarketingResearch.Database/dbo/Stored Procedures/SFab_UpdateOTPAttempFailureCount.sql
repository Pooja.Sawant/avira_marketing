﻿
CREATE PROCEDURE [dbo].[SFab_UpdateOTPAttempFailureCount]
(
@OtpEntity	NVARCHAR(512)=null,
@AttempFailureCount int=null,
@ClientDeviceId nvarchar(512)=null
)
as
/*================================================================================
Procedure Name: SFab_UpdateOTPAttempFailureCount
Author: Pooja Sawant
Create date: 06/10/2019
Description: update a record in otpvalidation table 
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
06/10/2019	Pooja			Initial Version
================================================================================*/
BEGIN
declare @ErrorMsg NVARCHAR(2048);
declare @ID UNIQUEIDENTIFIER=null
declare @AttemptCount int=null

set @ID= (select  top 1 id from OTPVerification where OtpEntity=@OtpEntity and ClientDeviceId=@ClientDeviceId order by GeneratedDateTime desc)
set @AttemptCount= (select  top 1 isnull(AttempFailureCount,0) from OTPVerification where OtpEntity=@OtpEntity and ClientDeviceId=@ClientDeviceId order by GeneratedDateTime desc)
BEGIN TRY
if(@ID is not null)
begin
if(@AttemptCount<@AttempFailureCount)
begin
	update OTPVerification set AttempFailureCount=@AttemptCount+1 where  Id=@ID
	SELECT StoredProcedureName ='SFab_UpdateOTPAttempFailureCount',Message =@ErrorMsg,Success = 1;  
end
else
begin
set @ErrorMsg='Max Attempt failure count is exceeded';
  SELECT StoredProcedureName ='SFab_UpdateOTPAttempFailureCount',Message =@ErrorMsg,Success = 0;
end


end
else
begin
  SELECT StoredProcedureName ='SFab_UpdateOTPAttempFailureCount',Message =@ErrorMsg,Success = 0;  
end
END TRY
BEGIN CATCH
    -- Execute the error retrieval routine.
	DECLARE @ErrorNumber	int;
	DECLARE @ErrorSeverity	int;
	DECLARE @ErrorProcedure	varchar(100);
	DECLARE @ErrorLine	int;
	DECLARE @ErrorMessage	varchar(500);

	 SELECT @ErrorNumber = ERROR_NUMBER(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorProcedure = ERROR_PROCEDURE(),
        @ErrorLine = ERROR_LINE(),
        @ErrorMessage = ERROR_MESSAGE()

	insert into dbo.Errorlog(ErrorNumber,
							ErrorSeverity,
							ErrorProcedure,
							ErrorLine,
							ErrorMessage,
							ErrorDate)
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());

 set @ErrorMsg = 'Error while Updateing a OTPVerification, please contact Admin for more details.';    
--THROW 50000,@ErrorMsg,1;      
--return(1)  
SELECT Number = @ErrorNumber, 
		Severity =@ErrorSeverity,
		StoredProcedureName =@ErrorProcedure,
		LineNumber= @ErrorLine,
		Message =@ErrorMsg,
		Success = 0;     
END CATCH
end