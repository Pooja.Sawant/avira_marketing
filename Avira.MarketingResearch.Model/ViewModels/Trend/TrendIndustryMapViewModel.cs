﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.Trend
{
    public class TrendIndustryMapViewModel
    {
        public Guid TrendId { get; set; }
        public Guid IndustryId { get; set; }
        public Guid ImpactId { get; set; }
        public string IndustryName { get; set; }
        public bool IsMapped { get; set; }
        public bool EndUser { get; set; }
        public int TotalItems { get; set; }
        public TrendViewModel TrendModel { get; set; }
        public string RowType
        {
            get
            {
                return "IndustryMap";
            }
        }
    }

    //public class MainTrendIndustrytMapViewModel
    //{
    //    public List<TrendIndustryMapViewModel> TrendIndustryMapViewModel { get; set; }
    //    public TrendViewModel TrendModel { get; set; }
    //}

    public class TrendIndustryMapInsertDbViewModel
    { 
        public Guid TrendId { get; set; }
        public string IndustryName { get; set; }
        public List<TrendIndustryMapViewModel> TrendIndustryMapViewModel { get; set; }
        public Guid AviraUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public TrendViewModel TrendModel { get; set; }
        public TrendNoteViewModel TrendNoteModel { get; set; }
    }
}