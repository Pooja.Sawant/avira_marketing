﻿CREATE TABLE [dbo].[Customer] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [CustomerName]     NVARCHAR (250)   NOT NULL,
    [DisplayName]      NVARCHAR (128)   NOT NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_Customer_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    [PrimaryPurpose]   NVARCHAR (500)   NULL,
    [FunctionalArea]   NVARCHAR (500)   NULL,
    [PrimaryPurposeId] UNIQUEIDENTIFIER NULL,
    [FunctionalAreaId] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Customer_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_Customer_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id])
);



