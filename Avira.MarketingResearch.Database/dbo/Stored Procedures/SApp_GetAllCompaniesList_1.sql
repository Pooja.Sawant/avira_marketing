﻿CREATE procedure [dbo].[SApp_GetAllCompaniesList]
as
/*================================================================================
Procedure Name: [SApp_GetAllCompaniesList]
Author: Sai Krishna
Create date: 05/06/2019
Description: Get list from Company table
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
05/06/2019	Sai Krishna		Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT  [Id] as CompanyId,
        [ParentId],
        [CompanyName],
		[CompanyLocation],		
		[IsHeadQuarters]
FROM [dbo].[Company]
Where IsDeleted = 0

end