﻿
--drop Procedure SFab_GetCIFinancialAnalysisSummaryTabData

-- 

CREATE   Procedure [dbo].[SFab_GetCIComapnyFinancialAnalysisSummaryData] 
(
	@CompanyID uniqueidentifier = null
)
As
/*================================================================================  
Procedure Name: [SFab_GetCIComapnyFinancialAnalysisSummaryData]  
Author: Nitin 
Create date: 01-Nov-2019  
Description: Get CI Company Financial Analysis Summary Tab Data
Change History  
Date         Developer     Reason  
____________ __________  ______________________________________________________  
01-Nov-2019  Nitin       Initial Version  
================================================================================*/  
BEGIN

	--Declare @CompanyID uniqueidentifier = 'f73f74e0-e9cd-4790-9c6e-119ea6566a8e'

	Declare @UnitId uniqueidentifier

	Declare @Years Table (CompanyID uniqueidentifier, [Year] int)
	Declare @MaxYear int, @MinYear int

	Select @MaxYear = Max([Year]), @MinYear = Min([Year])
	From CompanyPLStatement
	Where CompanyId = @CompanyID;	

	If (@MaxYear Is Not Null And @MinYear Is Not Null)
	Begin
		WITH CTE
			 AS (SELECT @CompanyID As CompanyID, @MinYear As Year1
				 UNION ALL
				 SELECT @CompanyID As CompanyID, Year1 + 1 
				 FROM   CTE
				 WHERE  Year1 < @MaxYear)
		Insert Into @Years
		SELECT *
		FROM   CTE 
	End
	
	--Select * From @Years Order by 1 desc;

	Declare @CICompanyFinancialAnalysisSummaryData Table(
		CompanyId	uniqueidentifier,
		[Year]	int,
		Revenue	decimal (19, 4),
		GrowthPercentage decimal (19, 4),
		GrossProfit	decimal (19, 4),
		GrossGrowthPercentage decimal (19,4),
		MarginPercentage decimal (19, 4),
		EBIT decimal (19, 4),
		EBITGrowthPercentage DECIMAL (19,4),
		EBITMarginPercentage decimal (19, 4),
		EBITDA decimal (19, 4),
		EBITDAGrowthPercentage decimal(19,4),
		EBITDAMarginPercentage decimal (19, 4),
		NetIncome decimal (19, 4),
		NetIncomeGrowthPercentage DECIMAL (19,4),
		NetIncomeMarginPercentage decimal (19, 4),
		DilutedEPS decimal (19, 4),
		CashAndCashEquivalents decimal (19, 4),
		ShortTermInvestments decimal (19, 4),
		TotalCurrentAssets decimal (19, 4),
		ShortTermDebt decimal (19, 4),
		LongTermDebt decimal (19, 4),
		NetCashProvidedByOrUsedInOperatingActivities decimal (19, 4),
		NetCashProvidedByOrUsedInInvestingActivities decimal (19, 4),
		NetCashProvidedByOrUsedInFinancingActivities decimal (10,4),
		NetIncomeFromContinuingOperations DECIMAL(19,4),
		NetIncomeFromContinuingOperationsGrowthPercentage DECIMAL(19,4),
		NetIncomeFromContinuingOperationsMarginPercentage DECIMAL(19,4),
		TangibleNetWorth decimal(19,2),
		CapitalEmployed decimal(19,2)
	)

	Select @UnitId = Id 
	From ValueConversion
	Where ValueName = 'Millions';

	Insert Into @CICompanyFinancialAnalysisSummaryData (CompanyId, [Year], Revenue,
	GrowthPercentage, GrossProfit,GrossGrowthPercentage, MarginPercentage, 
	EBIT, EBITGrowthPercentage,EBITMarginPercentage,EBITDA,EBITDAGrowthPercentage, EBITDAMarginPercentage, NetIncome, NetIncomeGrowthPercentage,NetIncomeMarginPercentage, 
	DilutedEPS,NetIncomeFromContinuingOperations,NetIncomeFromContinuingOperationsGrowthPercentage,NetIncomeFromContinuingOperationsMarginPercentage,TangibleNetWorth,CapitalEmployed)
	Select Y.CompanyId, Y.[Year], 
			Case When CPLS.Revenue Is Null Then Null
			Else [dbo].[fun_UnitDeConversion](@UnitId, CPLS.Revenue)
			End As Revenue, 
((CPLS.Revenue * 1.00)/ NullIf(LAG(CPLS.Revenue) Over (Order By CPLS.[Year]), 0) -1) * 100 As GrowthPercentage, 
[dbo].[fun_UnitDeConversion](@UnitId, CPLS.GrossProfit) As GrossProfit,
((CPLS.GrossProfit * 1.00)/ NullIf(LAG(CPLS.GrossProfit) Over (Order By CPLS.[Year]), 0) -1) * 100 As GrossGrowthPercentage,
           (CPLS.GrossProfit / NULLIF(CPLS.Revenue, 0)) * 100  As MarginPercentage,
		   [dbo].[fun_UnitDeConversion](@UnitId, CPLS.EBIT) As EBIT,
((CPLS.EBIT * 1.00)/ NullIf(LAG(CPLS.EBIT) Over (Order By CPLS.[Year]), 0) -1) * 100 As EBITGrowthPercentage,
		    (CPLS.EBIT / NULLIF(CPLS.Revenue, 0)) * 100 As EBITMarginPercentage,
		   [dbo].[fun_UnitDeConversion](@UnitId, CPLS.EBITDA) As EBITDA, 
((CPLS.EBIT * 1.00)/ NullIf(LAG(CPLS.EBITDA) Over (Order By CPLS.[Year]), 0) -1) * 100 As EBITDAGrowthPercentage,
		   (CPLS.EBITDA / NULLIF(CPLS.Revenue, 0)) * 100 As EBITDAMarginPercentage,
		   [dbo].[fun_UnitDeConversion](@UnitId, CPLS.NetIncome) As NetIncome,
((CPLS.NetIncome * 1.00)/ NullIf(LAG(CPLS.NetIncome) Over (Order By CPLS.[Year]), 0) -1) * 100 As NetIncomeGrowthPercentage, 
		   (CPLS.NetIncome / NULLIF(CPLS.Revenue, 0)) * 100 As NetIncomeMarginPercentage,
		   [dbo].[fun_UnitDeConversion](@UnitId, CPLS.DilutedEPS) As DilutedEPS,
		   [dbo].[fun_UnitDeConversion](@UnitId, CPLS.NetIncomeFromContinuingOperations) As NetIncomeFromContinuingOperations,
((CPLS.NetIncomeFromContinuingOperations * 1.00)/ NullIf(LAG(CPLS.NetIncomeFromContinuingOperations) Over (Order By CPLS.[Year]), 0) -1) * 100 As NetIncomeFromContinuingOperationsGrowthPercentage,
           (CPLS.NetIncomeFromContinuingOperations / NULLIF(CPLS.NetIncomeFromContinuingOperations, 0)) * 100  As NetIncomeFromContinuingOperationsMarginPercentage,
		   (select (cast(CBS.TotalAssets as dec) - cast(CBS.TotalLiabilities as dec) - CAST(CBS.IntangibleAssets as dec) ) as tangiblenetworth ),
		   	   (select (cast(CBS.TotalAssets as dec) - cast(CBS.TotalCurrentLiabilities as dec) ) as CapitalEmployed )
		

		  From @Years Y Left Join CompanyPLStatement CPLS On Y.[Year] = CPLS.[Year] And CPLS.CompanyId = @CompanyID
		  inner join CompanyBalanceSheet CBS On Y.[Year] = CBS.[Year] And CBS.CompanyId = CPLS.CompanyId
	Order By Y.[Year] 

	Update @CICompanyFinancialAnalysisSummaryData Set
		CashAndCashEquivalents = [dbo].[fun_UnitDeConversion](@UnitId, CBS.CashAndCashEquivalents),
		ShortTermInvestments =  [dbo].[fun_UnitDeConversion](@UnitId, CBS.ShortTermInvestments),
		TotalCurrentAssets = [dbo].[fun_UnitDeConversion](@UnitId, CBS.TotalCurrentAssets),
		ShortTermDebt = [dbo].[fun_UnitDeConversion](@UnitId, CBS.ShortTermDebt),
		LongTermDebt = [dbo].[fun_UnitDeConversion](@UnitId, CBS.LongTermDebt)
	From @CICompanyFinancialAnalysisSummaryData T Inner Join CompanyBalanceSheet CBS On T.CompanyId = CBS.CompanyId And T.[Year] = CBS.[Year]
	
	Update @CICompanyFinancialAnalysisSummaryData Set
		NetCashProvidedByOrUsedInOperatingActivities = [dbo].[fun_UnitDeConversion](@UnitId, CCFS.NetCashProvidedByOrUsedInOperatingActivities),
		NetCashProvidedByOrUsedInInvestingActivities = [dbo].[fun_UnitDeConversion](@UnitId, CCFS.NetCashProvidedByOrUsedInInvestingActivities),
		NetCashProvidedByOrUsedInFinancingActivities = [dbo].[fun_UnitDeConversion](@UnitId, CCFS.NetCashProvidedByOrUsedInFinancingActivities)
	From @CICompanyFinancialAnalysisSummaryData T Inner Join CompanyCashFlowStatement CCFS On T.CompanyId = CCFS.CompanyId And T.[Year] = CCFS.[Year]
		
	/*
	Select * From CompanyBalanceSheet
	Select * From CompanyCashFlowStatement
	*/
	Update @CICompanyFinancialAnalysisSummaryData
	Set GrowthPercentage = Null
	Where [Year] = @MinYear

	Select top 7  CompanyID, [Year], Revenue, GrowthPercentage, GrossProfit,GrossGrowthPercentage, MarginPercentage, EBIT,EBITGrowthPercentage, EBITMarginPercentage,
		   EBITDA,EBITDAGrowthPercentage, EBITDAMarginPercentage, NetIncome, NetIncomeGrowthPercentage,NetIncomeMarginPercentage, DilutedEPS,
		   CashAndCashEquivalents, ShortTermInvestments, TotalCurrentAssets, ShortTermDebt, LongTermDebt, 
		   NetCashProvidedByOrUsedInOperatingActivities, NetCashProvidedByOrUsedInInvestingActivities, NetCashProvidedByOrUsedInFinancingActivities,
		   NetIncomeFromContinuingOperations,NetIncomeFromContinuingOperationsGrowthPercentage,NetIncomeFromContinuingOperationsMarginPercentage,TangibleNetWorth,CapitalEmployed
	From @CICompanyFinancialAnalysisSummaryData
	Order By [Year] Desc 

	--Select CompanyID, FinacialTerm, sum(D2016) As Year1, sum(D2017) As Year2, sum(D2018) As Year3
	--From (
	--	Select 'd' + Convert (Char(10), [Year], 112) As Year1, Amount As Amount1, * 
	--	From @CIFinancialAnalysisSummaryTabData As T
	--	UNPIVOT 
	--	(
	--		Amount for FinacialTerm In (Revenue, GrowthPercentage, GrossProfit)
	--	) as a
	--) As B
	--Pivot
	--(
	--	Sum(Amount1) For Year1 In (d2016, d2017, d2018)
	--) as C
	--group by CompanyId, FinacialTerm

END