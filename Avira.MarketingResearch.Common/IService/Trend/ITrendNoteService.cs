﻿using Avira.MarketingResearch.Model.ViewModels.Trend;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.Trend
{
    public interface ITrendNoteService
    {
        TrendNoteViewModel GetById(Guid guid, string trendType);
        IEnumerable<TrendNoteViewModel> GetAllByTrendId(Guid guid ,string StatusName);
    }
}
