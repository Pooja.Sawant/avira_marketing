﻿CREATE TYPE [dbo].[udtGUIDCompanySwotThreatMultiValue] AS TABLE (
    [Id]              UNIQUEIDENTIFIER NOT NULL,
    [CompanyId]       UNIQUEIDENTIFIER NULL,
    [ThreatsCategory] UNIQUEIDENTIFIER NULL,
    [Description]     NVARCHAR (512)   NULL);

