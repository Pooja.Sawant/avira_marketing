﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanySegmentInformationController : AviraBaseController
    {
        private readonly ICompanySegmentInformationService _iCompanySegmentInformationService;
        private ILoggerManager _logger;

        public CompanySegmentInformationController(
           ICompanySegmentInformationService iCompanySegmentInformationService,
            ILoggerManager logger)
        {
            _iCompanySegmentInformationService = iCompanySegmentInformationService;
            _logger = logger;

        }


        [HttpGet("{id}")]
        public ActionResult<IEnumerable<CompanySegmentInformationViewModel>> GetCompanySegmentInformation(Guid Id)
        {
            List<CompanySegmentInformationViewModel> res = null;
            AviraUser user = new AviraUser();
            try
            {
                res = _iCompanySegmentInformationService.GetCompanySegmentInformationData(Id, user).ToList();

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);

            }
            return res;
        }
    }
}
