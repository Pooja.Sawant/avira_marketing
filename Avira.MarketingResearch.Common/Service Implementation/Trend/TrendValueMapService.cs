﻿using Avira.MarketingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.Trend
{
    public class TrendValueMapService : ITrendValueMapService
    {
        private readonly ITrendValueMapRepository _trendValueRepository;
        private readonly ITrendNoteRepository _iTrendNoteRepository;

        public TrendValueMapService(ITrendValueMapRepository trendValueMapRepository, ITrendNoteRepository iTrendNoteRepository)
        {
            _trendValueRepository = trendValueMapRepository;
            _iTrendNoteRepository = iTrendNoteRepository;
        }

        public IEnumerable<TrendValueViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false, string id = "")
        {
            return _trendValueRepository.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude = false, id);
        }

        public IEnumerable<ValueConversionViewModel> GetAllValueConversion()
        {
            return _trendValueRepository.GetAllValueConversion();
        }

        public IEnumerable<ImportanceViewModel> GetAllImportance()
        {
            return _trendValueRepository.GetAllImportance();
        }
        public IEnumerable<ImpactDirectionViewModel> GetAllImpactDirection()
        {
            return _trendValueRepository.GetAllImpactDirection();
        }

        public IEnumerable<TrendValuePreviewViewModel> GetById(Guid Id)
        {
            return _trendValueRepository.GetById(Id);
        }

        public Result<SpTransactionMessage> InsertUpdate(MainTrendValueInsertUpdateDbViewModel tfc, AviraUser currenUser)
        {

            var res = _trendValueRepository.InsertUpdate(tfc);
            if (res.IsSuccess == true)
            {
                tfc.TrendNoteModel.Id = Guid.NewGuid();
                tfc.TrendNoteModel.UserCreatedById = currenUser.Id;
                tfc.TrendNoteModel.TrendId = tfc.TrendId;
                _iTrendNoteRepository.Insert(tfc.TrendNoteModel);
            }
            //tfc.TrendNoteModel.Id = Guid.NewGuid();
            //tfc.TrendNoteModel.UserCreatedById = tfc.AviraUserId;
            //tfc.TrendNoteModel.TrendId = tfc.TrendId;
            //_iTrendNoteRepository.Insert(tfc.TrendNoteModel);

            return res;
        }
    }
}
