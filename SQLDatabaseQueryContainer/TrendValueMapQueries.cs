﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public class TrendValueMapQueries
    {
        public static readonly string StoreProcInsertUpdate = "SApp_InsertUpdateTrendValueMap";

        public static string StoreProcSelectAll = "SApp_GetValueByTrendID";

        public static string GetAllValueConversion = "SApp_GetConversionValues";

        public static string GetAllImportance = "SApp_GetAllImportance";

        public static string GetAllImpactDirection = "SApp_GetAllImpactDirection";

        public static string GetById = "SApp_GetTrendValuePreviewbyTrendID"; 
    }
}
