﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.CompanyProfile
{
    public class CompanyOtherInfoRepository : ICompanyOtherInfoRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "CompanyOtherInfo";

        public CompanyOtherInfoRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public IEnumerable<CompanyOtherInfoInsertUpdateViewModel> GetAll(Guid? id)
        {

            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<CompanyOtherInfoInsertUpdateViewModel>(CompanyOtherInfoQueries.StoreProcSelectAll
                            , new {  @CompanyId = id }
                            , null, true, null, CommandType.StoredProcedure);
        }

        public Result<SpTransactionMessage> InsertUpdate(MainCompanyOtherInfoInsertUpdateViewModel maincompanyOtherInfoInsertUpdateViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(CompanyOtherInfoQueries.StoreProcInsertUpdate, new
            {
                @CompanyId = maincompanyOtherInfoInsertUpdateViewModel.CompanyId,

                @jsonOtherInfo = maincompanyOtherInfoInsertUpdateViewModel.jsonOtherInfo,

                @UserCreatedById = maincompanyOtherInfoInsertUpdateViewModel.UserCreatedById
            }, CommandType.StoredProcedure);
            return spTransactionMessage;

        }

    }
}
