﻿
CREATE procedure [dbo].[SApp_insertService]    
(    
@ServiceName nvarchar(256),    
@Description nvarchar(256),  
@ParentServiceId uniqueidentifier,  
@UserCreatedById uniqueidentifier,  
@Id uniqueidentifier,  
@CreatedOn Datetime
)    
as    
/*================================================================================    
Procedure Name: SApp_insertService    
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
03/26/2019 Harshal   Initial Version    
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;    
declare @ErrorMsg NVARCHAR(2048)
declare @SegmentId uniqueidentifier;
select @SegmentId = Id from Segment
where SegmentName = 'Services';
  
--Validations    
IF exists(select 1 from [dbo].SubSegment     
   where SubSegmentName = @ServiceName
   and SegmentId = @SegmentId
   		 --and (ParentId = @ParentServiceId or (ParentId is null and @ParentServiceId is null))
 AND IsDeleted = 0)    
begin    
--declare @ErrorMsg NVARCHAR(2048);    
set @ErrorMsg = 'Service with Name "'+ @ServiceName + '" already exists.';    
--THROW 50000,@ErrorMsg,1;    
--return(1)    
SELECT StoredProcedureName ='SApp_insertService',Message =@ErrorMsg,Success = 0;  
RETURN;  
end    
--End of Validations    
BEGIN TRY       
       
insert into [dbo].SubSegment([Id],  
         SegmentId,  
        SubSegmentName,    
        [Description], 
		[ParentId],   
        [IsDeleted],    
        [UserCreatedById],    
        [CreatedOn])    
 values(@Id,  
   @SegmentId, 
   @ServiceName,     
   @Description, 
   @ParentServiceId,    
   0,    
   @UserCreatedById,     
   @CreatedOn);    
    

SELECT StoredProcedureName ='SApp_insertService',Message =@ErrorMsg,Success = 1;     
  
END TRY    
BEGIN CATCH    
    -- Execute the error retrieval routine.    
 DECLARE @ErrorNumber int;    
 DECLARE @ErrorSeverity int;    
 DECLARE @ErrorProcedure varchar(100);    
 DECLARE @ErrorLine int;    
 DECLARE @ErrorMessage varchar(500);    
    
  SELECT @ErrorNumber = ERROR_NUMBER(),    
        @ErrorSeverity = ERROR_SEVERITY(),    
        @ErrorProcedure = ERROR_PROCEDURE(),    
        @ErrorLine = ERROR_LINE(),    
        @ErrorMessage = ERROR_MESSAGE()    
    
 insert into dbo.Errorlog(ErrorNumber,    
       ErrorSeverity,    
       ErrorProcedure,    
       ErrorLine,    
       ErrorMessage,    
       ErrorDate)    
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,@CreatedOn);    
  
 set @ErrorMsg = 'Error while creating a new Service, please contact Admin for more details.';    
--THROW 50000,@ErrorMsg,1;      
--return(1)  
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;     
     
END CATCH    
    
    
end