Create PROCEDURE [dbo].[SFab_CheckIfCurrencyConversionRatesAvailable]    
(
	@InCurrencyId UNIQUEIDENTIFIER=NULL, 
	@OutCurrencyId UNIQUEIDENTIFIER, 
	@TransactionDate Datetime = NULL
)
 AS  
/*================================================================================  
Procedure Name: [SFab_CheckIfCurrencyConversionRatesAvailable]  
Author: Nitin  
Create date: 30-Aug-2019  
Description: Function to check if currency conversion rates are avialable
Change History  
Date        Developer     Reason  
__________  ____________  ______________________________________________________  
30-Aug-2019 Nitin Tarkar   Initial Version  
================================================================================*/   
BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
	
	Select [dbo].[fun_IsCurrencyConversionRatesAvailable](@InCurrencyId, @OutCurrencyId, @TransactionDate) AS IsCurrencyConversionRatesAvailable

END  