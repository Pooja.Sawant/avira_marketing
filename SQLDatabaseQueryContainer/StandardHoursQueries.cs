﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public class StandardHoursQueries
    {

        public static readonly string StoreProcUpdate = "SApp_UpdateStandardHours";

        public static string StoreProcSelectAll = "SApp_StandardHoursGridWithFilters";
    }
}
