﻿using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using static Avira.MarketingResearch.Model.ViewModels.ApplicationViewModel;

namespace Avira.MarketingResearch.Common.Service_Implementation
{
    public class ApplicationService: IApplicationService
    {
        private readonly IApplicationRepository _applicationRepository;

        public ApplicationService(IApplicationRepository iApplicationRepository)
        {
            _applicationRepository = iApplicationRepository;
        }

        public Result<SpTransactionMessage> Create(ApplicationInsertDbViewModel tfc, AviraUser currentUser)
        {
            return _applicationRepository.Create(tfc);
        }

        public IEnumerable<ApplicationViewModel> GetAll(Guid Id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ApplicationViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false)
        {
            return _applicationRepository.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName,Search, isDeletedInclude);
        }

        public ApplicationViewModel GetById(Guid guid)
        {
            return _applicationRepository.GetById(guid);
        }

        public Result<SpTransactionMessage> Update(ApplicationUpdateDbViewModel tfc, AviraUser currenUser)
        {
            return _applicationRepository.Update(tfc);
        }
    }
}
