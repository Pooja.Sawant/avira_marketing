﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Repository.CoreInterfaces.ProjectDashBoard;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.ProjectDashBoard
{
    public class ProjectSegmentMappingRepository : IProjectSegmentMappingRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "ProjectSegmentMapping";
        public ProjectSegmentMappingRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }


        public ProjectSegmentMappingViewModel GetById(Guid guid)
        {
            ProjectSegmentMappingViewModel projectSegmentMappingViewModel = new ProjectSegmentMappingViewModel();
            projectSegmentMappingViewModel = GetAllProjectSegmentMapping(guid).FirstOrDefault();
            return projectSegmentMappingViewModel;
        }

        public IEnumerable<ProjectSegmentMappingViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted)
        {
            return GetAllProjectSegmentMapping(null, PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, includeDeleted);
        }

        private IEnumerable<ProjectSegmentMappingViewModel> GetAllProjectSegmentMapping(Guid? guid, int PageStart = 0, int pageSize = 0, int SortDirection = -1, string OrdbyByColumnName = "", string Search = "", bool includeDeleted = false)
        {

            DataTable projectSegmentMappingIdsTable = new DataTable();
            projectSegmentMappingIdsTable.Columns.Add("ID", typeof(Guid));

            if (guid != null)
            {
                projectSegmentMappingIdsTable.Rows.Add(guid);
            }
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<ProjectSegmentMappingViewModel>(ProjectSegmentMappingQueries.StoreProcSelectAll
                            , new { @ProjectSegmentMappingIDList = projectSegmentMappingIdsTable.AsTableValuedParameter("dbo.udtUniqueIdentifier"), @includeDeleted = includeDeleted, @PageStart = PageStart, @pageSize = pageSize, @SortDirection = SortDirection, @OrdbyByColumnName = OrdbyByColumnName, @SegmentType = Search }
                            , null, true, null, CommandType.StoredProcedure);
        }

        public Result<SpTransactionMessage> Create(ProjectSegmentMappingInsertDbViewModel projectSegmentMappingInsertDbViewModel)
        {
            DataTable AnalystUserIDLists = new DataTable();
            AnalystUserIDLists.Columns.Add("ID", typeof(Guid));
            if (projectSegmentMappingInsertDbViewModel != null)
            {

                if (projectSegmentMappingInsertDbViewModel.SegmentID != null)
                {
                    projectSegmentMappingInsertDbViewModel.UserIDs = projectSegmentMappingInsertDbViewModel.SegmentID.ToArray();
                }
            }
            if (projectSegmentMappingInsertDbViewModel.UserIDs != null)
            {
                for (int i = 0; i < projectSegmentMappingInsertDbViewModel.UserIDs.Count(); i++)
                {
                    AnalystUserIDLists.Rows.Add(new Object[] { projectSegmentMappingInsertDbViewModel.UserIDs[i] });
                }
            }
            var spTransactionMessage = _dbHelper.ExecuteQuery(ProjectSegmentMappingQueries.StoreProcInsert,
                 new
                 {                   
                     @ProjectID = projectSegmentMappingInsertDbViewModel.ProjectID,
                     @SegmentType = projectSegmentMappingInsertDbViewModel.SegmentType,                   
                     @SegmentIDlist = AnalystUserIDLists,
                     @UserCreatedById = projectSegmentMappingInsertDbViewModel.UserCreatedById
                 },
                CommandType.StoredProcedure);
            return spTransactionMessage;        

        }

        public Result<SpTransactionMessage> CreateProjectCode(ProjectSegmentMappingInsertDbViewModel projectSegmentMappingInsertDbViewModel)
        {            
            
            var spTransactionMessage = _dbHelper.ExecuteQuery(ProjectSegmentMappingQueries.StoreProcUpdatePCode,
                 new
                 {                    
                     @ProjectID = projectSegmentMappingInsertDbViewModel.ProjectID
                 },
                CommandType.StoredProcedure);
            return spTransactionMessage;        

        }
        public Result<SpTransactionMessage> Update(ProjectSegmentMappingUpdateDbViewModel projectSegmentMappingUpdateDbViewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(ProjectSegmentMappingQueries.StoreProcUpdate, projectSegmentMappingUpdateDbViewModel, CommandType.StoredProcedure);
            return spTransactionMessage;
        }


    }
}
