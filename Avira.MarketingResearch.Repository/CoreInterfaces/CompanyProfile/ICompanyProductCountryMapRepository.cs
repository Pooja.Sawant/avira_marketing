﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using static Avira.MarketingResearch.Model.ViewModels.CompanyProfile.CompanyProductCountryMapViewModel;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile
{
    public interface ICompanyProductCountryMapRepository
    {
        IEnumerable<CompanyProductCountryMapViewModel> GetAll(Guid? Id);
        Result<SpTransactionMessage> InsertUpdate(CompanyProductViewMoel companyProductViewMoel);
    }
}
