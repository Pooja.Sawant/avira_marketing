﻿

 CREATE PROCEDURE [dbo].[SApp_GetTrendAllTabsStatusbyTrendID]          
 (    
  @TrendID uniqueidentifier = null,
  @TrendStatusName VARCHAR(512)=null
 )          
 AS          
/*================================================================================          
Procedure Name: SApp_GetTrendAllTabsStatusbyTrendID    
Author: Harshal          
Create date: 08/13/2019          
Description: Get trend Mapping Status of by TrendID        
Change History          
Date  Developer  Reason          
__________ ____________ ______________________________________________________          
08/13/2019 Harshal   Initial Version 
================================================================================*/         
          
BEGIN          
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
 if(@TrendStatusName IS NULL)
   Begin
      SELECT Upper(Trim(TrendType))AS TrendType,TrendStatusName,AuthorRemark,ApproverRemark FROM TrendNote
      INNER JOIN TrendStatus ON TrendNote.TrendStatusID=TrendStatus.Id
      WHERE TrendId = @TrendID 
   End
Else
  Begin
 
  SELECT Upper(Trim(TrendType)) AS TrendType,TrendStatusName,AuthorRemark,ApproverRemark FROM TrendNote
  INNER JOIN TrendStatus ON TrendNote.TrendStatusID=TrendStatus.Id
  WHERE TrendId = @TrendID AND TrendStatusID =(SELECT Id FROM TrendStatus where TrendStatusName = @TrendStatusName)    

   End   
END