﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile
{
    public interface ICompanyProfitAndLossRepository
    {
        IEnumerable<CompanyProfitAndLossViewModel> GetCompanyProfitAndLossData(Guid CompanyId, AviraUser user);
    }
}
