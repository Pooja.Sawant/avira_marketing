﻿CREATE TABLE [dbo].[CompanyClientsMapping] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [CompanyId]        UNIQUEIDENTIFIER NULL,
    [ClientName]       NVARCHAR (256)   NULL,
    [ClientIndustry]   UNIQUEIDENTIFIER NULL,
    [ClientRemark]     NVARCHAR (512)   NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CompanyClientsMapping] PRIMARY KEY CLUSTERED ([Id] ASC)
);



