﻿CREATE PROCEDURE [dbo].[SFab_GetESMCompanyList]    
(  
	@ProjectId uniqueidentifier, 
	@SegmentId uniqueidentifier,
	@RecordCount int = 0,
	@SubSegmentIdList [dbo].[udtUniqueIdentifier] READONLY,--Optional record 0 ... list
	@AviraUserID uniqueidentifier = null
)
As    
-------Test Data
--Declare	@ProjectId uniqueidentifier = '3386de60-4720-43da-9f9a-83a95406aad2', 
--	@SegmentId uniqueidentifier = '328B5BE8-D1AE-469E-86BE-C9A1950705AD',
--	@SubSegmentIdList [dbo].[udtUniqueIdentifier], --Optional record 0 ... list
--	@AviraUserID uniqueidentifier = null

	--Insert Into @SubSegmentIdList (ID) Select 'D69B6108-037D-4D8E-8D70-AD59AB469C34'

/*================================================================================    
Procedure Name: SFab_GetESMCompanyList   
Author: Praveen    
Create date: 12/09/2019    
Description: To return all companies which are mapped with ecosystem mapping based on the segmentId and projectId. And optional imput values for subsegmentIds
Change History    
Date  Developer  Reason  
__________ ____________ ______________________________________________________    
12/09/2019  Praveen   Initial Version    
10-Dec-2019 Nitin	  Store procedure implementation
================================================================================*/    
BEGIN    
------------------

	--Required Tables: ESMSegmentMapping,QualitativeSegmentMapping, CompanyEcosystemPresenceESMMap and Company
	--Notes: in the CompanyEcosystemPresenceESMMap.ESMId is the placeholder for SegmentID from Segment Table
	--Case 1: if @SubSegmentIdList is not null then it should return data based on the segmentID and SubSegmentIdList for particular project
	--Case 2: if @SubSegmentIdList is null then it should return data based on the segmentID level for particular project
	--Expected Columns : CompanyId and CompanyName	
	
	Declare @CheckSubsegments bit = 0

	If exists(Select 1 From @SubSegmentIdList)
	Begin
		
		Set @CheckSubsegments = 1
	End

	If (@RecordCount > 0)
	Begin

		Select Distinct Top(@RecordCount) C.Id As CompanyId, C.CompanyName,
			(Select NatureTypeName From Nature Where Id = C.NatureId) As NatureTypeName,
			case when  C.IsHeadQuarters=1 then C.CompanyLocation else '' end AS CompanyHeadQuarters,  
			STUFF((SELECT ', '+ Region.RegionName  FROM CompanyRegionMap  
			   INNER JOIN Region ON Region.Id=CompanyRegionMap.RegionId  
				  WHERE CompanyRegionMap.CompanyId=C.Id  
			   FOR XML PATH('')), 1, 1, '') AS GeographicPresence
		From Company C
			Inner Join CompanyProjectMap CPM On
				C.Id = CPM.CompanyId And
				CPM.ProjectId = @ProjectId 
			Inner Join CompanyEcosystemSegmentMap CESM On 
				C.Id = CESM.CompanyId And
				CESM.SegmentId = @SegmentId					
			Inner Join QualitativeSegmentMapping QESM On
				CESM.SegmentId = QESM.SegmentId And
				QESM.ProjectId = @ProjectId
		Where (@CheckSubsegments = 0) Or 
				(@CheckSubsegments = 1 And CESM.SubSegmentId Is Null) Or
				(@CheckSubsegments = 1 And CESM.SubSegmentId Is Not Null And CESM.SubSegmentId In (Select Id From @SubSegmentIdList))
		Order By C.CompanyName 
	End
	Else
	Begin

		Select Distinct C.Id As CompanyId, C.CompanyName,
			(Select NatureTypeName From Nature Where Id = C.NatureId) As NatureTypeName,
			case when  C.IsHeadQuarters=1 then C.CompanyLocation else '' end AS CompanyHeadQuarters,  
			STUFF((SELECT ', '+ Region.RegionName  FROM CompanyRegionMap  
			   INNER JOIN Region ON Region.Id=CompanyRegionMap.RegionId  
				  WHERE CompanyRegionMap.CompanyId=C.Id  
			   FOR XML PATH('')), 1, 1, '') AS GeographicPresence
		From Company C
			Inner Join CompanyProjectMap CPM On
				C.Id = CPM.CompanyId And
				CPM.ProjectId = @ProjectId 
			Inner Join CompanyEcosystemSegmentMap CESM On 
				C.Id = CESM.CompanyId And
				CESM.SegmentId = @SegmentId					
			Inner Join QualitativeSegmentMapping QESM On
				CESM.SegmentId = QESM.SegmentId And
				QESM.ProjectId = @ProjectId
		Where (@CheckSubsegments = 0) Or 
				(@CheckSubsegments = 1 And CESM.SubSegmentId Is Null) Or
				(@CheckSubsegments = 1 And CESM.SubSegmentId Is Not Null And CESM.SubSegmentId In (Select Id From @SubSegmentIdList))
		Order By C.CompanyName 
	End	
END