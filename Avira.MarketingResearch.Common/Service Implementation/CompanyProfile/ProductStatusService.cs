﻿using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.CompanyProfile
{
    public class ProductStatusService : IProductStatusService
    {
        private readonly IProductStatusRepository _iProductStatusRepository;

        public ProductStatusService(IProductStatusRepository iProductStatusRepository)
        {
            _iProductStatusRepository = iProductStatusRepository;
        }

        public IEnumerable<ProductStatusViewModel> GetAll()
        {
            return _iProductStatusRepository.GetAll();
        }
    }
}
