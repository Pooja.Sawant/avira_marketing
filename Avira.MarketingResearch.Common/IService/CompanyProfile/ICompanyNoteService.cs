﻿using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.CompanyProfile
{
    public interface ICompanyNoteService
    {
        CompanyNoteViewModel GetById(Guid companyId,string trendType,Guid? ProjectId);
    }
}
