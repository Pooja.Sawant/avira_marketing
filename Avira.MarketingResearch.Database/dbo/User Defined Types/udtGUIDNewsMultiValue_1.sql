﻿CREATE TYPE [dbo].[udtGUIDNewsMultiValue] AS TABLE (
    [ID]                UNIQUEIDENTIFIER NOT NULL,
    [Date]              DATETIME         NOT NULL,
    [News]              NVARCHAR (512)   NOT NULL,
    [NewsCategoryId]    UNIQUEIDENTIFIER NOT NULL,
    [ImportanceId]      UNIQUEIDENTIFIER NOT NULL,
    [ImpactDirectionId] UNIQUEIDENTIFIER NOT NULL,
    [OtherParticipants] NVARCHAR (128)   NULL,
    [Remarks]           NVARCHAR (128)   NULL);



