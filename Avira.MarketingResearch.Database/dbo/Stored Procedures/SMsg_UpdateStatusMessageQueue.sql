﻿CREATE procedure [dbo].[SMsg_UpdateStatusMessageQueue]    
(@Id uniqueidentifier,    
@StatusName nvarchar(50),
@UserModifiedById  uniqueidentifier = null
)
as    
/*================================================================================    
Procedure Name: SMsg_UpdateStatusMessageQueue    
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
05/13/2019 Praveen   Initial Version  
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
DECLARE @StatusId uniqueidentifier
declare @ErrorMsg NVARCHAR(2048);
BEGIN TRY     
--update record    

SELECT @StatusId = Id FROM MessageStatus WHERE StatusName = @StatusName

update [dbo].[Message]  
set StatusId = @StatusId,
 [ModifiedOn] = GETDATE()    
 --[UserModifiedById] = @UserModifiedById    
    where ID = @Id   
   
SELECT StoredProcedureName ='SMsg_UpdateStatusMessageQueue',Message = @ErrorMsg,Success = 1;     
  
END TRY    
BEGIN CATCH    
    -- Execute the error retrieval routine.    
 DECLARE @ErrorNumber int;    
 DECLARE @ErrorSeverity int;    
 DECLARE @ErrorProcedure varchar;    
 DECLARE @ErrorLine int;    
 DECLARE @ErrorMessage varchar;    
    
  SELECT @ErrorNumber = ERROR_NUMBER(),    
        @ErrorSeverity = ERROR_SEVERITY(),    
        @ErrorProcedure = ERROR_PROCEDURE(),    
        @ErrorLine = ERROR_LINE(),    
        @ErrorMessage = ERROR_MESSAGE()    
    
 insert into dbo.Errorlog(ErrorNumber,    
       ErrorSeverity,    
       ErrorProcedure,    
       ErrorLine,    
       ErrorMessage,    
       ErrorDate)    
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE()); 

set @ErrorMsg = 'Error while updating Message Status, please contact Admin for more details.' 		   
 --set @ErrorMsg = 'Unspecified Error';    
--THROW 50000,@ErrorMsg,1;    
 --return(1)  
 SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;     
END CATCH    
    
  
  
    
end