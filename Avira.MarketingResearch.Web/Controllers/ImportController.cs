﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.Web.ImportExportUtility;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OfficeOpenXml;
using Syncfusion.XlsIO;

namespace Avira.MarketingResearch.Web.Controllers
{
    [NoCache]
    [Authorization]
    public class ImportController : BaseController
    {
        public readonly IServiceRepository _serviceRepository;
        private ILoggerManager _logger;
        public ImportController(IServiceRepository serviceRepository, ILoggerManager logger)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
        }
        public IActionResult Index(ImportResponse list)
        {
            if (list == null)
            {
                list = new ImportResponse();
            }
            ViewBag.result = list;
            return View(list);
        }

        public IActionResult Image(ImportResponse list)
        {

            if (list == null)
            {
                list = new ImportResponse();
            }
            ViewBag.result = list;
            return View(list);
        }

        public IActionResult ImageDelete(ImportResponse list)
        {

            if (list == null)
            {
                list = new ImportResponse();
            }
            ViewBag.result = list;
            return View(list);
        }
        [HttpPost]
        public IActionResult Index(IFormFile formFile, string importTemplate)
        {
            var res = new ImportResponse();
            if (formFile == null || formFile.Length <= 0)
            {
                ViewBag.Msg = "formfile is empty";
                return View();
            }

            if (!Path.GetExtension(formFile.FileName).Equals(".xlsx", StringComparison.OrdinalIgnoreCase))
            {
                ViewBag.Msg = "Not Support file extension";
                return View();
            }

            IExcelImport objIExcelImport = null;
            switch (importTemplate)
            {
                case "User":
                    objIExcelImport = new ImportUser();
                    res = objIExcelImport.Import(formFile);
                    break;
                case "Trend":
                    objIExcelImport = new ImportTrend(_serviceRepository, _logger);

                    res = objIExcelImport.Import(formFile);
                    res.ImportFor = "Trend";
                    //if (res.ResponseMessage == "Import Trend have some errors")
                    //{
                    //    //Create an instance of ExcelEngine
                    //    ViewBag.result = res;
                    //    //var trendExport = TrendExport(res.GetTrendErrorList);
                    //    List<TrendImportViewModel> model = res.GetTrendErrorList;
                    //    ViewBag.TrendErrorList = JsonConvert.SerializeObject(model);
                    //    ViewBag.Base64TrendString = CreateTrendExcel(model);
                    //    return View();
                    //    //return trendExport;
                    //}
                    //if (res.ResponseMessage == "Trend Details successfully Imported")
                    //{
                    //    ViewBag.result = res;
                    //    return View();
                    //}
                    //if (res.ResponseMessage == "Please upload proper excel file")
                    //{
                    //    ViewBag.result = res;
                    //    return View();
                    //}
                    if (res.ResponseStatus == "Failed")
                    {
                        ViewBag.result = res;
                        //var model = res.ResponseModel;
                        // ViewBag.TrendErrorList = JsonConvert.SerializeObject(model);
                        ViewBag.Base64TrendString = CreateTrendExcel(res.ResponseModel.ToList());
                        return View();

                    }
                    else if (res.ResponseStatus == "Error")
                    {
                        res.TotalSuccessRecords = 0;
                        res.TotalFailedRecords = res.TotalRecords;
                        ViewBag.result = res;
                        return View();
                    }
                    else
                    {
                        res.TotalSuccessRecords = res.TotalRecords;
                        res.TotalFailedRecords = 0;
                        ViewBag.result = res;
                        return View();
                    }
                    break;
                case "Company Basic Info":
                    objIExcelImport = new ImportCompanyBasicInfo(_serviceRepository, _logger);

                    res = objIExcelImport.Import(formFile);
                    res.ImportFor = "CPBI";
                    
                    if (res.ResponseStatus == "Failed")
                    {
                        //var cpExport = CPIBExport(res.ResponseModel.ToList());
                        //return cpExport;
                        ViewBag.result = res;
                        ViewBag.Base64TrendString = CPIBExport(res.ResponseModel.ToList());
                        return View();

                    }
                    else if (res.ResponseStatus == "Error")
                    {
                        res.TotalSuccessRecords = 0;
                        res.TotalFailedRecords = res.TotalRecords;
                        ViewBag.result = res;
                        return View();
                    }
                    else                    
                    {
                        res.TotalSuccessRecords = res.TotalRecords;
                        res.TotalFailedRecords = 0;
                        ViewBag.result = res;
                        return View();
                    }
                    break;
                case "CP_Fundamentals":
                    objIExcelImport = new ImportCP_Fundamentals(_serviceRepository, _logger);

                    res = objIExcelImport.Import(formFile);
                    res.ImportFor = "CPFA";

                    //if (res.ResponseMessage == "Import CompanyProfile have some errors")
                    if (res.ResponseStatus == "Failed")
                    {
                        //Create an instance of ExcelEngine
                        var cpExport = CPExport(res.GetCompanyProfileErrorList);
                        return cpExport;

                    }
                    else if (res.ResponseStatus == "Error")
                    {
                        ViewBag.result = res;
                        return View(res);
                    }
                    else
                    //if (res.ResponseMessage == "Import CompanyProfile successfully.")
                    {
                        ViewBag.result = res;
                        return View(res);
                    }
                    break;
                case "Company Profile":
                    objIExcelImport = new ImportCompanyProfile(_serviceRepository, _logger);

                    res = objIExcelImport.Import(formFile);
                    res.ImportFor = "CP";

                    //if (res.ResponseMessage == "Import CompanyProfile have some errors")
                    if (res.ResponseStatus == "Failed")
                    {
                        //Create an instance of ExcelEngine
                        var cpExport = CPExport(res.GetCompanyProfileErrorList);
                        return cpExport;

                    }
                    else if (res.ResponseStatus == "Error")
                    {
                        ViewBag.result = res;
                        return View(res);
                    }
                    else
                    //if (res.ResponseMessage == "Import CompanyProfile successfully.")
                    {
                        ViewBag.result = res;
                        return View(res);
                    }
                    break;
                case "CP Financial":
                    objIExcelImport = new ImportCPFinancial(_serviceRepository, _logger);

                    res = objIExcelImport.Import(formFile);
                    res.ImportFor = "CPF";
                    if (res.ResponseStatus == "Failed")
                    {
                        //Create an instance of ExcelEngine
                        ViewBag.result = res;
                        var model = res.ResponseModel;
                        return CPFinancialExport(model != null? model.ToList() : null);
                    }
                    else
                    {
                        ViewBag.result = res;
                        return View();
                    }
                    break;
                case "QualitativeAnalysis":
                    objIExcelImport = new ImportQualitativeAnalysis(_serviceRepository, _logger);

                    res = objIExcelImport.Import(formFile);
                    res.ImportFor = "QA";
                    if (res.ResponseStatus == "Failed")
                    {
                        //Create an instance of ExcelEngine
                        ViewBag.result = res;
                        //IEnumerable<ImportCommonResponseModel> model = res.ResponseModel == null ? new List<ImportCommonResponseModel>(): res.ResponseModel;
                        if (res.ResponseModel != null && res.ResponseModel.Count() > 0)
                        {
                            return QualitativeAnalysisExport(res.ResponseModel.ToList());
                        }
                        else
                        {
                            res.TotalSuccessRecords = 0;
                            res.TotalFailedRecords = res.TotalRecords;
                            ViewBag.result = res;
                            return View();
                        }
                       
                    }
                    else
                    {
                        res.TotalSuccessRecords = 0;
                        res.TotalFailedRecords = res.TotalRecords;
                        ViewBag.result = res;
                        return View();
                    }
                    break;
                case "Market Sizeing":
                    objIExcelImport = new ImportMarketSizing(_serviceRepository, _logger);
                    res = objIExcelImport.Import(formFile);
                    res.ImportFor = "Market";
                    if (res.ResponseMessage == "Import marketSizing have some errors")
                    {
                        //Create an instance of ExcelEngine
                        //var marketSizeExport = MarketSizeExport(res.GetMarketSizeErrorList);
                        //return marketSizeExport;
                        ViewBag.result = res;
                        List<MarketSizingImportViewModel> model = res.GetMarketSizeErrorList;
                        //ViewBag.MarketSizingErrorList = JsonConvert.SerializeObject(model);

                        ViewBag.Base64TrendString = CreateMSExcel(model);


                        return View();
                    }
                    if (res.ResponseMessage == "MarketSizing Details successfully Imported")
                    {
                        ViewBag.result = res;
                        return View();
                    }
                    if (res.ResponseMessage == "Please upload proper excel file")
                    {
                        ViewBag.result = res;
                        return View();
                    }
                    break;

                case "Report Generation":
                    objIExcelImport = new ImportReportGeneration(_serviceRepository, _logger);
                    res = objIExcelImport.Import(formFile);
                    res.ImportFor = "RG";
                    if (res.ResponseStatus == "Failed")
                    {
                        ViewBag.result = res;
                        ViewBag.Base64TrendString = ReportGenerationExport(res.ResponseModel.ToList());
                        return View();
                    }
                    else
                    {
                        ViewBag.result = res;
                        return View();
                    }
                    break;
                case "Strategy Instance":
                    objIExcelImport = new ImportStrategyInstance(_serviceRepository, _logger);
                    res = objIExcelImport.Import(formFile);
                    res.ImportFor = "SI";
                    if (res.ResponseStatus == "Failed")
                    {
                        ViewBag.result = res;
                        ViewBag.Base64TrendString = StrategyInstanceExport(res.ResponseModel.ToList());
                        return View();
                    }
                    else
                    {
                        ViewBag.result = res;
                        return View();

                    }
                    break;
                case "CompanyBulkImport":
                    objIExcelImport = new CompanyBulkImport(_serviceRepository, _logger);

                    res = objIExcelImport.Import(formFile);
                    res.ImportFor = "CBI";

                    //if (res.ResponseMessage == "Import CompanyProfile have some errors")
                    if (res.ResponseStatus == "Failed")
                    {
                        //Create an instance of ExcelEngine
                        var cpExport = CBIExport(res.GetCompanyProfileErrorList);
                        return cpExport;

                    }
                    else if (res.ResponseStatus == "Error")
                    {
                        ViewBag.result = res;
                        return View(res);
                    }
                    else
                    //if (res.ResponseMessage == "Import CompanyProfile successfully.")
                    {
                        ViewBag.result = res;
                        return View(res);
                    }
                    break;
                default:
                    break; ;
            }

             return View(res);
        }

        [HttpPost]
        public ActionResult TrendExport([FromBody] List<TrendImportViewModel> model)
        {
            //List<TrendImportViewModel> model = new List<TrendImportViewModel>();
            string handle = Guid.NewGuid().ToString();
            return new JsonResult(new { FileGuid = handle, FileName = "TrendErrorsList.xlsx" });
            //return CreateTrendExcel(model);
        }

        private string CreateTrendExcel(List<ImportCommonResponseModel> model)
        {
            var inputAsString = string.Empty;
            using (ExcelEngine excelEngine = new ExcelEngine())
            {
                IApplication application = excelEngine.Excel;
                application.DefaultVersion = ExcelVersion.Excel2016;
                //Create a workbook
                IWorkbook workbook = application.Workbooks.Create(1);
                IWorksheet worksheet = workbook.Worksheets[0];

                worksheet.Range["A1"].Text = "Module";
                worksheet.Range["B1"].Text = "RowNo";
                worksheet.Range["C1"].Text = "ErrorNotes";

                int row = 2;
                for (int i = 0; i < model.Count; i++)
                {
                    worksheet.Range["A" + row].Text = model[i].Module.ToString();
                    worksheet.Range["B" + row].Text = model[i].RowNo.ToString();
                    worksheet.Range["C" + row].Text = model[i].ErrorNotes;

                    row++;
                }
                
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    workbook.SaveAs(memoryStream);
                    memoryStream.Position = 0;
                    //TempData[handle] = memoryStream.ToArray();
                    //HttpContext.Session.SetComplexData("ErrorData", memoryStream.ToArray());

                    inputAsString = Convert.ToBase64String(memoryStream.ToArray());
                    //Console.WriteLine(inputAsString);
                }

               

                //Download the Excel file in the browser
                //FileStreamResult fileStreamResult = new FileStreamResult(stream, "application/excel");
                //fileStreamResult.FileDownloadName = "TrendErrorsList.xlsx";
                //return fileStreamResult;
                //return PartialView("_TrendExcel", fileStreamResult);
            }
            return inputAsString;
        }

        [HttpGet]
        public virtual ActionResult Download(string fileGuid, string fileName)
        {
            if (fileGuid != null)
            {
                string errordata = HttpContext.Session.GetComplexData("ErrorData");
                byte[] data = Encoding.ASCII.GetBytes(errordata);
                return File(data, "application/vnd.ms-excel", fileName);
            }
            else
            {
                return new EmptyResult();
            }

            //if (TempData[fileGuid] != null)
            //{
            //    byte[] data = TempData[fileGuid] as byte[];
            //    return File(data, "application/vnd.ms-excel", fileName);
            //}
            //else
            //{
            //    // Problem - Log the error, generate a blank file,
            //    //           redirect to another controller action - whatever fits with your application
            //    return new EmptyResult();
            //}
        }

        [HttpPost]
        public ActionResult MarketSizeExport([FromBody] List<MarketSizingImportViewModel> model)
        {
            string handle = CreateMSExcel(model);
            return new JsonResult(new { FileGuid = handle, FileName = "MarketSizeErrorList.xlsx" });
        }

        private string CreateMSExcel(List<MarketSizingImportViewModel> model)
        {
            string handle = Guid.NewGuid().ToString();
            var inputAsString = string.Empty;
            using (ExcelEngine excelEngine = new ExcelEngine())
            {
                IApplication application = excelEngine.Excel;

                application.DefaultVersion = ExcelVersion.Excel2016;

                //Create a workbook
                IWorkbook workbook = application.Workbooks.Create(1);
                IWorksheet worksheet = workbook.Worksheets[0];

                worksheet.Range["B1"].Text = "Market";
                worksheet.Range["C1"].Text = "ErrorNotes";

                int row = 2;
                for (int i = 0; i < model.Count; i++)
                {
                    worksheet.Range["B" + row].Text = model[i].Market;
                    worksheet.Range["C" + row].Text = model[i].ErrorNotes;
                    row++;
                }

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    workbook.SaveAs(memoryStream);
                    memoryStream.Position = 0;
                    inputAsString = Convert.ToBase64String(memoryStream.ToArray());
                }


            }

            return inputAsString;
        }        

        public string CPIBExport(List<ImportCommonResponseModel> model)
        {
            var fileStreamResult = string.Empty;
            using (ExcelEngine excelEngine = new ExcelEngine())
            {
                IApplication application = excelEngine.Excel;
                application.DefaultVersion = ExcelVersion.Excel2016;
                //Create a workbook
                IWorkbook workbook = application.Workbooks.Create(1);
                IWorksheet worksheet = workbook.Worksheets[0];
                worksheet.Name = "Basics Error Response";
                worksheet.Range["A1"].Text = "RowNo";
                worksheet.Range["B1"].Text = "ErrorNotes";
              
                int row = 2;
                for (int i = 0; i < model.Count; i++)
                {
                    worksheet.Range["A" + row].Text = model[i].RowNo.ToString();
                    worksheet.Range["B" + row].Text = model[i].ErrorNotes;

                    row++;
                }
                //Saving the Excel to the MemoryStream 
                MemoryStream stream = new MemoryStream();
                workbook.SaveAs(stream);
                //Set the position as '0'.
                stream.Position = 0;
                fileStreamResult = Convert.ToBase64String(stream.ToArray());
                //Download the Excel file in the browser
                //FileStreamResult fileStreamResult = new FileStreamResult(stream, "application/excel");
                //fileStreamResult.FileDownloadName = "CompanyBasicInfoErrorsList.xlsx";
                return fileStreamResult;

            }
        }

        public ActionResult CPExport(List<ImportCommonResponseModel> model)
        {
            using (ExcelEngine excelEngine = new ExcelEngine())
            {
                IApplication application = excelEngine.Excel;
                application.DefaultVersion = ExcelVersion.Excel2016;
                //Create a workbook
                IWorkbook workbook = application.Workbooks.Create(1);
                IWorksheet worksheet = workbook.Worksheets[0];

                worksheet.Range["A1"].Text = "Module";
                worksheet.Range["B1"].Text = "RowNo";
                worksheet.Range["C1"].Text = "ErrorNotes";

                int row = 2;
                for (int i = 0; i < model.Count; i++)
                {
                    worksheet.Range["A" + row].Text = model[i].Module;
                    worksheet.Range["B" + row].Text = model[i].RowNo.ToString();
                    worksheet.Range["C" + row].Text = model[i].ErrorNotes;

                    row++;
                }
                //Saving the Excel to the MemoryStream 
                MemoryStream stream = new MemoryStream();
                workbook.SaveAs(stream);
                //Set the position as '0'.
                stream.Position = 0;
                //Download the Excel file in the browser
                FileStreamResult fileStreamResult = new FileStreamResult(stream, "application/excel");
                fileStreamResult.FileDownloadName = "CompanyProfileErrorsList.xlsx";
                return fileStreamResult;

            }
        }

        public ActionResult CBIExport(List<ImportCommonResponseModel> model)
        {
            using (ExcelEngine excelEngine = new ExcelEngine())
            {
                IApplication application = excelEngine.Excel;
                application.DefaultVersion = ExcelVersion.Excel2016;
                //Create a workbook
                IWorkbook workbook = application.Workbooks.Create(1);
                IWorksheet worksheet = workbook.Worksheets[0];

                worksheet.Range["A1"].Text = "Module";
                worksheet.Range["B1"].Text = "RowNo";
                worksheet.Range["C1"].Text = "ErrorNotes";

                int row = 2;
                for (int i = 0; i < model.Count; i++)
                {
                    worksheet.Range["A" + row].Text = model[i].Module;
                    worksheet.Range["B" + row].Text = model[i].RowNo.ToString();
                    worksheet.Range["C" + row].Text = model[i].ErrorNotes;

                    row++;
                }
                //Saving the Excel to the MemoryStream 
                MemoryStream stream = new MemoryStream();
                workbook.SaveAs(stream);
                //Set the position as '0'.
                stream.Position = 0;
                //Download the Excel file in the browser
                FileStreamResult fileStreamResult = new FileStreamResult(stream, "application/excel");
                fileStreamResult.FileDownloadName = "CompanyBulkImportErrorsList.xlsx";
                return fileStreamResult;

            }
        }

        public string ReportGenerationExport(List<ImportCommonResponseModel> model)
        {
            var fileStreamResult = string.Empty;
            using (ExcelEngine excelEngine = new ExcelEngine())
            {
                IApplication application = excelEngine.Excel;
                application.DefaultVersion = ExcelVersion.Excel2016;
                //Create a workbook
                IWorkbook workbook = application.Workbooks.Create(1);
                IWorksheet worksheet = workbook.Worksheets[0];

                worksheet.Range["A1"].Text = "Module";
                worksheet.Range["B1"].Text = "ErrorNotes";

                int row = 2;
                for (int i = 0; i < model.Count; i++)
                {
                    worksheet.Range["A" + row].Text = model[i].Module;
                    worksheet.Range["B" + row].Text = model[i].ErrorNotes;
                    row++;
                }
                //Saving the Excel to the MemoryStream 
                MemoryStream stream = new MemoryStream();
                workbook.SaveAs(stream);
                //Set the position as '0'.
                stream.Position = 0;
                fileStreamResult = Convert.ToBase64String(stream.ToArray());
                //Download the Excel file in the browser
                //FileStreamResult fileStreamResult = new FileStreamResult(stream, "application/excel");
                //fileStreamResult.FileDownloadName = "ReportGenerationErrorsList.xlsx";
                return fileStreamResult;

            }
        }

        public string StrategyInstanceExport(List<ImportCommonResponseModel> model)
        {
            var fileStreamResult = string.Empty;
            using (ExcelEngine excelEngine = new ExcelEngine())
            {
                IApplication application = excelEngine.Excel;
                application.DefaultVersion = ExcelVersion.Excel2016;
                //Create a workbook
                IWorkbook workbook = application.Workbooks.Create(1);
                IWorksheet worksheet = workbook.Worksheets[0];

                worksheet.Range["A1"].Text = "Module";
                worksheet.Range["B1"].Text = "RowNo";
                worksheet.Range["C1"].Text = "ErrorNotes";

                int row = 2;
                for (int i = 0; i < model.Count; i++)
                {
                    worksheet.Range["A" + row].Text = model[i].Module;
                    worksheet.Range["B" + row].Text = model[i].RowNo.ToString();
                    worksheet.Range["C" + row].Text = model[i].ErrorNotes;
                    row++;
                }
                //Saving the Excel to the MemoryStream 
                MemoryStream stream = new MemoryStream();
                workbook.SaveAs(stream);
                //Set the position as '0'.
                stream.Position = 0;
                fileStreamResult = Convert.ToBase64String(stream.ToArray());
                //Download the Excel file in the browser
                //FileStreamResult fileStreamResult = new FileStreamResult(stream, "application/excel");
                //fileStreamResult.FileDownloadName = "ReportGenerationErrorsList.xlsx";
                return fileStreamResult;

            }
        }


        public ActionResult CPFinancialExport(List<ImportCommonResponseModel> model)
        {
            using (ExcelEngine excelEngine = new ExcelEngine())
            {
                IApplication application = excelEngine.Excel;
                application.DefaultVersion = ExcelVersion.Excel2016;
                //Create a workbook
                IWorkbook workbook = application.Workbooks.Create(1);
                IWorksheet worksheet = workbook.Worksheets[0];
                worksheet.Name = "CP_Financial Error Response";

                worksheet.Range["A1"].Text = "SheetName";   
                worksheet.Range["B1"].Text = "RowNo";
                worksheet.Range["C1"].Text = "ErrorNotes";

                int row = 2;
                for (int i = 0; i < model.Count; i++)
                {
                    worksheet.Range["A" + row].Text = model[i].Module;
                    worksheet.Range["B" + row].Text = model[i].RowNo.ToString();
                    worksheet.Range["C" + row].Text = model[i].ErrorNotes;

                    row++;
                }

                string handle = Guid.NewGuid().ToString();
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    workbook.SaveAs(memoryStream);
                    memoryStream.Position = 0;
                    //TempData[handle] = memoryStream.ToArray();
                }


                //Saving the Excel to the MemoryStream 
                MemoryStream stream = new MemoryStream();
                workbook.SaveAs(stream);
                //Set the position as '0'.
                stream.Position = 0;
                //Download the Excel file in the browser
                FileStreamResult fileStreamResult = new FileStreamResult(stream, "application/excel");
                fileStreamResult.FileDownloadName = "CompanyFinancialErrorsList.xlsx";
                return fileStreamResult;


               // return new JsonResult(new { FileGuid = handle, FileName = "CPFinancialErrorsList.xlsx" });
            }
        }

        public ActionResult QualitativeAnalysisExport(List<ImportCommonResponseModel> model)
        {
            using (ExcelEngine excelEngine = new ExcelEngine())
            {
                IApplication application = excelEngine.Excel;
                application.DefaultVersion = ExcelVersion.Excel2016;
                //Create a workbook
                IWorkbook workbook = application.Workbooks.Create(1);
                IWorksheet worksheet = workbook.Worksheets[0];
                worksheet.Range["A1"].Text = "SheetName";
                worksheet.Range["B1"].Text = "RowNo";
                worksheet.Range["C1"].Text = "ErrorNotes";

                int row = 2;
                for (int i = 0; i < model.Count(); i++)
                {
                    worksheet.Range["A" + row].Text = model[i].Module;
                    worksheet.Range["B" + row].Text = model[i].RowNo.ToString();
                    worksheet.Range["C" + row].Text = model[i].ErrorNotes;
                    row++;
                }

                string handle = Guid.NewGuid().ToString();
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    workbook.SaveAs(memoryStream);
                    memoryStream.Position = 0;
                    //TempData[handle] = memoryStream.ToArray();
                }


                //Saving the Excel to the MemoryStream 
                MemoryStream stream = new MemoryStream();
                workbook.SaveAs(stream);
                //Set the position as '0'.
                stream.Position = 0;
                //Download the Excel file in the browser
                FileStreamResult fileStreamResult = new FileStreamResult(stream, "application/excel");
                fileStreamResult.FileDownloadName = "QualitativeAnalysisErrorsList.xlsx";
                return fileStreamResult;


                // return new JsonResult(new { FileGuid = handle, FileName = "CPFinancialErrorsList.xlsx" });
            }
        }

        public FileResult DownloadTemplate(string type)
        {
            HttpResponseMessage Response = _serviceRepository.GetResponseWithMethodName("ImportRequest", "GetImportTemplate", type);
            ImportRequestViewModel ImportRequest = Response.Content.ReadAsAsync<ImportRequestViewModel>().Result;           
            if (ImportRequest != null && ImportRequest.fileByte != null)
            {
                return File(ImportRequest.fileByte, System.Net.Mime.MediaTypeNames.Application.Octet, ImportRequest.ImportFileName);
            }
            else
            {
                return File("Data not found", System.Net.Mime.MediaTypeNames.Application.Octet, ImportRequest.ImportFileName);
            }
        }

    }
}