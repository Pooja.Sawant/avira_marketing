﻿CREATE TABLE [dbo].[CompanyShareVolumeImage] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [ImageURL]         NVARCHAR (256)   NULL,
    [ImageDisplayName] NVARCHAR (256)   NULL,
    [ActualImageName]  NVARCHAR (256)   NULL,
    [CompanyId]        UNIQUEIDENTIFIER NULL,
    [ParentCompanyId]  UNIQUEIDENTIFIER NULL,
    [ProjectId]        UNIQUEIDENTIFIER NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_CompanyShareVolumeImage_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CompanyShareVolumeImage] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CompanyShareVolumeImage_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyShareVolumeImage_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyShareVolumeImage_AviraUser2] FOREIGN KEY ([UserDeletedById]) REFERENCES [dbo].[AviraUser] ([Id])
);

