﻿using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Common.IService.Masters;
using Avira.MarketingResearch.Common.Service_Implementation;
using Avira.MarketingResearch.Common.IService.ProjectDashBoard;
using Avira.MarketingResearch.Common.Service_Implementation.Masters;
using Avira.MarketingResearch.Common.Service_Implementation.ProjectDashBoard;
using Avira.MarketingResearch.Repository;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.CoreInterfaces.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces.ProjectDashBoard;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Repository.Implementations;
using Avira.MarketingResearch.Repository.Implementations.Masters;
using Avira.MarketingResearch.Repository.Implementations.ProjectDashBoard;
using Microsoft.Extensions.DependencyInjection;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Implementations.Trend;
using Avira.MarketingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Common.Service_Implementation.Trend;
using Avira.KeyCompanyingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Repository.Implementations.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Common.Service_Implementation.CompanyProfile;
//using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
//using Avira.MarketingResearch.Repository.Implementations.CompanyProfile;

using Avira.MarketingResearch.Common.IService.ImportExport;
using Avira.MarketingResearch.Common.Service_Implementation.ImportExport;
using Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport;
using Avira.MarketingResearch.Repository.Implementations.ImportExport;
using Avira.MarketingResearch.Common.IService.QualitativeInfo;
using Avira.MarketingResearch.Repository.CoreInterfaces.QualitativeInfo;
using Avira.MarketingResearch.Common.Service_Implementation.QualitativeInfo;
using Avira.MarketingResearch.Repository.Implementations.QualitativeInfo;

namespace Avira.MarketingResearch.WebApi
{
    public class DependencyConfig
    {
        public static void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IRawMaterialService, RawMaterialService>();
            services.AddTransient<IRawMaterialRepository, RawMaterialRepository>();
            services.AddTransient<IRegionService, RegionService>();
            services.AddTransient<IRegionRepository, RegionRepository>();
            services.AddTransient<IApplicationService, ApplicationService>();
            services.AddTransient<IApplicationRepository, ApplicationRepository>();
            services.AddTransient<IComponentService, ComponentService>();
            services.AddTransient<IComponentRepository, ComponentRepository>();
            services.AddTransient<IMenuService, MenuService>();
            services.AddTransient<IMenuRepository, MenuRepository>();
            services.AddTransient<IIndustryService, IndustryService>();
            services.AddTransient<IIndustryRepository, IndustryRepository>();
            services.AddTransient<IServiceService, ServiceService>();
            services.AddTransient<IServiceRepository, ServiceRepository>();
            services.AddTransient<ICompanyService, CompanyService>();
            services.AddTransient<ICompanyRepository, CompanyRepository>();
            services.AddTransient<ICountryService, CountryService>();
            services.AddTransient<ICountryRepository, CountryRepository>();
            services.AddTransient<IMarketService, MarketService>();
            services.AddTransient<IMarketRepository, MarketRepository>();
            services.AddTransient<IEcoSystemService, EcoSystemService>();
            services.AddTransient<IEcoSystemRepository, EcoSystemRepository>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IRoleService, RoleService>();
            services.AddTransient<IRoleRepository, RoleRepository>();
            services.AddTransient<IFundamentalService, FundamentalService>();
            services.AddTransient<IFundamentalRepository, FundamentalRepository>();
            services.AddTransient<ICompanyGroupService, CompanyGroupService>();
            services.AddTransient<ICompanyGroupRepository, CompanyGroupRepository>();
            services.AddTransient<IProjectService, ProjectService>();
            services.AddTransient<IProjectRepository, ProjectRepository>();
            services.AddTransient<IProjectAnalistDetailsService, ProjectAnalistDetailsService>();
            services.AddTransient<IProjectAnalistDetailsRepository, ProjectAnalistDetailsRepository>();
            services.AddTransient<IProjectSegmentMappingService, ProjectSegmentMappingService>();
            services.AddTransient<IProjectSegmentMappingRepository, ProjectSegmentMappingRepository>();
            services.AddTransient<ITrendKeywordMapService, TrendKeywordMapService>();
            services.AddTransient<ITrendKeywordMapRepository, TrendKeyWordMapRepository>();
            services.AddTransient<ICurrencyService, CurrencyService>();
            services.AddTransient<ICurrencyRepository, CurrencyRepository>();
            services.AddScoped<ITrendIndustryMapService, TrendIndustryMapService>();
            services.AddScoped<ITrendIndustryMapRepository, TrendIndustryMapRepository>();
            services.AddTransient<IStandardHoursService, StandardHoursService>();
            services.AddTransient<IStandardHoursRepository, StandardHoursRepository>();
            services.AddTransient<IProductCategoryService, ProductCategoryService>();
            services.AddTransient<IProductcategoryRepository, ProductCategoryRepository>();

            //Trend Tracker And Mapping Services Configuration
            services.AddTransient<ITrendService, TrendService>();
            services.AddTransient<ITrendRepository, TrendRepository>();
            services.AddScoped<IDbHelper, DbHelper>();
            services.AddScoped<ITrendMarketMapService, TrendMarketMapService>();
            services.AddScoped<ITrendMarketMapRepository, TrendMarketMapRepository>();
            services.AddScoped<ITrendRegionMapService, TrendRegionMapService>();
            services.AddScoped<ITrendRegionMapRepository, TrendRegionMapRepository>();
            services.AddScoped<IImpactTypeService, ImpactTypeService>();
            services.AddScoped<IImpactTypeRepository, ImpactTypeRepository>();
            services.AddTransient<ITrendMarketMapService, TrendMarketMapService>();
            services.AddTransient<ITrendMarketMapRepository, TrendMarketMapRepository>();
            services.AddTransient<IImpactTypeService, ImpactTypeService>();
            services.AddTransient<IImpactTypeRepository, ImpactTypeRepository>();
            services.AddTransient<ITrendNoteService, TrendNoteService>();
            services.AddTransient<ITrendNoteRepository, TrendNoteRepository>();
            services.AddTransient<ICompanyNoteService, CompanyNoteService>();
            services.AddTransient<ICompanyNoteRepository, CompanyNoteRepository>();
            services.AddTransient<IQualitativeNoteService, QualitativeNoteService>();
            services.AddTransient<IQualitativeNoteRepository, QualitativeNoteRepository>();

            services.AddTransient<ITrendCompanyGroupMapService, TrendCompanyGroupMapService>();
            services.AddTransient<ITrendCompanyGroupMapRepository, TrendCompanyGroupMapRepository>();

            services.AddTransient<ITrendProjectMapService, TrendProjectMapService>();
            services.AddTransient<ITrendProjectMapRepository, TrendProjectMapRepository>();

            services.AddTransient<ITrendEcoSystemMapService, TrendEcoSystemMapService>();
            services.AddTransient<ITrendEcoSystemMapRepository, TrendEcoSystemMapRepository>();
            services.AddTransient<ITrendKeyCompanyMapService, TrendKeyCompanyMapService>();
            services.AddTransient<ITrendKeyCompanyMapRepository, TrendKeyCompanyMapRepository>();

            services.AddTransient<ITrendPreviewService, TrendPreviewService>();
            services.AddTransient<ITrendPreviewRepository, TrendPreviewRepository>();

            services.AddTransient<ITrendTimeTagService, TrendTimeTagService>();
            services.AddTransient<ITrendTimeTagRepository, TrendTimeTagRepository>();

            services.AddTransient<ICurrencyConversionService, CurrencyConversionService>();
            services.AddTransient<ICurrencyConversionRepository, CurrencyConversionRepository>();

            services.AddScoped<ITrendValueMapService, TrendValueMapService>();
            services.AddScoped<ITrendValueMapRepository, TrendValueMapRepository>();

            services.AddTransient<ICompanyStructureService, CompanyStructureService>();
            services.AddTransient<ICompanyStructureRepository, CompanyStructureRepository>();

            services.AddTransient<ITrendImportExportService, TrendImportExportService>();
            services.AddTransient<ITrendImportExportRepository, TrendImportExportRepository>();

            services.AddTransient<IMarketSizingImportExportService, MarketSizingImportExportService>();
            services.AddTransient<IMarketSizingImportExportRepository, MarketSizingImportExportRepository>();

            services.AddTransient<ICompanyProfileImportExportService, CompanyProfileImportExportService>();
            services.AddTransient<ICompanyProfileImportExportRepository, CompanyProfileImportExportRepository>();

            services.AddTransient<ICompanyBasicInfoImportExportService, CompanyBasicInfoImportExportService>();
            services.AddTransient<ICompanyBasicInfoImportExportRepository, CompanyBasicInfoImportExportRepository>();

            services.AddTransient<ICompanyFundamentalService, CompanyFundamentalService>();
            services.AddTransient<ICompanyFundamentalRepository, CompanyFundamentalRepository>();

            services.AddTransient<ICPFinancialImportExportService, CPFinancialImportExportService>();
            services.AddTransient<ICPFinancialImportExportRepository, CPFinancialImportExportRepository>();


            //CompanyProfile Tracker And Mapping Services Configuration

            services.AddTransient<INewsService, NewsService>();

            services.AddTransient<INewsRepository, NewsRepository>();

            services.AddTransient<IRevenueProfitAnalysisService, RevenueProfitAnalysisService>();
            services.AddTransient<IRevenueProfitAnalysisRepository, RevenueProfitAnalysisRepository>();


            services.AddTransient<ICompanyShareVolumeService, CompanyShareVolumeService>();
            services.AddTransient<ICompanyShareVolumeRepository, CompanyShareVolumeRepository>();

            services.AddTransient<ISegmentService, SegmentService>();
            services.AddTransient<ISegmentRepository, SegmentRepository>();

            services.AddTransient<ICompanyProductSegmentMapService, CompanyProductSegmentMapService>();
            services.AddTransient<ICompanyProductSegmentMapRepository, CompanyProductSegmentMapRepository>();

            services.AddTransient<ICompanyProductCategoryMapService, CompanyProductCategoryMapService>();
            services.AddTransient<ICompanyProductCategoryMapRepository, CompanyProductCategoryMapRepository>();

            services.AddTransient<ICompanyProductService, CompanyProductService>();
            services.AddTransient<ICompanyProductRepository, CompanyProductRepository>();
            services.AddTransient<ICompanyProductCountryMapService, CompanyProductCountryMapService>();
            services.AddTransient<ICompanyProductCountryMapRepository, CompanyProductCountryMapRepositorycs>();

            services.AddTransient<ICompanyFinancialAnalysisService, CompanyFinancialAnalysisService>();
            services.AddTransient<ICompanyFinancialAnalysisRepository, CompanyFinancialAnalysisRepository>();

            services.AddTransient<ICompanyOtherInfoService, CompanyOtherInfoService>();
            services.AddTransient<ICompanyOtherInfoRepository, CompanyOtherInfoRepository>();

            services.AddTransient<ICompanyTrendsMapService, CompanyTrendsMapService>();
            services.AddTransient<ICompanyTrendsMapRepository, CompanyTrendsMapRepository>();

            services.AddTransient<IImportanceService, ImportanceService>();
            services.AddTransient<IImportanceRepository, ImportanceRepository>();

            services.AddTransient<IImpactDirectionService, ImpactDirectionService>();
            services.AddTransient<IImpactDirectionRepository, ImpactDirectionRepository>();

            services.AddTransient<ICompanyClientAndStrategyService, CompanyClientAndStrategyService>();
            services.AddTransient<ICompanyClientsStrategyRepository, CompanyClientStrategyRepository>();

            services.AddTransient<IEcoSystemPresenceService, EcoSystemPresenceService>();
            services.AddTransient<IEcoSystemPresenceRepository, EcoSystemPresenceRepositary>();

            services.AddTransient<IProductStatusService, ProductStatusService>();
            services.AddTransient<IProductStatusRepository, ProductStatusRepository>();

            services.AddTransient<ICompanySWOTService, CompanySWOTService>();
            services.AddTransient<ICompanySwotRepository, CompanySwotRepository>();

            services.AddTransient<ICompanyOrganizationService, CompanyOrganizationService>();
            services.AddTransient<ICompanyOrganizationRepository, CompanyOrganizationRepository>();

            services.AddTransient<ICompanyAnalystViewService, CompanyAnalystViewService>();
            services.AddTransient<ICompanyAnalystViewRepository, CompanyAnalystViewRepository>();

            services.AddTransient<ICompanyTrackerService, CompanyTrackerService>();
            services.AddTransient<ICompanyTrackerRepository, CompanyTrackerRepository>();

            services.AddTransient<IQualitativeInfoTrackerService, QualitativeInfoTrackerService>();
            services.AddTransient<IQualitativeInfoTrackerRepository, QualitativeInfoTrackerRepository>();

            services.AddTransient<IQualitativeAddTableService, QualitativeAddTableService>();
            services.AddTransient<IQualitativeAddTableRepository, QualitativeAddTableRepository>();

            //services.AddTransient<ICompanyProfileRevenueAddTableService, CompanyProfileRevenueAddTableService>();
            services.AddTransient<ICompanyProfitAndLossService, CompanyProfitAndLossService>();
            services.AddTransient<ICompanyProfitAndLossRepository, CompanyProfitAndLossRepository>();

            services.AddTransient<ICompanyBalanceSheetService, CompanyBalanceSheetService>();
            services.AddTransient<ICompanyBalanceSheetRepository, CompanyBalanceSheetRepository>();

            services.AddTransient<ICompanyCashFlowStatementService, CompanyCashFlowStatementService>();
            services.AddTransient<ICompanyCashFlowStatementRepository, CompanyCashFlowStatementRepository>();

            services.AddTransient<ICompanySegmentInformationService, CompanySegmentInformationService>();
            services.AddTransient<ICompanySegmentInformationRepository, CompanySegmentInformationRepository>();

            services.AddTransient<IQualitativeAnalysisImportExportService, QualitativeAnalysisImportExportService>();
            services.AddTransient<IQualitativeAnalysisImportExportRepository, QualitativeAnalysisImportExportRepository>();

            services.AddTransient<IImportRequestService, ImportRequestService>();
            services.AddTransient<IImportRequestRepository, ImportRequestRepository>();

            services.AddTransient<IStatusService, StatusService>();
            services.AddTransient<IStatusRepository, StatusRepository>();

            services.AddTransient<IInfographicsService, InfographicsService>();
            services.AddTransient<IInfographicsRepository, InfographicsRepository>();

            services.AddTransient<ILookupCategoryService, LookupCategoryService>();
            services.AddTransient<ILookupCategoryRepository, LookupCategoryRepository>();

            services.AddTransient<ISqlTableSchemaService, SqlTableSchemaService>();
            services.AddTransient<ISqlTableSchemaRepository, SqlTableSchemaRepository>();

            services.AddTransient<IReportGenerationImportExportService, ReportGenerationImportExportService>();
            services.AddTransient<IReportGenerationImportExportRepository, ReportGenerationImportExportRepository>();

            services.AddTransient<IStrategyInstanceImportExportService, StrategyInstanceImportExportService>();
            services.AddTransient<IStrategyInstanceImportExportRepository, StrategyInstanceImportExportRepository>();

            services.AddTransient<ICPFundamentalImportExportService, CPFundamentalImportExportService>();
            services.AddTransient<ICPFundamentalImportExportRepository, CPFundamentalImportExportRepository>();

            services.AddTransient<ICompanyBulkImportExportService, CompanyBulkImportExportService>();
            services.AddTransient<ICompanyBulkImportExportRepository, CompanyBulkImportExportRepository>();

        }

    }
}
