﻿CREATE TABLE [dbo].[UserQueries] (
    [Id]                   UNIQUEIDENTIFIER NOT NULL,
    [SupportCategoryId]    UNIQUEIDENTIFIER NOT NULL,
    [SupportSubCategoryId] UNIQUEIDENTIFIER NOT NULL,
    [UserMessage]          NVARCHAR (3000)  NOT NULL,
    [UserId]               UNIQUEIDENTIFIER NOT NULL,
    [ContactChanel]        NVARCHAR (50)    NOT NULL,
    [CreatedOn]            DATETIME2 (7)    NULL,
    [UserCreatedById]      UNIQUEIDENTIFIER NULL,
    [AccountOwner]         UNIQUEIDENTIFIER NULL,
    [StatusId]             UNIQUEIDENTIFIER NULL,
    [ActionPoint]          NVARCHAR (3000)  NULL,
    [OwnerRemarks]         NVARCHAR (3000)  NULL,
    [LastActionOn]         DATETIME2 (7)    NULL,
    CONSTRAINT [PK_UserQueries] PRIMARY KEY CLUSTERED ([Id] ASC)
);



