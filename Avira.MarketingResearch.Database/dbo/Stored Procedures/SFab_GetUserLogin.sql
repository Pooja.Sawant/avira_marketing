﻿
CREATE procedure [dbo].[SFab_GetUserLogin]
(@UserName nvarchar(100))
as
/*================================================================================
Procedure Name: SSec_GetUserLogin
Author: Laxmikant
Create date: 02/11/2019
Description: Get user login details by Name
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
31/08/2019	Praveen			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT [Id], 
		[UserName],
		[DisplayName],
		[EmailAddress],
		[Salt],
		[PassWord],
		UserRoleId,
		IsUserLocked,
		ResetPasswordLinkCreatedOn,
		ResetPasswordSalt
FROM [dbo].[AviraUser]
where (UserName = @UserName )--or EmailAddress = @UserName
and IsDeleted = 0 AND UserTypeId = 2--2 for customer user
--and IsUserLocked = 0;
end