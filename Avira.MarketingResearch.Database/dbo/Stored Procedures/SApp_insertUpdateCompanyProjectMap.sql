﻿CREATE procedure [dbo].[SApp_insertUpdateCompanyProjectMap]
(
    @ID uniqueidentifier,
	@CompanyID uniqueidentifier,
	@ProjectID uniqueidentifier,
	@IsDeleted bit,
	@AviraUserId uniqueidentifier
)
as
/*================================================================================
Procedure Name: [SApp_insertUpdateCompanyProjectMap]
Author: Sai Krishna
Create date: 05/06/2019
Description: soft delete a record from Company table by ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
05/06/2019	Sai Krishna		Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

Delete From CompanyProjectMap 
Where CompanyId = @CompanyID
and ProjectId = @ProjectID;

--IF (@ID is null or @ID = '00000000-0000-0000-0000-000000000000')
--begin
--Insert new record
	set @ID = NEWID();
	insert into [dbo].[CompanyProjectMap](
									[Id],
									[CompanyId],
									[ProjectId],
									[IsDeleted],
									[CreatedOn],
									[UserCreatedById]
								)
	values(
			@ID,
			@CompanyID,
			@ProjectID, 
			0,
			getdate(),
			@AviraUserId
			);
--end
--else
--begin
----update record
--	update [dbo].[CompanyProjectMap]
--	set [CompanyId] = @CompanyID,
--		[ProjectId] = @ProjectID,
--		[ModifiedOn] = getdate(),
--		DeletedOn = case when IsDeleted = 0 and @IsDeleted = 1 then getdate() else DeletedOn end,
--		UserDeletedById = case when IsDeleted = 0 and @IsDeleted = 1 then @AviraUserId else UserDeletedById end,
--		IsDeleted = isnull(@IsDeleted, IsDeleted),
--		[UserModifiedById] = @AviraUserId
--     where Id = @ID
--end
select @ID, Success = 1;
end