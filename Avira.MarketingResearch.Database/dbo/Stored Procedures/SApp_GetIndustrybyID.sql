﻿CREATE procedure [dbo].[SApp_GetIndustrybyID]
(@IndustryID uniqueidentifier=null,
@ParentIndustryID uniqueidentifier = null,
@includeDeleted bit = 0,
@IncludeParentOnly bit = 0)
as
/*================================================================================
Procedure Name: SApp_GetIndustrybyID
Author: Sharath
Create date: 02/19/2019
Description: Get list from Industry table by Name or ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
02/19/2019	Sharath			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @SegmentId uniqueidentifier;
select @SegmentId = Id from Segment
where SegmentName = 'End User';

if @includeDeleted is null set @includeDeleted = 0

SELECT [Id],
		SubSegmentName [IndustryName],
		[Description],
		[ParentId] 
FROM [dbo].SubSegment
where (@IndustryID is null or ID = @IndustryID)
and (@ParentIndustryID is null or ParentId = @ParentIndustryID)
and (@includeDeleted = 1 or IsDeleted = 0)
and SegmentId = @SegmentId
and (@IncludeParentOnly = 0 or ParentId is null)
--exists (select 1 from [dbo].[Industry] childIndustry where childIndustry.ParentId = Industry.Id))


end