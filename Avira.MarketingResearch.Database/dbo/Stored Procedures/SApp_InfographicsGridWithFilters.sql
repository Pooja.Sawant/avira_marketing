﻿CREATE PROCEDURE [dbo].[SApp_InfographicsGridWithFilters]
	(@Id uniqueidentifier = null,
	@Title nvarchar(256)='',
	@ProjectId	uniqueidentifier =  null,
	@LookupCategoryId	uniqueidentifier= null,
	@Description nvarchar(256)='',		
	@OrdbyByColumnName NVARCHAR(50) ='CreatedDate',
	@SortDirection INT = -1,
	@PageStart INT = 0,
	@PageSize INT = Null)
	AS
/*================================================================================
Procedure Name:SApp_InfographicsGridWithFilters
Author: Praveen
Create date: 10/28/2019
Description: Get list from Infographics table by Name or ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
10/28/2019	Praveen			Initial Version
================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

if len(@Title)>2 set @Title = '%'+ @Title + '%'
else set @Title = ''
if len(@Description)>2 set @Description = '%'+ @Description + '%';
else set @Description = '';
	if @PageSize is null or @PageSize =0
	 set @PageSize = 10  
	begin
	;WITH CTE_TBL AS (

	SELECT 
InfoGr.Id				,
InfoGr.ProjectId			,
InfoGr.LookupCategoryId	,
InfoGr.InfogrTitle				,
InfoGr.InfogrDescription			,
InfoGr.SequenceNo			,
InfoGr.ImageActualName		,
InfoGr.ImageName,
InfoGr.ImagePath,
InfoGr.CreatedOn			,
InfoGr.UserCreatedById		,
InfoGr.ModifiedOn			,
InfoGr.UserModifiedById,
Project.ProjectName,
LookupCategory.CategoryName as Category,
		CASE
		WHEN @OrdbyByColumnName = 'Title' THEN InfoGr.InfogrTitle
		WHEN @OrdbyByColumnName = 'Description' THEN InfoGr.InfogrDescription		
		
		ELSE [InfogrTitle]
	END AS SortColumn

FROM Infographics InfoGr
JOIN Project ON Project.ID = InfoGr.ProjectId
JOIN LookupCategory ON LookupCategory.ID = InfoGr.LookupCategoryId AND LookupCategory.CategoryType = 'Infographics'
where ISNULL(@Id,'00000000-0000-0000-0000-000000000000') = CASE WHEN ISNULL(@Id,'00000000-0000-0000-0000-000000000000') = '00000000-0000-0000-0000-000000000000' THEN ISNULL(@Id,'00000000-0000-0000-0000-000000000000') ELSE InfoGr.Id END
	AND (@Title = '' or InfoGr.[InfogrTitle] like @Title)
	AND (@Description = '' or InfoGr.[InfogrDescription] like @Description)
	AND ISNULL(@ProjectId,'00000000-0000-0000-0000-000000000000') = 
	CASE 
		WHEN ISNULL(@ProjectId,'00000000-0000-0000-0000-000000000000') = '00000000-0000-0000-0000-000000000000' 
		THEN
		CASE 
		WHEN ISNULL(@LookupCategoryId,'00000000-0000-0000-0000-000000000000') != '00000000-0000-0000-0000-000000000000' 
		THEN ISNULL(@LookupCategoryId,'00000000-0000-0000-0000-000000000000') 
		ELSE ISNULL(@ProjectId,'00000000-0000-0000-0000-000000000000')
	END
		 
		ELSE InfoGr.ProjectId 
	END
	AND ISNULL(@LookupCategoryId,'00000000-0000-0000-0000-000000000000') = 
	CASE 
		WHEN ISNULL(@LookupCategoryId,'00000000-0000-0000-0000-000000000000') = '00000000-0000-0000-0000-000000000000' 
		THEN ISNULL(@LookupCategoryId,'00000000-0000-0000-0000-000000000000') 
		ELSE InfoGr.LookupCategoryId 
	END

	)

	SELECT 
		  CTE_TBL.*,
		  tCountResult.TotalItems
		FROM CTE_TBL
		CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult
		ORDER BY 
		  CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,
		  CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC 
		OFFSET @PageStart ROWS
		FETCH NEXT @PageSize ROWS ONLY;
		end

	END