﻿CREATE TABLE [dbo].[MakeMyMarket_Trends_History] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [MakeMyMarketID]   UNIQUEIDENTIFIER NOT NULL,
    [TrendID]          UNIQUEIDENTIFIER NOT NULL,
    [Year]             INT              NOT NULL,
    [TrendValue]       DECIMAL (19, 4)  NULL,
    [CreatedOn]        DATETIME2 (7)    NOT NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NOT NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    FOREIGN KEY ([TrendID]) REFERENCES [dbo].[Trend] ([Id]),
    FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[Customer] ([Id]),
    FOREIGN KEY ([UserDeletedById]) REFERENCES [dbo].[Customer] ([Id]),
    FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[Customer] ([Id]),
    CONSTRAINT [FK__MakeMyMar__MakeM__1336D48F] FOREIGN KEY ([MakeMyMarketID]) REFERENCES [dbo].[MakeMyMarkets] ([Id])
);

