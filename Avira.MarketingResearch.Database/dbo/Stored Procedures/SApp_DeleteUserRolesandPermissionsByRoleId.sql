﻿
CREATE procedure [dbo].[SApp_DeleteUserRolesandPermissionsByRoleId]      
(@Id uniqueidentifier,      
@IsDeleted bit=0,    
@DeletedOn datetime = null,    
@UserDeletedById uniqueidentifier = null    
)  
as      
/*================================================================================      
Procedure Name: SApp_DeleteUserRolesandPermissionsByRoleId     
Author: Sai Krishna      
Create date: 04/25/2019      
Description: update a record in User Roles table by ID. Also used for delete records from user permissions map table      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
04/25/2019 Sai Krishna   Initial Version     
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;  

Declare @ErrorMsg nvarchar(max)
    
BEGIN TRY       
--update record      
update [dbo].[UserRole]      
set       
 DeletedOn = case when IsDeleted = 0 and @IsDeleted = 1 then getdate() else DeletedOn end,      
 UserDeletedById = case when IsDeleted = 0 and @IsDeleted = 1 then @Id else UserDeletedById end,      
 IsDeleted = isnull(@IsDeleted, IsDeleted)      
 where ID = @Id;
 
 Delete from [dbo].[UserRolePermissionMap] 
 where UserRoleId = @Id;    
     
SELECT StoredProcedureName ='SApp_DeleteUserRolesandPermissionsByRoleId',Success = 1;       
    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);      
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());   
  
set @ErrorMsg = 'Error while updating Market, please contact Admin for more details.'        
   
 SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
END CATCH      
      
    
    
      
end