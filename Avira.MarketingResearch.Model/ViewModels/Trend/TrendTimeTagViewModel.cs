﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.Trend
{
    public class TrendTimeTagViewModel
    {
        public Guid TrendId { get; set; }
        public Guid TimeTagId { get; set; }
        public Guid ImpactId { get; set; }
        public string TimeTagName { get; set; }
        public bool IsMapped { get; set; }
        public bool EndUser { get; set; }
        public int TotalItems { get; set; }
        public int Seq {get;set;}
        public string RowType
        {
            get
            {
                return "TrendTimeTag";
            }
        }
    }

    public class MainTrendTimeTagInsertUpdateDbViewModel
    {
        public Guid TrendId { get; set; }
        public string TimeTagName { get; set; }
        public List<TrendTimeTagViewModel> TrendTimeTagInsertUpdate { get; set; }
        public Guid UserCreatedById { get; set; }
        public TrendNoteInsertUpdateDbViewModel TrendNote { get; set; }
        public TrendViewModel TrendModel { get; set; }
        public TrendNoteViewModel TrendNoteModel { get; set; }
        public bool IsDeleted { get; set; }
    }
}
