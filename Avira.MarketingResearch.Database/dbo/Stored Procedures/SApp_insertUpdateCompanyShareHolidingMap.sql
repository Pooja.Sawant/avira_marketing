﻿

CREATE procedure [dbo].[SApp_insertUpdateCompanyShareHolidingMap]      
(
	@ShareHolidingListmap [dbo].[udtGUIDShareHolidingMultiValue] readonly,
	@CompanyId uniqueidentifier,
	@UserCreatedById uniqueidentifier
)      
as      
/*================================================================================      
Procedure Name: [SApp_insertUpdateCompanyShareHolidingMap]      
Author: Sai Krishna      
Create date: 05/06/2019      
Description: Insert a record into ShareHoldingPatternMap table      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
05/06/2019 Sai Krishna   Initial Version    
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)  
    
      
BEGIN TRY         
----mark records as delete which are not in list
--update [dbo].[ShareHoldingPatternMap]
--set IsDeleted = 1,
--UserDeletedById = @UserCreatedById,
--DeletedOn = GETDATE()
--from [dbo].[ShareHoldingPatternMap]
--where [ShareHoldingPatternMap].CompanyId = @CompanyId
--and not exists (select 1 from @ShareHolidingListmap map where map.Id = [ShareHoldingPatternMap].Id)

-- --Update records for change
--update [dbo].[ShareHoldingPatternMap]
--set ShareHoldingName = map.Name,
--EntityTypeId = map.EntityTypeId,
--ShareHoldingPercentage = map.DecimalValue,
--ModifiedOn = GETDATE(),
--UserModifiedById = @UserCreatedById
--from [dbo].[ShareHoldingPatternMap]
--inner join @ShareHolidingListmap map 
--on map.Id = [ShareHoldingPatternMap].Id
--where [ShareHoldingPatternMap].CompanyId = @CompanyId;

Delete from [dbo].[ShareHoldingPatternMap]
Where [ShareHoldingPatternMap].[CompanyId] = @CompanyId

--Insert new records
insert into [dbo].[ShareHoldingPatternMap](
		[Id],      
        [ShareHoldingName], 
		[EntityTypeId],
		[ShareHoldingPercentage],
		[CompanyId],
        [IsDeleted],      
        [UserCreatedById],      
        [CreatedOn])      
 select NEWID(),
   map.Name,       
   map.EntityTypeId,
   map.DecimalValue,
   @CompanyId,    
   0,      
   @UserCreatedById,       
   GETDATE()
 from @ShareHolidingListmap map
 where not exists (select 1 from [dbo].[ShareHoldingPatternMap] map1
					where map1.CompanyId = @CompanyId
					and map.Id = map1.Id);       
     
SELECT StoredProcedureName ='SApp_insertUpdateCompanyShareHolidingMap',Message =@ErrorMsg,Success = 1;       
    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);      
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());      
    
 set @ErrorMsg = 'Error while insert a new TrendMarketMap, please contact Admin for more details.';    
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
       
END CATCH      
      
      
end