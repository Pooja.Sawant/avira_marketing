﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels
{
   public class IndustryViewModel
    {
        public string IndustryName { get; set; }
        public string Description { get; set; }
        public Guid? ParentId { get; set; }
        public string ParentIndustryName { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public Guid Id { get; set; }
        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "Industry";
            }
        }
     
    }

    public class IndustryInsertDbViewModel
    {
        [Required]
        [Display(Name = "Industry Name")]
        public string IndustryName { get; set; }
        public string Description { get; set; }
        public Guid? ParentId { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public Guid Id { get; set; }
    }

    public class IndustryUpdateDbViewModel
    {
        [Required]
        [Display(Name = "Industry Name")]
        public string IndustryName { get; set; }
        public string Description { get; set; }
        public Guid? ParentId { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool IsDeleted { get; set; } = false;
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        [Required]
        public Guid Id { get; set; }
    }
}
