﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.ImportExport
{
    public interface ICompanyProfileImportExportService
    {
        //Result<SpTransactionMessage> Create(CompanyProfileImportInsertViewModel tfc, AviraUser currentUser);
        List<ImportCommonResponseModel> Create(CompanyProfileImportInsertViewModel viewModel, AviraUser currentUser);
    }
}
