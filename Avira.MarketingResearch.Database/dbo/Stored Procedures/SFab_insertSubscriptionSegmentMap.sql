﻿
CREATE procedure [dbo].[SFab_insertSubscriptionSegmentMap]
(
	@SubscriptionId uniqueidentifier,
	@SegmentList dbo.udtGUIDVarcharValue READONLY,
	@AviraUserId uniqueidentifier  
)
as
/*================================================================================
Procedure Name: [SFab_insertSubscriptionSegmentMap]
Author: Adarsh
Create date: 23/05/2019
Description: Insert a record into Subscription Segment Table
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
23/05/2019	Sai Krishna			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ErrorMsg NVARCHAR(512);

--BEGIN TRY

INSERT INTO [dbo].[SubscriptionSegmentMap]
           (
			[Id]
           ,[SubscriptionId]
           ,[SegmentType]
           ,[SegmentId]
           ,[CreatedOn]
           ,[UserCreatedById]
           ,[IsDeleted]
           )
   select NEWID(),
   @SubscriptionId,
   [Value],
   [ID],
   GETDATE(),
   @AviraUserId,
   0
   from @SegmentList;

SELECT StoredProcedureName ='SFab_insertSubscriptionSegmentMap',Message =@ErrorMsg,Success = 1;  
--END TRY
--BEGIN CATCH
--    -- Execute the error retrieval routine.
--	DECLARE @ErrorNumber	int;
--	DECLARE @ErrorSeverity	int;
--	DECLARE @ErrorProcedure	varchar;
--	DECLARE @ErrorLine	int;
--	DECLARE @ErrorMessage	varchar;

--	 SELECT @ErrorNumber = ERROR_NUMBER(),
--        @ErrorSeverity = ERROR_SEVERITY(),
--        @ErrorProcedure = ERROR_PROCEDURE(),
--        @ErrorLine = ERROR_LINE(),
--        @ErrorMessage = ERROR_MESSAGE()

--	insert into dbo.Errorlog(ErrorNumber,
--							ErrorSeverity,
--							ErrorProcedure,
--							ErrorLine,
--							ErrorMessage,
--							ErrorDate)
--        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());

--	set @ErrorMsg = 'Error while permission to role, please contact Admin for more details.';

--SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
--    ,Message =@ErrorMsg,Success = 0;

--END CATCH


end