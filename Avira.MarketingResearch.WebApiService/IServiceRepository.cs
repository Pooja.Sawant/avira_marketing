﻿using System;
using System.Net.Http;

namespace Avira.MarketingResearch.WebApiService
{
    public interface IServiceRepository
    {
        HttpResponseMessage GetResponseByUserId(string segment, Guid Id, Guid AviraUserId, int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string SearchData);
        HttpResponseMessage GetResponse(string segment, int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string searchData);
        HttpResponseMessage GetResponse(string segment, int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string SearchData, Guid projectId, Guid categoryId);
        HttpResponseMessage GetResponseWithMethodName(string segment, string methodName, string id = "");
        HttpResponseMessage GetResponse(string segment,Guid Id, int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string searchData);
        HttpResponseMessage GetResponse(string segment);
        HttpResponseMessage GetResponse(string segment,string guid, string type);
        HttpResponseMessage GetAllParentResponse(string segment);
        HttpResponseMessage GetByIdResponse(string segment, string guid);

        HttpResponseMessage GetProjectByIdResponse(string segment, string guid, Guid AviraUserId);
        HttpResponseMessage GetByIdResponse(string segment, string guid, string type);
        HttpResponseMessage GetByIdResponse(string segment, string guid, string type, string idType);
        HttpResponseMessage GetCompanyNoteByIdResponse(string segment, string guid,string type, string guid2);
        HttpResponseMessage GetQualitativeNoteByIdResponse(string segment, string guid,string guid2);
        HttpResponseMessage PutResponse(string segment, object model);
        HttpResponseMessage PostResponse(string segment, object model);
        HttpResponseMessage PostResponseWithFile(string segment, object model);
        HttpResponseMessage DeleteResponse(string segment, object model);
        HttpResponseMessage GetSubResponse(string segment);
        HttpResponseMessage GetByGuIdResponse(string segment, Guid guid);

        HttpResponseMessage GetPermissionByGuIdResponse(string segment, string guid);
        HttpResponseMessage GetUserByRoleResponse(string segment, string roleName);
        HttpResponseMessage GetAllSegmentListIdResponse(string segment, string id, bool includeParentOnly, string segName);
        HttpResponseMessage PutdataResponse(string segment, Guid guid);

        HttpResponseMessage GetTrendByIdResponse(string segment, Guid guid);
        HttpResponseMessage GetTrendNameByIdResponse(string segment, Guid guid);
        HttpResponseMessage GetByAllResponse(string segment, object model);

        HttpResponseMessage GetImportanceResponse(string segment);
        HttpResponseMessage GetImpactDirectionResponse(string segment);

        HttpResponseMessage GetShareVolByIdResponse(string segment, string guid);
        HttpResponseMessage SaveImageByIdResponse(string segment, object model);

        HttpResponseMessage GetAllImportanceByCompanyIdResponse(string segment);
        HttpResponseMessage GetAllImpactDirectionByCompanyIdResponse(string segment);
        HttpResponseMessage GetAllApproachByCompanyIdSegmentResponse(string segment);
        HttpResponseMessage GetAllCategoriesByCompanyIdSegmentResponse(string segment);
        HttpResponseMessage GetAllIndustryByCompanyIdSegmentResponse(string segmentName);
        HttpResponseMessage GetAllStrategyByCompanyIdSegmentResponse(string segmentName, string guid);
        HttpResponseMessage GetAllclientByCompanyIdSegmentResponse(string segmentName, string guid);
        HttpResponseMessage GetCompanyClientStrategyByCompanyId(string segment, string guid);

        HttpResponseMessage GetCompanySWOTStrengthByCompanyID(string segment, string guid);
        HttpResponseMessage GetCompanySWOTWeeknessesByCompanyID(string segment, string guid);
        HttpResponseMessage GetCompanySWOTOpportunitiesByCompanyID(string segment, string guid);
        HttpResponseMessage GetCompanySWOTThreatsByCompanyID(string segment, string guid);
        HttpResponseMessage GetAllParentSegResponse(string segment);
        HttpResponseMessage GetAllCategoryResponse(string segment);
        HttpResponseMessage GetOtherFinancialByGuIdResponse(string segment, Guid guid);

        HttpResponseMessage GetCompanyDetailsByGuIdsResponse(string segment, string guidone, string guidtwo);
        HttpResponseMessage GetAllCompaniesResponse(string segment);
        HttpResponseMessage GetAllDesignationsResponse(string segment);
        HttpResponseMessage GetAllCompanyStagesResponse(string segment);
        HttpResponseMessage GetCompanyFundamentalResponse(string segment);
        HttpResponseMessage GetCompanyRegionResponse(string segment);
        HttpResponseMessage GetCompanyEntityResponse(string segment);
        HttpResponseMessage GetCompanySubsidaryResponse(string segment, string guid);
        HttpResponseMessage GetEcoSystemPresenceMapByCompanyId(string segment, Guid guid);
        HttpResponseMessage GetSectorPresenceMapByCompanyId(string segment, Guid guid);
        HttpResponseMessage GetEcoSystemPresenceKeyCompByCompanyId(string segment, Guid guid);

        HttpResponseMessage GetCompanyOrganizationChart(string segment, string guid);
        HttpResponseMessage GetCompanyOtherInfo(string segment, string guid);
        HttpResponseMessage GetCompanyManagerResponse(string segment, string guid);

        HttpResponseMessage GetAllCurrencies(string segment);
        HttpResponseMessage GetAllRegions(string segment);
        HttpResponseMessage GetAllValueConversion(string segment);
        HttpResponseMessage GetAllCategoryURL(string segment);
        HttpResponseMessage GetDynamicURL(string segment,string guid);
        HttpResponseMessage GetRevenueProfitBaseYear(string segment);

        HttpResponseMessage GetCompanyNatureResponse(string segment);
    }
}
