CREATE TABLE [dbo].[MarketValue] (
    [Id]                        UNIQUEIDENTIFIER NOT NULL,
    [MarketId]                  UNIQUEIDENTIFIER NOT NULL,
    [ProjectId]                 UNIQUEIDENTIFIER NULL,
    [RegionId]                  UNIQUEIDENTIFIER NOT NULL,
    [CountryId]                 UNIQUEIDENTIFIER NULL,
    [ValueConversionId]         UNIQUEIDENTIFIER NULL,
    [CurrencyId]                UNIQUEIDENTIFIER NOT NULL,
    [Value]                     DECIMAL (19, 4)  NULL,
    [Volume]                    DECIMAL (19, 4)  NULL,
    [AvgSalePrice]              DECIMAL (19, 4)  NULL,
    [GrowthRatePercentage]      DECIMAL (19, 4)  NULL,
    [TrendGrowthRatePercentage] DECIMAL (19, 4)  NULL,
    [Amount]                    DECIMAL (19, 4)  NOT NULL,
    [Year]                      INT              NOT NULL,
    [GraphOptions]              NVARCHAR (512)   NOT NULL,
    [UserDeletedById]           UNIQUEIDENTIFIER NULL,
    [CAGR_3Years]               DECIMAL (19, 4)  NULL,
    [CAGR_5Years]               DECIMAL (19, 4)  NULL,
    [TotalCAGR]                 DECIMAL (19, 4)  NULL,
    [Source1]                   NVARCHAR (MAX)   NULL,
    [Source2]                   NVARCHAR (MAX)   NULL,
    [Source3]                   NVARCHAR (MAX)   NULL,
    [Source4]                   NVARCHAR (MAX)   NULL,
    [Source5]                   NVARCHAR (MAX)   NULL,
    [Rationale]                 NVARCHAR (MAX)   NULL,
    [VolumeUnitId]              UNIQUEIDENTIFIER NULL,
    [ASPUnitId]                 UNIQUEIDENTIFIER NULL,
    [CreatedOn]                 DATETIME2 (7)    NULL,
    [UserCreatedById]           UNIQUEIDENTIFIER NULL,
    [ModifiedOn]                DATETIME2 (7)    NULL,
    [UserModifiedById]          UNIQUEIDENTIFIER NULL,
    [IsDeleted]                 BIT              NOT NULL,
    [DeletedOn]                 DATETIME2 (7)    NULL,
    [StagingId]                 UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_MarketValueDup] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_MarketValueDup_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_MarketValueDup_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_MarketValueDup_Currency] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[Currency] ([Id]),
    CONSTRAINT [FK_MarketValueDup_Market] FOREIGN KEY ([MarketId]) REFERENCES [dbo].[Market] ([Id]),
    CONSTRAINT [FK_MarketValueDup_Region] FOREIGN KEY ([RegionId]) REFERENCES [dbo].[Region] ([Id]),
    CONSTRAINT [FK_MarketValueDup_ValueConversion] FOREIGN KEY ([ValueConversionId]) REFERENCES [dbo].[ValueConversion] ([Id])
);




















GO
CREATE NONCLUSTERED INDEX [IX_MarketValue_Project_Year_Country]
    ON [dbo].[MarketValue]([ProjectId] ASC, [Year] ASC)
    INCLUDE([RegionId], [CountryId], [ValueConversionId], [VolumeUnitId], [ASPUnitId], [Value], [Volume], [AvgSalePrice], [GrowthRatePercentage]);


GO
CREATE NONCLUSTERED INDEX [IX_MarketValue_Project]
    ON [dbo].[MarketValue]([ProjectId] ASC)
    INCLUDE([ValueConversionId], [VolumeUnitId], [ASPUnitId]);

