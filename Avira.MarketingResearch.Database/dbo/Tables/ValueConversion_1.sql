﻿CREATE TABLE [dbo].[ValueConversion] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [ValueName]        NVARCHAR (256)   CONSTRAINT [DF_ValueConversion_ValueName] DEFAULT ('') NOT NULL,
    [Conversion]       DECIMAL (19, 4)  NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_ValueConversion_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_ValueConversion] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ValueConversion_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_ValueConversion_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id])
);

