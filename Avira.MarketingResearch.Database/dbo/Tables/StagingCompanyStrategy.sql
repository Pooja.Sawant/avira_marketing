﻿CREATE TABLE [dbo].[StagingCompanyStrategy] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [ImportId]         UNIQUEIDENTIFIER NULL,
    [Date]             DATETIME         NULL,
    [OrganicInorganic] NVARCHAR (255)   NULL,
    [Strategy]         NVARCHAR (255)   NULL,
    [Category]         NVARCHAR (50)    NULL,
    [Importance]       NVARCHAR (4)     NULL,
    [Impact]           NVARCHAR (20)    NULL,
    [Remarks]          NVARCHAR (512)   NULL,
    [ApproachId]       UNIQUEIDENTIFIER NULL,
    [CategoryId]       UNIQUEIDENTIFIER NULL,
    [ImportanceId]     UNIQUEIDENTIFIER NULL,
    [ImpactId]         UNIQUEIDENTIFIER NULL,
    [CompanyId]        UNIQUEIDENTIFIER NULL,
    [ErrorNotes]       NVARCHAR (1000)  NULL,
    [RowNo]            INT              NULL,
    CONSTRAINT [PK_StagingCompanyStrategy] PRIMARY KEY CLUSTERED ([Id] ASC)
);



