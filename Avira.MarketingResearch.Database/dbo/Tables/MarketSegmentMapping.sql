﻿CREATE TABLE [dbo].[MarketSegmentMapping] (
    [ID]               UNIQUEIDENTIFIER NOT NULL,
    [MarketID]         UNIQUEIDENTIFIER NULL,
    [SegmentType]      NVARCHAR (100)   NULL,
    [SegmentID]        UNIQUEIDENTIFIER NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_MarketSegmentMapping] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK__MarketSegmentMapping__Market] FOREIGN KEY ([MarketID]) REFERENCES [dbo].[Market] ([Id]),
    CONSTRAINT [FK_MarketSegmentMapping_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_MarketSegmentMapping_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_MarketSegmentMapping_AviraUser2] FOREIGN KEY ([UserDeletedById]) REFERENCES [dbo].[AviraUser] ([Id])
);

