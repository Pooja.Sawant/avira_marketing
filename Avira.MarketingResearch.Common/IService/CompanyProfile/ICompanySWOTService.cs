﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.CompanyProfile
{
    public interface ICompanySWOTService
    {
        //IEnumerable<CompanySWOTOpportunitiesViewModel> GetOpportunitiesById(Guid? guid);
        //IEnumerable<CompanySWOTStrengthViewModel> GetStrengthById(Guid? guid);
        //IEnumerable<CompanySWOTThreatsViewModel> GetThreatsById(Guid? guid);
        MainCompanySWOTViewModel GetAll(Guid CompanyId,Guid ProjectId);
        Result<SpTransactionMessage> InsertUpdate(MainCompanySWOTViewModel viewModel, AviraUser currenUser);
    }
}
