﻿CREATE TABLE [dbo].[Permission] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [PermissionName]   NVARCHAR (256)   CONSTRAINT [DF_Permission_PermissionName] DEFAULT ('') NOT NULL,
    [UserType]         TINYINT          NOT NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_Permission_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_Permission] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Permission_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_Permission_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [IX_Permission] UNIQUE NONCLUSTERED ([PermissionName] ASC)
);




GO

GO

GO

GO

GO

GO

GO