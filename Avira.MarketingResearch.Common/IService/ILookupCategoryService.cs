﻿using Avira.MarketingResearch.Model.ViewModels.Masters;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService
{
    public interface ILookupCategoryService
    {
        IEnumerable<CategoryViewModel> GetAll(string categoryType);
    }
}
