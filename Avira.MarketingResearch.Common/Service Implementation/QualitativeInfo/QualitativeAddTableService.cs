﻿using Avira.MarketingResearch.Common.IService.QualitativeInfo;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.QualitativeInfo;
using Avira.MarketingResearch.Repository.CoreInterfaces.QualitativeInfo;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.QualitativeInfo
{
    public class QualitativeAddTableService : IQualitativeAddTableService
    {
        private readonly IQualitativeAddTableRepository _qualitativeAddTableRepository;
        private readonly IQualitativeNoteRepository _iQualitativeNoteRepository;

        public QualitativeAddTableService(IQualitativeAddTableRepository QualitativeAddTableRepository, IQualitativeNoteRepository QualitativeNoteRepository)
        {
            _qualitativeAddTableRepository = QualitativeAddTableRepository;
            _iQualitativeNoteRepository = QualitativeNoteRepository;
        }

        public IEnumerable<QualitativeAddTableViewModel> GetAll(Guid Id)
        {
            return _qualitativeAddTableRepository.GetAll(Id);
        }

        public Result<SpTransactionMessage> InsertUpdate(QualitativeGenerateTableCRUDViewModel viewModel)
        {
            Result<SpTransactionMessage> res = null;
            if (viewModel.Id==Guid.Empty)
            {
                viewModel.Id = Guid.NewGuid();
                res = _qualitativeAddTableRepository.Insert(viewModel);
            }
            else
            {
                res = _qualitativeAddTableRepository.Update(viewModel);
            }
            //var res = _qualitativeAddTableRepository.InsertUpdate(viewModel);
            viewModel.QualitativeNoteViewModel.Id = Guid.NewGuid();
            viewModel.QualitativeNoteViewModel.UserCreatedById = viewModel.AviraUserId;
            viewModel.QualitativeNoteViewModel.QualitativeId = viewModel.Id;
            viewModel.QualitativeNoteViewModel.ProjectId = viewModel.ProjectId;
            _iQualitativeNoteRepository.Insert(viewModel.QualitativeNoteViewModel);

            return res;
        }
    }
}
