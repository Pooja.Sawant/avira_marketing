﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrendKeywordMapController : AviraBaseController
    {
        private readonly ITrendKeywordMapService _trendKeywordMapService;
        private readonly ITrendKeywordMapRepository _trendKeywordMapRepository;
        private ILoggerManager _logger;

        public TrendKeywordMapController(
            ITrendKeywordMapService trendKeywordMapService,
            ITrendKeywordMapRepository trendKeywordMapRepository,
             ILoggerManager logger)
        {
            _trendKeywordMapRepository = trendKeywordMapRepository;
            _trendKeywordMapService = trendKeywordMapService;
            _logger = logger;

        }


        [HttpGet]
        public ActionResult<IEnumerable<TrendKeyWordMapViewModel>> Get(int PageStart = 0, int pageSize = 0, int SortDirection = 1, string OrdbyByColumnName = "CreatedDate", string Search = "", bool isDeletedInclude = false)
        {
            List<TrendKeyWordMapViewModel> res = null;
            try
            {
                res = _trendKeywordMapService.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpPost]
        public ActionResult Post([FromBody] TrendKeyWordMapInsertDbViewModel1 viewModel)
        {
            viewModel.CreatedOn = System.DateTime.UtcNow;
            viewModel.UserCreatedById = CurrentUser.Id;
            //viewModel.Id = Guid.NewGuid();

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while KeyWord Trend Mapping, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                var result = _trendKeywordMapService.Create(viewModel, CurrentUser);
                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Trend Keyword " + viewModel.TrendKeyWord + " is inserted successfully.";
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }

        [HttpPut]
        public ActionResult Put([FromBody] TrendKeyWordMapUpdateDbViewModel viewModel)
        {
            viewModel.ModifiedOn = System.DateTime.UtcNow;
            viewModel.UserModifiedById = CurrentUser.Id;

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while updating a new TrendKeywordMap, please contact Admin for more details."
            };
            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                Result<SpTransactionMessage> result = _trendKeywordMapService.Update(viewModel, CurrentUser);
                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "   " + viewModel.TrendKeyWord + " is Updated successfully.";
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Ok(viewData);
        }

        //[HttpGet]
        //[Route("GetAllTrendNames")]
        //public ActionResult<IEnumerable<TrendViewModel>> GetAllRoles()
        //{
        //    return _trendKeywordMapService.GetAllTrendNames().ToList();
        //}

        [HttpGet]
        [Route("GetAllTrendNames")]
        public ActionResult<IEnumerable<TrendKeywordNames>> GetAllRoles()
        {
            List<TrendKeywordNames> res = null;
            try
            {
                res = _trendKeywordMapService.GetAllTrendNames().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        //[HttpGet("{id}")]
        //public ActionResult<TrendKeyWordMapViewModel> Get(Guid id)
        //{
        //    TrendKeyWordMapViewModel trendKeyword = _trendKeywordMapService.GetById(id);
        //    if (trendKeyword == null)
        //    {
        //        return NotFound();
        //    }

        //    return trendKeyword;
        //}

        [Route("DeleteSegment")]
        [HttpPut]
        public ActionResult DeleteSegment([FromBody] TrendKeyWordMapUpdateDbViewModel viewModel)
        {
            viewModel.DeletedOn = System.DateTime.UtcNow;
            viewModel.UserDeletedById = CurrentUser.Id;
            viewModel.IsDeleted = true;

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while deleting a new Trend Keyword, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }

                Result<SpTransactionMessage> result = _trendKeywordMapService.Update(viewModel, CurrentUser);

                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;

                    viewData.Message = "TrendKeyword name " + viewModel.TrendKeyWord + " is deleted.";

                }
                else
                {
                    viewData.Message = result.Error;
                }
            }

            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }

        [HttpGet]
        [Route("GetByTrendID")]
        public ActionResult<IEnumerable<TrendKeyWordMapViewModel>> Get(Guid TrendId)
        {
            List<TrendKeyWordMapViewModel> res = null;
            try
            {
                res = _trendKeywordMapService.GetById(TrendId).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet]
        [Route("GetTrendNameByID")]
        public ActionResult<TrendKeywordNames> GetTrendNameByID(Guid TrendId)
        {
            TrendKeywordNames res = null;
            try
            {
                res = _trendKeywordMapService.GetTrendNameByID(TrendId);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

    }
}
