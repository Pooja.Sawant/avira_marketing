﻿CREATE PROCEDURE [dbo].[SApp_GetAllDesignations]

AS
/*================================================================================
Procedure Name: [SApp_GetAllDesignations]
Author: Sai Krishna
Create date: 21/05/2019
Description: Get list from Designation
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
21/05/2019	Sai Krishna		Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT Id AS DesignationId,
	   Designation,
	   Description 
FROM Designation
Where IsDeleted = 0 
 
END