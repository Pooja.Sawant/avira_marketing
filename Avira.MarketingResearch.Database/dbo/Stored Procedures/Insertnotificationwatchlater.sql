﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Insertnotificationwatchlater]
	-- Add the parameters for the stored procedure here
@ActionName	varchar(1000),
@IsBroadCosted	bit = 1,
@AssignedById	uniqueidentifier,
@AssignedToId	uniqueidentifier,
@Description	nvarchar(1000) ,
@LowId	uniqueidentifier,
@HighId	uniqueidentifier,
@CreatedOn	datetime,
@UserCreatedById	uniqueidentifier,
@UserId	uniqueidentifier
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  INSERT INTO [dbo].[NotificationWatcher]
            
			([NotificationWatcherID]
           ,[ActionName]
           ,[IsBroadCosted]
           ,[AssignedById]
           ,[AssignedToId]
           ,[Description]
           ,[LowId]
           ,[HighId]
           ,[CreatedOn]
           ,[UserCreatedById])
     VALUES
           (newid(),
           @ActionName,
           @IsBroadCosted,
            @UserId,
           @AssignedToId,
           @Description,
           @LowId,
           @HighId,
          getdate(),
           @UserCreatedById
           )
	
END