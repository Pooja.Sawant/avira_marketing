﻿CREATE TABLE [dbo].[DecisionParameter] (
    [Id]                      UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [DecisionId]              UNIQUEIDENTIFIER NOT NULL,
    [BaseDecisionParameterId] UNIQUEIDENTIFIER NULL,
    [Description]             NVARCHAR (1024)  NOT NULL,
    [CategoryId]              UNIQUEIDENTIFIER NOT NULL,
    [RatingId]                UNIQUEIDENTIFIER NOT NULL,
    [Weightage]               DECIMAL (18, 2)  NOT NULL,
    [CreatedOn]               DATETIME2 (7)    DEFAULT (getdate()) NOT NULL,
    [UserCreatedById]         UNIQUEIDENTIFIER NULL,
    [ModifiedOn]              DATETIME2 (7)    NULL,
    [UserModifiedById]        UNIQUEIDENTIFIER NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([BaseDecisionParameterId]) REFERENCES [dbo].[BaseDecisionParameter] ([Id]),
    FOREIGN KEY ([CategoryId]) REFERENCES [dbo].[LookupCategory] ([Id]),
    FOREIGN KEY ([RatingId]) REFERENCES [dbo].[LookupCategory] ([Id]),
    FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK__DecisionP__Decis__0D717793] FOREIGN KEY ([DecisionId]) REFERENCES [dbo].[Decision] ([Id])
);

