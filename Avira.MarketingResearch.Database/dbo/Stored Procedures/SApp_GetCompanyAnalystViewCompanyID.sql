﻿
/*================================================================================      
Procedure Name: [SApp_GetCompanyTrendsMapByCompanyID]   
Author: Harshal      
Create date: 05/08/2019      
Description: Get Records froms ComapnyTrendsMap By CompanyId      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
05/08/2019 Harshal   Initial Version     
================================================================================*/  
CREATE PROCEDURE [dbo].[SApp_GetCompanyAnalystViewCompanyID]     
	@CompanyID uniqueidentifier , 
	@ProjectID uniqueidentifier , 
	@includeDeleted bit = 0     
AS
BEGIN 
	SET NOCOUNT ON;  
	SELECT [CompanyAnalystView].[Id]
	  ,[AnalystView]
      ,[CompanyId]
	  ,[ProjectId]
      ,[CreatedOn]
      ,[UserCreatedById]
      ,[ModifiedOn]
      ,[UserModifiedById]
      ,[IsDeleted]
      ,[DeletedOn]
      ,[UserDeletedById]
  FROM [dbo].[CompanyAnalystView]
  
  WHERE (CompanyId = @CompanyID and ProjectId=@ProjectID)      
 AND (@includeDeleted = 0 or IsDeleted = 1)  
END