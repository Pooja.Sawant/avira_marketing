﻿using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.CompanyProfile
{
    public interface ICompanyTrackerService
    {
        MainCompanyTrackerViewModel GetAll(Guid ProjectId, int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false);
        CompanyTrackerViewModel GetById(Guid guid);
    }
}
