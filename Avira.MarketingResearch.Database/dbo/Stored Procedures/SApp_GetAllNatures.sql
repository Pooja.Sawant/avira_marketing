﻿Create procedure [dbo].[SApp_GetAllNatures]
as
/*================================================================================
Procedure Name: [SApp_GetAllNatures]
Author: Sai Krishna
Create date: 26/06/2019
Description: Get list from ImpactDirection
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
26/06/2019	Sai Krishna		Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT Id as NatureId,
       NatureTypeName as NatureType
FROM Nature
WHERE IsDeleted = 0
END