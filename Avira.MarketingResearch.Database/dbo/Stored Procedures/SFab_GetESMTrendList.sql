﻿CREATE PROCEDURE [dbo].[SFab_GetESMTrendList]    
(  
	@ProjectId uniqueidentifier, 
	@SegmentId uniqueidentifier,
	@SubSegmentIdList [dbo].[udtUniqueIdentifier] READONLY,--Optional
	@AviraUserID uniqueidentifier = null    
)    
as    
	
	--Declare @ProjectId uniqueidentifier = '5A264349-7408-47B2-996C-8E73CDF126DC', 
	--	@SegmentId uniqueidentifier = '57D02A9D-1EF9-4A0B-BBC6-040AE0CA3E25',
	--	@SubSegmentIdList [dbo].[udtUniqueIdentifier],--Optional
	--	@AviraUserID uniqueidentifier = null    
/*================================================================================    
Procedure Name: SFab_GetESMTrendList   
Author: Praveen    
Create date: 12/09/2019    
Description: To return all trends which are mapped with ecosystem mapping based on the segmentId and projectId. And optional input values for subsegmentIds
Change History    
Date  Developer  Reason  
__________ ____________ ______________________________________________________    
12/09/2019  Praveen   Initial Version    
================================================================================*/    
BEGIN    
------------------Sudo Code-----------------
	--Required Tables: ESMSegmentMapping, TrendEcoSystemMap and Trend
	--Notes: in the TrendEcoSystemMap.EcoSystemId is the placeholder for SegmentID from Segment Table
	--Case 1: if @SubSegmentIdList is not null then it should return data based on the segmentID and SubSegmentIdList for particular project
	--Case 2: if @SubSegmentIdList is null then it should return data based on the segmentID level for particular project
	--Expected Columns : TrendId and TrendName	
	
	Declare @CheckSubsegments bit = 0

	If exists(Select 1 From @SubSegmentIdList)
	Begin
		
		Set @CheckSubsegments = 1
	End
	Select Distinct Top 10 T.Id As TrendId, T.TrendName,imp.GroupName as Importance, 
	  impact.ImpactDirectionName As Nature,TT.TimeTagName As Outlook,
			Case  When imp.GroupName = 'High' Then 1
				  When imp.GroupName = 'Mid' Then 2
				  When imp.GroupName = 'Low' Then 3
				  Else 4
			End				   --, CESM.ESMId, CESM.SubSegmentId, QESM.SegmentId, QESM.SubSegmentId
	From Trend T
		Inner Join TrendEcoSystemMap TESM On
			T.Id = TESM.TrendId And
			T.ProjectId = @ProjectId And
			TESM.EcoSystemId = @SegmentId			
		Inner Join ESMSegmentMapping ESM On 
			TESM.EcoSystemId = ESM.SegmentId			
		Inner Join Importance Imp On
			T.ImportanceId = Imp.Id
		Inner JOIN ImpactDirection impact On
		    T.ImpactDirectionId= impact.Id
		Inner Join TrendTimeTag TTT On
		    T.id=TTT.TrendId
		Inner Join TimeTag TT On
		   TTT.TimeTagId=TT.Id
	Where (@CheckSubsegments = 0) Or 
			(@CheckSubsegments = 1 And TESM.SubSegmentId Is Null) Or
			(@CheckSubsegments = 1 And TESM.SubSegmentId Is Not Null And TESM.SubSegmentId In (Select Id From @SubSegmentIdList))	
	Order By Case When imp.GroupName = 'High' Then 1
				  When imp.GroupName = 'Mid' Then 2
				  When imp.GroupName = 'Low' Then 3
				  Else 4
			 End, TrendName 
	
	

		--Select Distinct Top 10 T.Id As TrendId, T.TrendName, Imp.Sequence --, CESM.ESMId, CESM.SubSegmentId, QESM.SegmentId, QESM.SubSegmentId
		--From Trend T
		--	Inner Join TrendEcoSystemMap TESM On
		--		T.Id = TESM.TrendId And
		--		T.ProjectId = @ProjectId And
		--		TESM.EcoSystemId = @SegmentId And
		--		TESM.SubSegmentId Is Null
		--	Inner Join ESMSegmentMapping ESM On 
		--		TESM.EcoSystemId = ESM.SegmentId
		--	Inner Join @SubSegmentIdList SS On
		--		TESM.SubSegmentId = ESM.SubSegmentId And TESM.SubSegmentId = SS.ID
		--	Inner Join Importance Imp On
		--		T.ImportanceId = Imp.Id
		--Order By Imp.Sequence Desc
	
END