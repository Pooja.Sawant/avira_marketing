﻿using Avira.MarketingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.Trend
{
    public class TrendMarketMapService : ITrendMarketMapService
    {
        private readonly ITrendMarketMapRepository _trendMarketRepository;
        private readonly ITrendNoteRepository _iTrendNoteRepository;

        public TrendMarketMapService(ITrendMarketMapRepository trendEcoSystemMapRepository, ITrendNoteRepository iTrendNoteRepository)
        {
            _trendMarketRepository = trendEcoSystemMapRepository;
            _iTrendNoteRepository = iTrendNoteRepository;
        } 
        /// <summary>
        /// To get all market with mapped/unmapped status by trend id
        /// </summary>
        /// <param name="TrendIdint"></param>
        /// <param name="PageStart"></param>
        /// <param name="pageSize"></param>
        /// <param name="SortDirection"></param>
        /// <param name="OrdbyByColumnName"></param>
        /// <param name="Search"></param>
        /// <param name="isDeletedInclude"></param>
        /// <returns></returns>
        public IEnumerable<TrendMarketMapViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false, string id="")
        {
            return _trendMarketRepository.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude = false, id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tfc"></param>
        /// <param name="currenUser"></param>
        /// <returns></returns>
        public Result<SpTransactionMessage> InsertUpdate(MainTrendMarketMapInsertUpdateDbViewModel tfc, AviraUser currenUser)
        {
         
            var res= _trendMarketRepository.InsertUpdate(tfc);
            tfc.TrendNoteModel.Id = Guid.NewGuid();
            tfc.TrendNoteModel.UserCreatedById = tfc.AviraUserId;
            tfc.TrendNoteModel.TrendId = tfc.TrendId;
            _iTrendNoteRepository.Insert(tfc.TrendNoteModel);

            return res;
        }


    }
}
