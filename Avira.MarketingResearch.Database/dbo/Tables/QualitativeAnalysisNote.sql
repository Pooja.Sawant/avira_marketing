﻿CREATE TABLE [dbo].[QualitativeAnalysisNote] (
    [Id]                      UNIQUEIDENTIFIER NOT NULL,
    [QualitativeAnalysisType] NVARCHAR (512)   NOT NULL,
    [ProjectId]               UNIQUEIDENTIFIER NOT NULL,
    [AuthorRemark]            NVARCHAR (512)   NULL,
    [ApproverRemark]          NVARCHAR (512)   NULL,
    [QualitativeStatusID]     UNIQUEIDENTIFIER NULL,
    [CreatedOn]               DATETIME2 (7)    NULL,
    [UserCreatedById]         UNIQUEIDENTIFIER NULL,
    [ModifiedOn]              DATETIME2 (7)    NULL,
    [UserModifiedById]        UNIQUEIDENTIFIER NULL,
    [IsDeleted]               BIT              NOT NULL,
    [DeletedOn]               DATETIME2 (7)    NULL,
    [UserDeletedById]         UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_QualitativeAnalysisNote] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_QualitativeAnalysisNote_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_QualitativeAnalysisNote_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_QualitativeAnalysisNote_Project] FOREIGN KEY ([ProjectId]) REFERENCES [dbo].[Project] ([ID]),
    CONSTRAINT [FK_QualitativeAnalysisNote_QualitativeStatus] FOREIGN KEY ([QualitativeStatusID]) REFERENCES [dbo].[QualitativeStatus] ([Id])
);

