﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.CompanyProfile
{
    public class CompanyProfitAndLossViewModel
    {
        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
        public Guid CurrencyId { get; set; }
        public Guid ValueConversionId { get; set; }
        public string CompanyName { get; set; }
        public string Currency { get; set; }
        public string Units { get; set; }
        public int Year { get; set; }
        public decimal Revenue { get; set; }
        public decimal CostsAndExpenses { get; set; }
        public decimal CostOfProduction { get; set; }
        public decimal ResourceCost { get; set; }
        public decimal SalaryCost { get; set; }
        public decimal CostOfSales { get; set; }
        public decimal GrossProfit { get; set; }
        public decimal SellingExpenses { get; set; }
        public decimal MarketingExpenses { get; set; }
        public decimal AdministrativeExpenses { get; set; }
        public decimal GeneralExpenses { get; set; }
        public decimal SellingGeneralAndAdminExpenses { get; set; }
        public decimal ResearchAndDevelopmentExpenses { get; set; }
        public decimal Depreciation { get; set; }
        public decimal Amortization { get; set; }
        public decimal Restructuring { get; set; }
        public decimal OtherIncomeOrDeductions { get; set; }
        public decimal TotalOperatingExpenses { get; set; }
        public decimal EBIT { get; set; }
        public decimal EBITDA { get; set; }
        public decimal InterestIncome { get; set; }
        public decimal InterestExpense { get; set; }
        public decimal EBT { get; set; }
        public decimal Tax { get; set; }
        public decimal NetIncomeFromContinuingOperations { get; set; }
        public decimal DiscontinuedOperations { get; set; }
        public decimal IncomeLossFromDiscontinuedOperations { get; set; }
        public decimal GainOnDisposalOfDiscontinuedOperations { get; set; }
        public decimal DiscontinuedOperationsNetOfTax { get; set; }
        public decimal NetIncomeBeforeAllocationToNoncontrollingInterests { get; set; }
        public decimal NonControllingInterests { get; set; }
        public decimal NetIncome { get; set; }
        public decimal BasicEPS_ { get; set; }
        public decimal BasicEPSFromContinuingOperations { get; set; }
        public decimal BasicEPSFromDiscontinuedOperations { get; set; }
        public decimal BasicEPS { get; set; }
        public decimal DilutedEPS_ { get; set; }
        public decimal DilutedEPSFromContinuingOperations { get; set; }
        public decimal DilutedEPSFromDiscontinuedOperations { get; set; }
        public decimal DilutedEPS { get; set; }
        public decimal WeightedAverageSharesOutstandingBasic { get; set; }
        public decimal WeightedAverageSharesOutstandingDiluted { get; set; }
        public DateTime? CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid UserModifiedById { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid UserDeletedById { get; set; }
    }

    public class MainCompanyProfitAndLossViewModel
    {
        public Guid ProjectId { get; set; }
        public Guid CompanyId { get; set; }
        public List<CompanyProfitAndLossViewModel> CompanyProfitAndLossViewModelList { get; set; }
        public CompanyNoteViewModel CompanyNoteViewModel { get; set; }
        public List<int> YearList { get; set; }
    }



}
