﻿CREATE TABLE [dbo].[CompanyEcosystemSegmentMap] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [CompanyId]        UNIQUEIDENTIFIER NOT NULL,
    [SegmentId]        UNIQUEIDENTIFIER NOT NULL,
    [SubSegmentId]     UNIQUEIDENTIFIER NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CompanyEcosystemSegmentMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CompanyEcosystemSegmentMap_Segment] FOREIGN KEY ([SegmentId]) REFERENCES [dbo].[Segment] ([Id]),
    CONSTRAINT [FK_CompanyEcosystemSegmentMap_SubSegment] FOREIGN KEY ([SubSegmentId]) REFERENCES [dbo].[SubSegment] ([Id]),
    CONSTRAINT [IX_CompanyEcosystemSegmentMap] UNIQUE NONCLUSTERED ([CompanyId] ASC, [SegmentId] ASC, [SubSegmentId] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'This is the placeholder for SegmentID from Segment Table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'CompanyEcosystemSegmentMap', @level2type = N'COLUMN', @level2name = N'SegmentId';

