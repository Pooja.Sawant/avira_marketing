﻿create procedure [dbo].[SApp_DeleteMarketbyID]
(@MarketID uniqueidentifier,
@AviraUserId uniqueidentifier)
as
/*================================================================================
Procedure Name: SApp_DeleteMarketbyID
Author: Sharath
Create date: 02/21/2019
Description: soft delete a record from Market table by ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
02/21/2019	Sharath			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


UPDATE [dbo].[Market]
   SET [IsDeleted] = 1,
   [DeletedOn] = getdate(),
   [UserDeletedById] = @AviraUserId
where ID = @MarketID
and IsDeleted = 0 -- no need to delete record which is already deleted

end