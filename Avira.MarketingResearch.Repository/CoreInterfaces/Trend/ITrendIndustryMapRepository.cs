﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.Trend
{
    public interface ITrendIndustryMapRepository
    {
        /// <summary>
        /// 
        /// </summary> 
        /// <param name="PageStart"></param>
        /// <param name="pageSize"></param>
        /// <param name="SortDirection"></param>
        /// <param name="OrdbyByColumnName"></param>
        /// <param name="Search"></param>
        /// <param name="includeDeleted"></param>
        /// <returns></returns> 
        IEnumerable<TrendIndustryMapViewModel> GetAll(int pageStart, int pageSize, int sortDirection, string ordbyByColumnName, string search, bool isDeletedInclude, string id = "");

        /// <summary>
        /// 
        /// </summary>
        /// <param name="trendIndustryMapInsertDbViewModel"></param>
        /// <returns></returns>
        Result<SpTransactionMessage> Create(TrendIndustryMapInsertDbViewModel trendIndustryMapInsertDbViewModel); 
         
    }
}
