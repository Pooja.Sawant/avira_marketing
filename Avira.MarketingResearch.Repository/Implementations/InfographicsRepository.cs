﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations
{
    public class InfographicsRepository : IInfographicsRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "Infographics";
        public InfographicsRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public InfographicsViewModel GetById(Guid guid)
        {
            InfographicsViewModel InfographicsViewModel = new InfographicsViewModel();
            InfographicsViewModel = GetAllInforGgraphics(guid).FirstOrDefault();
            return InfographicsViewModel;
        }

        public IEnumerable<InfographicsViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, Guid? projectId = null, Guid? lookupCategoryId = null)
        {
            return GetAllInforGgraphics(null, PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, projectId, lookupCategoryId);
        }

        private IEnumerable<InfographicsViewModel> GetAllInforGgraphics(Guid? guid, int PageStart = 0, int pageSize = 0, int SortDirection = -1, string OrdbyByColumnName = "", string Search = "", Guid? projectId = null, Guid? lookupCategoryId = null)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<InfographicsViewModel>(InfographicsQueries.StoreProcSelectAll
                            , new { @Id = guid, @ProjectId = projectId, @LookupCategoryId = lookupCategoryId, @PageStart = PageStart, @pageSize = pageSize, @SortDirection = SortDirection, @OrdbyByColumnName = OrdbyByColumnName, @Title = Search }
                            , null, true, null, CommandType.StoredProcedure);
        }

        public Result<SpTransactionMessage> CreateUpdate(InfographicsViewModel viewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(InfographicsQueries.StoreProcInsertUpdate, new
            {

                @Id = viewModel.Id,
                @ProjectId = viewModel.ProjectId,
                @LookupCategoryId = viewModel.LookupCategoryId,
                @InfogrTitle = viewModel.InfogrTitle,
                @InfogrDescription = viewModel.InfogrDescription,
                @SequenceNo = viewModel.SequenceNo,
                @ImageActualName = viewModel.ImageActualName,
                @ImageName = viewModel.ImageName,
                @ImagePath = viewModel.ImagePath,
                @AviraUserId = viewModel.UserCreatedById
            }, CommandType.StoredProcedure);
            return spTransactionMessage;
        }

        public Result<SpTransactionMessage> Delete(Guid Id)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(InfographicsQueries.StoreProcInsertDelete, new
            {

                @Id = Id
            }, CommandType.StoredProcedure);
            return spTransactionMessage;
        }
    }
}
