﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.ProjectDashBoard
{
    public interface IProjectAnalistDetailsRepository
    {
        ProjectAnalistDetailsViewModel GetById(Guid guid);
        IEnumerable<ProjectAnalistDetailsViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted);
        Result<SpTransactionMessage> Create(ProjectAnalistDetailsInsertDbViewModel viewModel);
        Result<SpTransactionMessage> Update(ProjectAnalistDetailsUpdateDbViewModel viewModel);
    }
}
