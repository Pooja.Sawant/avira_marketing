﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RevenueProfitAnalysisController : AviraBaseController
    {
        private readonly IRevenueProfitAnalysisService _revenueProfitAnalysisService;
        private ILoggerManager _logger;
        public RevenueProfitAnalysisController(
            IRevenueProfitAnalysisService revenueProfitAnalysisService, ILoggerManager logger)
        {
            _revenueProfitAnalysisService = revenueProfitAnalysisService;
            _logger = logger;
        }
        //public IActionResult Index()
        //{
        //    return View();
        //}

        [HttpGet]
        [Route("GetAllCurrencies")]
        public ActionResult<IEnumerable<CurrencyMapViewModel>> GetAllCurrencies()
        {
            List<CurrencyMapViewModel> res = null;
            try
            {
                res = _revenueProfitAnalysisService.GetAllCurrencies().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet]
        [Route("GetAllValueConversion")]
        public ActionResult<IEnumerable<ValueConversionViewModel>> GetAllValueConversion()
        {
            List<ValueConversionViewModel> res = null;
            try
            {
                res = _revenueProfitAnalysisService.GetAllValueConversion().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpGet]
        [Route("GetAllRegions")]
        public ActionResult<IEnumerable<RegionMapViewModel>> GetAllRegions()
        {
            List<RegionMapViewModel> res = null;
            try
            {
                res = _revenueProfitAnalysisService.GetAllRegions().ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        [HttpPost]
        public ActionResult Post([FromBody] MainRevenueProfitAnalysisInsertUpdateDbViewModel viewModel)
        {
            viewModel.UserCreatedById = CurrentUser.Id;
            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a new Revenue & Profit Analysis, please contact Admin for more details."
            };
            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                var result = _revenueProfitAnalysisService.InsertUpdate(viewModel, CurrentUser);
                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "Company Revenue & Profit Analasis is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return Ok(viewData);
        }
    }
}