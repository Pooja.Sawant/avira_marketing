﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels
{
    public class ServiceViewModel
    {
        public string ServiceName { get; set; }
        public string Description { get; set; }
        public Guid? ParentServiceId { get; set; }
        public string ParentServiceName { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        public Guid Id { get; set; }
        public int TotalItems { get; set; }
        public string RowType
        {
            get
            {
                return "Service";
            }
        }

    }

    public class ServiceInsertDbViewModel
    {
        [Required]
        [Display(Name = "Service Name")]
        public string ServiceName { get; set; }
        public string Description { get; set; }
        public Guid? ParentServiceId { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid UserCreatedById { get; set; }
        public Guid Id { get; set; }
    }

    public class ServiceUpdateDbViewModel
    {
        [Required]
        [Display(Name = "Service Name")]
        public string ServiceName { get; set; }
        public string Description { get; set; }
        public Guid? ParentServiceId { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? UserModifiedById { get; set; }
        public bool IsDeleted { get; set; } = false;
        public DateTime? DeletedOn { get; set; }
        public Guid? UserDeletedById { get; set; }
        [Required]
        public Guid Id { get; set; }
    }
}
