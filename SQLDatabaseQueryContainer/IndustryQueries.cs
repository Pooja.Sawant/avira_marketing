﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public static class IndustryQueries
    {
        public static readonly string StoreProcInsert = "SApp_insertIndustry";

        public static readonly string StoreProcUpdate = "SApp_UpdateIndustry";

        public static string StoreProcSelectAll = "SApp_IndustryGridWithFilters";

        //public static string StoreProcSelectAllParent = "SApp_GetIndustrybyID";
        public static string StoreProcSelectAllParent = "SApp_GetAllParentIndustry";
    }
}
