﻿  
  
CREATE PROCEDURE [dbo].[SApp_GetUserRoles]  
 (  
  @includeDeleted bit = 0  
 )  
 AS  
/*================================================================================  
Procedure Name: [SApp_GetUserRoles]  
Author: Gopi  
Create date: 04/03/2019  
Description: Get list of roles from table   
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
04/03/2019 Gopi   Initial Version  
================================================================================*/  
  
BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
  
 SELECT [UserRole].[Id],  
  [UserRole].[UserRoleName],  
  [UserRole].[UserType],  
  [UserRole].[IsDeleted]  
FROM [dbo].[UserRole]  
where ([UserRole].IsDeleted = 0)   
 END