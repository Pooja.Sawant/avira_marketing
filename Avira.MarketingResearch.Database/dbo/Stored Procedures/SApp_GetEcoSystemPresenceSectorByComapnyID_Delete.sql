﻿

      
 CREATE PROCEDURE [dbo].[SApp_GetEcoSystemPresenceSectorByComapnyID_Delete]            
 (@CompanyID uniqueidentifier = null)  
 AS            
/*================================================================================            
Procedure Name:[SApp_GetEcoSystemPresenceSectorByComapnyID]       
Author: Harshal            
Create date: 05/20/2019            
Description: Get list from EcoSystem table by companyId            
Change History            
Date  Developer  Reason            
__________ ____________ ______________________________________________________            
04/22/2019 Harshal   Initial Version 
================================================================================*/           
            
BEGIN            
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED         

begin        
      
 SELECT         
   [Industry].[Id] AS [IndustryId],            
   [Industry].[IndustryName],           
   [Industry].IsDeleted,        
   CompanyEcosystemPresenceSectorMap.isChecked,      
   CompanyEcosystemPresenceSectorMap.CompanyId,      
   CompanyEcosystemPresenceSectorMap.Id as CompanyEcosystemPresenceSectorMapId,      
  cast (case when exists(select 1 from dbo.CompanyEcosystemPresenceSectorMap         
     where CompanyEcosystemPresenceSectorMap.CompanyId = @CompanyID        
     and CompanyEcosystemPresenceSectorMap.IsDeleted = 0        
     and CompanyEcosystemPresenceSectorMap.SectorId = Industry.Id) then 1 else 0 end as bit) as IsMapped       
  
        
FROM [dbo].Industry      
LEFT JOIN dbo.CompanyEcosystemPresenceSectorMap      
ON Industry.Id = CompanyEcosystemPresenceSectorMap.SectorId
and CompanyEcosystemPresenceSectorMap.CompanyId = @CompanyID
and CompanyEcosystemPresenceSectorMap.IsDeleted = 0
where ( Industry.IsDeleted = 0)        
END       
        
END