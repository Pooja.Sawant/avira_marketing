﻿
CREATE procedure [dbo].[SSec_GetUserLogin]
(
@UserName nvarchar(100)
)
as
/*================================================================================
Procedure Name: SSec_GetUserLogin
Author: Laxmikant
Create date: 02/11/2019
Description: Get user login details by Name
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
03/04/2019	Laxmikant			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT [Id], 
		[UserName],
		[Salt],
		[PassWord],
		UserRoleId,
		IsUserLocked
FROM [dbo].[AviraUser]
where (UserName = @UserName or EmailAddress = @UserName) 
and IsDeleted = 0 AND UserTypeId = 1
--and IsUserLocked = 0;
end