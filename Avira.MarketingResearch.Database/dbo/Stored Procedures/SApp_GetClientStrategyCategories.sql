﻿CREATE PROCEDURE [dbo].[SApp_GetClientStrategyCategories]        
 (        
  @includeDeleted bit = 0        
 )        
 AS        
/*================================================================================        
Procedure Name: [SApp_GetClientStrategyCategories]        
Author: Laxmikant        
Create date: 05/07/2019        
Description: Get list of Client Strategy Categories from table         
Change History        
Date  Developer  Reason        
__________ ____________ ______________________________________________________        
05/07/2019 Gopi   Initial Version        
================================================================================*/        
        
BEGIN        
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED        
        
 SELECT [Id]
      ,[CategoryName]
      ,[Description]       
 FROM [dbo].[Category]
where ([Category].IsDeleted = 0)         
END