﻿CREATE TABLE [dbo].[MMM_Variables] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [PageLevel]        VARCHAR (100)    NOT NULL,
    [SectionName]      VARCHAR (255)    NOT NULL,
    [VariableName]     VARCHAR (255)    NOT NULL,
    [SortOrder]        SMALLINT         CONSTRAINT [DF_MMM_Variables_SortOrder] DEFAULT ((0)) NOT NULL,
    [CreatedOn]        DATETIME2 (7)    NOT NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_MMM_Variables] PRIMARY KEY CLUSTERED ([Id] ASC)
);

