﻿--sp_helptext SApp_InsertRegion

CREATE procedure [dbo].[SApp_insertRegion]      
(@Id uniqueidentifier,  
@RegionName nvarchar(256),      
@RegionLevel int,  
@ParentRegionId uniqueidentifier = null,  
@CountryIDList dbo.udtUniqueIdentifier READONLY,  
@UserCreatedById uniqueidentifier,   
@CreatedOn Datetime  
)      
as      
/*================================================================================      
Procedure Name: SApp_insertRegion      
Author: Sharath      
Create date: 03/19/2019      
Description: Insert a record into Region table and Map it to countries  
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
03/19/2019 Sharath   Initial Version      
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)  
    
--Validations      
IF exists(select 1 from [dbo].[Region]       
   where RegionName = @RegionName  
   and (ParentRegionId = @ParentRegionId or (ParentRegionId is null and @ParentRegionId is null)  AND IsDeleted = 0
   ))      
begin      
--declare @ErrorMsg NVARCHAR(2048);      
set @ErrorMsg = 'Region with Name "'+ @RegionName + '" already exists.';      
--THROW 50000,@ErrorMsg,1;      
--return(1)      
SELECT StoredProcedureName ='SApp_insertRegion',Message =@ErrorMsg,Success = 0;    
RETURN;    
end      
--End of Validations      
BEGIN TRY         
         
insert into [dbo].[Region]([Id],      
        [RegionName],      
        [RegionLevel],  
  [ParentRegionId],      
        [IsDeleted],      
        [UserCreatedById],      
        [CreatedOn])      
 values(@Id,       
   @RegionName,       
   @RegionLevel,  
   @ParentRegionId,       
   0,      
   @UserCreatedById,       
   @CreatedOn);      
     
INSERT INTO [dbo].[CountryRegionMap]  
  ([Id],  
  CountryId,  
  RegionId,   
  [IsDeleted],  
  [UserCreatedById],   
  [CreatedOn])   
   SELECT NEWID() as ID,  
   list.ID as CountryId,   
   @Id as RegionId,  
   0 as [IsDeleted],   
   @UserCreatedById as [UserCreatedById],   
   @CreatedOn as [CreatedOn]  
   FROM @CountryIDList list  
   WHERE NOT EXISTS (SELECT 1 FROM [dbo].[CountryRegionMap] lkup(NOLOCK)  
      WHERE list.id = lkup.CountryId  
      AND LKUP.RegionId = @Id);  
  
  
--select @RegionID;      
--return(0)      
SELECT StoredProcedureName ='SApp_insertRegion',Message =@ErrorMsg,Success = 1;       
    
END TRY      
BEGIN CATCH      
    -- Execute the error retrieval routine.      
 DECLARE @ErrorNumber int;      
 DECLARE @ErrorSeverity int;      
 DECLARE @ErrorProcedure varchar(100);      
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);      
      
  SELECT @ErrorNumber = ERROR_NUMBER(),      
        @ErrorSeverity = ERROR_SEVERITY(),      
        @ErrorProcedure = ERROR_PROCEDURE(),      
        @ErrorLine = ERROR_LINE(),      
        @ErrorMessage = ERROR_MESSAGE()      
      
 insert into dbo.Errorlog(ErrorNumber,      
       ErrorSeverity,      
       ErrorProcedure,      
       ErrorLine,      
       ErrorMessage,      
       ErrorDate)      
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,@CreatedOn);      
    
 set @ErrorMsg = 'Error while creating a new Region, please contact Admin for more details.';      
--THROW 50000,@ErrorMsg,1;        
--return(1)    
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;       
       
END CATCH      
      
      
end