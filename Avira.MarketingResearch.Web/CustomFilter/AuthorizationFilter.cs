﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using static Avira.MarketingResearch.Model.ViewModels.PermissionsByUserRoleIdViewModel;

namespace Avira.MarketingResearch.Web.CustomFilter
{
    public class AuthorizationFilter : IAuthorizationFilter
    {
        readonly Claim _claim;

        public AuthorizationFilter(Claim claim)
        {
            _claim = claim;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (context.HttpContext.Session.GetComplexData("Username") == string.Empty)
            {
                context.Result = new RedirectResult("~/Account/Login"); //Session expired so redirecting to login page.
            }
            else
            {
                var formName = context.RouteData.Values["controller"].ToString().ToUpper();
                List<PermissionByForm> FormsList = context.HttpContext.Session.GetComplexData<List<PermissionByForm>>("FormsList");
                var hasClaim = (FormsList != null && FormsList.Count() > 0 && FormsList.Any(x => x.FormName.ToUpper().Equals(formName)));
                if (!hasClaim)
                {
                    context.HttpContext.Response.Redirect("/Unauthorized.html");
                }
            }
                
        }
    }
}
