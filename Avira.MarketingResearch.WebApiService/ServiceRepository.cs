﻿using Avira.MarketingResearch.Web.Helpers;
using Microsoft.Extensions.Configuration;
using System;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Avira.MarketingResearch.WebApiService
{
    public class ServiceRepository : IServiceRepository
    {
        protected IAPIHelper _apiHelper;
        public HttpClient Client { get; set; }

        public ServiceRepository(IAPIHelper apiHelper)
        {
            _apiHelper = apiHelper;
            Client = new HttpClient();
            Client.Timeout = TimeSpan.FromMinutes(59); 

            Client.BaseAddress = new Uri(_apiHelper.GetBaseURL());
        }

        public HttpResponseMessage PostResponse(string segment, object model)
        {
            string segmentURL = _apiHelper.GetCreateSegmentURL(segment);
            return Client.PostAsJsonAsync(segmentURL, model).Result;
        }
        public HttpResponseMessage PostResponseWithFile(string segment, object model)
        {
            string segmentURL = _apiHelper.GetCreateSegmentURL(segment);
            string _ContentType = "multipart/form-data";
            Client.DefaultRequestHeaders.Add("enctype", "multipart/form-data");//.Accept.Add(new MediaTypeWithQualityHeaderValue(_ContentType));
            return Client.PostAsJsonAsync(segmentURL, model).Result;
        }
        
        public HttpResponseMessage GetByIdResponse(string segment, string guid)
        {
            string segmentURL = _apiHelper.GetReadByIdSegmentURL(segment) + guid;
            return Client.GetAsync(segmentURL).Result;
        }
        public HttpResponseMessage GetProjectByIdResponse(string segment, string guid, Guid AviraUserId)
        {
            //string segmentURL = _apiHelper.GetReadByIdSegmentURL(segment) + guid;
            //return Client.GetAsync(segmentURL).Result;
            string segmentURL = _apiHelper.GetReadProjectSegmentURL(segment) + "?Id=" + guid + "&AviraUserId=" + AviraUserId ;
            return Client.GetAsync(segmentURL).Result;
        }
        public HttpResponseMessage GetByIdResponse(string segment, string guid, string type)
        {
            string segmentURL = _apiHelper.GetReadByIdSegmentURL(segment) + "?companyId=" + guid + "&type=" + type;
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetQualitativeNoteByIdResponse(string segment, string guid, string guid2)
        {
            string segmentURL = _apiHelper.GetReadByIdSegmentURL(segment) + "?Id=" + guid + "&projectId=" + guid2;
            return Client.GetAsync(segmentURL).Result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="segment"></param>
        /// <param name="guid"></param>
        /// <param name="type"></param>
        /// <param name="idType">This parameter is determine how it search</param>
        /// <returns></returns>
        public HttpResponseMessage GetByIdResponse(string segment, string guid, string type, string idType)
        {
            string segmentURL = _apiHelper.GetReadByIdSegmentURL(segment) + "?" + idType + "=" + guid + "&type=" + type;
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetCompanyNoteByIdResponse(string segment, string guid, string type, string guid2)
        {
            string segmentURL = _apiHelper.GetReadByIdSegmentURL(segment) + "?CompanyId=" + guid + "&type=" + type + "&projectId=" + guid2;
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetByGuIdResponse(string segment, Guid guid)
        {
            string segmentURL = _apiHelper.GetReadSegmentURL(segment) + "?aviraUserId=" + guid;
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetResponse(string segment, int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string SearchData)
        {
            string segmentURL = _apiHelper.GetReadSegmentURL(segment) + "?PageStart=" + PageStart + "&pageSize=" + pageSize + "&SortDirection=" + SortDirection + "&OrdbyByColumnName=" + OrdbyByColumnName + "&Search=" + SearchData;
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetResponseWithMethodName(string segment, string methodName, string id = "")
        {
            string segmentURL = _apiHelper.GetReadSegmentURL(segment, methodName) + "?Id=" + id;
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetResponse(string segment, Guid Id, int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string SearchData)
        {
            string segmentURL = _apiHelper.GetReadSegmentURL(segment) + "?Id=" + Id + "&PageStart=" + PageStart + "&pageSize=" + pageSize + "&SortDirection=" + SortDirection + "&OrdbyByColumnName=" + OrdbyByColumnName + "&Search=" + SearchData;
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetResponse(string segment, int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string SearchData, Guid projectId, Guid categoryId)
        {
            string segmentURL = _apiHelper.GetReadSegmentURL(segment) + "?PageStart=" + PageStart + "&pageSize=" + pageSize + "&SortDirection=" + SortDirection + "&OrdbyByColumnName=" + OrdbyByColumnName + "&Search=" + SearchData + "&ProjectId=" + projectId + "&lookupCategoryId=" + categoryId;
            return Client.GetAsync(segmentURL).Result;
        }


        public HttpResponseMessage GetResponseByUserId(string segment, Guid Id, Guid AviraUserId, int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string SearchData)
        {
            string segmentURL = _apiHelper.GetReadSegmentURL(segment) + "?Id=" + Id + "&AviraUserId="+ AviraUserId + "&PageStart=" + PageStart + "&pageSize=" + pageSize + "&SortDirection=" + SortDirection + "&OrdbyByColumnName=" + OrdbyByColumnName + "&Search=" + SearchData;
            return Client.GetAsync(segmentURL).Result;
        }
        public HttpResponseMessage GetResponse(string segment)
        {
            string segmentURL = _apiHelper.GetReadSegmentURL(segment);
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetResponse(string segment, string guid, string type)
        {
            string segmentURL = _apiHelper.GetReadSegmentURL(segment) + "?Id=" + guid + "&type=" + type;
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetAllParentResponse(string segment)
        {
            string segmentURL = _apiHelper.GetReadAllParentSegmentURL(segment);
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetAllCategoryResponse(string segment)
        {
            string segmentURL = _apiHelper.GetAllCategorySegmentURL(segment);
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage PutResponse(string segment, object model)
        {
            string segmentURL = _apiHelper.GetUpdateSegmentURL(segment);
            HttpResponseMessage response = Client.PutAsJsonAsync(segmentURL, model).Result;
            return response;

        }
        public HttpResponseMessage PutdataResponse(string segment, Guid id)
        {
            string segmentURL = _apiHelper.GetUpdateStatusSegmentURL(segment) + "?id=" + id;
            return Client.GetAsync(segmentURL).Result;

        }
        public HttpResponseMessage DeleteResponse(string segment, object model)
        {
            string segmentURL = _apiHelper.GetDeleteSegmentURL(segment);
            return Client.PutAsJsonAsync(segmentURL, model).Result;
        }

        public HttpResponseMessage GetSubResponse(string segment)
        {
            string segmentURL = _apiHelper.GetSubSegmentURL(segment);
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetPermissionByGuIdResponse(string segment, string guid)
        {
            string segmentURL = _apiHelper.GetReadSegmentURL(segment) + "/GetPermissionsByUserRoleId?id=" + guid;
            return Client.GetAsync(segmentURL).Result;
        }
        public HttpResponseMessage GetUserByRoleResponse(string segment, string roleName)
        {
            string segmentURL = _apiHelper.GetReadAllUserByRoleSegmentURL(segment) + "?roleName=" + roleName;
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetAllSegmentListIdResponse(string segment, string id, bool includeParentOnly, string segName)
        {
            string segmentURL;
            if (segName == "Industry")
            {
                segmentURL = _apiHelper.GetIndustrySegmentListIdSegmentURL(segment) + "?id=" + id + "&includeParentOnly=" + includeParentOnly;
            }
            else
            {
                segmentURL = _apiHelper.GetMarketSegmentListIdSegmentURL(segment) + "?id=" + id + "&includeParentOnly=" + includeParentOnly;
            }
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetTrendByIdResponse(string segment, Guid guid)
        {
            string segmentURL = _apiHelper.GetReadByIdSegmentURL(segment) + "?TrendId=" + guid;
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetTrendNameByIdResponse(string segment, Guid guid)
        {
            string segmentURL = _apiHelper.GetReadByTrendIdSegmentURL(segment) + "?TrendId=" + guid;
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetByAllResponse(string segment, object model)
        {
            string segmentURL = _apiHelper.GetByAllResponseSegmentURL(segment);
            return Client.PostAsJsonAsync(segmentURL, model).Result;
        }

        public HttpResponseMessage GetImportanceResponse(string segment)
        {
            string segmentURL = _apiHelper.GetImportanceResponse(segment);
            return Client.GetAsync(segmentURL).Result;
        }
        public HttpResponseMessage GetImpactDirectionResponse(string segment)
        {
            string segmentURL = _apiHelper.GetImpactDirectionResponse(segment);
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetShareVolByIdResponse(string segment, string guid)
        {
            string segmentURL = _apiHelper.GetShareVolByCompanyIdSegmentURL(segment) + "?CompanyId=" + guid;
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage SaveImageByIdResponse(string segment, object model)
        {
            string segmentURL = _apiHelper.SaveImagebyCompanyIdSegmentURL(segment);
            return Client.PostAsJsonAsync(segmentURL, model).Result;
        }

        public HttpResponseMessage GetAllImportanceByCompanyIdResponse(string segment)
        {
            string segmentURL = _apiHelper.GetAllImportanceByCompanyIdSegmentURL(segment);
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetAllImpactDirectionByCompanyIdResponse(string segment)
        {
            string segmentURL = _apiHelper.GetAllImpactDirectionByCompanyIdSegmentURL(segment);
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetAllApproachByCompanyIdSegmentResponse(string segment)
        {
            string segmentURL = _apiHelper.GetAllApproachByCompanyIdSegmentURL(segment);
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetAllCategoriesByCompanyIdSegmentResponse(string segment)
        {
            string segmentURL = _apiHelper.GetAllCategoriesByCompanyIdSegmentURL(segment);
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetAllIndustryByCompanyIdSegmentResponse(string segment)
        {
            string segmentURL = _apiHelper.GetAllIndustryByCompanyIdSegmentURL(segment);
            return Client.GetAsync(segmentURL).Result;
        }


        public HttpResponseMessage GetCompanyClientStrategyByCompanyId(string segment, string guid)
        {
            string segmentURL = _apiHelper.GetCompanyclientAndStrategyByIdURL(segment) + "?Id=" + guid;
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetAllStrategyByCompanyIdSegmentResponse(string segmentName, string guid)
        {
            string segmentURL = _apiHelper.GetCompanyStrategyByIdURL(segmentName) + "?Id=" + guid;
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetAllclientByCompanyIdSegmentResponse(string segmentName, string guid)
        {
            string segmentURL = _apiHelper.GetCompanyClientByIdURL(segmentName) + "?Id=" + guid;
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetCompanySWOTStrengthByCompanyID(string segment, string guid)
        {
            string segmentURL = _apiHelper.GetCompanySWOTStrengthURL(segment) + "?Id=" + guid;
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetCompanySWOTWeeknessesByCompanyID(string segment, string guid)
        {
            string segmentURL = _apiHelper.GetCompanySWOTWeeknessesURL(segment) + "?Id=" + guid;
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetCompanySWOTOpportunitiesByCompanyID(string segment, string guid)
        {
            string segmentURL = _apiHelper.GetCompanySWOTOpportunitiesURL(segment) + "?Id=" + guid;
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetCompanySWOTThreatsByCompanyID(string segment, string guid)
        {
            string segmentURL = _apiHelper.GetCompanySWOTThreatsURL(segment) + "?Id=" + guid;
            return Client.GetAsync(segmentURL).Result;
        }


        public HttpResponseMessage GetAllParentSegResponse(string segment)
        {
            string segmentURL = _apiHelper.GetAllParentSegmentURL(segment);
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetOtherFinancialByGuIdResponse(string segment, Guid guid)
        {
            string segmentURL = _apiHelper.GetReadSegmentURL(segment) + "?companyId=" + guid;
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetEcoSystemPresenceMapByCompanyId(string segment, Guid guid)
        {
            string segmentURL = _apiHelper.GetEcoSystemPresenceMapByCompanyIdURL(segment) + "?companyId=" + guid;
            return Client.GetAsync(segmentURL).Result;
        }
        public HttpResponseMessage GetSectorPresenceMapByCompanyId(string segment, Guid guid)
        {
            string segmentURL = _apiHelper.GetSectorPresenceMapByCompanyIdURL(segment) + "?companyId=" + guid;
            return Client.GetAsync(segmentURL).Result;
        }
        public HttpResponseMessage GetEcoSystemPresenceKeyCompByCompanyId(string segment, Guid guid)
        {
            string segmentURL = _apiHelper.GetEcoSystemPresenceKeyCompByCompanyIdURL(segment) + "?companyId=" + guid;
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetCompanyOrganizationChart(string segment, string guid)
        {
            string segmentURL = _apiHelper.GetCompanyOrganizationChartURL(segment) + "?Id=" + guid;
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetCompanyDetailsByGuIdsResponse(string segment, string guidone, string guidtwo)
        {
            string segmentURL = _apiHelper.GetAllCompanyDetailsURL(segment) + "?companyId=" + guidone + "&projectId=" + guidtwo;
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetAllCompaniesResponse(string segment)
        {
            string segmentURL = _apiHelper.GetAllCompaniesResponse(segment);
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetAllDesignationsResponse(string segment)
        {
            string segmentURL = _apiHelper.GetAllDesignationsResponse(segment);
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetAllCompanyStagesResponse(string segment)
        {
            string segmentURL = _apiHelper.GetAllCompanyStagesResponse(segment);
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetCompanyFundamentalResponse(string segment)
        {
            string segmentURL = _apiHelper.GetCompanyFundamentalResponse(segment);
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetCompanyRegionResponse(string segment)
        {
            string segmentURL = _apiHelper.GetCompanyRegionResponse(segment);
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetCompanyEntityResponse(string segment)
        {
            string segmentURL = _apiHelper.GetCompanyEntityResponse(segment);
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetCompanySubsidaryResponse(string segment, string guid)
        {
            string segmentURL = _apiHelper.GetCompanySubsidaryResponse(segment) + "?Id=" + guid;
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetCompanyOtherInfo(string segment, string guid)
        {
            string segmentURL = _apiHelper.GetCompanyOtherInfoByCompanyIdURL(segment) + "?Id=" + guid;
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetCompanyManagerResponse(string segment, string guid)
        {
            string segmentURL = _apiHelper.GetCompanyManagerResponse(segment) + "?companyId=" + guid;
            return Client.GetAsync(segmentURL).Result;
        }
        public HttpResponseMessage GetAllCurrencies(string segment)
        {
            string segmentURL = _apiHelper.GetAllCurrenciesURL(segment);
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetRevenueProfitBaseYear(string segment)
        {
            string segmentURL = _apiHelper.GetRevenueProfitBaseYearURL(segment);
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetAllRegions(string segment)
        {
            string segmentURL = _apiHelper.GetAllRegionsURL(segment);
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetAllValueConversion(string segment)
        {
            string segmentURL = _apiHelper.GetAllValueConversionURL(segment);
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetAllCategoryURL(string segment)
        {
            string segmentURL = _apiHelper.GetAllCategoryURL(segment);
            return Client.GetAsync(segmentURL).Result;
        }

        public HttpResponseMessage GetDynamicURL(string segment, string guid)
        {
            string segmentURL = _apiHelper.GetDynamicURL(segment) + "/GetByAviraUserId?userId=" + guid;
            return Client.GetAsync(segmentURL).Result;
        }


        public HttpResponseMessage GetCompanyNatureResponse(string segment)
        {
            string segmentURL = _apiHelper.GetCompanyNatureResponse(segment);
            return Client.GetAsync(segmentURL).Result;
        }

    }
}
