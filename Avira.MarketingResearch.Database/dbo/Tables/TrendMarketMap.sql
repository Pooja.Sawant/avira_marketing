﻿CREATE TABLE [dbo].[TrendMarketMap] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [TrendId]          UNIQUEIDENTIFIER NOT NULL,
    [MarketId]         UNIQUEIDENTIFIER NOT NULL,
    [Impact]           UNIQUEIDENTIFIER NULL,
    [EndUser]          BIT              NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_TrendMarketMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TrendMarketMap_CreatedUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_TrendMarketMap_ModifiedUser] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_TrendMarketMap_RegionId] FOREIGN KEY ([MarketId]) REFERENCES [dbo].[Market] ([Id]),
    CONSTRAINT [FK_TrendMarketMap_Trend] FOREIGN KEY ([TrendId]) REFERENCES [dbo].[Trend] ([Id])
);


GO

GO

GO

GO

GO

GO

GO

GO

GO