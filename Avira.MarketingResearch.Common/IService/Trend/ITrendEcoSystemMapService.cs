﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.Trend
{
    public interface ITrendEcoSystemMapService
    {
        IEnumerable<TrendEcoSystemMapViewModel> GetAll(Guid? Id,int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false);
        Result<SpTransactionMessage> InsertUpdate(MainTrendEcoSystemMapInsertUpdateDbViewModel tfc, AviraUser currenUser);
    }
}