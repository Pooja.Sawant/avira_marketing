﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile
{
    public interface ICompanySwotRepository
    {
        //IEnumerable<CompanySWOTOpportunitiesViewModel> GetOpportunitiesById(Guid? guid);
        //IEnumerable<CompanySWOTStrengthViewModel> GetStrengthById(Guid? guid);
        //IEnumerable<CompanySWOTThreatsViewModel> GetThreatsById(Guid? guid);
        //IEnumerable<CompanySWOTWeeknessesViewModel> GetWeeknessesById(Guid? guid);
        IEnumerable<CompanySWOTViewModel> GetAll(Guid CompanyId, Guid ProjectId);
        Result<SpTransactionMessage> InsertUpdate(MainCompanySWOTViewModel viewModel, AviraUser currenUser);
    }
}
