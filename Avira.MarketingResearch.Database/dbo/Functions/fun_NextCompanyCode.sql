﻿CREATE function [dbo].[fun_NextCompanyCode]() 
returns char(8) 
as 
begin 
	declare @lastval char(8) 
	set @lastval = (select max(CompanyCode) from Company with (nolock)) 
	if @lastval is null set @lastval = 'C0000001' 
	declare @i int 
	set @i = right(@lastval,7) + 1 
	return 'C' + right('000000' + convert(varchar(10),@i),7) 
end