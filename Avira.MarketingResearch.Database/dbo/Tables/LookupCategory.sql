﻿CREATE TABLE [dbo].[LookupCategory] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [CategoryType]     NVARCHAR (50)    NOT NULL,
    [CategoryName]     NVARCHAR (256)   CONSTRAINT [DF_LookupCategory_CategoryName] DEFAULT ('') NOT NULL,
    [Description]      NVARCHAR (256)   NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_LookupCategory_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    [SeqNumber]        INT              NULL,
    CONSTRAINT [PK_LookupCategory] PRIMARY KEY CLUSTERED ([Id] ASC)
);



