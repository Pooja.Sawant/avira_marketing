﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Model.CustomValidation
{
    public class ValidateImageCustomAttribute : ValidationAttribute
    {
        int _maxContentLength =0;
        int _minContentLength = 0;
        string[] _allowedFileExtensions = { };
        int _imageHeightMax = 0;
        int _imageWidthMax = 0;
        int _imageHeightMin = 0;
        int _imageWidthMin = 0;
        public ValidateImageCustomAttribute(int maxContentLength, string[] allowedFileExtensions, int imageWidthMax, int imageHeightMax,  int minContentLength = 0, int imageWidthMin = 0, int imageHeightMin = 0)
        {
            _maxContentLength = maxContentLength;
            _minContentLength = minContentLength;
            _allowedFileExtensions = allowedFileExtensions;
            _imageHeightMax = imageHeightMax;
            _imageWidthMax = imageWidthMax;
            _imageHeightMin = imageHeightMin;
            _imageWidthMin = imageWidthMin;
        }
        public override bool IsValid(object value)
        {
            //int MaxContentLength = 1024 * 1024 * 1; //Max 1 MB file
            
            //string[] AllowedFileExtensions = new
            //    string[] { ".jpg", ".jpeg", ".gif", ".png" };

            var file = value as IFormFile;


            if (file == null)
                return true;
            else if (!_allowedFileExtensions.Contains((file != null) ?
                file.FileName.Substring(file.FileName.LastIndexOf('.')).ToLower()
                : string.Empty))
            {
                ErrorMessage = "Please upload Your Photo of type: " +
                    string.Join(", ", _allowedFileExtensions);
                return false;
            }
            else if (file.Length > _maxContentLength)
            {
                ErrorMessage = "Your Image is too large, maximum allowed size is : "
                       //+ (_maxContentLength / 1024).ToString() + "MB";
                       + (Math.Round((_maxContentLength* 0.0009765625), 2)) + " Kb. Current size is:" + (Math.Round((file.Length* 0.0009765625), 2)) + " Kb";
                return false;
            }
            else if (file.Length < _minContentLength)
            {
                ErrorMessage = "Your Image is too small, minimum allowed size is : "
                    // + Convert.ToDecimal(_minContentLength / 1048576).ToString() + "KB";
                    + (Math.Round((_minContentLength * 0.0009765625),2)) + " Kb. Current size is:" + (Math.Round((file.Length * 0.0009765625), 2)) + " Kb";
                return false;
            }
            else
            {
                var imageHeight = 0;
                var imageWidth = 0;
                using (var image = Image.FromStream(file.OpenReadStream()))
                {
                    imageHeight = image.Height;
                    imageWidth = image.Width;
                    // use image.Width and image.Height
                }

                if (imageHeight > _imageHeightMax || imageWidth > _imageWidthMax)
                {
                    ErrorMessage = "Your Image dimension is too large, maximum allowed size is " + _imageHeightMax + "px * "+ _imageWidthMax + "px ";
                    return false;
                }
                else if (imageHeight < _imageHeightMin || imageWidth < _imageWidthMin)
                {
                    ErrorMessage = "Your Image dimension is too small, minimum allowed size is " + _imageHeightMin + "px * " + _imageWidthMin + "px ";
                    return false;
                }
                
                else
                {
                    return true;
                }
            }

        }
    }
}
