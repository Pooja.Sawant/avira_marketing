﻿CREATE TABLE [dbo].[MessageTemplate] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [Type]             NVARCHAR (100)   CONSTRAINT [DF_MessageTemplate_Type] DEFAULT ('E') NOT NULL,
    [TemplateName]     NVARCHAR (300)   NOT NULL,
    [Subject]          NVARCHAR (500)   NULL,
    [Body]             NVARCHAR (MAX)   NULL,
    [IsActive]         BIT              CONSTRAINT [DF_MessageTemplate_IsActive] DEFAULT ((0)) NOT NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_MessageTemplate_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_MessageTemplate] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'E for Email, M for Mobile', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MessageTemplate', @level2type = N'COLUMN', @level2name = N'Type';

