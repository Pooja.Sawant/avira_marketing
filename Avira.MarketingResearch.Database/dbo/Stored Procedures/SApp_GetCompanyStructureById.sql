﻿--Param
CREATE PROCEDURE [dbo].[SApp_GetCompanyStructureById]            
(@Id uniqueidentifier)            
AS  
BEGIN     
     
Declare @CompanyId uniqueidentifier = @Id;

declare @ParentId uniqueidentifier;
declare @prevParentId uniqueidentifier;
select @ParentId = @CompanyId;

while(@ParentId is not null)
begin
set @prevParentId = @ParentId

select @ParentId = ParentId
from Company
where id = @ParentId 
end

set @ParentId = @prevParentId;

;WITH pcompany_src as (      
SELECT Id as CompanyId,
	CompanyName,    
	ParentId,
	1 as Generation
FROM Company      
where id = @ParentId
UNION ALL      
SELECT ChildCompany.Id as CompanyID,
	ChildCompany.CompanyName,      
	ChildCompany.ParentId,
	Generation + 1 as Generation
FROM Company ChildCompany      
INNER JOIN pcompany_src
ON pcompany_src.CompanyId = ChildCompany.ParentId      
)      
SELECT 
	CompanyId,      
	CompanyName,      
	ParentId,
	Generation
FROM pcompany_src 

END