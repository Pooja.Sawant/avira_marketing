﻿CREATE TABLE [dbo].[CompanyShareVolume] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [CompanyId]        UNIQUEIDENTIFIER NOT NULL,
    [CurrencyId]       UNIQUEIDENTIFIER NOT NULL,
    [Period]           INT              NOT NULL,
    [BaseDate]         DATETIME2 (7)    NULL,
    [Date]             INT              NULL,
    [Price]            DECIMAL (19, 4)  NOT NULL,
    [Volume]           DECIMAL (19, 4)  NOT NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CompanyShareVolumeTable] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CompanyShareVolume_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CompanyShareVolume_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [fk_CompanyShareVolume_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([Id]),
    CONSTRAINT [fk_CompanyShareVolume_Currency] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[Currency] ([Id])
);

