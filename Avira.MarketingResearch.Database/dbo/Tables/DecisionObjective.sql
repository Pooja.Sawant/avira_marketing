﻿CREATE TABLE [dbo].[DecisionObjective] (
    [Id]                UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [DecisionId]        UNIQUEIDENTIFIER NOT NULL,
    [BusinessObjective] NVARCHAR (1024)  NOT NULL,
    [CreatedOn]         DATETIME2 (7)    DEFAULT (getdate()) NOT NULL,
    [UserCreatedById]   UNIQUEIDENTIFIER NULL,
    [ModifiedOn]        DATETIME2 (7)    NULL,
    [UserModifiedById]  UNIQUEIDENTIFIER NULL,
    [SeqNumber]         SMALLINT         NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK__DecisionO__Decis__00177C75] FOREIGN KEY ([DecisionId]) REFERENCES [dbo].[Decision] ([Id])
);

