﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyProfitAndLossController : AviraBaseController
    {
        private readonly ICompanyProfitAndLossService _iCompanyProfitAndLossService;
        private ILoggerManager _logger;

        public CompanyProfitAndLossController(
           ICompanyProfitAndLossService iCompanyProfitAndLossAddTableService,
            ILoggerManager logger)
        {
            _iCompanyProfitAndLossService = iCompanyProfitAndLossAddTableService;
            _logger = logger;

        }

       
        [HttpGet("{id}")]
        public ActionResult<IEnumerable<CompanyProfitAndLossViewModel>> GetFinancial(Guid Id)
        {
            List<CompanyProfitAndLossViewModel> res = null;
            AviraUser user = new AviraUser();
            try
            {
                res = _iCompanyProfitAndLossService.GetCompanyProfitAndLossData(Id,user).ToList();

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);  

            }
            return res;
        }
    }
}