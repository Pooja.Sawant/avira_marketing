﻿CREATE PROCEDURE [dbo].[SFab_UpdateDecisionAlternativeTaskStatus]  
(  
@DecisionAlternativeTaskId uniqueidentifier,
@DecisionId uniqueidentifier,
@DecisionAlternativeId uniqueidentifier,
@StatusId uniqueidentifier,
@UserId uniqueidentifier  
)  
As     
  
/*================================================================================      
Procedure Name: SFab_UpdateDecisionAlternativeTaskStatus     
Author: Praveen      
Create date: 07-Jan-2020  
Description: Add upadate new task of the alternative
Change History      
Date  Developer  Reason    
__________ ____________ ______________________________________________________      
07-Jan-2020  Praveen   Initial Version 
03-Feb-2020  Praveen   Implementations
================================================================================*/      
BEGIN      

------------------  
 --Required Tables: Decision, DecisionAlternativeTask and LookupStatus

Declare @ErrorMsg NVARCHAR(2048)
Declare @DecisionStatusDecisionMadeId uniqueidentifier
	
	Select @DecisionStatusDecisionMadeId = ID From LookupStatus
	Where StatusName = 'Decision made' And StatusType = 'DMDecisionStatus'
	
	Declare @DecisionStatusDecisionOngoingId uniqueidentifier
	Select @DecisionStatusDecisionOngoingId = ID From LookupStatus
    Where StatusName = 'Ongoing' And StatusType = 'DMDecisionStatus'
	
	   
BEGIN TRY
	If Exists (Select ID from Decision Where Id = @DecisionId and UserId= @UserID and StatusId=@DecisionStatusDecisionMadeId)
	Begin
        	set @ErrorMsg = 'Can not add/edit any new details since this decision has been made.';   
        		SELECT StoredProcedureName ='SFab_UpdateDecisionAlternativeTaskStatus','Message' =@ErrorMsg,'Success' = 0;  
    End
	Else If NOT Exists (Select ID from Decision Where Id = @DecisionId and UserId= @UserID and StatusId=@DecisionStatusDecisionOngoingId)
    Begin
          set @ErrorMsg = 'Please change decision status to Ongoing in order to add/edit details.';  
          SELECT StoredProcedureName ='SFab_InsertDecisionAlternativeTask','Message' =@ErrorMsg,'Success' = 0;
    End
	Else
	Begin
	 
		If Exists(Select 0 
			From DecisionAlternativeTask
			INNER JOIN DecisionAlternative ON DecisionAlternativeTask.DecisionAlternativeId = DecisionAlternative.Id
			INNER JOIN Decision ON DecisionAlternative.DecisionId = Decision.Id
			 Where DecisionAlternativeTask.Id = @DecisionAlternativeTaskId AND DecisionAlternativeId = @DecisionAlternativeId)
		Begin
				UPDATE DecisionAlternativeTask SET				
				StatusId=@StatusId,				
				ModifiedOn=GETDATE(),
				UserModifiedById=@UserId,
				StatusModifiedOn= case when @StatusId=statusid then StatusModifiedOn else getdate() end
				WHERE Id = @DecisionAlternativeTaskId 
				AND DecisionAlternativeId = @DecisionAlternativeId
				AND DecisionId=@DecisionId
		End
	End
	SELECT StoredProcedureName ='SFab_UpdateDecisionAlternativeTaskStatus',Message =@ErrorMsg,Success = 1;
END TRY
BEGIN CATCH 
        EXEC SApp_LogErrorInfo

		Set @ErrorMsg = 'Error while inserting SFab_UpdateDecisionAlternativeTaskStatus, please contact Admin for more details.';  
		SELECT StoredProcedureName ='SFab_UpdateDecisionAlternativeTaskStatus', Message = @ErrorMsg, Success = 0;     
      
		
	END CATCH 
END