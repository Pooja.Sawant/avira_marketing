﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Model.ViewModels.ImportExport
{
    public class ImportResponse
    {
        public string ResponseStatus { get; set; }
        public string ResponseMessage { get; set; }
        public int TotalRecords { get; set; }
        public int TotalSuccessRecords { get; set; }
        public int TotalFailedRecords { get; set; }
        public string[] SuccessList { get; set; }
        public string[] FailedList { get; set; }
        public List<MarketSizingImportViewModel> GetMarketSizeErrorList { get; set; }
        public List<TrendImportViewModel> GetTrendErrorList { get; set; }
        public List<ImportCommonResponseModel> GetCompanyProfileErrorList { get; set; }
        public List<CPFinancialImportModel> GetCPFinancialErrorList { get; set; }
        public IEnumerable<ImportCommonResponseModel> ResponseModel { get; set; }
        public string ImportFor { get; set; }
    }
    public class UserInfo
    {
        public string UserName { get; set; }
        public int Age { get; set; }
    }

    public class ImportCommonResponseMainModel : IViewModel
    {
        public string Module { get; set; }
        public string ErrorNotes { get; set; }
        public string Success { get; set; }
        public IEnumerable<ImportCommonResponseModel> ResponseModel { get; set; }

        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public Guid Id { get; set; }

        public List<MarketSizingImportViewModel> Data { get; set; }
        public List<TrendImportViewModel> TrendData { get; set; }
        public List<ImportCommonResponseModel> cpData { get; set; }
        public List<CPFinancialImportModel> CPFData { get; set; }
        public List<ImportCommonResponseModel> CBIData { get; set; }
    }

    public class ImportCommonResponseModel
    {
        public int RowNo { get; set; }
        public string Module { get; set; }        
        public string ErrorNotes { get; set; }
        public string Success { get; set; }
    }
}
