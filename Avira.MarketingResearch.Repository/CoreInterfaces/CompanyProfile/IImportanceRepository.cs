﻿using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile
{
    public interface IImportanceRepository
    {
        IEnumerable<ImportanceViewModel> GetAll();
    }
}
