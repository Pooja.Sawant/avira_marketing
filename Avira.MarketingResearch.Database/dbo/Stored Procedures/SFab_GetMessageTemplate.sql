﻿
CREATE procedure [dbo].[SFab_GetMessageTemplate]
(@Type nvarchar(100),
@TemplateName nvarchar(200),
@AviraUserID uniqueidentifier = null)
as
/*================================================================================
Procedure Name: SFab_GetMessageTemplate
Author: Pooja Sawant
Create date: 05/13/2019
Description: Get message template by template name

Change History
Date		Developer		Reason
__________	____________	______________________________________________________
05/13/2019	Pooja			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT * FROM MessageTemplate
where Type = @Type 
and  TemplateName = @TemplateName
and IsDeleted = 0 
end