﻿using Avira.MarketingResearch.Common.IService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Repository;
using Avira.MarketingResearch.Repository.CoreInterfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation
{
    public class CountryService : ICountryService
    {
        private readonly ICountryRepository _countryRepository;

        public CountryService(ICountryRepository buildLevelRepository)
        {
            _countryRepository = buildLevelRepository;
        }
        public IEnumerable<CountryViewModel> GetAll(Guid Id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<CountryViewModel> GetAll(bool isDeletedInclude = false)
        {
            return _countryRepository.GetAll(isDeletedInclude);
        }

        public CountryViewModel GetById(Guid guid)
        {
            return _countryRepository.GetById(guid);
        }
    }
}
