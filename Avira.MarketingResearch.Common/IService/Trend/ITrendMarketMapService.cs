﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.Trend
{
    public interface ITrendMarketMapService
    {

        /// <summary>
        /// To get all market with mapped/unmapped status by trend id
        /// </summary> 
        /// <param name="PageStart"></param>
        /// <param name="pageSize"></param>
        /// <param name="SortDirection"></param>
        /// <param name="OrdbyByColumnName"></param>
        /// <param name="Search"></param>
        /// <param name="isDeletedInclude"></param>
        /// <returns></returns>
        IEnumerable<TrendMarketMapViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false, string id="");
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tfc"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        /// 
        Result<SpTransactionMessage> InsertUpdate(MainTrendMarketMapInsertUpdateDbViewModel tfc, AviraUser currenUser);
    }
}
