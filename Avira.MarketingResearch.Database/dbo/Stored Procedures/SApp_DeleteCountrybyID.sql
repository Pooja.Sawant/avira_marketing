﻿create procedure [dbo].[SApp_DeleteCountrybyID]
(@CountryID uniqueidentifier,
@AviraUserId uniqueidentifier)
as
/*================================================================================
Procedure Name: SApp_DeleteCountrybyID
Author: Sharath
Create date: 02/11/2019
Description: soft delete a record from Country table by ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
02/11/2019	Sharath			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


UPDATE [dbo].[Country]
   SET [IsDeleted] = 1,
   [DeletedOn] = getdate(),
   [UserDeletedById] = @AviraUserId
where ID = @CountryID
and IsDeleted = 0 -- no need to delete record which is already deleted

end