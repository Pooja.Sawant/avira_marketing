﻿using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.CompanyProfile
{
    public class CompanyNoteService:ICompanyNoteService
    {
        private readonly ICompanyNoteRepository _iCompanyNoteRepository;

        public CompanyNoteService(ICompanyNoteRepository iCompanyNoteRepository)
        {
            _iCompanyNoteRepository = iCompanyNoteRepository;
        }

        public CompanyNoteViewModel GetById(Guid companyId, string noteType, Guid? ProjectId)
        {
            return _iCompanyNoteRepository.GetById(companyId,noteType,ProjectId);
        } 
    }
}
