﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.Trend
{
    public interface ITrendRegionMapService
    {

        /// <summary>
        /// To get all Region with mapped/unmapped status by trend id
        /// </summary>
        /// <param name="TrendIdint"></param>
        /// <param name="PageStart"></param>
        /// <param name="pageSize"></param>
        /// <param name="SortDirection"></param>
        /// <param name="OrdbyByColumnName"></param>
        /// <param name="Search"></param>
        /// <param name="isDeletedInclude"></param>
        /// <returns></returns>
        IEnumerable<TrendRegionMapViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tfc"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        /// 
        Result<SpTransactionMessage> InsertUpdate(MainTrendRegionMapInsertUpdateDbViewModel tfc, AviraUser currenUser);
    }
}
