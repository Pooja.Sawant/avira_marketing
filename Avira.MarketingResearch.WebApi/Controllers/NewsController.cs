﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NewsController : AviraBaseController
    {
        private readonly INewsService _newsService;
        private ILoggerManager _logger;
        public NewsController(
            INewsService newsService, ILoggerManager logger)
        {
            _newsService = newsService;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<NewsViewModel>> Get(Guid companyId,Guid projectId)
        {
            List<NewsViewModel> res = null;
            try
            {
                res = _newsService.GetAll(companyId, projectId).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;          
        }

        [HttpPost]
        public ActionResult Post([FromBody] MainNewsInsertUpdateDbViewModel viewModel)
        {
            viewModel.CreatedOn = System.DateTime.UtcNow;
            viewModel.UserCreatedById = CurrentUser.Id;
            //viewModel.Id = Guid.NewGuid();

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating News, please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                var result = _newsService.InsertUpdate(viewModel, CurrentUser);
                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "News is inserted successfully.";
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }

        [HttpGet]
        [Route("GetAllNewsCategory")]
        public ActionResult<IEnumerable<NewsCategoryViewModel>> GetAllNewsCategory()
        {
            return _newsService.GetAllNewsCategory().ToList();
        }

        [HttpGet]
        [Route("GetAllImportance")]
        public ActionResult<IEnumerable<NewsImportanceViewModel>> GetAllImportance()
        {
            return _newsService.GetAllImportance().ToList();
        }

        [HttpGet]
        [Route("GetAllImpactDirection")]
        public ActionResult<IEnumerable<NewsImpactDirectionViewModel>> GetAllImpactDirection()
        {
            return _newsService.GetAllImpactDirection().ToList();
        }
    }
}