﻿
  
CREATE PROCEDURE [dbo].[SFab_GetAllImportanceGroup] 
(@UserId uniqueidentifier = null)   
 AS  
/*================================================================================  
Procedure Name: [SFab_GetAllImportance]  
Author: Praveen  
Create date: 05/25/2019  
Description: Get list of conversion values from table  
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
05/25/2019 Praveen   Initial Version  
================================================================================*/  
  
BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
	
	
	SELECT GroupName AS ImportanceName, MAX(sequence) sequence FROM Importance
	WHERE IsDeleted = 0 
	GROUP BY GroupName
	order by sequence 

END