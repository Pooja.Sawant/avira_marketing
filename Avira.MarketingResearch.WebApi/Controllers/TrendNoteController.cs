﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.Trend;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Mvc;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrendNoteController: AviraBaseController     
    {
        private ILoggerManager _logger;
        public readonly ITrendNoteService _trendNoteService;

        public TrendNoteController(ITrendNoteService trendNoteService,ILoggerManager logger)
        {
            _trendNoteService = trendNoteService;
            _logger = logger;
        }
        
        [HttpGet()]
        public ActionResult<TrendNoteViewModel> Get(Guid id, string type)
        {
            TrendNoteViewModel trendNote = null;
            try
            {
                trendNote = _trendNoteService.GetById(id, type);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return trendNote;
        }

        [HttpGet]
        [Route("GetAllByTrend")]
        public ActionResult<IEnumerable<TrendNoteViewModel>> GetAllByTrend(Guid id, string type)
        {
            List<TrendNoteViewModel> TrendList = null;
            try
            {
                TrendList = _trendNoteService.GetAllByTrendId(id, type).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
             return TrendList;
        }
    }
}
