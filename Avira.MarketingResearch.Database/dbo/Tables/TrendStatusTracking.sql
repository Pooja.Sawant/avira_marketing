﻿CREATE TABLE [dbo].[TrendStatusTracking] (
    [Id]              UNIQUEIDENTIFIER NOT NULL,
    [TrendNoteID]     UNIQUEIDENTIFIER NOT NULL,
    [TrendId]         UNIQUEIDENTIFIER NOT NULL,
    [TrendType]       NCHAR (512)      NOT NULL,
    [TrendStatusID]   UNIQUEIDENTIFIER NULL,
    [CreatedOn]       DATETIME2 (7)    NULL,
    [UserCreatedById] UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_TrendStatusTracking] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TrendStatusTracking_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_TrendStatusTracking_Trend] FOREIGN KEY ([TrendId]) REFERENCES [dbo].[Trend] ([Id]),
    CONSTRAINT [FK_TrendStatusTracking_TrendStatus] FOREIGN KEY ([TrendStatusID]) REFERENCES [dbo].[TrendStatus] ([Id])
);



