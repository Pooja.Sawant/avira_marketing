﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Avira.MarketingResearch.Common.IService.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Utility;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static Avira.MarketingResearch.Model.ViewModels.CompanyProfile.CompanyProductCountryMapViewModel;

namespace Avira.MarketingResearch.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyProductCountryMapController : AviraBaseController
    {
        private readonly ICompanyProductCountryMapService _companyProductCountryMapService;
        private readonly ICompanyProductCountryMapRepository _companyProductCountryMapRepository;
        private ILoggerManager _logger;

        public CompanyProductCountryMapController(
            ICompanyProductCountryMapService companyProductCountryMapService,
            ICompanyProductCountryMapRepository companyProductCountryMapRepository,
             ILoggerManager logger)
        {
            _companyProductCountryMapRepository = companyProductCountryMapRepository;
            _companyProductCountryMapService = companyProductCountryMapService;
            _logger = logger;

        }

        [HttpGet]
        public ActionResult<IEnumerable<CompanyProductCountryMapViewModel>> Get(Guid? Id)
        {
            List<CompanyProductCountryMapViewModel> res = null;
            try
            {
                 res = _companyProductCountryMapService.GetAll(Id).ToList();
        }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }


        [HttpPost]
        public ActionResult Post([FromBody] CompanyProductViewMoel viewModel)
        {
            viewModel.CreatedOn = System.DateTime.UtcNow;
            viewModel.UserCreatedById = CurrentUser.Id;

            IViewModel viewData = new ViewData()
            {
                IsSuccess = false,
                Message = "Error while creating a adding CompanyProductCountryMap  , please contact Admin for more details."
            };

            try
            {
                if (!ModelState.IsValid)
                {
                    viewData.Message = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                    return Ok(viewData);
                }
                var result = _companyProductCountryMapService.InsertUpdate(viewModel, CurrentUser);
                if (result != null && result.IsSuccess)
                {
                    viewData.IsSuccess = true;
                    viewData.Message = "CompanyProductCountryMap is created successfully."; ;
                }
                else
                {
                    viewData.Message = result.Error;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok(viewData);
        }
    }
}