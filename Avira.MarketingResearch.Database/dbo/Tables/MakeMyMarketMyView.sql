﻿CREATE TABLE [dbo].[MakeMyMarketMyView] (
    [Id]          UNIQUEIDENTIFIER NOT NULL,
    [ProjectId]   UNIQUEIDENTIFIER NULL,
    [CustomerId]  UNIQUEIDENTIFIER NULL,
    [Year]        INT              NULL,
    [MyViewValue] DECIMAL (18, 4)  NULL,
    [MarketId]    UNIQUEIDENTIFIER NULL,
    [CAGR]        DECIMAL (18, 4)  NULL,
    [CreatedOn]   DATETIME         NULL,
    [CreatedBy]   UNIQUEIDENTIFIER NULL,
    [ModifiedOn]  DATETIME         NULL,
    [ModifiedBy]  UNIQUEIDENTIFIER NULL,
    [IsDeleted]   BIT              NULL,
    [SegmentId]   UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_MakeMyMarketMyView] PRIMARY KEY CLUSTERED ([Id] ASC)
);

