﻿CREATE TABLE [dbo].[NotificationList] (
    [ID]          INT            IDENTITY (1, 1) NOT NULL,
    [Text]        NVARCHAR (MAX) NOT NULL,
    [UserID]      NVARCHAR (50)  NOT NULL,
    [CreatedDate] DATETIME       CONSTRAINT [DF_NotificationList_CreatedDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_NotificationList] PRIMARY KEY CLUSTERED ([ID] ASC)
);

