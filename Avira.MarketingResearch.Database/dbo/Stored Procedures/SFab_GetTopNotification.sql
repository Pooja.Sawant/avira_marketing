﻿CREATE PROCEDURE [dbo].[SFab_GetTopNotification]      
( 
 @AssignedToId UNIQUEIDENTIFIER  
)  
As      
  
/*================================================================================      
Procedure Name: SFab_GetTopNotification     
Author: Pooja Sawant      
Create date: 25-Feb-2020  
Description: To return count of NotificationWatcher by assigntoid
Change History      
Date  Developer  Reason    
__________ ____________ ______________________________________________________      
25-Feb-2020  Pooja S   Initial Version      
================================================================================*/      
BEGIN   

select NotificationWatcherID,ActionName,IsBroadCosted,Description from NotificationWatcher where IsBroadCosted = 0 and ModifiedOn is null and AssignedToId = @AssignedToId 
    
END