﻿
CREATE procedure [dbo].[SApp_insertApplication]
(
@ApplicationName nvarchar(256),
@Description nvarchar(256),
@UserCreatedById uniqueidentifier,  
@Id uniqueidentifier,  
@CreatedOn Datetime)
as
/*================================================================================
Procedure Name: SApp_insertApplication
Author: Adarsh
Create date: 03/16/2019
Description: Insert a record into Application table
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
03/16/2019	Adarsh			Initial Version
03/25/2019  Harshal         1) All the paramter of Model should be decalre as input parameter   
                               with default value is null if those parameter is not mandetory.      
                            2) Return should select with all the value of standard Model that we are using in Code
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ErrorMsg NVARCHAR(2048);
declare @SegmentId uniqueidentifier;
select @SegmentId = Id from Segment
where SegmentName = 'Applications';

--Validations
IF exists(select 1 from [dbo].SubSegment 
		 where SubSegmentName = @ApplicationName 
		 and SegmentId = @SegmentId
		 AND IsDeleted = 0)
begin
--declare @ErrorMsg NVARCHAR(2048);
set @ErrorMsg = 'Application with Name "'+ @ApplicationName + '" already exists.';
THROW 50000,@ErrorMsg,1;
return(1)
end
--End of Validations
BEGIN TRY

insert into [dbo].SubSegment([Id],
								[SegmentId],
								[SubSegmentName],
								[Description],
								[IsDeleted],
								[UserCreatedById],
								[CreatedOn])
	values(@Id, 
			@SegmentId,
			@ApplicationName, 
			@Description, 
			0,
			@UserCreatedById, 
			@CreatedOn);

--select @Id;
--return(0)
SELECT StoredProcedureName ='SApp_insertApplication',Message =@ErrorMsg,Success = 1;  
END TRY
BEGIN CATCH
    -- Execute the error retrieval routine.
	DECLARE @ErrorNumber	int;
	DECLARE @ErrorSeverity	int;
	DECLARE @ErrorProcedure	varchar;
	DECLARE @ErrorLine	int;
	DECLARE @ErrorMessage	varchar;

	 SELECT @ErrorNumber = ERROR_NUMBER(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorProcedure = ERROR_PROCEDURE(),
        @ErrorLine = ERROR_LINE(),
        @ErrorMessage = ERROR_MESSAGE()

	insert into dbo.Errorlog(ErrorNumber,
							ErrorSeverity,
							ErrorProcedure,
							ErrorLine, 
							ErrorMessage,
							ErrorDate)
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,@CreatedOn);

	set @ErrorMsg = 'Error while creating a new Application, please contact Admin for more details.';

SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;

END CATCH


end