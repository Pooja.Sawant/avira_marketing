﻿CREATE PROCEDURE [dbo].[SApp_GetAllEcoSystemPresancesByProjectCompanyId]
(
	  @ProjectId UniqueIdentifier=null,
	  @CompanyId UniqueIdentifier
)
as
/*================================================================================
Procedure Name: [SApp_GetAllEcoSystemPresancesByProjectCompanyId]
Author: Sai Krishna
Create date: 18/05/2019
Description: Get list from EcoSystemPresanceMap by Project Id and CompanyId
Change History
Date		Developer		Reason
__________	____________ ______________________________________________________
18/05/2019	Sai Krishna		Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

Select cepesmm.ESMId
from CompanyEcosystemPresenceESMMap cepesmm
where cepesmm.CompanyId = @CompanyId
and IsDeleted = 0

Select cepsm.SectorId
from CompanyEcosystemPresenceSectorMap cepsm
where cepsm.CompanyId = @CompanyId
and IsDeleted = 0

Select cepcm.KeyCompetiters
from CompanyEcosystemPresenceKeyCompetitersMap cepcm
where cepcm.CompanyId = @CompanyId
and IsDeleted = 0

END