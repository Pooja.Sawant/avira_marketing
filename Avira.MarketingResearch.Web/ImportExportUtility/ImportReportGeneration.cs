﻿using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Avira.MarketingResearch.Web.ImportExportUtility
{
    public class ImportReportGeneration : IExcelImport
    {
        public readonly IServiceRepository _serviceRepository;
        private ILoggerManager _logger;
        public ImportReportGeneration(IServiceRepository serviceRepository, ILoggerManager logger)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
        }
        public ImportResponse Import(IFormFile formFile)
        {
            var res = new ImportResponse();
            var inputModel = new ReportGenerationImportModel();
            using (var stream = new MemoryStream())
            {
                formFile.CopyTo(stream);

                using (var package = new ExcelPackage(stream))
                {
                    try
                    {
                        List<ReportGenerationViewModel> list = new List<ReportGenerationViewModel>();
                        if (package.Workbook.Worksheets["Ecosystem"] != null)
                        {
                            ExcelWorksheet worksheet = package.Workbook.Worksheets["Ecosystem"];
                            List<string> ColumnsList = GetColumnList(worksheet, 6, 4);
                            List<ReportGenerationViewModel> importList = ExcelToModel(worksheet, ColumnsList, 18);
                            list.AddRange(importList);
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'Ecosystem' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'Ecosystem' worksheet is missing.");
                            return res;
                        }
                        if (package.Workbook.Worksheets["Company Profile"] != null)
                        {
                            ExcelWorksheet worksheet = package.Workbook.Worksheets["Company Profile"];
                            List<string> ColumnsList = GetColumnList(worksheet, 6, 3);
                            List<ReportGenerationViewModel> importList = ExcelToModel(worksheet, ColumnsList, 16);
                            list.AddRange(importList);
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'Company Profile' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'Company Profile' worksheet is missing.");
                            return res;
                        }
                        if (package.Workbook.Worksheets["Trends"] != null)
                        {
                            ExcelWorksheet worksheet = package.Workbook.Worksheets["Trends"];
                            List<string> ColumnsList = GetColumnList(worksheet, 6, 3);
                            List<ReportGenerationViewModel> importList = ExcelToModel(worksheet, ColumnsList, 16);
                            list.AddRange(importList);
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'Trends' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'Trends' worksheet is missing.");
                            return res;
                        }
                        if (package.Workbook.Worksheets["Infographics & Overviews"] != null)
                        {
                            ExcelWorksheet worksheet = package.Workbook.Worksheets["Infographics & Overviews"];
                            List<string> ColumnsList = GetColumnList(worksheet, 6, 4);
                            List<ReportGenerationViewModel> importList = ExcelToModel(worksheet, ColumnsList, 16);
                            list.AddRange(importList);
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'Infographics & Overviews' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'Infographics & Overviews' worksheet is missing.");
                            return res;
                        }
                        if (package.Workbook.Worksheets["Market Tree"] != null)
                        {
                            ExcelWorksheet worksheet = package.Workbook.Worksheets["Market Tree"];
                            List<string> ColumnsList = GetColumnList(worksheet, 6, 3);
                            List<ReportGenerationViewModel> importList = ExcelToModel(worksheet, ColumnsList, 18);
                            list.AddRange(importList);
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'Market Tree' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'Market Tree' worksheet is missing.");
                            return res;
                        }
                        if (package.Workbook.Worksheets["Market Size"] != null)
                        {
                            ExcelWorksheet worksheet = package.Workbook.Worksheets["Market Size"];
                            List<string> ColumnsList = GetColumnList(worksheet, 6, 3);
                            List<ReportGenerationViewModel> importList = ExcelToModel(worksheet, ColumnsList, 18);
                            list.AddRange(importList);
                        }
                        else
                        {
                            res.ResponseStatus = "Error";
                            res.ResponseMessage = "Please select correct template for the file you want to import/Error while reading template. 'Market Size' worksheet is missing.";
                            res.TotalFailedRecords = 1;
                            _logger.LogError("Error while reading template. 'Market Size' worksheet is missing.");
                            return res;
                        }
                        inputModel.ReportGenerationViewModel=list;
                    }
                    catch (Exception ex)
                    {
                        res.ResponseStatus = "Error";
                        res.ResponseMessage = "Error while reading template. Error Message: " + ex.Message + ". Please check input excel. More details logged in error log.";
                        res.TotalFailedRecords = 1;
                        _logger.LogError("Error Message: " + ex.Message + " Stack Trace: " + ex.StackTrace.ToString());
                        return res;
                    }
                }
            }

            //--------Call api to post data            
            inputModel.TemplateName = "ReportGeneration Import Template";
            inputModel.ImportFileName = formFile.FileName;
            try
            {

                HttpResponseMessage response = _serviceRepository.PostResponse("ReportGenerationImportExport", inputModel);
                response.EnsureSuccessStatusCode();
                ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;

                res.ResponseStatus = viewModel.IsSuccess ? "Success" : "Failed";
                res.ResponseMessage = viewModel.Message;
                res.TotalRecords = inputModel.ReportGenerationViewModel.Count;
                if (res.ResponseStatus == "Success")
                {
                    res.TotalSuccessRecords = inputModel.ReportGenerationViewModel.Count;
                    res.TotalFailedRecords = 0;
                }
                else
                {
                    res.TotalSuccessRecords = 0;
                    res.TotalFailedRecords = inputModel.ReportGenerationViewModel.Count;
                }
                res.ResponseModel = viewModel.cpData;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return res;
        }

        private List<ReportGenerationViewModel> ExcelToModel(ExcelWorksheet worksheet, List<string> columnsList, int rowCount)
        {
            List<ReportGenerationViewModel> importList = new List<ReportGenerationViewModel>();
            if (columnsList != null && columnsList.Count > 0)
            {
                for (int i = 0; i < columnsList.Count; i++)
                {
                    Dictionary<string, string> keyValues = new Dictionary<string, string>();
                    var colIndex = 3 + i;
                    ReportGenerationViewModel model = new ReportGenerationViewModel();
                    for (int rowIndex = 7; rowIndex <= rowCount; rowIndex++)
                    {
                        if (worksheet.Cells[rowIndex, 2].Value != null && worksheet.Cells[rowIndex, 2].Value.ToString() != string.Empty)
                        {
                            var value = worksheet.Cells[rowIndex, colIndex].Value == null ? "" : worksheet.Cells[rowIndex, colIndex].Value.ToString();

                            keyValues.Add(worksheet.Cells[rowIndex, 2].Value?.ToString().Trim(), value);
                        }
                    }
                    model.ProjectName = keyValues["Project Name"].ToString().Trim();
                    model.ModuleName = columnsList[i];
                    model.Header = keyValues["Header"].ToString().Trim();
                    model.Footer = keyValues["Footer"].ToString().Trim();
                    model.ReportTitle = keyValues["Report Title"].ToString().Trim();
                    model.CoverPageImageLink = keyValues["Cover Page - Image link"].ToString().Trim();
                    if(!string.IsNullOrEmpty(model.CoverPageImageLink))
                    {
                        Image image = Image.FromFile(model.CoverPageImageLink);
                        if (image.RawFormat != null)
                        {
                            using (MemoryStream memoryStream = new MemoryStream())
                            {
                                image.Save(memoryStream, image.RawFormat);
                                model.CoverPageImageFileByte = memoryStream.ToArray();
                            }
                            var fileExt = Path.GetExtension(model.CoverPageImageLink);
                            model.CoverPageImageName = Guid.NewGuid().ToString() + fileExt;
                        }
                    }
                    model.Methodology = keyValues["Methodology"].ToString().Trim();
                    model.MethodologyImageLink = keyValues["Methodology Image link"].ToString().Trim();
                    if (!string.IsNullOrEmpty(model.MethodologyImageLink))
                    {
                        Image image = Image.FromFile(model.MethodologyImageLink);
                        if (image.RawFormat != null)
                        {
                            using (MemoryStream memoryStream = new MemoryStream())
                            {
                                image.Save(memoryStream, image.RawFormat);
                                model.MethodologyImageFileByte = memoryStream.ToArray();
                            }
                            var fileExt = Path.GetExtension(model.MethodologyImageLink);
                            model.MethodologyImageName = Guid.NewGuid().ToString() + fileExt;
                        }
                    }
                    model.ContactUsAnalystName = keyValues["Contact Us - Analyst name"].ToString().Trim();
                    model.ContactUsOfficeContactNo = keyValues["Contact Us - office contact no"].ToString().Trim();
                    model.ContactUsAnalystEmailId = keyValues["Contact Us - Analyst Email id"].ToString().Trim();
                    if (rowCount == 18)
                    {
                        model.Overview = keyValues["Overview"].ToString().Trim();
                        model.KeyPoints = keyValues["Key Points"].ToString().Trim();
                    }
                    importList.Add(model);
                }
            }
            return importList;
        }

        private List<string> GetColumnList(ExcelWorksheet worksheet, int rowIndex, int columnsCount)
        {
            List<string> ColumnsList = new List<string>();

            for (int colIndex = 3; colIndex <= columnsCount; colIndex++)
            {
                if (worksheet.Cells[rowIndex, colIndex].Value != null)
                {
                    ColumnsList.Add(worksheet.Cells[rowIndex, colIndex].Value.ToString());
                }
            }
            return ColumnsList;
        }
    }
}
