﻿using Avira.MarketingResearch.Common.IService.ImportExport;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Repository.CoreInterfaces.ImportExport;
using Avira.MarketingResearch.Repository.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.ImportExport
{
    public class TrendImportExportService : ITrendImportExportService
    {
        private readonly ITrendImportExportRepository _trendImportExportRepository;

        public TrendImportExportService(ITrendImportExportRepository buildLevelRepository)
        {
            _trendImportExportRepository = buildLevelRepository;           
        }

        public List<ImportCommonResponseModel> Create(TrendImportInsertViewModel tfc, AviraUser currentUser)
        {    
            tfc.TrendJson = JsonConvert.SerializeObject(tfc.TrendsImportViewModel);
            tfc.ProjectKeywordJson= JsonConvert.SerializeObject(tfc.ProjectKeyWordImportViewModel);
            var trendImportResponse =_trendImportExportRepository.Create(tfc);
            return trendImportResponse;
        }
    }
}
