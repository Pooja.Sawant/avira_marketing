﻿

CREATE PROCEDURE [dbo].[SApp_GetProfitAndLossDataByCompanyID]             
 @CompanyID uniqueidentifier = null,         
 @includeDeleted bit = 0             
AS
/*================================================================================  
Procedure Name: [SApp_GetProfitAndLossDataByCompanyID]  
Author: Harshal 
Create date: 08/09/2019  
Description:Getting Data From Table WRT CompanyId 
Change History  
Date        Developer     Reason  
__________  ____________  ______________________________________________________  
08/09/2019  Harshal   Initial Version  
================================================================================*/          
BEGIN         
 SET NOCOUNT ON;          
 SELECT  Id,
CompanyId,
CurrencyId,
ValueConversionId,
CompanyName,
Currency,
Units,
Year,
Revenue,
CostsAndExpenses,
CostOfProduction,
ResourceCost,
SalaryCost,
CostOfSales,
GrossProfit,
SellingExpenses,
MarketingExpenses,
AdministrativeExpenses,
GeneralExpenses,
SellingGeneralAndAdminExpenses,
ResearchAndDevelopmentExpenses,
Depreciation,
Amortization,
Restructuring,
OtherIncomeOrDeductions,
TotalOperatingExpenses,
EBIT,
EBITDA,
InterestIncome,
InterestExpense,
EBT,
Tax,
NetIncomeFromContinuingOperations,
DiscontinuedOperations,
IncomeLossFromDiscontinuedOperations,
GainOnDisposalOfDiscontinuedOperations,
DiscontinuedOperationsNetOfTax,
NetIncomeBeforeAllocationToNoncontrollingInterests,
NonControllingInterests,
NetIncome,
BasicEPS_,
BasicEPSFromContinuingOperations,
BasicEPSFromDiscontinuedOperations,
BasicEPS,
DilutedEPS_,
DilutedEPSFromContinuingOperations,
DilutedEPSFromDiscontinuedOperations,
DilutedEPS,
WeightedAverageSharesOutstandingBasic,
WeightedAverageSharesOutstandingDiluted

FROM [dbo].[CompanyPLStatement]        
  
  WHERE (CompanyPLStatement.CompanyId = @CompanyID)              
 AND (@includeDeleted = 0 or [CompanyPLStatement].IsDeleted = 0)          
END