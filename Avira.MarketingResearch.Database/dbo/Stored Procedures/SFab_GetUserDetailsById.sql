﻿CREATE procedure [dbo].[SFab_GetUserDetailsById]
(
	@UserId uniqueidentifier
)
as
/*================================================================================
Procedure Name: SFab_GetUserDetailsById
Author: Pooja
Create date: 09/06/2019
Description: Get user login details by Id
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
09/06/2019	Pooja			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SELECT [Id], 
		[UserName],
		[EmailAddress],
		[Salt],
		[PassWord],
		UserRoleId,
		IsUserLocked,
		ResetPasswordLinkCreatedOn
FROM [dbo].[AviraUser]
where Id = @UserId
and IsDeleted = 0 AND UserTypeId = 2; --2 for customer user
--and IsUserLocked = 0;

 
end