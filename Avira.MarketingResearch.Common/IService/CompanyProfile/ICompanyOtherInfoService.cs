﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.CompanyProfile
{
    public interface ICompanyOtherInfoService
    {
        IEnumerable<CompanyOtherInfoInsertUpdateViewModel> GetAll(Guid? Id);

        Result<SpTransactionMessage> InsertUpdate(MainCompanyOtherInfoInsertUpdateViewModel tfc, AviraUser currenUser);

    }
}
