﻿using System;
using System.Collections.Generic;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.CompanyProfile
{
    public interface ICompanyStructureService
    {
        //IEnumerable<CompanyStructureModel> GetCompanyStructure(Guid Id);
        IEnumerable<CompanyStructureModel> GetAll(Guid Id);
    }
}
