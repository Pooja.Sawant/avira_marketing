﻿CREATE procedure [dbo].[SApp_UpdateStagingTrend]      
as      
/*================================================================================      
Procedure Name: SApp_UpdateStagingTrend     
Author: Sai Krishna      
Create date: 13/06/2019      
Description: to update StaggingTrend      
Change History      
Date       Developer    Reason      
__________ ____________ ______________________________________________________      
13/06/2019 Sai Krishna   Initial Version      
================================================================================*/      
BEGIN      
SET NOCOUNT ON;      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;      
declare @ErrorMsg NVARCHAR(2048)        
DECLARE @ProjectId uniqueidentifier        
BEGIN TRY       

SELECT @ProjectId = Id FROM  project 
WHERE ProjectName = (select top 1 Project from StaginTrend) and IsDeleted = 0 


Update StaginTrend 
SET

Project = [dbo].[udfTrim](Project),
TrendName = [dbo].[udfTrim](TrendName),
Description = [dbo].[udfTrim](Description),
TrendStatus = [dbo].[udfTrim](TrendStatus),
Importance = [dbo].[udfTrim](Importance),
ImpactDirection = [dbo].[udfTrim](ImpactDirection),
TrendValue = [dbo].[udfTrim](TrendValue),
Category = [dbo].[udfTrim](Category),
CompanyGroup = [dbo].[udfTrim](CompanyGroup),
CompanyGroupImpact = [dbo].[udfTrim](CompanyGroupImpact),
CompanyGroupEndUser = [dbo].[udfTrim](CompanyGroupEndUser),
Company = [dbo].[udfTrim](Company),
Country = [dbo].[udfTrim](Country),
CountryImpact = [dbo].[udfTrim](CountryImpact),
CountryEndUser = [dbo].[udfTrim](CountryEndUser),
EcoSystem = [dbo].[udfTrim](EcoSystem),
EcoSystemSubSegment= [dbo].[udfTrim](EcoSystemSubSegment),
EcoSystemImpact = [dbo].[udfTrim](EcoSystemImpact),
EcoSystemEndUser = [dbo].[udfTrim](EcoSystemEndUser),
TrendKeyWord = [dbo].[udfTrim](TrendKeyWord),
Industry = [dbo].[udfTrim](Industry),
IndustryImpact = [dbo].[udfTrim](IndustryImpact),
IndustryEndUser = [dbo].[udfTrim](IndustryEndUser),
Market = [dbo].[udfTrim](Market),
Region = [dbo].[udfTrim](Region),
RegionImpact = [dbo].[udfTrim](RegionImpact),
RegionEndUser = [dbo].[udfTrim](RegionEndUser),
MarketImpact = [dbo].[udfTrim](MarketImpact),
MarketEndUser = [dbo].[udfTrim](MarketEndUser),
Project1 = [dbo].[udfTrim](Project1),
ProjectImpact = [dbo].[udfTrim](ProjectImpact),
ProjectEndUser = [dbo].[udfTrim](ProjectEndUser),
StrengthtType = [dbo].[udfTrim](StrengthtType),
StrengthDescription = [dbo].[udfTrim](StrengthDescription),
WeaknessType = [dbo].[udfTrim](WeaknessType),
WeaknessDescription = [dbo].[udfTrim](WeaknessDescription),
OpportunityType = [dbo].[udfTrim](OpportunityType),
OpportunityDescription = [dbo].[udfTrim](OpportunityDescription),
ThreatType = [dbo].[udfTrim](ThreatType),
ThreatDescription = [dbo].[udfTrim](ThreatDescription),
TimeTag = [dbo].[udfTrim](TimeTag),
TimeTagImpact = [dbo].[udfTrim](TimeTagImpact),
TimeTagEndUser = [dbo].[udfTrim](TimeTagEndUser),
ValueConversion = [dbo].[udfTrim](ValueConversion),
Rationale_2018 = [dbo].[udfTrim](Rationale_2018),
Value_2018 = [dbo].[udfTrim](Value_2018),
Rationale_2019 = [dbo].[udfTrim](Rationale_2019),
Value_2019 = [dbo].[udfTrim](Value_2019),
Rationale_2020 = [dbo].[udfTrim](Rationale_2020),
Value_2020 = [dbo].[udfTrim](Value_2020),
Rationale_2021 = [dbo].[udfTrim](Rationale_2021),
Value_2021 = [dbo].[udfTrim](Value_2021),
Rationale_2022 = [dbo].[udfTrim](Rationale_2022),
Value_2022 = [dbo].[udfTrim](Value_2022),
Rationale_2023 = [dbo].[udfTrim](Rationale_2023),
Value_2023 = [dbo].[udfTrim](Value_2023),
Rationale_2024 = [dbo].[udfTrim](Rationale_2024),
Value_2024 = [dbo].[udfTrim](Value_2024),
Rationale_2025 = [dbo].[udfTrim](Rationale_2025),
Value_2025 = [dbo].[udfTrim](Value_2025),
Rationale_2026 = [dbo].[udfTrim](Rationale_2026),
Value_2026 = [dbo].[udfTrim](Value_2026),
Rationale_2027 = [dbo].[udfTrim](Rationale_2027),
Value_2027 = [dbo].[udfTrim](Value_2027),
Rationale_2028 = [dbo].[udfTrim](Rationale_2028),
Value_2028 = [dbo].[udfTrim](Value_2028),
Rationale_2029 = [dbo].[udfTrim](Rationale_2029),
Value_2029 = [dbo].[udfTrim](Value_2029),
Rationale_2030 = [dbo].[udfTrim](Rationale_2030),
Value_2030 = [dbo].[udfTrim](Value_2030),
Rationale_2031 = [dbo].[udfTrim](Rationale_2031),
Value_2031 = [dbo].[udfTrim](Value_2031),
Rationale_2032 = [dbo].[udfTrim](Rationale_2032),
Value_2032 = [dbo].[udfTrim](Value_2032),


ErrorNotesIndustry = null, 
ErrorNotesKeyword = null,
ErrorNotesMarket = null,
ErrorNotesProject = null,
ErrorNotesCompanyGroup = null,
ErrorNotesTimeTag = null,
ErrorNotesKeyCompany = null,
ErrorNotesProject1  = null,
ErrorNotesRegion = null,
ErrorNotesCountry = null,
ErrorNotesTrend = null,
ErrorNotesValueConversion = null,
ErrorNotesEcoSystem = null,
ErrorNotesEcoSystemSubSegemnt = null,
ErrorNotesAuthorRemark = null;




Update StaginTrend 
SET ErrorNotesProject = 'Invalid Project Name '+StaginTrend.Project
where not exists (select 1 from Project where StaginTrend.Project = Project.ProjectName);

;with cteRegionlist as (select distinct StaginTrend.Id, 
Country.CountryName
from StaginTrend
cross apply string_split(StaginTrend.Region, ',') Rlist
inner join Region
on Rlist.value = Region.RegionName
inner join CountryRegionMap CRM
on Region.id = CRM.RegionId
inner join Country
on crm.CountryId = Country.Id
and (Country.CountryName = 'All Of '+ Region.RegionName or Country.CountryName ='All Of World')
where (StaginTrend.Country = 'All' or StaginTrend.Country= '' or StaginTrend.Country is null)
--group by StaginTrend.Id
),
cteCList as (select Id,
STRING_AGG(CountryName,',') as Country
from cteRegionlist
group by Id)
Update StaginTrend
set Country = cteCList.Country
from StaginTrend
inner join cteCList
on StaginTrend.Id = cteCList.Id;
--CTE for Validate Industry
;With cteIndustryNames as (
	SELECT StaginTrend.Id,
	ind.value as IndustryNames,
	Industry.id as Industryid,
	CASE     
     WHEN Industry.id is null THEN 'Industry name '+ind.value+' is not present in database'    
     ELSE NULL    
     END AS IndustryIdError
	From StaginTrend
	Cross apply string_split(StaginTrend.Industry, ',') ind
	left join 
	[dbo].SubSegment Industry
INNER JOIN Segment ON Industry.SegmentId = Segment.Id AND Segment.SegmentName = 'End User'

	 on ind.value = Industry.SubSegmentName
)
Update StaginTrend 
SET ErrorNotesIndustry = ISNULL(cteIndustryNames.IndustryIdError,'')
From cteIndustryNames
Inner Join StaginTrend st
On cteIndustryNames.Id = st.Id

--CTE for Validate Industry ImpactType
;With cteIndustryNamesCount as (
	SELECT StaginTrend.Id,
	count(*) as industryCnt
	From StaginTrend
	Cross apply string_split(StaginTrend.Industry, ',') 
	group by StaginTrend.Id
),
CTEIndustryImpactCount as (
	Select StaginTrend.Id,
	COUNT(st.value) as IndustryImpactcnt
	from StaginTrend
	Cross apply string_split(StaginTrend.IndustryImpact, ',') st
	group by StaginTrend.Id
)
Update StaginTrend 
SET ErrorNotesIndustry = isnull(ErrorNotesIndustry+',','')+ 'Please enter the equal number of industries and impacts'
From StaginTrend
Inner Join cteIndustryNamesCount
On cteIndustryNamesCount.id = StaginTrend.Id
inner join CTEIndustryImpactCount
on CTEIndustryImpactCount.id = StaginTrend.Id 
where cteIndustryNamesCount.industryCnt != CTEIndustryImpactCount.IndustryImpactcnt

--CTE for Validate MarketId
;With cteMarketNames as (
    Select StaginTrend.Id,
	st.value as MarketName,
	case when mar.id is null THEN 'Market name '+st.value+' is not present in database'    
     ELSE NULL    
     END AS MarketIdError
	from StaginTrend
	Cross apply string_split(StaginTrend.Market, ',') st
	left join Market mar
ON st.value = mar.MarketName and mar.IsDeleted = 0
)
Update StaginTrend 
SET ErrorNotesMarket = ISNULL(cteMarketNames.MarketIdError,'')
From cteMarketNames
Inner Join StaginTrend st
On cteMarketNames.Id = st.Id

--CTE for Validate Market ImpactType
;With cteMarketNamesCount as (
	SELECT StaginTrend.Id,
	count(*) as MarketCnt
	From StaginTrend
	Cross apply string_split(StaginTrend.Market, ',') 
	group by StaginTrend.Id
),
CTEMarketImpactCount as (
	Select StaginTrend.Id,
	COUNT(st.value) as MarketImpactCnt
	from StaginTrend
	Cross apply string_split(StaginTrend.MarketImpact, ',') st
	group by StaginTrend.Id
)
Update StaginTrend 
SET ErrorNotesMarket = isnull(ErrorNotesMarket+',','')+ 'Please enter the equal number of Markets and impacts'
From StaginTrend
Inner Join cteMarketNamesCount
On cteMarketNamesCount.id = StaginTrend.Id
inner join CTEMarketImpactCount
on CTEMarketImpactCount.id = StaginTrend.Id 
where CTEMarketImpactCount.MarketImpactCnt != cteMarketNamesCount.MarketCnt;

--CTE for Validate ProjectId
;With cteProjectErrorNotes as (
    Select StaginTrend.Id,
	st.value as ProjectName,
	case when Project.id is null THEN 'Project name '+st.value+' is not present in database'    
     ELSE NULL    
     END AS ProjectIdError
	from StaginTrend
	Cross apply string_split(StaginTrend.Project1, ',') st
	left join Project
ON st.value = Project.ProjectName and Project.IsDeleted = 0)
Update StaginTrend 
SET ErrorNotesProject1 = ISNULL(cteProjectErrorNotes.ProjectIdError,'')
From cteProjectErrorNotes
Inner Join StaginTrend st
On cteProjectErrorNotes.Id = st.Id

--CTE for Validate Project ImpactType
;With cteProjectCount as (
	SELECT StaginTrend.Id,
	count(*) as ProjectCnt
	From StaginTrend
	Cross apply string_split(StaginTrend.Project1, ',') 
	group by StaginTrend.Id
),
CTEProjectImpactCount as (
	Select StaginTrend.Id,
	COUNT(st.value) as ProjectImpactCnt
	from StaginTrend
	Cross apply string_split(StaginTrend.ProjectImpact, ',') st
	group by StaginTrend.Id
)
Update StaginTrend 
SET ErrorNotesProject1 = isnull(ErrorNotesProject1+',','')+ 'Please enter the equal number of Projects and impacts'
From StaginTrend
Inner Join cteProjectCount
On cteProjectCount.id = StaginTrend.Id
inner join CTEProjectImpactCount
on CTEProjectImpactCount.id = StaginTrend.Id 
where CTEProjectImpactCount.ProjectImpactCnt != cteProjectCount.ProjectCnt;

--CTE for Validate CompanyGroupId
declare @CompanyGroupCount int
select @CompanyGroupCount = COUNT(*) from CompanyGroup where CompanyGroup.IsDeleted = 0;

Update StaginTrend
set CompanyGroup = (select STRING_AGG(CompanyGroupName,',') from CompanyGroup where CompanyGroup.IsDeleted = 0),
CompanyGroupImpact = replicate(CompanyGroupImpact+',',@CompanyGroupCount-1) + CompanyGroupImpact--,
--CompanyGroupEndUser = replicate(CompanyGroupEndUser+',',@CompanyGroupCount-1) + CompanyGroupEndUser
from StaginTrend
where StaginTrend.CompanyGroup = 'All';

;With cteCompanyGroupErrorNotes as (
    Select StaginTrend.Id,
	st.value as CompanyGroupName,
	case when CompanyGroup.id is null THEN 'Company Group Name '+st.value+' is not present in database'    
     ELSE NULL    
     END AS CompanyGroupIdError
	from StaginTrend
	Cross apply string_split(StaginTrend.CompanyGroup, ',') st
	left join CompanyGroup
ON st.value = CompanyGroup.CompanyGroupName and CompanyGroup.IsDeleted = 0)
Update StaginTrend 
SET ErrorNotesCompanyGroup = ISNULL(cteCompanyGroupErrorNotes.CompanyGroupIdError,'')
From cteCompanyGroupErrorNotes
Inner Join StaginTrend st
On cteCompanyGroupErrorNotes.Id = st.Id;

;With cteCompanyGroupCount as (
	SELECT StaginTrend.Id,
	count(*) as CompanyGroupCnt
	From StaginTrend
	Cross apply string_split(StaginTrend.CompanyGroup, ',') 
	group by StaginTrend.Id
),
CTECompanyGroupImpactCount as (
	Select StaginTrend.Id,
	COUNT(st.value) as CompanyGroupImpactCnt
	from StaginTrend
	Cross apply string_split(StaginTrend.CompanyGroupImpact, ',') st
	group by StaginTrend.Id
)
Update StaginTrend 
SET ErrorNotesCompanyGroup = isnull(ErrorNotesCompanyGroup+',','')+ 'Please enter the equal number of Company Groups and impacts'
From StaginTrend
Inner Join cteCompanyGroupCount
On cteCompanyGroupCount.id = StaginTrend.Id
inner join CTECompanyGroupImpactCount
on CTECompanyGroupImpactCount.id = StaginTrend.Id 
where CTECompanyGroupImpactCount.CompanyGroupImpactCnt != cteCompanyGroupCount.CompanyGroupCnt;

--CTE for Validate TimeTagId
;With cteTimeTagNames as (    
	Select StaginTrend.Id,
	st.value as TimeTagNames,
	 CASE  WHEN tt.Id is null THEN 'TimeTag name '+st.value+' is not present in database'    
     ELSE NULL    
     END AS TimeTagIdError    
	from StaginTrend
	Cross apply string_split(StaginTrend.TimeTag, ',') st 
	Left JOIN TimeTag tt
ON st.value = tt.TimeTagName and tt.IsDeleted = 0 and st.value !=''
)

--SELECT * FROM cteTimeTagNames
Update StaginTrend 
SET ErrorNotesTimeTag = ISNULL(cteTimeTagNames.TimeTagIdError,'') 
+ CASE When (not exists (select 1 from TimeTag where TimeTag.TimeTagName =cteTimeTagNames.TimeTagNames)) then 'Invalid TimeTag' else '' end
From cteTimeTagNames
Inner Join StaginTrend st
On cteTimeTagNames.Id = st.Id
--CTE for Validate TimeTag ImpactType

;With cteTimeTagNamesCount as (
	Select StaginTrend.Id,
	Count(st.value) as TimeTagNamesCnt
	from StaginTrend
	Cross apply string_split(StaginTrend.TimeTag, ',') st
	group by StaginTrend.Id
),
cteTimeTagImpactCount as (
    Select StaginTrend.Id,
	COUNT(st.value) as TimeTagImpactcnt
	from StaginTrend
	Cross apply string_split(StaginTrend.TimeTagImpact, ',') st
	group by StaginTrend.Id
)
Update StaginTrend 
SET ErrorNotesTimeTag =isnull(ErrorNotesTimeTag+',','')+ 'Please enter the equal number of Timetags and impacts'
From StaginTrend st
inner join cteTimeTagNamesCount
On cteTimeTagNamesCount.Id = st.Id
inner join cteTimeTagImpactCount
On cteTimeTagImpactCount.Id = st.Id
where cteTimeTagImpactCount.TimeTagImpactcnt != cteTimeTagNamesCount.TimeTagNamesCnt


---------------------------------
--CTE for Validate EcoSystemId
declare @IsValid int
Set @IsValid = (select COUNT(EcoSystem) from StaginTrend);
IF(@IsValid = 0)
Begin
;With cteEcosystemErrorNotes As(
SELECT es.ID AS EcoSystemId,
CASE     
     WHEN es.Id is null THEN 'Please fill EcoSystem name'    
     ELSE NULL    
     END AS EcoSystemError,    
     st.Id AS StaggingId
FROM StaginTrend st
LEFT JOIN Segment es
INNER JOIN ESMSegmentMapping ON ESMSegmentMapping.SegmentId = es.Id AND ProjectId = @ProjectId
ON st.EcoSystem = es.SegmentName and es.IsDeleted = 0
)
Update StaginTrend 
SET ErrorNotesEcoSystem = ISNULL(cteEcosystemErrorNotes.EcoSystemError,'')
From cteEcosystemErrorNotes
Inner Join StaginTrend st
On cteEcosystemErrorNotes.StaggingId = st.Id
End
Else
Begin
;With cteEcoSystemNames as (
	Select StaginTrend.Id,
	st.value as EcoSystemNames,
	StaginTrend.Project as projectName
	from StaginTrend
	Cross apply string_split(StaginTrend.EcoSystem, ',') st
),
 
cteEcoSystemNamesCount as (
	Select StaginTrend.Id,
	Count(st.value) as EcoSystemNames
	from StaginTrend
	Cross apply string_split(StaginTrend.EcoSystem, ',') st
	Where StaginTrend.Id in (Select StaginTrend.Id as StaggingId from StaginTrend
	Cross apply string_split(StaginTrend.EcoSystem, ',') st)
	group by StaginTrend.Id
),

--aaaaaaaaaaaaaaaaaaaaaaaaaaa

cetDuplicateSegment as (
SELECT Id, EcoSystemNames, projectName FROM cteEcoSystemNames  GROUP BY Id, EcoSystemNames,projectName HAVING COUNT(EcoSystemNames) > 1

),

cetDuplicateSegmentError as
(

SELECT distinct
st.EcoSystemNames,
st.projectName,

--CASE 
--     WHEN exists (select 1 from cteEcoSystemNamesCount icnt where icnt.Id = st.id and icnt.EcoSystemNames < 2) THEN 'Please Enter minium two EcoSystem'
--	 ELSE Null
--	 END 
	 Null as EcoSystemCountError,
null  AS EcoSystemIdError, 
'EcoSystem name '+ st.EcoSystemNames +' is duplicate.' as  EcoSystemDuplicateError   , 
    st.Id AS StaggingId
FROM cetDuplicateSegment st
--add esmsegemntmapping
--INNER JOIN segment es ON st.EcoSystemNames = es.SegmentName
	--INNER join  ESMSegmentMapping esm on es.Id = esm.SegmentId and esm.ProjectId = @ProjectId and es.IsDeleted = 0 
	

),
--aaaaaaaaaaaaaaaaaaaaaaa
cteValidateEcoSystemNames as (
SELECT Distinct(esm.Id) as EcoSystemId,
st.EcoSystemNames,
st.projectName,

--CASE 
--     WHEN exists (select 1 from cteEcoSystemNamesCount icnt where icnt.Id = st.id and icnt.EcoSystemNames < 2) THEN 'Please Enter minium two EcoSystem'
--	 ELSE Null
--	 END 
	 Null as EcoSystemCountError,
CASE     
     WHEN esm.Id is null THEN 'EcoSystem name '+st.EcoSystemNames+' is not presant in database'    
     ELSE NULL    
     END AS EcoSystemIdError,     
    st.Id AS StaggingId
FROM cteEcoSystemNames st
--add esmsegemntmapping
Left JOIN segment es ON st.EcoSystemNames = es.SegmentName
	Left join  ESMSegmentMapping esm on es.Id = esm.SegmentId and esm.ProjectId = @ProjectId and es.IsDeleted = 0 
	
--Left JOIN Segment es

),
cteEcoSystemErrorNotes as(
select
    Distinct(t1.StaggingId) as StaggingId,
    stuff((
        select ',' + t.EcoSystemNames
        from cteValidateEcoSystemNames t
        where t.StaggingId = t1.StaggingId
        order by t.EcoSystemNames
        for xml path('')
    ),1,1,'') as EcoSystem,
	stuff((
        select ',' + t.EcoSystemIdError
        from cteValidateEcoSystemNames t
        where t.StaggingId = t1.StaggingId
        order by t.EcoSystemIdError
        for xml path('')
    ),1,1,'') as EcosystemIdError,
	t1.EcoSystemCountError,

	stuff((
        select ',' + t.EcoSystemDuplicateError
        from cetDuplicateSegmentError t
        where t.StaggingId = t1.StaggingId
        order by t.EcoSystemDuplicateError
        for xml path('')
    ),1,1,'') as EcoSystemDuplicateError
	
	--,
	--t1.EcoSystemDuplicateError

from cteValidateEcoSystemNames t1
group by t1.EcoSystemIdError,t1.StaggingId, t1.EcoSystemCountError--, t1.EcoSystemDuplicateError
)
Update StaginTrend 
SET ErrorNotesEcoSystem = ISNULL(cteEcoSystemErrorNotes.EcosystemIdError,'') + ',' + ISNULL(cteEcoSystemErrorNotes.EcoSystemDuplicateError,'') + ',' + ISNULL(cteEcoSystemErrorNotes.EcoSystemCountError,'')
From cteEcoSystemErrorNotes
Inner Join StaginTrend st
On cteEcoSystemErrorNotes.StaggingId = st.Id
End

--------xxxxxxxxxxxxxxxxxxxxxxxxxxxx
;with
cteSegmentSubsegmentByProject as
( 
--SELECT SubSegment.SegmentId, SubSegment.Id AS subSegmentId, SegmentName, SubSegmentName FROM segment 
--INNER JOIN SubSegment on segment.Id = SubSegment.SegmentId
--INNER JOIN ESMSegmentMapping ON segment.Id = ESMSegmentMapping.SegmentId and SubSegment.id=ESMSegmentMapping.SubSegmentId
--WHERE ProjectId = @ProjectId
SELECT SubSegment.SegmentId, SubSegment.Id AS subSegmentId, SegmentName, SubSegmentName FROM segment 
INNER JOIN SubSegment on segment.Id = SubSegment.SegmentId
INNER JOIN ESMSegmentMapping ON SubSegment.id = ESMSegmentMapping.SubSegmentId 
--and SubSegment.id=ESMSegmentMapping.SubSegmentId
WHERE ProjectId =@ProjectId
),
cteEcoSystemNames_1 as (
    Select StaginTrend.Id,
    st2.value as EcoSystemNames,
    StaginTrend.Project as projectName
    from StaginTrend
    Cross apply string_split(StaginTrend.EcoSystem, ',') st2
),
cteEcoSystemSubSegmentNames as (
    Select StaginTrend.Id,
    st1.value as EcoSystemSubSegmentNames,
    StaginTrend.Project as projectName
    from StaginTrend
    Cross apply string_split(StaginTrend.EcoSystemSubSegment, ',') st1
       WHERE ISNULL(StaginTrend.EcoSystemSubSegment,'') != ''
),
cteValidateEcoSystemSubSegmentNames as (
    SELECT --(cteSegmentSubsegmentByProject.subSegmentId) as EcoSystemSubSegmentId,
    st.EcoSystemSubSegmentNames,    
    st.projectName,
    --cteEcoSystemNames_1.EcoSystemNames,
    CASE WHEN not exists (select 1 from cteSegmentSubsegmentByProject 
						  where  cteSegmentSubsegmentByProject.SegmentName in (select cteEcoSystemNames_1.EcoSystemNames from cteEcoSystemNames_1 
																				where cteEcoSystemNames_1.Id = st.Id)
						  --AND  st.EcoSystemSubSegmentNames = cteSegmentSubsegmentByProject.SubSegmentName 
						   --where
       
	   )
						  THEN 'EcoSystem Sub Segment name '+st.EcoSystemSubSegmentNames+' is not presant in mapping'
         ELSE NULL    
         END AS EcoSystemSubSegmentIdError ,     
        st.Id AS StaggingId    
        
    FROM cteEcoSystemSubSegmentNames st
	where st.EcoSystemSubSegmentNames != ''-- on cteEcoSystemNames_1.Id = st.Id
    


),
cteEcoSystemSubSegmentErrorNotes as(
select
    Distinct(t1.StaggingId) as StaggingId,
    stuff((
        select ',' + t.EcoSystemSubSegmentNames
        from cteValidateEcoSystemSubSegmentNames t
        where t.StaggingId = t1.StaggingId
              GROUP BY t.EcoSystemSubSegmentNames
        order by t.EcoSystemSubSegmentNames
        for xml path('')

    ),1,1,'') as EcoSystemSubSegment,
    stuff((
        select ',' + t.EcoSystemSubSegmentIdError
        from cteValidateEcoSystemSubSegmentNames t
        where t.StaggingId = t1.StaggingId
              GROUP BY t.EcoSystemSubSegmentIdError
        order by t.EcoSystemSubSegmentIdError
        for xml path('')
    ),1,1,'') as EcosystemsubSegmentIdError
    
from cteValidateEcoSystemSubSegmentNames t1
group by t1.EcoSystemSubSegmentIdError,t1.StaggingId
)
Update StaginTrend 
SET ErrorNotesEcoSystemSubSegemnt = ISNULL(cteEcoSystemSubSegmentErrorNotes.EcosystemsubSegmentIdError,'') 
From cteEcoSystemSubSegmentErrorNotes
Inner Join StaginTrend st
On cteEcoSystemSubSegmentErrorNotes.StaggingId = st.Id

--------xxxxxxxxxxxxxxxxxxxxxxxxxxxx

--CTE for Validate EcoSystem ImpactType
Declare @EcoSystemNamesCount int;
Declare @EcoSystemImpactCount int;
SET @EcoSystemNamesCount = 
(
	Select COUNT(st.value) as EcoSystemNames
	from StaginTrend
	Cross apply string_split(StaginTrend.EcoSystem, ',') st
)
SET @EcoSystemImpactCount = 
(
	Select COUNT(st.value) as EcoSystemImpact
	from StaginTrend
	Cross apply string_split(StaginTrend.EcoSystemImpact, ',') st
)

IF((@EcoSystemNamesCount != @EcoSystemImpactCount) and (@EcoSystemNamesCount = 0) or (@EcoSystemImpactCount = 0))
Begin
;With cteEcoSystemImpactErrorNotes as (
SELECT st.EcoSystemImpact, 
CASE     
     WHEN (@EcoSystemNamesCount != @EcoSystemImpactCount) and (@EcoSystemNamesCount = 0) or (@EcoSystemImpactCount = 0) THEN 'Please fill the EcoSystem impact for all the EcoSystem you entered'    
     ELSE NULL    
     END AS EcoSystemImpactIdError,    
     st.Id AS StaggingId
FROM StaginTrend st
)
Update StaginTrend 
SET ErrorNotesEcoSystem = ISNULL(ErrorNotesEcoSystem,'') + ',' + ISNULL(cteEcoSystemImpactErrorNotes.EcoSystemImpactIdError,'')
From cteEcoSystemImpactErrorNotes
Inner Join StaginTrend st
On cteEcoSystemImpactErrorNotes.StaggingId = st.Id
End
Else
Begin
;With cteEcoSystemImpact as (
	Select StaginTrend.Id,
	st.value as EcoSystemImpact
	from StaginTrend
	Cross apply string_split(StaginTrend.EcoSystemImpact, ',') st
),
cteEcoSystemImpactCount as (
	Select StaginTrend.Id,
	Count(st.value) as EcoSystemImpact
	from StaginTrend
	Cross apply string_split(StaginTrend.EcoSystemImpact, ',') st
	Where StaginTrend.Id in (Select StaginTrend.Id as StaggingId from StaginTrend
	Cross apply string_split(StaginTrend.EcoSystemImpact, ',') st)
	group by StaginTrend.Id
),
cteEcoSystemNamesCount as (
	Select StaginTrend.Id,
	Count(st.value) as EcoSystemNames
	from StaginTrend
	Cross apply string_split(StaginTrend.EcoSystem, ',') st
	Where StaginTrend.Id in (Select StaginTrend.Id as StaggingId from StaginTrend
	Cross apply string_split(StaginTrend.EcoSystem, ',') st)
	group by StaginTrend.Id
),
cteValidateEcoSystemImpactNames as (
SELECT it.Id as EcoSystemImpactId,
st.EcoSystemImpact,
--CASE
--     WHEN exists (select 1 from cteEcoSystemImpactCount icnt where icnt.Id = st.id and icnt.EcoSystemImpact < 2) THEN 'Please Enter minium two Impact for Ecosystem'
--	 ELSE Null
--	 END as 
	 Null as EcoSystemImapctCountError,
CASE
     WHEN ((select EcoSystemImpact from cteEcoSystemImpactCount icnt where icnt.Id = st.id) != (select EcoSystemNames from cteEcoSystemNamesCount cnc where cnc.Id = st.id)) THEN 'Please enter the equal number of ecosystems and impacts'
	 ELSE Null
	 END as EcoSystemandImpactCountError,
CASE     
     WHEN it.Id is null THEN 'Please fill the Ecosystem impact for all the Ecosystem you entered'    
     ELSE NULL    
     END AS EcoSystemImpactIdError,     
    st.Id AS StaggingId
FROM cteEcoSystemImpact st
Left JOIN ImpactType it
ON st.EcoSystemImpact = it.ImpactTypeName
),
cteEcoSystemImpactErrorNotes as(
select
    Distinct(t1.StaggingId),
    stuff((
        select ',' + t.EcoSystemImpact
        from cteValidateEcoSystemImpactNames t
        where t.StaggingId = t1.StaggingId
        order by t.EcoSystemImpact
        for xml path('')
    ),1,1,'') as EcoSystemImpact,
	stuff((
        select ',' + t.EcoSystemImpactIdError
        from cteValidateEcoSystemImpactNames t
        where t.StaggingId = t1.StaggingId
        order by t.EcoSystemImpactId
        for xml path('')
    ),1,1,'') as EcoSystemImpactIdError,
	t1.EcoSystemImapctCountError,
	t1.EcoSystemandImpactCountError
from cteValidateEcoSystemImpactNames t1
group by t1.EcoSystemImpactIdError,t1.StaggingId,t1.EcoSystemImapctCountError, t1.EcoSystemandImpactCountError
)
Update StaginTrend 
SET ErrorNotesEcoSystem = ISNULL(ErrorNotesEcoSystem,'') + ',' + ISNULL(cteEcoSystemImpactErrorNotes.EcoSystemImpactIdError,'') + ',' + ISNULL(cteEcoSystemImpactErrorNotes.EcoSystemImapctCountError,'') + ',' + ISNULL(cteEcoSystemImpactErrorNotes.EcoSystemandImpactCountError,'')
From cteEcoSystemImpactErrorNotes
Inner Join StaginTrend st
On cteEcoSystemImpactErrorNotes.StaggingId = st.Id
End

--CTE for Validate KeyCompanyId
Set @IsValid = (select COUNT(Company) from StaginTrend);
IF(@IsValid = 0)
Begin
;with cteKeyCompanyErrorNotes as (
SELECT c.ID AS CompanyId,
CASE     
     WHEN c.Id is null THEN 'Please fill Company name'    
     ELSE NULL    
     END AS KeyCompanyError,    
     st.Id AS StaggingId
FROM StaginTrend st
LEFT JOIN Company c
ON st.Company = c.CompanyName and c.IsDeleted = 0
)
Update StaginTrend 
SET ErrorNotesKeyCompany = ISNULL(cteKeyCompanyErrorNotes.KeyCompanyError,'')
From cteKeyCompanyErrorNotes
Inner Join StaginTrend st
On cteKeyCompanyErrorNotes.StaggingId = st.Id
End
Else
Begin
;With cteCompanyNames as (
	Select StaginTrend.Id,
	[dbo].[udfTrim](st.value) as CompanyNames
	from StaginTrend
	Cross apply string_split(StaginTrend.Company, ',') st
	WHERE ISNULL(st.value,'') != ''
)
,cteValidateCompanyNames as (
SELECT Distinct(c.Id) as CompanyId,
st.CompanyNames,
CASE     
     WHEN c.Id is null THEN 'Company name '+st.CompanyNames+' is not presant in database'    
     ELSE NULL    
     END AS CompanyIdError,     
    st.Id AS StaggingId
FROM cteCompanyNames st
Left JOIN Company c
ON st.CompanyNames = c.CompanyName and c.IsDeleted = 0
),
cteKeyCompanyErrorNotes as (
select
    Distinct(t1.StaggingId),
    stuff((
        select ',' + t.CompanyNames
        from cteValidateCompanyNames t
        where t.StaggingId = t1.StaggingId
        order by t.CompanyNames
        for xml path('')
    ),1,1,'') as Company,
	stuff((
        select ',' + t.CompanyIdError
        from cteValidateCompanyNames t
        where t.StaggingId = t1.StaggingId
        order by t.CompanyIdError
        for xml path('')
    ),1,1,'') as CompanyIdError
from cteValidateCompanyNames t1
group by t1.CompanyIdError,t1.StaggingId
)
Update StaginTrend 
SET ErrorNotesKeyCompany = ISNULL(cteKeyCompanyErrorNotes.CompanyIdError,'')
From cteKeyCompanyErrorNotes
Inner Join StaginTrend st
On cteKeyCompanyErrorNotes.StaggingId = st.Id
End


----CTE for Validate RegionId
--Set @IsValid = (select COUNT(Region) from StaginTrend);
--IF(@IsValid = 0)
--Begin
--;with cteRegionErrorNotes as(
--SELECT r.ID AS RegionId,
--CASE     
--     WHEN r.Id is null THEN 'Please fill Region name'    
--     ELSE NULL    
--     END AS RegionError,    
--     st.Id AS StaggingId
--FROM StaginTrend st
--LEFT JOIN Region r
--ON st.Region = r.RegionName and r.IsDeleted = 0
--)
--Update StaginTrend 
--SET ErrorNotesRegion = ISNULL(cteRegionErrorNotes.RegionError,'')
--From cteRegionErrorNotes
--Inner Join StaginTrend st
--On cteRegionErrorNotes.StaggingId = st.Id
--End
--Else
--Begin
--;With cteRegionNames as (
--	Select StaginTrend.Id,
--	st.value as RegionNames
--	from StaginTrend
--	Cross apply string_split(StaginTrend.Region, ',') st
--)
--,cteValidateRegionNames as (
--SELECT Distinct(r.Id) as RegionId,
--st.RegionNames,
--CASE     
--     WHEN r.Id is null THEN 'Region name is not presant in database'    
--     ELSE NULL    
--     END AS RegionIdError,     
--    st.Id AS StaggingId
--FROM cteRegionNames st
--Left JOIN Region r
--ON st.RegionNames = r.RegionName and r.IsDeleted = 0
--),
--cteRegionErrorNotes as(
--select
--    Distinct(t1.StaggingId),
--    stuff((
--        select ',' + t.RegionNames
--        from cteValidateRegionNames t
--        where t.StaggingId = t1.StaggingId
--        order by t.RegionNames
--        for xml path('')
--    ),1,1,'') as EcoSystem,
--	stuff((
--        select ',' + t.RegionIdError
--        from cteValidateRegionNames t
--        where t.StaggingId = t1.StaggingId
--        order by t.RegionIdError
--        for xml path('')
--    ),1,1,'') as RegionIdError
--from cteValidateRegionNames t1
--group by t1.RegionIdError,t1.StaggingId
--)
--Update StaginTrend 
--SET ErrorNotesRegion = ISNULL(cteRegionErrorNotes.RegionIdError,'')
--From cteRegionErrorNotes
--Inner Join StaginTrend st
--On cteRegionErrorNotes.StaggingId = st.Id
--End

--CTE for Validate CountryId

Set @IsValid = (select COUNT(Country) from StaginTrend);
IF(@IsValid = 0)
Begin
;with cteCountryErrorNotes as(
SELECT r.ID AS CountryId,
CASE     
     WHEN r.Id is null THEN 'Please fill Country name'    
     ELSE NULL    
     END AS CountryError,    
     st.Id AS StaggingId
FROM StaginTrend st
LEFT JOIN Country r
ON st.Country = r.CountryName and r.IsDeleted = 0
)
Update StaginTrend 
SET ErrorNotesCountry = ISNULL(cteCountryErrorNotes.CountryError,'')
From cteCountryErrorNotes
Inner Join StaginTrend st
On cteCountryErrorNotes.StaggingId = st.Id
End
Else
Begin
;With cteCountryNames as (
	Select StaginTrend.Id,
	st.value as CountryNames
	from StaginTrend
	Cross apply string_split(StaginTrend.Country, ',') st
),
cteCountryNamesCount as (
	Select StaginTrend.Id,
	Count(st.value) as CountryNames
	from StaginTrend
	Cross apply string_split(StaginTrend.Country, ',') st
	Where StaginTrend.Id in (Select StaginTrend.Id as StaggingId from StaginTrend
	Cross apply string_split(StaginTrend.Country, ',') st)
	group by StaginTrend.Id
),
cteValidateCountryNames as (
SELECT Distinct(r.Id) as CountryId,
st.CountryNames,
--CASE 
----WHEN exists(select 1 from StaginTrend tr where tr.Id = st.Id and tr.Region != '') then null -- ignore when region is given
--     WHEN exists (select 1 from cteCountryNamesCount icnt where icnt.Id = st.id and icnt.CountryNames < 2) THEN 'Please Enter minium two Countries'
--	 ELSE Null
--	 END 
	 Null as CountryCountError,
CASE     
     WHEN r.Id is null THEN 'Country name '+st.CountryNames+' is not presant in database'    
     ELSE NULL    
     END AS CountryIdError,     
    st.Id AS StaggingId
FROM cteCountryNames st
Left JOIN Country r
ON st.CountryNames = r.CountryName and r.IsDeleted = 0
),
cteCountryErrorNotes as(
select
    Distinct(t1.StaggingId),
    stuff((
        select ',' + t.CountryNames
        from cteValidateCountryNames t
        where t.StaggingId = t1.StaggingId
        order by t.CountryNames
        for xml path('')
    ),1,1,'') as Country,
	stuff((
        select ',' + t.CountryIdError
        from cteValidateCountryNames t
        where t.StaggingId = t1.StaggingId
        order by t.CountryIdError
        for xml path('')
    ),1,1,'') as CountryIdError,
	t1.CountryCountError
from cteValidateCountryNames t1
group by t1.CountryIdError,t1.StaggingId,t1.CountryCountError
)
Update StaginTrend 
SET ErrorNotesCountry = ISNULL(cteCountryErrorNotes.CountryIdError,'') + ',' + ISNULL(cteCountryErrorNotes.CountryCountError,'')
From cteCountryErrorNotes
Inner Join StaginTrend st
On cteCountryErrorNotes.StaggingId = st.Id
End

;With cteSplitRegionNames as (
SELECT st.Id as StaggingId,
	   I.value as RegionName,
	   I.Position as RegionPosition,
	   ind.Id as RegionId,
	   st.TrendName
FROM StaginTrend st
Cross apply FUN_STRING_TOKENIZER(st.Region,',') as I
Left Join Region ind 
On I.Value = ind.RegionName and ind.IsDeleted = 0
),
cteSplitCountryNames as (
SELECT st.Id as StaggingId,
	   I.value as CountryName,
	   I.Position as CountryPosition,
	   ind.Id as CountryId,
	   st.TrendName,
	   case when exists (select 1 from cteSplitRegionNames R
						inner join CountryRegionMap CRM
						on R.RegionId = CRM.RegionId
						where CRM.CountryId = ind.Id) then NULL 
						else 'No mapping between regions and countries' 
						end as ErrorNotesRegionCountry
FROM StaginTrend st
Cross apply FUN_STRING_TOKENIZER(st.Country,',') as I
Left Join Country ind 
On I.value = ind.CountryName and ind.IsDeleted = 0
),
CteRegionMap as (
select RN.*,
case when exists (select 1 from cteSplitCountryNames C
						inner join CountryRegionMap CRM
						on C.CountryId = CRM.CountryId
						where CRM.RegionId = RN.RegionId) then NULL 
						else 'No mapping between regions and countries' 
						end as ErrorNotesCountryRegion
from cteSplitRegionNames RN
Inner Join cteSplitCountryNames CN
On RN.StaggingId = CN.StaggingId
)
Update StaginTrend 
SET ErrorNotesCountry = ISNULL(ErrorNotesCountry,'') + ',' + ISNULL(CteRegionMap.ErrorNotesCountryRegion,'')
From CteRegionMap
Inner Join StaginTrend st
On CteRegionMap.StaggingId = st.Id



;With cteProject as(
--CTE for Validate ProjectId
SELECT prj.ID AS ProjectId,

CASE     
     WHEN prj.Id is null THEN 'Project name is not presant in database'    
     ELSE NULL    
     END AS ProjectIdError,    
     st.Id AS StaggingId
FROM StaginTrend st
LEFT JOIN Project prj
ON st.Project = prj.ProjectName and prj.IsDeleted = 0
),
cteTrend as(
--CTE for Validate TrendId
SELECT --trnd.ID AS TrendId,
       st.TrendName as TrendName,
	   ts.Id as TrendStatusId,
	   imp.Id as ImportanceId,
	   impdir.Id as ImpactDirectionId,
CASE     
     WHEN st.TrendName is null THEN 'Please fill Trend name'    
     ELSE NULL    
     END AS TrendIdError, 
CASE     
     WHEN ts.Id is null THEN 'Trend status name is not presant in database'    
     ELSE NULL    
     END AS TrendStatusIdError, 
CASE     
     WHEN imp.Id is null THEN 'Trend importance name is not presant in database'    
     ELSE NULL    
     END AS TrendImportanceIdError,
CASE     
     WHEN impdir.Id is null THEN 'Trend impact direction name is not presant in database'    
     ELSE NULL    
     END AS TrendImpactDirectionIdError,
CASE     
     WHEN st.TrendValue is null THEN 'Please fill trend value'
	 WHEN isnumeric(st.TrendValue) = 0 THEN 'Please fill trend value field only numbers'    
     ELSE NULL    
     END AS TrendValueError,	     
     st.Id AS StaggingId
FROM StaginTrend st
Left Join TrendStatus ts
ON st.TrendStatus = ts.TrendStatusName and ts.IsDeleted = 0
Left Join Importance imp
On st.Importance = imp.ImportanceName and imp.IsDeleted = 0
Left Join ImpactDirection impdir
On st.ImpactDirection = impdir.ImpactDirectionName and impdir.IsDeleted = 0
),
cteTrendNew as(
SELECT st.TrendName as TrendName,
	   ts.Id as TrendStatusId,
	   imp.Id as ImportanceId,
	   impdir.Id as ImpactDirectionId,
	   p.ID as PrjId,
     st.Id AS StaggingId
FROM StaginTrend st
Left Join TrendStatus ts
ON st.TrendStatus = ts.TrendStatusName and ts.IsDeleted = 0
Left Join Importance imp
On st.Importance = imp.ImportanceName and imp.IsDeleted = 0
Left Join ImpactDirection impdir
On st.ImpactDirection = impdir.ImpactDirectionName and impdir.IsDeleted = 0
Left Join Project p
On p.ProjectName = st.Project and p.IsDeleted = 0
),
cteTrendNameErr as (
Select ct.StaggingId,
CASE
    WHEN
			(select COUNT(*) from 
			[dbo].[Trend] t
			Inner Join cteTrendNew c
			On t.TrendName in (Select Distinct(TrendName) from Trend where TrendName = c.TrendName and c.StaggingId = st.Id)
			and t.ProjectID in (Select Distinct(ProjectID) from Trend where ProjectID = c.PrjId and c.StaggingId = st.Id)
			--and t.TrendStatusID in (Select Distinct(TrendStatusID) from Trend where TrendStatusID = c.TrendStatusId and c.StaggingId = st.Id)
            and t.IsDeleted = 0
			Inner Join StaginTrend st
			On st.Id = c.StaggingId
			) > 0 Then 'Trend is already exists'
		Else null
		End as TrendNameError
		from cteTrendNew ct
		Inner Join StaginTrend st
		On st.Id = ct.StaggingId
),
cteTrendKeyword as(
--CTE for Validate TrendKeyword
SELECT TrendKeyWord, 
CASE     
     WHEN TrendKeyWord is null THEN 'Please fill trend keywords'    
     ELSE NULL    
     END AS TrendIdError,    
     Id AS StaggingId
FROM StaginTrend
),
cteIndustryNames as (
	SELECT ind1.Id as IndustryId,
	StaginTrend.Id as StaggingId,
	ind.value as IndustryNames
	From StaginTrend
	Cross apply string_split(StaginTrend.Industry, ',') ind
	Left JOIN 
	[dbo].SubSegment ind1
INNER JOIN Segment ON ind1.SegmentId = Segment.Id AND Segment.SegmentName = 'End User'
    ON ind.value = ind1.SubSegmentName and ind1.IsDeleted = 0
),
cteIndEndUserList as (
	Select Id, 
	s.value as IndustryEndUser 
	from StaginTrend 
    Cross apply string_split(StaginTrend.IndustryEndUser, ',') s
),
cteValidateIndustryEndUser as(
--CTE for Validate Industry EndUser
Select ind.IndustryId as IndustryEndUserId,
       st.IndustryEndUser,	    
	   ind.IndustryNames,
CASE     
     WHEN (ind.IndustryId is null) or (st.IndustryEndUser is null) THEN 'Please fill the industry end user with entered industry names'
     ELSE NULL    
     END AS IndustryEndUserIdError,    
     st.Id AS StaggingId
from cteIndEndUserList st
Left Join cteIndustryNames ind 
On st.IndustryEndUser = ind.IndustryNames
and st.Id = ind.StaggingId
),
cteIndustryEndUser as(
select
    Distinct(t1.StaggingId),
    stuff((
        select ',' + t.IndustryEndUser
        from cteValidateIndustryEndUser t
        where t.StaggingId = t1.StaggingId
        order by t.IndustryEndUser
        for xml path('')
    ),1,1,'') as IndustryEndUser,
	stuff((
        select ',' + t.IndustryEndUserIdError
        from cteValidateIndustryEndUser t
        where t.StaggingId = t1.StaggingId
        order by t.IndustryEndUserIdError
        for xml path('')
    ),1,1,'') as IndustryEndUserIdError
from cteValidateIndustryEndUser t1
group by t1.IndustryEndUserIdError,t1.StaggingId
),
cteMarketNames as (
	SELECT mar1.Id as MarketId,
	StaginTrend.Id as StaggingId,
	mar.value as MarketNames
	From StaginTrend
	Cross apply string_split(StaginTrend.Market, ',') mar
	Left JOIN Market mar1
    ON mar.value = mar1.MarketName and mar1.IsDeleted = 0
),
cteMarketEndUserList as (
	Select Id, 
	s.value as MarketEndUser 
	from StaginTrend 
    Cross apply string_split(StaginTrend.MarketEndUser, ',') s
),
cteValidateMarketEndUser as(
--CTE for Validate Market EndUser
Select st.MarketEndUser,
	   mar.MarketId as MarketEndUserId,
CASE     
     WHEN (mar.MarketId is null) or (st.MarketEndUser is null) THEN 'Please fill the market end user with entered Market names'
     ELSE NULL    
     END AS MarketEndUserIdError,    
     st.Id AS StaggingId
from cteMarketEndUserList st
Left Join cteMarketNames mar 
On st.MarketEndUser = mar.MarketNames
and st.Id = mar.StaggingId
),
cteMarketEndUser as(
select
    Distinct(t1.StaggingId),
    stuff((
        select ',' + t.MarketEndUser
        from cteValidateMarketEndUser t
        where t.StaggingId = t1.StaggingId
        order by t.MarketEndUser
        for xml path('')
    ),1,1,'') as MarketEndUser,
	stuff((
        select ',' + t.MarketEndUserIdError
        from cteValidateMarketEndUser t
        where t.StaggingId = t1.StaggingId
        order by t.MarketEndUserIdError
        for xml path('')
    ),1,1,'') as MarketEndUserIdError
from cteValidateMarketEndUser t1
group by t1.MarketEndUserIdError,t1.StaggingId
),
cteProjectNames as (
	SELECT prj1.Id as ProjectId,
	StaginTrend.Id as StaggingId,
	prj.value as ProjectNames
	From StaginTrend
	Cross apply string_split(StaginTrend.Project1, ',') prj
	Left JOIN Project prj1
    ON prj.value = prj1.ProjectName and prj1.IsDeleted = 0
),
cteProjectEndUserList as (
	Select Id, 
	s.value as ProjectEndUser 
	from StaginTrend 
    Cross apply string_split(StaginTrend.ProjectEndUser, ',') s
),
cteValidateProjectEndUser as(
--CTE for Validate Project EndUser
Select st.ProjectEndUser,
	   p.ProjectId as ProjectEndUserId,
CASE     
     WHEN (p.ProjectId is null) or (st.ProjectEndUser is null) THEN 'Please fill the Project end user with entered project1 names'
     ELSE NULL    
     END AS ProjectEndUserIdError,    
     st.Id AS StaggingId
from cteProjectEndUserList st
Left Join cteProjectNames p 
On st.ProjectEndUser = p.ProjectNames
and st.Id = p.StaggingId
),
cteProjectEndUser as(
select
    Distinct(t1.StaggingId),
    stuff((
        select ',' + t.ProjectEndUser
        from cteValidateProjectEndUser t
        where t.StaggingId = t1.StaggingId
        order by t.ProjectEndUser
        for xml path('')
    ),1,1,'') as MarketEndUser,
	stuff((
        select ',' + t.ProjectEndUserIdError
        from cteValidateProjectEndUser t
        where t.StaggingId = t1.StaggingId
        order by t.ProjectEndUserIdError
        for xml path('')
    ),1,1,'') as ProjectEndUserIdError
from cteValidateProjectEndUser t1
group by t1.ProjectEndUserIdError,t1.StaggingId
),
cteCompanyGroupNames as (
	SELECT cg1.Id as CompanyGroupId,
	StaginTrend.Id as StaggingId,
	cg.value as CompanyGroupNames
	From StaginTrend
	Cross apply string_split(StaginTrend.CompanyGroup, ',') cg
	Left JOIN CompanyGroup cg1
    ON cg.value = cg1.CompanyGroupName and cg1.IsDeleted = 0
),
cteCompanyGroupEndUserList as (
	Select Id, 
	s.value as CompanyGroupEndUser 
	from StaginTrend 
    Cross apply string_split(StaginTrend.CompanyGroupEndUser, ',') s
),
cteValidateCompanyGroupEndUser as(
--CTE for Validate CompanyGroup EndUser
Select st.CompanyGroupEndUser,
	   cg.CompanyGroupId as CompanyGroupEndUserId,
CASE     
     WHEN (cg.CompanyGroupId is null) or (st.CompanyGroupEndUser is null) THEN 'Please fill the CompanyGroup end user with entered Companygroup names'
     ELSE NULL    
     END AS CompanyGroupEndUserIdError,    
     st.Id AS StaggingId
from cteCompanyGroupEndUserList st
Left Join cteCompanyGroupNames cg 
On st.CompanyGroupEndUser = cg.CompanyGroupNames
and st.Id = cg.StaggingId
),
cteCompanyGroupEndUser as(
select
    Distinct(t1.StaggingId),
    stuff((
        select ',' + t.CompanyGroupEndUser
        from cteValidateCompanyGroupEndUser t
        where t.StaggingId = t1.StaggingId
        order by t.CompanyGroupEndUser
        for xml path('')
    ),1,1,'') as CompanyGroupEndUser,
	stuff((
        select ',' + t.CompanyGroupEndUserIdError
        from cteValidateCompanyGroupEndUser t
        where t.StaggingId = t1.StaggingId
        order by t.CompanyGroupEndUserIdError
        for xml path('')
    ),1,1,'') as CompanyGroupEndUserIdError
from cteValidateCompanyGroupEndUser t1
group by t1.CompanyGroupEndUserIdError,t1.StaggingId
),
cteTimeTagNames as (
	SELECT tt1.Id as TimeTagId,
	StaginTrend.Id as StaggingId,
	tt.value as TimeTagNames
	From StaginTrend
	Cross apply string_split(StaginTrend.TimeTag, ',') tt
	Left JOIN TimeTag tt1
    ON tt.value = tt1.TimeTagName and tt1.IsDeleted = 0
),
cteTimeTagEndUserList as (
	Select Id, 
	s.value as TimeTagEndUser 
	from StaginTrend 
    Cross apply string_split(StaginTrend.TimeTagEndUser, ',') s
),
cteValidateTimeTagEndUser as(
--CTE for Validate TimeTag EndUser
Select st.TimeTagEndUser,
	   tt.TimeTagId as TimeTagEndUserId,
CASE     
     WHEN (tt.TimeTagId is null) or (st.TimeTagEndUser is null) THEN 'Please fill the TimeTag end user with entered Timetag names'
     ELSE NULL    
     END AS TimeTagEndUserIdError,    
     st.Id AS StaggingId
from cteTimeTagEndUserList st
Left Join cteTimeTagNames tt 
On st.TimeTagEndUser = tt.TimeTagNames
and st.Id = tt.StaggingId
),
cteTimeTagEndUser as(
select
    Distinct(t1.StaggingId),
    stuff((
        select ',' + t.TimeTagEndUser
        from cteValidateTimeTagEndUser t
        where t.StaggingId = t1.StaggingId
        order by t.TimeTagEndUser
        for xml path('')
    ),1,1,'') as TimeTagEndUser,
	stuff((
        select ',' + t.TimeTagEndUserIdError
        from cteValidateTimeTagEndUser t
        where t.StaggingId = t1.StaggingId
        order by t.TimeTagEndUserIdError
        for xml path('')
    ),1,1,'') as TimeTagEndUserIdError
from cteValidateTimeTagEndUser t1
group by t1.TimeTagEndUserIdError,t1.StaggingId
),








cteEcoSystemNames as (
	SELECT es1.Id as EcoSystemId,
	StaginTrend.Id as StaggingId,
	es.value as EcoSystemNames
	From StaginTrend
	Cross apply string_split(StaginTrend.EcoSystem, ',') es
	--Left JOIN Segment es1
	--ESM mapping
	Left JOIN segment es1 ON es.value = es1.SegmentName
	inner join  ESMSegmentMapping esm1 on es1.Id = esm1.SegmentId and esm1.ProjectId=@ProjectId and es1.IsDeleted = 0 

    --ON es.value = es1.SegmentName and es1.IsDeleted = 0
),
cteEcoSystemEndUserList as (
	Select Id, 
	s.value as EcoSystemEndUser 
	from StaginTrend 
    Cross apply string_split(StaginTrend.EcoSystemEndUser, ',') s
),
cteValidateEcoSystemEndUser as(
--CTE for Validate EcoSystem EndUser
Select st.EcoSystemEndUser,
	   es.EcoSystemId as EcoSystemEndUserId,
CASE     
     WHEN (es.EcoSystemId is null) or (st.EcoSystemEndUser is null) THEN 'Please fill the EcoSystem end user with entered Ecosystem names'
     ELSE NULL    
     END AS EcoSystemEndUserIdError,    
     st.Id AS StaggingId
from cteEcoSystemEndUserList st
Left Join cteEcoSystemNames es 
On st.EcoSystemEndUser = es.EcoSystemNames and st.Id = es.StaggingId
),
cteEcoSystemEndUser as(
select
    Distinct(t1.StaggingId),
    stuff((
        select ',' + t.EcoSystemEndUser
        from cteValidateEcoSystemEndUser t
        where t.StaggingId = t1.StaggingId
        order by t.EcoSystemEndUser
        for xml path('')
    ),1,1,'') as EcoSystemEndUser,
	stuff((
        select ',' + t.EcoSystemEndUserIdError
        from cteValidateEcoSystemEndUser t
        where t.StaggingId = t1.StaggingId
        order by t.EcoSystemEndUserIdError
        for xml path('')
    ),1,1,'') as EcoSystemEndUserIdError
from cteValidateEcoSystemEndUser t1
group by t1.EcoSystemEndUserIdError,t1.StaggingId
),



cteCountryNames as (
	SELECT c1.Id as CountryId,
	StaginTrend.Id as StaggingId,
	c.value as CountryNames
	From StaginTrend
	Cross apply string_split(StaginTrend.Country, ',') c
	Left JOIN Country c1
    ON c.value = c1.CountryName and c1.IsDeleted = 0
),
cteCountryEndUserList as (
	Select Id, 
	s.value as CountryEndUser 
	from StaginTrend 
    Cross apply string_split(StaginTrend.CountryEndUser, ',') s
),
cteValidateCountryEndUser as(
--CTE for Validate Country EndUser
Select st.CountryEndUser,
	   r.CountryId as CountryEndUserId,
CASE     
     WHEN (r.CountryId is null) or (st.CountryEndUser is null) THEN 'Please fill the Country end user with entered country names'
     ELSE NULL    
     END AS CountryEndUserIdError,    
     st.Id AS StaggingId
from cteCountryEndUserList st
Left Join cteCountryNames r 
On st.CountryEndUser = r.CountryNames and st.Id = r.StaggingId
),
cteCountryEndUser as(
select
    Distinct(t1.StaggingId),
    stuff((
        select ',' + t.CountryEndUser
        from cteValidateCountryEndUser t
        where t.StaggingId = t1.StaggingId
        order by t.CountryEndUser
        for xml path('')
    ),1,1,'') as CountryEndUser,
	stuff((
        select ',' + t.CountryEndUserIdError
        from cteValidateCountryEndUser t
        where t.StaggingId = t1.StaggingId
        order by t.CountryEndUserIdError
        for xml path('')
    ),1,1,'') as CountryEndUserIdError
from cteValidateCountryEndUser t1
group by t1.CountryEndUserIdError,t1.StaggingId
),
cteValueConversionId as(
--CTE for Validate ValueConversionId
SELECT valcon.ID AS ValueConversionId,
CASE     
     WHEN (valcon.Id is null) or (st.ValueConversion is null) THEN 'ValueConversion name is not presant in database'    
     ELSE NULL    
     END AS ValueConversionIdError,   	  
     st.Id AS StaggingId
FROM StaginTrend st
LEFT JOIN ValueConversion valcon
ON st.ValueConversion = valcon.ValueName and valcon.IsDeleted = 0
),
cteIndustry as (
SELECT st.Industry AS Industry,
CASE     
     WHEN st.Industry is null THEN 'Please fill industry name'    
     ELSE NULL    
     END AS IndustryIdError,    
     st.Id AS StaggingId
FROM StaginTrend st
),
cteMarket as (
SELECT st.Market AS Market,
CASE     
     WHEN st.Market is null THEN 'Please fill market name'    
     ELSE NULL    
     END AS MarketIdError,    
     st.Id AS StaggingId
FROM StaginTrend st
),
cteProject1 as (
SELECT st.Project1 AS Project1,
CASE     
     WHEN st.Project1 is null THEN 'Please fill Project1 name'    
     ELSE NULL    
     END AS Project1IdError,    
     st.Id AS StaggingId
FROM StaginTrend st
),
cteCompanyGroup as (
SELECT st.CompanyGroup AS CompanyGroup,
CASE     
     WHEN st.CompanyGroup is null THEN 'Please fill CompanyGroup name'    
     ELSE NULL    
     END AS CompanyGroupIdError,    
     st.Id AS StaggingId
FROM StaginTrend st
),
cteTimeTag as (
SELECT st.TimeTag AS TimeTag,
CASE     
     WHEN st.TimeTag is null THEN 'Please fill TimeTag name'    
     ELSE NULL    
     END AS TimeTagIdError,    
     st.Id AS StaggingId
FROM StaginTrend st
),
cteEcosystem as (
SELECT st.EcoSystem AS EcoSystem,
CASE     
     WHEN st.EcoSystem is null THEN 'Please fill EcoSystem name'    
     ELSE NULL    
     END AS EcoSystemIdError,    
     st.Id AS StaggingId
FROM StaginTrend st
),
--cteRegion as (
--SELECT st.Region AS Region,
--CASE     
--     WHEN st.Region is null THEN 'Please fill Region name'    
--     ELSE NULL    
--     END AS RegionIdError,    
--     st.Id AS StaggingId
--FROM StaginTrend st
--),
cteCountry as (
SELECT st.Country AS Country,
CASE     
     WHEN st.Country is null THEN 'Please fill Country name'    
     ELSE NULL    
     END AS CountryIdError,    
     st.Id AS StaggingId
FROM StaginTrend st
),
cteKeyCompany as (
SELECT st.Company AS Company,
CASE     
     WHEN st.Company is null THEN 'Please fill Company name'    
     ELSE NULL    
     END AS CompanyIdError,    
     st.Id AS StaggingId
FROM StaginTrend st
),
cteTrendKeywordAuthorNotes as (
Select st.keyword_AuthorRemark, 
CASE     
     WHEN st.keyword_AuthorRemark is null THEN 'Please fill Keyword Author Remark'   
     ELSE NULL    
     END AS KeywordAuthorRemarkError,    
     st.Id AS StaggingId
from StaginTrend st
),
cteIndustryAuthorNotes as (
Select st.Industry_AuthorRemark, 
CASE     
     WHEN st.Industry_AuthorRemark is null THEN 'Please fill Industry Author Remark' 
     ELSE NULL    
     END AS IndustryAuthorRemarkError,    
     st.Id AS StaggingId
from StaginTrend st
),
cteMarketAuthorNotes as (
Select st.Market_AuthorRemark, 
CASE     
     WHEN st.Market_AuthorRemark is null THEN 'Please fill Market Author Remark' 
     ELSE NULL    
     END AS MarketAuthorRemarkError,    
     st.Id AS StaggingId
from StaginTrend st
),
cteProjectAuthorNotes as (
Select st.Project_AuthorRemark, 
CASE     
     WHEN st.Project_AuthorRemark is null THEN 'Please fill Project Author Remark' 
     ELSE NULL    
     END AS ProjectAuthorRemarkError,    
     st.Id AS StaggingId
from StaginTrend st
),
cteCompanyGroupsAuthorNotes as (
Select st.CompanyGroup_AuthorRemark, 
CASE     
     WHEN st.CompanyGroup_AuthorRemark is null THEN 'Please fill CompanyGroup Author Remark' 
     ELSE NULL    
     END AS CompanyGroupAuthorRemarkError,    
     st.Id AS StaggingId
from StaginTrend st
),
cteTimeTagAuthorNotes as (
Select st.TimeTag_AuthorRemark, 
CASE     
     WHEN st.TimeTag_AuthorRemark is null THEN 'Please fill TimeTag Author Remark' 
     ELSE NULL    
     END AS TimeTagAuthorRemarkError,    
     st.Id AS StaggingId
from StaginTrend st
),
cteEcoSystemAuthorNotes as (
Select st.EcoSystem_AuthorRemark, 
CASE     
     WHEN st.EcoSystem_AuthorRemark is null THEN 'Please fill EcoSystem Author Remark'
     ELSE NULL    
     END AS EcoSystemAuthorRemarkError,    
     st.Id AS StaggingId
from StaginTrend st
),
cteKeyCompaniesAuthorNotes as (
Select st.KeyCompany_AuthorRemark, 
CASE     
     WHEN st.KeyCompany_AuthorRemark is null THEN 'Please fill KeyCompany Author Remark' 
     ELSE NULL    
     END AS KeyCompanyAuthorRemarkError,    
     st.Id AS StaggingId
from StaginTrend st
),
cteRegionAuthorNotes as (
Select st.Region_AuthorRemark, 
CASE     
     WHEN st.Region_AuthorRemark is null THEN 'Please fill Region Author Remark' 
     ELSE NULL    
     END AS RegionAuthorRemarkError,    
     st.Id AS StaggingId
from StaginTrend st
),
cteValuesAuthorNotes as (
Select st.Value_AuthorRemark, 
CASE     
     WHEN st.Value_AuthorRemark is null THEN 'Please fill Value Author Remark' 
     ELSE NULL    
     END AS ValueAuthorRemarkError,    
     st.Id AS StaggingId
from StaginTrend st
),
cteTrendAuthorNotes as (
Select st.Trend_AuthorRemark, 
CASE     
     WHEN st.Trend_AuthorRemark is null THEN 'Please fill Trend Author Remark' 
     ELSE NULL    
     END AS TrendAuthorRemarkError,    
     st.Id AS StaggingId
from StaginTrend st
)
Update StaginTrend 
SET ErrorNotesProject = ISNULL(cteProject.ProjectIdError,''),
    ErrorNotesKeyword = ISNULL(cteTrendKeyword.TrendIdError,''),
	ErrorNotesIndustry = ISNULL(ErrorNotesIndustry,' ') + ',' + ISNULL(cteIndustry.IndustryIdError,'') + ',' + ISNULL(cteIndustryEndUser.IndustryEndUserIdError,' '),
	ErrorNotesMarket = ISNULL(ErrorNotesMarket,' ') + ',' + ISNULL(cteMarket.MarketIdError,'') + ',' + ISNULL(cteMarketEndUser.MarketEndUserIdError,' '),
	ErrorNotesProject1 = ISNULL(ErrorNotesProject1,' ') + ',' + ISNULL(cteProject1.Project1IdError,'') + ',' + ISNULL(cteProjectEndUser.ProjectEndUserIdError,' '),
	ErrorNotesTrend = ISNULL(cteTrendNameErr.TrendNameError,'') + ',' + ISNULL(cteTrend.TrendIdError,'') + ',' + ISNULL(cteTrend.TrendStatusIdError,' ') + ',' + ISNULL(cteTrend.TrendImportanceIdError,' ') + ',' + ISNULL(cteTrend.TrendImpactDirectionIdError,' ') + ',' + ISNULL(cteTrend.TrendValueError,' '),
	ErrorNotesCompanyGroup = ISNULL(ErrorNotesCompanyGroup,' ') + ',' + ISNULL(cteCompanyGroup.CompanyGroupIdError,'') + ',' + ISNULL(cteCompanyGroupEndUser.CompanyGroupEndUserIdError,' '),
	ErrorNotesEcoSystem = ISNULL(ErrorNotesEcoSystem,' ') + ',' + ISNULL(cteEcosystem.EcoSystemIdError,'') + ',' + ISNULL(cteEcoSystemEndUser.EcoSystemEndUserIdError,' '),
	ErrorNotesEcoSystemSubSegemnt=ISNULL(ErrorNotesEcoSystemSubSegemnt,' ') ,
	ErrorNotesTimeTag = ISNULL(ErrorNotesTimeTag,' ') + ',' + ISNULL(cteTimeTag.TimeTagIdError,'') + ',' + ISNULL(cteTimeTagEndUser.TimeTagEndUserIdError,' '),
	ErrorNotesRegion = ISNULL(ErrorNotesRegion,' '), 
	ErrorNotesCountry = ISNULL(ErrorNotesCountry,' ') + ',' + ISNULL(cteCountry.CountryIdError,'') + ',' + ISNULL(cteCountryEndUser.CountryEndUserIdError,' '),
	ErrorNotesValueConversion = ISNULL(cteValueConversionId.ValueConversionIdError,''),
	ErrorNotesKeyCompany = ISNULL(ErrorNotesKeyCompany,' ') + ',' + ISNULL(cteKeyCompany.CompanyIdError,''),
	ErrorNotesAuthorRemark = ISNULL(cteTrendKeywordAuthorNotes.KeywordAuthorRemarkError,'') + ',' + ISNULL(cteIndustryAuthorNotes.IndustryAuthorRemarkError,'') + ',' + ISNULL(cteMarketAuthorNotes.MarketAuthorRemarkError,'') + ',' + ISNULL(cteProjectAuthorNotes.ProjectAuthorRemarkError,'') + ',' + ISNULL(cteCompanyGroupsAuthorNotes.CompanyGroupAuthorRemarkError,'') + ',' + ISNULL(cteTimeTagAuthorNotes.TimeTagAuthorRemarkError,'') + ',' + ISNULL(cteEcoSystemAuthorNotes.EcoSystemAuthorRemarkError,'') + ',' + ISNULL(cteKeyCompaniesAuthorNotes.KeyCompanyAuthorRemarkError,'') + ',' + ISNULL(cteRegionAuthorNotes.RegionAuthorRemarkError,'') + ',' + ISNULL(cteValuesAuthorNotes.ValueAuthorRemarkError,'') + ',' + ISNULL(cteTrendAuthorNotes.TrendAuthorRemarkError,'')
From cteProject
Inner Join StaginTrend st
ON st.Id = cteProject.StaggingId

Inner Join cteTrendNameErr
On st.Id = cteTrendNameErr.StaggingId

Inner Join cteTrendKeyword
ON st.Id = cteTrendKeyword.StaggingId

Inner Join cteTrend
ON st.Id = cteTrend.StaggingId

Inner Join cteIndustry 
ON st.Id = cteIndustry.StaggingId

Inner Join cteIndustryEndUser 
ON st.Id = cteIndustryEndUser.StaggingId

Inner Join cteMarket 
ON st.Id = cteMarket.StaggingId

Inner Join cteMarketEndUser 
ON st.Id = cteMarketEndUser.StaggingId

Inner Join cteProject1 
ON st.Id = cteProject1.StaggingId

Inner Join cteProjectEndUser 
ON st.Id = cteProjectEndUser.StaggingId

Inner Join cteCompanyGroup
ON st.Id = cteCompanyGroup.StaggingId

Inner Join cteCompanyGroupEndUser 
ON st.Id = cteCompanyGroupEndUser.StaggingId

Inner Join cteTimeTag
ON st.Id = cteTimeTag.StaggingId

Inner Join cteTimeTagEndUser 
ON st.Id = cteTimeTagEndUser.StaggingId

Inner Join cteEcosystem
ON st.Id = cteEcosystem.StaggingId

Inner Join cteEcoSystemEndUser 
ON st.Id = cteEcoSystemEndUser.StaggingId


--Inner Join cteRegion
--ON st.Id = cteRegion.StaggingId

Inner Join cteCountry
ON st.Id = cteCountry.StaggingId

Inner Join cteCountryEndUser 
ON st.Id = cteCountryEndUser.StaggingId

Inner Join cteValueConversionId 
ON st.Id = cteValueConversionId.StaggingId

Inner Join cteKeyCompany 
ON st.Id = cteKeyCompany.StaggingId

Inner Join cteTrendKeywordAuthorNotes 
ON st.Id = cteTrendKeywordAuthorNotes.StaggingId

Inner Join cteIndustryAuthorNotes 
ON st.Id = cteIndustryAuthorNotes.StaggingId

Inner Join cteMarketAuthorNotes 
ON st.Id = cteMarketAuthorNotes.StaggingId

Inner Join cteProjectAuthorNotes 
ON st.Id = cteProjectAuthorNotes.StaggingId

Inner Join cteCompanyGroupsAuthorNotes 
ON st.Id = cteCompanyGroupsAuthorNotes.StaggingId

Inner Join cteTimeTagAuthorNotes 
ON st.Id = cteTimeTagAuthorNotes.StaggingId

Inner Join cteEcoSystemAuthorNotes 
ON st.Id = cteEcoSystemAuthorNotes.StaggingId

Inner Join cteKeyCompaniesAuthorNotes 
ON st.Id = cteKeyCompaniesAuthorNotes.StaggingId

Inner Join cteRegionAuthorNotes 
ON st.Id = cteRegionAuthorNotes.StaggingId

Inner Join cteValuesAuthorNotes 
ON st.Id = cteValuesAuthorNotes.StaggingId

Inner Join cteTrendAuthorNotes 
ON st.Id = cteTrendAuthorNotes.StaggingId

;With cteRemoveDuplicateValidations AS (
select Id,
CAST('<x>' + REPLACE(ErrorNotesProject,',','</x><x>') + '</x>' AS XML) AS ErrorNotesProject,
CAST('<x>' + REPLACE(ErrorNotesTrend,',','</x><x>') + '</x>' AS XML) AS ErrorNotesTrend,
CAST('<x>' + REPLACE(ErrorNotesKeyword,',','</x><x>') + '</x>' AS XML) AS ErrorNotesKeyword,
CAST('<x>' + REPLACE(ErrorNotesIndustry,',','</x><x>') + '</x>' AS XML) AS ErrorNotesIndustry,
CAST('<x>' + REPLACE(ErrorNotesMarket,',','</x><x>') + '</x>' AS XML) AS ErrorNotesMarket,
CAST('<x>' + REPLACE(ErrorNotesProject1,',','</x><x>') + '</x>' AS XML) AS ErrorNotesProject1,
CAST('<x>' + REPLACE(ErrorNotesCompanyGroup,',','</x><x>') + '</x>' AS XML) AS ErrorNotesCompanyGroup,
CAST('<x>' + REPLACE(ErrorNotesTimeTag,',','</x><x>') + '</x>' AS XML) AS ErrorNotesTimeTag,
CAST('<x>' + REPLACE(ErrorNotesEcoSystem,',','</x><x>') + '</x>' AS XML) AS ErrorNotesEcoSystem,
CAST('<x>' + REPLACE(ErrorNotesEcoSystemSubSegemnt,',','</x><x>') + '</x>' AS XML) AS ErrorNotesEcoSystemSubSegemnt,
CAST('<x>' + REPLACE(ErrorNotesKeyCompany,',','</x><x>') + '</x>' AS XML) AS ErrorNotesKeyCompany,
CAST('<x>' + REPLACE(ErrorNotesRegion,',','</x><x>') + '</x>' AS XML) AS ErrorNotesRegion,
CAST('<x>' + REPLACE(ErrorNotesCountry,',','</x><x>') + '</x>' AS XML) AS ErrorNotesCountry,
CAST('<x>' + REPLACE(ErrorNotesValueConversion,',','</x><x>') + '</x>' AS XML) AS ErrorNotesValueConversion,
CAST('<x>' + REPLACE(ErrorNotesAuthorRemark,',','</x><x>') + '</x>' AS XML) AS ErrorNotesAuthorRemark
from StaginTrend
),
cteErrorValues as (
Select cteRemoveDuplicateValidations.Id as StaggingId,
cteRemoveDuplicateValidations.ErrorNotesProject.query('distinct-values(/x/text())').value('.','nvarchar(max)') AS ErrorNotesProject,
cteRemoveDuplicateValidations.ErrorNotesTrend.query('distinct-values(/x/text())').value('.','nvarchar(max)') AS ErrorNotesTrend,
cteRemoveDuplicateValidations.ErrorNotesKeyword.query('distinct-values(/x/text())').value('.','nvarchar(max)') AS ErrorNotesKeyword,
cteRemoveDuplicateValidations.ErrorNotesIndustry.query('distinct-values(/x/text())').value('.','nvarchar(max)') AS ErrorNotesIndustry,
cteRemoveDuplicateValidations.ErrorNotesMarket.query('distinct-values(/x/text())').value('.','nvarchar(max)') AS ErrorNotesMarket,
cteRemoveDuplicateValidations.ErrorNotesProject1.query('distinct-values(/x/text())').value('.','nvarchar(max)') AS ErrorNotesProject1,
cteRemoveDuplicateValidations.ErrorNotesCompanyGroup.query('distinct-values(/x/text())').value('.','nvarchar(max)') AS ErrorNotesCompanyGroup,
cteRemoveDuplicateValidations.ErrorNotesTimeTag.query('distinct-values(/x/text())').value('.','nvarchar(max)') AS ErrorNotesTimeTag,
cteRemoveDuplicateValidations.ErrorNotesEcoSystem.query('distinct-values(/x/text())').value('.','nvarchar(max)') AS ErrorNotesEcoSystem,
--cteRemoveDuplicateValidations.ErrorNotesEcoSystemSubSegemnt.query('distinct-values(/x/text())').value('.','nvarchar(max)') AS ErrorNotesEcoSystemSubSegemnt,
cteRemoveDuplicateValidations.ErrorNotesKeyCompany.query('distinct-values(/x/text())').value('.','nvarchar(max)') AS ErrorNotesKeyCompany,
cteRemoveDuplicateValidations.ErrorNotesRegion.query('distinct-values(/x/text())').value('.','nvarchar(max)') AS ErrorNotesRegion,
cteRemoveDuplicateValidations.ErrorNotesCountry.query('distinct-values(/x/text())').value('.','nvarchar(max)') AS ErrorNotesCountry,
cteRemoveDuplicateValidations.ErrorNotesValueConversion.query('distinct-values(/x/text())').value('.','nvarchar(max)') AS ErrorNotesValueConversion,
cteRemoveDuplicateValidations.ErrorNotesAuthorRemark.query('distinct-values(/x/text())').value('.','nvarchar(max)') AS ErrorNotesAuthorRemark
From cteRemoveDuplicateValidations
Inner Join StaginTrend st
On cteRemoveDuplicateValidations.Id = st.Id
)
Update StaginTrend
Set ErrorNotesProject = cev.ErrorNotesProject,
ErrorNotesTrend = cev.ErrorNotesTrend,
ErrorNotesKeyword = cev.ErrorNotesKeyword,
ErrorNotesIndustry = cev.ErrorNotesIndustry,
ErrorNotesMarket = cev.ErrorNotesMarket,
ErrorNotesProject1 = cev.ErrorNotesProject1,
ErrorNotesCompanyGroup = cev.ErrorNotesCompanyGroup,
ErrorNotesTimeTag = cev.ErrorNotesTimeTag,
ErrorNotesEcoSystem = cev.ErrorNotesEcoSystem,
--ErrorNotesEcoSystemSubSegemnt=cev.ErrorNotesEcoSystemSubSegemnt,
ErrorNotesKeyCompany = cev.ErrorNotesKeyCompany,
ErrorNotesRegion = cev.ErrorNotesRegion,
ErrorNotesCountry = cev.ErrorNotesCountry,
ErrorNotesValueConversion = cev.ErrorNotesValueConversion,
ErrorNotesAuthorRemark = cev.ErrorNotesAuthorRemark
from cteErrorValues cev
Inner Join StaginTrend st
On cev.StaggingId = st.Id


;with cteGetTimeTag as(
	Select StaginTrend.Id as StaggingId,
	tt.Id as TimeTagId,
	st.value as TimeTagName
	From StaginTrend
	Cross Apply string_split(StaginTrend.TimeTag,',') st
	Inner Join TimeTag tt
	On tt.TimeTagName = st.value
),
cteImmediate as (
SELECT st.Id,
       cgtt.TimeTagName,
CASE 
	WHEN (st.Value_2019 is null) Then 'please enter the value for Value_2019'
	ELSE Null
	END as ImmediateValueError,
CASE 
    WHEN (ISNUMERIC(Value_2019) = 0) Then 'please enter the only digits for Value_2019'
	ELSE Null
	END as ImmediateValueNumberError,
CASE 
    WHEN (st.Rationale_2019 is null) Then 'please enter the Rationale_2019'
	ELSE Null
	END as ImmediateRationaleError
FROM StaginTrend st
Inner Join cteGetTimeTag cgtt
On cgtt.StaggingId = st.Id
and UPPER(cgtt.TimeTagName) = UPPER('Immediate')
),
cteShortTerm as (
SELECT st.Id,
       cgtt.TimeTagName,
CASE 
	WHEN (st.Value_2019 is null) or (st.Value_2020 is null)  Then 'please enter the value for Value_2019,Value_2020'--or (st.Value_2021 is null),Value_2021
	ELSE Null
	END as ShortTermValueError,
CASE 
    WHEN (ISNUMERIC(Value_2019) = 0) or (ISNUMERIC(Value_2020) = 0)  Then 'please enter the only digits for Value_2019,Value_2020'--or (ISNUMERIC(Value_2021) = 0) ,Value_2021
	ELSE Null
	END as ShortTermValueNumberError,
CASE 
    WHEN (st.Rationale_2019 is null) or (st.Rationale_2020 is null)  Then 'please enter the Rationale_2019, Rationale_2020'--or (st.Rationale_2021 is null), Rationale_2021
	ELSE Null
	END as ShortTermRationaleError
FROM StaginTrend st
Inner Join cteGetTimeTag cgtt
On cgtt.StaggingId = st.Id
and UPPER(cgtt.TimeTagName) = UPPER('ShortTerm')
),
cteMidTerm as (
SELECT st.Id,
       cgtt.TimeTagName,
CASE 
	WHEN (st.Value_2019 is null) or (st.Value_2020 is null) or (st.Value_2021 is null) or (st.Value_2022 is null)  Then 'please enter the value for Value_2019,Value_2020,Value_2021,Value_2022'
	ELSE Null
	END as MidTermValueError,
CASE 
    WHEN (ISNUMERIC(Value_2019) = 0) or (ISNUMERIC(Value_2020) = 0) or (ISNUMERIC(Value_2021) = 0) or (ISNUMERIC(Value_2022) = 0)  Then 'please enter the only digits for Value_2019,Value_2020,Value_2021,Value_2022'
	ELSE Null
	END as MidTermValueNumberError,
CASE 
    WHEN (st.Rationale_2019 is null) or (st.Rationale_2020 is null) or (st.Rationale_2021 is null) or (st.Rationale_2022 is null)  Then 'please enter the Rationale_2019, Rationale_2020, Rationale_2021, Rationale_2022'
	ELSE Null
	END as MidTermRationaleError
FROM StaginTrend st
Inner Join cteGetTimeTag cgtt
On cgtt.StaggingId = st.Id
and UPPER(cgtt.TimeTagName) = UPPER('MidTerm')
),
cteLongTerm as (
SELECT st.Id,
       cgtt.TimeTagName,
CASE 
	WHEN (st.Value_2019 is null) or (st.Value_2020 is null) or (st.Value_2021 is null) or (st.Value_2022 is null) or (st.Value_2023 is null) 
	 Then 'please enter the value for Value_2019,Value_2020,Value_2021,Value_2022,Value_2023'
	ELSE Null
	END as LongTermValueError,
CASE 
    WHEN (ISNUMERIC(Value_2019) = 0) or (ISNUMERIC(Value_2020) = 0) or (ISNUMERIC(Value_2021) = 0) or (ISNUMERIC(Value_2022) = 0) or (ISNUMERIC(Value_2023) = 0) 
	Then 'please enter the only digits for Value_2019,Value_2020,Value_2021,Value_2022,Value_2023'
	ELSE Null
	END as LongTermValueNumberError,
CASE 
    WHEN (st.Rationale_2019 is null) or (st.Rationale_2020 is null) or (st.Rationale_2021 is null) or (st.Rationale_2022 is null) or (st.Rationale_2023 is null)
	Then 'please enter the Rationale_2019, Rationale_2020, Rationale_2021, Rationale_2022, Rationale_2023'
	ELSE Null
	END as LongTermRationaleError
FROM StaginTrend st
Inner Join cteGetTimeTag cgtt
On cgtt.StaggingId = st.Id
and UPPER(cgtt.TimeTagName) = UPPER('LongTerm') 
),
cteVeryLongTerm as (
SELECT st.Id,
       cgtt.TimeTagName,
CASE 
	WHEN (st.Value_2019 is null) or (st.Value_2020 is null) or (st.Value_2021 is null) or (st.Value_2022 is null) or (st.Value_2023 is null)or (st.Value_2024 is null)or (st.Value_2025 is null)or (st.Value_2026 is null)or (st.Value_2027 is null) or (st.Value_2028 is null) or (st.Value_2029 is null)  Then 'please enter the value for Value_2019,Value_2020,Value_2021,Value_2022,Value_2023,Value_2024,Value_2025,Value_2026,Value_2027,Value_2028 ,Value_2029'--or (st.Value_2030 is null) or (st.Value_2031 is null)
	ELSE Null
	END as VeryLongTermValueError,
CASE 
    WHEN (ISNUMERIC(Value_2019) = 0) or (ISNUMERIC(Value_2020) = 0) or (ISNUMERIC(Value_2021) = 0) or (ISNUMERIC(Value_2022) = 0) or (ISNUMERIC(Value_2023) = 0) or (ISNUMERIC(Value_2024) = 0) or (ISNUMERIC(Value_2025) = 0) or (ISNUMERIC(Value_2026) = 0) or (ISNUMERIC(Value_2027) = 0) or (ISNUMERIC(Value_2028) = 0) or (ISNUMERIC(Value_2029) = 0) 
	Then 'please enter the only digits for Value_2019,Value_2020,Value_2021,Value_2022,Value_2023,Value_2024,Value_2025,Value_2026,Value_2027,Value_2028, Value_2029'
	ELSE Null
	END as VeryLongTermValueNumberError,
CASE 
    WHEN (st.Rationale_2019 is null) or (st.Rationale_2020 is null) or (st.Rationale_2021 is null) or (st.Rationale_2022 is null) or (st.Rationale_2023 is null) or (st.Rationale_2024 is null)or (st.Rationale_2025 is null)or (st.Rationale_2026 is null)or (st.Rationale_2027 is null)or (st.Rationale_2028 is null) or (st.Rationale_2029 is null) Then 'please enter the Rationale_2019, Rationale_2020, Rationale_2021, Rationale_2022, Rationale_2023, Rationale_2024, Rationale_2025, Rationale_2026, Rationale_2027, Rationale_2028, Rationale_2029'
	ELSE Null
	END as VeryLongTermRationaleError
FROM StaginTrend st
Inner Join cteGetTimeTag cgtt
On cgtt.StaggingId = st.Id
and UPPER(cgtt.TimeTagName) = UPPER('VeryLongTerm')
),
ctePerennial as (
SELECT st.Id,
       cgtt.TimeTagName,
CASE 
	WHEN (st.Value_2019 is null) or (st.Value_2020 is null) or (st.Value_2021 is null) or (st.Value_2022 is null) or (st.Value_2023 is null)or (st.Value_2024 is null)or (st.Value_2025 is null)or (st.Value_2026 is null)or (st.Value_2027 is null) or (st.Value_2028 is null) or (st.Value_2029 is null)or (st.Value_2030 is null)or (st.Value_2031 is null) or (st.Value_2032 is null) Then 'please enter the value for Value_2019,Value_2020,Value_2021,Value_2022,Value_2023,Value_2024,Value_2025,Value_2026,Value_2027,Value_2028 ,Value_2029,Value_2030,Value_2031, Value_2032'
	ELSE Null
	END as PerennialValueError,
CASE 
    WHEN (ISNUMERIC(Value_2019) = 0) or (ISNUMERIC(Value_2020) = 0) or (ISNUMERIC(Value_2021) = 0) or (ISNUMERIC(Value_2022) = 0) or (ISNUMERIC(Value_2023) = 0) or (ISNUMERIC(Value_2024) = 0) or (ISNUMERIC(Value_2025) = 0) or (ISNUMERIC(Value_2026) = 0) or (ISNUMERIC(Value_2027) = 0) or (ISNUMERIC(Value_2028) = 0) or (ISNUMERIC(Value_2029) = 0)or (ISNUMERIC(Value_2030) = 0)or (ISNUMERIC(Value_2031) = 0) or(ISNUMERIC(Value_2032) = 0) 
	Then 'please enter the only digits for Value_2019,Value_2020,Value_2021,Value_2022,Value_2023,Value_2024,Value_2025,Value_2026,Value_2027,Value_2028, Value_2029, Value_2030, Value_2031,Value_2032'
	ELSE Null
	END as PerennialValueNumberError,
CASE 
    WHEN (st.Rationale_2019 is null) or (st.Rationale_2020 is null) or (st.Rationale_2021 is null) or (st.Rationale_2022 is null) or (st.Rationale_2023 is null) or (st.Rationale_2024 is null)or (st.Rationale_2025 is null)or (st.Rationale_2026 is null)or (st.Rationale_2027 is null)or (st.Rationale_2028 is null) or (st.Rationale_2029 is null) or (st.Rationale_2030 is null)or (st.Rationale_2031 is null) or(st.Rationale_2032 is null) Then 'please enter the Rationale_2019, Rationale_2020, Rationale_2021, Rationale_2022, Rationale_2023, Rationale_2024, Rationale_2025, Rationale_2026, Rationale_2027, Rationale_2028, Rationale_2029, Rationale_2030, Rationale_2031,Rationale_2032'
	ELSE Null
	END as PerennialRationaleError
FROM StaginTrend st
Inner Join cteGetTimeTag cgtt
On cgtt.StaggingId = st.Id
and UPPER(cgtt.TimeTagName) = UPPER('Perennial')
),
cteErrorNotesValues as (
Select st.Id,
st.TimeTag,
CI.ImmediateValueError,CI.ImmediateValueNumberError,CI.ImmediateRationaleError,
CST.ShortTermValueError,CST.ShortTermValueNumberError,CST.ShortTermRationaleError,
CMT.MidTermValueError,CMT.MidTermValueNumberError,CMT.MidTermRationaleError,
CLT.LongTermValueError, CLT.LongTermValueNumberError,CLT.LongTermRationaleError,
CVLT.VeryLongTermValueError,CVLT.VeryLongTermValueNumberError,CVLT.VeryLongTermRationaleError,
CP.PerennialValueError,CP.PerennialValueNumberError ,CP.PerennialRationaleError
from StaginTrend ST
Left Join cteImmediate CI
ON st.Id = CI.Id
Left join cteShortTerm CST
On st.Id = CST.Id
Left Join cteMidTerm CMT
On st.Id = CMT.Id
Left Join cteLongTerm CLT
On st.Id = CLT.Id
Left Join cteVeryLongTerm CVLT
On st.Id = CVLT.Id
Left Join ctePerennial CP
On st.Id = CP.Id
),
cteAllErrors as (
Select Id as StaggingId,
	   ErrorNotesAuthorRemark,
	   ErrorNotesCompanyGroup,
	   ErrorNotesEcoSystem,
	   ErrorNotesEcoSystemSubSegemnt,
	   ErrorNotesIndustry,
	   ErrorNotesKeyCompany,
	   ErrorNotesKeyword,
	   ErrorNotesMarket,
	   ErrorNotesProject,
	   ErrorNotesProject1,
	   ErrorNotesRegion,
	   ErrorNotesCountry,
	   ErrorNotesTimeTag,
	   ErrorNotesTrend,
	   ErrorNotesValueConversion 
From StaginTrend
)
Update StaginTrend
SET ErrorNotes = ISNULL(cae.ErrorNotesAuthorRemark,'') + ',' + 
				 ISNULL(cae.ErrorNotesCompanyGroup,'') + ',' + 
				 ISNULL(cae.ErrorNotesEcoSystem,'') + ',' + 
				 isnull(cae.ErrorNotesEcoSystemSubSegemnt,'')+','+
				 ISNULL(cae.ErrorNotesIndustry,'') + ',' + 
				 ISNULL(cae.ErrorNotesKeyCompany,'') + ',' + 
				 ISNULL(cae.ErrorNotesKeyword,'') + ',' + 
				 ISNULL(cae.ErrorNotesMarket,'') + ',' + 
				 ISNULL(cae.ErrorNotesProject,'') + ',' + 
				 ISNULL(cae.ErrorNotesProject1,'') + ',' + 
				 ISNULL(cae.ErrorNotesRegion,'') + ',' + 
				 ISNULL(cae.ErrorNotesCountry,'') + ',' + 
				 ISNULL(cae.ErrorNotesTimeTag,'') + ',' + 
				 ISNULL(cae.ErrorNotesTrend,'') + ',' + 
				 ISNULL(cae.ErrorNotesValueConversion,'') + ',' +
				 ISNULL(cenv.ImmediateValueError,'') + ' ' + ISNULL(cenv.ImmediateValueNumberError,'') + ' ' + ISNULL(cenv.ShortTermValueError,'') + ' ' + ISNULL(cenv.ShortTermValueNumberError,'') + ' ' + ISNULL(cenv.MidTermValueError,'') + ' ' + ISNULL(cenv.MidTermValueNumberError,'') + ' ' + ISNULL(cenv.LongTermValueError,'') + ' ' + ISNULL(cenv.LongTermValueError,'') + ' ' + ISNULL(cenv.VeryLongTermValueError,'') + ' ' + ISNULL(cenv.VeryLongTermValueNumberError,'') + ' ' + ISNULL(cenv.PerennialValueError,'') + ' ' + ISNULL(cenv.PerennialValueNumberError,'') + ' ' + ISNULL(cenv.ImmediateRationaleError,'') + ' ' + ISNULL(cenv.ShortTermRationaleError,'') + ' ' + ISNULL(cenv.MidTermRationaleError,'') + ' ' + ISNULL(cenv.LongTermRationaleError,'') + ' ' + ISNULL(cenv.VeryLongTermRationaleError,'') + ' ' + ISNULL(cenv.PerennialRationaleError,'')
From cteAllErrors cae
Inner Join StaginTrend st
ON st.Id = cae.StaggingId
Inner Join cteErrorNotesValues cenv
On st.Id = cenv.Id
             
Declare @Notes int;
If((select COUNT(ErrorNotes) from StaginTrend where ErrorNotes = ',,,,,,,,,,,,,,,') = (select COUNT(ErrorNotes) from StaginTrend))
Begin
set @Notes = 0
Update StaginTrend Set ErrorNotes = null
Where ErrorNotes = ',,,,,,,,,,,,,,,,'
End
Else
Begin 
set @Notes = (
Select COUNT(*) 
from StaginTrend 
where ErrorNotes != '' and ErrorNotes is not null 
) 
Update StaginTrend Set ErrorNotes = NULL
Where ErrorNotes = ',,,,,,,,,,,,,,,' 
End

IF(@Notes > 0)  
BEGIN  
   Select RowNo,ErrorNotes, Message = @ErrorMsg, Success = 1, StoredProcedureName ='SApp_UpdateStagingTrend' from StaginTrend WHERE ErrorNotes != '' ORDER BY RowNo; 
   Delete from ImportData where Id in (select Distinct ImportId from StaginTrend sm); 
   DELETE from StaginTrend ;   
END 
Else
Begin
	SELECT StoredProcedureName ='SApp_UpdateStagingTrend',Message =@ErrorMsg,Success = 1;
    Exec [dbo].[SApp_insertAllTrendDetailsFromStagging];
	Delete from ImportData where Id in (select Distinct ImportId from StaginTrend sm); 
    DELETE from StaginTrend;
 
End   
END TRY            
BEGIN CATCH            
    -- Execute the error retrieval routine.            
 DECLARE @ErrorNumber int;            
 DECLARE @ErrorSeverity int;            
 DECLARE @ErrorProcedure varchar(100);            
 DECLARE @ErrorLine int;    
 DECLARE @ErrorMessage varchar(500);            
            
  SELECT @ErrorNumber = ERROR_NUMBER(),            
        @ErrorSeverity = ERROR_SEVERITY(),            
        @ErrorProcedure = ERROR_PROCEDURE(),            
        @ErrorLine = ERROR_LINE(),            
        @ErrorMessage = ERROR_MESSAGE()            
            
 insert into dbo.Errorlog(ErrorNumber,            
       ErrorSeverity,            
       ErrorProcedure,            
       ErrorLine,            
       ErrorMessage,            
       ErrorDate)            
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());            
          
 set @ErrorMsg = 'Error while updating StagingTrend, please contact Admin for more details.';          
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine          
,Message =@ErrorMsg,Success = 0;             
END CATCH                  
end