﻿
 CREATE PROCEDURE [dbo].[SFab_GetCompanyProduct]      
 ( 
 @CompanyID UniqueIdentifier =null,  
 @ProjectID uniqueidentifier, 
 @AviraUserID uniqueidentifier = null,
 @NoOfRow INT = -1       
 )      
 AS      
/*================================================================================      
Procedure Name: SFab_GetComapnyProduct      
Author: Praveen      
Create date: 07/10/2019      
Description: Get Details from ComapnyProduct table by ID      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
07/10/2019 Praveen   Initial Version  
================================================================================*/      
      
BEGIN      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

declare @USDCurrencyId uniqueidentifier;
declare @USDCurrencyName nvarchar(256) = 'United States dollar';

select @USDCurrencyId = Id from Currency
where CurrencyName = @USDCurrencyName;


;with CTE_CPVConv as (select ROW_NUMBER() over (partition by CompanyProductId order by ValueConversion.Conversion desc) as rnum,
CompanyProductId,
ValueConversionId,
ValueConversion.ValueName
from [dbo].CompanyProductValue 
LEFT JOIN [dbo].[ValueConversion] ValueConversion
ON [CompanyProductValue].ValueConversionId= [ValueConversion].Id
where exists(select 1 from [CompanyProduct]
			WHERE [CompanyProduct].CompanyId = @CompanyID 
			--AND [CompanyProduct].ProjectId = @ProjectID
			and [CompanyProduct].id = CompanyProductValue.CompanyProductId
			and [CompanyProduct].IsDeleted = 0)

),
CTE_CPVAmt as (select CompanyProductValue.CompanyProductId,
--sum(dbo.fun_CurrencyConversion(Amount, CurrencyId, @USDCurrencyId, '01/01/'+cast([Year] as varchar))) as Amount
sum(dbo.fun_UnitDeConversion(ValueConversionId,Amount)) as Amount
from [dbo].CompanyProductValue
where exists(select 1 from [CompanyProduct]
			WHERE [CompanyProduct].CompanyId = @CompanyID 
			--AND [CompanyProduct].ProjectId = @ProjectID
			and [CompanyProduct].id = CompanyProductValue.CompanyProductId
			and [CompanyProduct].IsDeleted = 0)
and [Year] > '1900'
group by CompanyProductValue.CompanyProductId
)

SELECT * FROM (
		 SELECT 
		 ROW_NUMBER() OVER (ORDER BY CASE WHEN  [CompanyProduct].ProjectId = @ProjectID THEN 0 ELSE 1 END,[Project].[ProjectName], [CompanyProduct].[ProductName] ASC) AS rownumber,
		 [CompanyProduct].[Id],
		  [Project].[ProjectName], 
		  [CompanyProduct].CompanyId,
		  [CompanyProduct].ProjectId,     
		  [CompanyProduct].[ProductName],      
		  [CompanyProduct].[LaunchDate] ,  
		  [CompanyProduct].[Description], 
		  [CompanyProduct].[Patents],  
		  [CompanyProduct].[StatusId] AS ProductStatusId,  
		  [ProductStatus].[ProductStatusName],
		  [CompanyProduct].[ImageURL],
		  [CompanyProduct].[ImageDisplayName], 
		  [CompanyProduct].[ActualImageName] As ImageActualName, 
		  --[Currency].[CurrencyName],
		  @USDCurrencyName as [CurrencyName],
		  --[CompanyProductValue].[CurrencyId] As CurrencyId,
		  @USDCurrencyId AS CurrencyId,
		  CTE_CPVConv.[ValueConversionId] As ValueConversionId,
		  CTE_CPVConv.[ValueName],
		  CTE_CPVAmt.[Amount],
		  --[CompanyProductValue].[Amount],
		  --dbo.fun_UnitDeConversion(CTE_CPVConv.[ValueConversionId],CTE_CPVAmt.[Amount]) as [Amount],
		  [CompanyProduct].[IsDeleted],

		  (select STRING_AGG(ProductCategory.ProductCategoryName, ', ') from ProductCategory
		  where exists (select 1 from CompanyProductCategoryMap CPCM 
						where CPCM.CompanyProductId = [CompanyProduct].Id
						and CPCM.ProductCategoryId = ProductCategory.Id)) AS ProductCategories,

		  --STUFF((SELECT ', '+ ProductCategory.ProductCategoryName  FROM CompanyProductCategoryMap
			 -- INNER JOIN ProductCategory ON ProductCategory.Id=CompanyProductCategoryMap.ProductCategoryId
		  --    WHERE CompanyProductCategoryMap.CompanyProductId=[CompanyProduct].Id
			 -- FOR XML PATH('')), 1, 1, '') AS ProductCategories,

			(select STRING_AGG(Segment.SegmentName, ', ') from Segment
			where exists (select 1 from CompanyProductSegmentMap CPSM 
						where CPSM.CompanyProductId = [CompanyProduct].Id
						and CPSM.SegmentId = Segment.Id)) AS ProductSegments,

		  --STUFF((SELECT ', '+ Segment.SegmentName  FROM CompanyProductSegmentMap
			 -- INNER JOIN Segment ON Segment.Id=CompanyProductSegmentMap.SegmentId
		  --    WHERE CompanyProductSegmentMap.CompanyProductId=[CompanyProduct].Id
			 -- FOR XML PATH('')), 1, 1, '') AS ProductSegments,
			 (select STRING_AGG(Country.CountryName, ', ') from Country
			where exists (select 1 from CompanyProductCountryMap CPCM 
						where CPCM.CompanyProductId = [CompanyProduct].Id
						and CPCM.CountryId = Country.Id)) AS ProductCountries

			 --   STUFF((SELECT ', '+ Country.CountryName  FROM CompanyProductCountryMap
			 -- INNER JOIN Country ON Country.Id=CompanyProductCountryMap.CountryId
		  --    WHERE CompanyProductCountryMap.CompanyProductId=[CompanyProduct].Id
			 -- FOR XML PATH('')), 1, 1, '') AS ProductCountries


		FROM [dbo].[CompanyProduct]  
		INNER JOIN [dbo].[ProductStatus] ProductStatus      
		ON [CompanyProduct].StatusId = [ProductStatus].Id
		LEFT JOIN [dbo].[Project] Project
		ON [CompanyProduct].[ProjectId]=Project.ID
		LEFT JOIN CTE_CPVConv
		on CTE_CPVConv.CompanyProductId=[CompanyProduct].Id
		and CTE_CPVConv.rnum = 1
		left join CTE_CPVAmt
		on CTE_CPVAmt.CompanyProductId=[CompanyProduct].Id
		WHERE [CompanyProduct].CompanyId = @CompanyID 
		--AND [CompanyProduct].ProjectId = @ProjectID
		and [CompanyProduct].IsDeleted = 0
		) AS product
		WHERE rownumber <= CASE WHEN @NoOfRow < 1 THEN rownumber ELSE @NoOfRow END ORDER BY rownumber
		
END