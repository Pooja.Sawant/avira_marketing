﻿CREATE TABLE [dbo].[Location] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [LocationName]     NVARCHAR (256)   CONSTRAINT [DF_Location_LocationName] DEFAULT ('') NOT NULL,
    [Description]      NVARCHAR (256)   NULL,
    [Address1]         NVARCHAR (256)   NOT NULL,
    [Address2]         NVARCHAR (256)   NULL,
    [City]             NVARCHAR (256)   NOT NULL,
    [State]            NVARCHAR (256)   NOT NULL,
    [Zip]              NVARCHAR (256)   NOT NULL,
    [CountryId]        UNIQUEIDENTIFIER NOT NULL,
    [CreatedOn]        DATETIME2 (7)    NULL,
    [UserCreatedById]  UNIQUEIDENTIFIER NULL,
    [ModifiedOn]       DATETIME2 (7)    NULL,
    [UserModifiedById] UNIQUEIDENTIFIER NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_Location_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]        DATETIME2 (7)    NULL,
    [UserDeletedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Location_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_Location_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_Location_Country] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Country] ([Id])
);




GO

GO

GO

GO

GO

GO

GO

GO

GO