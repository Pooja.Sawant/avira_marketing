﻿              
CREATE PROCEDURE [dbo].[SStagin_InsertImportMarketSizing]            
@TemplateName NVARCHAR(200)='',            
@ImportFileName NVARCHAR(200)='',            
@jsonTrend NVARCHAR(MAX),   
@ImportId UNIQUEIDENTIFIER,         
@UserCreatedById UNIQUEIDENTIFIER
AS            
/*================================================================================                
Procedure Name: SStagin_InsertImportMarketSizing               
Change History                
Date  Developer  Reason                
__________ ____________ ______________________________________________________                
05/25/2019 Gopi   Initial Version                
================================================================================*/             
--DECLARE @ImportId UNIQUEIDENTIFIER =NEWID()            
declare @ErrorMsg NVARCHAR(2048),@CreatedOn Datetime            
BEGIN            
 SET @CreatedOn = GETDATE()            
 BEGIN TRY               
-- INSERT INTO ImportData            
-- (Id               ,             
--TemplateName    ,            
--ImportFileName    ,            
----ImportStatusId    ,            
----ImportException    ,            
--ImportedOn     ,            
--UserImportedById)            
--VALUES(@ImportId, @TemplateName, @ImportFileName,@CreatedOn, @UserCreatedById)            
---------------------------------------------------            
INSERT INTO StagingMarketSizing(Id,              
	ImportId,
	RowNo,
	Project,     
	Market,      
Region,      
Country,   
CurrencyName,    
ValueName   ,    
ASPUnit     ,
VolumeUnit   , 
         
ASP_2018      ,            
Value_2018       ,            
Volume_2018       ,            
OranicGrowth_2018      ,            
TrendsGrowth_2018     ,   
Source1_2018  ,
Source2_2018  ,
Source3_2018  ,
Source4_2018  ,
Source5_2018  ,
Rationale_2018  ,
ASP_2019     ,            
Value_2019        ,            
Volume_2019        ,            
OranicGrowth_2019      ,            
TrendsGrowth_2019      ,      
Source1_2019  ,
Source2_2019  ,
Source3_2019  ,
Source4_2019  ,
Source5_2019  ,
Rationale_2019  ,
ASP_2020       ,            
Value_2020      ,            
Volume_2020     ,            
OranicGrowth_2020      ,            
TrendsGrowth_2020       ,  
Source1_2020  ,
Source2_2020  ,
Source3_2020  ,
Source4_2020  ,
Source5_2020  ,
Rationale_2020  ,
ASP_2021      ,            
Value_2021      ,            
Volume_2021        ,            
OranicGrowth_2021        ,            
TrendsGrowth_2021      ,   
Source1_2021 ,
Source2_2021  ,
Source3_2021  ,
Source4_2021  ,
Source5_2021  ,
Rationale_2021  ,
ASP_2022      ,            
Value_2022      ,            
Volume_2022      ,            
OranicGrowth_2022       ,            
TrendsGrowth_2022       ,   
Source1_2022  ,
Source2_2022  ,
Source3_2022  ,
Source4_2022  ,
Source5_2022  ,
Rationale_2022  ,
ASP_2023      ,            
Value_2023      ,            
Volume_2023      ,            
OranicGrowth_2023       ,            
TrendsGrowth_2023       ,    
Source1_2023   ,
Source2_2023  ,
Source3_2023  ,
Source4_2023  ,
Source5_2023  ,
Rationale_2023  ,
ASP_2024      ,            
Value_2024      ,            
Volume_2024      ,            
OranicGrowth_2024       ,            
TrendsGrowth_2024       ,  
Source1_2024  ,
Source2_2024  ,
Source3_2024  ,
Source4_2024  ,
Source5_2024  ,
Rationale_2024  ,
ASP_2025      ,            
Value_2025      ,            
Volume_2025      ,            
OranicGrowth_2025       ,            
TrendsGrowth_2025       ,  
Source1_2025  ,
Source2_2025  ,
Source3_2025  ,
Source4_2025  ,
Source5_2025  ,
Rationale_2025  ,
ASP_2026      ,            
Value_2026      ,            
Volume_2026      ,            
OranicGrowth_2026       ,            
TrendsGrowth_2026       ,  
Source1_2026  ,
Source2_2026  ,
Source3_2026  ,
Source4_2026  ,
Source5_2026  ,
Rationale_2026  ,
ASP_2027      ,            
Value_2027      ,            
Volume_2027      ,            
OranicGrowth_2027       ,            
TrendsGrowth_2027       ,   
Source1_2027  ,
Source2_2027  ,
Source3_2027  ,
Source4_2027  ,
Source5_2027  ,
Rationale_2027  ,
ASP_2028      ,            
Value_2028      ,            
Volume_2028      ,            
OranicGrowth_2028       ,            
TrendsGrowth_2028  ,  
Source1_2028  ,
Source2_2028  ,
Source3_2028  ,
Source4_2028  ,
Source5_2028  ,
Rationale_2028  ,
CAGR_3Years  ,
CAGR_5Years  ,
TotalCAGR  ,
UserCreatedById ,
SegmentMapJson
)            
SELECT            
       NEWID(),	   
	   @ImportId,
	   RowNo,
Project         ,      
Market          ,           
Region        ,       
Country       ,  
Currency  ,      
Value    ,  
ASPUnit  ,
VolumeUnit   , 
ASP_2018      ,            
Value_2018       ,            
Volume_2018       ,            
OranicGrowth_2018      ,            
TrendsGrowth_2018     ,
Source1_2018  ,
Source2_2018  ,
Source3_2018  ,
Source4_2018  ,
Source5_2018  ,
Rationale_2018  ,            
ASP_2019     ,            
Value_2019        ,            
Volume_2019        ,            
OranicGrowth_2019      ,            
TrendsGrowth_2019      ,   
Source1_2019  ,
Source2_2019  ,
Source3_2019  ,
Source4_2019  ,
Source5_2019  ,
Rationale_2019  ,
ASP_2020       ,            
Value_2020      ,            
Volume_2020     ,            
OranicGrowth_2020      ,            
TrendsGrowth_2020       ,     
Source1_2020  ,
Source2_2020  ,
Source3_2020  ,
Source4_2020  ,
Source5_2020  ,
Rationale_2020  ,       
ASP_2021      ,            
Value_2021      ,            
Volume_2021        ,            
OranicGrowth_2021        ,            
TrendsGrowth_2021      ,   
Source1_2021 ,
Source2_2021  ,
Source3_2021  ,
Source4_2021  ,
Source5_2021  ,
Rationale_2021  ,         
ASP_2022      ,            
Value_2022      ,            
Volume_2022      ,            
OranicGrowth_2022       ,            
TrendsGrowth_2022       ,   
Source1_2022  ,
Source2_2022  ,
Source3_2022  ,
Source4_2022  ,
Source5_2022  ,
Rationale_2022  , 
ASP_2023      ,            
Value_2023      ,            
Volume_2023      ,            
OranicGrowth_2023       ,            
TrendsGrowth_2023       ,   
Source1_2023   ,
Source2_2023  ,
Source3_2023  ,
Source4_2023  ,
Source5_2023  ,
Rationale_2023  , 
ASP_2024      ,            
Value_2024      ,            
Volume_2024      ,            
OranicGrowth_2024       ,            
TrendsGrowth_2024       ,   
Source1_2024  ,
Source2_2024  ,
Source3_2024  ,
Source4_2024  ,
Source5_2024  ,
Rationale_2024  , 
ASP_2025      ,            
Value_2025      ,            
Volume_2025      ,            
OranicGrowth_2025       ,            
TrendsGrowth_2025       ,  
Source1_2025  ,
Source2_2025  ,
Source3_2025  ,
Source4_2025  ,
Source5_2025  ,
Rationale_2025  ,  
ASP_2026      ,            
Value_2026      ,            
Volume_2026      ,            
OranicGrowth_2026       ,            
TrendsGrowth_2026       ,   
Source1_2026  ,
Source2_2026  ,
Source3_2026  ,
Source4_2026  ,
Source5_2026  ,
Rationale_2026  , 
ASP_2027      ,            
Value_2027      ,            
Volume_2027      ,            
OranicGrowth_2027       ,            
TrendsGrowth_2027       ,    
Source1_2027  ,
Source2_2027  ,
Source3_2027  ,
Source4_2027  ,
Source5_2027  ,
Rationale_2027  ,
ASP_2028      ,            
Value_2028      ,            
Volume_2028      ,            
OranicGrowth_2028       ,            
TrendsGrowth_2028  ,  
Source1_2028  ,
Source2_2028  ,
Source3_2028  ,
Source4_2028  ,
Source5_2028  ,
Rationale_2028  ,
CAGR_3Years  ,
CAGR_5Years  ,
Total_CAGR  ,
@UserCreatedById  ,
SegmentMapJson
    FROM OPENJSON(@jsonTrend)            
    WITH (   
	[RowNo] Int,         
	[Project] [nvarchar](255) ,            
	[Market] [nvarchar](255) ,                  
 [Region] [nvarchar](255) ,       
 [Country] [nvarchar](255) , 
 [Currency] [nvarchar](255) ,    
 [Value] [nvarchar](255) ,  
 [ASPUnit] [nvarchar](255) ,
 [VolumeUnit] [nvarchar](255) , 
 [ASP_2018] [nvarchar](255) ,            
 [Value_2018] [nvarchar](255) ,            
 [Volume_2018] [nvarchar](255) ,            
 [OranicGrowth_2018] [nvarchar](255) ,            
 [TrendsGrowth_2018] [nvarchar](255) ,  
 [Source1_2018] [nvarchar](255) , 
 [Source2_2018] [nvarchar](255) , 
 [Source3_2018] [nvarchar](255) , 
 [Source4_2018] [nvarchar](255) , 
 [Source5_2018] [nvarchar](255) , 
 [Rationale_2018] [nvarchar](255) ,          
 [ASP_2019] [nvarchar](255) ,            
 [Value_2019] [nvarchar](255) ,            
 [Volume_2019] [nvarchar](255) ,            
 [OranicGrowth_2019] [nvarchar](255) ,            
 [TrendsGrowth_2019] [nvarchar](255) ,
 [Source1_2019] [nvarchar](255) ,
 [Source2_2019] [nvarchar](255) ,
 [Source3_2019] [nvarchar](255) ,
 [Source4_2019] [nvarchar](255) ,
 [Source5_2019] [nvarchar](255) ,
 [Rationale_2019] [nvarchar](255) ,        
 [ASP_2020] [nvarchar](255) ,            
 [Value_2020] [nvarchar](255) ,            
 [Volume_2020] [nvarchar](255) ,            
 [OranicGrowth_2020] [nvarchar](255) ,            
 [TrendsGrowth_2020] [nvarchar](255) ,    
 [Source1_2020] [nvarchar](255) ,   
 [Source2_2020] [nvarchar](255) ,   
 [Source3_2020] [nvarchar](255) ,   
 [Source4_2020] [nvarchar](255) ,   
 [Source5_2020] [nvarchar](255) ,   
 [Rationale_2020] [nvarchar](255) ,          
 [ASP_2021] [nvarchar](255) ,            
 [Value_2021] [nvarchar](255) ,            
 [Volume_2021] [nvarchar](255) ,            
 [OranicGrowth_2021] [nvarchar](255) ,            
 [TrendsGrowth_2021] [nvarchar](255) ,  
 [Source1_2021] [nvarchar](255) , 
 [Source2_2021] [nvarchar](255) , 
 [Source3_2021] [nvarchar](255) , 
 [Source4_2021] [nvarchar](255) , 
 [Source5_2021] [nvarchar](255) , 
 [Rationale_2021] [nvarchar](255) ,          
 [ASP_2022] [nvarchar](255) ,            
 [Value_2022] [nvarchar](255) ,            
 [Volume_2022] [nvarchar](255) ,            
 [OranicGrowth_2022] [nvarchar](255) ,            
 [TrendsGrowth_2022] [nvarchar](255) ,  
 [Source1_2022] [nvarchar](255) ,
 [Source2_2022] [nvarchar](255) ,
 [Source3_2022] [nvarchar](255) ,
 [Source4_2022] [nvarchar](255) ,
 [Source5_2022] [nvarchar](255) ,
 [Rationale_2022] [nvarchar](255) ,
 [ASP_2023] [nvarchar](255) ,            
 [Value_2023] [nvarchar](255) ,            
 [Volume_2023] [nvarchar](255) ,            
 [OranicGrowth_2023] [nvarchar](255) ,            
 [TrendsGrowth_2023] [nvarchar](255) ,  
 [Source1_2023] [nvarchar](255) ,
 [Source2_2023] [nvarchar](255) ,
 [Source3_2023] [nvarchar](255) ,
 [Source4_2023] [nvarchar](255) ,
 [Source5_2023] [nvarchar](255) ,
 [Rationale_2023] [nvarchar](255) ,
 [ASP_2024] [nvarchar](255) ,            
 [Value_2024] [nvarchar](255) ,            
 [Volume_2024] [nvarchar](255) ,            
 [OranicGrowth_2024] [nvarchar](255) ,            
 [TrendsGrowth_2024] [nvarchar](255) ,
 [Source1_2024] [nvarchar](255) ,
 [Source2_2024] [nvarchar](255) ,
 [Source3_2024] [nvarchar](255) ,
 [Source4_2024] [nvarchar](255) ,
 [Source5_2024] [nvarchar](255) ,
 [Rationale_2024] [nvarchar](255) ,   
 [ASP_2025] [nvarchar](255) ,            
 [Value_2025] [nvarchar](255) ,            
 [Volume_2025] [nvarchar](255) ,            
 [OranicGrowth_2025] [nvarchar](255) ,            
 [TrendsGrowth_2025] [nvarchar](255) , 
 [Source1_2025] [nvarchar](255) , 
 [Source2_2025] [nvarchar](255) , 
 [Source3_2025] [nvarchar](255) , 
 [Source4_2025] [nvarchar](255) , 
 [Source5_2025] [nvarchar](255) , 
 [Rationale_2025] [nvarchar](255) ,   
 [ASP_2026] [nvarchar](255) ,            
 [Value_2026] [nvarchar](255) ,            
 [Volume_2026] [nvarchar](255) ,            
 [OranicGrowth_2026] [nvarchar](255) ,            
 [TrendsGrowth_2026] [nvarchar](255) ,   
 [Source1_2026] [nvarchar](255) , 
 [Source2_2026] [nvarchar](255) , 
 [Source3_2026] [nvarchar](255) , 
 [Source4_2026] [nvarchar](255) , 
 [Source5_2026] [nvarchar](255) , 
 [Rationale_2026] [nvarchar](255) ,  
 [ASP_2027] [nvarchar](255) ,            
 [Value_2027] [nvarchar](255) ,            
 [Volume_2027] [nvarchar](255) ,            
 [OranicGrowth_2027] [nvarchar](255) ,            
 [TrendsGrowth_2027] [nvarchar](255) ,   
 [Source1_2027] [nvarchar](255) , 
 [Source2_2027] [nvarchar](255) , 
 [Source3_2027] [nvarchar](255) , 
 [Source4_2027] [nvarchar](255) , 
 [Source5_2027] [nvarchar](255) , 
 [Rationale_2027] [nvarchar](255) ,  
 [ASP_2028] [nvarchar](255) ,            
 [Value_2028] [nvarchar](255) ,            
 [Volume_2028] [nvarchar](255) ,            
 [OranicGrowth_2028] [nvarchar](255) ,             
 [TrendsGrowth_2028] [nvarchar](255) , 
 [Source1_2028] [nvarchar](255) , 
 [Source2_2028] [nvarchar](255) , 
 [Source3_2028] [nvarchar](255) , 
 [Source4_2028] [nvarchar](255) , 
 [Source5_2028] [nvarchar](255) , 
 [Rationale_2028] [nvarchar](255) , 
 [CAGR_3Years] [nvarchar](255) ,
 [CAGR_5Years] [nvarchar](255) ,
 [Total_CAGR] [nvarchar](255) , 
 [UserCreatedById] [nvarchar](255),
 [SegmentMapJson]   [nvarchar](max)
    ) AS jsonTrends            

DELETE del FROM StagingMarketSizing del
JOIN StagingMarketSizing main ON main.Id = del.Id
WHERE 
  ( 
ISNULL(main.Project         ,'') = '' AND       
ISNULL(main.Market          ,'') = '' AND            
ISNULL(main.Region        ,'') = '' AND        
ISNULL(main.Country       ,'') = '' AND   
ISNULL(main.CurrencyName  ,'') = '' AND       
ISNULL(main.ValueName    ,'') = '' AND   
ISNULL(main.ASPUnit  ,'') = '' AND 
ISNULL(main.VolumeUnit   ,'') = '' AND 
ISNULL(main.ASP_2018      ,'') = '' AND             
ISNULL(main.Value_2018       ,'') = '' AND             
ISNULL(main.Volume_2018       ,'') = '' AND             
ISNULL(main.OranicGrowth_2018      ,'') = '' AND             
ISNULL(main.TrendsGrowth_2018     ,'') = '' AND 
ISNULL(main.Source1_2018  ,'') = '' AND 
ISNULL(main.Source2_2018  ,'') = '' AND 
ISNULL(main.Source3_2018  ,'') = '' AND 
ISNULL(main.Source4_2018  ,'') = '' AND 
ISNULL(main.Source5_2018  ,'') = '' AND 
ISNULL(main.Rationale_2018  ,'') = '' AND             
ISNULL(main.ASP_2019     ,'') = '' AND             
ISNULL(main.Value_2019        ,'') = '' AND             
ISNULL(main.Volume_2019        ,'') = '' AND             
ISNULL(main.OranicGrowth_2019      ,'') = '' AND             
ISNULL(main.TrendsGrowth_2019      ,'') = '' AND    
ISNULL(main.Source1_2019  ,'') = '' AND 
ISNULL(main.Source2_2019  ,'') = '' AND 
ISNULL(main.Source3_2019  ,'') = '' AND 
ISNULL(main.Source4_2019  ,'') = '' AND 
ISNULL(main.Source5_2019  ,'') = '' AND 
ISNULL(main.Rationale_2019  ,'') = '' AND 
ISNULL(main.ASP_2020       ,'') = '' AND             
ISNULL(main.Value_2020      ,'') = '' AND             
ISNULL(main.Volume_2020     ,'') = '' AND             
ISNULL(main.OranicGrowth_2020      ,'') = '' AND             
ISNULL(main.TrendsGrowth_2020       ,'') = '' AND      
ISNULL(main.Source1_2020  ,'') = '' AND 
ISNULL(main.Source2_2020  ,'') = '' AND 
ISNULL(main.Source3_2020  ,'') = '' AND 
ISNULL(main.Source4_2020  ,'') = '' AND 
ISNULL(main.Source5_2020  ,'') = '' AND 
ISNULL(main.Rationale_2020  ,'') = '' AND        
ISNULL(main.ASP_2021      ,'') = '' AND             
ISNULL(main.Value_2021      ,'') = '' AND             
ISNULL(main.Volume_2021        ,'') = '' AND             
ISNULL(main.OranicGrowth_2021        ,'') = '' AND             
ISNULL(main.TrendsGrowth_2021      ,'') = '' AND    
ISNULL(main.Source1_2021 ,'') = '' AND 
ISNULL(main.Source2_2021  ,'') = '' AND 
ISNULL(main.Source3_2021  ,'') = '' AND 
ISNULL(main.Source4_2021  ,'') = '' AND 
ISNULL(main.Source5_2021  ,'') = '' AND 
ISNULL(main.Rationale_2021  ,'') = '' AND          
ISNULL(main.ASP_2022      ,'') = '' AND             
ISNULL(main.Value_2022      ,'') = '' AND             
ISNULL(main.Volume_2022      ,'') = '' AND             
ISNULL(main.OranicGrowth_2022       ,'') = '' AND             
ISNULL(main.TrendsGrowth_2022       ,'') = '' AND    
ISNULL(main.Source1_2022  ,'') = '' AND 
ISNULL(main.Source2_2022  ,'') = '' AND 
ISNULL(main.Source3_2022  ,'') = '' AND 
ISNULL(main.Source4_2022  ,'') = '' AND 
ISNULL(main.Source5_2022  ,'') = '' AND 
ISNULL(main.Rationale_2022  ,'') = '' AND  
ISNULL(main.ASP_2023      ,'') = '' AND             
ISNULL(main.Value_2023      ,'') = '' AND             
ISNULL(main.Volume_2023      ,'') = '' AND             
ISNULL(main.OranicGrowth_2023       ,'') = '' AND             
ISNULL(main.TrendsGrowth_2023       ,'') = '' AND    
ISNULL(main.Source1_2023   ,'') = '' AND 
ISNULL(main.Source2_2023  ,'') = '' AND 
ISNULL(main.Source3_2023  ,'') = '' AND 
ISNULL(main.Source4_2023  ,'') = '' AND 
ISNULL(main.Source5_2023  ,'') = '' AND 
ISNULL(main.Rationale_2023  ,'') = '' AND  
ISNULL(main.ASP_2024      ,'') = '' AND             
ISNULL(main.Value_2024      ,'') = '' AND             
ISNULL(main.Volume_2024      ,'') = '' AND             
ISNULL(main.OranicGrowth_2024       ,'') = '' AND             
ISNULL(main.TrendsGrowth_2024       ,'') = '' AND    
ISNULL(main.Source1_2024  ,'') = '' AND 
ISNULL(main.Source2_2024  ,'') = '' AND 
ISNULL(main.Source3_2024  ,'') = '' AND 
ISNULL(main.Source4_2024  ,'') = '' AND 
ISNULL(main.Source5_2024  ,'') = '' AND 
ISNULL(main.Rationale_2024  ,'') = '' AND  
ISNULL(main.ASP_2025      ,'') = '' AND             
ISNULL(main.Value_2025      ,'') = '' AND             
ISNULL(main.Volume_2025      ,'') = '' AND             
ISNULL(main.OranicGrowth_2025       ,'') = '' AND             
ISNULL(main.TrendsGrowth_2025       ,'') = '' AND   
ISNULL(main.Source1_2025  ,'') = '' AND 
ISNULL(main.Source2_2025  ,'') = '' AND 
ISNULL(main.Source3_2025  ,'') = '' AND 
ISNULL(main.Source4_2025  ,'') = '' AND 
ISNULL(main.Source5_2025  ,'') = '' AND 
ISNULL(main.Rationale_2025  ,'') = '' AND   
ISNULL(main.ASP_2026      ,'') = '' AND             
ISNULL(main.Value_2026      ,'') = '' AND             
ISNULL(main.Volume_2026      ,'') = '' AND             
ISNULL(main.OranicGrowth_2026       ,'') = '' AND             
ISNULL(main.TrendsGrowth_2026       ,'') = '' AND    
ISNULL(main.Source1_2026  ,'') = '' AND 
ISNULL(main.Source2_2026  ,'') = '' AND 
ISNULL(main.Source3_2026  ,'') = '' AND 
ISNULL(main.Source4_2026  ,'') = '' AND 
ISNULL(main.Source5_2026  ,'') = '' AND 
ISNULL(main.Rationale_2026  ,'') = '' AND  
ISNULL(main.ASP_2027      ,'') = '' AND             
ISNULL(main.Value_2027      ,'') = '' AND             
ISNULL(main.Volume_2027      ,'') = '' AND             
ISNULL(main.OranicGrowth_2027       ,'') = '' AND             
ISNULL(main.TrendsGrowth_2027       ,'') = '' AND     
ISNULL(main.Source1_2027  ,'') = '' AND 
ISNULL(main.Source2_2027  ,'') = '' AND 
ISNULL(main.Source3_2027  ,'') = '' AND 
ISNULL(main.Source4_2027  ,'') = '' AND 
ISNULL(main.Source5_2027  ,'') = '' AND 
ISNULL(main.Rationale_2027  ,'') = '' AND 
ISNULL(main.ASP_2028      ,'') = '' AND             
ISNULL(main.Value_2028      ,'') = '' AND             
ISNULL(main.Volume_2028      ,'') = '' AND             
ISNULL(main.OranicGrowth_2028       ,'') = '' AND             
ISNULL(main.TrendsGrowth_2028  ,'') = '' AND   
ISNULL(main.Source1_2028  ,'') = '' AND 
ISNULL(main.Source2_2028  ,'') = '' AND 
ISNULL(main.Source3_2028  ,'') = '' AND 
ISNULL(main.Source4_2028  ,'') = '' AND 
ISNULL(main.Source5_2028  ,'') = '' AND 
ISNULL(main.Rationale_2028  ,'') = '' AND 
ISNULL(main.CAGR_3Years  ,'') = '' AND 
ISNULL(main.CAGR_5Years  ,'') = '' AND 
ISNULL(main.TotalCAGR  ,'') = '' AND
ISNULL(main.SegmentMapJson, '') = '' 
		 )
            
 SELECT StoredProcedureName ='SStagin_InsertImportMarketSizing',Message =@ErrorMsg,Success = 1;               
END TRY                
BEGIN CATCH                
    -- Execute the error retrieval routine.                
 DECLARE @ErrorNumber int;                
 DECLARE @ErrorSeverity int;                
 DECLARE @ErrorProcedure varchar(100);                
 DECLARE @ErrorLine int;                
 DECLARE @ErrorMessage varchar(500);                
                
  SELECT @ErrorNumber = ERROR_NUMBER(),                
        @ErrorSeverity = ERROR_SEVERITY(),                
        @ErrorProcedure = ERROR_PROCEDURE(),                
        @ErrorLine = ERROR_LINE(),                
        @ErrorMessage = ERROR_MESSAGE()                
                
 insert into dbo.Errorlog(ErrorNumber,                
       ErrorSeverity,                
       ErrorProcedure,                
       ErrorLine,                
       ErrorMessage,                
       ErrorDate)                
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,@CreatedOn);                
              
 set @ErrorMsg = 'Error while importing marketSizing, please contact Admin for more details.';                
--THROW 50000,@ErrorMsg,1;                  
--return(1)              
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine              
    ,Message =@ErrorMsg,Success = 0;                 
                 
END CATCH                 
            
            
END