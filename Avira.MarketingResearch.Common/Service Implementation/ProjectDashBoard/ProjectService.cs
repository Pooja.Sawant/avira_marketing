﻿using Avira.MarketingResearch.Common.IService.ProjectDashBoard;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Repository.CoreInterfaces.ProjectDashBoard;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.ProjectDashBoard
{
    public class ProjectService : IProjectService
    {
        private readonly IProjectRepository _projectRepository;
        private readonly IProjectAnalistDetailsRepository _projectAnalistDetailsRepository;
        private readonly IProjectSegmentMappingRepository _projectSegmentMappingRepository;


        public ProjectService(IProjectRepository buildLevelRepository, IProjectAnalistDetailsRepository projectAnalistDetailsRepositor, IProjectSegmentMappingRepository projectSegmentMappingRepository)
        {
            _projectRepository = buildLevelRepository;
            _projectAnalistDetailsRepository = projectAnalistDetailsRepositor;
            _projectSegmentMappingRepository = projectSegmentMappingRepository;
    }


        public Result<SpTransactionMessage> Create(MainProjectInsertDBViewModel tfc, AviraUser currentUser)
        {
            var projectResponse = _projectRepository.Create(tfc.projectInsertDbViewModel);
            if (projectResponse != null && projectResponse.IsSuccess)
            {

                var proAanalyDetlRespons = _projectAnalistDetailsRepository.Create(tfc.projectAnalistDetailsInsertTrendDbViewModel);
                 proAanalyDetlRespons = _projectAnalistDetailsRepository.Create(tfc.projectAnalistDetailsInsertCompnyDbViewModel);
                 proAanalyDetlRespons = _projectAnalistDetailsRepository.Create(tfc.projectAnalistDetailsInsertMarketDbViewModel);
                 proAanalyDetlRespons = _projectAnalistDetailsRepository.Create(tfc.projectAnalistDetailsInsertQuantDbViewModel);
                 proAanalyDetlRespons = _projectAnalistDetailsRepository.Create(tfc.projectAnalistDetailsInsertPrimarDbViewModel);
                proAanalyDetlRespons = _projectAnalistDetailsRepository.Create(tfc.projectAnalistDetailsInsertApproverDbViewModel);
                if(tfc.projectAnalistDetailsInsertCoApproDbViewModel.CoApproverUserID != null)
                {
                    proAanalyDetlRespons = _projectAnalistDetailsRepository.Create(tfc.projectAnalistDetailsInsertCoApproDbViewModel);
                }
             
                if (proAanalyDetlRespons ==null || !proAanalyDetlRespons.IsSuccess)
                {
                    projectResponse = proAanalyDetlRespons;
                }
                var proSegmentmapResponse = _projectSegmentMappingRepository.Create(tfc.projectSegmentMappingInsertInduDbViewModel);
                proSegmentmapResponse = _projectSegmentMappingRepository.Create(tfc.projectSegmentMappingInsertSubInduDbViewModel);
                proSegmentmapResponse = _projectSegmentMappingRepository.Create(tfc.projectSegmentMappingInsertMarketDbViewModel);
                proSegmentmapResponse = _projectSegmentMappingRepository.Create(tfc.projectSegmentMappingInsertSubMarkDbViewModel);

                if (proSegmentmapResponse == null || !proSegmentmapResponse.IsSuccess)
                {
                    projectResponse = proSegmentmapResponse;
                }
                else
                {
                    proSegmentmapResponse = _projectSegmentMappingRepository.CreateProjectCode(tfc.projectSegmentMappingInsertInduDbViewModel);
                    if (proSegmentmapResponse == null || !proSegmentmapResponse.IsSuccess)
                    {
                        projectResponse = proSegmentmapResponse;
                    }
                }
            }         
             return projectResponse;
        }


        public IEnumerable<ProjectViewModel> GetAll(Guid Id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ProjectViewModel> GetAll(Guid? Id, Guid AviraUserId, int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false)
        {
            IEnumerable<ProjectViewModel> model ;
            model= _projectRepository.GetAll(Id, AviraUserId,PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude);
            return model;
        }

        //public DisplayProjectDBViewModel GetById(Guid guid, Guid AviraUserId)
        //{
        //    return _projectRepository.GetById(guid, AviraUserId);
        //}


        public Result<SpTransactionMessage> Update(MainProjectUpdateDBViewModel tfc, AviraUser currenUser)
        {           
            var projectResponse = _projectAnalistDetailsRepository.Update(tfc.projectAnalistDetailsUpdateTrendDbViewModel);

            projectResponse = _projectAnalistDetailsRepository.Update(tfc.projectAnalistDetailsUpdateQuantDbViewModel);
            projectResponse = _projectAnalistDetailsRepository.Update(tfc.projectAnalistDetailsUpdatePrimiDbViewModel);
            projectResponse = _projectAnalistDetailsRepository.Update(tfc.projectAnalistDetailsUpdateMarketDbViewModel);
            projectResponse = _projectAnalistDetailsRepository.Update(tfc.projectAnalistDetailsUpdateCompanyDbViewModel);
            projectResponse = _projectAnalistDetailsRepository.Update(tfc.projectAnalistDetailsUpdateCoApproverDbViewModel);
            projectResponse = _projectAnalistDetailsRepository.Update(tfc.projectAnalistDetailsUpdateApproverDbViewModel);
            return projectResponse;
        }

        public IEnumerable<IndustryViewModel> GetIndustrySegmentList(string id, bool includeParentOnly = false )
        {
            return _projectRepository.GetIndustrySegmentList(id, includeParentOnly);
        }
        public IEnumerable<MarketViewModel> GetMarketSegmentList(string id, bool includeParentOnly = false)
        {
            return _projectRepository.GetMarketSegmentList(id, includeParentOnly);
        }

        public Result<SpTransactionMessage> UpdateProjectStatus(MainProjectUpdateDBViewModel tfc, AviraUser currenUser)
        {
            var projectResponse = _projectRepository.Update(tfc.projectUpdateDbViewModel);
            
            return projectResponse;
        }

        public IEnumerable<ProjectViewModel> GetAllProject()
        {
            return _projectRepository.GetAllProject();
        }
    }
}
