﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public class ImportRequestQueries
    {
        public static readonly string StoreProcInsert = "SApp_InsertImportRequest";

        public static readonly string StoreProcUpdate = "SApp_UpdateImportRequest";

        public static string StoreProcSelectAll = "SApp_GetImportRequestByUserId";

        public static readonly string StoreProcGetImportRequest = "SApp_GetImportRequest";

        public static string StoreProcSelectRecord = "SApp_GetImportRequestById";

    }
}
