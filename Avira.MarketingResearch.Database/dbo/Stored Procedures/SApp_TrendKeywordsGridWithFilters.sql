﻿
CREATE PROCEDURE [dbo].[SApp_TrendKeywordsGridWithFilters]
	(@TrendKeywordsIDList dbo.udtUniqueIdentifier READONLY,
	@TrendKeyWord nvarchar(256)='',
	@includeDeleted bit = 0,
	@OrdbyByColumnName NVARCHAR(50) ='CreatedDate',
	@SortDirection INT = -1,
	@PageStart INT = 0,
	@PageSize INT = null)
	AS
/*================================================================================
Procedure Name: SApp_TrendKeywordsGridWithFilters
Author: Gopi
Create date: 04/19/2019
Description: Get list from Keywords table by Name or ID
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
04/19/2019	Gopi			Initial Version
__________	____________	______________________________________________________
04/22/2019	Pooja			For Client side paging comment pagesize and null init
================================================================================*/

BEGIN
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
if @includeDeleted is null set @includeDeleted = 0;
if len(@TrendKeyWord)>2 set @TrendKeyWord = '%'+ @TrendKeyWord + '%'
else set @TrendKeyWord = ''
	if @PageSize is null or @PageSize =0
	set @PageSize = 10
	begin
	;WITH CTE_TBL AS (

	SELECT [TrendKeywords].[Id],
		[TrendKeywords].[TrendKeyWord],
		[TrendKeywords].[IsDeleted],
		CASE
		WHEN @OrdbyByColumnName = 'TrendKeyWord' THEN [TrendKeywords].TrendKeyWord
		ELSE [TrendKeywords].TrendKeyWord
	END AS SortColumn

FROM [dbo].[TrendKeywords]
where (NOT EXISTS (SELECT * FROM @TrendKeywordsIDList) or  [TrendKeywords].Id  in (select * from @TrendKeywordsIDList))
	AND (@TrendKeyWord = '' or [TrendKeywords].[TrendKeyWord] like @TrendKeyWord)
	AND (@includeDeleted = 1 or [TrendKeywords].IsDeleted = 0)
	)

	SELECT 
		  CTE_TBL.*,
		  tCountResult.TotalItems
		FROM CTE_TBL
		CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult
		ORDER BY 
		  CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,
		  CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC 
		OFFSET @PageStart ROWS
		FETCH NEXT @PageSize ROWS ONLY;
		END
	END