﻿using Avira.MarketingResearch.Common.IService.ProjectDashBoard;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Repository.CoreInterfaces.ProjectDashBoard;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.Service_Implementation.ProjectDashBoard
{
    public class ProjectAnalistDetailsService : IProjectAnalistDetailsService
    {
        private readonly IProjectAnalistDetailsRepository _projectAnalistDetailsRepository;
        public ProjectAnalistDetailsService(IProjectAnalistDetailsRepository buildLevelRepository)
        {
            _projectAnalistDetailsRepository = buildLevelRepository;
        }

        public Result<SpTransactionMessage> Create(ProjectAnalistDetailsInsertDbViewModel tfc, AviraUser currentUser)
        {
            return _projectAnalistDetailsRepository.Create(tfc);
        }


        public IEnumerable<ProjectAnalistDetailsViewModel> GetAll(Guid Id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ProjectAnalistDetailsViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false)
        {
            return _projectAnalistDetailsRepository.GetAll(PageStart, pageSize, SortDirection, OrdbyByColumnName, Search, isDeletedInclude);
        }

        public ProjectAnalistDetailsViewModel GetById(Guid guid)
        {
            return _projectAnalistDetailsRepository.GetById(guid);
        }


        public Result<SpTransactionMessage> Update(ProjectAnalistDetailsUpdateDbViewModel tfc, AviraUser currenUser)
        {
            return _projectAnalistDetailsRepository.Update(tfc);
        }
    }
}
