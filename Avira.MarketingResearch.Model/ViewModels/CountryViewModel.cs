﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Avira.MarketingResearch.Model.ViewModels
{
    public class CountryViewModel
    {
        public Guid TrendId { get; set; }
        public Guid RegionId { get; set; }
        public string RegionName { get; set; }
        public int RegionLevel { get; set; }
        public Guid CountryId { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        public Guid ParentRegionId { get; set; }
        public int IsAssigned { get; set; }
        public Guid? Impact { get; set; }
        public bool EndUser { get; set; }
        public Guid TrendRegionMapId { get; set; }

        public string RowType
        {
            get
            {
                return "Country";
            }
        }
    }
}
