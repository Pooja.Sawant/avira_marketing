﻿--sp_helptext SApp_CompanyRevenueGeographicSplitByCompanyID

 -- =============================================          
-- Author:  Laxmikant          
-- Create date: 05/29/2019          
-- Description: Get Revenue Company total revenue by Company Id    
-- =============================================          
CREATE PROCEDURE [dbo].[SApp_CompanyRevenueGeographicSplitByCompanyID]               
 @CompanyID uniqueidentifier = null,   
 @includeDeleted bit = 0               
AS          
BEGIN           
 SET NOCOUNT ON;            
 SELECT [CompanyRevenueGeographicSplitMap].[Id]     
   ,[CompanyRevenueGeographicSplitMap].[CompanyRevenueId]    
      ,[CompanyRevenueGeographicSplitMap].[RegionId]    
      ,[CompanyRevenueGeographicSplitMap].[RegionValue]    
      ,[CompanyRevenueGeographicSplitMap].[Year]    
      ,[CompanyRevenueGeographicSplitMap].[CreatedOn]    
      ,[CompanyRevenueGeographicSplitMap].[UserCreatedById]    
      ,[CompanyRevenueGeographicSplitMap].[ModifiedOn]    
      ,[CompanyRevenueGeographicSplitMap].[UserModifiedById]    
      ,[CompanyRevenueGeographicSplitMap].[IsDeleted]    
      ,[CompanyRevenueGeographicSplitMap].[DeletedOn]    
      ,[CompanyRevenueGeographicSplitMap].[UserDeletedById]    
      ,[CompanyRevenueGeographicSplitMap].[CompanyId]    
  FROM [dbo].[CompanyRevenueGeographicSplitMap]          
       
  WHERE ([CompanyRevenueGeographicSplitMap].CompanyId = @CompanyID) --and [CompanyRevenue].[projectId] = @ProjectID)                
 AND (@includeDeleted = 0 or [CompanyRevenueGeographicSplitMap].IsDeleted = 1)            
END