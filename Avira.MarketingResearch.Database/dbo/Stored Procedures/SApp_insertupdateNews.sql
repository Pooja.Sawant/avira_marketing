﻿      
      
CREATE procedure [dbo].[SApp_insertupdateNews]    
(          
@NewsList dbo.udtGUIDNewsMultiValue READONLY,          
@UserCreatedById uniqueidentifier,        
@CreatedOn Datetime      
)          
as          
/*================================================================================          
Procedure Name: SApp_insertupdateNews    
Change History          
Date  Developer  Reason          
__________ ____________ ______________________________________________________          
05/06/2019 Gopi   Initial Version          
================================================================================*/          
BEGIN          
SET NOCOUNT ON;          
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;        
declare @ErrorMsg NVARCHAR(2048)      
    
BEGIN TRY             
             
DELETE FROM [DBO].[NEWS]    
WHERE NOT EXISTS(SELECT 1 FROM @NEWSLIST LIST 
             WHERE LIST.NEWS = [NEWS].[NEWS])

    
INSERT INTO [dbo].[News]([Id],          
        [Date],          
		[News],  
		[NewsCategoryId],
		[ImportanceId],
		[ImpactDirectionId],
		[OtherParticipants],
		[Remarks],
        [IsDeleted],          
        [UserCreatedById],          
        [CreatedOn])          
 SELECT NEWID(),           
    Date,           
   News, 
   NewsCategoryId,
   ImportanceId,
   ImpactDirectionId,
   OtherParticipants,
   Remarks,
   0,          
   @UserCreatedById,           
   @CreatedOn    
   from @NewsList list    
   where not exists (select 1 from [News]     
    where list.News = [News].[News])    
    
SELECT StoredProcedureName ='SApp_insertupdateNews',Message =@ErrorMsg,Success = 1;           
        
END TRY          
BEGIN CATCH          
    -- Execute the error retrieval routine.          
 DECLARE @ErrorNumber int;          
 DECLARE @ErrorSeverity int;          
 DECLARE @ErrorProcedure varchar(100);          
 DECLARE @ErrorLine int;          
 DECLARE @ErrorMessage varchar(500);          
          
  SELECT @ErrorNumber = ERROR_NUMBER(),          
        @ErrorSeverity = ERROR_SEVERITY(),          
        @ErrorProcedure = ERROR_PROCEDURE(),          
        @ErrorLine = ERROR_LINE(),          
        @ErrorMessage = ERROR_MESSAGE()          
          
 insert into dbo.Errorlog(ErrorNumber,          
       ErrorSeverity,          
       ErrorProcedure,          
       ErrorLine,          
       ErrorMessage,          
       ErrorDate)          
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,@CreatedOn);          
        
 set @ErrorMsg = 'Error while creating a new News, please contact Admin for more details.';          
      
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine        
    ,Message =@ErrorMsg,Success = 0;           
           
END CATCH          
          
          
end