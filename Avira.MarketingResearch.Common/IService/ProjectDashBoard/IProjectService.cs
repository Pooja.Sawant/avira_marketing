﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.ProjectDashBoard
{
    public interface IProjectService
    {
        IEnumerable<ProjectViewModel> GetAll(Guid Id);
        IEnumerable<ProjectViewModel> GetAll(Guid? guid, Guid AviraUserId, int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool includeDeleted);
        IEnumerable<ProjectViewModel> GetAllProject();
        //DisplayProjectDBViewModel GetById(Guid guid, Guid AviraUserId);
        Result<SpTransactionMessage> Create(MainProjectInsertDBViewModel tfc, AviraUser currentUser);
        Result<SpTransactionMessage> Update(MainProjectUpdateDBViewModel tfc, AviraUser currenUser);
        Result<SpTransactionMessage> UpdateProjectStatus(MainProjectUpdateDBViewModel tfc, AviraUser currenUser); 
        IEnumerable<IndustryViewModel> GetIndustrySegmentList(string id=null, bool includeParentOnly = false);
        IEnumerable<MarketViewModel> GetMarketSegmentList(string id = null, bool includeParentOnly = false);
        


    }
}
