﻿CREATE PROCEDURE [dbo].[SFab_GetESMAnalystViewList]    
(  
	@ProjectId uniqueidentifier, 
	@SegmentId uniqueidentifier,
	@SubSegmentIdList [dbo].[udtUniqueIdentifier] READONLY,--Optional
	@AviraUserID uniqueidentifier = null    
)    
as    
/*================================================================================    
Procedure Name: SFab_GetESMAnalystViewList   
Author: Praveen    
Create date: 12/09/2019    
Description: To return all Analyst View which are mapped with ecosystem mapping based on the segmentId and projectId. And optional imput values for subsegmentIds
Change History    
Date  Developer  Reason  
__________ ____________ ______________________________________________________    
12/09/2019  Praveen   Initial Version
================================================================================*/    
BEGIN    
------------------Sudo Code-----------------
	--Required Tables: ESMSegmentMapping, ESMAnalysis	
	--Case 1: if @SubSegmentIdList is not null then it should return data based on the segmentID and SubSegmentIdList for particular project
	--Case 2: if @SubSegmentIdList is null then it should return data based on the segmentID level for particular project
	--Expected Columns : Analysisd and AnalysisText
	
	Declare @CheckSubsegments bit = 0

	If exists(Select 1 From @SubSegmentIdList)
	Begin
		
		Set @CheckSubsegments = 1
	End
		
	Select Distinct EA.Id As AnalysisId, EA.AnalysisText
	From ESMAnalysis EA
		Inner Join ESMSegmentMapping ESM On
			ESM.SegmentId = EA.SegmentId AND
			EA.ProjectId = @ProjectId AND 
			EA.SegmentId = @SegmentId 
	Where (@CheckSubsegments = 0) Or 
			(@CheckSubsegments = 1 And EA.SubSegmentId Is Null) Or
			(@CheckSubsegments = 1 And EA.SubSegmentId Is Not Null And EA.SubSegmentId In (Select Id From @SubSegmentIdList))
	Order by EA.AnalysisText	
END