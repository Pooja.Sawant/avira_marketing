﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.QualitativeInfo;
using Avira.MarketingResearch.Repository.CoreInterfaces.QualitativeInfo;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.QualitativeInfo
{
    public class QualitativeAddTableRepository : IQualitativeAddTableRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "QualitativeAddTable";

        public QualitativeAddTableRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public IEnumerable<QualitativeAddTableViewModel> GetAll(Guid Id)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<QualitativeAddTableViewModel>(QualitativeAddTableQueries.StoreProcGetQualitativeTable
                            , new { @QualitativeID = Id }
                            , null, true, null, CommandType.StoredProcedure);
        }
        public Result<SpTransactionMessage> Update(QualitativeGenerateTableCRUDViewModel viewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(QualitativeAddTableQueries.StoreProcUpdate,
               new
               {
                   @Id = viewModel.Id,
                   @ProjectId = viewModel.ProjectId,
                   @TableName = viewModel.qualitativeAddTableViewModel.TableName,
                   @TableMetadata = viewModel.qualitativeAddTableViewModel.TableMetaData,
                   @TableData = viewModel.qualitativeAddTableViewModel.TableData,
                   @ModifiedOn = viewModel.ModifiedOn,
                   @UserModifiedById = viewModel.UserModifiedById,
                   @IsDeleted = viewModel.IsDeleted,
                   @DeletedOn = viewModel.DeletedOn,
                   @UserDeletedById = viewModel.UserDeletedById
               }
                    , CommandType.StoredProcedure);
            return spTransactionMessage;
        }

        public Result<SpTransactionMessage> Insert(QualitativeGenerateTableCRUDViewModel viewModel)
        {
            var spTransactionMessage = _dbHelper.ExecuteQuery(QualitativeAddTableQueries.StoreProcInsert,
               new
               {
               
                    @Id = viewModel.Id,
                    @ProjectId = viewModel.ProjectId,
                    @TableName = viewModel.qualitativeAddTableViewModel.TableName,
                    @TableMetadata = viewModel.qualitativeAddTableViewModel.TableMetaData,
                    @TableData = viewModel.qualitativeAddTableViewModel.TableData,
                    @CreatedOn = viewModel.CreatedOn,
                    @UserCreatedById = viewModel.UserCreatedById,
                    @ModifiedOn = viewModel.ModifiedOn,
                    @UserModifiedById=viewModel.UserModifiedById,
                    @IsDeleted=viewModel.IsDeleted,
                    @DeletedOn=viewModel.DeletedOn,
                    @UserDeletedById=viewModel.UserDeletedById
               }
                    , CommandType.StoredProcedure);
            return spTransactionMessage;
        }
    }
}
