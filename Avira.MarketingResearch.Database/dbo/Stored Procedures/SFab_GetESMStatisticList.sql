﻿CREATE PROCEDURE [dbo].[SFab_GetESMStatisticList]    
(  
	@ProjectId uniqueidentifier, 
	@SegmentId uniqueidentifier,
	@SubSegmentIdList [dbo].[udtUniqueIdentifier] READONLY,--Optional
	@AviraUserID uniqueidentifier = null    
)    
as    
/*================================================================================    
Procedure Name: SFab_GetESMStatisticList   
Author: Praveen    
Create date: 12/09/2019    
Description: To return all data of Value, Volume and CAGR column based on the segmentId or subsegmentId from Market size for its ‘Base year’ & ‘Last year’.
Change History    
Date  Developer  Reason  
__________ ____________ ______________________________________________________    
12/09/2019  Praveen   Initial Version    
================================================================================*/    
BEGIN    
declare @baseYear int, @baseYearValue decimal(18, 4),@baseYearVolume decimal(18, 4);
declare @YearMax int, @YearMaxValue decimal(18, 4),@YearMaxVolume decimal(18, 4);
DECLARE @yearSpan INT;

------------------Sudo Code-----------------
	--Required Tables: ESMSegmentMapping, QualitativeSegmentMapping and MarketValuesummary
	--Notes: in the TrendEcoSystemMap.EcoSystemId is the placeholder for SegmentID from Segment Table
	--Case 1: if @SubSegmentIdList is not null then it should return data based on the segmentID and SubSegmentIdList for particular project
	--Case 2: if @SubSegmentIdList is null then it should return data based on the segmentID level for particular project
	--Expected Columns : 
		Declare @SubSegmentIDs As Table ([ID] [uniqueidentifier] NOT NULL)

		CREATE TABLE #tmp_MS_TABLE ([year] int, Value decimal(18,4) NULL, Volume decimal(18,4)  NULL,ValueCAGR decimal(18,4)  NULL, VolumeCAGR decimal(18,4)  NULL, OperatingMargin decimal(18,4)  NULL)

	If Not exists(Select 1 From @SubSegmentIdList) 
	Begin

		Insert Into @SubSegmentIDs (ID)
		Select ID From SubSegment Where SegmentId = @SegmentId		
	End
	Else
	Begin

		Insert Into @SubSegmentIDs (ID)
		Select ID From @SubSegmentIdList
	End
	
	SELECT @baseYear = BaseYear FROM BaseYearMaster;

	SELECT @YearMax = MAX([Year]) FROM MarketValuesummary
	Inner Join @SubSegmentIDs SS On
			MarketValuesummary.SubSegmentId = SS.ID
	WHERE Projectid = @ProjectId AND SegmentId = @SegmentId AND (value > 0 OR Volume > 0)

	select @yearSpan =  @YearMax - @baseYear;

	;with cteCompany as (
		SELECT Company.Id as CompanyId, ECPE.SubSegmentId
			FROM Company
			INNER JOIN CompanyEcosystemSegmentMap ECPE ON Company.Id = ECPE.CompanyId AND ECPE.SegmentId = @SegmentId
					--Inner Join @SubSegmentIDs SS On ECPE.SubSegmentId = CASE WHEN ECPE.SubSegmentId IS NOT NULL THEN ECPE.SubSegmentId ELSE SS.ID END
			WHERE 
				 EXISTS(SELECT 1 FROM CompanyProjectMap WHERE Company.Id = CompanyProjectMap.CompanyId AND CompanyProjectMap.ProjectId = @ProjectId)
				
		)


--AS ValueCAGR
INSERT INTO #tmp_MS_TABLE([Year], Value, Volume, OperatingMargin)
	SELECT MS.[Year], Value AS Value, Volume AS Volume, OperatingMargin AS OperatingMargin
	 FROM (

			SELECT [Year],  SUM([dbo].[fun_UnitDeConversion](ValueConversionId, [value])) AS value, SUM([dbo].[fun_UnitDeConversion](VolumeConversionId, Volume)) as Volume 
			FROM MarketValuesummary
			--Inner Join @SubSegmentIDs SS On	MarketValuesummary.SubSegmentId = SS.ID
			WHERE Projectid = @ProjectId AND SegmentId = @SegmentId AND
			--EXISTS(SELECT 1 FROM @SubSegmentIDs SS WHERE MarketValuesummary.SubSegmentId = SS.ID) AND
			([Year] = @baseYear OR [Year] = @YearMax)

			 AND EXISTS(

				  SELECT 1 FROM QualitativeSegmentMapping
				  INNER JOIN @SubSegmentIDs SS ON SS.ID = QualitativeSegmentMapping.SubSegmentId AND MarketValuesummary.SubSegmentId = SS.ID
				  WHERE QualitativeSegmentMapping.ProjectId = @ProjectId AND QualitativeSegmentMapping.SegmentId = @SegmentId
				  
				  )



			GROUP BY [Year], ValueConversionId, VolumeConversionId

	) MS
	LEFT JOIN 
	 (
			SELECT [Year],  
			AVG(
			
			[dbo].[fun_UnitDeConversion](ValueConversionId,CompanyPLStatement.EBIT) / [dbo].[fun_UnitDeConversion](ValueConversionId,NULLIF(CompanyPLStatement.Revenue, 0)) * 100
			
			) 
			AS OperatingMargin 
			--FROM CompanyEcosystemSegmentMap ECPE
			FROM CompanyPLStatement 
			--INNER JOIN cteCompany ON cteCompany.CompanyId = CompanyPLStatement.CompanyId			
			WHERE ([Year] = @baseYear OR [Year] = @YearMax)
			AND EXISTS(SELECT 1 FROM cteCompany WHERE cteCompany.CompanyId = CompanyPLStatement.CompanyId)
			GROUP BY [Year], ValueConversionId

	) OM ON MS.Year = OM.Year
	

	SELECT @baseYearValue = value, @baseYearVolume = Volume FROM  #tmp_MS_TABLE WHERE [year] = @baseYear
	SELECT @YearMaxValue = value, @YearMaxVolume = Volume FROM  #tmp_MS_TABLE WHERE [year] = @YearMax

	SELECT [year] ,  Value , Volume, OperatingMargin
	 ,
	CASE WHEN ISNULL(@baseYearValue,0) = 0 OR ISNULL(@yearSpan, 0) <= 0  THEN 0 ELSE
	CAST(100*ABS(POWER((CAST(cast(@YearMaxValue AS nvarchar(100))  AS DECIMAL(18,4))  /CAST(CAST(@baseYearValue AS nvarchar(100)) AS DECIMAL(18,4))),(CAST(1 AS DECIMAL(18,4))/ 
	CAST(@yearSpan AS nvarchar(100)) ))-1) AS DECIMAL(18,4)) END AS ValueCAGR,
	CASE WHEN ISNULL(@baseYearVolume, 0) = 0 OR ISNULL(@yearSpan, 0) <= 0 THEN 0 ELSE
	CAST(100*ABS(POWER((CAST(cast(@YearMaxVolume AS nvarchar(100))  AS DECIMAL(18,4))  /CAST(CAST(@baseYearVolume AS nvarchar(100)) AS DECIMAL(18,4))),(CAST(1 AS DECIMAL(18,4))/ 
	CAST(@yearSpan AS nvarchar(100)) ))-1) AS DECIMAL(18,4)) END AS VolumeCAGR
	
	FROM #tmp_MS_TABLE

	DROP TABLE #tmp_MS_TABLE
END