﻿using Avira.MarketingResearch.Model.ViewModels.Masters;
using System;
using System.Collections.Generic;
using System.Text;

namespace Avira.MarketingResearch.Common.IService.Masters
{
    public interface IStatusService
    {
        IEnumerable<StatusViewModel> GetAll(string statusType);
    }
}
