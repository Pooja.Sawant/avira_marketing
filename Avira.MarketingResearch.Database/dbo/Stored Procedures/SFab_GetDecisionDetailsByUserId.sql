﻿

CREATE PROCEDURE [dbo].[SFab_GetDecisionDetailsByUserId]
	(		
		@DecisionId uniqueidentifier=null,
		@UserID uniqueidentifier = null
	)
	AS

--Declare @DecisionId uniqueidentifier=null,
--		@UserID uniqueidentifier = null
--select @DecisionId='65a72300-796a-4fea-aef1-84ff0fa4fdfe',@UserID='1570416B-443A-4833-AA8F-941E159DD90F'

/*================================================================================
Procedure Name: [dbo].[SFab_GetDecisionDetailsByUserId]
Author: Pooja Sawant
Create date: 06/01/2020
Description: Get list from decision table by userid and decision id
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
06/01/2020	Pooja S			Initial Version
================================================================================*/

BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	
	declare @DecisionStatusId uniqueidentifier
	
	select @DecisionStatusId = ID from LookupStatus
	where StatusName = 'Decision made' and StatusType='DMDecisionStatus';
	
	If Exists (Select ID From Decision Where BaseDecisionId = @DecisionId)
	Begin

		Select @DecisionId = ID From Decision Where BaseDecisionId = @DecisionId
	End

	If Not Exists (Select ID from Decision Where Id = @DecisionId)
	Begin

		Select Id AS DecisionId, Description, Score, '' As TargetedOutcome, @UserID UserId, 'false' as IsReadonly
		From BaseDecision
		Where ID = @DecisionId		
	End
	Else
	Begin

		SELECT Decision.Id AS DecisionId, Decision.Description,  Decision.Score, DecisionOutcome.TargetedOutcome, Decision.UserId, 
			Case when (Decision.StatusId = @DecisionStatusId) then 'true' else 'false' end as IsReadonly
		FROM Decision Left Join
					DecisionOutcome ON Decision.Id = DecisionOutcome.DecisionId
		where Decision.Id=@DecisionId and Decision.UserId=@UserID       
		order by Decision.Description
	End
END