﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.Trend;
using Avira.MarketingResearch.Repository.CoreInterfaces.Trend;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.Trend
{
    public class TrendKeyCompanyMapRepository : ITrendKeyCompanyMapRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "TrendKeyCompanyMapping";

        public TrendKeyCompanyMapRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }
 
        public IEnumerable<TrendKeyCompanyMapViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false, string id = "")
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<TrendKeyCompanyMapViewModel>(TrendKeyCompanyMapQueries.StoreProcSelectAll
                            , new { @companyName = Search, @TrendID = id, @includeDeleted = false, @OrdbyByColumnName = @OrdbyByColumnName, @PageStart = PageStart, @pageSize = pageSize, @SortDirection = SortDirection }
                            , null, true, null, CommandType.StoredProcedure);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="trendKeyCompanyMapInsertDbViewModel"></param>
        /// <returns></returns>
        public Result<SpTransactionMessage> InsertUpdate(MainTrendKeyCompanyMapInsertUpdateDbViewModel trendKeyCompanyMapInsertDbViewModel)
        {

            DataTable trendMarketMapTable = new DataTable();
            trendMarketMapTable.Columns.Add("ID", typeof(Guid));
            if (trendKeyCompanyMapInsertDbViewModel.TrendKeyCompanyMapInsertUpdate != null & trendKeyCompanyMapInsertDbViewModel.TrendKeyCompanyMapInsertUpdate.Count > 0)
            {
                foreach (var item in trendKeyCompanyMapInsertDbViewModel.TrendKeyCompanyMapInsertUpdate)
                {
                    trendMarketMapTable.Rows.Add(new Object[] { item.CompanyId });
                }
            }


            var spTransactionMessage = _dbHelper.ExecuteQuery(TrendKeyCompanyMapQueries.StoreProcInsertUpdate,
               new
               {
                   @TrendId = trendKeyCompanyMapInsertDbViewModel.TrendId,
                   TrendKeyCompanymap = trendMarketMapTable,
                   @AviraUserId = trendKeyCompanyMapInsertDbViewModel.AviraUserId
               }
                    , CommandType.StoredProcedure);
            return spTransactionMessage;
        }

        public Result<SpTransactionMessage> Insert(MainTrendKeyCompanyMapInsertUpdateDbViewModel trendKeyCompanyMapInsertDbViewModel)
        {

            DataTable trendMarketMapTable = new DataTable();
            trendMarketMapTable.Columns.Add("ID", typeof(Guid));
            if (trendKeyCompanyMapInsertDbViewModel.TrendKeyCompanyMapInsertUpdate != null & trendKeyCompanyMapInsertDbViewModel.TrendKeyCompanyMapInsertUpdate.Count > 0)
            {
                foreach (var item in trendKeyCompanyMapInsertDbViewModel.TrendKeyCompanyMapInsertUpdate)
                {
                    trendMarketMapTable.Rows.Add(new Object[] { item.CompanyId });
                }
            }


            var spTransactionMessage = _dbHelper.ExecuteQuery(TrendKeyCompanyMapQueries.StoreProcInsert,
               new
               {
                   @TrendId = trendKeyCompanyMapInsertDbViewModel.TrendId,
                   TrendKeyCompanymap = trendMarketMapTable,
                   @AviraUserId = trendKeyCompanyMapInsertDbViewModel.AviraUserId
               }
                    , CommandType.StoredProcedure);
            return spTransactionMessage;
        }


        public Result<SpTransactionMessage> Delete(MainTrendKeyCompanyMapInsertUpdateDbViewModel trendKeyCompanyMapInsertDbViewModel)
        {

            DataTable trendMarketMapTable = new DataTable();
            trendMarketMapTable.Columns.Add("ID", typeof(Guid));
            if (trendKeyCompanyMapInsertDbViewModel.TrendKeyCompanyMapInsertUpdate != null & trendKeyCompanyMapInsertDbViewModel.TrendKeyCompanyMapInsertUpdate.Count > 0)
            {
                foreach (var item in trendKeyCompanyMapInsertDbViewModel.TrendKeyCompanyMapInsertUpdate)
                {
                    trendMarketMapTable.Rows.Add(new Object[] { item.CompanyId });
                }
            }


            var spTransactionMessage = _dbHelper.ExecuteQuery(TrendKeyCompanyMapQueries.StoreProcDelete,
               new
               {
                   @TrendId = trendKeyCompanyMapInsertDbViewModel.TrendId,
                   TrendKeyCompanymap = trendMarketMapTable,
                   @AviraUserId = trendKeyCompanyMapInsertDbViewModel.AviraUserId
               }
                    , CommandType.StoredProcedure);
            return spTransactionMessage;
        }
    }
}
