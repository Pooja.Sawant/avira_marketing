﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Model.ViewModels.ProjectDashBoard;
using Avira.MarketingResearch.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Avira.MarketingResearch.Common.IService.CompanyProfile
{
    public interface ICompanyFinancialAnalysisService
    {       
        Task<MainCompanyFinancialAnalysisInsertUpdateViewModel> GetAll(int PageStart, int pageSize, int SortDirection, string OrdbyByColumnName, string Search, bool isDeletedInclude = false, string id = "");
        IEnumerable<ValueConversionViewModel> GetAllValueConversion();
        Result<SpTransactionMessage> InsertUpdate(CompanyFinancialAnalysisInsertUpdateModel tfc, AviraUser currenUser);
        IEnumerable<CurrencyViewModel> GetAllCurrencies();
        IEnumerable<CategoryViewModel> GetAllCategories();
    }
}
