﻿CREATE TABLE [dbo].[QualitativeQuotes] (
    [Id]                    UNIQUEIDENTIFIER NOT NULL,
    [ProjectId]             UNIQUEIDENTIFIER NOT NULL,
    [MarketId]              UNIQUEIDENTIFIER NULL,
    [ResourceName]          NVARCHAR (255)   NOT NULL,
    [ResourceDesignation]   NVARCHAR (255)   NULL,
    [ResourceDesignationId] UNIQUEIDENTIFIER NULL,
    [ResourceCompanyName]   NVARCHAR (255)   NOT NULL,
    [ResourceCompanyId]     UNIQUEIDENTIFIER NULL,
    [DateOfQuote]           DATETIME         NOT NULL,
    [Quote]                 NVARCHAR (4000)  NULL,
    [OtherRemarks]          NVARCHAR (4000)  NULL,
    [CreatedOn]             DATETIME2 (7)    NULL,
    [UserCreatedById]       UNIQUEIDENTIFIER NULL,
    [ModifiedOn]            DATETIME2 (7)    NULL,
    [UserModifiedById]      UNIQUEIDENTIFIER NULL,
    [IsDeleted]             BIT              NOT NULL,
    [DeletedOn]             DATETIME2 (7)    NULL,
    [UserDeletedById]       UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_QualitativeQuotes] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_QualitativeQuotes_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_QualitativeQuotes_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_QualitativeQuotes_Market] FOREIGN KEY ([MarketId]) REFERENCES [dbo].[Market] ([Id]),
    CONSTRAINT [FK_QualitativeQuotes_Project] FOREIGN KEY ([ProjectId]) REFERENCES [dbo].[Project] ([ID])
);



