﻿CREATE procedure [dbo].[SFab_ValidateUserResetPassword]
(
	@UserName nvarchar(100)
)
as
/*================================================================================
Procedure Name: SFab_ValidateUserResetPassword
Author: Pooja
Create date: 09/06/2019
Description: Get user login details by Id
Change History
Date		Developer		Reason
__________	____________	______________________________________________________
09/06/2019	Pooja			Initial Version
================================================================================*/
BEGIN
SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @ErrorMsg NVARCHAR(2048);  
  
--Validations  
if not exists (select 1 from [dbo].[AviraUser] where (UserName = @UserName) and ResetPasswordLinkCreatedOn  >= DATEADD(MINUTE,-5, getdate()) 
and   ResetPasswordLinkCreatedOn  <= getdate() )
	begin  	
     set @ErrorMsg = 'Your reset link is expired, or if you alredy used that link';  
     SELECT StoredProcedureName ='SFab_GetUserDetailsById',Message =@ErrorMsg,Success = 0;    
     RETURN;    
    end
BEGIN TRY  
SELECT StoredProcedureName ='SFab_GetUserDetailsById',Message = @ErrorMsg,Success = 1;  
END TRY  
BEGIN CATCH  
    -- Execute the error retrieval routine.  
 DECLARE @ErrorNumber int;  
 DECLARE @ErrorSeverity int;  
 DECLARE @ErrorProcedure varchar(100);  
 DECLARE @ErrorLine int;  
 DECLARE @ErrorMessage varchar(500);  
  
  SELECT @ErrorNumber = ERROR_NUMBER(),  
        @ErrorSeverity = ERROR_SEVERITY(),  
        @ErrorProcedure = ERROR_PROCEDURE(),  
        @ErrorLine = ERROR_LINE(),  
        @ErrorMessage = ERROR_MESSAGE()  
  
 insert into dbo.Errorlog(ErrorNumber,  
       ErrorSeverity,  
       ErrorProcedure,  
       ErrorLine,  
       ErrorMessage,  
       ErrorDate)  
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());  
  
 set @ErrorMsg = 'Error while validating a AviraUser , please contact Admin for more details.';  
  
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine    
    ,Message =@ErrorMsg,Success = 0;  
  
END CATCH  
end