﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SQLDatabaseQueryContainer
{
    public class CompanyFundamentalQueries
    {
        public static string StoreProcSelectAll = "SApp_GetNatureType";
        public static string StoreProcSelectAllForCompanyRegions = "SApp_GetAllRegions";
        public static string StoreProcSelectAllForEntities = "SApp_GetAllEntityTypes";
        public static string StoreProcSelectAllForCompanySubsidary = "SApp_GetSubsidaryCompanyListbyCompanyId";
        public static string StoreProcSelectAllForCompanyDetails = "SApp_GetCompanyDetailsbyProjectID";
        public static string StoreProcSelectAllForAllCompaniesList = "SApp_GetAllCompaniesList";

        public static string StoreProcSelectAllDesignations = "SApp_GetAllDesignations";

        public static string StoreProcSelectCompanyStageList = "SApp_GetAllCompanyStage";
        public static string StoreProcInsertUpdateCompProjMap = "SApp_insertUpdateCompanyProjectMap";
        public static string StoreProcInsertUpdateCompDetails = "SApp_insertUpdateCompanyProfile";
        public static string StoreProcInsertUpdateCompSubsidary = "SApp_insertUpdateSubsidaryCompaniesByCompanyParentId";
        public static string StoreProcInsertUpdateCompKeyEmp = "SApp_insertCompanyKeyEmpMap";
        public static string StoreProcInsertUpdateCompBoardOfDirectories = "SApp_insertUpdateCompanyBoardOfDirectorsMap";
        public static string StoreProcInsertUpdateCompShareHolding = "SApp_insertUpdateCompanyShareHolidingMap";
        public static string StoreProcInsertUpdateCompGeographies = "SApp_insertUpdateCompanyGeographiesMap";

        public static string StoreProcSelectAllManagers = "SApp_GetAllManagers";

        public static string StoreProcSelectAllNatures = "SApp_GetAllNatures";
    }
}
