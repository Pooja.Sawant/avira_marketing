﻿CREATE TABLE [dbo].[ImportData] (
    [Id]                      UNIQUEIDENTIFIER NOT NULL,
    [TemplateName]            NVARCHAR (200)   NULL,
    [ImportFileName]          NVARCHAR (200)   NULL,
    [FileName]                NVARCHAR (200)   NULL,
    [ImportFilePath]          NVARCHAR (1000)  NULL,
    [ImportStatusId]          UNIQUEIDENTIFIER NULL,
    [ImportException]         NVARCHAR (MAX)   NULL,
    [ImportExceptionFilePath] NVARCHAR (1000)  NULL,
    [ImportedOn]              DATETIME2 (7)    NULL,
    [UserImportedById]        UNIQUEIDENTIFIER NULL,
    [ModifiedOn]              DATETIME2 (7)    NULL,
    [UserModifiedById]        UNIQUEIDENTIFIER NULL
);





