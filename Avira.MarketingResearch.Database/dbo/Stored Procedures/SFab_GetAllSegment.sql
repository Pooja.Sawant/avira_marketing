﻿
  
CREATE PROCEDURE [dbo].[SFab_GetAllSegment]
(@AviraUserID uniqueidentifier = null)    
 AS  
/*================================================================================  
Procedure Name: [SFab_GetAllSegment]  
Author: Pooja Sawant  
Create date: 05/28/2019  
Description: Get list of segment from table  
Change History  
Date  Developer  Reason  
__________ ____________ ______________________________________________________  
05/28/2019 Pooja   Initial Version  
================================================================================*/  
  
BEGIN  
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
	
	select * from segment WHERE IsDeleted = 0 
	order by SegmentName asc

END