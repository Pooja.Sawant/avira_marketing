﻿CREATE TABLE [dbo].[QualitativeDescription] (
    [Id]                           UNIQUEIDENTIFIER NOT NULL,
    [ProjectId]                    UNIQUEIDENTIFIER NOT NULL,
    [MarketId]                     UNIQUEIDENTIFIER NULL,
    [QualitativeAnalysisTypeId]    UNIQUEIDENTIFIER NOT NULL,
    [QualitativeAnalysisSubTypeId] UNIQUEIDENTIFIER NULL,
    [DescriptionText]              NVARCHAR (4000)  NULL,
    [CreatedOn]                    DATETIME2 (7)    NULL,
    [UserCreatedById]              UNIQUEIDENTIFIER NULL,
    [ModifiedOn]                   DATETIME2 (7)    NULL,
    [UserModifiedById]             UNIQUEIDENTIFIER NULL,
    [IsDeleted]                    BIT              NOT NULL,
    [DeletedOn]                    DATETIME2 (7)    NULL,
    [UserDeletedById]              UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_QualitativeDescription] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_QualitativeDescription_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_QualitativeDescription_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_QualitativeDescription_Market] FOREIGN KEY ([MarketId]) REFERENCES [dbo].[Market] ([Id]),
    CONSTRAINT [FK_QualitativeDescription_Project] FOREIGN KEY ([ProjectId]) REFERENCES [dbo].[Project] ([ID]),
    CONSTRAINT [FK_QualitativeDescription_QualitativeAnalysisSubType] FOREIGN KEY ([QualitativeAnalysisSubTypeId]) REFERENCES [dbo].[QualitativeAnalysisSubType] ([Id]),
    CONSTRAINT [FK_QualitativeDescription_QualitativeAnalysisType] FOREIGN KEY ([QualitativeAnalysisTypeId]) REFERENCES [dbo].[QualitativeAnalysisType] ([Id])
);

