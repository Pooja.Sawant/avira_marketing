﻿using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.CompanyProfile;
using Avira.MarketingResearch.Repository.CoreInterfaces.CompanyProfile;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.CompanyProfile
{
    public class CompanySwotRepository : ICompanySwotRepository
    {
        protected IDbHelper _dbHelper;
        private const string CacheRegion = "CompanySWOT";

        public CompanySwotRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public IEnumerable<CompanySWOTViewModel> GetAll(Guid CompanyId,Guid ProjectId)
        {
            var dapperStrategy = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapperStrategy.Query<CompanySWOTViewModel>(CompanySwotQueries.StoreProcSelectAll
                            , new { @ComapanyID = CompanyId,@ProjectId= ProjectId }
                            , null, true, null, CommandType.StoredProcedure);
        }

        public Result<SpTransactionMessage> InsertUpdate(MainCompanySWOTViewModel viewModel, AviraUser currenUser)
        {

            DataTable CompanySWOTTable = new DataTable();
            CompanySWOTTable.Columns.Add("Id", typeof(Guid));
            CompanySWOTTable.Columns.Add("CompanyId", typeof(Guid));
            CompanySWOTTable.Columns.Add("ProjectId", typeof(Guid));
            CompanySWOTTable.Columns.Add("CategoryId", typeof(Guid));
            CompanySWOTTable.Columns.Add("SWOTTypeName", typeof(string));
            CompanySWOTTable.Columns.Add("SWOTDescription", typeof(string));

            if (viewModel.companySWOTOpportunitiesViewModel != null & viewModel.companySWOTOpportunitiesViewModel.Count > 0)
            { 
               
                   foreach (var item in viewModel.companySWOTOpportunitiesViewModel)
                    {
                        if (item.Id == Guid.Empty)
                        {
                            item.Id = Guid.NewGuid();
                        }
                        item.CompanyId = viewModel.CompanyId;
                        item.ProjectId = viewModel.ProjectId;
                        CompanySWOTTable.Rows.Add(new Object[] { item.Id, item.CompanyId, item.ProjectId, item.CategoryId, item.SWOTTypeName, item.SWOTDescription});
                    }
            }


            if (viewModel.companySWOTStrengthViewModel != null & viewModel.companySWOTStrengthViewModel.Count > 0)
            {
                foreach (var item in viewModel.companySWOTStrengthViewModel)
                {
                    if (item.Id == Guid.Empty)
                    {
                        item.Id = Guid.NewGuid();
                    }
                    item.CompanyId = viewModel.CompanyId;
                    item.ProjectId = viewModel.ProjectId;
                    CompanySWOTTable.Rows.Add(new Object[] { item.Id, item.CompanyId, item.ProjectId, item.CategoryId, item.SWOTTypeName, item.SWOTDescription });
                }
            }

            if (viewModel.companySWOTThreatsViewModel != null & viewModel.companySWOTThreatsViewModel.Count > 0)
            { 
                foreach (var item in viewModel.companySWOTThreatsViewModel)
                {
                    if (item.Id == Guid.Empty)
                    {
                        item.Id = Guid.NewGuid();
                    }
                    item.CompanyId = viewModel.CompanyId;
                    item.ProjectId = viewModel.ProjectId;
                    CompanySWOTTable.Rows.Add(new Object[] { item.Id, item.CompanyId, item.ProjectId, item.CategoryId, item.SWOTTypeName, item.SWOTDescription });
                }
            }

            if (viewModel.companySWOTWeeknessesViewModel != null & viewModel.companySWOTWeeknessesViewModel.Count > 0)
            {
                foreach (var item in viewModel.companySWOTWeeknessesViewModel)
                {
                    if (item.Id == Guid.Empty)
                    {
                        item.Id = Guid.NewGuid();
                    }
                    item.CompanyId = viewModel.CompanyId;
                    item.ProjectId = viewModel.ProjectId;
                    CompanySWOTTable.Rows.Add(new Object[] { item.Id, item.CompanyId, item.ProjectId, item.CategoryId, item.SWOTTypeName, item.SWOTDescription });
                }
            }

            var spTransactionMessage = _dbHelper.ExecuteQuery(CompanySwotQueries.StoreProcInsert,
               new
               {
                   @CompanyId = viewModel.CompanyId,
                   @ProjectId = viewModel.ProjectId,
                   @CompanySwotMap = CompanySWOTTable,
                   @UserCreatedById = currenUser.Id
               }
                    , CommandType.StoredProcedure);
            return spTransactionMessage;

        }
    }
}
