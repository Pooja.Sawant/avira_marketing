﻿    
CREATE PROCEDURE [dbo].[SApp_GetAllStatusbyType]      
 (      
  @statusType nvarchar(50)      
 )      
 AS      
/*================================================================================      
Procedure Name: [SApp_GetAllStatusbyType]      
Author: Harshal      
Create date: 10/17/2019      
Description: Get list of LookupStatus from table       
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
10/17/2019 Harshal   Initial Version      
================================================================================*/      
      
BEGIN      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED      
      
 SELECT [LookupStatus].[Id],      
  [LookupStatus].[StatusName],      
  [LookupStatus].[Description]     
FROM [dbo].[LookupStatus]      
WHERE ([LookupStatus].StatusType =@statusType)       
 END