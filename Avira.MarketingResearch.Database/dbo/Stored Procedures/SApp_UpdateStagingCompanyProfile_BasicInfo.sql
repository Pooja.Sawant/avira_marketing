﻿CREATE procedure [dbo].[SApp_UpdateStagingCompanyProfile_BasicInfo]
        
as        
/*================================================================================        
Procedure Name: SApp_UpdateStagingCompanyProfile_BasicInfo       
Author: Sharath        
Create date: 08/22/2019        
Description: to update Company Profiles 
Change History        
Date  Developer  Reason        
__________ ____________ ______________________________________________________        
08/22/2019 Praveen   Initial Version        
================================================================================*/        
BEGIN        
SET NOCOUNT ON;        
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;        
declare @ErrorMsg NVARCHAR(2048)          
        
BEGIN TRY         
        

--declare @CompanyId [UNIQUEIDENTIFIER] = NEWID();
--Fundamental verification
update staging
set ErrorNotes = isnull(ErrorNotes,'')  
+ case when LEN(staging.CompanyName) = 0 OR ISNULL(staging.CompanyName,'') = '' then 'Company name is mandatory, ' else '' end
+ case when LEN(staging.CompanyName) > 256  then 'Enter Maximum 256 Characters in Company name, ' else '' end
+ case when exists (select 1 from company where company.CompanyName = staging.CompanyName) then 'Company name Already Exists, ' else '' end
+ Case When Exists(Select 1 From StagingCompanyProfileFundamentals Group By StagingCompanyProfileFundamentals.CompanyName Having Count(*) > 1) Then 'Duplicate Company found,' else '' end
+ case when LEN(staging.Nature) = 0 OR ISNULL(staging.Nature,'') = '' then 'Nature is mandatory, ' else '' end
+ case when not exists (select 1 from Nature where Nature.NatureTypeName = staging.Nature) then 'Invalid Nature, ' else '' end
--+ case when not exists (select 1 from CompanyStage where CompanyStage.CompanyStageName = staging.CompanyStage) then 'Invalid Company Stage, ' else '' end
--+ case when isnumeric(Foundedyear) = 0 then 'Invalid Founded year, ' else '' end
--+ case when isnumeric(NumberOfEmployee) = 0 then 'Invalid NumberOfEmployee, ' else '' end
--+ case when isdate(AsonDate) = 0 then 'Invalid AsonDate, ' else '' end
--+ case when not exists (select 1 from Currency where Currency.Currencycode = staging.Currency1) then 'Invalid Currency, ' else '' end
+ case when LEN(staging.GeographicPresence) = 0 OR ISNULL(staging.GeographicPresence,'') = '' then 'GeographicPresence is mandatory, ' else '' end
+ case when exists (select 1 from dbo.FUN_STRING_TOKENIZER(GeographicPresence,',') geo
					left join Country 
					on geo.value = Country.CountryName
					left join Region
					on geo.value = Region.RegionName 
					where Country.ID is null and Region.ID is null) then 'Invalid GeographicPresence, ' else '' end
,CurrencyId = (select top 1 id from Currency where Currency.Currencycode = staging.Currency1)
,NatureId = (select top 1 id from Nature where Nature.NatureTypeName = staging.Nature)
,CompanyStageId = (select top 1 id from CompanyStage where CompanyStage.CompanyStageName = staging.CompanyStage),
CompanyId = NEWID(),
staging.CompanyName = staging.CompanyName
from [StagingCompanyProfileFundamentals] staging



update staging
set ErrorNotes = isnull(ErrorNotes,'') + 
case when LEN(staging.CompanyName) = 0 OR ISNULL(staging.CompanyName,'') = '' then 'Company name is mandatory, ' else '' end
+ case when LEN(staging.CompanyName) > 256  then 'Enter Maximum 256 Characters in Company name, ' else '' end
+ case when LEN(staging.Units)>0 then 
               case when not exists(select 1 from ValueConversion where ValueConversion.ValueName = staging.Units) then 'Invalid Units, 'else '' end
else '' end
+ case when LEN(staging.Currency)>0 then 
               case when not exists(select 1 from Currency where Currency.CurrencyCode = staging.Currency) then 'Invalid Currency, 'else '' end
else '' end
,ValueConversionId=(select top 1 id from ValueConversion  where ValueConversion.ValueName = staging.Units)
,CurrencyId = (select top 1 id from Currency where Currency.Currencycode = staging.Currency)
,CompanyId = (select top 1 CompanyId from StagingCompanyProfileFundamentals where StagingCompanyProfileFundamentals.CompanyName=staging.CompanyName)
from [StagingCompanyBasicRevenue] staging


IF(EXISTS(SELECT 1 FROM StagingCompanyProfileFundamentals WHERE ErrorNotes != '')
OR EXISTS(SELECT 1 FROM StagingCompanyBasicRevenue WHERE ErrorNotes != ''))    
BEGIN   
Select RowNo, 'Basics' as Module, ErrorNotes , CAST(0 AS BIT) AS Success FROM StagingCompanyProfileFundamentals WHERE ErrorNotes != '' UNION ALL
Select RowNo, 'Basics' as Module, ErrorNotes , CAST(0 AS BIT) AS Success FROM StagingCompanyBasicRevenue WHERE ErrorNotes != ''
ORDER BY RowNo

DELETE FROM ImportData where Id in (select ImportId from StagingCompanyProfileFundamentals sm);    
DELETE FROM StagingCompanyProfileFundamentals;
DELETE FROM StagingCompanyBasicRevenue;
END  
ELSE  
Begin
	Exec [dbo].[SApp_insertAllCompanyProfileFromStagging_BasicInfo];
	DELETE FROM StagingCompanyProfileFundamentals;	
	DELETE FROM StagingCompanyBasicRevenue;
End 

END TRY              
BEGIN CATCH              
    -- Execute the error retrieval routine.              
 DECLARE @ErrorNumber int;              
 DECLARE @ErrorSeverity int;              
 DECLARE @ErrorProcedure varchar(100);              
 DECLARE @ErrorLine int;      
 DECLARE @ErrorMessage varchar(500);              
              
  SELECT @ErrorNumber = ERROR_NUMBER(),              
        @ErrorSeverity = ERROR_SEVERITY(),              
        @ErrorProcedure = ERROR_PROCEDURE(),              
        @ErrorLine = ERROR_LINE(),              
        @ErrorMessage = ERROR_MESSAGE()              
              
 insert into dbo.Errorlog(ErrorNumber,              
       ErrorSeverity,              
       ErrorProcedure,              
       ErrorLine,              
       ErrorMessage,              
       ErrorDate)              
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE());              
            
 set @ErrorMsg = 'Error while updating SApp_UpdateStagingCompanyProfile_BasicInfo, please contact Admin for more details.';            
SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine            
,Message =@ErrorMsg,Success = 0;               
               
END CATCH              
        
        
end