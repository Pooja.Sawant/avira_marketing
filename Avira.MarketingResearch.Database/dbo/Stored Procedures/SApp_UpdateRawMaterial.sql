﻿CREATE procedure [dbo].[SApp_UpdateRawMaterial]    
(@Id uniqueidentifier,    
@Description nvarchar(256),    
@RawMaterialName nvarchar(256),    
@IsDeleted bit=0,  
@UserModifiedById  uniqueidentifier,  
@DeletedOn datetime = null,  
@UserDeletedById uniqueidentifier = null,  
@ModifiedOn Datetime   
)
as    
/*================================================================================    
Procedure Name: SApp_UpdateRawMaterial    
Author: Sharath    
Create date: 02/19/2019    
Description: update a record in RawMaterial table by ID. Also used for delete    
Change History    
Date  Developer  Reason    
__________ ____________ ______________________________________________________    
02/19/2019 Sharath   Initial Version  
------------------------------------------------------------------------------  
03/18/2019 Nimishekh 1) All the paramter of Model should be decalre as input parameter   
						with default value is null if those parameter is not mandetory.      
					 2) Return should select with all the value of standard Model that we are using in Code  
================================================================================*/    
BEGIN    
SET NOCOUNT ON;    
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
declare @SegmentId uniqueidentifier;
select @SegmentId = Id from Segment
where SegmentName = 'Raw materials';
  
IF exists(select 1 from [dbo].[SubSegment]     
   where [Id] !=  @Id 
   and SegmentId = @SegmentId
   and  SubSegmentName = @RawMaterialName AND IsDeleted = 0)    
begin    
declare @ErrorMsg NVARCHAR(2048);    
set @ErrorMsg = 'Raw-Material Name "'+ @RawMaterialName + '" already exists.';    
--SELECT 50000,@ErrorMsg,1;   
--return(1);   
SELECT StoredProcedureName ='SApp_UpdateRawMaterial','Message' =@ErrorMsg,'Success' = 0;  
RETURN;
end    
  
BEGIN TRY     
--update record    
update [dbo].[SubSegment]    
set SubSegmentName = @RawMaterialName,    
 [Description] = @Description,    
 [ModifiedOn] = getdate(),    
 DeletedOn = case when IsDeleted = 0 and @IsDeleted = 1 then getdate() else DeletedOn end,    
 UserDeletedById = case when IsDeleted = 0 and @IsDeleted = 1 then @UserModifiedById else UserDeletedById end,    
 IsDeleted = isnull(@IsDeleted, IsDeleted),    
 [UserModifiedById] = @UserModifiedById    
where ID = @Id   
   
SELECT StoredProcedureName ='SApp_UpdateRawMaterial',Message =@ErrorMsg,Success = 1;     
  
END TRY    
BEGIN CATCH    
    -- Execute the error retrieval routine.    
 DECLARE @ErrorNumber int;    
 DECLARE @ErrorSeverity int;    
 DECLARE @ErrorProcedure varchar(100);    
 DECLARE @ErrorLine int;    
 DECLARE @ErrorMessage varchar(500);    
    
  SELECT @ErrorNumber = ERROR_NUMBER(),    
        @ErrorSeverity = ERROR_SEVERITY(),    
        @ErrorProcedure = ERROR_PROCEDURE(),    
        @ErrorLine = ERROR_LINE(),    
        @ErrorMessage = ERROR_MESSAGE()    
    
 insert into dbo.Errorlog(ErrorNumber,    
       ErrorSeverity,    
       ErrorProcedure,    
       ErrorLine,    
       ErrorMessage,    
       ErrorDate)    
        values(@ErrorNumber,@ErrorSeverity,@ErrorProcedure,@ErrorLine,@ErrorMessage,GETDATE()); 

set @ErrorMsg = 'Error while updating RawMaterial, please contact Admin for more details.' 		   
 --set @ErrorMsg = 'Unspecified Error';    
--THROW 50000,@ErrorMsg,1;    
 --return(1)  
 SELECT Number = @ErrorNumber, Severity =@ErrorSeverity,StoredProcedureName =@ErrorProcedure,LineNumber= @ErrorLine  
    ,Message =@ErrorMsg,Success = 0;     
END CATCH    
    
  
  
    
end