﻿CREATE TABLE [dbo].[CurrencyConversion] (
    [Id]                  UNIQUEIDENTIFIER NOT NULL,
    [CurrencyId]          UNIQUEIDENTIFIER NOT NULL,
    [ConversionRangeType] TINYINT          NULL,
    [ConversionRate]      DECIMAL (19, 4)  NOT NULL,
    [StartDate]           DATETIME         NULL,
    [EndDate]             DATETIME         NULL,
    [Year]                INT              NULL,
    [Quarter]             NVARCHAR (25)    NULL,
    [CreatedOn]           DATETIME2 (7)    NULL,
    [UserCreatedById]     UNIQUEIDENTIFIER NULL,
    [ModifiedOn]          DATETIME2 (7)    NULL,
    [UserModifiedById]    UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_CurrencyConversion] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CurrencyConversion_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_CurrencyConversion_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id])
);

