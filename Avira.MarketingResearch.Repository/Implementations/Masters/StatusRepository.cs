﻿using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Repository.CoreInterfaces.Masters;
using Avira.MarketingResearch.Repository.Helpers;
using Dapper;
using SQLDatabaseQueryContainer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Avira.MarketingResearch.Repository.Implementations.Masters
{
    public class StatusRepository : IStatusRepository
    {
        protected IDbHelper _dbHelper;

        public StatusRepository(IDbHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }
        public IEnumerable<StatusViewModel> GetAll(string statusType)
        {
            var dapper = _dbHelper.GetDbConnection(_dbHelper.DatabaseAviraDB);
            return dapper.Query<StatusViewModel>(StatusQueries.StoreProcSelectAll
                            , new { @statusType= statusType }
                            , null, true, null, CommandType.StoredProcedure);
        }

    }
}
