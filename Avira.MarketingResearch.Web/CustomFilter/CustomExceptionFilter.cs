﻿using Avira.MarketingResearch.LogService;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Avira.MarketingResearch.Web.CustomFilter
{
    public class CustomExceptionFilter : ExceptionFilterAttribute
    {
        private ILoggerManager _logger;
        public CustomExceptionFilter()
        {
            _logger = new LoggerManager();
        }
        public override void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled && filterContext.Exception is Exception)
            {
                _logger.LogError(filterContext.Exception.Message);
                filterContext.Result = new RedirectResult("~/Error.html");
                filterContext.ExceptionHandled = true;
            }
        }
    }
}
