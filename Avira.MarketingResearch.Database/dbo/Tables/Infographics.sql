﻿CREATE TABLE [dbo].[Infographics] (
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [ProjectId]         UNIQUEIDENTIFIER NOT NULL,
    [LookupCategoryId]  UNIQUEIDENTIFIER NOT NULL,
    [InfogrTitle]       NVARCHAR (512)   NULL,
    [InfogrDescription] NVARCHAR (MAX)   NULL,
    [SequenceNo]        SMALLINT         CONSTRAINT [DF_Infographics_SequenceNo] DEFAULT ((0)) NOT NULL,
    [ImageActualName]   NVARCHAR (200)   NULL,
    [ImageName]         NVARCHAR (100)   NULL,
    [ImagePath]         NVARCHAR (200)   NULL,
    [CreatedOn]         DATETIME2 (7)    NULL,
    [UserCreatedById]   UNIQUEIDENTIFIER NULL,
    [ModifiedOn]        DATETIME2 (7)    NULL,
    [UserModifiedById]  UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_Infographics] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Infographics_LookupCategory] FOREIGN KEY ([LookupCategoryId]) REFERENCES [dbo].[LookupCategory] ([Id]),
    CONSTRAINT [FK_Infographics_Project] FOREIGN KEY ([ProjectId]) REFERENCES [dbo].[Project] ([ID])
);

