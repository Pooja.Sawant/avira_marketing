﻿
CREATE PROCEDURE [dbo].[SApp_GetCountryByTrendID]      
 (@TrendID uniqueidentifier = null,  
 @RegionIDList dbo.udtUniqueIdentifier READONLY,
 @includeDeleted bit = 0,  
 @OrdbyByColumnName NVARCHAR(50) ='RegionName',  
 @SortDirection INT = -1,  
 @PageStart INT = 0,  
 @PageSize INT = null)      
 AS      
/*================================================================================      
Procedure Name: SApp_GetRegionbyTrendID  
Author: Swami      
Create date: 04/12/2019      
Description: Get list from Region table by TrendID      
Change History      
Date  Developer  Reason      
__________ ____________ ______________________________________________________      
04/12/2019 Swami   Initial Version      
================================================================================*/     
      
BEGIN      
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED   
if @PageSize is null or @PageSize =0  
set @PageSize = 10  
   
begin  
--;WITH CTE_TBL AS (  
 SELECT  distinct
 TrendCountryMap.TrendId,
 [Region].[Id] AS [RegionId],      
  [Region].[RegionName],     
   [Region].[RegionLevel],
   [Country].Id as CountryId,
   [Country].CountryName,
   [Country].CountryCode,
   [Country].IsDeleted,  
   [Region].ParentRegionId,  
   TrendCountryMap.Impact as Impact,
   TrendCountryMap.EndUser as EndUser,
   TrendCountryMap.Id as TrendCountryMapId,
 CASE  
  WHEN @OrdbyByColumnName = 'CountryName' THEN [Country].CountryName
  WHEN @OrdbyByColumnName = 'CountryCode' THEN [Country].CountryCode  
  WHEN @OrdbyByColumnName = 'RegionName' THEN [Region].RegionName  
  WHEN @OrdbyByColumnName = 'RegionLevel' THEN cast([Region].[RegionLevel] as nvarchar(10))
  ELSE [Region].RegionName  
 END AS SortColumn  
  
FROM dbo.Country
inner join CountryRegionMap
on CountryRegionMap.CountryId = Country.Id
inner join [dbo].Region
on Region.id = CountryRegionMap.RegionId
LEFT JOIN dbo.TrendCountryMap 
ON Country.Id = TrendCountryMap.CountryId   
and (@TrendID is null or TrendCountryMap.TrendId = @TrendID)
where (@includeDeleted = 1 or Country.IsDeleted = 0)  
and (not exists (select 1 from @RegionIDList) or Region.id in (select id from @RegionIDList)) 

 --SELECT   
 --   CTE_TBL.*,  
 --   tCountResult.TotalItems  
 -- FROM CTE_TBL  
 -- CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult  
 -- ORDER BY   
 --   CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,  
 --   CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC   
 -- OFFSET @PageStart ROWS  
 -- FETCH NEXT @PageSize ROWS ONLY;  
 END  
  
END