﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Avira.MarketingResearch.LogService;
using Avira.MarketingResearch.Model.ViewModels;
using Avira.MarketingResearch.Model.ViewModels.ImportExport;
using Avira.MarketingResearch.Model.ViewModels.Masters;
using Avira.MarketingResearch.Web.CustomFilter;
using Avira.MarketingResearch.WebApiService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Avira.MarketingResearch.Web.Controllers
{
    [NoCache]
    [Authorization]
    public class ImportRequestController : BaseController
    {
        public readonly IServiceRepository _serviceRepository;
        private ILoggerManager _logger;
        private readonly IConfiguration _configuration;

        public ImportRequestController(IServiceRepository serviceRepository, ILoggerManager logger, IConfiguration configuration)
        {
            _serviceRepository = serviceRepository;
            _logger = logger;
            _configuration = configuration;
        }

        public IActionResult Index()
        {
            HttpResponseMessage StatusResponse = _serviceRepository.GetByIdResponse("Status", "ImportStatus");
            List<StatusViewModel> statusViewModelList = StatusResponse.Content.ReadAsAsync<List<StatusViewModel>>().Result;
            ViewBag.StatusList = statusViewModelList;
            ViewBag.Message = string.Empty;
            return View();
        }

        [HttpPost]
        public IActionResult Index(IFormFile formFile, string importTemplate)
        {
            int fileSize =int.Parse(_configuration.GetSection("ImportFileSize")["MSFileSize"]);
            double MaxContentLength = 1024 * 1024 * fileSize;
            if (formFile == null || formFile.Length <= 0)
            {
                ViewBag.Message = "Import file is empty";
                HttpResponseMessage StatusResponse = _serviceRepository.GetByIdResponse("Status", "ImportStatus");
                List<StatusViewModel> statusViewModelList = StatusResponse.Content.ReadAsAsync<List<StatusViewModel>>().Result;
                ViewBag.StatusList = statusViewModelList;
                return View();
            }
            if (formFile.Length > MaxContentLength)
            {
                ViewBag.Message = "Only "+fileSize+" MB File Allowed";
                HttpResponseMessage StatusResponse = _serviceRepository.GetByIdResponse("Status", "ImportStatus");
                List<StatusViewModel> statusViewModelList = StatusResponse.Content.ReadAsAsync<List<StatusViewModel>>().Result;
                ViewBag.StatusList = statusViewModelList;
                return View();
            }

            if (!Path.GetExtension(formFile.FileName).Equals(".xlsx", StringComparison.OrdinalIgnoreCase))
            {
                ViewBag.Message = "Not Support file extension";
                HttpResponseMessage StatusResponse = _serviceRepository.GetByIdResponse("Status", "ImportStatus");
                List<StatusViewModel> statusViewModelList = StatusResponse.Content.ReadAsAsync<List<StatusViewModel>>().Result;
                ViewBag.StatusList = statusViewModelList;
                return View();
            }
            
            if(formFile!=null && !string.IsNullOrEmpty(importTemplate))
            {
                ImportRequestViewModel model = new ImportRequestViewModel();

                using (var memoryStream = new MemoryStream())
                {
                    formFile.CopyTo(memoryStream);
                    model.fileByte = memoryStream.ToArray();
                }
                model.TemplateName = importTemplate;
                model.ImportFileName = formFile.FileName;
                model.FileName = formFile.FileName;
                model.StatusName = "Pending";

                var contollerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                HttpResponseMessage response = _serviceRepository.PostResponse(contollerName, model);
                response.EnsureSuccessStatusCode();
                ViewData viewModel = response.Content.ReadAsAsync<ViewData>().Result;
                if (viewModel != null)
                {
                    ViewBag.Message = viewModel.Message;
                    HttpResponseMessage StatusResponse = _serviceRepository.GetByIdResponse("Status", "ImportStatus");
                    List<StatusViewModel> statusViewModelList = StatusResponse.Content.ReadAsAsync<List<StatusViewModel>>().Result;
                    ViewBag.StatusList = statusViewModelList;
                }

            }

            return View();
        }

        public IActionResult _ImportRequestHistory(ImportRequestViewModel ImportRequestModel)
        {
            HttpResponseMessage Response = _serviceRepository.GetByAllResponse("ImportRequest", ImportRequestModel);
            List<ImportRequestViewModel> ImportRequestList = Response.Content.ReadAsAsync<List<ImportRequestViewModel>>().Result;
            return PartialView(ImportRequestList);
        }

        public FileResult ExcelDownload(Guid Id)
        {
            HttpResponseMessage Response = _serviceRepository.GetByIdResponse("ImportRequest",Id.ToString());
            ImportRequestViewModel ImportRequest = Response.Content.ReadAsAsync<ImportRequestViewModel>().Result;
            string fileName = Path.GetFileName(ImportRequest.ImportExceptionFilePath);
            if(ImportRequest!=null && ImportRequest.fileByte!=null)
            {
                return File(ImportRequest.fileByte, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
            }
            else
            {
                return File("Data not found", System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
            }
        }

    }
}