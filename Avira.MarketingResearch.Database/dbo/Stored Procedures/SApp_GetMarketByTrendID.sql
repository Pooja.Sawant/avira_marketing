﻿      
 CREATE PROCEDURE [dbo].[SApp_GetMarketByTrendID]            
 (@TrendID uniqueidentifier = null,    
 @MarketName nvarchar(256)='',    
 @includeDeleted bit = 0,        
 @OrdbyByColumnName NVARCHAR(50) ='TrendMarketMapID',        
 @SortDirection INT = -1,        
 @PageStart INT = 0,        
 @PageSize INT = null)            
 AS            
/*================================================================================            
Procedure Name: SApp_GetMarketbyTrendID        
Author: Swami            
Create date: 04/12/2019            
Description: Get list from Market table by TrendID            
Change History            
Date  Developer  Reason            
__________ ____________ ______________________________________________________            
04/12/2019 Swami   Initial Version 
04/17/2019 Praveen Added search parameter for marketname   
================================================================================*/           
            
BEGIN            
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED         
if @PageSize is null or @PageSize =0        
set @PageSize = 10        
if isnull(@MarketName,'') = '' set @MarketName = '' 
else set @MarketName = '%'+ @MarketName +'%';
begin        
;WITH CTE_TBL AS (        
 SELECT         
 --@TrendID as TrendID,          
 [Market].[Id] AS [MarketId],            
  [Market].[MarketName],           
   [Market].[Description],        
   [Market].IsDeleted,        
   [Market].ParentId,        
   TrendMarketMap.Impact as ImpactId,      
   TrendMarketMap.EndUser,      
   TrendMarketMap.TrendId,      
   TrendMarketMap.Id as TrendMarketMapId,      
  cast (case when exists(select 1 from dbo.TrendMarketMap         
     where TrendMarketMap.TrendId = @TrendID        
     and TrendMarketMap.IsDeleted = 0        
     and TrendMarketMap.MarketId = Market.Id) then 1 else 0 end as bit) as IsMapped,        
 CASE        
  WHEN @OrdbyByColumnName = 'MarketName' THEN [Market].MarketName        
  WHEN @OrdbyByColumnName = 'Description' THEN [Market].[Description]
  when @OrdbyByColumnName = 'IsMapped' THEN cast(case when TrendMarketMap.TrendId is null then 1 else 0 end as varchar(2)) 
  ELSE [Market].MarketName        
 END AS SortColumn        
        
FROM [dbo].Market      
LEFT JOIN dbo.TrendMarketMap       
ON Market.Id = TrendMarketMap.MarketId
and TrendMarketMap.TrendId = @TrendID
and TrendMarketMap.IsDeleted = 0
where (@MarketName = '' or [Market].[MarketName] LIKE @MarketName)
AND (@includeDeleted = 1 or Market.IsDeleted = 0)        
)        
        
 SELECT         
    CTE_TBL.*,        
    tCountResult.TotalItems        
  FROM CTE_TBL        
  CROSS JOIN (SELECT Count(*) AS TotalItems FROM CTE_TBL) AS tCountResult        
  ORDER BY         
    CASE WHEN @SortDirection = 1  THEN CTE_TBL.SortColumn END ASC,        
    CASE WHEN @SortDirection < 0 THEN CTE_TBL.SortColumn END DESC         
  OFFSET @PageStart ROWS        
  FETCH NEXT @PageSize ROWS ONLY;        
  END        
        
END