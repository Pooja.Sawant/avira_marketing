﻿CREATE TABLE [dbo].[ShareHoldingPatternMap] (
    [Id]                     UNIQUEIDENTIFIER NOT NULL,
    [ShareHoldingName]       NVARCHAR (256)   NULL,
    [CompanyId]              UNIQUEIDENTIFIER NOT NULL,
    [EntityTypeId]           UNIQUEIDENTIFIER NOT NULL,
    [ShareHoldingPercentage] DECIMAL (19, 4)  NULL,
    [CreatedOn]              DATETIME2 (7)    NULL,
    [UserCreatedById]        UNIQUEIDENTIFIER NULL,
    [ModifiedOn]             DATETIME2 (7)    NULL,
    [UserModifiedById]       UNIQUEIDENTIFIER NULL,
    [IsDeleted]              BIT              CONSTRAINT [DF_ShareHoldingPatternMap_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DeletedOn]              DATETIME2 (7)    NULL,
    [UserDeletedById]        UNIQUEIDENTIFIER NULL,
    CONSTRAINT [Pk_ShareHoldingPatternMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ShareHoldingPatternMap_AviraUser] FOREIGN KEY ([UserCreatedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_ShareHoldingPatternMap_AviraUser1] FOREIGN KEY ([UserModifiedById]) REFERENCES [dbo].[AviraUser] ([Id]),
    CONSTRAINT [FK_ShareHoldingPatternMap_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([Id]),
    CONSTRAINT [FK_ShareHoldingPatternMap_EntityType] FOREIGN KEY ([EntityTypeId]) REFERENCES [dbo].[EntityType] ([Id])
);

